<?php

class TripleSMS
{

    public function __construct()
    {
    }

    public function Send($params)
    {

        $myObj = new stdClass();

        $myObj->sender = $params['sender'];
        $myObj->to = $params['phone'];
        $myObj->body = $params['message'];

        $myJSON = json_encode($myObj);

        $result = $this->callAPI('POST', 'https://triplesms.com/api/v1/message',$myJSON);
        return $result;

    }

    public function callAPI($method, $url, $data)
    {
        $curl = curl_init();

        switch ($method){
          case "POST":
             curl_setopt($curl, CURLOPT_POST, 1);
             if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
             break;
          case "PUT":
             curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);                              
             break;
          default:
             if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
        }


        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Authorization: Bearer NDJlYTQxZGRhNTNkY2NkOTMyN2RlOGIwOTNiYTkwYzcwMzI4ODZiMzkwM2Q5OGRi',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        //var_dump($result);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }


}
