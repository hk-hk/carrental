
<section class="newset-cars clearfix pa pt-40">
	<div class="container">
    	<div class="row">
        	<h2 class="section-title black text-center mb-20"><?php echo getLang('TOUR_PACKAGE_HEADING')?></h2>
            <div class="newest-car-box owl-carousel">
            <?php foreach ($tour_packages as $key => $value) { 
                $tour_url = $value->id;
                ?>
                <div class="car-box-inner item">
                    <div class="car-image">
                        <img  src="<?php echo base_url();?>uploads/article/<?php echo $value->featured_img; ?>" alt="">
                    
                    </div>
                    <div class="car-details pa">
                        <?php $activeLanguage = $this->session->userdata('active_language');
                            if($activeLanguage=='mk'){
                                $value->title = ($value->title_mya!='')?$value->title_mya:$value->title;
                            }
                        ?>
                        <a class="book_nowh m0 mb mb" href="<?php echo site_url("attractive-places/$tour_url");?>"><?php echo getLang('TEXT_PACKAGE_DETAIL')?></a>
                        <h4><?= $value->title ?></h4>
                        <ul>
                            <p><?= $value->short_description ?></p>
                            
                        </ul>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</section><!-- / Newest Car Section / -->
