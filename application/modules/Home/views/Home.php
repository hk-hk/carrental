<?php  $this->load->view('home_search'); 
$activeLanguage = $this->session->userdata('active_language');?>
<!-- / <section class="our-policy clearfix">
	<div class="container">
    	<?php echo $policies_content; ?>
    </div>
</section>Our Policy / -->
<?php // $this->load->view('newest_car'); ?>
<?php $this->load->view('tour_packages'); ?>
<section class="weclome-section clearfix">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="welcome-text">
                    <?php if($activeLanguage=='mk'){
                            $welcome->title = ($welcome->title_mya!='')?$welcome->title_mya:$welcome->title;
                            $welcome->content = ($welcome->content_mya!='')?$welcome->content_mya:$welcome->content;
                        }else{
                            $welcome->title = $welcome->title;
                            $welcome->content = $welcome->content;
                        }
                        ?>
                    <h2 class="section-title black"><?php echo str_replace("sonic star", "<span>Sonic Star</span>", $welcome->title); ?></h2>
                    <?php echo $welcome->content; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="welcome-image">
                        <ul class="owl-carousel">
                            <?php $img = json_decode($welcome->banner_images); ?>
                            <?php foreach($img as $k => $img){
                                ?>
                                <li class="item">
                                    <img src="<?php echo base_url();?>uploads/article/banner/<?php echo $img;?>" alt="" />
                                </li>
                                <?php
                            }?>
                        </ul>
                </div>
            </div>
        </div>
    </div>
</section><!-- Welcome Section -->

<section class="client-section clearfix">
	<div class="container">
    	<div class="row">
            <h2 class="section-title white text-center"><?php echo $this->home->get_attributes(array(30))[84]['title'];?></h2>
            <div class="owl-carousel">
                <?php 
                if(!empty($client_comment)){
                    foreach ($client_comment as $key => $comment) {
                        if($activeLanguage=='mk'){
                            $comment->short_description = ($comment->short_description_mya!='')?$comment->short_description_mya:$comment->short_description;
                            $comment->content = ($comment->content_mya!='')?$comment->content_mya:$comment->content;
                        }
        
                        if($comment->status == 1){ ?>
                        <div class="item client-inner-box">
                            <p class="comment-sec"><?php echo substr($comment->short_description,0,330); ?></p>
                            <div class="client-info">
                            <?php if($comment->featured_img!=''){ ?>
                            <span class="client-img"><img src="<?php echo base_url();?>uploads/article/<?php echo $comment->featured_img ?>" /></span> 
                            <?php } ?>
                            <span class="client-name"><?php echo $comment->title; ?></span></div>
                        </div>
                        <?php }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</section><!-- Client Section -->
<!--
<section class="Brands-Section clearfix">
    <div class="container">
        <div class="row">
            <h2 class="section-title black text-center"><?php echo getLang('OUR_BRANDS_LOGO');?></h2>
            <?php foreach ($all_models as $key => $value) {?>
            <div class="col-sm-2">
                <div class="brand-image-box">
                    <?php if($value->short_desc == ''){ ?>
                    <img src="<?php echo base_url();?>public/front/images/No_Image_Available.png" alt="" height="62" width="90" />
                    <?php }else if(!file_exists("./uploads/model/thumb/90X62/".$value->short_desc)){ ?>
                    <img src="<?php echo base_url();?>public/front/images/No_Image_Available.png" alt="" height="62" width="90" />
                    <?php }else{ ?>
                    <img src="<?php echo base_url();?>uploads/model/thumb/90X62/<?php echo $value->short_desc;?>" alt="" />
                    <?php } ?>
                </div>
            </div>
            <?php 
            } ?>
        </div>
    </div>
</section>
-->
<?php if(FALSE){ ?>
<script>
$(function(){
   $(".select2").select2({
        placeholder: "Select a state"
    });
});    
</script>
<?php }?>