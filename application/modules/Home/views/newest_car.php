
<section class="newset-cars clearfix">
	<div class="container">
    	<div class="row">
        	<h2 class="section-title black text-center"><?php echo getLang('NEWEST_CAR_HEADING')?></h2>
            <div class="newest-car-box owl-carousel">
            <?php foreach ($get_newest_cars as $key => $car) { 
                $car_url = strtolower(str_replace(" ", "-", $car->id."-".$makers[$car->maker]['title']."-".$model[$car->model]['title']));
             $car_image = json_decode($car->car_images);
                ?>
                <div class="car-box-inner item">
                    <div class="car-image">
                    <?php if(!empty($car_image)) { ?> 
                        <img src="<?php echo base_url();?>uploads/car/<?php echo $car_image[0];?>" alt="">
                        <?php }else{?>
                        <img  src="<?php echo base_url();?>uploads/car/default.jpg" alt="">
                    <?php }?>
                    </div>
                    <div class="car-details">
                        <h4><?php echo  $car->maker_name." - ".$car->model_name; ?></h4>
                        <ul>
                            <?php if(isset($attributes_by_id[$car->car_type]['title'])){ ?><li><?php echo $attributes_by_id[$car->no_of_passanger]['title']; ?></li>
                            <?php }?>
                            <?php if(isset($attributes_by_id[$car->car_type]['title'])){ ?><li><?php echo $attributes_by_id[$car->car_type]['title']; ?></li><?php }?>
                            <?php if(isset($attributes_by_id[$car->fuel_type]['title'])){ ?><li><?php echo $attributes_by_id[$car->fuel_type]['title']; ?></li><?php }?>
                            <?php if(isset($attributes_by_id[$car->car_condition]['title'])){ ?><li><?php echo $attributes_by_id[$car->car_condition]['title']; ?></li><?php }?>
                        </ul>
                        <a class="book_nowh" href="<?php echo site_url("car/$car_url");?>"><?php echo getLang('TEXT_BOOK_NOW_BTN')?></a>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</section><!-- / Newest Car Section / -->
