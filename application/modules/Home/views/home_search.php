<style>
    .error {
        border-color: red !important;
        color: red;
    }
</style>
<?php //ksort($car_attributes[70]); ?>
<section class="banner-sc">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10  col-md-offset-1">
                <div class="search-form">
                    <div class="form-header search-form-tab">
                    	<ul class="nav nav-tabs">
                        	<li class="active cus-bg">
                            	<a href="#CarHire" data-toggle="tab">
                                    <span class="search-icon">
                                        <img src="<?php echo base_url('public/front/') ?>images/car-icon.png" alt="" />
                                    </span>
                                    <h3>Car Rental</h3>
                                </a>
                            </li>
                            <!-- <li>
                            	<a href="#FilghtHire" data-toggle="tab">
                                    <span class="search-icon">
                                        <img src="<?php echo base_url('public/front/') ?>images/flight-icon.png" alt="" />
                                    </span>
                                    <h3>Air Tickets</h3>
                                </a>    
                            </li> -->
                        </ul>
                        
                    </div>
                    <div id="CatagoryTabs" class="search-category tab-content"> 
                    	<div id="CarHire" class="tab-pane active">
                            <ul class="nav nav-tabs">
                                <li class="active rent_type" id="36">
                                    <a href="#Daily" data-toggle="tab"><?php echo getLang('CAR_FILTER_BY_DAILY');?></a>
                                </li>
                                <li class="rent_type" id="37">
                                    <a href="#Weekly" data-toggle="tab"><?php echo getLang('CAR_FILTER_BY_WEEKLY');?></a>
                                </li>
                                <li class="rent_type" id="38">
                                    <a href="#Monthly" data-toggle="tab"><?php echo getLang('CAR_FILTER_BY_MONTHLY');?></a>
                                    <input type="hidden" id="selected_rent_type" value="36"/>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane daily-tab active" id="Daily">
                                    <form action="<?php echo site_url('cars'); ?>" method="post" id="frm_daily">
                                        <div class="loation-box"> <?php //print_r($car_attributes[70]); ?>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="pickup-option-box">
                                                        <ul>
                                                            <li>
                                                                <input type="checkbox" name="daily[airport]" id="pickup" class="airport_active" value="pickup" />
                                                                <label for="pickup"><?php echo getLang('AIRPORT_PICKUP_OPTION');?></label>
                                                            </li>
                                                            <li>
                                                                <input type="checkbox" name="daily[airport]" id="dropoff"  class="airport_active"  value="dropoff"/>
                                                                <label for="dropoff"><?php echo getLang('AIRPORT_DROPOFF_OPTION');?></label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <div class="pickup-option-box in_city_highway">
                                                        <ul>
                                                            <li>
                                                                <input type="radio" name="daily[incity]" id="city" value="in_city" checked="" />
                                                                <label for="city"><?php echo getLang('CAR_FILTER_IN_CITY');?></label>
                                                            </li>
                                                            <li id="HighwayClick">
                                                                <input type="radio" name="daily[incity]" id="Highway" value="highway"/>
                                                                <label for="Highway"><?php echo getLang('CAR_FILTER_HIGHWAY');?></label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                        </div><!-- / Location 1 / -->
                                        
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="loation-box-cus">
                                                        <?php if (isset($car_attributes[70])) { ?>
                                                        <label id="car_city_daily_lbl"><?php echo getLang('CAR_FILTER_PICKUP_CITY');?></label>
                                                            <div class="pickup-location-input">
                                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                <select name="car_city" id="car_city_daily" class="city_select select2" >
                                                                    <?php foreach ($car_attributes[70] as $city) {
                                                                        if($city['id']==89){ ?>
                                                                            <option value="<?php echo $city['id']; ?>" selected="selected"><?php echo $city['title']; ?></option>
                                                                        <?php }else{ ?>
                                                                            <option value="<?php echo $city['id']; ?>"><?php echo $city['title']; ?></option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-3"  id="desitanted-location">
                                                    <div class="loation-box-cus">
                                                        <label><?php echo getLang('CAR_FILTER_DESTINATION');?></label>
                                                        <div class="pickup-location-input">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                            <select name="daily[destination]" id="destination" class="select2" ></select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 air_pickup">
                                                    <div class="loation-box-cus">
                                                        <label id="daily_pickup_date_lbl"><?php echo getLang('CAR_FILTER_PICKUP_DATE_TIME');?></label>
                                                        <div class="pickup-location-input">
                                                            <div class="date-box-cus">
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                <input type="text" name="daily[pickup_date]" id="daily_pickup_date" placeholder="<?php echo getLang('CAR_FILTER_PICKUP_DATE');?>" value="<?php echo date("m/d/Y");?>">
                                                            </div>
                                                            <div class="time-box-cus">
                                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                <input type="text" class="timepicker" id="daily_pickup_time" name="daily[pickup_time]" value="<?php echo '8:00am';?>"> 
                                                            </div>
                                                        </div>
                                                    </div><!-- / Location 3 / -->
                                                </div>
                                                <div class="col-md-3 air_dropoff">
                                                    <div class="loation-box-cus">
                                                        <label><?php echo getLang('CAR_FILTER_DROPOFF_DATE_TIME');?></label>
                                                        <div class="pickup-location-input">
                                                            <div class="date-box-cus">
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                <input type="text" id="daily_dropoff_date" name="daily[dropoff_date]" placeholder="<?php echo getLang('CAR_FILTER_DROPOFF_DATE');?>" value="<?php echo date("m/d/Y");?>">
                                                            </div>
                                                            <div class="time-box-cus">
                                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                <input type="text" class="timepicker" id="daily_dropoff_time" name="daily[dropoff_time]" value="<?php echo '6:00pm';?>">
                                                                    
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="clearfix"></div>
                                        <!-- / Location 4 / -->
                                        <input type="hidden" name="rent_type" value="36" id="daily_rent_type" />
                                        <div class="search-submit col-md-2 col-sm-11 col-xs-10">
                                            <input type="submit" id="sbt_daily" name="" onclick="return checkValid();" value="<?php echo getLang('CAR_FILTER_SEARCH_BTN');?>" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="Weekly">
                                    <form action="<?php echo site_url('cars'); ?>" method="post">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="loation-box-cus">
                                                        <label id="car_city_weekly_lbl"><?php echo getLang('CAR_FILTER_PICKUP_CITY');?></label>
                                                        <div class="pickup-location-input">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                            <select name="car_city" id="car_city_weekly" class="select2">
                                                                <?php foreach ($car_attributes[70] as $city) {
                                                                    if($city['id']==89){ ?>
                                                                        <option value="<?php echo $city['id']; ?>" selected="selected"><?php echo $city['title']; ?></option>
                                                                    <?php }else{ ?>
                                                                        <option value="<?php echo $city['id']; ?>"><?php echo $city['title']; ?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div><!-- / Location 1 / -->
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="loation-box-cus">
                                                        <label id="weekly_pickup_date_lbl"><?php echo getLang('CAR_FILTER_PICKUP_DATE_TIME');?></label>
                                                        <div class="pickup-location-input">
                                                            <div class="date-box-cus">
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                <input type="text" name="weekly[pickup_date]" id="weekly_pickup_date"  placeholder="<?php echo getLang('CAR_FILTER_PICKUP_DATE');?>" value="<?php echo date("m/d/Y");?>">
                                                            </div>
                                                            <div class="time-box-cus">
                                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                <input type="text" class="timepicker" name="weekly[pickup_time]" value="<?php echo '8:00am';?>">
                                                                    
                                                            </div>
                                                        </div>
                                                    </div><!-- / Location 2 / -->
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="loation-box-cus">
                                                        <label><?php echo getLang('CAR_FILTER_NO_OF_WEEK');?></label>
                                                        <div class="pickup-location-input pickup-select">
                                                            <select id="" name="weekly[no_of_weeks]">
                                                                <option value="1">1 <?php echo getLang('FILTER_TEXT_WEEK');?></option>
                                                                <option value="2">2 <?php echo getLang('FILTER_TEXT_WEEKS');?></option>
                                                                <option value="3">3 <?php echo getLang('FILTER_TEXT_WEEKS');?></option>
                                                                <option value="4">4 <?php echo getLang('FILTER_TEXT_WEEKS');?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <!-- / Location 3 / -->   
                                        <div class="search-submit col-md-2 col-sm-11 col-xs-10">
                                            <input type="submit" name="" onclick="return checkValid();" value="<?php echo getLang('CAR_FILTER_SEARCH_BTN');?>" />
                                        </div>
                                        <div class="clearfix"></div>
                                        <input type="hidden" name="rent_type" value="37" id="weekly_rent_type" />
                                    </form>
                                </div>
                                <div class="tab-pane" id="Monthly">
                                    <form action="<?php echo site_url('cars'); ?>" method="post">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="loation-box-cus">
                                                        <label id="car_city_monthly_lbl"><?php echo getLang('CAR_FILTER_PICKUP_CITY');?></label>
                                                        <div class="pickup-location-input">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                            <select name="car_city" id="car_city_monthly" class="select2" >
                                                                <?php foreach ($car_attributes[70] as $city) {
                                                                    if($city['id']==89){ ?>
                                                                        <option value="<?php echo $city['id']; ?>" selected="selected"><?php echo $city['title']; ?></option>
                                                                    <?php }else{ ?>
                                                                        <option value="<?php echo $city['id']; ?>"><?php echo $city['title']; ?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div><!-- / Location 1 / -->
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="loation-box-cus">
                                                        <label id="monthly_pickup_date_lbl"><?php echo getLang('CAR_FILTER_PICKUP_DATE_TIME');?></label>
                                                        <div class="pickup-location-input">
                                                            <div class="date-box-cus">
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                <input type="text" name="monthly[pickup_date]" id="monthly_pickup_date" placeholder="<?php echo getLang('CAR_FILTER_PICKUP_DATE');?>" value="<?php echo date("m/d/Y");?>">
                                                            </div>
                                                            <div class="time-box-cus">
                                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                <input type="text" class="timepicker"  name="monthly[pickup_time]" value="<?php echo '8:00am';?>">
                                                                    
                                                            </div>
                                                        </div>
                                                    </div><!-- / Location 2 / -->
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="loation-box-cus">
                                                        <label><?php echo getLang('CAR_FILTER_NO_OF_MONTH');?></label>
                                                        <div class="pickup-location-input pickup-select">
                                                            <select id="" name="monthly[no_of_month]">
                                                                <?php for ($i = 1; $i <= 12; $i++) {  $monthText = ($i==1)?getLang('FILTER_TEXT_MONTH'):getLang('FILTER_TEXT_MONTHS')?>
                                                                    <option value="<?php echo $i; ?>"><?php echo $i.$monthText; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div><!-- / Location 3 / --> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="search-submit col-md-2 col-sm-11 col-xs-10">
                                            <input type="submit" name="" onclick="return checkValid();" value="<?php echo getLang('CAR_FILTER_SEARCH_BTN');?>" />
                                        </div>
                                        <div class="clearfix"></div>
                                        <input type="hidden" name="rent_type" value="38" id="monthly_rent_type" />
                                    </form>
                                </div>
                            </div>
                        </div> 
                        <!-- <div id="FilghtHire" class="tab-pane air-ticket-box">   
                        	<?php echo $comming_soon_text; ?>
                            
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-5 col-md-5 pull-right">
                <div class="dash-banner"><img class="dash-banner-img" src="<?php echo base_url("uploads/model/".$banner_img)?>"</div>
            </div> -->
        </div>
        <div class="clearfix"></div> 
    </div><!-- Container Closed -->
</section><!-- / Banner Section / -->
<?php $this->load->view("Car_manager/front/multi_days_selection");?>
<?php $this->load->view("Car_manager/front/multi_locations_selection");?>