<?php
$route['']     = 'Home/home/index';
$route['changecurrency/(:any)']     = 'Home/home/changeCurrancy/$1';
$route['change-lang/(:any)']     = 'Home/home/changeLang/$1';
$route['set-default-country']     = 'Home/home/setDefaultCountry';
$route['/register']     = 'Home/home/registration';
$route['/agent-register']     = 'Home/home/agentRegistration';
$route['cities'] = 'Home/home/cities';