<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends MY_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function get_page($slug) // Get total row count of search query
    {
        $this->db->from('cr_manage_attributes');
        $this->db->where('id', $slug);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_home_content($slug) // Get total row count of search query
    {
        $this->db->from('cr_articles');
        $this->db->where('url', $slug);
        $query = $this->db->get()->row_object();
        return $query;
    }

    function get_all_models(){
    	$res = $this->db->query("SELECT * FROM cr_manage_attributes WHERE parent_id IN (SELECT id from cr_manage_attributes WHERE parent_id = 45) AND status = 1 ORDER BY ordering ASC LIMIT 0, 6");
    	return $res->result_object();
    }

    function get_newest_cars(){
        $from_date = $this->session->userdata('booking_from_date');
        $to_date = $this->session->userdata('booking_to_date');
        $res = $this->db->query("SELECT car.*, makers.title as maker_name, models.title as model_name FROM cr_manage_cars as car "
                . "INNER JOIN `cr_users` as usr ON `usr`.`id` = `car`.`user_id`"
                . "INNER JOIN `cr_manage_attributes` as makers ON `makers`.`id` = `car`.`maker`"
                . "INNER JOIN `cr_manage_attributes` as models ON `models`.`id` = `car`.`model`"
                . " WHERE car.status = '1' AND usr.user_active='YES' AND `usr`.`user_role`='supplier' "
                . 'AND `car`.`id` NOT IN (SELECT DISTINCT `car_id` FROM `cr_orders` WHERE ( "'.$from_date.'" BETWEEN cr_orders.pickup_date AND cr_orders.drop_date OR "'.$to_date.'" BETWEEN cr_orders.pickup_date AND cr_orders.drop_date ))'
                . 'AND `car`.`id` NOT IN (SELECT DISTINCT `car_id` FROM `cr_supplier_booking` WHERE ( "'.$from_date.'" BETWEEN cr_supplier_booking.from_date AND cr_supplier_booking.to_date OR "'.$to_date.'" BETWEEN cr_supplier_booking.from_date AND cr_supplier_booking.to_date ))'
                . "ORDER BY id DESC LIMIT 0, 10");
        $data = $res->result_object();
        //echo $this->db->last_query();die;
        return $data;
    }
}
