<?php
defined('BASEPATH') OR exit('ယခုစာမ်က္နာအား ၾကည့္ရႈ႕ခြင့္မျပဳပါ');

//MultyDays Popup in City
$lang['CAR_PER_DAY_HOURS_HEADING'] = 'ေန႔စဥ္သံုးမည့္အခ်ိန္အားေရႊးခ်ယ္ပါ';
$lang['CAR_PER_DAY_HOURS_SUBMIT_BTN'] = ' ႏွိပ္ပါ';
//MultyDays Popup in Highway PDDP=ေန႔စဥ္ဦးတည္ရာေနရာ
$lang['CAR_PER_DAY_DESTINATION_HEADING'] = 'ေန႔စဥ္ ခရီးအားေရႊးခ်ယ္ပါ';
$lang['CAR_PDDP_FROM'] = 'ေန႔ရက္';
$lang['CAR_PDDP_TO'] = 'သြားမည့္ၿမိဳ႕';
$lang['CAR_PDDP_PICKUP_TIME'] = ' ၾကိဳရမည့္အခ်ိန္';
$lang['CAR_PDDP_DROPOFF_TIME'] = ' ပို႔ရမည့္အခ်ိန္';
$lang['CAR_PER_DAY_DESTINATION_SUBMIT_BTN'] = ' ႏွိပ္ပါ';

$lang['CAR_FILTER_HEADING'] = 'ငွားမည့္ကားအား ရွာေဖြပါ';
$lang['CAR_FILTER_BY_DAILY'] = 'ရက္ အငွား';
$lang['CAR_FILTER_BY_WEEKLY'] = 'အပါတ္စဥ္ အငွား';
$lang['CAR_FILTER_BY_MONTHLY'] = 'လ အငွား';
$lang['CAR_FILTER_PICKUP_CITY'] = ' ၾကိဳရမည့္ ျမိဳ႕';
$lang['CAR_FILTER_IN_CITY'] = ' ျမိဳ႕တြင္း';
$lang['CAR_FILTER_HIGHWAY'] = ' ျမိဳ႕ျပင္ခရီး';
$lang['CAR_FILTER_DESTINATION'] = ' သြားမည့္ၿမိဳ႕';
$lang['CAR_FILTER_PICKUP_DATE_TIME'] = ' ၾကိဳရမည့္ ေန႔၊ အခ်ိန္';
$lang['CAR_FILTER_DROPOFF_DATE_TIME'] = 'ျပန္မည့္ ေန႔၊ အခ်ိန္';
$lang['CAR_FILTER_PICKUP_DATE'] = ' ၾကိဳရမည့္ ေန႔';
$lang['CAR_FILTER_NO_OF_WEEK'] = 'ျပန္မည့္ ေန႔';
$lang['FILTER_TEXT_WEEK'] = '  ပါတ္';
$lang['FILTER_TEXT_WEEKS'] = '  ပါတ္';
$lang['FILTER_TEXT_MONTH'] = ' လ';
$lang['FILTER_TEXT_MONTHS'] = ' လ';
$lang['CAR_FILTER_NO_OF_MONTH'] = 'လ စုုစုုေပါင္း';
$lang['CAR_FILTER_SEARCH_BTN'] = 'ရွာရန္';
$lang['NEWEST_CAR_HEADING'] = '၀န္ေဆာင္မႈ အဆင့္ျမင့္ကားမ်ား';
$lang['TEXT_BOOK_NOW_BTN'] = 'ေအာ္ဒါ တင္မည္';
$lang['OUR_BRANDS_LOGO'] = 'အဆင့္ျမင့္ၿပီး တန္ဖိုုးသင့္ ၀န္ေဆာင္မႈ';
$lang['TEXT_PACKAGE_DETAIL'] = 'ခရီးစဥ္ အေသးစိတ္ၾကည့္ရန္';
$lang['TOUR_PACKAGE_HEADING'] = 'ခရီးစဥ္မ်ား';
?>



