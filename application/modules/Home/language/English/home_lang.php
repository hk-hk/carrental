<?php

defined('BASEPATH') OR exit('No direct script access allowed');



//MultyDays Popup in City

$lang['CAR_PER_DAY_HOURS_HEADING'] = 'Choose hours for each Day';

$lang['CAR_PER_DAY_HOURS_SUBMIT_BTN'] = 'Apply';

//MultyDays Popup in Highway PDDP=PER_DAY_DESTINATION_POPUP

$lang['CAR_PER_DAY_DESTINATION_HEADING'] = 'Choose destination for each Day';

$lang['CAR_PDDP_FROM'] = 'Date';

$lang['CAR_PDDP_TO'] = 'Destination';

$lang['CAR_PDDP_PICKUP_TIME'] = 'Pickup Time';

$lang['CAR_PDDP_DROPOFF_TIME'] = 'Dropoff Time';

$lang['CAR_PER_DAY_DESTINATION_SUBMIT_BTN'] = 'Apply';



$lang['CAR_FILTER_HEADING'] = 'Search for a car hire';

$lang['CAR_FILTER_BY_DAILY'] = 'Daily';

$lang['CAR_FILTER_BY_WEEKLY'] = 'Weekly';

$lang['CAR_FILTER_BY_MONTHLY'] = 'Monthly';

$lang['CAR_FILTER_PICKUP_CITY'] = 'Pick Up City';

$lang['CAR_FILTER_IN_CITY'] = 'In City';

$lang['CAR_FILTER_HIGHWAY'] = 'Highway';

$lang['CAR_FILTER_DESTINATION'] = 'Destinated location';

$lang['CAR_FILTER_PICKUP_DATE_TIME'] = 'Pickup Date & Time';

$lang['CAR_FILTER_DROPOFF_DATE_TIME'] = 'DropOff Date & Time';

$lang['CAR_FILTER_PICKUP_DATE'] = 'Pickup Date';

$lang['CAR_FILTER_NO_OF_WEEK'] = 'Weeks';

$lang['FILTER_TEXT_WEEK'] = ' Week';

$lang['FILTER_TEXT_WEEKS'] = ' Weeks';

$lang['FILTER_TEXT_MONTH'] = ' Month';

$lang['FILTER_TEXT_MONTHS'] = ' Months';

$lang['CAR_FILTER_NO_OF_MONTH'] = 'No of months';

$lang['CAR_FILTER_SEARCH_BTN'] = 'Search';



$lang['NEWEST_CAR_HEADING'] = 'Our Inventories';

$lang['TOUR_PACKAGE_HEADING'] = 'Our Tour Packages';

$lang['TEXT_BOOK_NOW_BTN'] = 'Book Now';

$lang['TEXT_PACKAGE_DETAIL'] = 'Package Details';

$lang['OUR_BRANDS_LOGO'] = 'Big Brands. Massive Savings!';







