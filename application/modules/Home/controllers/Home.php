<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends MY_Controller {

	public function __construct() {
		parent::__construct(); 
                
		$this->data['module'] = 'Home'; // Load Users module
        $this->load->model('Attributes/Attributes', 'attributes'); // Load User modal
        $this->load->model('Attributes/AirportLocations', 'airport_location'); // Load User modal
		$this->load->model('Home_model', 'home'); // Load User modal
                $this->load->model('Articles/Articles', 'articles'); // Load User modal
                //Load Language file based on active language
                $this->session->set_userdata('active_language','mk');
                $this->session->set_userdata('active_language','en');
                $this->lang->load('Home/home', $this->getActiveLanguage());
                
	}
	public function changeLang($lang='mk'){
            if($lang=='mk'){
                $this->session->set_userdata('active_language','mk');
                $this->session->set_userdata('aler_active_language','mk');
            }else{
                $this->session->set_userdata('active_language','en');
                $this->session->set_userdata('aler_active_language','en');
            }
            redirect($_SERVER['HTTP_REFERER']);
            
           // $current_url = $this->session->userdata('return_url');
           // if(in_array($lang,array('mk','en'))){
           //     redirect(site_url($current_url));
           // } else {
           // }
        }
	public function changeCurrancy($currency='mmk'){
        
            $current_url = $this->session->userdata('return_url');
            if(in_array($currency,array('mmk','usd'))){
               $this->session->set_userdata('active_currency',$currency);
               // redirect($_SERVER['HTTP_REFERER']);
            } 
           // else {
            redirect($_SERVER['HTTP_REFERER']);
           // }
        }
        public function setDefaultCountry(){
            $country_code = $this->input->post('country_code' ,true);
            $country_name = $this->input->post('country_name' ,true);
            $this->session->set_userdata('active_country',  strtolower($country_code));
            if(strtolower($country_code)=='mm'){
                $this->session->set_userdata('active_language','mk');
                $this->session->set_userdata('aler_active_language','mk');
            }else{
                $this->session->set_userdata('active_language','en');
                $this->session->set_userdata('aler_active_language','en');
            }
            echo strtolower($country_code);die;
        }

        public function index($lang='')
	{
           // die("dfgdfg");


           // $this->load->module('Android');
           // $this->Android->connect();
            if($this->session->userdata('active_currency')==""){
                $this->session->set_userdata('active_currency',"mmk");
            }
            $this->data['car_attributes'] = $this->home->get_attributes(array(70),TRUE); 
            $this->data['attributes_by_id'] = $this->home->get_attributes(array(3,4,39,42)); 
            $this->data['makers'] = $this->home->get_attributes(array(45));//print_r($this->data['makers']);die;
            $model_id = array();
            foreach ($this->data['makers'] as $key => $value) {
                $model_id[] = $key;
            }
            $this->session->set_userdata('datewiseDestinations','');
            $this->session->set_userdata('datewise',''); 
            $this->data['model'] = $this->home->get_attributes($model_id);
            $this->data['extra_css'] = array('jquery.timepicker.css','select2.min.css','select2.css');
            $this->data['extra_js'] = array('owl.carousel.min.js','jquery.timepicker.js','select2.min.js','select2.js','custom_car_listing.js');
            $list = $this->home->get_page(23);    
            $activeLanguage = $this->session->userdata('active_language');
            if($activeLanguage=='mk'){ 
                $this->data['policies_content'] = $this->home->get_home_content("policies")->content_mya;
            }else{
                $this->data['policies_content'] = $this->home->get_home_content("policies")->content;
            }
            $this->bannerImgCommingTxt();
            $this->data['welcome'] = $this->home->get_home_content("welcome-to-sonic-star");
            $this->data['all_models'] = $this->home->get_all_models();  
            // $this->data['get_newest_cars'] = $this->home->get_newest_cars();
            $this->data['tour_packages'] = $this->articles->get_many_by(array('cat_id'=>48,'status'=>'1'));
            $this->data['client_comment'] = $this->articles->get_many_by("cat_id" , 84);
            //print_r($this->data['get_newest_cars']);die;
            $this->data['view'] = 'Home'; 
            $this->data['data'] = array(1, 3,5,6,7,8,9); 
            
            $this->layout->front($this->data); 
         
	} 
    private function bannerImgCommingTxt(){
        $active_language = $this->session->userdata('active_language');
        if($active_language=='en'){
            if(isset($this->data['attributesByid'][4236]['short_desc'])){ 
                $this->data['comming_soon_text'] = $this->data['attributesByid'][4236]['short_desc'];
            }else{
                $this->data['comming_soon_text'] = 'Comming Soon';
            }
            if(isset($this->data['attributesByid'][4240]['short_desc'])){ 
                $this->data['banner_img'] = $this->data['attributesByid'][4240]['short_desc'];
            }else{
                $this->data['banner_img'] = 'default_banner.jpg';
            }
        }else{
            if(isset($this->data['attributesByid'][4237]['short_desc'])){ 
                $this->data['comming_soon_text'] = $this->data['attributesByid'][4237]['short_desc'];
            }
            else{
                $this->data['comming_soon_text'] = 'Comming Soon';
            }
            if(isset($this->data['attributesByid'][4241]['short_desc'])){ 
                $this->data['banner_img'] = $this->data['attributesByid'][4241]['short_desc'];
            }
            else{
                $this->data['banner_img'] = 'default_banner.jpg';
            }
        }
    }

    public function cities(){
        // echo "hello";
        $list = $this->attributes->get_datatables(70);

        usort($list, function($a,$b) {
            return $a->ordering <=> $b->ordering;
        });
        // print "<pre/>";
        // print_r($list);

        // exit();
        if(isset($_GET['airport']))
            echo json_encode($this->airport_location->get_located());
        else
            echo json_encode($list);
    }


}
