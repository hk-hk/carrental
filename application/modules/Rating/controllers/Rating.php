<?php
/***
 *
 * Created by: CIS
 * This is for authorized user's only.
 * This file contains all Property's related functions.
 *
***/
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends MY_Controller {

  public function __construct() {
    parent::__construct(); 
    $this->data['module'] = 'Rating';
    $this->load->model('Ratings', 'ratings');
    $this->load->model('Users/User', 'user');
    $this->load->model('Car_manager/Car_manager', 'car_manager');
  }
  public function index()
  {

  }
  /* list of rating record */
  public function listRating() {  
  	$this->isAuthorized();
  	$this->checkPermission('rating_list');
    $this->data['suppliers'] = $this->user->get_many_by("user_role" , "supplier");
    $this->data['customers'] = $this->user->get_many_by("user_role" , "customer");
    $this->data['order_status'] = $this->uri->segment(4)?$this->uri->segment(4):"";
    $this->data['view'] = 'listRating'; 
    $this->layout->admin($this->data);
  }
  /* list of rating record */


  /* ajax call for rating table */

  public function ajax_rating_list() //list of all terminals for ajax datatables
  {
      $this->isAuthorized();
      $this->checkPermission('rating_list');
      $this->is_ajax(); 
      $list = $this->ratings->get_datatables();

     
      $data = array();
      $no = $_POST['start'];
      $makers = $this->ratings->get_attributes(array(45));//
      $model = $this->ratings->get_attributes(array_keys($makers));
      foreach ($list as $term) {
          $no++;
          $row = array();
          $row[] = $no;
          $row[] = $term->user_fname." ".$term->user_lname;
          $row[] = $makers[$term->maker]['title']." - ".$model[$term->model]['title'];
          $row[] = $term->rating;
          $row[] = $term->comment;
          $row[] = $term->rating_by;
          $row[] = date("d M, Y", strtotime($term->created_date));
          $isActive = ($term->status == 'unpublish')? 'btn-danger' : 'btn-success';

          $btntxt = $term->status;
          $row[] = '<span><a title="Change Status" onclick="return(confirm(\'Are you Sure ?\'))" href="'.site_url(CRM_VAR).'/rating/change_status/'.$term->rating_id.'/'.$term->car_id.'" class="btn '.$isActive.' btn-xs change-status">'.$btntxt.'</a></span>';

          $data[] = $row;
      }

      $output = array(
                      "draw" => $_POST['draw'],
                      "recordsTotal" => $this->ratings->count_all(),
                      "recordsFiltered" => $this->ratings->count_filtered(),
                      "data" => $data,
              );
      //output to json format
      echo json_encode($output);
  }

  public function change_status($rating_id,$car_id)
  {
    $this->checkPermission('rating_status_change');
    $ratinginfo = $this->ratings->get_by("rating_id", $rating_id);
    //print_r($ratinginfo);die;
    if(empty($ratinginfo)){
      $this->set_msg_flash(['msg'=>'This is not a valid Rating record','typ'=>2]);
      redirect(CRM_VAR.'/rating/list');
    }
    $current_status = $ratinginfo->status;
    $new_status = $ratinginfo->status == 'publish'?'unpublish':'publish';
    if($this->ratings->update($rating_id,array("status" => $new_status))){
      $new_rating = $this->ratings->get_rating($car_id);
      $this->car_manager->update($car_id,array("ratings" => $new_rating->rating));
      $this->set_msg_flash(['msg'=>'Rating Updated!!','typ'=>1]);
      redirect(CRM_VAR.'/rating/list');
    }else { 
      $this->set_msg_flash(['msg'=>'Error in changing status','typ'=>3]);
      redirect(CRM_VAR.'/rating/list');
    }
  }

}