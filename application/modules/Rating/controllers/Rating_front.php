<?php
/***
 *
 * Created by: CIS
 * This is for authorized user's only.
 * This file contains all Property's related functions.
 *
***/
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating_front extends MY_Controller {

  public function __construct() {
    parent::__construct(); 
    $this->data['module'] = 'Rating';
    $this->load->model('Ratings', 'ratings');
    $this->load->model('Users/User', 'user');
    $this->load->model('Car_manager/Car_manager', 'car_manager');
    $this->load->model('Order/Orders', 'orders');
    // if(!$this->session->userdata("site_user_logged")){
    //   redirect(site_url().'login');
    // }
  }
  public function index()
  {

  }
  
  public function rating()
  {
    $this->data['orders'] = $this->ratings->get_my_order();
    $this->load->view('front/rating',$this->data);
    if($_SERVER['REQUEST_METHOD'] == 'POST'){

      $validate_arr = array(
          array(
                  'field' => 'order_id',
                  'label' => 'Order ID',
                  'rules' => 'trim|required'
          ),
          array(
              'field' => 'rating',
              'label' => 'Rating',
              'rules' => 'trim|required|numeric'
          ),
          array(
              'field' => 'comment',
              'label' => 'Comment',
              'rules' => 'trim|required'
          )
      );
      $this->form_validation->set_rules($validate_arr);
      if ($this->form_validation->run() == FALSE)
      {
        $this->set_msg_flash([
            'msg'=>'Invalid Form Submittion!!',
            'typ'=>3
        ]);
        redirect(site_url().'rating');
      }
      else{
        $order_id = $this->input->post('order_id');
        $rating = $this->input->post('rating');
        $comment = $this->input->post("comment");
        $res = $this->do_rating($order_id, $rating, $comment);
        if($res){
          $this->set_msg_flash(['msg'=>'Update Successfully.','typ'=>1]);
          redirect(site_url().'rating');
        }
      }
    }
  }

  public function do_rating($order_id, $rating, $comment)
  {
    $user_id = $this->session->userdata("site_user_id");
    $user_role = $this->session->userdata("site_user_role");
    //for car id
    $orderInfo = $this->orders->get_order($order_id);
    $car_id = $orderInfo->car_id;

    //array for inserting rating
    $rating_array = array(
      'order_id' => $order_id, 
      'car_id' => $car_id, 
      'user_id' => $user_id, 
      'rating' => $rating, 
      'comment' => $comment, 
      'rating_by' => $user_role, 
    );
    if($this->ratings->insert($rating_array)){
      return true;
    }
    else{
      return false;
    }
  }

  public function ajax_rating($car_id){
    $ratings=$this->ratings->get_rating_by_user($car_id);
    echo json_encode($ratings);
    // print "<pre/>";
    // print_r($ratings);
  }

}