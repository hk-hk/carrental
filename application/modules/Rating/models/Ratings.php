<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ratings extends MY_Model {

	public $_table = 'cr_car_rating'; // database table
	public $primary_key = 'rating_id'; // primary key
	public $before_create = array( 'timestamps' );
	public $column_order = array(null,"user_fname",null,"rating",null,"created_date",null); //set column field database for datatable orderable	
	public $column_search = array('status','user_id','supplier_id'); //set column field database for datatable searchable 	
	public $order = array('user_fname' => 'asc'); // default order
	
	public $config = array(         // Set validation for add/edit form.
    
	
	);

	
  protected function timestamps($terminal)
    {
        $terminal['modify_date'] = date('Y-m-d H:i:s');
        return $terminal;
    }
    
  private function _get_datatables_query() // dynamic query for search terminal
    {
        
        if($this->input->post('user_id'))
        {
            $this->db->where('cr_car_rating.user_id', $this->input->post('user_id'));

        }
        if($this->input->post('supplier_id'))
        {
            $this->db->where('cr_car_rating.user_id', $this->input->post('supplier_id'));

        }
        if($this->input->post('status'))
        {
            $this->db->where('cr_car_rating.status', $this->input->post('status'));
        }
        
        $this->db->join('cr_users', 'cr_car_rating.user_id = cr_users.id', 'left');
        $this->db->join('cr_manage_cars', 'cr_car_rating.car_id = cr_manage_cars.id', 'left');
        $this->db->select('cr_car_rating.*, cr_users.user_fname, cr_users.user_lname, cr_manage_cars.maker, cr_manage_cars.model'); 
        $this->db->from($this->_table);
        
        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();      
        return $query->result();
    }

    function count_filtered() // Get total row count of search query
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_my_order() // Get total row count of search query
    {
        $query = "SELECT * from cr_orders WHERE user_id = ".$this->session->userdata("site_user_id");
        $res = $this->db->query($query);
        return $res->result_object();
    }

    function get_order_ratings($car_id) // Get total row count of search query
    {
        $query = "SELECT * from cr_car_rating WHERE order_id = ".$car_id;
        $res = $this->db->query($query);
        return $res->result_object();
    }

    function get_rating($car_id){
        $query = "SELECT AVG( rating ) AS rating FROM cr_car_rating WHERE  car_id = ".$car_id." AND status = 'publish' AND rating_by='Customer'";
        $res = $this->db->query($query);
        return $res->row_object();
    }

    public function check_for_rating($order_id)
    {
        $query = "SELECT * FROM `cr_car_rating` WHERE order_id = ".$order_id." AND user_id = ".$this->session->userdata("crm_user_id");
        $res = $this->db->query($query);
        return $res->num_rows();
    }

    public function num_of_reviews($car_id)
    {
        $query = "SELECT * FROM `cr_car_rating` WHERE car_id = ".$car_id;
        $res = $this->db->query($query);
        return $res->num_rows();
    }

    function get_rating_by_user($car_id){
        $query ="SELECT cr_car_rating.*, cr_users.user_lname FROM cr_car_rating INNER JOIN cr_users ON cr_car_rating.user_id = cr_users.id WHERE cr_car_rating.car_id=".$car_id;
        // $query = "SELECT rating.*,`cr_users`.user_lastname FROM cr_car_rating, cr_users WHERE  car_id = ".$car_id." AND status = 'publish' AND rating_by='Customer'";
        $res = $this->db->query($query);
        return $res->result();
    }

}
