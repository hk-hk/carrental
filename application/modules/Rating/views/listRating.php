<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Orders</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    
          <div class="box-body">
          <h3 class="box-title">Filter</h3>
          <form id="form-filter" class="form-horizontal">
            <div class="row">
              <div class="form-group col-md-6">
                  <label for="LastName" class="col-sm-4 control-label">Supplier</label>
                  <div class="col-sm-8">
                      <select id="supplier_id" name="supplier_id" class="form-control">
                        <option value="0">Select Option</option>
                        <?php foreach ($suppliers as $k => $supplier) {if($supplier->user_active == 'YES'){
                        ?>
                        <option value="<?php echo $supplier->id; ?>"><?php echo $supplier->user_fname." ".$supplier->user_lname; ?></option>
                        <?php 
                        }} ?>
                       
                      </select>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                  <label for="LastName" class="col-sm-4 control-label">Customer</label>
                  <div class="col-sm-8">
                      <select id="user_id" name="user_id" class="form-control">
                        <option value="0">Select Option</option>
                        <?php foreach ($customers as $k => $customer) {if($customer->user_active == 'YES'){
                        ?>
                        <option value="<?php echo $customer->id; ?>"><?php echo $customer->user_fname." ".$customer->user_lname; ?></option>
                        <?php 
                        }} ?>
                       
                      </select>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                  <label for="LastName" class="col-sm-4 control-label">Order Status</label>
                  <div class="col-sm-8">
                      <select name="status" id="status" class="form-control">
                        <option value="" <?php if($order_status == 0){echo "selected";} ?>>Select Option</option>
                        <option value="publish"  <?php if($order_status == "publish"){echo "selected";} ?>>Publish</option>
                        <option value="unpublish"  <?php if($order_status == "unpublish"){echo "selected";} ?>>Unpublish</option>
                      </select>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                  <label for="LastName" class="col-sm-1 control-label"></label>
                  <div class="col-sm-11">
                      <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                      <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                  </div>
              </div>
            </div>
          </form>
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <!-- Username(First+Last Name) , Car name(Model+Maker), Rating, Comment, Feedback By, created_date, Status  -->
                <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Car name</th>
                    <th>Rating</th>
                    <th>Comment</th>
                    <th>Feedback As</th>
                    <th>Created date</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Car name</th>
                    <th>Rating</th>
                    <th>Comment</th>
                    <th>Feedback As</th>
                    <th>Created date</th>
                    <th>Status</th>
                </tr>
            </tfoot>
        </table>
        </div>
      </div>
          <!-- /.box-body -->
         <!--  <div class="box-footer">
            <button type="submit" class="btn btn-info">Add User</button>
          </div> -->
          <!-- /.box-footer -->
         
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
 
var table;
 
$(document).ready(function() {
 $('.datepicker').datepicker({
         autoclose: true
       });
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/rating/ajax_rating_list')?>",
            "type": "POST",
            "data": function (data) {		
                data.supplier_id = $('#supplier_id').val();
                data.user_id = $('#user_id').val();
                data.status = $('#status').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,7 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    }); 
 
});
 
</script>