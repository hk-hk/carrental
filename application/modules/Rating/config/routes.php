<?php

$route[CRM_VAR.'/rating/list']     = 'Rating/Rating/listRating'; 
$route[CRM_VAR.'/rating/ajax_rating_list'] = 'Rating/Rating/ajax_rating_list'; 
$route[CRM_VAR.'/rating/change_status/(:any)/(:any)']     = 'Rating/Rating/change_status/$1/$2';
$route[CRM_VAR.'/rating/change_status/(:any)']     = 'Rating/Rating/change_status/$1';

$route['rating']     = 'Rating/Rating_front/rating'; 
$route['rating/(:any)']     = 'Rating/Rating_front/ajax_rating/$1'; 