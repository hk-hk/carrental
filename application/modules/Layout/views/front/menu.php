<section class="header">
   <nav class="navbar" id="navbar-main" role="navigation">
      <div class="container">
        <div class="row">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <div class="logo_sc">
        	<a href="/"><img src="<?php echo base_url('public/front/')?>images/logo.png" alt="Sonic Stars" /></a>
         </div>
        </div>

        <div class="pull-right header-right">
        <?php $menuSegments = array(
            '../'=>getLang('MENU_HOME'),
            ''=>getLang('MENU_CAR_RENTAL'),
            '../flight/public'=>getLang('MENU_AIR_TICKET'),
            
            // 'about-us'=>getLang('MENU_ABOUT_US'),
            'attractive-places'=>getLang('MENU_ATTRACTIONS'),
            // 'faq'=>getLang('MENU_FAQ'),
            // 'contact-us'=>getLang('MENU_CONTACT_US')
            );?>
        <div class="collapse navbar-collapse navbar-box" id="navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <?php foreach ($menuSegments as $k=>$menu){
              if($this->uri->segment(1)==$k){ ?>
                 <li class="active"><a href="<?php echo site_url($k); ?>"><?php echo $menu;?></a></li>
              <?php }else{ ?>
                 <li><a href="<?php echo site_url($k); ?>"><?php echo $menu;?></a></li>
            <?php }
              } ?> 

              
              <!-- YOU can add another menu after this line-->	 
              <?php if(!$this->session->userdata("crm_user_logged")){ ?>
                    <li><a href="<?php echo site_url("login"); ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> <?php echo getLang('MENU_LOGIN');?></a></li>
                <?php }else{
                    $profileUrl = ($this->session->userdata("cr_user_role")=='customer')?'/myaccount/orders':CRM_VAR.'/dashboard';
                    $welcomeName = substr($this->session->userdata("crm_user_lname"),0,7);
                    $logout = ($this->session->userdata("cr_user_role")=='customer')?'/logout':CRM_VAR.'/logout';
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hi, <?php echo $welcomeName;?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url($profileUrl); ?>"><i class="fa fa-user" aria-hidden="true"></i> <?php echo getLang('MENU_ACCOUNT');?></a></li>
                            <li><a href="<?php echo site_url($logout); ?>"><i class="fa fa-lock" aria-hidden="true"></i> <?php echo 'Logout';//getLang('MENU_ACCOUNT');?></a></li>
                        </ul>
                    </li>
                <?php } ?>
                    <li class="dropdown">

                        <?php if($active_currency=='usd'){ ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">US$ <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url('changecurrency/mmk')?>">MMK</a></li>
                        </ul>
                        <?php }else{ ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MMK <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url('changecurrency/usd')?>">US$</a></li>
                        </ul>
                        <?php } ?>
                      </li>
                      <li class="dropdown">
                       <?php $activeLanguage = $this->session->userdata('active_language');?>
                        <?php if($activeLanguage=='en'){ ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <img class="flag" style="width: 25px;" src="https://lipis.github.io/flag-icon-css/flags/4x3/um.svg" alt="US"> US <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url('change-lang/mk')?>">
                           <img class="flag" style="width: 25px;" src="https://lipis.github.io/flag-icon-css/flags/4x3/mm.svg" alt="Myanmar Flag"> MM
                          </a></li>
                        </ul>
                        <?php }else{ ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <img class="flag" style="width: 25px;" src="https://lipis.github.io/flag-icon-css/flags/4x3/mm.svg" alt="Myanmar Flag"> MM <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo site_url('change-lang/en')?>">
                            <img class="flag" style="width: 25px;" src="https://lipis.github.io/flag-icon-css/flags/4x3/um.svg" alt="US"> US</a></li>
                        </ul>
                        <?php } ?>
                    </li>
                    <li class="mobile-icon"><a href="javascript:void(0);"><i class="fa fa-phone" aria-hidden="true"></i></a>
                    	<div class="show-contact">
                        	<p>Have any Question? Call us :</p>
                            <?php 
                            if($this->session->userdata('active_language')=='en'){ 
                              $phone = (isset($attributesByid[4225]['short_desc'])?$attributesByid[4225]['short_desc']:'');
                            }else{
                              $phone = (isset($attributesByid[54]['short_desc'])?$attributesByid[54]['short_desc']:'');
                            }
                              ?>
                            <span><i class="fa fa-volume-control-phone" aria-hidden="true"></i> <?php echo $phone;?></span>
                        </div>
                    </li>

            	
          </ul>
        </div>
        <div class="header-number">
        <a href="javascript:void(0);">
        <i class="fa fa-phone" aria-hidden="true"></i>
        </a>
        
        <div class="show-contact">
        <p><?php echo getLang('MENU_PHONE'); ?></p>
        <span>
        <i class="fa fa-volume-control-phone" aria-hidden="true"></i>
        <?php echo $phone; ?>
        </span>
        </div>
        
        </div>
        
        </div>
        
        </div>
        <div class="clearfix"></div>
      </div><!-- /.container -->
    </nav>
</section><!-- / Section / -->
