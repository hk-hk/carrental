<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://www.sonicstartravel.com/public/front/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Sonic Star travel " />
<!-- <meta name="description" content="Sonic Star travel: <?php echo (isset($page_title)?$page_title.'!!':'')?> Car rental, you can choose many types of budget to luxury cars from different suppliers with very competitive prices." /> -->

<!-- <title>Sonic Star Travel Services Provider</title> -->
<meta name="Description" content="Sonic Star travel:  Air Tickets, Air Port Transfer , Car rental, Myanmar Tour Packages, we provide the best travel services with very competitive prices.">
<meta name="Keywords" content="Myanmar, Myanmar Air Ticket, Myanmar Flight, Myanmar Tour, Myanmar Airport Transfer, Myanmar Car Rental, Shwe, Myanmar Travel, Yangon Airport, Yangon Flight, Mandalay Air Ticket, Mandalay Flight">
<meta name="generator" content="Sonic Star travel car Rental online booking." />

<title> :: Sonic Star :: Best Myanmar Travel Services Provider :: <?php echo (isset($page_title)?$page_title:'')?></title>

<?php $activeLanguage = $this->session->userdata('active_language');
if($activeLanguage=='mk'){
    $jsLangFile = 'constant_mm.js';
}else{
    $jsLangFile = 'constant_en.js';
}
?>
<script type="text/javascript">
  var ISFROMMAIN="";
  var SELECTED_CITY="";
  <?php if (!empty($_GET['q'])): ?>
      ISFROMMAIN="airport";
        // $('.airport_active').get(0).click();
  <?php endif ?>
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet"> 
<!-- CSS Plugins File -->
<link href="<?php echo base_url('public/front/')?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url('public/front/')?>css/jquery-ui.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo base_url('public/front/')?>css/owl.carousel.min.css" />
<link rel="stylesheet" href="<?php echo base_url('public/front/')?>css/animate.css">
<!-- Custom CSS Files -->
<?php if(isset($extra_css) && is_array($extra_css) && count($extra_css)>0){
     foreach ($extra_css as $css){ 
        if (filter_var($css, FILTER_VALIDATE_URL)) {
            $cssUrl = $css;
        } else {
            $cssUrl = base_url('public/front/css/'.$css);
        }?>
        <link rel="stylesheet" href="<?php echo $cssUrl;?>">
     <?php }
} ?>  
<link href="<?php echo base_url('public/front/')?>css/style.css" rel="stylesheet" type="text/css"  />
<link href="<?php echo base_url('public/front/')?>css/custom.css" rel="stylesheet" type="text/css"  />
<link href="<?php echo base_url('public/front/')?>css/responsive.css" rel="stylesheet" type="text/css"  />
<!--[if IE]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?php echo base_url('public/front/')?>js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('public/front/')?>js/<?php echo $jsLangFile; ?>" type="text/javascript"></script>
<script>
var SITE_URL = '<?php echo site_url();?>';
var BASE_URL = '<?php echo base_url();?>';

</script>
<script>(function(d,t,u,s,e){e=d.getElementsByTagName(t)[0];s=d.createElement(t);s.src=u;s.async=1;e.parentNode.insertBefore(s,e);})(document,'script','//www.sonicstartravel.com/livechat/php/app.php?widget-init.js');</script>
</head>

<body class="<?php echo ($activeLanguage=='mk')?'myanmar-lang':''?>">