<!-- Footer Section -->

<section class="footer">

        <div class="container">

            <div class="row">

            <?php if(FALSE){?>

                <div class="col-sm-4 center-footer-col">

                    <a href="javascript:void(0);"> <h3><?php echo getLang('FOOTER_SECTION_FIRST'); ?></h3></a>

                    <?php echo getLang('FOOTER_ABOUT_US_INFO'); ?>

                </div>

                <?php } ?>

                <div class="col-sm-6 center-footer-col  col-sm-offset-1">

                    <h3><?php echo getLang('FOOTER_SECTION_SECOND'); ?></h3>

                    <ul>
                        <li><a href="<?php echo site_url('');?>"><?php echo getLang('MENU_FOOTER_WELCOME'); ?></a></li>
                        <li><a href="<?php echo site_url('faq');?>"><?php echo getLang('MENU_FAQ'); ?></a></li>
                        <li><a href="<?php echo site_url('contact-us');?>"><?php echo getLang('MENU_CONTACT_US'); ?></a></li>

                        <!--<li><a href="<?php //echo site_url('cpage/for-client');?>"><?php //echo getLang('MENU_FOOTER_FOR_CLIENT'); ?></a></li>-->

                        <li><a href="<?php echo site_url('cpage/privacy-policy');?>"><?php echo getLang('MENU_FOOTER_PRIVACY_POLICY'); ?></a></li>
                        <li><a href="<?php echo site_url('cpage/terms-and-conditions');?>"><?php echo getLang('MENU_FOOTER_TERMS_AND_CONDITIONS'); ?></a></li>

                        <li><a href="<?php echo site_url('about-us');?>"><?php echo getLang('MENU_FOOTER_ABOUT_US'); ?></a></li>

                        <li><a href="<?php //echo site_url('cpage/candidates');?>"><?php // echo getLang('MENU_FOOTER_CANDIDATES'); ?></a></li>

                        <li><a href="<?php echo site_url('register/agent');?>"><?php echo getLang('MENU_FOOTER_AGENT_REGISTER'); ?></a></li>

                        <li><a href="<?php echo site_url('register/supplier');?>"><?php echo getLang('MENU_FOOTER_SUPPLIER_REGISTER'); ?></a></li>

                    </ul>

                </div>

                

                <div class="col-sm-4 center-footer-col col-sm-offset-1"> 

                    <a href="javascript:void(0);"><h3><?php echo getLang('FOOTER_SECTION_THIRD');?></h3></a>  

                    <ul class="footer-info"><?php //print_r($attributesByid);die;?>

                    	<li><i class="fa fa-map-marker" aria-hidden="true"></i>

                        <?php echo (isset($attributesByid[52]['short_desc'])?$attributesByid[52]['short_desc']:'') ?></li>

                        <?php if($this->session->userdata('active_language')=='en'){ ?>

                        <li><i class="fa fa-phone" aria-hidden="true"></i>

                        <a href="tel:<?php echo (isset($attributesByid[4225]['short_desc'])?$attributesByid[4225]['short_desc']:'') ?>"><?php echo (isset($attributesByid[4225]['short_desc'])?$attributesByid[4225]['short_desc']:'') ?></a>

                        </li>

                        <?php }else{ ?>

                        <li><i class="fa fa-phone" aria-hidden="true"></i>

                        <a href="tel:<?php echo (isset($attributesByid[54]['short_desc'])?$attributesByid[54]['short_desc']:'') ?>"><?php echo (isset($attributesByid[54]['short_desc'])?$attributesByid[54]['short_desc']:'') ?></a>

                        </li>

                        <?php } ?>

                        

                        <li><i class="fa fa-envelope" aria-hidden="true"></i><?php echo getLang('TEXT_EMAIL');?>: 

                        <a href="mailto:<?php echo (isset($attributesByid[53]['short_desc'])?$attributesByid[53]['short_desc']:'') ?>">

                        <?php echo (isset($attributesByid[53]['short_desc'])?$attributesByid[53]['short_desc']:'') ?></a></li>

                     </ul>

                </div>



            </div>

        </div>

</section><!-- Footer Section -->



<section class="copyright-section">

    <div class="container">

    <div class="row">

        <?php if(FALSE){?>

        <div class="col-sm-4">

            <ul class="term-links">

                    <li><a href="#"><?php echo getlang('MENU_FOOTER_PRIVACY');?></a></li>

                <li><a href="#"><?php echo getlang('MENU_FOOTER_TERMS');?></a></li>

            </ul>

        </div>

        <?php } ?>

        <div class="col-sm-4">

            <p><?php echo getLang('FOOTER_COPYRIGHTS');?></p>

        </div>

        <div class="col-sm-4">

            <ul class="social-links">

                <li><a href="<?php echo $attributesByParent[51][1]['short_desc']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                <li><a href="<?php echo $attributesByParent[51][2]['short_desc']; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

                <li><a href="<?php echo $attributesByParent[51][5]['short_desc']; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>

                <li><a href="<?php echo $attributesByParent[51][4]['short_desc']; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

            </ul>

        </div>

        </div>

    </div>

</section>



<!-- JS Main Plugin files -->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <p class="modal-title"><h4>Avg Rate: <span class="review-rating-box" id="avg_rate"></span></h4>Rental Company: <span id="supplier_name"></span>, Model: <span id="car_model"></span></p>

      </div>
      <div class="modal-body" id="review-body">
        <!-- <a href="" title="" class="text-center">Load More</a> -->
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script src="<?php echo base_url('public/front/')?>js/jquery-3.1.1.min.js" type="text/javascript"></script>

<script src="<?php echo base_url('public/front/')?>js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php echo base_url('public/front/')?>js/jquery-ui.min.js"></script>

<?php if(isset($extra_js) && is_array($extra_js) && count($extra_js)>0){

     foreach ($extra_js as $js){ 

        if (filter_var($js, FILTER_VALIDATE_URL)) {

            $jsUrl = $js;

        } else {

            $jsUrl = base_url('public/front/js/'.$js);

        }?>

         <script src="<?php echo $jsUrl?>" type="text/javascript"></script>

     <?php }

} ?>    

<script src="<?php echo base_url('public/front/')?>js/custom.js"></script>

<script type="text/javascript">

    function get_destination(city){ 
        // console.log(city);
        if(city != ''){

            var destination = <?php if(isset($daily['destination'])){echo $daily['destination']; }else{echo '112';} ?>;

            $.ajax({

              type: 'POST',

              url: "<?php echo site_url('get_destination')?>",

              data: {city:city},

              dataType: "JSON",

              success: function(resultData) { 

                var html = '';

                $.each(resultData, function(i,v){

                    html = html+'<option value="'+v.id+'"';

                    if(v.id == destination) {

                        html = html+' selected ';

                    }

                    html = html+'>'+v.title+'</option>';

                });

                $("#destination").html(html);

              }

            });

        }

        else{

            alert('Select city first !');

        }

    }

    function empty_destination(){

        $("#destination").html('');

    }

    if($("#destination").length>0){

        get_destination($("#car_city_daily").val());

    }

    $(".city_select").on("change", function(){

        var city = $(".city_select").val();

        get_destination(city);

    });

    

</script>

<?php if($this->active_country==''){ ?>

<script> 

     getActiveCountry();  

</script>

<?php } ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    
// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='https://embed.tawk.to/5b333c29eba8cd3125e336cb/default';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();
</script>
<!--End of Tawk.to Script-->
</body>

</html>