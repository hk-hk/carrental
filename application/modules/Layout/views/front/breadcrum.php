<?php if(isset($breadcrumbs) && count($breadcrumbs)>0){ ?>
<section class="login-header common-header">
	<div class="container">
    	<div class="row">
        <?php if(false){ ?>
            <h2 class="black section-title common-title"><?php echo $page_title?></h2>
        <?php } ?>
            <ul>
                <?php foreach ($breadcrumbs as $path){ ?>
            	<li><a href="<?php echo $path['url']; ?>"><?php echo $path['title']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>
<?php } ?>
