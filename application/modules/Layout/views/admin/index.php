<!-- header-->
<?php  $this->load->view('header'); ?>
<!-- header-->
<!-- Left side column. contains the logo and sidebar -->
<?php  $this->load->view('sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php  $this->load->view('breadcrum'); ?>
  <!-- Main content -->
  <section class="content">
  <?php if($this->session->flashdata('typ')): 
  switch ($this->session->flashdata('typ')) {
    case 1:
      $put = 'alert-success';
      break;
    case 2:
      $put = 'alert-warning';
      break;
    case 3:
      $put = 'alert-danger';
      break;
  }
  ?>
    <div class="alert <?php echo $put; ?> alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    <?php echo $this->session->flashdata('msg'); ?>
  </div>
  <?php endif;?>
  
  <!-- <div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    Success alert preview. This alert is dismissable.
  </div>
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Alert!</h4>
    Success alert preview. This alert is dismissable.
  </div> -->
  <?php $this->load->view($module.'/'.$view); ?>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- footer-->
<?php  $this->load->view('footer'); ?>
