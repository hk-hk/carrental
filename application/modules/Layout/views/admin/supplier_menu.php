<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Manage Car</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/car_list"><i class="fa fa-circle-o"></i>All Car</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/add_car"><i class="fa fa-circle-o"></i> Add Car</a></li>
    </ul>
</li>
<!-- article menu by CIS 1443 -->
<li class="treeview">
    <a href="<?php echo site_url(CRM_VAR);?>/order/list">
      <i class="fa fa-user"></i>
      <span>Manage Orders</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
</li>
<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Commission Reports</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/reports"><i class="fa fa-circle-o"></i>Commission Reports</a></li>
      <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Pending Orders</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Cancle Orders</a></li> -->
    </ul>
</li>
<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Booking Calendar</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/booking-calendar"><i class="fa fa-circle-o"></i>Booking Calendar</a></li>
    </ul>
</li>
  
