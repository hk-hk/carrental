<!-- article menu by CIS 1443 -->
<li class="treeview">
    <a href="<?php echo site_url(CRM_VAR);?>/order/list">
      <i class="fa fa-user"></i>
      <span>Manage Orders</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
</li>
<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Commission Reports</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/reports"><i class="fa fa-circle-o"></i>Commission Reports</a></li>
      <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Pending Orders</a></li>
      <li><a href="#"><i class="fa fa-circle-o"></i> Cancle Orders</a></li> -->
    </ul>
</li>
   