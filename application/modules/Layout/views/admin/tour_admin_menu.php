    <!-- article menu by CIS 1443 -->
<li class="treeview">
      <a href="javascript:void(0);">
        <i class="fa fa-user"></i>
        <span>Manage Articles</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo site_url(CRM_VAR);?>/article/list"><i class="fa fa-circle-o"></i>All Articles</a></li>
        <li><a href="<?php echo site_url(CRM_VAR);?>/article/add"><i class="fa fa-circle-o"></i> Add Article</a></li>
      </ul>
</li>