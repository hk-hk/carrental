<?php $role =  $this->session->userdata('cr_user_role'); // Get user role of current logged in user ?> 
<?php $profileImg = ($profile_image!='')?'uploads/user/'.$profile_image:'public/dist/img/1484856365_human.png'; ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo base_url($profileImg);?>" class="img-circle" alt="User Image" style="height: 45px;">
        </div>
        <div class="pull-left info">
          <p><?php echo $admin_fname; ?> <?php echo $admin_lname; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online<?php //echo $role;?></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        <?php if ($role!='tour_admin'): ?>
          
        <li class="active">
          <a href="<?php echo base_url(CRM_VAR);?>/dashboard" >
            <i class="fa fa-dashboard"></i> <span>Dashboard<?php //echo $role?></span>
          </a>
        </li>
        <?php endif ?>
        <?php if($role=='admin'){ ?>
            <?php  $this->load->view('admin_menu'); ?>
        <?php } elseif($role=='agent'){ ?>
            <?php $this->load->view('agent_menu'); ?>       
        <?php }elseif($role=='customer'){ ?>
            <?php $this->load->view('agent_menu'); ?>       
        <?php } elseif($role=='supplier'){ ?>
            <?php $this->load->view('supplier_menu'); ?>
        <?php } elseif($role=='service_provider'){ ?>
            <?php $this->load->view('service_provider_menu'); ?>
		    <?php } elseif($role=='finance_admin') {?>
            <?php $this->load->view('finance_admin_menu'); ?>
        <?php } elseif($role=='finance') {?>
            <?php $this->load->view('finance_admin_menu'); ?>
        <?php } elseif($role=='tour_admin') {?>
            <?php $this->load->view('tour_admin_menu'); ?>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
