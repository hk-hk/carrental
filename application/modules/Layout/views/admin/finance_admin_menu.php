<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Reports</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/order_report"><i class="fa fa-circle-o"></i>Order Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/maintenance_report"><i class="fa fa-circle-o"></i>Maintenance Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/fuel_report"><i class="fa fa-circle-o"></i>Fuel Expense Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/driver_bonus_report"><i class="fa fa-circle-o"></i>Driver Bonus Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/financial_report"><i class="fa fa-circle-o"></i>Financial Report</a></li>
    </ul>
</li>
 <li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Orders</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/order/list"><i class="fa fa-circle-o"></i>Orders</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/order/list/Pending"><i class="fa fa-circle-o"></i> Pending Orders</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/order/list/Cancelled"><i class="fa fa-circle-o"></i> Cancle Orders</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-list"></i>
      <span>Manage Attributes</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/attributes"><i class="fa fa-circle-o"></i>All Attributes</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/add_attr"><i class="fa fa-circle-o"></i> Add Attribute</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/car_models"><i class="fa fa-circle-o"></i> Manage Car Models</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/fuel-prices"><i class="fa fa-circle-o"></i> Manage Fuel & Toll Prices</a></li>
    </ul>
</li> 

<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Reports</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/order_report"><i class="fa fa-circle-o"></i>Order Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/maintenance_report"><i class="fa fa-circle-o"></i>Maintenance Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/fuel_report"><i class="fa fa-circle-o"></i>Fuel Expense Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/driver_bonus_report"><i class="fa fa-circle-o"></i>Driver Bonus Report</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/financial_report"><i class="fa fa-circle-o"></i>Financial Report</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="javascript:void(0);">
      <i class="fa fa-user"></i>
      <span>Finance</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url(CRM_VAR);?>/banks"><i class="fa fa-circle-o"></i>Banks</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/accounts"><i class="fa fa-circle-o"></i>Accounts</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/transfer"><i class="fa fa-circle-o"></i>Transfer</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/inout"><i class="fa fa-circle-o"></i>Income / Expense</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/car_maintenance"><i class="fa fa-circle-o"></i>Car Maintenance</a></li>
      <li><a href="<?php echo site_url(CRM_VAR);?>/oil_slip"><i class="fa fa-circle-o"></i> Fuel Expense</a></li>
      
    </ul>
</li>
        <!-- article menu by CIS 1443 -->
      <?php if(FALSE){?>
      <li class="treeview">
          <a href="javascript:void(0);">
            <i class="fa fa-user"></i>
            <span>Manage Contacts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url(CRM_VAR);?>/terminal/list"><i class="fa fa-circle-o"></i>All Contacts</a></li>
            <li><a href="<?php echo site_url(CRM_VAR);?>/terminal/add"><i class="fa fa-circle-o"></i> Add Contact</a></li>
          </ul>
        </li>	
       <li class="treeview">
          <a href="javascript:void(0);">
            <i class="fa fa-user"></i>
            <span>Manage Properties</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url(CRM_VAR);?>/property/list"><i class="fa fa-circle-o"></i>All Properties</a></li>
            <li><a href="<?php echo site_url(CRM_VAR);?>/property/add"><i class="fa fa-circle-o"></i> Add Property</a></li>
          </ul>
        </li>
      <?php }
