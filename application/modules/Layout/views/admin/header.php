<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo SITE_NAME;?> | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('public/bootstrap/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('public/dist/css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('public/dist/css/skins/_all-skins.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('public/plugins/iCheck/flat/blue.css');?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('public/plugins/morris/morris.css');?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('public/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('public/plugins/datepicker/datepicker3.css');?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('public/plugins/daterangepicker/daterangepicker.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('public/plugins/bootstrap-select/bootstrap-select.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('public/plugins/timepicker/bootstrap-timepicker.min.css');?>">
  <!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="<?php echo base_url('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/plugins/datatables/dataTables.bootstrap.css');?>">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url('public/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
  <script src="<?php echo base_url('public/plugins/bootstrap-select/bootstrap-select.js');?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script src="<?php echo base_url('public/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('public/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>public/waitMe.min.css">
  <script type="text/javascript">
    var BASE_URL="<?= site_url() ?>";
    var SITE_URL="<?= site_url(CRM_VAR) ?>";
    
  </script>
  <script src="<?php echo base_url('public/front/js/financial.js');?>"></script>
  <style>
    span.my-error-class {
        color: #8B0000;
    }
     #lightbox .modal-content {
    display: inline-block;
    text-align: center;   
}

#lightbox .close {
    opacity: 1;
    color: rgb(255, 255, 255);
    background-color: rgb(25, 25, 25);
    padding: 5px 8px;
    border-radius: 30px;
    border: 2px solid rgb(255, 255, 255);
    position: absolute;
    top: -15px;
    right: -55px;
    
    z-index:1032;
}
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $role =  $this->session->userdata('cr_user_role');
//echo $role;die; 
$roleHeading = array('admin'=>'Admin','agent'=>'Agent','supplier'=>'Supplier','service_provider'=>'Service Provider','customer'=>'Customer','finance_admin'=>'Finance Admin','finance'=>'Finance','tour_admin'=>'Tour Admin');
$profileImg = ($profile_image!='')?'uploads/user/'.$profile_image:'public/dist/img/1484856467_human.png';
?>
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo SITE_NAME;?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo $roleHeading[$role];?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url($profileImg);?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $admin_fname; ?> <?php echo $admin_lname; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url($profileImg);?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $admin_fname; ?> <?php echo $admin_lname; ?> - <?php echo $admin_role; ?>
                  <small>Member since <?php echo date("F Y",strtotime($user_created_at));?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
               
              </li> -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo  base_url(CRM_VAR.'/myProfile'); ?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo  base_url(CRM_VAR.'/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
