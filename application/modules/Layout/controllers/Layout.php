<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
This class handle the layouts
 */
class Layout extends MY_Controller {
  public function __construct() {
		parent::__construct();
		// $this->session->set_userdata('active_language','mm');
		$this->load->module("Layout");

		
        $this->lang->load('Layout/layout', $this->getActiveLanguage());
	}
	public function admin($data)
	{
		$data['admin_email'] = $this->session->userdata('crm_user_email');
		$data['admin_fname'] = $this->session->userdata('crm_user_fname');
		$data['admin_lname'] = $this->session->userdata('crm_user_lname');
		$data['admin_role'] = $this->session->userdata('crm_user_role');
		$data['user_created_at'] = $this->session->userdata('user_created_at');
		$data['profile_image'] = $this->session->userdata('profile_image');
		$this->load->view('admin/index',$data);
		// print "<pre/>";
		// print_r($data);
	}
	
	function authorize()
	{	
		
		$data['module'] = 'Layout';
		$data['view'] = 'admin/authorize';
		$this->layout->admin($data);
	}
	public function front($data)
	{
		$data['active_currency']=$this->session->userdata('active_currency');
		$this->load->view('front/index',$data);
	}
	
}
