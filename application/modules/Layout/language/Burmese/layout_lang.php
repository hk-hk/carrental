<?php

defined('BASEPATH') OR exit('ယခုစာမ်က္နာအား ၾကည့္ရႈ႕ခြင့္မျပဳပါ');



//$lang['MENU_HOME'] = 'ပင္မ';

$lang['MENU_HOME'] = 'ပင္မ';
$lang['MENU_CAR_RENTAL'] = 'ကားငွားရန္';
$lang['MENU_AIR_TICKET'] = 'ေလယာဥ္လက္မွတ္';
$lang['MENU_ABOUT_US'] = 'ကၽြႏု္ပ္တု႔ိအေၾကာင္း';

$lang['MENU_ATTRACTIONS'] = 'ခရီးစဥ္မ်ား';

$lang['MENU_FAQ'] = 'အျမဲေမးေလ့ရိွေသာေမးခြန္းမ်ား';

$lang['MENU_CONTACT_US'] = 'ဆက္သြယ္ရန္';

$lang['MENU_LOGIN'] = 'အေကာင့္သိုု႔၀င္ရန္';

$lang['MENU_ACCOUNT'] = ' မိမိအေကာင့္';

$lang['MENU_FOOTER_USEFUL_LINK'] = 'အသံုး၀င္ေသာ စာမ်က္ႏွာမ်ား';

$lang['MENU_FOOTER_WELCOME'] = ' ၾကိဳဆိုပါသည္';

$lang['MENU_FOOTER_FOR_CLIENT'] = 'မိတ္ေဆြတိုု႔ အတြက္';

$lang['MENU_FOOTER_PRIVACY_POLICY'] = 'သင္ရဲ႕ကိုယ္ေရးကိုယ္တာအခ်က္အလက္မ်ား';

$lang['MENU_FOOTER_ABOUT_US'] = 'ကၽြႏု္ပ္တု႔ိအေၾကာင္း';

$lang['MENU_FOOTER_CANDIDATES'] = 'ကိုယ္စားလွယ္';

$lang['MENU_FOOTER_PRIVACY'] = 'ေစာင့္စည္းမႈ';

$lang['MENU_FOOTER_AGENT_REGISTER'] = 'အေရာင္းကိုယ္စလွယ္အေကာင့္ဖြင့္ရန္ ';
$lang['MENU_FOOTER_SUPPLIER_REGISTER'] = 'အက်ိဳးတူကားအငွားအေကာင့္ဖြင့္ရန္';

$lang['MENU_FOOTER_TERMS_AND_CONDITIONS'] = 'ေစာင့္စည္းရမည့္ မူ၀ါဒ';

$lang['MENU_FOOTER_TERMS'] = 'သတ္မွတ္ခ်က္';

$lang['MENU_PHONE'] = 'ေမးစရာ ေမးခြန္းရိွရင္  ဖုန္းေခၚဆိုုႏိုုင္ပါတယ္';

$lang['FOOTER_ABOUT_US_INFO'] = '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has </p><p>more-or-less normal distribution of letters, as opposed</p>';

$lang['FOOTER_COPYRIGHTS'] = 'Copyright 2017 Sonic Star | All Rights Reserved';

$lang['TEXT_EMAIL'] = 'အီးေမးလ္';

$lang['TEXT_FAX'] = 'ဖက္စ္';

$lang['TEXT_CONTACT_US'] = 'ဆက္္သြယ္ရန္';

$lang['FOOTER_SECTION_FIRST'] = 'ကၽြႏု္ပ္တို႔ အေၾကာင္း';

$lang['FOOTER_SECTION_SECOND'] = 'အသံုး၀င္ေသာ လင့္မ်ား';

$lang['FOOTER_SECTION_THIRD'] = 'ဆက္္သြယ္ရန္';

$lang['AIRPORT_PICKUP_OPTION'] = 'ေလယာဥ္ကြင္းႀကိဳ ';
$lang['AIRPORT_DROPOFF_OPTION'] = 'ေလယာဥ္ကြင္းပို႔';



