<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['MENU_HOME'] = 'Home';
$lang['MENU_CAR_RENTAL'] = 'Car Rental';
$lang['MENU_AIR_TICKET'] = 'Flights';

$lang['MENU_ABOUT_US'] = 'About Us';
$lang['MENU_ATTRACTIONS'] = 'Tour Packages';
$lang['MENU_FAQ'] = 'FAQ';
$lang['MENU_CONTACT_US'] = 'Contact Us';
$lang['MENU_LOGIN'] = 'Login';
$lang['MENU_ACCOUNT'] = 'Account';
$lang['MENU_FOOTER_USEFUL_LINK'] = 'USEFUL LINK';
$lang['MENU_FOOTER_WELCOME'] = 'Welcome';
$lang['MENU_FOOTER_FOR_CLIENT'] = 'For Client';
$lang['MENU_FOOTER_PRIVACY_POLICY'] = 'Privacy Policy';
$lang['MENU_FOOTER_TERMS_AND_CONDITIONS'] = 'Terms and Conditions';
$lang['MENU_FOOTER_ABOUT_US'] = 'About Us';
$lang['MENU_FOOTER_CANDIDATES'] = 'Candidates';
$lang['MENU_FOOTER_PRIVACY'] = 'Privacy';
$lang['MENU_FOOTER_AGENT_REGISTER'] = 'Agent Register';
$lang['MENU_FOOTER_SUPPLIER_REGISTER'] = 'Supplier Register';

$lang['MENU_FOOTER_TERMS'] = 'Terms';
$lang['MENU_PHONE'] = 'Have any Question? Call us :';
$lang['FOOTER_ABOUT_US_INFO'] = '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has </p><p>more-or-less normal distribution of letters, as opposed</p>';
$lang['FOOTER_COPYRIGHTS'] = 'Copyright 2017 Sonic Star | All Rights Reserved';
$lang['TEXT_EMAIL'] = 'Email';
$lang['TEXT_FAX'] = 'Fax';
$lang['TEXT_CONTACT_US'] = 'Contact Us';
$lang['FOOTER_SECTION_FIRST'] = 'About Us';
$lang['FOOTER_SECTION_SECOND'] = 'Useful Link';
$lang['FOOTER_SECTION_THIRD'] = 'Contact Us';

$lang['AIRPORT_PICKUP_OPTION'] = 'Airport PickUp';
$lang['AIRPORT_DROPOFF_OPTION'] = 'Airport DropOff';