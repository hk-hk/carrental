<?php
$route[CRM_VAR]                 = 'Users/Admin';
$route[CRM_VAR.'/login']        = 'Users/Admin';
$route[CRM_VAR.'/forgotpass']   = 'Users/Admin/forgotpass';
$route[CRM_VAR.'/get_password/(:any)']      = 'Users/Admin/get_password/$1';
$route[CRM_VAR.'/crm-do-login'] = 'Users/Admin/post_login';
$route[CRM_VAR.'/logout']       = 'Users/Admin/logout';
$route[CRM_VAR.'/adduser']      = 'Users/Admin/add_user';
$route[CRM_VAR.'/adduser/(:num)'] = 'Users/Admin/add_user/$1';
$route[CRM_VAR.'/listuser']     = 'Users/Admin/listUsers';
$route[CRM_VAR.'/ajax_users_list'] = 'Users/Admin/ajax_users_list';
$route[CRM_VAR.'/ajax_update_user_status/(:any)/(:any)'] = 'Users/Admin/ajax_update_user_status/$1/$2';
$route[CRM_VAR.'/ajax_delete_user/(:any)'] = 'Users/Admin/ajax_delete_user/$1';
$route['adduser']      = 'Users/Admin/add_user';


$route['login']      = 'Users/Users/index';
$route['fb-login']      = 'Users/Users/facebook_login';
$route['update-type']      = 'Users/Users/update_account_type';
$route['test-email']      = 'Users/Users/test_email';
$route['register']      = 'Users/Users/register';
$route['profile']     = 'Users/UserProfile';
$route['logout']       = 'Users/UserProfile/logout';
$route['lost_password']      = 'Users/Users/lost_password';
$route['lost_password/(:any)']      = 'Users/Users/lost_password/$1';
$route['get_password/(:any)']      = 'Users/Users/get_password/$1';

$route['register/agent']      = 'Users/Users/register';
$route['register/supplier']      = 'Users/Users/register';
$route[CRM_VAR.'/myProfile'] = 'Users/Admin/edit_myprofile';