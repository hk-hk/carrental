<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Model {

	public $_table = 'cr_users'; // database table
	public $before_create = array( 'timestamps' );
    public $column_order = array(null, 'user_lname','user_email','user_tel','user_role','user_active'); 
     //set column field database for datatable orderable
	 
    public $column_search = array('user_lname','user_email','user_tel','user_role'); 
     //set column field database for datatable searchable 
    public $order = array('id' => 'asc'); // default order
	 
     // validations rules
	 public $config = array(
        array(
                'field' => 'user_fname',
                'label' => 'User First Name',
                'rules' => 'required'
        ),
        /*array(
                'field' => 'user_lname',
                'label' => 'User Last Name',
                'rules' => 'required'
        ),
        array(
                'field' => 'user_password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
                        'required' => 'You must provide a %s.',
                ),
        ),*/

	);

     public function __construct() {

        parent::__construct();

    }

	
     protected function timestamps($user)
    {
        $user['created_at'] = $user['updated_at'] = date('Y-m-d H:i:s');
        return $user;
    }

    private function _get_datatables_query() // dynamic search query of. user.
    {

	if($this->session->userdata('crm_user_role') == 'MANAGER') // Check for manager user.
	{
        $this->db->where('user_role', 'PROSPECTOR');
	}
        //add custom filter here
        if($this->input->post('user_fname'))
        {
            $this->db->where('user_fname', $this->input->post('user_fname'));
        }
        if($this->input->post('user_lname'))
        {
            $this->db->like('user_lname', $this->input->post('user_lname'));
        }
        if($this->input->post('user_email'))
        {
            $this->db->like('user_email', $this->input->post('user_email'));
        }
      if($this->input->post('user_role'))
        {
            $this->db->where('user_role', $this->input->post('user_role'));
        }
        if($this->input->post('user_active')){
            $this->db->where('user_active', $this->input->post('user_active'));
        }
        $this->db->from($this->_table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
	

    }

    
function check_email_exist($id,$email)  // check email is exist or not  for edit user case
    {
        $this->db->from('cr_users');      
        $this->db->where('user_email', $email);
        $this->db->where('user_role!=', '');
	    $this->db->where('id!=', $id);	    
        return $this->db->count_all_results();
	   
    }
    function get_datatables() 
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();      
        return $query->result();
    }

    function count_filtered() // Get total row count of search query
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function deleteUserPermanantly($userid){
        if($userid>0 && $userid!=1){
            $sql = "DELETE `od`,`pi` FROM `cr_order_detail` as od LEFT JOIN `cr_payment_info` as pi ON od.order_id=pi.order_id  WHERE od.`order_id` IN (SELECT `order_id` FROM `cr_orders` WHERE `user_id`='".$userid."')";
            $this->db->query($sql);
            
            $sql = "DELETE `o`,`u`,`mc` FROM `cr_users` as u LEFT JOIN `cr_orders` as o ON u.id=o.user_id LEFT JOIN `cr_manage_cars` as mc ON u.id=mc.user_id WHERE u.id=".$userid;
            return $this->db->query($sql);
        }else{
            return false;
        }
    }
    public function manageSocialUser($userinfo){

        $this->db->from('cr_users');      
        $this->db->where('user_email', $userinfo['email']);
        //echo $this->db->last_query();die;
        $query = $this->db->get();  
        $userStatus = array();
        if($query->num_rows()>0){
            $uinfo = $query->row();
            $userStatus['user_id'] = $uinfo->id;
            $userStatus['register_type'] = 'byweb';
            //$uinfo = $uinfo->user_id;
        }else{
            $data = array(
                'user_email' => $userinfo['email'],
                'user_lname' => $userinfo['name'],
                'user_role' => 'customer',
                'user_active' => 'YES'
            );
            $this->db->insert('cr_users', $data);
            $userStatus['user_id'] = $this->db->insert_id();
            $userStatus['register_type'] = 'byfb';
        }
        $data = array(
            'user_id' => $userStatus['user_id'],
            'oauth_uid' => $userinfo['id'],
            'profile_pic' => $userinfo['picture']['data']['url']
        );
        $this->db->insert('cr_social_user_oauth', $data);
        return $userStatus;

    }
    public function checkSocialUser($oauth_uid,$oauth_provider){     
        $this->db->from('cr_social_user_oauth');      
        $this->db->where('oauth_uid', $oauth_uid);
        $this->db->where('oauth_provider', $oauth_provider);
        //echo $this->db->last_query();die;
        $query = $this->db->get();  
        if($query->num_rows()>0){
            $uinfo = $query->row();
            //$uinfo = $uinfo->user_id;
        }else{
            $uinfo = false;
        }
        return $uinfo;
    }
    
    function get_role_customers($role) 
    {
        $this->db->from('cr_users');      
        $this->db->where('user_role', $role);
        //echo $this->db->last_query();die;
        $query = $this->db->get();      
        return $query->result();
    }    
}
