<?php
/***
 *
 * Created by: CIS
 * This is for authorized user's only.
 * This file contains all user's related functions like login, forgot passowrd, logout etc.
 *
 ***/
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'Users'; // Load Users module
        $this->load->model('User', 'user'); // Load User modal
        $this->user->check_permission(); // check permission for accessing function of this file
    }

    public function index()
    {
        if ($this->session->userdata('crm_user_logged') === true) {
            // check user is logged in or not
            if ($this->session->userdata('crm_user_id') > 0) {
                redirect(CRM_VAR . '/dashboard'); // if user is logged in then, Load user's dashoboard page.
            }
        }
        $this->load->view('Login'); //if user is not logged then, Load user's Login page
    }

    public function post_login()
    {
        // no direct access
        $this->is_ajax();
        // user posted data
        $this->email    = $this->input->post('email', true);
        $this->password = $this->input->post('password', true);
        // get user by email
        $user_obj = $this->user->get_by(array('user_email' => $this->email, 'user_active' => 'YES'));
        if ($user_obj && $user_obj->user_active != 'deleted') {
            if (password_verify($this->password, $user_obj->user_has_password)) {

                $set_sessions = [
                    'crm_user_id'     => $user_obj->id,
                    'crm_user_email'  => $user_obj->user_email,
                    'crm_user_fname'  => $user_obj->user_fname,
                    'crm_user_lname'  => $user_obj->user_lname,
                    'cr_user_role'    => $user_obj->user_role,
                    'crm_user_logged' => true,
                ];
                $this->set_sessions($set_sessions);

                $this->set_msg_flash([
                    'msg' => sprintf($this->welcome_message, $user_obj->user_email),
                    'typ' => 1,
                ]);
                echo json_encode(array("success" => true));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => 'Invalid Credentials!!'));
            }
            exit;

        } else {
            echo json_encode(array("success" => false, "msg" => 'Invalid Credentials!!'));exit;
        }

    }

    public function logout()
    {
        $this->session->sess_destroy();
        $set_sessions = [
            'crm_user_id'     => 0,
            'crm_user_logged' => false,
        ];
        $this->session->set_userdata($set_sessions);
        $this->isAuthorized();
    }

    public function get_password($code = '')
    {
        $user_info = $this->user->get_by('unique_forgetpass_code', $code);
        if (empty($user_info)) {
            // Set message,If user id is invalid
            $this->set_msg_flash(['msg' => 'This is not a valid code for change password. Please try again!', 'typ' => 2]);
            redirect(site_url(CRM_VAR . '/forgotpass'));
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $validate_arr = array(
                array(
                    'field' => 'user_pass',
                    'label' => 'Password',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'cnf_user_pass',
                    'label' => 'Confirm Password',
                    'rules' => 'required|matches[user_pass]',
                ),
            );
            $this->form_validation->set_rules($validate_arr);
            if ($this->form_validation->run() == false) {
                $this->set_msg_flash([
                    'msg' => 'Password and Confirm password does not matched!!',
                    'typ' => 3,
                ]);
            } else {
                $user_info = $this->user->get_by('unique_forgetpass_code', $code);
                if (empty($user_info)) {
                    // Set message,If user id is invalid
                    $this->set_msg_flash(['msg' => 'This is not a valid code for change password. Please try again!', 'typ' => 2]);
                    redirect(site_url(CRM_VAR . '/forgotpass'));
                } else {
                    $postarray = array(
                        'user_has_password'      => password_hash($this->input->post('user_pass', true), PASSWORD_DEFAULT),
                        'unique_forgetpass_code' => '',
                        'user_password'          => $this->input->post('user_pass', true),
                    );
                    $this->user->update($user_info->id, $postarray);
                    $this->set_msg_flash(['msg' => 'Your Password has been changed sucessfully.', 'typ' => 1]);
                    redirect(site_url(CRM_VAR . '/login'));
                }
            }
        }
        $this->load->view('get_password');
    }

    public function forgotpass() // Forgot password function

    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $email     = $this->input->post('user_email');
            $user_info = $this->user->get_by('user_email', $email);

            $unique_forgetpass_code = md5(uniqid(rand(), true));

            if (empty($user_info)) {
                // Set message,If user id is invalid
                $this->set_msg_flash(['msg' => 'This is not a valid User ID', 'typ' => 2]);
                redirect(site_url(CRM_VAR . '/forgotpass'));
            } else {
                $this->user->update($user_info->id, array("unique_forgetpass_code" => $unique_forgetpass_code));
                if ($email != '') {
                    $to       = $email;
                    $cc       = 'khushboocis@mailinator.com';
                    $bcc      = '';
                    $subject  = "Lost Password"; //subject for mail
                    $body     = file_get_contents("./templates/lost_password.html"); //mail template path
                    $body     = str_replace("{lost_password_link}", site_url() . CRM_VAR . "/get_password/" . $unique_forgetpass_code, $body);
                    $mailData = array(
                        "to"      => $to,
                        "cc"      => $cc,
                        "bcc"     => $bcc,
                        "subject" => $subject,
                        "message" => $body,
                    );

                    //mail function call
                    $result = $this->sendMail($mailData);
                    $this->set_msg_flash(['msg' => 'Change password link sent to your email. Please check it.', 'typ' => 1]);
                    redirect(site_url(CRM_VAR . '/login'));
                }
            }

        }
        $this->load->view('Forgot'); // Load forgot password page.
    }

    public function error_404_page()
    {
        $this->load->view('404page'); //if user hit woring url then, Load error page.
    }
    public function get_user_type($type = '')
    {
        $userTypes = array('customer' => 'Customer', 'supplier' => 'Car Supplier', 'agent' => 'Agent', 'admin' => 'Admin' , 'finance_admin' => 'Finance Admin','finance' => 'Finance','service_provider' => 'Service Provider', 'driver' => 'Driver','tour_admin' => 'Tour Admin');
        if ($type != '' && in_array($type, $userTypes)) {
            return $userTypes[$type];
        } else {
            return $userTypes;
        }
    }
    public function add_user($id = 0) //user add/edit function.

    {

        $this->isAuthorized(); // check user is authoried or not
        // $this->checkPermission('adduser');
        $this->load->model('ToolModel');
        $countryList               = $this->user->get_attributes(array(4045), true);
        $this->data['countryList'] = $countryList[4045];

        $this->data['user_types'] = $this->get_user_type();
        $this->data['countries']  = $countries  = $this->ToolModel->create_options('crm_countries', array(), array('name_country' => 'ASC'));
        $this->data['userinfo']   = (object) array('id' => '', 'user_fname' => '', 'user_fname' => '', 'user_lname' => '', 'user_email' => '', 'user_role' => '', 'user_active' => '', 'user_company' => '', 'user_title' => '', 'user_tel' => '', 'profile_image' => '', 'country_id' => '', 'address' => '', 'commission_rate' => '');
        $commission_rate          = $this->input->post('commission_rate', true);
        $postarray                = array('user_fname' => $this->input->post('user_fname', true), 'user_lname' => $this->input->post('user_lname', true), 'user_email' => $this->input->post('user_email', true), 'user_role' => $this->input->post('user_role', true), 'user_active' => ($this->input->post('user_status', true) == 'on') ? 'YES' : 'NO', 'user_tel' => $this->input->post('tel', true), 'address' => $this->input->post('address', true), 'country_id' => $this->input->post('country_id', true), 'commission_rate' => $commission_rate);

        if ($this->input->post('user_role', true) === 'agent' || $this->input->post('user_role', true) === 'supplier') {
            $postarray['user_company'] = $this->input->post('company_name', true);
            $postarray['user_title']   = $this->input->post('position_title', true);
        } else {
            $postarray['user_company'] = '';
            $postarray['user_title']   = '';
        }

        $thumb = array(
            0 => array("height" => '100', "width" => '100'),
        );
        $image_upload = array(
            "file_name"   => "profile_image",
            "upload_path" => "./uploads/user",
            "prefix"      => "user",
            "thumb_array" => $thumb,
        );
        if ($id > 0) // Check for edit user
        {
            // $this->checkPermission('edit_user');
            $user_info = $this->user->get_by('id', $id);

            if (empty($user_info)) {
                // Set message,If user id is invalid
                $this->set_msg_flash(['msg' => 'This is not a valid User ID', 'typ' => 2]);
                redirect(CRM_VAR . '/listuser');
            }
            /* image validation for edit */

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if ($_FILES['profile_image']['name'] == '') {
                    $postarray["profile_image"] = $user_info->profile_image;
                } else {
                    $image = $this->uploadImage($image_upload);

                    if (!$image) {
                        $this->form_validation->set_rules("profile_image", "Valid Profile Image", "required");
                    } else {
                        if ($user_info->profile_image != '') {
                            if (file_exists("./uploads/user/" . $user_info->profile_image)) {
                                unlink("./uploads/user/" . $user_info->profile_image);
                            }
                            if (file_exists("./uploads/user/thumb/100X100/" . $user_info->profile_image)) {
                                unlink("./uploads/user/thumb/100X100/" . $user_info->profile_image);
                            }
                        }
                        $postarray["profile_image"] = $image; // we can have file path by indexing 'file_path'
                    }
                }
            }
            /* image validation for edit */
            $this->data['userinfo'] = $user_info;
            //user_email'=>$this->input->post('user_email' ,true)
            if ($this->input->post('user_email', true) != $user_info->user_email) {
                $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|callback_check_email');
            }
            $this->form_validation->set_rules('tel', 'Telephone No.', 'numeric');
            if ($this->input->post('user_password') != '') {
                $this->form_validation->set_rules('user_password', 'User Password.', 'required');
                $this->form_validation->set_rules('conpassword', 'Confirm Password.', 'required|matches[user_password]');
            }
            $postarray['user_has_password'] = $this->input->post('user_password', true) == '' ? $user_info->user_has_password : password_hash($this->input->post('user_password', true), PASSWORD_DEFAULT);
            $postarray['user_password']     = $this->input->post('user_password', true) == '' ? $user_info->user_password : $this->input->post('user_password', true);
        } else {
            /* image validation for add */
            if (isset($_FILES['profile_image']['name'])) {
                if ($_FILES['profile_image']['name'] != '') {
                    $image = $this->uploadImage($image_upload);
                    if (!$image) {
                        $this->form_validation->set_rules("profile_image", "Valid Profile Image", "required");
                    } else {
                        $postarray["profile_image"] = $image; // we can have file path by indexing 'file_path'
                    }
                }}
            /* image validation for add */
            // $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email|is_unique[cr_users.user_email]');
            $this->form_validation->set_rules('tel', 'Telephone No.', 'numeric');
            $this->form_validation->set_rules('user_password', 'User Password.', 'required');
            $this->form_validation->set_rules('conpassword', 'Confirm Password.', 'required|matches[user_password]');
            $postarray['user_has_password'] = password_hash($this->input->post('user_password', true), PASSWORD_DEFAULT);
            $postarray['user_password']     = $this->input->post('user_password', true);
        }

        $this->data['view'] = 'Adduser'; // Load add/edit user form
        $from_car_booking=$this->session->userdata('from_car_booking');
        // echo $from_car_booking;
        $this->form_validation->set_rules($this->user->config); // set validation rules
        if ($id > 0 && $this->form_validation->run($this) == false) // check validation for edit user
        {
            $this->layout->admin($this->data);
        } 
        else if ($id == 0 && $this->form_validation->run() == false) // check validation for add user
        {
            $this->layout->admin($this->data);
        } else {
            if ($id > 0) {

                if ($this->user->update($id, $postarray)) {
                    // Upade user's information
                    $this->set_msg_flash(['msg' => 'User Updated!!', 'typ' => 1]);
                    redirect(CRM_VAR . '/listuser');
                } else {
                    redirect(CRM_VAR . '/adduser/' . $id);
                }

            } else {
                        // print "<pre/>";
                        // print_r($_POST);
                if ($this->user->insert($postarray)) {
                    // Add new user
                    if(!empty($this->input->post('redirect'))){
                        redirect($this->input->post('redirect'));
                    }
                    if(!empty($from_car_booking)){
                        $this->session->set_userdata('from_car_booking',"");
                        redirect($from_car_booking);   
                    }else{
                        // else{
                            redirect(CRM_VAR . '/adduser');
                        // }
                        // echo "not here";
                    }
                } else {
                    redirect(CRM_VAR . '/adduser');
                }
            }
        }
    }

    public function edit_myprofile()
    {
        $this->isAuthorized(); // check user is authoried or not
        $this->checkPermission('admin-myprofile');
        $this->data['edit_profile'] = true;
        $countryList                = $this->user->get_attributes(array(4045), true);
        $this->data['countryList']  = $countryList[4045];
        $this->load->model('ToolModel');
        $this->data['countries'] = $countries = $this->ToolModel->create_options('crm_countries', array(), array('name_country' => 'ASC'));
        $this->data['userinfo']  = (object) array('id' => '', 'user_fname' => '', 'user_fname' => '', 'user_lname' => '', 'user_email' => '', 'user_role' => '', 'user_active' => '', 'user_company' => '', 'user_title' => '', 'user_tel' => '', 'country_id' => '', 'address' => '');

        $postarray = array('user_fname' => $this->input->post('user_fname', true), 'user_lname' => $this->input->post('user_lname', true), 'user_email' => $this->input->post('user_email', true), 'user_tel' => $this->input->post('tel', true), 'address' => $this->input->post('address', true), 'country_id' => $this->input->post('country_id', true));

        if ($this->input->post('user_role', true) === 'agent' || $this->input->post('user_role', true) === 'supplier') {
            $postarray['user_company'] = $this->input->post('company_name', true);
            $postarray['user_title']   = $this->input->post('position_title', true);
        } else {
            $postarray['user_company'] = '';
            $postarray['user_title']   = '';
        }

        $user_info              = $this->user->get_by('id', $this->session->userdata('crm_user_id'));
        $this->data['userinfo'] = $user_info;
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('tel', 'Telephone No.', 'numeric');

        if ($this->input->post('user_password') != '') {
            $this->form_validation->set_rules('user_password', 'User Password.', 'required');
            $this->form_validation->set_rules('conpassword', 'Confirm Password.', 'required|matches[user_password]');
        }
        $postarray['user_has_password'] = $this->input->post('user_password', true) == '' ? $user_info->user_has_password : password_hash($this->input->post('user_password', true), PASSWORD_DEFAULT);
        $postarray['user_password']     = $this->input->post('user_password', true) == '' ? $user_info->user_password : $this->input->post('user_password', true);

        $this->data['view'] = 'Adduser'; // Load add/edit user form
        $thumb              = array(
            0 => array("height" => '100', "width" => '100'),
        );
        $image_upload = array(
            "file_name"   => "profile_image",
            "upload_path" => "./uploads/user",
            "prefix"      => "user",
            "thumb_array" => $thumb,
        );
        /* image validation for edit */

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if ($_FILES['profile_image']['name'] == '') {
                $postarray["profile_image"] = $user_info->profile_image;
            } else {
                $image = $this->uploadImage($image_upload);

                if (!$image) {
                    $this->form_validation->set_rules("profile_image", "Valid Profile Image", "required");
                } else {
                    if ($user_info->profile_image != '') {
                        if (file_exists("./uploads/user/" . $user_info->profile_image)) {
                            unlink("./uploads/user/" . $user_info->profile_image);
                        }
                        if (file_exists("./uploads/user/thumb/100X100/" . $user_info->profile_image)) {
                            unlink("./uploads/user/thumb/100X100/" . $user_info->profile_image);
                        }
                    }
                    $postarray["profile_image"] = $image; // we can have file path by indexing 'file_path'
                }
            }
        }
        /* image validation for edit */
        $this->form_validation->set_rules($this->user->config); // set validation rules
        if ($this->form_validation->run() == false) // check validation for add user
        { $this->layout->admin($this->data);} else {

            if ($this->user->update($this->session->userdata('crm_user_id'), $postarray)) {
                // Upade user's information
                $this->set_msg_flash(['msg' => 'Profile Updated!!', 'typ' => 1]);
                redirect(CRM_VAR . '/myProfile');
            } else {redirect(CRM_VAR . '/myProfile');}

        }
    }

    public function check_email()
    {
        // Email validation callback funtion for edit user.
        $id        = $this->uri->segment(3);
        $email     = $this->input->post('user_email', true);
        $user_info = $this->user->check_email_exist($id, $email);
        if ($user_info > 0) {
            $this->form_validation->set_message('check_email', 'Email is already exist');
            return false;
        } else {return true;}
    }

    public function listUsers()
    {

        $this->isAuthorized();
        $this->checkPermission('listuser');
        $this->data['user_types'] = $this->get_user_type();
        $this->data['view']       = 'listUsers'; //Load all user's list page
        $this->layout->admin($this->data);
    }

    public function ajax_users_list() //function for load all user's list in ajax datatable

    {
        $this->is_ajax(); // checking for ajax request
        $this->isAuthorized(); // check user authorization
        $list = $this->user->get_datatables();
        $this->checkPermission('listuser');
        $user_active  = $this->input->post('user_active', true);
        $data         = array();
        $no           = $_POST['start'];
        $userStatuses = array('NO' => 'Deactive', 'YES' => 'Active', 'deleted' => 'Deleted');
        foreach ($list as $user) {
            $stat = ($user->user_active == 'NO') ? 'Deactive' : 'Active';
            $show = '<div id="user_' . $user->id . '" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content">
          <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">User Information</h4></div><div class="modal-body"><p><b>Full Name: </b>' . $user->user_fname . ' ' . $user->user_lname . '<br><b>Email: </b>' . $user->user_email . '<br><b>Address: </b>' . $user->address . '<br><b>User Role: </b>' . $user->user_role . '<br><b>Member Since: </b>' . $user->created_at . '<br><b>Status: </b>' . $stat . '<br></p></div></div></div></div>';
            $no++;
            $row      = array();
            $row[]    = $no;
            $row[]    = $user->user_fname . " " . $user->user_lname;
            $row[]    = $user->user_email;
            $row[]    = $user->user_tel;
            $row[]    = $user->user_role;
            $isActive = ($user->user_active == 'NO' || $user->user_active == 'deleted') ? 'btn-danger' : 'btn-success';
            $btntxt   = (isset($userStatuses[$user->user_active]) ? $userStatuses[$user->user_active] : 'Deactive'); //($user->user_active == 'NO')? 'Active' : 'Deactive';
            $status   = ($user->user_active == 'NO') ? 'YES' : 'NO';
            $dataID   = base64_encode('ID_' . $user->id);
            $row[]    = '<span data-parent="' . $dataID . '"><a class="btn ' . $isActive . ' btn-xs change-status" data-id="' . $dataID . '" data_status="' . $status . '">' . $btntxt . '</a></span>';
            if ($this->session->userdata('crm_user_role') == 'MANAGER') // Check for manager user.
            {
                $row[] = $show . '<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#user_' . $user->id . '">View</button>';} else {
                if ($user_active == 'Deleted') {
                    $deletedStatus = 'pdeleted';
                } else {
                    $deletedStatus = 'deleted';
                }
                $row[] = $show . '<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#user_' . $user->id . '">View</button>&nbsp;<a href="' . site_url(CRM_VAR) . '/adduser/' . $user->id . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;<button class="btn btn-danger btn-sm change-status" data_status="' . $deletedStatus . '" data-id="' . $dataID . '"><i class="fa fa-remove"></i></button>';
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->user->count_all(),
            "recordsFiltered" => $this->user->count_filtered(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_update_user_status($uid, $status) // update user's status by ajax

    {
        $this->is_ajax();
        $this->isAuthorized();
        $this->checkPermission('edit_user');
        $id = str_replace('ID_', '', base64_decode($uid));
        if ($status == 'pdeleted') {
            $this->user->deleteUserPermanantly($id);
            $output = "User Permanently Deleted";
        } else {
            $res = $this->user->update($id, array('user_active' => $status));
            if ($res) {
                if ($status == 'deleted') {
                    $output = 'Deleted successfully';
                } else {
                    $output = 'Updated successfully';
                }

            } else {
                $output = 'Please try again.';}
        }
        echo $output;
        die();
    }
    public function ajax_delete_user($uid)
    {
        $this->is_ajax();
        $this->isAuthorized();
        $this->checkPermission('delete_user');
        $id  = str_replace('ID_', '', base64_decode($uid));
        $res = $this->user->delete($id);
        if ($res) {$output = 'Updated successfully';} else { $output = 'Please try again.';}
        echo $output . $res;
        die();
    }

}
