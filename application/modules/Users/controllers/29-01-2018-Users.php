<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->data['module'] = 'Users'; // Load Users module
		$this->load->model('User', 'user'); // Load User modal
                //$this->user->check_permission(); // check permission for accessing function of this file

		if($this->session->userdata("crm_user_logged")){
			redirect(site_url().'profile');
		}
                $this->lang->load($this->data['module'].'/users', $this->getActiveLanguage());
	}

	public function index()
	{
            //die("dfgdfg");
           
            $this->data['extra_js'] = array('jquery.flagstrap.min.js');
            $this->data['page_title'] = getlang('BREADCRUMBS_LOGIN_LOGIN');
            $this->data['breadcrumbs'] = array(
                                            array('url'=>site_url(),'title'=>getLang('BREADCRUMBS_HOME')),
                                            array('url'=>'','title'=> getLang('BREADCRUMBS_LOGIN')),
                                        );
            
            if($this->input->get('req',TRUE)){
                $redirecturl = $this->input->get('req',TRUE);
                $this->session->set_userdata('redirect',$redirecturl);
            }
            $this->data['view'] = 'front/Login'; 
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
            	$this->email    = $this->input->post('user_email' ,true);
	            $this->password = $this->input->post('user_pass',true);
	            // get user by email
	            $user_obj = $this->user->get_by(array('user_email'=>$this->email,'user_active'=>'YES'));
	            if ($user_obj && $user_obj->user_active !='deleted') {
	                    if (password_verify($this->password, $user_obj->user_has_password)) {
	                    		//print_r($user_obj);die;
	                            $set_sessions = [
	                                    'site_user_id'=>$user_obj->id,
	                                    'site_user_email'=>$user_obj->user_email,
	                                    'site_user_fname'=>$user_obj->user_fname,
	                                    'site_user_lname'=>$user_obj->user_lname,
	                                    'site_user_role' =>$user_obj->user_role,
	                                    'site_user_logged'=>TRUE
	                            ];
	                            $set_sessions = [
                                            'crm_user_id'=>$user_obj->id,
                                            'crm_user_email'=>$user_obj->user_email,
                                            'crm_user_fname'=>$user_obj->user_fname,
                                            'crm_user_lname'=>$user_obj->user_lname,
                                            'cr_user_role' => ($user_obj->user_role!='')?$user_obj->user_role:'customer',
                                            'crm_user_logged'=>TRUE,
                                            'user_created_at' =>$user_obj->created_at,
                                            'profile_image' =>$user_obj-> profile_image,
                                    ];
	                            $this->set_sessions($set_sessions);

	                            $this->set_msg_flash([
	                                    'msg'=>sprintf($this->welcome_message,$user_obj->user_fname." ".$user_obj->user_lname),
	                                    'typ'=>1
	                            ]);

	                            /*echo json_encode(array("success"=>true));
	                            exit;*/
	                            $redirecturl = $this->session->userdata('redirect');
                                    if($redirecturl!=''){
                                        redirect(urldecode($redirecturl));
                                    }else{  
                                    	if($user_obj->user_role!='customer'){
                                    		redirect(site_url(CRM_VAR.'/dashboard'));
                                    	}else{
                                    		redirect(site_url('myaccount/orders'));
                                    	}
                                    }
	                    }else {
	                    	$this->set_msg_flash([
                                    'msg'=> getLang('LOGIN_MSG_INVALID'),
                                    'typ'=>3
                            ]);
	                    	//exit;
	                    }

	            }else {
	                    $this->set_msg_flash([
                                'msg'=> getLang('LOGIN_MSG_INVALID'),
                                'typ'=>3
                        ]);
                    	//exit;
	            }
            }
            $this->layout->front($this->data);
	}
    public function register()
	{

            $this->data['extra_css'] = array('flags.css');
            $this->data['extra_js'] = array('owl.carousel.min.js','jquery.flagstrap.min.js','wow.js');
            $this->data['page_title'] =  getLang('BREADCRUMBS_REGISTER');
            $this->data['breadcrumbs'] = array(
                                            array('url'=>site_url(),'title'=> getLang('BREADCRUMBS_HOME')),
                                            array('url'=>'','title'=> getLang('BREADCRUMBS_REGISTER')),
                                        );
            $countryList = $this->user->get_attributes(array(4045),TRUE);
            
            $this->data['countryList'] = $countryList[4045]; 
            $this->data['view'] = 'front/register'; 
            if($this->input->get('req',TRUE)){
                $redirecturl = $this->input->get('req',TRUE);
                $this->session->set_userdata('redirect',$redirecturl);
            }
            
            if($_SERVER['REQUEST_METHOD'] == 'POST'){ 
            	$validate_arr = array(
			        array(
			                'field' => 'fname',
			                'label' => getLang('PROFILE_FIELD_TITLE'),
			                'rules' => 'trim|required'
			        ),
                                array(
			                'field' => 'lname',
			                'label' => getLang('PROFILE_FIELD_FULL_NAME'),
			                'rules' => 'trim|required'
			        ),
			        array(
			                'field' => 'email',
			                'label' => getLang('PROFILE_FIELD_EMAIL'),
			                'rules' => 'trim|required|valid_email|is_unique[cr_users.user_email]'
			        ),
			        array(
			                'field' => 'pass',
			                'label' => getLang('PROFILE_FIELD_PASSWORD'),
			                'rules' => 'required',
			        ),
			        array(
			                'field' => 'cpass',
			                'label' => getLang('PROFILE_FIELD_CON_PASSWORD'),
			                'rules' => 'required|matches[pass]',
			        ),
			        array(
                                        'field' => 'tel',
                                        'label' => getLang('PROFILE_FIELD_TELEPHONE'),
                                        'rules' => 'numeric'
			        )
			    );
				$this->form_validation->set_rules($validate_arr);
				if ($this->form_validation->run() == FALSE)
                {

                	/*$this->set_msg_flash([
                            'msg'=> getLang('REGISTER_INVALID_SUBMITION'),
                            'typ'=>3
                    ]);*/
                }
                else{
                	$postarray = array(
                		'user_fname'=>$this->input->post('fname' ,true),
                		'user_lname'=>$this->input->post('lname' ,true),
                		'user_email'=>$this->input->post('email' ,true),
                		'user_has_password'=>password_hash($this->input->post('pass' ,true),PASSWORD_DEFAULT),
                		'country_id'=>$this->input->post('country_id' ,true),
                		'address'=>$this->input->post('address' ,true),
                		'user_password'=>$this->input->post('pass' ,true),
                		'user_tel'=> $this->input->post('tel' ,true),
                		'user_active'=> 'YES'
                	);

                	if($this->uri->segment(2) == 'agent' || $this->uri->segment(2) == 'supplier'){
                		$postarray['user_company'] = $this->input->post('company_name' ,true);
                		$postarray['user_title'] = $this->input->post('title_position' ,true);
                	}

                	switch ($this->uri->segment(2)) {
                		case 'agent':
                			$postarray['user_role'] = 'agent';
                			break;
                		
                		case 'supplier':
                			$postarray['user_role'] = 'supplier';
                			break;

                		default:
                			$postarray['user_role'] = 'customer';
                			break;
                	}
                	$thumb = array(
			            0 => array("height" => '100', "width" => '100'),
			        );
			        $image_upload = array(
			            "file_name" => "profile_image",
			            "upload_path" => "./uploads/user",
			            "prefix" => "user",
			            "thumb_array" => $thumb
			        );
			        /* image validation for add */
		              if(isset($_FILES['profile_image']['name'])){if($_FILES['profile_image']['name'] != ''){
		                $image = $this->uploadImage($image_upload);
		                if ( ! $image)
		                {
		                  $this->form_validation->set_rules("profile_image", getLang('REGISTER_INVALID_PROFILE_IMG'), "required");
		                }
		                else
		                {
		                  $postarray["profile_image"] = $image; // we can have file path by indexing 'file_path'
		                }
		              }}
		              /* image validation for add */
                	if($this->user->insert($postarray)) {  // Add new user

                		/* sending mail to registered user */
                                        $to = $this->input->post('email');
				        $subject = "Successfully Registered"; //subject for mail
						$body = file_get_contents("./templates/success.html");  //mail template path
				        $body = str_replace("{NAME}", $this->input->post('fname' ,true)." ".$this->input->post('lname' ,true), $body);
						$body = str_replace("{SITE}", site_url(), $body);
						$body = str_replace("{LOGINURL}", site_url('login'), $body);
						$body = str_replace("{SITE_EMAIL_ADDRESS}", 'info@sonicstartravel.com', $body);
        				$body = str_replace("{SITE_PHONE_NO}", '09 426 999 100', $body);
						$mailData = array(
				            "to" => $to,
				            "subject" => $subject,
				            "message" => $body
				        );

				        //mail function call
				        $result = $this->sendMail($mailData);
				        /* sending mail to registered user */
                                        if($result){
                                            $this->set_msg_flash(['msg'=> getLang('REGISTER_SUCCESSFULL'),'typ'=>1]);
                                        }else{
                                            $this->set_msg_flash(['msg'=> getLang('REGISTER_SUCCESSFULL') ,'typ'=>2]);
                                        }
                                        redirect(site_url().'login');
					}
                }
            }
            $this->layout->front($this->data);
	}

	public function get_password($code='')
	{
		$user_info = $this->user->get_by('unique_forgetpass_code', $code);
		if(empty($user_info)) { // Set message,If user id is invalid
			$this->set_msg_flash(['msg'=> getLang('CHANGE_PASS_INVALID_CODE'),'typ'=>2]);
	        redirect(site_url('lost_password'));
        }
		$this->data['extra_js'] = array('jquery.flagstrap.min.js');
        $this->data['page_title'] = getLang('BREADCRUMBS_CHANGE_PASS');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=> getLang('BREADCRUMBS_HOME')),
                                        array('url'=>'','title'=> getLang('BREADCRUMBS_CHANGE_PASS')),
                                    );
        $this->data['view'] = 'front/get_password';
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        	$validate_arr = array(
		        array(
		                'field' => 'user_pass',
		                'label' => 'Password',
		                'rules' => 'required',
		        ),
		        array(
		                'field' => 'cnf_user_pass',
		                'label' => 'Confirm Password',
		                'rules' => 'required|matches[user_pass]',
		        )
		    );
			$this->form_validation->set_rules($validate_arr);
			if ($this->form_validation->run() == FALSE)
            {
            	/*$this->set_msg_flash([
                        'msg'=> getLang('REGISTER_INVALID_SUBMITION'),
                        'typ'=>3
                ]);*/
            }
            else{
            	$user_info = $this->user->get_by('unique_forgetpass_code', $code);
				if(empty($user_info)) { // Set message,If user id is invalid
					$this->set_msg_flash(['msg'=> getLang('CHANGE_PASS_INVALID_CODE'),'typ'=>2]);
			        redirect(site_url('lost_password'));
		        }
		        else{
	            	$postarray = array(
	            		'user_has_password'=>password_hash($this->input->post('user_pass' ,true),PASSWORD_DEFAULT),
	            		'unique_forgetpass_code'=>'',
	            		'user_password'=>$this->input->post('user_pass' ,true),
	            	);
	            	$this->user->update($user_info->id ,$postarray);
	            	$this->set_msg_flash(['msg'=> getLang('CHANGE_PASS_SUCCESSFULLY'),'typ'=>1]);
		        	redirect(site_url('login'));
	            }
            }
        }
        $this->layout->front($this->data);
	}

	public function lost_password($email='')
	{
		$this->data['extra_js'] = array('jquery.flagstrap.min.js');
        $this->data['page_title'] = getLang('BREADCRUMBS_FORGOT_PASS');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=> getLang('BREADCRUMBS_HOME')),
                                        array('url'=>'','title'=> getLang('BREADCRUMBS_FORGOT_PASS')),
                                    );
        $this->data['email'] = $email; 
        $this->data['view'] = 'front/lost_password'; 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        	$email = $this->input->post('user_email');
        	$user_info = $this->user->get_by('user_email', $email);

        	$unique_forgetpass_code = md5(uniqid(rand(), true));

			if(empty($user_info)) { // Set message,If user id is invalid
				$this->set_msg_flash(['msg'=> getLang('FORGOT_PASS_INVALID_USER'),'typ'=>2]);
		        redirect(site_url('lost_password'));
	        }
			else{
				$this->user->update($user_info->id ,array("unique_forgetpass_code" => $unique_forgetpass_code));
				if($email != ''){
		        	$to = $email;
			        $subject = "Lost Password"; //subject for mail
					$body = file_get_contents("./templates/lost_password.html");  //mail template path
			        $body = str_replace("{lost_password_link}", site_url()."get_password/".$unique_forgetpass_code, $body); 
			        $body = str_replace("{EMAIL}", $email, $body); 
					$body = str_replace("{SITE}", site_url(), $body); 
					$body = str_replace("{SITE_EMAIL_ADDRESS}", 'info@sonicstartravel.com', $body);
    				$body = str_replace("{SITE_PHONE_NO}", '09 426 999 100', $body);
					$mailData = array(
			            "to" => $to,
			            "subject" => $subject,
			            "message" => $body
			        );
			        //print_r($mailData);die;
			        //mail function call
			        $result = $this->sendMail($mailData);
                                if($result)
                                    $this->set_msg_flash(['msg'=> getLang('FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL'),'typ'=>1]);
                                else 
                                    $this->set_msg_flash(['msg'=> getLang('FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL_ISSUE'),'typ'=>2]);
		        	redirect(site_url('login'));
				}
	        }

        }
        $this->layout->front($this->data);
	}

}
