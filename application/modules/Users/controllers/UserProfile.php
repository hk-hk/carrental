<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserProfile extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->data['module'] = 'Users'; // Load Users module
		$this->load->model('User', 'user'); // Load User modal
		if(!$this->session->userdata("crm_user_logged")){
			redirect(site_url().'login');
		}else if($this->session->userdata("cr_user_role")!='customer'){
                    redirect(site_url().CRM_VAR.'/dashboard');
                }
                $this->lang->load($this->data['module'].'/users', $this->getActiveLanguage());
	}

	public function index()
	{ 
        $this->checkPermission('my-profile');
        $this->data['user_info'] = $this->user->get_by('id', $this->session->userdata("crm_user_id"));
        //echo "<pre>";print_r($this->data['user_info']);echo "</pre>";
        $this->data['extra_css'] = array('flags.css');
        $this->data['extra_js'] = array('owl.carousel.min.js','jquery.flagstrap.min.js','wow.js');
        $this->data['page_title'] = getLang('BREADCRUMBS_MY_PROFILE');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=> getLang('BREADCRUMBS_HOME')),
                                        array('url'=>'','title'=> getLang('BREADCRUMBS_MY_PROFILE')),
                                    );
        $countryList = $this->user->get_attributes(array(4045),TRUE); 
        $this->data['countryList'] = $countryList[4045]; 
        $this->data['view'] = 'front/myProfile'; 
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        	$validate_arr = array(
		        array(
		                'field' => 'fname',
		                'label' => getLang('PROFILE_FIELD_TITLE'),
		                'rules' => 'trim|required'
		        ),
                        array(
		                'field' => 'lname',
		                'label' => getLang('PROFILE_FIELD_FULL_NAME'),
		                'rules' => 'trim|required'
		        ),
		        array(
                                'field' => 'tel',
                                'label' => getLang('PROFILE_FIELD_TELEPHONE'),
                                'rules' => 'numeric'
		        )
		    );
		    $this->form_validation->set_rules($validate_arr);
		    if($this->input->post('pass' ,true) != ''){
		    	$this->form_validation->set_rules('pass', getLang('PROFILE_FIELD_PASSWORD'),'required');
		    	$this->form_validation->set_rules('cpass', getLang('PROFILE_FIELD_CON_PASSWORD') ,'required|matches[pass]');
		    }
			if ($this->form_validation->run() == FALSE)
            {
            	$this->set_msg_flash([
                    'msg'=> getLang('REGISTER_INVALID_SUBMITION'),
                    'typ'=>3
                ]);
            }
            else{
                $postarray = array(
                    'user_fname'=>$this->input->post('fname' ,true),
                    'user_lname'=>$this->input->post('lname' ,true),
                    'address'=>$this->input->post('address' ,true),
                    'country_id'=>$this->input->post('country_id' ,true),
                    'user_tel'=> $this->input->post('tel' ,true),
                );

                $user_info = $this->data['user_info'];
                $thumb = array(
                    0 => array("height" => '100', "width" => '100'),
                );
                $image_upload = array(
                    "file_name" => "profile_image",
                    "upload_path" => "./uploads/user",
                    "prefix" => "user",
                    "thumb_array" => $thumb
                );
                /* image validation for edit */
            
                  if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    if($_FILES['profile_image']['name'] == ''){
                      $postarray["profile_image"] = $user_info->profile_image;
                    }
                    else{
                      $image = $this->uploadImage($image_upload);

                      if ( ! $image)
                      {
                        $this->form_validation->set_rules("profile_image", getLang('PROFILE_FIELD_PROFILE_IMAGE'), "required");
                      }
                      else
                      {
                        if(file_exists("./uploads/user/".$user_info->profile_image)){
                          unlink("./uploads/user/".$user_info->profile_image);
                        }
                        if(file_exists("./uploads/user/thumb/100X100/".$user_info->profile_image)){
                          unlink("./uploads/user/thumb/100X100/".$user_info->profile_image);
                        }
                        $postarray["profile_image"] = $image; // we can have file path by indexing 'file_path'die
                      }
                    }
                  }
                  /* image validation for edit */
            	if($this->input->post('pass' ,true) != ''){
            		$postarray['user_has_password'] = password_hash($this->input->post('pass' ,true),PASSWORD_DEFAULT);
            		$postarray['user_password'] = $this->input->post('pass' ,true);
            	}
            	if($this->data['user_info']->user_role == 'agent' || $this->data['user_info']->user_role == 'supplier'){
            		$postarray['user_company'] = $this->input->post('company_name' ,true);
            		$postarray['user_title'] = $this->input->post('title_position' ,true);
            	}
            	if($this->user->update($this->data['user_info']->id, $postarray)) {  // Update User

            		$this->set_msg_flash(['msg'=> getLang('PROFILE_UPDATE_SUCCESSFULLY'),'typ'=>1]);
					redirect(site_url().'profile');
				}
            }
        }
        
        $this->layout->front($this->data);
    }

    public function logout() {
        $activeLanguage = $this->session->userdata('active_language');
		$this->session->sess_destroy();
                $set_sessions = [
                        'crm_user_id'=>0,
                        'crm_user_email'=>'',
                        'crm_user_fname'=>'',
                        'crm_user_lname'=>'',
                        'cr_user_role' =>'',
                        'user_created_at' =>'',
                        'profile_image' =>'',
                        'crm_user_logged'=>FALSE
					];
		$this->session->set_userdata($set_sessions);

        //$this->session->set_userdata('active_language',$activeLanguage);
		// redirect(site_url('change-lang/'.$activeLanguage));
        redirect(site_url().'login');
	}

}