<style type="text/css">
    .error{
        color: #ED1C24;
    }
</style>
<section class="login-form common-padding">
    <div class="container">
        <div class="row">


            <?php
                           if ($this->session->flashdata('typ')):
                               switch ($this->session->flashdata('typ')) {
                                   case 1:
                                       $put = 'alert-success';
                                       break;
                                   case 2:
                                       $put = 'alert-warning';
                                       break;
                                   case 3:
                                       $put = 'alert-danger';
                                       break;
                               }
                               
                               ?>
                        <div class="alert <?php echo $put; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i></h4>
                        <?php echo $this->session->flashdata('msg');
                         echo validation_errors();
                        ?>
                            
                        </div><?php endif; ?>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="sign-in-box">
                    <div class="login_info_block">
                        <h2 class="black section-title common-title"><span>sign up now</span></h2>
                        <form action="" method="post" enctype='multipart/form-data'>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_TITLE');?></label>&nbsp;<?php echo form_error('fname', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> 
                                    <select name="fname" id="fname" required="required" >
                                        <option value="Mr."><?php echo getLang('PROFILE_FIELD_TITLE_OPTION1');?></option>
                                        <option value="Ms."><?php echo getLang('PROFILE_FIELD_TITLE_OPTION3');?></option>
                                        <option value="Mrs."><?php echo getLang('PROFILE_FIELD_TITLE_OPTION2');?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_FULL_NAME');?></label>
                                <div class="input-block"> 
                                    <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                    <input class="input-fill2" type="text" name="lname" id="lname" value="<?php echo set_value('lname'); ?>">
                                </div>
                            </div>

                            <span class="error">
                                <?php echo isset($error_duplicated_email) ? getLang('REGISTER_EMAIL_ALREADY_USED_MGS') : ''; ?>
                            </span>
                            <!-- fields for the agent and supplier -->
                            <?php if ($this->uri->segment(2) == 'agent' || $this->uri->segment(2) == 'supplier') { ?>
                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('PROFILE_FIELD_COMPANY_NAME');?></label>
                                    <div class="input-block"> 
                                        <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="company_name" id="company_name" value="<?php echo set_value('company_name'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('PROFILE_FIELD_POSITION');?></label>
                                    <div class="input-block"> 
                                        <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="title_position" id="title_position" value="<?php echo set_value('title_position'); ?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- fields for the agent and supplier -->

                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_EMAIL');?></label>&nbsp;<?php echo form_error('email', '<span class="error user_email">', '</span>'); ?>

                                <div class="input-block"> 
                                    <span class="input-icon"><i class="fa fa-envelope"></i></span>
                                    <input class="input-fill2" type="email" name="email" id="email" required="required" value="<?php echo set_value('email'); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_PASSWORD');?></label>&nbsp;<?php echo form_error('pass', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" name="pass" id="pass" required="required" value="<?php echo set_value('pass'); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_CON_PASSWORD');?></label>&nbsp;<?php echo form_error('cpass', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" name="cpass" id="cpass" required="required" value="<?php echo set_value('cpass'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_ADDRESS');?></label>&nbsp;<?php echo form_error('address', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="text" name="address" id="address"  value="<?php echo set_value('address'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_COUNTRY');?></label>&nbsp;<?php echo form_error('country_id', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> 
                                    <select  name="country_id" id="country_id">
                                        <?php foreach ($countryList as $country) {
                                            if (strtolower($country['title']) == 'myanmar') {
                                                ?>
                                                <option value="<?php echo $country['id'] ?>" selected="selected"><?php echo $country['title'] ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $country['id'] ?>"><?php echo $country['title'] ?></option>
                                            <?php }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_TELEPHONE');?></label>&nbsp;<?php echo form_error('tel', '<span class="error">', '</span>'); ?>
                                <div class="input-block telephone-block"> 
                                    <span class="input-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="flagstrap select-country" id="select_country" data-input-name="NewBuyer_country" data-selected-country="" style="display: none"></div>
                                    <input class="input-fill2" type="text" placeholder="+1 - 123 456 7890" name="tel" id="tel" value="<?php echo set_value('tel'); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_PROFILE_IMAGE');?></label>&nbsp;<?php echo form_error('profile_image', '<span class="error">', '</span>'); ?>
                                <input type="file" accept="image/*" name="profile_image" />
                            </div>

                            <div class="form-group">
                                <button type="submit" class="more login-btn"><?php echo getLang('REGISTER_FIELD_SBT_BTN');?></button>
                            </div>
                    </div>

                    </form>
                </div>
            </div>



        </div>
    </div> 
</section>
<script>
$(function(){
    if($(".user_email").text()!=''){
       // alert("You can get your password by link");
        //$(".user_email").html('');
        $(".user_email").html('<?php echo getLang('REGISTER_EMAIL_ALREADY_USED_MGS');?><a href="'+SITE_URL+'lost_password/'+$("#email").val()+'"><?php echo getLang('REGISTER_EMAIL_ALREADY_USED_MGS1');?></a>');
    }
});
</script>
