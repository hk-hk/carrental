<section class="login-form common-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="sign-in-box">
                    <?php
                    if ($this->session->flashdata('typ')):
                        switch ($this->session->flashdata('typ')) {
                            case 1:
                                $put = 'alert-success';
                                break;
                            case 2:
                                $put = 'alert-warning';
                                break;
                            case 3:
                                $put = 'alert-danger';
                                break;
                        }
                        ?>
                        <div class="alert <?php echo $put; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i></h4>
                        <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="login_info_block">
                        <h2 class="black section-title common-title"><span><?php echo getLang('FORGOT_PASS_HEADING'); ?></span></h2>
                        <form method="post">
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('FORGOT_EMAIL_FIELD'); ?></label>
                                <div class="input-block"> 
                                    <span class="input-icon"><i class="fa fa-envelope"></i></span>
                                    <input class="input-fill2" type="email" required="required" name="user_email" id="user_email" value="<?php echo $email; ?>"placeholder="<?php echo getLang('FORGOT_EMAIL_FIELD'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="Submit" value="<?php echo getLang('FORGOT_SUBMIT_BUTTON'); ?>" class="more login-btn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>