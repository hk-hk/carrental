<section class="login-form common-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="sign-in-box">
                    <?php
                    if ($this->session->flashdata('typ')):
                        switch ($this->session->flashdata('typ')) {
                            case 1:
                                $put = 'alert-success';
                                break;
                            case 2:
                                $put = 'alert-warning';
                                break;
                            case 3:
                                $put = 'alert-danger';
                                break;
                        }
                        ?>
                        <div class="alert <?php echo $put; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i></h4>
                        <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="login_info_block">
                        <h2 class="black section-title common-title"><span><?php echo getLang('LOGIN_HEADING'); ?></span></h2>
                        <form method="post">
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('LOGIN_EMAIL'); ?></label>
                                <div class="input-block"> 
                                    <span class="input-icon"><i class="fa fa-envelope"></i></span>
                                    <input class="input-fill2" type="email" required="required" name="user_email" id="user_email" placeholder="<?php echo getLang('LOGIN_EMAIL'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('LOGIN_PASSWORD'); ?></label>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" required="required" name="user_pass" id="user_pass" placeholder="<?php echo getLang('LOGIN_PASSWORD'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                
                                <input type="submit" name="login" value="<?php echo getLang('LOGIN_TEXT_BTN'); ?>" class="more login-btn">
                                <div class="lost-password"><a href="<?php echo site_url(); ?>lost_password"><?php echo getLang('LOGIN_TEXT_FORGOT_PASSWORD'); ?></a></div>
                            </div>
                        </form>
                    </div>
                    <?php if(true){?>
                    <div class="or-box"><span><?php echo getLang('LOGIN_TEXT_OR') ?></span></div>
                    <div class="form-group fb-custom-botton">
                        <fb:login-button size="xlarge"
                          scope="public_profile,email"
                          onlogin="checkLoginState();">
                          Login with Facebook
                        </fb:login-button>
                    </div>
                    <?php } ?>
                    <div class="or-box"><span><?php echo getLang('LOGIN_TEXT_OR') ?></span></div>
                    <div class="form-group">
                        <a href="<?php echo site_url(); ?>register"><button type="button" class="more signup-btn"><?php echo getLang('LOGIN_TEXT_REGISTER'); ?></button></a>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>
