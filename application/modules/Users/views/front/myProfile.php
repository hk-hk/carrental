<style type="text/css">
    .error{
        color: #ED1C24;
    }
</style>
<section class="login-form common-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="sign-in-box">
                    <div class="login_info_block">
                        <p id="msg" style="display: none;color: red;
                           font-size: 16px;
                           text-align: center;"></p>
                           <?php
                           if ($this->session->flashdata('typ')):
                               switch ($this->session->flashdata('typ')) {
                                   case 1:
                                       $put = 'alert-success';
                                       break;
                                   case 2:
                                       $put = 'alert-warning';
                                       break;
                                   case 3:
                                       $put = 'alert-danger';
                                       break;
                               }
                               ?>
                        <div class="alert <?php echo $put; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i></h4>
                        <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                            <?php endif; ?>
                        <ul class="sub-menu">
                            <li><a href="<?php echo site_url('myaccount/orders'); ?>"><?php echo getLang('MENU_MY_BOOKING');?></a></li>
                            <li><a href="<?php echo site_url('logout'); ?>"><?php echo getLang('MENU_LOGOUT');?></a></li>
                        </ul>
                        <h2 class="black section-title common-title"><span><?php echo getLang('PROFILE_PAGE_HEADING');?></span></h2>
                        <form action="" method="post" autocomplete="off" enctype='multipart/form-data'>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_TITLE');?></label>&nbsp;<?php echo form_error('fname', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> 
                                    <select name="fname" id="fname" required="required" >
                                        <option value="Mr." <?php if ($user_info->user_fname == 'Mr.') {
                                            echo "selected=selected";
                                            } ?>><?php echo getLang('PROFILE_FIELD_TITLE_OPTION1');?></option>
                                        <option value="Mrs." <?php if ($user_info->user_fname == 'Mrs.') {
                                            echo "selected=selected";
                                        } ?>><?php echo getLang('PROFILE_FIELD_TITLE_OPTION2');?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_FULL_NAME');?></label>
                                <div class="input-block"> 
                                    <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                    <input class="input-fill2" type="text" name="lname" id="lname" value="<?php echo $user_info->user_lname; ?>">
                                </div>
                            </div>
                            <!-- fields for the agent and supplier -->
                            <?php if ($this->uri->segment(2) == 'agent' || $this->uri->segment(2) == 'supplier') { ?>
                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('PROFILE_FIELD_COMPANY_NAME');?></label>
                                    <div class="input-block"> 
                                        <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="company_name" id="company_name" value="<?php echo $user_info->user_company; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('PROFILE_FIELD_POSITION');?></label>
                                    <div class="input-block"> 
                                        <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="title_position" id="title_position" value="<?php echo $user_info->user_title; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- fields for the agent and supplier -->
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_EMAIL');?></label>&nbsp;<?php echo form_error('email', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> 
                                    <span class="input-icon"><i class="fa fa-envelope"></i></span>
                                    <input class="input-fill2" type="email" name="email" id="email" disabled="disabled" value="<?php echo $user_info->user_email; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_PASSWORD');?></label>&nbsp;<?php echo form_error('pass', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" name="pass" id="pass" value="" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_CON_PASSWORD');?></label>&nbsp;<?php echo form_error('cpass', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" name="cpass" id="cpass" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_ADDRESS');?></label>&nbsp;<?php echo form_error('address', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-home"></i></span>
                                    <input class="input-fill2" type="text" name="address" id="address"  value="<?php echo $user_info->address; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_COUNTRY');?></label>&nbsp;<?php echo form_error('country_id', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> 
                                    <select  name="country_id" id="country_id">
                                        <?php foreach ($countryList as $country) {
                                            if (strtolower($country['id']) == $user_info->country_id) {
                                                ?>
                                                <option value="<?php echo $country['id'] ?>" selected="selected"><?php echo $country['title'] ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $country['id'] ?>"><?php echo $country['title'] ?></option>
                                            <?php }
                                            }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_TELEPHONE');?></label>&nbsp;<?php echo form_error('tel', '<span class="error">', '</span>'); ?>
                                <div class="input-block telephone-block"> 
                                    <span class="input-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <div class="flagstrap select-country" id="select_country" data-input-name="NewBuyer_country" data-selected-country=""></div>
                                    <input class="input-fill2" type="text" placeholder="+1 - 123 456 7890" name="tel" id="tel" value="<?php echo $user_info->user_tel; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('PROFILE_FIELD_PROFILE_IMAGE');?></label>&nbsp;
                                    <?php echo form_error('profile_image', '<span class="error">', '</span>'); ?>
                                <input type="file" accept="image/*" name="profile_image" />
                                    <?php if ($user_info->profile_image != '') { ?>
                                    <img height="100px" width="100px" src="<?php echo site_url(); ?>uploads/user/<?php echo $user_info->profile_image; ?>">
                                    <?php } ?>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="more login-btn"><?php echo getLang('PROFILE_FIELD_EDIT_BTN');?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

