<style>.error{color:red;}</style>
<section class="login-form common-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="sign-in-box">
                    <?php
                    if ($this->session->flashdata('typ')):
                        switch ($this->session->flashdata('typ')) {
                            case 1:
                                $put = 'alert-success';
                                break;
                            case 2:
                                $put = 'alert-warning';
                                break;
                            case 3:
                                $put = 'alert-danger';
                                break;
                        }
                        ?>
                        <div class="alert <?php echo $put; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Alert!</h4>
                        <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="login_info_block">
                        <h2 class="black section-title common-title"><span><?php echo getLang('LOST_PASS_HEADING');?></span></h2>
                        <form method="post">
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('LOST_FIELD_PASS');?></label>&nbsp;<?php echo form_error('user_pass', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" required="required" name="user_pass" id="user_pass" placeholder="<?php echo getLang('LOST_FIELD_PASS');?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('LOST_FIELD_CON_PASS');?></label>&nbsp;<?php echo form_error('cnf_user_pass', '<span class="error">', '</span>'); ?>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" required="required" name="cnf_user_pass" id="cnf_user_pass" placeholder="<?php echo getLang('LOST_FIELD_CON_PASS');?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="change" value="<?php echo getLang('LOST_TEXT_SUBMIT_BTN');?>" class="more login-btn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

