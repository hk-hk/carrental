<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Users</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    
          <div class="box-body">
          <h3 class="box-title">Filter</h3>
          <form id="form-filter" class="form-horizontal">
                   
                    
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="user_lname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="user_email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label">User Type</label>
                        <div class="col-sm-4">
                           <select class="form-control" name="user_role" id="user_role">
                           <option value="">Select User Type</option>
				<?php foreach ($user_types as $k=>$utype){ ?>
                                    <option value="<?php echo $k?>"><?php echo $utype; ?></option>
                                <?php } ?>
			   </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label">User Status</label>
                        <div class="col-sm-4">
                           <select class="form-control" name="user_active" id="user_active">
                                <option value="">Select Status</option>
                                <option value="YES" selected="selected">Enable</option>
                                <option value="NO">Disable</option>
                                <option value="Deleted">Deleted</option>
                            </select>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </form>
            <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
		    <th>Active</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Role</th>
										<th>Active</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
          </div>
          </div>
          <!-- /.box-body -->
         <!--  <div class="box-footer">
            <button type="submit" class="btn btn-info">Add User</button>
          </div> -->
          <!-- /.box-footer -->
         
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
 $("#user_role").val('customer');
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
        "info" : false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/ajax_users_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.user_email = $('#user_email').val();
                data.user_fname = $('#user_fname').val();
                data.user_lname = $('#user_lname').val(); 
                data.user_role  = $('#user_role').val();              
                data.user_active  = $('#user_active').val();              
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,6 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });
 
 $(document).on("click",".change-status" ,function() {
     
    var status = $(this).attr("data_status");
    if(status=='deleted'){ 
        var chk_c = $(this).attr("data-id");
    }else{
        var chk_p = $(this).parent().attr("data-parent");
        var chk_c = $(this).attr("data-id");
        var status = $(this).attr("data_status");						
        if (chk_p === chk_c) {
            if ($( this ).hasClass( "btn-danger" ) ) {
                $( this ).removeClass("btn-danger");
                $( this ).addClass("btn-success");
            }else {
                $( this ).addClass("btn-danger");
                $( this ).removeClass("btn-success");
            }
        }
    }						              
    $.ajax({
            url:"<?php echo site_url(CRM_VAR.'/ajax_update_user_status/')?>"+chk_c+"/"+status,
            data:"",
            type:"GET",
        success:function (data) {
                alert(data);
        table.ajax.reload();
        }
    });
            
            
    }); 
 $(document).on("click",".deleteuser" ,function() {
        var userid = $(this).attr("data-uid");	
        alert(userid);
        $.ajax({
            url:"<?php echo site_url(CRM_VAR.'/ajax_delete_user/')?>"+userid,
            data:"",
            type:"GET",
            success:function (data) {
                //alert(data);
                table.ajax.reload();
            }
        });
            
            
    }); 
});
 
</script>
