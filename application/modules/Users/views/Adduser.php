<?php //print_r($userinfo); die;?><section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?php if (!empty($userinfo)) {
                            echo 'Edit User Form';
                        } else {
                            echo 'Add User Form';
                        }
                        //print_r($userinfo);die;
                        ?>
                    </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <?php $attributes = array('id' => 'adduser-form', 'class' => 'form-horizontal', 'autocomplete' => 'off'); ?>
                    <?php if (!empty($userinfo)) {
                        if (!isset($edit_profile)) {
                            echo form_open_multipart(CRM_VAR . '/adduser/' . $userinfo->id, $attributes);
                        } else {
                            echo form_open_multipart(CRM_VAR . '/myProfile', $attributes);
                        }
                    } else {
                        echo form_open_multipart(CRM_VAR . '/adduser', $attributes);
                    }
                    ?>

                    <?php echo validation_errors(); ?>
                <div class="box-body">
<?php if (!isset($edit_profile)) { ?>
                        <div class="form-group">
                            <label for="state" class="col-sm-2 control-label" >Role</label>
                            <div class="col-sm-4"> <?php //print_r($userinfo);  ?>
                                <select class="form-control" id="user_role" name="user_role">
    <?php foreach ($user_types as $k => $utype) { ?>
                                        <option value="<?php echo $k ?>" <?php if ($userinfo->user_role == $k) {
            echo 'selected';
        } ?>><?php echo $utype; ?></option>
    <?php } ?>
                                </select>
                            </div>
                        </div>
<?php }else { echo '<input type="hidden" id="user_role" name="user_role" value="'.$userinfo->user_role.'">'; } ?>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-4">
                        <select name="user_fname" id="user_fname" required="required" class="form-control" >
                        <option value="Mr." <?php echo set_select('user_fname','Mr.', ($userinfo->user_fname == "Mr." ? TRUE : FALSE ));?>>Mr.</option>
                        <option value="Ms." <?php echo set_select('user_fname','Ms.', ($userinfo->user_fname == "Ms." ? TRUE : FALSE ));?>>Ms.</option>
                        <option value="Mrs." <?php echo set_select('user_fname','Mrs.', ($userinfo->user_fname == "Mrs." ? TRUE : FALSE ));?>>Mrs.</option>
                        </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Full Name</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="user_lname" name="user_lname" placeholder="Full Name" 
                            value="<?php echo set_value('user_lname',$userinfo->user_lname);?>"/>
                        </div>
                    </div>    

                    <div id="agent_supplier_div" style="display: none;">
                        <div class="form-group">
                            <label for="lastname" class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="<?php 
                                echo set_value('user_company',$userinfo->user_company); ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lastname" class="col-sm-2 control-label">Position / Title</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="position_title" name="position_title" placeholder="Position / Title" 
                                value='<?php echo set_value('position_title',$userinfo->user_title);?>'>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-4">
                            <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" 
                            value='' autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-4">
                            <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password" value='' autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cpassword" class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-4">
                            <input type="password" class="form-control" id="conpassword" name="conpassword" placeholder="Confirm Password" value=''>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address</label>&nbsp;<?php echo form_error('address', '<span class="error">', '</span>'); ?>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" name="address" id="address"  
                            value="<?php echo set_value('address',$userinfo->address); ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Country</label>&nbsp;<?php echo form_error('country_id', '<span class="error">', '</span>'); ?>
                        <div class="col-sm-4">
                            <select  name="country_id" id="country_id" class="form-control">
                            <?php foreach ($countryList as $country) { ?>
                                <option value="<?php echo $country['id'] ?>" <?php echo set_select('country_id',$country['id'], ($userinfo->country_id == $country['id'] ? TRUE : FALSE ));?>><?php echo $country['title'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cpassword" class="col-sm-2 control-label">Telephone No.</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="tel" name="tel" placeholder="Telephone No." 
                            value='<?php echo set_value('tel',$userinfo->user_tel);?>' />
                        </div>
                    </div>
                    <?php if(in_array($userinfo->user_role, array('agent','supplier'))){ ?>
                    <div class="form-group">
                        <label for="commission_rate" class="col-sm-2 control-label">Commission Rate</label>
                        <div class="col-sm-4">
                            <?php if($this->session->userdata('cr_user_role')=='admin' || $this->session->userdata('cr_user_role')=='service_provider'){?>
                            <input type="number" step="0.25" name="commission_rate" value="<?php echo set_value('commission_rate',$userinfo->commission_rate); ?>" class="form-control"/>
                            <?php }else{ echo $userinfo->commission_rate; } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Profile Image</label>
                        <div class="col-sm-4">
                            <input type="file" accept="image/*" name="profile_image" />
<?php if ($userinfo->profile_image != '') { ?><img height="100px" width="100px" src="<?php echo site_url(); ?>uploads/user/<?php echo $userinfo->profile_image; ?>"><?php } ?>

                        </div>
                    </div>
<?php if (!isset($edit_profile)) { ?>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="user_status" <?php if (!empty($userinfo) && ($userinfo->user_active == 'YES')) {
        echo 'Checked';
    } ?> value="on"> Active
                                    </label>
                                </div>
                            </div>
                        </div>
<?php } ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <input type="hidden" name="redirect" value="<?= !empty($_GET['redirect'])? $_GET['redirect'] : '' ?>">
<?php if (!empty($userinfo)) { ?> <button type="submit" class="btn btn-info">Edit User</button> <?php } else {
    ?> <button type="submit" class="btn btn-info">Add User</button> <?php } ?>           
                </div>
                <!-- /.box-footer -->
<?php echo form_close(); ?>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
    </div>
    <!-- /.row -->
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $("#adduser-form").validate({
        rules: {
        user_fname: "required",
                user_lname : "required",
                user_gender   : "required",
                user_email  	 : {
                required:true,
                        email   :true
                },
<?php if ($userinfo->user_password == '') { ?>
            user_password : "required",
                    conpassword: {
                    equalTo:"#user_password"
                    },
<?php } ?>
        user_address: "required",
                user_country: "required",
                /*user_state  : "required",
                 user_city   : "required",*/
    },
            messages: {
            user_fname  : "Please enter first name",
                    user_lname   : "Please enter last name",
                    //user_gender     : "Please select gender",
                    user_email      : "Please enter valid email",
                    user_password   : "Please enter password",
                    conpassword: "Confirm password should match",
                    //user_address    : "Please enter address",
                    //user_country    : "Please select country",
                    /*user_state      : "Please select state",
                     user_city       : "Please enter city",*/
            },
            errorClass: "my-error-class",
            errorElement: "span", // default is 'label'
            errorPlacement: function(error, element) {
            error.insertAfter(element);
            },
    });
            $(".choose-country").change(function() {
    var pcall = $(this);
            $.ajax({
            url:"<?php echo base_url('Tool/getStates'); ?>",
                    data:{country:pcall.val()},
                    type:'GET',
                    dataType:"JSON",
                    success:function(response) {
                    $.each(response.records, function(i, s) {
                    $("#user_state").append("<option value='" + s.id_state + "'>" + s.name_state + "</option>");
                    });
                    }
            })
    });
    });</script>
<script type="text/javascript">
            $(document).ready(function(){
    function check_user($role){
    if ($role == 'agent' || $role == 'supplier'){
    $("#agent_supplier_div").show();
            $("#company_name").val('<?php echo $userinfo->user_company; ?>');
            $("#position_title").val('<?php echo $userinfo->user_title; ?>');
    }
    else{
    $("#company_name").val('');
            $("#position_title").val('');
            $("#agent_supplier_div").hide();
    }
    }
    check_user($("#user_role").val());
            $("#user_role").change(function(){
    check_user($(this).val());
    });
    });
</script>