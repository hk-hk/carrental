<?php
defined('BASEPATH') OR exit('ယခုစာမ်က္နာအား ၾကည့္ရႈခြင့္မျပဳပါ');
//Login page
$lang['LOGIN_HEADING'] = 'အေကာင့္သိုု႔၀င္ရန္';
$lang['LOGIN_EMAIL'] = 'အီးေမးလ္';
$lang['LOGIN_PASSWORD'] = ' လွ်ိဳ႕၀ွက္နံပါတ္';
$lang['LOGIN_TEXT_BTN'] = ' အေကာင့္သိုု႔၀င္ရန္';
$lang['LOGIN_TEXT_FORGOT_PASSWORD'] = 'သင့္ လွ်ိဳ႕၀ွက္နံပါတ္ ေမ့ေနပါသလား?';
$lang['LOGIN_TEXT_OR'] = 'သို႔မဟုတ္';
$lang['LOGIN_TEXT_REGISTER'] = 'အေကာင့္သစ္ဖြင့္ရန္';

$lang['BREADCRUMBS_HOME'] = 'မူလေနရာ';
$lang['BREADCRUMBS_LOGIN'] = 'အေကာင့္သိုု႔၀င္ရန္';
$lang['LOGIN_MSG_INVALID'] = 'အေထာက္အထား မျပည့္စံုပါ!!';

//Get password page
$lang['LOST_PASS_HEADING'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အသစ္ ေျပာင္းမည္';
$lang['LOST_FIELD_PASS'] = ' လွ်ိဳ႕၀ွက္နံပါတ္';
$lang['LOST_FIELD_CON_PASS'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အတည္ျပဳပါ';
$lang['LOST_TEXT_SUBMIT_BTN'] = 'ေျပာင္းမည္';

$lang['CHANGE_PASS_INVALID_CODE'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ မွားေနပါသည္၊ ေနာက္တစ္ၾကိမ္ ထပ္လုပ္ပါ !';
$lang['BREADCRUMBS_CHANGE_PASS'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အသစ္ ေျပာင္းမည္';
$lang['CHANGE_PASS_SUCCESSFULLY'] = 'သင့္ လွ်ိဳ႕၀ွက္နံပါတ္ ေျပာင္းလဲျခင္း ေအာင္ျမင္ပါသည္';

//Forgot Password
$lang['FORGOT_PASS_HEADING'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ ေမ့ေနပါသလား';
$lang['FORGOT_EMAIL_FIELD'] = 'အီးေမးလ္';
$lang['FORGOT_SUBMIT_BUTTON'] = 'တင္သြင္းမည္';

$lang['BREADCRUMBS_FORGOT_PASS'] = 'လွ်ိဳ႕၀ွက္ကုဒ္ ေပ်ာက္ေနသည္';
$lang['FORGOT_PASS_INVALID_USER'] = 'အသံုးျပဳသူ သက္ေသခံ မဟုတ္ပါ';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL'] = 'လွ်ိဳ႕၀ွက္ကုဒ္ ေျပာင္းလဲျခင္း လင့္ အား သင့္အီးေမးလ္သို႔ပို႔လိုက္ပါျပီ၊ ေက်းဇူးျပဳ၍စစ္ၾကည့္ပါ';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL_ISSUE'] = 'အီးေမးလ္ ပို႔ျခင္းအဆင္မေျပပါ၊ ေနာက္တစ္ၾကိမ္ၾကိဳးစား ၾကည့္ပါ';

//Profile Page
$lang['MENU_MY_BOOKING'] = 'ကၽြႏု္ပ္၏ ဘြတ္ကင္';
$lang['MENU_LOGOUT'] = 'ထြက္မည္';
$lang['PROFILE_PAGE_HEADING'] = 'သင့္ကိုယ္ေရးကိုယ္တာ အခ်က္အလက္အား  ျပဳျပင္မည္';
$lang['PROFILE_FIELD_TITLE'] = 'အေၾကာင္းအရာ';
$lang['PROFILE_FIELD_TITLE_OPTION1'] = ' ဦး';
$lang['PROFILE_FIELD_TITLE_OPTION2'] = 'ေဒၚ';
$lang['PROFILE_FIELD_TITLE_OPTION3'] = 'ဒေါ်';
$lang['PROFILE_FIELD_FULL_NAME'] = 'နာမည္အျပည့္အစံု';
$lang['PROFILE_FIELD_COMPANY_NAME'] = 'ကုမၸဏီ နာမည္';
$lang['PROFILE_FIELD_POSITION'] = 'ရာထူး';
$lang['PROFILE_FIELD_EMAIL'] = 'အီးေမးလ္';
$lang['PROFILE_FIELD_PASSWORD'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ ';
$lang['PROFILE_FIELD_CON_PASSWORD'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အတည္ျပဳပါ';
$lang['PROFILE_FIELD_ADDRESS'] = 'လိပ္စာ';
$lang['PROFILE_FIELD_COUNTRY'] = ' ႏိုင္ငံ';
$lang['PROFILE_FIELD_TELEPHONE'] = 'တယ္လီဖုန္း နံပါတ္';
$lang['PROFILE_FIELD_PROFILE_IMAGE'] = 'ကိုယ္ေရးကိုယ္တာ ဓါတ္ပံု';
$lang['PROFILE_FIELD_EDIT_BTN'] = ' ျပဳျပင္မည္';
$lang['BREADCRUMBS_MY_PROFILE'] = 'မိမိ၏ စာမ်က္ႏွာ';
$lang['PROFILE_UPDATE_SUCCESSFULLY'] = ' ျပင္ဆင္ျခင္းအာင္ျမင္ပါသည္';

//Registration page
$lang['REGISTER_PAGE_HEADING'] = 'ယခုပဲ အေကာင့္ဖြင့္လိုုက္ပါ';
$lang['REGISTER_FIELD_SBT_BTN'] = 'အေကာင့္ဖြင္႔ရန္';

$lang['BREADCRUMBS_REGISTER'] = 'အေကာင့္ဖြင္႔ရန္';
$lang['REGISTER_INVALID_SUBMITION'] = 'တင္သြင္းျခင္း ပုံစံမွားေနသည္!!';
$lang['REGISTER_INVALID_PROFILE_IMG'] = 'ကိုယ္ေရးကိုယ္တာ ဓာတ္ပံု မွန္ကန္သည္';
$lang['REGISTER_SUCCESSFULL'] = 'မွတ္ပံုတင္ျခင္းေအာင္ျမင္ပါသည္၊ သင္ယခုခ်က္ျခင္း ၀င္ႏုိင္ပါျပီ!!';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS'] = 'ဤသည်ကိုအီးမေးလ်ပြီးသားကြှနျုပျတို့နှငျ့အတူမှတ်ပုံတင် !! ကျေးဇူးပြု';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS1'] = 'စကားဝှက်ကိုရ';
?>
