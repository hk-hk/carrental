<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//Login page
$lang['LOGIN_HEADING'] = 'Login';
$lang['LOGIN_EMAIL'] = 'Email';
$lang['LOGIN_PASSWORD'] = 'Password';
$lang['LOGIN_TEXT_BTN'] = 'Login';
$lang['LOGIN_TEXT_FORGOT_PASSWORD'] = 'Lost your password?';
$lang['LOGIN_TEXT_OR'] = 'or';
$lang['LOGIN_TEXT_REGISTER'] = 'Register';

$lang['BREADCRUMBS_HOME'] = 'Home';
$lang['BREADCRUMBS_LOGIN'] = 'Login';
$lang['LOGIN_MSG_INVALID'] = 'Invalid Credentials!!';

//Get password page
$lang['LOST_PASS_HEADING'] = 'Change New Password';
$lang['LOST_FIELD_PASS'] = 'Password';
$lang['LOST_FIELD_CON_PASS'] = 'Confirm Password';
$lang['LOST_TEXT_SUBMIT_BTN'] = 'change';

$lang['CHANGE_PASS_INVALID_CODE'] = 'This is not a valid code for change password. Please try again!';
$lang['BREADCRUMBS_CHANGE_PASS'] = 'Change New Password';
$lang['CHANGE_PASS_SUCCESSFULLY'] = 'Your Password has been changed sucessfully.';

//Forgot Password
$lang['FORGOT_PASS_HEADING'] = 'Lost Password';
$lang['FORGOT_EMAIL_FIELD'] = 'Email';
$lang['FORGOT_SUBMIT_BUTTON'] = 'Submit';

$lang['BREADCRUMBS_FORGOT_PASS'] = 'Lost Password';
$lang['FORGOT_PASS_INVALID_USER'] = 'This is not a valid User ID';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL'] = 'Change password link sent to your email. Please check it.';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL_ISSUE'] = 'Please try again.. Email sending have some issues.';

//Profile Page
$lang['MENU_MY_BOOKING'] = 'My Booking';
$lang['MENU_LOGOUT'] = 'Logout';
$lang['PROFILE_PAGE_HEADING'] = 'Update Profile';
$lang['PROFILE_FIELD_TITLE'] = 'Title';
$lang['PROFILE_FIELD_TITLE_OPTION1'] = 'Mr.';
$lang['PROFILE_FIELD_TITLE_OPTION2'] = 'Mrs.';
$lang['PROFILE_FIELD_TITLE_OPTION3'] = 'Ms.';
$lang['PROFILE_FIELD_FULL_NAME'] = 'Full Name';
$lang['PROFILE_FIELD_COMPANY_NAME'] = 'Company Name';
$lang['PROFILE_FIELD_POSITION'] = 'Title / Position';
$lang['PROFILE_FIELD_EMAIL'] = 'Email';
$lang['PROFILE_FIELD_PASSWORD'] = 'Password';
$lang['PROFILE_FIELD_CON_PASSWORD'] = 'Confirm Password';
$lang['PROFILE_FIELD_ADDRESS'] = 'Address';
$lang['PROFILE_FIELD_COUNTRY'] = 'Country';
$lang['PROFILE_FIELD_TELEPHONE'] = 'Telephone No.';
$lang['PROFILE_FIELD_PROFILE_IMAGE'] = 'Profile Image';
$lang['PROFILE_FIELD_EDIT_BTN'] = 'Update Profile';
$lang['BREADCRUMBS_MY_PROFILE'] = 'My Profile';
$lang['PROFILE_UPDATE_SUCCESSFULLY'] = 'Update Successfully.';

//Registration page
$lang['REGISTER_PAGE_HEADING'] = 'sign up now';
$lang['REGISTER_FIELD_SBT_BTN'] = 'Sign up';

$lang['BREADCRUMBS_REGISTER'] = 'Sign up';
$lang['REGISTER_INVALID_SUBMITION'] = 'Invalid Form Submition!!';
$lang['REGISTER_INVALID_PROFILE_IMG'] = 'Valid Profile Image';
$lang['REGISTER_SUCCESSFULL'] = 'Register Successfully. You can login now!!';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS'] = 'This email already registered with us!! Please ';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS1'] = 'Get Password';


