<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
This class handle the layouts
 */
class Transation extends MY_Controller {
  public function __construct() {
		// $this->isAuthorized();
        parent::__construct();
        $this->load->model('Transations', 'trans');
		// $this->session->set_userdata('active_language','mm');
		// $this->load->module("Layout");

		$this->load->model('Attributes/Attributes', 'attributes'); // Load User modal
        // $this->lang->load('Layout/layout', $this->getActiveLanguage());
	}
	public function tran_load($id){
		// print "<Pre/>";
		// print_r();

		$datas = $this->trans->get_joined_data($id);
		$row=array();
		foreach ($datas as $info) {

			$row['transaction_data_dd'] = ($info->transaction_date>0)?date('m-d-Y',$info->transaction_date) : '-';
            $row['category_dd'] = $this->attributes->get_by('id',$info->category_id)->title;
            $row['process_by_dd'] = $info->user_lname;
            $row['paymentto_dd'] = $info->payment_person;

            
            $paymentType=$this->attributes->get_by('id',$info->payment_type)->title;
            $row['description_dd'] = $info->description;
            
            $row['payment_type_dd'] = ($paymentType=="Deposit")? 'Deposit' : 'Withdraw';
            // $row['payment_type_dd'] = ($paymentType=="Deposit")? 'Deposit' : 'Withdraw';
            $row['amount_dd'] = number_format($info->amount);
            $row['files'] = $info->file_attachments;
            // $row['pmt_dd'] = ($paymentType=="Withdraw")? number_format($info->amount) : '-';


            // $row[] = number_format($info->final_balance);
		}
		echo json_encode($row);

	}
	
}
