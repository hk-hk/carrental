<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transations extends MY_Model {

	public $_table = 'transations_recs'; // database table
	public $primary_key   = 'id'; 
 	public function __construct() {

        parent::__construct();

    }

    public function get_many_by_order()
    {
        $where = func_get_args();
        $this->_set_where($where);

        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->_database->where($this->soft_delete_key, FALSE);
        }
        $this->_database->order_by('id','desc');

        return $this->get_all();
    }

    public function get_joined_data($id){
        $this->db->select('transations_recs.*');
        $this->db->select('ums_account.account_id');
        $this->db->select('ums_account.account_name');
        $this->db->select('ums_account.account_number');
        $this->db->select('cr_trip_expense.order_id');
        $this->db->select('cr_trip_expense.is_latest');
        $this->db->select('payment.pay_id');
        $this->db->select('payment.pay_price');
        $this->db->select('payment.pay_dollar');
        $this->db->select('payment.one_dollar');
        $this->db->select('ums_transfer.tran_id');
        $this->db->select('ums_transfer.transfer_amount');
        $this->db->select('ums_transfer.transfer_dollar');
        $this->db->select('cr_users.user_fname,');
        $this->db->select('cr_users.user_lname');

        $this->db->from('transations_recs');

        $this->db->join('ums_account','ums_account.account_id = transations_recs.account_id','LEFT');
        $this->db->join('ums_transfer','transations_recs.linked_id = ums_transfer.tran_id','LEFT');
        $this->db->join('payment','transations_recs.linked_id = payment.pay_id','LEFT');
        $this->db->join('cr_trip_expense','transations_recs.linked_id = cr_trip_expense.id','LEFT');
        $this->db->join('cr_users','cr_users.id = transations_recs.created_by','LEFT');
        $this->db->where('transations_recs.id',$id);
        $query = $this->db->get();   
        return $query->result();
    }

     public function get_by_category($order_id, $category_id){
        $this->db->select('transations_recs.*');
        $this->db->from('transations_recs');
        $this->db->where(array('linked_id' => $order_id, 'category_id' => $category_id));
        $result = $this->db->get()->result();
        return $result;

    }
}
