<?php
$route[CRM_VAR.'/attributes']     = 'Attributes/Admin/listAttributes';
$route[CRM_VAR.'/airport_location']     = 'Attributes/Admin/airport_location';
$route[CRM_VAR.'/airport_location_store']     = 'Attributes/Admin/airport_location_store';
$route[CRM_VAR.'/attributes/(:num)']     = 'Attributes/Admin/listAttributes/$1';
$route[CRM_VAR.'/attributes_list'] = 'Attributes/Admin/ajax_attribute_list';
$route[CRM_VAR.'/add_attr']      = 'Attributes/Admin/add_attr';
$route[CRM_VAR.'/add_attr/(:num)'] = 'Attributes/Admin/add_attr/$1';
$route[CRM_VAR.'/ajax_update_attr_status/(:any)/(:any)'] = 'Attributes/Admin/ajax_update_attr_status/$1/$2';
$route[CRM_VAR.'/ajax_delete_attr/(:any)'] = 'Attributes/Admin/ajax_delete_attr/$1';

$route[CRM_VAR.'/car_models']     = 'Attributes/Admin/list_child_Attributes/45';
$route[CRM_VAR.'/car_models/(:num)']     = 'Attributes/Admin/list_child_Attributes/45/$1';
$route[CRM_VAR.'/car_models_list'] = 'Attributes/Admin/ajax_attribute_child_list';
$route[CRM_VAR.'/add_car_model']      = 'Attributes/Admin/add_child_attr/45';
$route[CRM_VAR.'/edit_car_model/(:num)'] = 'Attributes/Admin/add_child_attr/45/$1';

$route[CRM_VAR.'/destination_list']     = 'Attributes/Admin/list_child_Attributes/70';
$route[CRM_VAR.'/add_destination']      = 'Attributes/Admin/add_child_attr/70';
$route[CRM_VAR.'/edit_destination/(:num)'] = 'Attributes/Admin/add_child_attr/70/$1';
$route[CRM_VAR.'/manage_distances/(:num)'] = 'Attributes/Admin/manage_distances/$1';
$route[CRM_VAR.'/fuel-prices'] = 'Attributes/Admin/manage_fuel_price';
