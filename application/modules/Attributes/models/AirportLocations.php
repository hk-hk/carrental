<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AirportLocations extends MY_Model {
	public $_table = 'airport_locations';

	public function __construct() {
		parent::__construct();
	}

	function get_located(){
		// SELECT cr_manage_attributes.* FROM `airport_locations` INNER JOIN cr_manage_attributes ON airport_locations.city_id=cr_manage_attributes.id
		$this->db->select("cr_manage_attributes.*");
        // $this->db->select("cr_manage_attributes.title as maker");

        $this->db->from('airport_locations');
        
        $this->db->join('cr_manage_attributes', '`airport_locations`.`city_id` = `cr_manage_attributes`.`id`','inner');
        $this->db->order_by('ordering','ASC');
        $query = $this->db->get();
        return $query->result();
        // $this->db->join('cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
        // $this->db->join('cr_manage_attributes`', '`cr_manage_attributes`.`id` = `cr_manage_cars`.`maker`','inner');
	} 
}