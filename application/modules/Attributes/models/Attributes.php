<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attributes extends MY_Model {

	public $_table = 'cr_manage_attributes'; // database table
	//public $before_create = array( 'timestamps' );
        public $column_order = array(null, 'title','short_desc','parent_id','ordering','status'); 
         //set column field database for datatable orderable

        public $column_search = array('title','parent_id'); 
         //set column field database for datatable searchable 
        public $order = array('id' => 'asc'); // default order

            //validations rules
            public $config = array(
            array(
                    'field' => 'title',
                    'label' => 'Attribute Title',
                    'rules' => 'required'
                )
            );

         public function __construct() {

            parent::__construct();

        }


        
        private function _get_attr_datatables_query($root_parent) // dynamic search query of. user.
        {
        //     echo 'hi';
        //     echo $root_parent;
        // exit();
            if($this->input->post('title'))
            {
                $this->db->like('title', $this->input->post('title'));
            }
            
            if($this->input->post('parent_id'))
            {
                $this->db->where('parent_id', $this->input->post('parent_id'));
            }
            if($root_parent>1)
                $this->db->where('parent_id', $root_parent);
            else
                $this->db->where('root_parent', 0);
            //$this->db->where_not_in('user_active', 'deleted');

            $this->db->from($this->_table);

            $i = 0;

            foreach ($this->column_search as $item) // loop column 
            {
                if(isset($_POST['search']['value']) && $_POST['search']['value']) // if datatable send POST for search
                {
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
            $this->db->where_not_in('status','2');
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
        
    function get_datatables($root_parent) 
    {
        $this->_get_attr_datatables_query($root_parent);

        if(isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //echo $this->db->last_query();die; 
        return $query->result();
    }

    function count_filtered($root_parent=0) // Get total row count of search query
    {
        $this->_get_attr_datatables_query($root_parent);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    function get_odering($parentid = 0) 
    {
        $this->db->from($this->_table);
        $this->db->select('ordering');
        $this->db->where('parent_id', $parentid);
        $this->db->order_by('ordering','DESC');
        $this->db->limit('1');
        $query = $this->db->get();    
        $fielval = $query->row();
        if(!empty($fielval))
            return $fielval->ordering+1;
        else
            return '1';
    }
    function get_fuel_price() 
    {
        $this->db->from('cr_car_type_fuel_price');
        $query = $this->db->get();    
        $prices = $query->result_array();
        $fuelPrices = array();
        if(is_array($prices) && count($prices)>0){
            foreach ($prices as $val){
                $fuelPrices[$val['car_type_id']] = $val;
            }
        }
        return $fuelPrices;
    }
    public function get_many_by_order()
    {
        $where = func_get_args();
        $this->_set_where($where);

        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->_database->where($this->soft_delete_key, FALSE);
        }
        $this->_database->order_by('title','asc');

        return $this->get_all();
    }
}
