<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Fuel & Toll Price based on Car Type</h3>
            <a style="float: right;" href="<?php echo site_url(CRM_VAR);?>">Back to Dashboard</a>
        </div>
          <?php echo form_open(CRM_VAR.'/fuel-prices/'); ?>
      <?php echo validation_errors();
      ?>
        <div class="box-body">
            <div class="row" style="margin-bottom: 5px;">
                <div class="form-group">
                    <label for="distances" class="col-sm-2 control-label">Car Type</label>
                    <div class="col-sm-4"> Fuel Price</div>
                    <div class="col-sm-4"> Toll Price</div>
                </div>
            </div>
            <?php $i=0; //print_r($all_destinations);die;
            foreach ($car_types as $car_type){ 
                $fuel_price = (isset($fuel_prices[$car_type['id']])?$fuel_prices[$car_type['id']]['fuel_price']:'');
                $toll_price = (isset($fuel_prices[$car_type['id']])?$fuel_prices[$car_type['id']]['toll_fee']:'');
                ?>
            <div class="row" style="margin-bottom: 5px;">
                <div class="form-group">
                    <label for="distances" class="col-sm-2 control-label"><?php echo $car_type['title'];?></label>
                    <div class="col-sm-4">
                        <?php if($fuel_price==''){ $fuelPrices = 'fuelPrices1';?>
                        <input type="hidden" name="fuelPrices1[<?php echo $i?>][car_type_id]" value='<?php echo $car_type['id']; ?>'>
                        <input type="number" step="0.5" class="form-control"  id="title" name="fuelPrices1[<?php echo $i?>][fuel_price]" placeholder="Fuel Price" value='<?php echo $fuel_price; ?>'>
                        <?php }else{ $fuelPrices = 'fuelPrices';?>
                          <input type="hidden" name="fuelPrices[<?php echo $i?>][car_type_id]" value='<?php echo $car_type['id']; ?>'>
                        <input type="number" step="0.5" class="form-control"  id="title" name="fuelPrices[<?php echo $i?>][fuel_price]" placeholder="Fuel Price" value='<?php echo $fuel_price; ?>'>
                        <?php } ?>
                    </div>
                    <div class="col-sm-4">
                        <input type="number" step="0.5" class="form-control"  id="title" name="<?php echo $fuelPrices;?>[<?php echo $i?>][toll_fee]" placeholder="Toll Price" value='<?php echo $toll_price; ?>'>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>
        </div>
        <div class="box-footer">
            <button type="submit" name="sbt_fuel_price" class="btn btn-info">Manage Fuel Price</button>     
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>