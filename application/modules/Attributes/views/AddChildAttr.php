<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      
      <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">
            <?php if($attrinfo->id>0) { echo $page_args['edit_heading']; }
            else { echo $page_args['add_heading']; } ?>
            </h3>
            <a style="float: right;" href="<?php echo site_url(CRM_VAR.'/'.$page_args['list_url'].'/'.$attrinfo->parent_id);?>">Back to list</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
			
            <?php $attributes = array('id' => 'addattr-form','class'=>'form-horizontal');
                if($attrinfo->id>0) { 
                    echo form_open_multipart(CRM_VAR.'/'.$page_args['edit_url'].'/'.$attrinfo->id,$attributes); }
                else { 
                    echo form_open_multipart(CRM_VAR.'/'.$page_args['add_url'],$attributes); 
                } ?>
	  
      <?php echo validation_errors();
      ?>
          <div class="box-body">
            <div class="form-group">
              <label for="firstname" class="col-sm-2 control-label"><?php echo $page_args['select_parent']?></label>
              <div class="col-sm-4">
                <select name="parent_id" class="form-control">
                  <?php
                    foreach($parent_attributes[$rootParent] as $val){
                        if($val['id']===$attrinfo->parent_id) { ?>
                            <option value="<?php echo $val['id']; ?>" selected="selected"><?php echo $val['title']; ?></option>
                        <?php }else{ ?>
                            <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                    <?php }
                    }
                    ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="firstname" class="col-sm-2 control-label"><?php echo $page_args['title_field']?></label>
              <div class="col-sm-4">
                  <input type="text" class="form-control" required="true" id="title" name="title" placeholder="Title" value='<?php echo $attrinfo->title; ?>'>
              </div>
            </div>
            <?php if(in_array($rootParent,array(45))){ ?>
                <div class="form-group">
                  <label for="lastname" class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-4">
                      <input type="file" accept="image/*" name="short_desc" />
                      <?php if($attrinfo->short_desc != ''){?><img height="100px" width="100px" src="<?php echo site_url();?>uploads/model/<?php echo $attrinfo->short_desc; ?>"><?php }?>
                      <input type="hidden" class="form-control" id="ordering" name="ordering" placeholder="Odering" value='<?php  echo $attrinfo->ordering; ?>'>
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ordering</label>
                    <div class="col-sm-4">
                      <input type="number" class="form-control" id="ordering" name="ordering" placeholder="Odering" value='<?php  echo $attrinfo->ordering; ?>'>
                    </div>
                </div>
            <?php }else{ ?>
                <div class="form-group">
                    <label for="lastname" class="col-sm-2 control-label"><?php echo $page_args['desc_field']?></label>
                    <div class="col-sm-4">
                        <?php if(in_array($rootParent,array(70))){ ?>
                        <input type="number" name="short_desc" id="short_desc" class="form-control" step="0.5" value="<?php echo $attrinfo->short_desc; ?>" />
                        <?php }else{ ?>
                        <textarea name="short_desc" id="short_desc" rows="5" cols="60"><?php echo $attrinfo->short_desc; ?></textarea>
                        <?php } ?>
                    </div>
                </div>    
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ordering</label>
                    <div class="col-sm-4">
                      <input type="number" class="form-control" id="ordering" name="ordering" placeholder="Odering" value='<?php  echo $attrinfo->ordering; ?>'>
                    </div>
                </div>
            <?php } ?>
            
            <div class="form-group">
              <label for="password" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-2">
                    <div class="checkbox" style="text-align: left;">
                    <label><?php //print_r($attrinfo);?>
                      <input type="radio" name="status" <?php if($attrinfo->status=='' || ($attrinfo->status == '1')){ echo 'Checked';} ?> value="1"> Publish
                      <input type="radio" name="status" <?php if(!empty($attrinfo) && ($attrinfo->status == '0')){ echo 'Checked';} ?> value="0"> Un Publish
                    </label>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <?php if($attrinfo->id>0) { ?> <button type="submit" class="btn btn-info"><?php echo $page_args['edit_button']?></button> <?php }
            else { ?> <button type="submit" class="btn btn-info"><?php echo $page_args['add_button']?></button> <?php } ?>           
          </div>
          <!-- /.box-footer -->
         <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#addattr-form").validate({
		  rules: {
					title: "required",
				    },
					/*user_state  : "required",
					user_city   : "required",*/
		  },
		  messages: {
			user_fname  : "Please enter Title",
		  },
		  errorClass: "my-error-class",
		  errorElement: "span", // default is 'label'
		  errorPlacement: function(error, element) {
		    error.insertAfter(element);
		  },
		});

    $(".choose-country").change(function() {
        var pcall = $(this);
        $.ajax({
          url:"<?php echo base_url('Tool/getStates'); ?>",
          data:{country:pcall.val()},
          type:'GET',
          dataType:"JSON",
          success:function( response ) {
            $.each(response.records,function(i,s) {
                $("#user_state").append("<option value='"+s.id_state+"'>"+s.name_state+"</option>");
            });
          }
        })
    });
	});
</script>
