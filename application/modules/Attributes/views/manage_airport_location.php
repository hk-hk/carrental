<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Attributes</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    <?php //print_r($parent_attributes);?>
          <div class="box-body">
          <h3 class="box-title">Filter</h3>
          <form id="form-filter" class="form-horizontal">
                   
                    <div class="form-group">
                        <label for="FirstName" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="parent_id" class="col-sm-2 control-label">Parent Attribute</label>
                        <div class="col-sm-4">
                          <select name="parent_id" id="parent_id" class="form-control">
                              <option value="0">Select</option>
                            <?php
                            foreach($parent_attributes[0] as $val){ 
                                if($val['id']==$parent_id){ ?>
                              <option value="<?php echo $val['id']; ?>" selected="selected"><?php echo $val['title']; ?></option>
                                <?php }else{ ?>
                                <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                                <?php } ?>
                            <?php }
                            ?>
                          </select>
                        </div>
                      </div>
     
                   
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </form>
                <?= form_open(CRM_VAR.'/airport_location_store') ?>
                <!-- <form method="post" action="<?= CRM_VAR . '/airport_location_store' ?>"> -->
                    
                    <div class="table-responsive">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>City</th>
                                <!-- <th>Description</th> -->
                                
                                <th>Check Airport Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($cities as $key => $city): ?>
                                <tr>
                                    <td><?= ($key+1) ?></td>
                                    <td><?= $city->title ?></td>
                                    <td><input type="checkbox" name="airport[]" value="<?= $city->id ?>" <?= (in_array($city->id, array_column($located, 'city_id'))? 'checked' : '') ?>/></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
             
                    </table>

                  </div>
                  <div class="row">
                      
                  <div class="col-md-12">
                      <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
                  </div>
                  </div>
                </form>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>

