<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">
            <?php if(!empty($attrinfo)) { echo 'Manage Distances'; }
            else { echo 'Manage Distances of '.$city_name; } ?>
            </h3>
            <a style="float: right;" href="<?php echo site_url(CRM_VAR.'/attributes/70');?>">Back to list</a>
        </div>
          <?php echo form_open(CRM_VAR.'/manage_distances/'.$city_id); ?>
      <?php echo validation_errors();
      ?>
        <div class="box-body">
            <?php $i=0; //print_r($all_destinations);die;
            foreach ($all_destinations as $k=>$val){ if($city_name==$val['title'])     continue;?>
            <div class="row" style="margin-bottom: 5px;">
                <div class="form-group">
                  <label for="distances" class="col-sm-2 control-label"><?php echo $val['title'];?></label>
                  <div class="col-sm-4">
                      <?php $key = array_search($val['title'],$is_destinations);
                      if($key){
                          $val['title'] = $existDestinations[$key]['title'];
                          $val['short_desc'] = $existDestinations[$key]['short_desc'];
                          ?>
                    <input type="hidden" name="existi[<?php echo $i;?>][id]" value='<?php echo $key; ?>' />
                    <input type="hidden" name="existi[<?php echo $i;?>][title]" value='<?php echo $val['title']; ?>' />
                    <input type="number" class="form-control"  id="title" name="existi[<?php echo $i;?>][short_desc]" placeholder="Disstance123" value='<?php echo $val['short_desc']; ?>'>
                      <?php }else{ ?>
                    <input type="hidden" name="dist[<?php echo $i;?>][parent_id]" value='<?php echo $city_id; ?>' />
                    <input type="hidden" name="dist[<?php echo $i;?>][title]" value='<?php echo $val['title']; ?>' />
                    <input type="number" class="form-control"  id="title" name="dist[<?php echo $i;?>][short_desc]" placeholder="Disstance" value='<?php echo $val['short_desc']; ?>'>
                      <?php } ?>
                    
                  </div>
                </div>
            </div>
            <?php $i++; } ?>
        </div>
        <div class="box-footer">
            <button type="submit" name="sbt_distance" class="btn btn-info">Manage Distances</button>     
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>