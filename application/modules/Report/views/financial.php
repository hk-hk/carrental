<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Financial Report</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="form-horizontal" >
          <div class="box-body">
            <!-- <h3 class="box-title">Filter</h3> -->
          
            <div class="row">
               
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Payment To/From</label>
                  <div class="col-md-8">

                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="payment_person" id="payment_person" class="form-control selectpicker"  data-live-search="true" data-show-subtext="false">
                      <option value="">All</option>
                      <optgroup label="Generals">
                        <?php foreach ($payment_persons as $k => $payment_person) { ?>
                          <option data-subtext="- Generals" value="<?= $payment_person->title; ?>"><?= $payment_person->title ?></option>
                        <?php } ?>
                      </optgroup>
                      <optgroup label="Customers">
                        <?php foreach ($customers as $k => $customer) { ?>
                          <option data-subtext="- Customers" value="<?= $customer->user_lname; ?>"><?= $customer->user_lname ?></option>
                        <?php } ?>
                      </optgroup>
                      <optgroup label="Suppliers">
                        <?php foreach ($suppliers as $k => $supplier) { ?>
                          <option data-subtext="- Suppliers" value="<?= $supplier->user_lname; ?>"><?= $supplier->user_lname ?></option>
                        <?php } ?>
                      </optgroup>
                      <optgroup label="Agents">
                        <?php foreach ($agents as $k => $agent) { ?>
                          <option data-subtext="- Agents" value="<?= $agent->user_lname; ?>"><?= $agent->user_lname ?></option>
                        <?php } ?>
                      </optgroup>
                      <optgroup label="Drivers">
                        <?php foreach ($drivers as $k => $driver) { ?>
                          <option data-subtext="- Drivers" value="<?= $driver->user_lname; ?>"><?= $driver->user_lname ?></option>
                        <?php } ?>
                      </optgroup>
                      <optgroup label="Gas Stations">
                        <?php foreach ($gas_tations as $k => $gas_tation) { ?>
                          <option data-subtext="- Gas Stations"  value="<?= $gas_tation->title; ?>"><?= $gas_tation->title ?></option>
                        <?php } ?>
                      </optgroup>
                       <optgroup label="Workshops">
                        <?php foreach ($workshops as $k => $workshop) { ?>
                          <option data-subtext="- Workshops" value="<?= $workshop->title; ?>"><?= $workshop->title ?></option>
                        <?php } ?>
                      </optgroup>
                      
                    </select>
                    <!-- <select class="selectpicker" data-live-search="true" >
  <optgroup label="Picnic">
    <option>Mustard</option>
    <option>Ketchup</option>
    <option>Relish</option>
  </optgroup>
  <optgroup label="Camping">
    <option>Tent</option>
    <option>Flashlight</option>
    <option>Toilet Paper</option>
  </optgroup>
</select>
 -->
                  </div>
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Processed By</label>
                    <div class="col-md-8">
                      <!-- <input type="text" class="form-control" id="car_tag"> -->
                      <select name="processed_by" id="processed_by" class="form-control">
                        <option value="">Select Process By</option>
                        <?php foreach ($processed_by as $k => $val) { ?>
                          <?php if ($val->user_role=="admin" || $val->user_role=="service_provider" || $val->user_role=="finance" || $val->user_role=="finance_admin"): ?>
                            <option value="<?php echo $val->id; ?>"><?php echo $val->user_lname ?></option>
                            
                          <?php endif ?>
                        <?php } ?>
                        
                      </select>
                    </div>
                  </div>
              </div>
            </div>
            <div class="row">
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Account</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="account_id" id="account_id" class="form-control">
                      <option value="">All</option>
                      <?php foreach ($accounts as $k => $account) { ?>
                        <option value="<?php echo $account->account_id; ?>"><?= $account->account_name ?> <?= $account->ums_bank ?></option>
                      <?php } ?>
                      
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Processed By (Flight)</label>
                    <div class="col-md-8">
                      <!-- <input type="text" class="form-control" id="car_tag"> -->
                      <select name="air_processed_by" id="air_processed_by" class="form-control">
                        <option value="">Select Process By</option>
                        <?php foreach ($air_processed_by as $k => $val) { ?>
                            <option value="<?php echo $val->id; ?>"><?php echo $val->name ?></option>
                            
                        <?php } ?>
                        
                      </select>
                    </div>
                  </div>
              </div>
              
            </div>
            <div class="row">
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Category</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="category_id" id="category_id" class="form-control selectpicker"  data-live-search="true" data-show-subtext="false">
                      <option value="0">All</option>
                      <?php foreach ($categories as $k => $category) { ?>
                      <option value="<?php echo $category->id; ?>"><?php echo $category->title ?></option>
                      <?php } ?>
                      
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2 control-label">Deposit / Withdraw</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="payment_type" id="payment_type" value="" checked=""> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="payment_type" id="payment_type" value="4417"> Deposit</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="payment_type" id="payment_type" value="4416"> Withdraw</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2 control-label">Transaction Date</label>
                  <!-- <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label> -->
                  <div class="col-md-3">
            
                    <input type="text" id="transaction_from_date" class="form-control datepicker">
                    <span>From Date</span>
                  </div>
            
                  <div class="col-md-3">
            
                    <input type="text" id="transaction_to_date" class="form-control datepicker">
                    <span>To Date</span>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  col-md-offset-1 control-label">Recieved From Vendor</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Yes</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> No</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  col-md-offset-1 control-label">Paid To Workshop</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Yes</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> No</label>
                  
                </div>
              </div>
            </div> -->
            
            <!-- <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  col-md-offset-2 control-label">Date Range</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Previous Month</label>
                  
                </div>
              </div>
            </div> -->
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  control-label">Processed / Created Date</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cmonth" name="date_type" id="date_type" checked=""> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pmonth" name="date_type" id="date_type"> Previous Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cday" name="date_type" id="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pday" name="date_type" id="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="all" name="date_type" id="date_type"> All</label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3 col-md-offset-1 control-label"></label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label>
                  <div class="col-md-3">
            
                    <input type="text" id="from_date" class="form-control datepicker fromdate">
                    <span>From Date</span>
                  </div>
            
                  <div class="col-md-3">
            
                    <input type="text" id="to_date" class="form-control datepicker fromdate">
                    <span>To Date</span>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          
        </div>
        <div class="box-footer">
          <button id="btn-search" type="submit" class="btn btn-info pull-right" >Search</button>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Current Available Balance - MMK Total: USD Total: </h3>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
           <table cellspacing="0" width="100%">
              <tbody>

                <tr>
                  <?php foreach ($accounts as $key => $value): ?>
                    <?php if ($value->account_type==0): ?>
                      
                    <td>
                        <table  class="table table-bordered">
                          <tbody>
                            <tr class="text-center">
                              <td colspan="2"><?= $value->account_name ?></td>
                            </tr>
                            <tr>
                              <td>MMK</td>
                              <td><?= $value->account_price? '<font color="green">'.$value->account_price.'</font>' : '<font color="red">No Balance</font>' ?></td>
                            </tr>
                            <tr>
                              <td>USD</td>
                              <td><?= $value->account_dollar? '<font color="green">'.$value->account_dollar.'</font>' : '<font color="red">No Balance</font>' ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    <?php endif ?>
                  <?php endforeach ?>
                  <?php foreach ($banklist as $key => $value): ?>
                      <td>
                        <table  class="table table-bordered">
                          <tbody>
                            <tr class="text-center">
                              <td colspan="2"><?= $key ?></td>
                            </tr>
                            <tr>
                              <td>MMK</td>
                              <td><?= $value['mmk']? '<font color="green">'.number_format($value['mmk']).'</font>' : '<font color="red">No Balance</font>' ?></td>
                            </tr>
                            <tr>
                              <td>USD</td>
                              <td><?= $value['usd']? '<font color="green">'.number_format($value['usd']).'</font>' : '<font color="red">No Balance</font>' ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                  <?php endforeach ?>


                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
            <div class="col-md-8">
            </div>
            <div class="col-md-4 text-right">
              <table class="table table-hover">
                <tr>
                  <td></td>
                  <td>MMK</td>
                  <td>USD</td>
                </tr>
                <tr>
                  <td>Deposit</td>
                  <td id="depositAmountMMK">0</td>
                  <td id="depositAmountUSD">0</td>
                </tr>
                <tr>
                  <td>Withdraw</td>
                  <td id="withdrawAmountMMK">0</td>
                  <td id="withdrawAmountUSD">0</td>
                </tr>
              </table>
            </div>
          </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Financial Report</h3>
          <div class="pull-right"><a href="#" id="btn-print">PDF</a> | <a href="#" id="btn-export">Excel</a></div>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
           <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <!-- <th>No</th> -->
                  <th>Processed At</th>
                  <th>Transaction Date</th>
                  <th>Category</th>
                  <th>Description</th>
                  <th>Process By</th>
                  <th>Account</th>
                  <th>Payment To/From</th>
                  <!-- th -->
                  <th>Deposit</th>
                  <th>Withdraw</th>
                  <th >Final Balance</th>
                  <!-- <th>Cash MMK Balance</th> -->
                  <!-- <th>Cash $ Balance</th> -->
                  
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
              
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.box -->
  </div>
  <!--/.col (left) -->
</div>
<!-- /.row -->
</section>

<?php if(TRUE){?>
<script type="text/javascript">
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/financial_report_list')?>",
            "type": "POST",
            "data": function(data){
                data.payment_type= $('#payment_type:checked').val();
                data.payment_person= $('#payment_person').val();
                data.account= $('#account_id').val();
                data.category= $('#category_id').val();
                data.processed_by= $('#processed_by').val();
                data.air_processed_by= $('#air_processed_by').val();
                data.transaction_from_date = $('#transaction_from_date').val();
                data.transaction_to_date = $('#transaction_to_date').val();
                var dateType=$('#date_type:checked').val();

                var fromto;
                data.date_type = dateType;
                if(dateType=='from_to'){
                  data.from_date=$("#from_date").val();
                  data.to_date=$("#to_date").val();
                  // data.fromto = {from:from_date, to:to_date};
                } 
                // console.log(data);
                // data.no_of_passanger = $('#no_of_passanger').val();
            },
            "dataSrc": function(res){
              console.log(res);
               var count = res.data.length;
               $("#depositAmountMMK").html(res.depositAmountMMK);
               $("#withdrawAmountMMK").html(res.withdrawAmountMMK);
               $("#depositAmountUSD").html(res.depositAmountUSD);
               $("#withdrawAmountUSD").html(res.withdrawAmountUSD);
               // depositAmount
               // alert(res.withdrawAmount+" - "+res.depositAmount);

               return res.data;
           }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,8 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        {
              "targets": [8,7,6],
              "className": "text-right",
         }
        ],
        // "initComplete": function(settings, json){ 
        //      var info = this.api().page.info();
        //      // alert('Total records', info.recordsTotal);
        //  }
    });
    $('#car_tag').change(function(){
      // console.log(this.val());
      $( "#car_tag option:selected" ).each(function() {
        // console.log($(this).val());
        $.post("<?php echo site_url(CRM_VAR.'/car_detail')?>",{'car_tag':$(this).val()},function(data){
          // console.log(data);
          var obj=$.parseJSON(data);
          $.each(obj,function(k,v){
            // console.log(k);
            $('#vendor').val(v[1]);
            $('#car_type').val(v[2]);
            $('#car_owner').val(v[3]);
          });
        });
      });
    });
    $('input[name=date_type]').change(function(){
      if(this.value!="from_to"){
        $("#from_date").val("");
        $("#to_date").val("");
      }
      // console.log();
    });
    $('.datepicker').datepicker({
        autoclose: true
    });
    $('.fromdate').focus(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('.fromdate').focusout(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    

    $('#btn-search').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
});
  serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}
   function requestReport(){

      var requestData={
        payment_type:$('#payment_type:checked').val(),
        payment_person:$('#payment_person').val(),
        account:$('#account_id').val(),
        category:$('#category_id').val(),
        transaction_from_date : $('#transaction_from_date').val(),
        transaction_to_date : $('#transaction_to_date').val(),
        processed_by:$('#processed_by').val(),
        date_type:$('#date_type:checked').val(),
        from_date:$("#from_date").val(),
        to_date:$("#to_date").val(),
      };
      
      return serialize(requestData);
    }
    $('#btn-export').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/financial_report_output')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });
    $('#btn-print').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/financial_report_print')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });
</script>
<?php } ?>