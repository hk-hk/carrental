<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Car Detail</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="form-horizontal" >
          <div class="box-body">
            <!-- <h3 class="box-title">Filter</h3> -->
            <div class="row">
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="vendor" id="vendor" class="form-control selectpicker" data-live-search="true">
                      <option value="">Select Vendor</option>
                      <?php
                      foreach ($suppliers as $option){
                      $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                      
                      <option value="<?php echo $option->id?>"><?php echo $optionText ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Driver</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="driver"> -->
                    <select name="driver" id="driver" class="form-control selectpicker" data-live-search="true">
                      <option value="">Select Driver</option>
                      <?php
                      foreach ($drivers as $option){
                      // $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                      
                      <option value="<?php echo $option->user_lname ?>"><?php echo $option->user_lname ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Car Tag</label>
                  <div class="col-md-8">
                    <select name="car_tag" id="car_tag" class="form-control selectpicker" data-live-search="true">
                      <option value="">Select Car</option>
                      <?php
                      foreach($car_tags as $val){ ?>
                      <option value="<?php echo $val->car_id; ?>"><?php echo $val->car_tag; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
               
              <div class="col-md-6">
                <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Payment Status</label>
                    <div class="col-md-8">
                      <select id="payment_status" class="form-control" required="">
                        <option value="">Select Payment Status</option>
                        <option value="pending">Pending</option>
                        <option value="complete">Complete</option>
                        
                      </select>
                      <!-- <input type="text" class="form-control" name="shop_name" required="" value="<?= !empty($oil_detail->shop_name)? $oil_detail->shop_name : '' ?>"> -->
                    </div>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Gas Station</label>
                    <div class="col-md-8">
                      <select id="gas_station" class="form-control" required="">
                        <option value="">Select Gas Station</option>
                        <?php foreach ($gas_tations as $key => $value): ?>
                          <?php if (!empty($oil_detail) && $value->title==$oil_detail->shop_name): ?>
                            
                            <option value="<?= $value->title ?>" selected><?= $value->title ?></option>
                          <?php else: ?>
                            <option value="<?= $value->title ?>"><?= $value->title ?></option>
                          <?php endif ?>
                        <?php endforeach ?>
                      </select>
                      <!-- <input type="text" class="form-control" name="shop_name" required="" value="<?= !empty($oil_detail->shop_name)? $oil_detail->shop_name : '' ?>"> -->
                    </div>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  col-md-offset-1 control-label">Payment Type</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="" id="payment_type" name="payment_type" checked=""> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="cash" id="payment_type" name="payment_type"> Cash</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="credit" id="payment_type" name="payment_type"> Credit</label>
                  
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  control-label">Date Range</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="" name="date_type" id="date_type" checked=""> All</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cday" name="date_type" id="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pday" name="date_type" id="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cmonth" name="date_type" id="date_type"> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pmonth" name="date_type" id="date_type"> Previous Month</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3 col-md-offset-1 control-label"></label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label>
                  <div class="col-md-3">
                    <!-- <label class="control-label"><input type="radio" class="" name="date_type"> Current Day</label> -->
                    <input type="text" id="from_date" class="form-control datepicker">
                    <span>From Date</span>
                  </div>
                  <!-- <span>-</span> -->
                  <div class="col-md-3">
                    <!-- <label class="control-label"><input type="radio" class="" name="date_type"> Current Day</label> -->
                    <input type="text" id="to_date" class="form-control datepicker">
                    <span>To Date</span>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          
        </div>
        <div class="box-footer">
          <button type="button" class="btn btn-info pull-right" id="btn-search">Search</button>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Fuel</h3>
          <div class="pull-right"><a href="#" id="btn-print">Print</a> | <a href="#" id="btn-export">Export</a></div>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
            <!-- <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Gas Station</th>
                  <th>Payment Type</th>
                  <th>Status</th>
                  <th>Type Of Use</th>
                  <th>Date</th>
                  <th>Liter</th>
                  <th>Amount</th>
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
              
              
            </table> -->
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Vendor</th>
                  <th>Car Tag</th>
                  <th>Shop Name</th>
                  <th>Car Operator Name</th>
                  <th>Payment Type</th>
                  <th>Status</th>
                  <th>Type Of Use</th>
                  <th>Date</th>
                  <th>Liter</th>
                  <th>Description</th>
                  <th>Comments</th>
                  <th>Amount</th>
                  <!-- <th>Action</th> -->

                </tr>
              </thead>
              <tbody>
              </tbody>
              
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.box -->
  </div>
  <!--/.col (left) -->
</div>
<!-- /.row -->
</section>
<?php if(TRUE){?>
<script type="text/javascript">
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/fuel_report_list')?>",
            "type": "POST",
            "data": function ( data ) {         
                data.vendor = $('#vendor').val();
                data.car_tag = $('#car_tag').val();
                data.driver = $('#driver').val();
                data.gas_station = $('#gas_station').val();
                data.payment_type = $('#payment_type:checked').val();
                data.payment_status = $('#payment_status').val();
                
                var dateType=$('#date_type:checked').val();
                var fromto;
                data.date_type = dateType;
                if(dateType=='from_to'){
                  data.from_date=$("#from_date").val();
                  data.to_date=$("#to_date").val();
                  // data.fromto = {from:from_date, to:to_date};
                }
            }
        },
        //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,11 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        {
              "targets": [6,5],
              "className": "text-right",
         }
        ],
 
    });

    $('input[name=date_type]').change(function(){
      if(this.value!="from_to"){
        $("#from_date").val("");
        $("#to_date").val("");
      }
      // console.log();
    });
    $('.datepicker').datepicker({
        autoclose: true
    });
    $('.datepicker').focus(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('.datepicker').focusout(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('#btn-search').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#driver').keyup(function(){
      // load();
      table.ajax.reload(null,false);
    });
    $('#gas_station').keyup(function(){
      table.ajax.reload(null,false);
    });
});
  serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}
   function requestReport(){
      // car_tag = $('#car_tag').val();
      // driver = $('#driver').val();
      // gas_station = $('#gas_station').val();
      // payment_type = $('#payment_type:checked').val();
      // payment_status = $('#payment_status').val();
      // var dateType=($('#date_type:checked').val())? $('#date_type:checked').val() : '';
      // var fromto='';

      // date_type = dateType;
      // if(dateType=='from_to'){
      //   var from_date=($("#from_date").val())? $("#from_date").val() : '';
      //   var to_date=($("#to_date").val())? $("#to_date").val() : '';
      //   // fromto = {from:from_date, to:to_date};
      // }
// car_tag:car_tag,
//         driver:driver,
//         payment_status:payment_status,
//         gas_station:gas_station,
//         payment_type:payment_type,
//         date_type:date_type,
      requestData={
        car_tag : $('#car_tag').val(),
        driver : $('#driver').val(),
        gas_station : $('#gas_station').val(),
        payment_type : $('#payment_type:checked').val(),
        payment_status : $('#payment_status').val(),
        vendor : $('#vendor').val(),
        date_type:$('#date_type:checked').val(),
        from_date:$("#from_date").val(),
        to_date:$("#to_date").val(),
      }
      return serialize(requestData);
    }
    $('#btn-export').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/fuel_report_output')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });
    $('#btn-print').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/fuel_report_print')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });
</script>
<?php } ?>
