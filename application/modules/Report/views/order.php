<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Order Report</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="form-horizontal" >
          <div class="box-body">
            <!-- <h3 class="box-title">Filter</h3> -->
            <div class="row">
              <div class=" col-md-12">
                  <div class="form-group">
                      <label for="LastName" class="col-sm-2 control-label">Order ID</label>
                      <div class="col-sm-10">
                          <input type="text" id="order_id" class="form-control" placeholder="Order ID">
                      </div>
                  </div>
              </div>
            </div>
            <div class="row">
              
              
              <div class="col-md-6">

                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Type</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="rent_type" id="rent_type" class="form-control">
                      <option value="">Select Type</option>
                      <!-- <option value="full-day">Daily</option>
                      <option value="half-day">Daily - Half Day</option> -->
                      <option value="36">Daily</option>
                      <option value="37">Weekly</option>
                      <option value="38">Monthly</option>
                      
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Driver</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="driver"> -->
                    <select name="driver" id="driver" class="form-control selectpicker" data-live-search="true">
                      <option value="">Select Driver</option>
                      <?php
                      foreach ($drivers as $option){
                      // $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                      
                      <option value="<?php echo $option->user_lname ?>"><?php echo $option->user_lname ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Client</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="client" id="client" class="form-control selectpicker" data-live-search="true">
                      <option value="">All</option>
                      <optgroup label="Agents">
                        <?php foreach ($agents as $k => $customer): ?>
                          <option data-subtext="- Generals" value="<?php echo $customer->user_lname; ?>"><?php echo $customer->user_lname ?></option>
                        <?php endforeach ?>
                      <optgroup label="Customers">
                        <?php foreach ($customers as $k => $customer): ?>
                          <option data-subtext="- Customers" value="<?php echo $customer->user_lname; ?>"><?php echo $customer->user_lname ?></option>
                        <?php endforeach ?>
                      </optgroup>
                      </optgroup>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="vendor" id="vendor" class="form-control selectpicker" data-live-search="true">
                      <option value="">Select Vendor</option>
                      <option value="only-vendor">Only Vendor</option>
                      <?php
                      foreach ($suppliers as $option){
                      $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                      
                      <option value="<?php echo $option->id?>"><?php echo $optionText ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Order Status</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="order_status" id="order_status" class="form-control">
                      <option value="0">All</option>
                      <option value="NotCancelled">Not Cancelled</option>
                      <option value="Pending">Pending</option>
                      <option value="Confirmed">Confirmed</option>
                      <option value="Completed">Completed</option>
                      <option value="Cancelled">Cancelled</option>
                      
                      
                    </select>
                  </div>
                </div>
              </div>
            <!-- </div>
            <div class="row"> -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Customer Status</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="customer_status" id="customer_status" class="form-control">
                      <option value="">All</option>
                      <option value="pending">Pending</option>
                      <!-- <option value="0">Confirmed</option> -->
                      <option value="complete">Completed</option>
                      <!-- <option value="0">Cancelled</option> -->
                      
                      
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Vendor Status</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="vendor_status" id="vendor_status" class="form-control">
                      <option value="">All</option>
                      <option value="pending">Pending</option>
                      <!-- <option value="0">Confirmed</option> -->
                      <option value="complete">Completed</option>
                      <!-- <option value="0">Cancelled</option> -->
                      
                      
                    </select>
                  </div>
                </div>
              </div>
            <!-- </div>

            <div class="row"> -->
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Agent Status</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="agent_status" id="agent_status" class="form-control">
                      <option value="0">All</option>
                      <option value="pending">Pending</option>
                      <!-- <option value="0">Confirmed</option> -->
                      <option value="complete">Completed</option>
                      <!-- <option value="0">Cancelled</option> -->
                      
                      
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
             
              <div class="col-md-6" id="trip_type_div">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Trip Type</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="trip_type" id="trip_type" class="form-control" >
                      <option value="">Select Option</option>
                      <option value="pickup">Airport Pickup</option>
                      <option value="dropoff">Airport Dropoff</option>
                      <option value="incity">Incity</option>
                      <option value="highway">Highway</option>

                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6" id="daily_status_div">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Daily Status</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="daily_status" id="daily_status" class="form-control" >
                      <option value="">Select Option</option>
                      <option value="full-day">Full-day</option>
                      <option value="half-day">Half-day</option>

                    </select>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  col-md-offset-1 control-label">Recieved From Vendor</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Yes</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> No</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  col-md-offset-1 control-label">Paid To Workshop</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Yes</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> No</label>
                  
                </div>
              </div>
            </div> -->
            
            <!-- <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  col-md-offset-2 control-label">Date Range</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Previous Month</label>
                  
                </div>
              </div>
            </div> -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Driver From</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="driver_from" id="driver_from" class="form-control">
                      <option value="">All</option>
                      
                      <!-- <option value="0">Confirmed</option> -->
                      <option value="1">Sonic Star</option>
                      <option value="2">Vendor</option>
                      <!-- <option value="0">Cancelled</option> -->
                      
                      
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3 col-md-offset-1 control-label">Trip Start Date</label>
                  <!-- <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label> -->
                  <div class="col-md-3">
            
                    <input type="text" id="pickup_date_from" class="form-control datepicker" placeholder="From Date">
                    <!-- <span></span> -->
                  </div>
            
                  <div class="col-md-3">
            
                    <input type="text" id="pickup_date_to" class="form-control datepicker" placeholder="To Date">
                    <!-- <span></span> -->
                  </div>
                </div>
              </div>
            <!-- </div>
            <div class="row"> -->
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3 col-md-offset-1 control-label">Trip End Date</label>
                  <!-- <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label> -->
                  <div class="col-md-3">
            
                    <input type="text" id="dropoff_date_from" class="form-control datepicker" placeholder="From Date">
                    <!-- <span>From Date</span> -->
                  </div>
            
                  <div class="col-md-3">
            
                    <input type="text" id="dropoff_date_to" class="form-control datepicker" placeholder="To Date">
                    <!-- <span>To Date</span> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  control-label">Order Created Date</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cmonth" name="date_type" id="date_type" checked=""> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pmonth" name="date_type" id="date_type"> Previous Month</label>
                  <!-- <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="" name="date_type" id="date_type" checked=""> All</label> -->
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cday" name="date_type" id="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pday" name="date_type" id="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="all" name="date_type" id="date_type"> All</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-3 col-md-offset-1 control-label"></label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label>
                  <div class="col-md-3">
            
                    <input type="text" id="from_date" class="form-control datepicker from_to_picker" placeholder="From Date">
                    <!-- <span>From Date</span> -->
                  </div>
            
                  <div class="col-md-3">
            
                    <input type="text" id="to_date" class="form-control datepicker from_to_picker" placeholder="To Date">
                    <!-- <span>To Date</span> -->
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
          
        </div>
        <div class="box-footer">
          <button id="btn-show-all-children" class="btn btn-info pull-left" type="button">View All</button>
          <button id="btn-search" type="button" class="btn btn-info pull-right">Search</button>
          <a href="#" style="display: none" id="download-btn">Download Btn</a>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title pull-left">List</h3>
          <div class="pull-right"><a href="#" id="btn-vprint">Vendor Report</a> | <a href="#" id="btn-print">Print</a> | <a href="#" id="btn-export">Export</a></div>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Serial No</th>
                  <th>Trip Record Name</th>
                  <th>Status</th>
                  <th>Supplier Name</th>
                  <th>Trip Type</th>
                  <th>Client</th>
                  <th>Car Info</th>
                  <th>Driver</th>
                  <th>Driver Cost</th>
                  <th>Trip Start Date</th>
                  <th>Trip End Date</th>
                  <th>Fees</th>
                  <!-- <th>Total Expenses</th> -->
                  <!-- <th>Profit</th> -->
                  
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
              <!-- <tfoot>
              <tr>
                <th colspan="9"></th>
                <th>Amount</th>
                <th>Amount</th>
                <th>Amount</th>
                
              </tr>
              </tfoot> -->
              
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.box -->
  </div>
  <!--/.col (left) -->

<!-- /.row -->
</section>
<?php if(TRUE){?>
<script type="text/javascript">
  function numberWithCommas(number) {
      var parts = number.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
  }
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
        // bLengthChange: false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/order_report_list')?>",
            "type": "POST",
            "data": function(data){
                // data.car_id = $('#car_tag').val();            
                // data.title = $('#title').val();     
                data.order_id = $('#order_id').val();
                data.order_status = $('#order_status').val();
                data.customer_status = $('#customer_status').val();
                data.vendor_status = $('#vendor_status').val();
                data.agent_status = $('#agent_status').val();
                data.driver_from = $('#driver_from').val();
                data.driver = $('#driver').val();
                data.rent_type = $('#rent_type').val();
                data.trip_type = $('#trip_type').val();
                data.daily_status = $('#daily_status').val();
                data.client = $('#client').val();            
                data.vendor = $('#vendor').val();     

                data.pickup_date_from = $('#pickup_date_from').val();
                data.pickup_date_to = $('#pickup_date_to').val();
                data.dropoff_date_from = $('#dropoff_date_from').val();
                data.dropoff_date_to = $('#dropoff_date_to').val();

                var dateType=$('#date_type:checked').val();
                var fromto;
                data.date_type = dateType;
                if(dateType=='from_to'){
                  data.from_date=$("#from_date").val();
                  data.to_date=$("#to_date").val();
                  // data.fromto = {from:from_date, to:to_date};
                }
                // data.car_type = $('#car_type').val();            
                // data.rent_type = $('#rent_type').val();            
                // data.maker = $('#maker').val();     
                // console.log(data);       
                // data.no_of_passanger = $('#no_of_passanger').val();
            }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,11 ], //first column / numbering column
            "orderable": true, //set not orderable
            "className": "details-control"
        },
        {
              "targets": [11,10,9],
              "className": "text-right",
         }
        ]
        // "footerCallback": function ( row, data, start, end, display ) {
        //     var api = this.api(), data;
 
        //     // Remove the formatting to get integer data for summation
        //     var intVal = function ( i ) {
        //         return typeof i === 'string' ?
        //             i.replace(/[\$,]/g, '')*1 :
        //             typeof i === 'number' ?
        //                 i : 0;
        //     };
 
        //     // Total over all pages
        //     total = api
        //         .column( 11 )
        //         .data()
        //         .reduce( function (a, b) {
        //           console.log(intVal(a) + intVal(b));
        //             return intVal(a) + intVal(b);
        //         }, 0 );

        //     total2 = api
        //         .column( 10 )
        //         .data()
        //         .reduce( function (a, b) {
        //           console.log(intVal(a) + intVal(b));
        //             return intVal(a) + intVal(b);
        //         }, 0 );
        //       total3 = api
        //         .column( 9 )
        //         .data()
        //         .reduce( function (a, b) {
        //           console.log(intVal(a) + intVal(b));
        //             return intVal(a) + intVal(b);
        //         }, 0 );
 
        //     // Total over this page
            
        //     // Update footer
        //     $( api.column( 11 ).footer() ).html(
        //        numberWithCommas(total)
        //     );
        //     $( api.column( 10 ).footer() ).html(
        //       numberWithCommas(total2)
        //     );
        //     $( api.column( 9 ).footer() ).html(
        //        numberWithCommas(total3)
        //     );
        // },
 
    });
    // Add event listener for opening and closing details
    $('#table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
    function format ( d ) {
      // console.log(d);
        // `d` is the original data object for the row
        var html= '<table class="table table-bordered" style="padding-left:50px;">'+
            '<tr class="info">';
              html +='<td>Customer Payment : <br/>'+d[12]+'</td>';
                  // html +='<td>Agent Payment : <br/>'+d[13]+'</td>';   
                if(d[2]!='Mr. Sonic Star'){
                  html +='<td>Fuel : <br/>'+d[13]+'</td>';
                }

                if(d[17]=="is_agent"){
                  html +='<td>Agent Payment : <br/>'+d[14]+'</td>';
                }

                if(d[2]=='Mr. Sonic Star'){
                  html +='<td>Fuel Expense : <br/>'+d[15]+'</td>';
                }

            html +='<td>Toll Expenses : <br/>'+d[16]+'</td>';
            html +='<td>Other Expenses : <br/>'+d[17]+'</td>';
            if((d[21]==36 && d[2]=='Mr. Sonic Star') || d[21]==38 || d[21]==37){
              html +='<td>Driver Expenses : <br/>'+d[22]+'</td>';
            }
            html +='<td>Net Income : <br/>'+d[23]+'</td>';
            if(d[20]=='Completed'){
              html +='<td>Profit : <br/>'+d[19]+'</td>';
            }else{
              html +='<td>Est Profit : <br/>'+d[19]+'</td>';
            }
            if(d[24]){
              html +='<td>Venrdor Est Pay : <br/>'+d[24]+'</td>';
            }
            html +'</tr>';
        '</table>';

        return html;
    }
    $('#car_tag').change(function(){
      // console.log(this.val());
      $( "#car_tag option:selected" ).each(function() {
        // console.log($(this).val());
        $.post("<?php echo site_url(CRM_VAR.'/car_detail')?>",{'car_tag':$(this).val()},function(data){
          // console.log(data);
          var obj=$.parseJSON(data);
          $.each(obj,function(k,v){
            // console.log(k);
            $('#vendor').val(v[1]);
            $('#car_type').val(v[2]);
            $('#car_owner').val(v[3]);
          });
        });
      });
    });
    $('input[name=date_type]').change(function(){
      if(this.value!="from_to"){
        $("#from_date").val("");
        $("#to_date").val("");
        // $('#pickup_date_from').val("");
        // $('#pickup_date_to').val("");
        // $('#dropoff_date_from').val("");
        // $('#dropoff_date_to').val("");
      }
      // console.log();
    });
    $('.datepicker').datepicker({
        autoclose: true
    });
    $('.from_to_picker').focus(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('.from_to_picker').focusout(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    

    $('#btn-search').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });

    // Handle click on "Expand All" button
    $('#btn-show-all-children').on('click', function(){
        // Expand row details
        table.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
    });
    



    // Handle click on "Collapse All" button
    $('#btn-hide-all-children').on('click', function(){
        // Collapse row details
        table.rows('.parent').nodes().to$().find('td:first-child').trigger('click');
    });

    function byRentType(rent_type) {
        if(rent_type == 36){
          $(".rent36").show();
          $("#trip_type_div").show();
          
        }else{
          $(".rent36").hide();
          $("#trip_type_div").hide();
          $("#trip_type").val("");
        }
        //console.log(rent_type); 
      }
      function byTripType(trip_type){
        if(trip_type=="incity"){
          $("#daily_status_div").show();
        }else{
          $("#daily_status_div").hide();
          $("#daily_status").val("");
        }
      }

      var rent_type = $("#rent_type").val();
      byRentType(rent_type);
      $("#daily_status_div").hide();
      $("#rent_type").change(function(){
        byRentType($(this).val());
      });

      var trip_type = $("#trip_type").val();
      byTripType(trip_type);
      $("#trip_type").change(function(){
        byTripType($(this).val());
        
      });
});
  serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}
   function requestReport(){
      // driver : $('#driver').val(),
      // trip_type : $('#trip_type').val(),
      // client : $('#client').val(),
      // vendor : $('#vendor').val(),
      // var dateType=$('#date_type:checked').val();
      // var fromto;
      // date_type = dateType;
      // if(dateType=='from_to'){
      //   from_date=$("#from_date").val();
      //   to_date=$("#to_date").val();
      //   // data.fromto = {from:from_date, to:to_date};
      // }
       requestData={
        order_id : $('#order_id').val(),
        order_status : $('#order_status').val(),
        customer_status : $('#customer_status').val(),
        vendor_status : $('#vendor_status').val(),
        agent_status : $('#agent_status').val(),
        driver : $('#driver').val(),
        rent_type : $('#rent_type').val(),
        trip_type : $('#trip_type').val(),
        daily_status : $('#daily_status').val(),
        client : $('#client').val(),
        vendor : $('#vendor').val(),
        pickup_date_from : $('#pickup_date_from').val(),
        pickup_date_to : $('#pickup_date_to').val(),
        dropoff_date_from : $('#dropoff_date_from').val(),
        dropoff_date_to : $('#dropoff_date_to').val(),
        date_type:$('#date_type:checked').val(),
        from_date:$("#from_date").val(),
        to_date:$("#to_date").val(),
        driver_from:$('#driver_from').val(),
      }
      return serialize(requestData);
    }
    $('#btn-export').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/order_report_output')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });
    $('#btn-print').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
        $('#download-btn').attr('onclick','');
        $('#download-btn').attr('onclick',"MyWindow=window.open('<?php echo site_url(CRM_VAR.'/order_report_print')?>?"+requestReport()+"','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=900,height=600'); return false;");
        $('#download-btn').get(0).click();
    });

    $('#btn-vprint').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
        $('#download-btn').attr('onclick','');
        $('#download-btn').attr('onclick',"MyWindow=window.open('<?php echo site_url(CRM_VAR.'/order_vreport_print')?>?"+requestReport()+"','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=900,height=600'); return false;");
        $('#download-btn').get(0).click();
    });
</script>
<?php } ?>