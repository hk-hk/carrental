<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Car Detail</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="form-horizontal" >
          <div class="box-body">
            <!-- <h3 class="box-title">Filter</h3> -->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="user_id" id="user_id" class="form-control selectpicker" data-live-search="true">
                      <option value="">Select Vendor</option>
                      <option value="only-vendor">Only Vendor</option>
                      <?php
                      foreach ($suppliers as $option){
                      $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                      
                      <option value="<?php echo $option->id?>"><?php echo $optionText ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Car Tag</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="car_tag" id="car_tag" class="form-control">
                      <!-- <option value="0">Select Car</option> -->
                      <option value=''>Select Car</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Workshop</label>
                  <div class="col-md-8">
                    <select name="workshop_name" id="workshop_name" class="form-control">
                      <option value="">All</option>
                      <?php foreach ($workshops as $key => $value): ?>
                        <option value="<?= $value->title ?>"><?= $value->title ?></option>
                        
                      <?php endforeach ?>
                    </select>
                    <!-- <input type="text" class="form-control"> -->
                  </div>
                </div>
              </div>
             
            </div>
            
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  control-label">Recieved From Vendor</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value=""  name="recieved" id="recieved" checked=""> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="pending" name="recieved" id="recieved"> Pending</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="complete" name="recieved" id="recieved"> Complete</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  control-label">Paid To Workshop</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="" name="paid" id="paid" checked=""> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="pending" name="paid" id="paid"> Pending</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" value="complete" name="paid" id="paid"> Complete</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  control-label">Date Range</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="" name="date_type" id="date_type" checked=""> All</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cday" name="date_type" id="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pday" name="date_type" id="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cmonth" name="date_type" id="date_type"> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pmonth" name="date_type" id="date_type"> Previous Month</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3 col-md-offset-1 control-label"></label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label>
                  <div class="col-md-3">
                    <!-- <label class="control-label"><input type="radio" class="" name="date_type"> Current Day</label> -->
                    <input type="text" id="from_date" class="form-control datepicker">
                    <span>From Date</span>
                  </div>
                  <!-- <span>-</span> -->
                  <div class="col-md-3">
                    <!-- <label class="control-label"><input type="radio" class="" name="date_type"> Current Day</label> -->
                    <input type="text" id="to_date" class="form-control datepicker">
                    <span>To Date</span>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          
        </div>
        <div class="box-footer">
          <button id="btn-search" type="button" class="btn btn-info pull-right">Search</button>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title pull-left">Maintenances</h3>
          <div class="pull-right"><a href="#" target="" id="btn-print">Print</a> | <a href="#" id="btn-export">Export</a></div>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Car Type</th>
                  <th>Car Tag</th>
                  <th>Date</th>
                  <th>Description</th>
                  <th>Comments</th>
                  <th>Total Reprair Cost</th>
                  <th>Received From Vendor</th>
                  <th>Paid To Workshop</th>
                  <th>Workshop Name</th>
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
              
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.box -->
  </div>
  <!--/.col (left) -->

<!-- /.row -->
</section>
<?php if(TRUE){?>
<script type="text/javascript">
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/maintenance_report_list')?>",
            "type": "POST",
            "data": function ( data ) {         
                data.car_tag = $('#car_tag').val();
                data.user_id = $('#user_id').val();
                // data.maker = $('#maker').val();
                data.workshop_name = $('#workshop_name').val();
                data.paid = $('#paid:checked').val();
                data.recieved = $('#recieved:checked').val();
                var dateType=$('#date_type:checked').val();
                var fromto;
                data.date_type = dateType;
                if(dateType=='from_to'){
                  data.from_date=$("#from_date").val();
                  data.to_date=$("#to_date").val();
                }
            }
        },
        //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,8 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });

   

    $('#user_id').change(function(){
      // console.log(this.val());
      $("#user_id option:selected" ).each(function() {
        // console.log("$(this).val()");
        
        $.get("<?php echo site_url(CRM_VAR.'/related_car_tag/')?>"+$(this).val(),function(data){
          var html="";
          html="<option value=''>Select Car</option>";
          var obj=$.parseJSON(data);
          $.each(obj,function(k,v){
            html+="<option value='"+v['id']+"'>"+v['car_number']+"</option>";
          });
            // console.log(html);
          $('#car_tag').html(html);
        });
      });
    });

    $('input[name=date_type]').change(function(){
      if(this.value!="from_to"){
        $("#from_date").val("");
        $("#to_date").val("");
      }
      // console.log();
    });
    $('.datepicker').datepicker({
        autoclose: true
    });
    $('.datepicker').focus(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('.datepicker').focusout(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('#btn-search').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
});
  serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}
   function requestReport(){
      requestData={
        car_tag : $('#car_tag').val(),
        user_id : $('#user_id').val(),
        // maker : $('#maker').val(),
        workshop_name : $('#workshop_name').val(),
        paid : $('#paid:checked').val(),
        recieved : $('#recieved:checked').val(),
        date_type:$('#date_type:checked').val(),
        from_date:$("#from_date").val(),
        to_date:$("#to_date").val()
      }
      return serialize(requestData);
    }
    $('#btn-export').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/maintenance_report_output')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });

    $('#btn-print').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/maintenance_report_print')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });
</script>
<?php } ?>
