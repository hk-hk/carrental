<!DOCTYPE html>
<html>
<head>
    <!-- <title></title> -->
    <meta charset="utf-8">
    <title>Order Report - <?= date('d-m-Y') ?></title>
    <!-- <style>
        .result-th {
            color: #000000;
            background-color: #bfbfbf;
            font-size: 10pt;
            text-align: center;
            border-bottom: 1px solid #000000;
        }
        
        .result-td {
            font-size: 8pt;
            border-left: 1px solid #efefef;
            border-right: 1px solid #efefef;
        }
        
        td {
            font-size: 8pt;
        }
        
        .cus {
            font-size: 11pt;
            line-height: 13px;
        }
        
        h3 {
            line-height: 0px;
        }
        
        .sub-result-td {
            font-size: 8pt;
            border-left: 1px solid #efefef;
            border-right: 1px solid #efefef;
            border-top: 1px solid #7b7b7b;
            border-bottom: 1px solid #000000;
        }
    </style> -->
    <style>
            .result-th{
                color:#ffffff;
                background-color: #BA221E;
                font-size: 10pt;
                text-align: center;
            }
            .result-td{
                font-size: 8pt;
                border-left: 1px solid #efefef;
                border-right: 1px solid #efefef;
            }
            td{
                font-size: 8pt;
            }
            .cus{
                font-size: 11pt;
                line-height:13px;
            }
            h3{
                line-height:0px;
            }

            .sub-result-td{
                font-size: 8pt;
                border-left: 1px solid #efefef;
                border-right: 1px solid #efefef;
                border-top: 1px solid #7b7b7b;
                border-bottom: 1px solid #000000;
            }

        </style>
</head>
<body>
    <?= $html ?>
</body>
</html>