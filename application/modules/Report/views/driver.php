<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Driver OT/Bounus Report</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="form-horizontal" >
          <div class="box-body">
            <!-- <h3 class="box-title">Filter</h3> -->
            <div class="row">
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Type</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="type" id="trip_type" class="form-control">
                      <option value="">Select Type</option>
                      <option value="36">Daily</option>
                      <option value="37">Weekly</option>
                      <option value="38">Monthly</option>
                      
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Driver</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="driver"> -->
                     <select name="driver" id="driver" class="form-control selectpicker"  data-live-search="true">
                      <option value="">Select Driver</option>
                      <?php foreach ($drivers as $key => $value): ?>
                        <option value="<?= $value->user_lname ?>"><?= $value->user_lname ?></option>
                      <?php endforeach ?>
                      </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Client</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="client" id="client" class="form-control selectpicker"  data-live-search="true">
                      <option value="">Select Client</option>
                      <?php foreach ($customers as $k => $customer) {
                          if($customer->user_role=='agent')
                              $optionLevel = ($customer->user_company!='')?ucfirst($customer->user_company):$customer->user_fname." ".$customer->user_lname;
                          else
                              $optionLevel = ucwords ($customer->user_fname." ".$customer->user_lname);
                          ?>
                      <option value="<?php echo $customer->user_fname.' '.$customer->user_lname; ?>"><?php echo $optionLevel ?></option>
                      <?php } ?>
                      
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                  <div class="col-md-8">
                    <!-- <input type="text" class="form-control" id="car_tag"> -->
                    <select name="vendor" id="vendor" class="form-control selectpicker"  data-live-search="true">
                      <option value="">Select Vendor</option>
                      <?php
                      foreach ($suppliers as $option){
                      $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                      
                      <option value="<?php echo $option->id?>"><?php echo $optionText ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  col-md-offset-1 control-label">Recieved From Vendor</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Yes</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> No</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3  col-md-offset-1 control-label">Paid To Workshop</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Both</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> Yes</label>
                  <label class="control-label col-md-2 " style="text-align: left;"><input type="radio" class="" name="date_type"> No</label>
                  
                </div>
              </div>
            </div> -->
            
            <!-- <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  col-md-offset-2 control-label">Date Range</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" class="" name="date_type"> Previous Month</label>
                  
                </div>
              </div>
            </div> -->
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="FirstName" class="col-md-2  control-label">Date Range</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="" name="date_type" id="date_type" checked=""> All</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cday" name="date_type" id="date_type"> Current Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pday" name="date_type" id="date_type"> Previous Day</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="cmonth" name="date_type" id="date_type"> Current Month</label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="pmonth" name="date_type" id="date_type"> Previous Month</label>
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="FirstName" class="col-md-3 col-md-offset-1 control-label"></label>
                  <label class="control-label col-md-2" style="text-align: left;"><input type="radio" value="from_to" class="from_to_check" name="date_type" id="date_type"> From</label>
                  <div class="col-md-3">
            
                    <input type="text" id="from_date" class="form-control datepicker">
                    <span>From Date</span>
                  </div>
            
                  <div class="col-md-3">
            
                    <input type="text" id="to_date" class="form-control datepicker">
                    <span>To Date</span>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          
        </div>
        <div class="box-footer">
          <button id="btn-search" type="button" class="btn btn-info pull-right">Search</button>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title pull-left">List</h3>
          <div class="pull-right"><a href="#" id="btn-export">Export</a></div>
          <!-- <a href="#">Print</a> |  -->
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Driver</th>
                  <!-- <th>Trip Type</th> -->
                  <th>Client</th>
                  <!-- <th>Vendor</th> -->
                  <!-- <th>Car Tag</th> -->
                  <!-- <th>Start Date</th> -->
                  <!-- <th>End Date</th> -->
                  <th>No of Trips</th>
                  <th>Total OT Hours</th>
                  <th>Total Driver OT Fees</th>
                  <th>Total Driver Bonus</th>
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
              
              
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.box -->
  </div>
  <!--/.col (left) -->

<!-- /.row -->
</section>
<?php if(TRUE){?>
<script type="text/javascript">
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/driver_bonus_report_list')?>",
            "type": "POST",
            "data": function(data){           
                data.driver = $('#driver').val();
                data.trip_type = $('#trip_type').val();
                data.client = $('#client').val();            
                data.vendor = $('#vendor').val();
                var dateType=$('#date_type:checked').val();
                var fromto;
                data.date_type = dateType;
                if(dateType=='from_to'){
                  data.from_date=$("#from_date").val();
                  data.to_date=$("#to_date").val();
                  // data.fromto = {from:from_date, to:to_date};
                }
            }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        ],
 
    });
    $('input[name=date_type]').change(function(){
      if(this.value!="from_to"){
        $("#from_date").val("");
        $("#to_date").val("");
      }
      // console.log();
    });
    $('.datepicker').datepicker({
        autoclose: true
    });
    $('.datepicker').focus(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('.datepicker').focusout(function(){
        $('.from_to_check').prop('checked',"checked");
    });
    $('#btn-search').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });

    $('#driver').keyup(function(){
      // load();
      table.ajax.reload(null,false);
    });
});
  serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}
   function requestReport(){
      driver = $('#driver').val();
      trip_type = $('#trip_type').val();
      client = $('#client').val();            
      vendor = $('#vendor').val();
      requestData={
        driver:driver,
        trip_type:trip_type,
        // maker:maker,
        client:client,
        vendor:vendor
      }
      return serialize(requestData);
    }
    $('#btn-export').click(function(e){ //button filter event click
      e.preventDefault(); 
      $('#btn-search').click();
      // var filename='';
      $.post("<?php echo site_url(CRM_VAR.'/driver_bonus_report_output')?>",requestReport(),function(data){
        window.open('<?= site_url() ?>/'+data);
      });
      
    });
</script>
<?php } ?>