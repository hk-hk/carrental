<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RModels extends MY_Model{
	private $_ci;
    function __construct() {
        parent::__construct();
    	$this->_ci =& get_instance();
	 	// $this->_ci->load->model('Car_manager/Car_manager', 'car_manager');
	 	// $this->_ci->load->model('Car_manager/Car_maintain', 'car_maintain');
	 	$role =  $this->session->userdata('cr_user_role');
    }

    function _get_maintenance_datatables_query() // dynamic search query of. user.
    {
        $column_order = array(null,'car_type','car_number','date','description','amount','recieved','paid','workshop_name','repaired_by'); 
        // echo json_encode($_POST);
        $this->db->select("cr_maintenance.*");
        $this->db->select("cr_maintenance.id as mid");
        $this->db->select("cr_users.user_fname");
        $this->db->select("cr_users.user_lname");
        $this->db->select("cr_manage_cars.*");
        $this->db->select("cr_manage_attributes.title as maker");

        $this->db->from('cr_maintenance');
        
        $this->db->join('cr_manage_cars`', '`cr_manage_cars`.`id` = `cr_maintenance`.`car_id`','inner');
        $this->db->join('cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
        $this->db->join('cr_manage_attributes`', '`cr_manage_attributes`.`id` = `cr_manage_cars`.`maker`','inner');
        
        $this->db->where('cr_users.user_role', 'supplier');
        if($this->input->post('car_tag'))
        {
            $this->db->where('cr_manage_cars.id', $this->input->post('car_tag'));
        }
        if($this->input->post('user_id'))
        {   
            if($this->input->post('user_id')=='only-vendor'){
                $this->db->where('cr_manage_cars.user_id !=', '4');
            }else{
                $this->db->where('cr_manage_cars.user_id', $this->input->post('user_id'));
            }
            
        }
        if($this->input->post('maker'))
        {
            $this->db->where('cr_manage_attributes.id', $this->input->post('maker'));
        }
        if($this->input->post('workshop_name'))
        {
            $this->db->like('cr_maintenance.workshop_name', $this->input->post('workshop_name'));
        }
        if($this->input->post('paid'))
        {
            $this->db->where('cr_maintenance.paid',$this->input->post('paid'));
        }
        if($this->input->post('recieved'))
        {
            $this->db->where('cr_maintenance.recieved',$this->input->post('recieved'));
        }

    
        if($this->input->post('date_type'))
        {
            $dt=$this->input->post('date_type');
            if($dt=="from_to"){
                // $fromto=$this->input->post('fromto');
                $from_date=$this->input->post('from_date');
                $to_date=$this->input->post('to_date');
                $this->db->where("cr_maintenance.date BETWEEN ".strtotime($from_date)." AND ".strtotime($to_date));
            }else if($dt=="cday"){
                $this->db->where('cr_maintenance.date',strtotime(date('m/d/y')));
            }else if($dt=="pday"){
                $this->db->where('cr_maintenance.date',strtotime(date('m/d/y',strtotime("-1 days"))));
            }else if($dt=="cmonth"){
                $cmonth= strtotime(date('M Y'));
                $lastday= date('t',strtotime($cmonth));
                $emonth= strtotime(date($lastday.' M Y 23:59:59'));
                $this->db->where("cr_maintenance.date BETWEEN $cmonth AND $emonth");
            }else if($dt=="pmonth"){
                $pmonth= strtotime(date('M Y',strtotime("-1 months")));
                $lastday= date('t',strtotime($pmonth));
                $emonth= strtotime(date($lastday.' M Y',strtotime("-1 months")));
                $this->db->where("cr_maintenance.date BETWEEN $pmonth AND $emonth");
            }else{
                $this->db->where('cr_maintenance.date','');
            }
        }
            // echo $this->input->post('order_by');
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by('cr_maintenance.date','desc');
                // $this->db->order_by('cr_orders.order_id','desc');
            }
        }
    }  
    
    function get_maintenance_datatables()
    { 
        $this->_get_maintenance_datatables_query();
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function _get_fuel_datatables_query() // dynamic search query of. user.
    {
        $column_order = array('shop_name','purchased_by','status','type_of_use','date','liter','amount'); 
        $this->db->select("cr_oil_slip.*");
        $this->db->select("cr_manage_cars.car_number");
        $this->db->select("cr_users.user_fname");
        $this->db->select("cr_users.user_lname");

        $this->db->from('cr_oil_slip');

        $this->db->join('cr_manage_cars`', '`cr_manage_cars`.`id` = `cr_oil_slip`.`car_id`','inner');
        // $this->db->join('cr_manage_cars`', '`cr_manage_cars`.`user_id` = `cr_oil_slip`.`car_id`','inner');
        $this->db->join('cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
        
        if($this->input->post('vendor'))
        {
            $this->db->where('cr_users.id', $this->input->post('vendor'));
        }
        if($this->input->post('car_tag'))
        {
            $this->db->where('cr_oil_slip.car_id', $this->input->post('car_tag'));
        }
        if($this->input->post('payment_type'))
        {
            $this->db->where('cr_oil_slip.purchased_by',$this->input->post('payment_type'));
        }
        if($this->input->post('driver'))
        {
            $this->db->like('cr_oil_slip.operator_name',$this->input->post('driver'));
        }
        if($this->input->post('gas_station'))
        {
            $this->db->like('cr_oil_slip.shop_name',$this->input->post('gas_station'));
        }
        if($this->input->post('payment_status'))
        {
            $this->db->like('cr_oil_slip.status',$this->input->post('payment_status'));
        }
        if($this->input->post('date_type'))
        {
            $dt=$this->input->post('date_type');
            if($dt=="from_to"){
                $from_date=$this->input->post('from_date');
                $to_date=$this->input->post('to_date');

                // $fromto=$this->input->post('fromto');
                $this->db->where("cr_oil_slip.date BETWEEN ".strtotime($from_date)." AND ".strtotime($to_date));
            }else if($dt=="cday"){
                $this->db->where('cr_oil_slip.date',strtotime(date('m/d/y')));
            }else if($dt=="pday"){
                $this->db->where('cr_oil_slip.date',strtotime(date('m/d/y',strtotime("-1 days"))));
            }else if($dt=="cmonth"){
                $cmonth= strtotime(date('M Y'));
                $lastday= date('t',strtotime($cmonth));
                $emonth= strtotime(date($lastday.' M Y 23:59:59'));
                $this->db->where("cr_oil_slip.date BETWEEN $cmonth AND $emonth");
            }else if($dt=="pmonth"){
                $pmonth= strtotime(date('M Y',strtotime("-1 months")));
                $lastday= date('t',strtotime($pmonth));
                $emonth= strtotime(date($lastday.' M Y',strtotime("-1 months")));
                $this->db->where("cr_oil_slip.date BETWEEN $pmonth AND $emonth");
            }else{
                $this->db->where('cr_oil_slip.date','');
            }
        }
        if($this->input->post('car_id'))
        {
            $this->db->where('car_id', $this->input->post('car_id'));
        }
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by('cr_oil_slip.date','desc');
                // $this->db->order_by('cr_orders.order_id','desc');
            }
        }
        
    }  
    function get_fuel_datatables()
    { 
        $this->_get_fuel_datatables_query();
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();   
        return $query->result();
    }
    function _get_order_datatable_query(){
        $this->db->select('cr_orders.order_id');
        $this->db->select("cr_orders.order_id AS o_id");
        $this->db->select("(SELECT Sum(payment.total_price) FROM payment AS payment WHERE payment.cr_order_id = o_id)AS customer_total");
        $this->db->select("(SELECT Sum(trip_exp.vendor_fees) FROM cr_trip_expense AS trip_exp WHERE trip_exp.order_id = o_id AND trip_exp.pay_to= 'vendor') AS vendor_total");
        $this->db->select("(SELECT Sum(trip_exp.vendor_fees) FROM cr_trip_expense AS trip_exp WHERE trip_exp.order_id = o_id AND trip_exp.pay_to= 'agent') AS agent_total");
        $this->db->select("(SELECT Sum(oil_slip.amount) FROM cr_oil_slip AS oil_slip WHERE oil_slip.order_id = o_id) AS fuel_expense_total");
        $this->db->select("(SELECT Sum(tran_rec.amount) FROM transations_recs AS tran_rec WHERE tran_rec.linked_id = o_id AND tran_rec.category_id=4451) AS toll_expense_total");
        $this->db->select("(SELECT Sum(tran_rec.amount) FROM transations_recs AS tran_rec WHERE tran_rec.linked_id = o_id AND tran_rec.category_id=4452) AS other_expense_total");
        $this->db->select("cr_orders.vendor_payment_status");
        $this->db->select("cr_orders.agent_payment_status");
        $this->db->select("cr_orders.customer_payment_status");
        $this->db->select("cr_orders.order_by");
        $this->db->select("cr_orders.order_history");
        $this->db->select("cr_orders.vendor_fees");
        
        $this->db->select("cr_orders.user_id");
        $this->db->select("cr_orders.car_id");
        $this->db->select("cr_orders.rent_type");
        $this->db->select("cr_orders.pickup_date");
        $this->db->select("cr_orders.drop_date");
        $this->db->select("cr_orders.status");

        // $this->db->select('cr_trip_expense.*');

        // $this->db->select('cr_payment_info.amount');
        // $this->db->select('cr_manage_cars.car_type');

        $this->db->from('cr_orders');
        // $this->db->join('cr_trip_expense','cr_orders.order_id = cr_trip_expense.order_id','inner');
        $this->db->join('payment','cr_orders.order_id = payment.cr_order_id','LEFT');

        $this->db->group_by('cr_orders.order_id'); 
        if($this->input->post('order_status'))
        {
            $this->db->where('cr_orders.status', $this->input->post('order_status'));
        }
        if($this->input->post('customer_status'))
        {
            $this->db->where('cr_orders.customer_payment_status', $this->input->post('customer_status'));
        }
        if($this->input->post('vendor_status'))
        {
            $this->db->where('cr_orders.vendor_payment_status', $this->input->post('vendor_status'));
        }
        if($this->input->post('agent_status'))
        {
            $this->db->where('cr_orders.agent_payment_status', $this->input->post('agent_status'));
        }

        
        if($this->input->post('trip_type'))
        {
            $this->db->where('cr_orders.rent_type', $this->input->post('trip_type'));
        }
        if($this->input->post('driver'))
        {
            $this->db->where('cr_order_detail.driver_name', $this->input->post('driver'));
        }
        if($this->input->post('vendor'))
        {
            $this->db->where('cr_order_detail.supplier_id', $this->input->post('vendor'));
        }
        if($this->input->post('client'))
        {
            $this->db->like('cr_order_detail.name', $this->input->post('client'));
        }
        if($this->input->post('date_type'))
        {
            $dt=$this->input->post('date_type');
            if($dt=="from_to"){
                $from_date=$this->input->post('from_date');
                $to_date=$this->input->post('to_date');
                $from=date('Y-m-d',strtotime($from_date));
                $to = date('Y-m-d',strtotime($to_date));
                $this->db->where("cr_orders.pickup_date BETWEEN '".$from."' AND '".$to."'");
            }else if($dt=="cday"){
                $this->db->like('cr_orders.pickup_date',date('Y-m-d'));
            }else if($dt=="pday"){
                $this->db->like('cr_orders.pickup_date',date('Y-m-d',strtotime("-1 days")));
            }else if($dt=="cmonth"){
                $cmonth= date('Y-m-01');
                $lastday= date('t',strtotime($cmonth));
                $emonth= date('Y-m-'.$lastday.' 23:59:59');
                $this->db->where("cr_orders.pickup_date BETWEEN '$cmonth' AND '$emonth'");
            }else if($dt=="pmonth"){
                $pmonth= date('Y-m',strtotime("-1 months"));
                $lastday= date('t',strtotime($pmonth));
                $emonth= date('Y-m-'.$lastday,strtotime("-1 months"));
                $this->db->where("cr_orders.pickup_date BETWEEN '$pmonth' AND '$emonth'");
            }else{
                $this->db->like('cr_orders.pickup_date','');
            }
        }
        $this->db->order_by('cr_orders.order_id','desc');
        
    }
    function _get_trip_detail_datatable_query(){
        $this->db->select("cr_orders.order_id AS o_id");
        $this->db->select("(SELECT Sum(payment.total_price) FROM payment AS payment WHERE payment.cr_order_id = o_id)AS customer_total");
        $this->db->select("(SELECT Sum(trip_exp.vendor_fees) FROM cr_trip_expense AS trip_exp WHERE trip_exp.order_id = o_id AND trip_exp.pay_to= 'vendor') AS vendor_total");
        $this->db->select("(SELECT Sum(trip_exp.vendor_fees) FROM cr_trip_expense AS trip_exp WHERE trip_exp.order_id = o_id AND trip_exp.pay_to= 'agent') AS agent_total");
        $this->db->select("(SELECT Sum(oil_slip.amount) FROM cr_oil_slip AS oil_slip WHERE oil_slip.order_id = o_id) AS fuel_expense_total");
        $this->db->select("(SELECT Sum(tran_rec.amount) FROM transations_recs AS tran_rec WHERE tran_rec.linked_id = o_id AND tran_rec.category_id=4451) AS toll_expense_total");
        $this->db->select("(SELECT Sum(tran_rec.amount) FROM transations_recs AS tran_rec WHERE tran_rec.linked_id = o_id AND tran_rec.category_id=4452) AS other_expense_total");
        
        $this->db->select('cr_orders.order_id');
        $this->db->select('cr_orders.vendor_fees');
        $this->db->select('cr_orders.agent_fees');
        $this->db->select('cr_orders.ot_hours');
        $this->db->select('cr_orders.driver_ot_fees');
        $this->db->select('cr_orders.driver_fees');
        $this->db->select('cr_orders.driver_percentage');
        $this->db->select('cr_orders.status');
        $this->db->select('cr_orders.rent_type');
        $this->db->select('cr_orders.car_id');
        $this->db->select('cr_orders.user_id');
        $this->db->select('cr_orders.pickup_date');
        $this->db->select('cr_orders.drop_date');
        $this->db->select('cr_orders.pickup_location');
        $this->db->select('cr_orders.drop_location');
        $this->db->select('cr_orders.booking_amount');
        $this->db->select('cr_orders.vendor_payment_status');
        $this->db->select('cr_orders.agent_payment_status');
        $this->db->select('cr_orders.customer_payment_status');
        $this->db->select('cr_orders.booking_amount as amount');
        $this->db->select('cr_orders.daily_status');
        $this->db->select('cr_orders.trip_record_name');
        $this->db->select('cr_orders.driver_salary');
        $this->db->select('cr_orders.order_history');
        $this->db->select('cr_orders.driver_from');
        
        $this->db->select("cr_orders.airport_status");
        $this->db->select('cr_order_detail.driver_name');
        $this->db->select('cr_order_detail.driver_phone');
        $this->db->select('cr_order_detail.type_of_rent');
        $this->db->select('cr_order_detail.name');
        $this->db->select('cr_order_detail.phone');
        $this->db->select('cr_users.user_fname');
        $this->db->select('cr_users.user_lname');
        $this->db->select('cr_manage_attributes.title');
        $this->db->select('cr_manage_attributes.short_desc AS short_desc1');
        $this->db->select('cr_manage_cars.car_number');
        $this->db->select('cr_manage_cars.model');
        // $this->db->select('cr_trip_expense.*');
        // $this->db->select('cr_payment_info.amount');
        $this->db->select('cr_manage_cars.car_type');

        $this->db->from('cr_orders');

        $this->db->join('payment','cr_orders.order_id = payment.cr_order_id','LEFT');
        // $this->db->join('cr_trip_expense','cr_orders.order_id = cr_trip_expense.order_id','inner');
        $this->db->join('cr_order_detail','cr_order_detail.order_id = cr_orders.order_id','inner');
        $this->db->join('cr_users','cr_order_detail.supplier_id = cr_users.id','inner');
        $this->db->join('cr_manage_cars','cr_manage_cars.id = cr_orders.car_id','inner');
        $this->db->join('cr_manage_attributes','cr_manage_attributes.id = cr_manage_cars.model','inner');
        // $this->db->join('cr_payment_info','cr_payment_info.order_id = cr_order_detail.order_id','inner');

        $this->db->group_by('cr_orders.order_id');


        // print "<Pre/>";

        parse_str(!empty($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:http_build_query($_POST), $request);
        // print_r($_SERVER);
        // print_r(http_build_query($_POST));
        // print_r($request);
        // exit();

        if (isset($request['order_id']) && $request['order_id']) {
            $this->db->where('cr_orders.order_id', $request['order_id']);
        }
        if($request['order_status'] && $request['order_status']!='NotCancelled'){
            $this->db->where('cr_orders.status', $request['order_status']);
        }else if($request['order_status'] && $request['order_status']=='NotCancelled'){
            $this->db->where('cr_orders.status !=', 'Cancelled');
            // echo 'hi';
        }
        if($request['customer_status'])
        {
            $this->db->where('cr_orders.customer_payment_status', $request['customer_status']);
        }
        if($request['vendor_status'])
        {
            $this->db->where('cr_orders.vendor_payment_status', $request['vendor_status']);
        }
        if($request['agent_status'])
        {
            $this->db->where('cr_orders.agent_payment_status', $request['agent_status']);
        }
        //Pickup Dates
        if ($request['pickup_date_from']) {
            $this->db->where("DATE_FORMAT(cr_orders.pickup_date,'%Y-%m-%d') >=", date("Y-m-d", strtotime($request['pickup_date_from'])));
        }
        if ($request['pickup_date_to']) {
            $this->db->where("DATE_FORMAT(cr_orders.pickup_date,'%Y-%m-%d') <=", date("Y-m-d", strtotime($request['pickup_date_to'])));
        }
        //DropOff Dates
        if ($request['dropoff_date_from']) {
            $this->db->where("DATE_FORMAT(cr_orders.drop_date,'%Y-%m-%d') >=", date("Y-m-d", strtotime($request['dropoff_date_from'])));
        }
        if ($request['dropoff_date_to']) {
            $this->db->where("DATE_FORMAT(cr_orders.drop_date,'%Y-%m-%d') <=", date("Y-m-d", strtotime($request['dropoff_date_to'])));
        }
        if(isset($request['rent_type']) && $request['rent_type'])
        {
            $this->db->where('cr_orders.rent_type', $request['rent_type']);
        }
        if(isset($request['trip_type']) && $request['trip_type'])
        {
            $this->db->where('cr_orders.airport_status', $request['trip_type']);
        }
        if(isset($request['daily_status']) && $request['daily_status'])
        {
            $this->db->where('cr_orders.daily_status', $request['daily_status']);
        }
        if(isset($request['driver_from']) && $request['driver_from']){
            // $this->db->where('cr_orders.driver_from', $request['driver_from']);
            $this->db->where('cr_orders.driver_fees','!=', '0');   
        }
        if(isset($request['driver']) && $request['driver'])
        {
            $this->db->where('cr_order_detail.driver_name', $request['driver']);
        }
        if(isset($request['vendor']) && $request['vendor'])
        {
            if($request['vendor']=='only-vendor'){
                $this->db->where('cr_order_detail.supplier_id !=','4');
            }else{
                $this->db->where('cr_order_detail.supplier_id', $request['vendor']);
            }
        }
        if(isset($request['client']) && $request['client'])
        {
            $this->db->like('cr_order_detail.name', $request['client']);
        }
        if(isset($request['date_type']) && $request['date_type'])
        {
            $dt=$request['date_type'];
            if($dt=="from_to"){
                $from_date=$request['from_date'];
                $to_date=$request['to_date'];
                $from=date('Y-m-d',strtotime($from_date));
                $to = date('Y-m-d',strtotime($to_date));
                $this->db->where("cr_orders.booking_date BETWEEN '".$from."' AND '".$to."'");
            }else if($dt=="cday"){
                $this->db->like('cr_orders.booking_date',date('Y-m-d'));
            }else if($dt=="pday"){
                $this->db->like('cr_orders.booking_date',date('Y-m-d',strtotime("-1 days")));
            }else if($dt=="cmonth"){
                $cmonth= date('Y-m-01');
                $lastday= date('t',strtotime($cmonth));
                $emonth= date('Y-m-'.$lastday.'  23:59:59');
                $this->db->where("cr_orders.booking_date BETWEEN '$cmonth' AND '$emonth'");
            }else if($dt=="pmonth"){
                // echo "hello";
                $pmonth= date('Y-m-01', strtotime('-' . date('d') . ' days'));
                $lastday= date('t',strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));

                $emonth= date('Y-m-'.$lastday,strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // echo $pmonth."<br/>";
                // echo $emonth."<br/>";

                $this->db->where("cr_orders.booking_date BETWEEN '$pmonth' AND '$emonth'");
            }else{
                $this->db->like('cr_orders.booking_date','');
            }
        }
        // print_r($)
                // exit();
        $this->db->order_by('cr_orders.order_id','desc');
        
    }
    function get_order_rec(){
        $this->db->select("cr_orders.order_id AS o_id");
        $this->db->select("(SELECT Sum(payment.total_price) FROM payment AS payment WHERE payment.cr_order_id = o_id)AS customer_total");
        $this->db->select("(SELECT Sum(trip_exp.vendor_fees) FROM cr_trip_expense AS trip_exp WHERE trip_exp.order_id = o_id AND trip_exp.pay_to= 'vendor') AS vendor_total");
        $this->db->select("(SELECT Sum(trip_exp.vendor_fees) FROM cr_trip_expense AS trip_exp WHERE trip_exp.order_id = o_id AND trip_exp.pay_to= 'agent') AS agent_total");
        $this->db->select("(SELECT Sum(oil_slip.amount) FROM cr_oil_slip AS oil_slip WHERE oil_slip.order_id = o_id) AS fuel_expense_total");
        $this->db->select("(SELECT Sum(tran_rec.amount) FROM transations_recs AS tran_rec WHERE tran_rec.linked_id = o_id AND tran_rec.category_id=4451) AS toll_expense_total");
        $this->db->select("(SELECT Sum(tran_rec.amount) FROM transations_recs AS tran_rec WHERE tran_rec.linked_id = o_id AND tran_rec.category_id=4452) AS other_expense_total");
        $this->db->select('cr_orders.order_id');
        $this->db->select('cr_orders.vendor_fees');
        $this->db->select('cr_orders.agent_fees');
        $this->db->select('cr_orders.ot_hours');
        $this->db->select('cr_orders.driver_ot_fees');
        $this->db->select('cr_orders.driver_fees');
        $this->db->select('cr_orders.driver_percentage');
        $this->db->select('cr_orders.status');
        $this->db->select('cr_orders.rent_type');
        $this->db->select('cr_orders.car_id');
        $this->db->select('cr_orders.user_id');
        $this->db->select('cr_orders.pickup_date');
        $this->db->select('cr_orders.drop_date');
        $this->db->select('cr_orders.pickup_location');
        $this->db->select('cr_orders.drop_location');
        $this->db->select('cr_orders.booking_amount');
        $this->db->select('cr_orders.vendor_payment_status');
        $this->db->select('cr_orders.agent_payment_status');
        $this->db->select('cr_orders.customer_payment_status');
        $this->db->select('cr_orders.status');
        $this->db->select('cr_orders.driver_from');
        $this->db->select('cr_order_detail.driver_name');
        $this->db->select('cr_order_detail.driver_phone');
        $this->db->select('cr_order_detail.type_of_rent');
        $this->db->select('cr_order_detail.name');
        $this->db->select('cr_users.user_fname');
        $this->db->select('cr_users.user_lname');
        $this->db->select('cr_manage_attributes.title');
        $this->db->select('cr_manage_attributes.short_desc AS short_desc1');
        $this->db->select('cr_manage_cars.car_number');
        // $this->db->select('cr_trip_expense.*');
        $this->db->select('cr_payment_info.amount');
        $this->db->select('cr_manage_cars.car_type');

        $this->db->from('cr_orders');

        $this->db->join('payment','cr_orders.order_id = payment.cr_order_id','LEFT');
        // $this->db->join('cr_trip_expense','cr_orders.order_id = cr_trip_expense.order_id','inner');
        $this->db->join('cr_order_detail','cr_order_detail.order_id = cr_orders.order_id','inner');
        $this->db->join('cr_users','cr_order_detail.supplier_id = cr_users.id','inner');
        $this->db->join('cr_manage_cars','cr_manage_cars.id = cr_orders.car_id','inner');
        $this->db->join('cr_manage_attributes','cr_manage_attributes.id = cr_manage_cars.model','inner');
        $this->db->join('cr_payment_info','cr_payment_info.order_id = cr_order_detail.order_id','inner');
        if($this->input->post('client'))
        {
            $this->db->where('cr_order_detail.name', $this->input->post('client'));
        }

        if($this->input->post('date_type'))
        {
            $dt=$this->input->post('date_type');
            if($dt=="from_to"){
                $from_date=$this->input->post('from_date');
                $to_date=$this->input->post('to_date');
                $from=date('Y-m-d',strtotime($from_date));
                $to = date('Y-m-d',strtotime($to_date));
                $this->db->where("cr_orders.pickup_date BETWEEN '".$from."' AND '".$to."'");
            }else if($dt=="cday"){
                $this->db->like('cr_orders.pickup_date',date('Y-m-d'));
            }else if($dt=="pday"){
                $this->db->like('cr_orders.pickup_date',date('Y-m-d',strtotime("-1 days")));
            }else if($dt=="cmonth"){
                $cmonth= date('Y-m-01');
                $lastday= date('t',strtotime($cmonth));
                $emonth= date('Y-m-'.$lastday.' 23:59:59');
                // echo "hello";
                $this->db->where("cr_orders.pickup_date BETWEEN '$cmonth' AND '$emonth'");
            }else if($dt=="pmonth"){
                $pmonth= date('Y-m', strtotime('-' . date('d') . ' days'));
                $lastday= date('t',strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                $emonth= date('Y-m-'.$lastday,strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));

                $this->db->where("cr_orders.pickup_date BETWEEN '$pmonth' AND '$emonth'");
            }else{
                $this->db->like('cr_orders.pickup_date','');
            }
        }
        $this->db->group_by('cr_orders.order_id'); 
        $query = $this->db->get();
        return $query->result();
    }

    function _get_driver_datatable_query(){
        $this->db->select('cr_users.id');
        $this->db->select('cr_users.user_lname');
        $this->db->select('cr_order_detail.supplier_id');
        $this->db->select('cr_order_detail.car_tag');
        $this->db->select('cr_orders.pickup_date');
        $this->db->select('cr_orders.drop_date');
        $this->db->select('Count(cr_users.id) AS no_of_trip');
        $this->db->select('Sum(cr_orders.ot_hours) AS total_ot_hours');
        $this->db->select('cr_orders.driver_ot_fees');
        $this->db->select('cr_orders.driver_fees');
        $this->db->select('cr_orders.driver_percentage');
        $this->db->select('cr_orders.order_id');
        $this->db->from('cr_users');

        $this->db->join('cr_order_detail','cr_users.user_lname = cr_order_detail.driver_name','inner');
        $this->db->join('cr_orders','cr_order_detail.order_id = cr_orders.order_id','inner');

        $this->db->where('cr_users.user_role','driver');
        if($this->input->post('client'))
        {
            $this->db->where('cr_order_detail.name', $this->input->post('client'));
        }
        if($this->input->post('driver'))
        {
            $this->db->where('cr_order_detail.driver_name', $this->input->post('driver'));
        }
        if($this->input->post('vendor'))
        {
            $this->db->where('cr_order_detail.supplier_id', $this->input->post('vendor'));
        }
        if($this->input->post('date_type'))
        {
            $dt=$this->input->post('date_type');
            if($dt=="from_to"){
                $from_date=$this->input->post('from_date');
                $to_date=$this->input->post('to_date');
                $from=date('Y-m-d',strtotime($from_date));
                $to = date('Y-m-d',strtotime($to_date));
                $this->db->where("cr_orders.pickup_date BETWEEN '".$from."' AND '".$to."'");
            }else if($dt=="cday"){
                $this->db->like('cr_orders.pickup_date',date('Y-m-d'));
            }else if($dt=="pday"){
                $this->db->like('cr_orders.pickup_date',date('Y-m-d',strtotime("-1 days")));
            }else if($dt=="cmonth"){
                $cmonth= date('Y-m-01');
                $lastday= date('t',strtotime($cmonth));
                $emonth= date('Y-m-'.$lastday.' 23:59:59');
                $this->db->where("cr_orders.pickup_date BETWEEN '$cmonth' AND '$emonth'");
            }else if($dt=="pmonth"){
                // $cmonth= date('Y-m-01');
                // $lastday= date('t',strtotime($cmonth));
                // $emonth= date('Y-m-'.$lastday.'  23:59:59');
                $pmonth= date('Y-m', strtotime('-' . date('d') . ' days'));
                $lastday= date('t',strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                $emonth= date('Y-m-'.$lastday,strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // $pmonth= date('Y-m',strtotime("-1 months"));
                // $lastday= date('t',strtotime($pmonth));
                // $emonth= date('Y-m-'.$lastday,strtotime("-1 months"));
                $this->db->where("cr_orders.pickup_date BETWEEN '$pmonth' AND '$emonth'");
            }else{
                $this->db->like('cr_orders.pickup_date','');
            }
        }
        $this->db->group_by('cr_users.id');

    }

    function get_driver_datatable(){
        $this->_get_driver_datatable_query();
        // $column_order = array('order_id','driver_name','rent_type','name','user_lname','car_number','pickup_date','drop_date','profit','driver_ot_hour',null,'net_income'); 
        // if($this->input->post('order_by')){
        //     $this->db->order_by($this->input->post('order_by'), 'DESC');
        // }else{
        //     if(isset($_POST['order'])) // here order processing
        //     {
        //         $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        //     } 
        //     else if(isset($this->order))
        //     {
        //         $order = $this->order;
        //         $this->db->order_by(key($order), $order[key($order)]);
        //     }
        // }
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function get_order_datatable(){
        $this->_get_trip_detail_datatable_query();
        $column_order = array('order_id','user_lname','rent_type','name','driver_name','car_number','car_type','pickup_date','drop_date','amount',null,'profit'); 
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();    
       // echo $this->db->last_query();die("dgfdfgs");
        return $query->result();
    }

    public function _get_withdraw_datatable_query(){
        $column_order = array(null,'tran_date','account_name','account_price',null);         
        $this->db->select('ums_transfer.*');
        $this->db->select('ums_account.account_name');
        $this->db->select('ums_account.account_number');
        $this->db->select('ums_account.account_type');
        $this->db->select('ums_account.account_price');
        $this->db->select('ums_account.account_dollar');
        $this->db->select('ums_bank.ums_bank');
        $this->db->from('ums_transfer');
        $this->db->join('ums_account','ums_account.account_id = ums_transfer.account_id','inner');
        $this->db->join('ums_bank','ums_bank.ums_id = ums_account.account_bank','inner');
        // $this->db->order_by('ums_bank.ums_id','asc');
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
        if(isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'] / 2, $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    public function _get_deposit_datatable_query(){
        $column_order = array(null,'date','account_name','total_price',null);
        $this->db->select('payment.*');
        $this->db->select('ums_account.account_name');
        $this->db->select('ums_account.account_number');
        $this->db->select('ums_account.account_type');
        $this->db->select('ums_account.account_price');
        $this->db->select('ums_account.account_dollar');
        $this->db->select('ums_bank.ums_bank');
        $this->db->from('payment');
        $this->db->join('ums_account','ums_account.account_id = payment.account_id','inner');
        $this->db->join('ums_bank','ums_bank.ums_id = ums_account.account_bank','inner');
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
        $this->db->order_by('payment.pay_id','desc');
        if(isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'] / 2, $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function _get_financial_datatable_query(){
        $column_order = array("created_at",'transaction_date','category_id','description','created_by','cash_type','payment_person','amount','amount','final_balance');
        // $this->db->select('transations_recs.*');
        // $this->db->select('payment.*');
        // $this->db->select('payment.*');
        // $this->db->select('payment.*');
        // $this->db->select('payment.*');

        // SELECT
        $this->db->select('transations_recs.*');
        $this->db->select('ums_account.account_id');
        $this->db->select('ums_account.account_name');
        $this->db->select('ums_account.account_number');
        $this->db->select('cr_trip_expense.order_id');
        $this->db->select('cr_trip_expense.is_latest');
        $this->db->select('payment.pay_id');
        $this->db->select('payment.pay_price');
        $this->db->select('payment.pay_dollar');
        $this->db->select('payment.one_dollar');
        $this->db->select('ums_transfer.tran_id');
        $this->db->select('ums_transfer.transfer_amount');
        $this->db->select('ums_transfer.transfer_dollar');
        $this->db->select('cr_users.user_fname,');
        $this->db->select('cr_users.user_lname');

        $this->db->from('transations_recs');

        $this->db->join('ums_account','ums_account.account_id = transations_recs.account_id','LEFT');
        $this->db->join('ums_transfer','transations_recs.linked_id = ums_transfer.tran_id','LEFT');
        $this->db->join('payment','transations_recs.linked_id = payment.pay_id','LEFT');
        $this->db->join('cr_trip_expense','transations_recs.linked_id = cr_trip_expense.id','LEFT');
        $this->db->join('cr_users','cr_users.id = transations_recs.created_by','LEFT');
        $this->db->join('users','users.id = transations_recs.created_by','LEFT');
        // $this->db->order_by('transations_recs.id','desc');

        if($this->input->post('account'))
        {
            $this->db->where('transations_recs.account_id', $this->input->post('account'));
        }
        if($this->input->post('category'))
        {
            $this->db->where('transations_recs.category_id', $this->input->post('category'));
        }
        if($this->input->post('processed_by'))
        {
            $this->db->where('cr_users.id', $this->input->post('processed_by'));
        }else if($this->input->post('air_processed_by'))
        {
            $this->db->where('users.id', $this->input->post('air_processed_by'));
        }



        if($this->input->post('payment_type'))
        {
            $this->db->where('transations_recs.payment_type', $this->input->post('payment_type'));
        }
        
        if($this->input->post('payment_person'))
        {
            $this->db->where('transations_recs.payment_person', $this->input->post('payment_person'));
        }

        

        //Transaction Dates
        if ($this->input->post('transaction_from_date')) {
            $this->db->where("transations_recs.transaction_date >=", strtotime($this->input->post('transaction_from_date')));
        }
        if ($this->input->post('transaction_to_date')) {
            $this->db->where("transations_recs.transaction_date <=", strtotime($this->input->post('transaction_to_date')));
        }
        if($this->input->post('date_type'))
        {
            $dt=$this->input->post('date_type');
            if($dt=="from_to"){
                $from_date=$this->input->post('from_date');
                $to_date=$this->input->post('to_date');
                $from=strtotime($from_date);
                // $to = strtotime($to_date);
                $to = strtotime(date('Y-m-d 23:59:59',strtotime($to_date)));
                // $this->db->where("transations_recs.created_at BETWEEN '".$from."' AND '".$to."'");
                $this->db->where("transations_recs.created_at >=", $from);
                $this->db->where("transations_recs.created_at <=", $to);
                // $this->db->like("transations_recs.created_at",$to);
                // echo "transations_recs.created_at BETWEEN '".$from."' AND '".$to."'";
            }else if($dt=="cday"){
                // echo strtotime(date('Y-m-d 23:59:59'))."<br/>";
                $this->db->where('transations_recs.created_at BETWEEN '.strtotime(date('Y-m-d')).' AND '.strtotime(date('Y-m-d 23:59:59')));
            }else if($dt=="pday"){
                $this->db->where('transations_recs.created_at BETWEEN '.strtotime(date('Y-m-d 00:00:00',strtotime("-1 days"))).' AND '.strtotime(date('Y-m-d 23:59:59',strtotime("-1 days"))));
                // $this->db->like('transations_recs.created_at',strtotime(date('Y-m-d',strtotime("-1 days"))));
            }else if($dt=="cmonth"){
                $cmonth= strtotime(date('Y-m-01'));
                // echo $cmonth."<br/>";
                $lastday= date('t',strtotime($cmonth));
                $emonth= strtotime(date('Y-m-'.$lastday.' 23:59:59'));
                $this->db->where("transations_recs.created_at BETWEEN '$cmonth' AND '$emonth'");
                // echo "transations_recs.created_at BETWEEN '$cmonth' AND '$emonth'";
                // echo $lastday;
            }else if($dt=="pmonth"){
                $pmonth= strtotime(date('Y-m', strtotime('-' . date('d') . ' days')));
                $lastday= date('t',strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // echo $pmonth."<br/>";
                $emonth= strtotime(date('Y-m-'.$lastday,strtotime(date('Y-m', strtotime('-' . date('d') . ' days')))));
                $this->db->where("transations_recs.created_at BETWEEN '$pmonth' AND '$emonth'");
                // echo "transations_recs.created_at BETWEEN '$pmonth' AND '$emonth'";
            }else{
                $this->db->like('transations_recs.created_at','');
            }
        }
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
                // echo $column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']."<br/>";
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
        $this->db->order_by('transations_recs.created_at','desc');
    }

    function get_financial_datatable(){
        $this->_get_financial_datatable_query();
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();   
        return $query->result();
    }
    function get_all_financial_datatable(){
        $this->_get_financial_datatable_query();
        // if(isset($_POST['length']) && $_POST['length'] != -1)
        // $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();   
        return $query->result();
    }

    public function count_financial_filtered(){
        $this->_get_financial_datatable_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_order_datatable(){
        $this->_get_trip_detail_datatable_query();
        // if(isset($_POST['length']) && $_POST['length'] != -1)
        // $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();   
        return $query->result();
    }

    public function count_order_filtered(){
        $this->_get_trip_detail_datatable_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_maintain_filtered(){
        $this->_get_maintenance_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_fuel_filtered(){
        $this->_get_fuel_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_driver_filtered(){
        $this->_get_driver_datatable_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
}