<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "/third_party/Tcpdf/tcpdf_include.php";
class PDFModels extends MY_Model
{
    private $_ci;
    public function __construct()
    {
        parent::__construct();
        $this->_ci = &get_instance();
        // $this->_ci->load->model('Car_manager/Car_manager', 'car_manager');
        $this->_ci->load->model('Users/User', 'user');
        $this->_ci->load->model('Accounts/Accounts', 'accounts');
        $this->_ci->load->model('Car_manager/Car_maintain', 'car_maintain');
        $this->_ci->load->model('Attributes/Attributes', 'attributes'); // Load User modal
        $role = $this->session->userdata('cr_user_role');

    }

    function pdfInit($pdf,$title){

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Sonic Star Rental');
        $pdf->SetTitle($title.' - Print ' . date('Y-m-d'));
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', array(0, 0, 0), array(255, 255, 255));
        $pdf->setFooterData(array(0, 64, 0), array(186, 34, 30));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', 20));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('L', 'A4');
       
        // set text shadow effect
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
    }

    public function financialReport($list)
    {

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Sonic Star Rental');
        $pdf->SetTitle('Financial Report - Print ' . date('Y-m-d'));
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', array(0, 0, 0), array(255, 255, 255));
        $pdf->setFooterData(array(0, 64, 0), array(186, 34, 30));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', 20));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('L', 'A4');
        $html = <<<EOF
        <style>
            .result-th{
                color:#ffffff;
                background-color: #BA221E;
                font-size: 10pt;
                text-align: center;
            }
            .result-td{
                font-size: 8pt;
                border-left: 1px solid #efefef;
                border-right: 1px solid #efefef;
            }
            td{
                font-size: 8pt;
            }
            .cus{
                font-size: 11pt;
                line-height:13px;
            }
            h3{
                line-height:0px;
            }
        </style>


EOF;
        // set text shadow effect
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
        // $html = $style."";
        // if($pdf->getaliasnumpage()==1){
        //     echo "hi";
        // }
        $querylist = $this->rmodels->get_all_financial_datatable();

        $deposit_total_mmk  = 0;
        $withdraw_total_mmk = 0;
        $deposit_total_usd  = 0;
        $withdraw_total_usd = 0;
        // print "<pre/>";
        // print_r($querylist);
        foreach ($querylist as $info) {
            if ($info->cash_type == 1) {
                $deposit_total_mmk += ($info->payment_type == 4417) ? $info->amount : 0;
                $withdraw_total_mmk += ($info->payment_type == 4416) ? $info->amount : 0;
            }
            if ($info->cash_type == 2) {
                $deposit_total_usd += ($info->payment_type == 4417) ? $info->amount : 0;
                $withdraw_total_usd += ($info->payment_type == 4416) ? $info->amount : 0;
            }
        }
// ()? $this->input->post('account') : '-' )
        $account_name    = "-";
        $account_balance = "-";
        $currency        = "";
        if (!empty($this->input->post('account'))) {
            $account      = $this->accounts->get_by("account_id", $this->input->post('account'));
            $account_name = $account->account_name;
            if ($account->account_type == 1) {
                $currency        = "MMK ";
                $account_balance = "MMK " . number_format($account->account_price);
            }
            if ($account->account_type == 2) {
                $currency        = "$ ";
                $account_balance = "$ " . number_format($account->account_dollar);
            }
            // $this->account->get_by("id",);
        }

        $date_range_text = "";

        if (!empty($this->input->post('date_type'))) {
            $dt = $this->input->post('date_type');
            if ($dt == "from_to") {
                $from_date       = $this->input->post('from_date');
                $to_date         = $this->input->post('to_date');
                $date_range_text = "Processed / Created Date : " . $from_date . " - " . $to_date;

            } else if ($dt == "cday") {
                $date_range_text = "Processed / Created Date : " . date('Y-m-d');
            } else if ($dt == "pday") {
                $date_range_text = "Processed / Created Date : " . date('Y-m-d', strtotime("-1 days"));
            } else if ($dt == "cmonth") {
                $cmonth          = date('Y-m-01');
                $lastday         = date('t', strtotime($cmonth));
                $emonth          = date('Y-m-' . $lastday);
                $date_range_text = "Processed / Created Date : From : " . $cmonth . " - To : " . $emonth;
                // $this->db->where("transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'");
                // echo "transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'";
            } else if ($dt == "pmonth") {
                $pmonth  = date('Y-m-1', strtotime("-1 months"));
                $lastday = date('t', strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // echo $pmonth."<br/>";
                $emonth = date('Y-m-' . $lastday, strtotime(date('M Y-m', strtotime('-' . date('d') . ' days'))));
                // $this->db->where("transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'");
                $date_range_text = "Processed / Created Date : From : " . $pmonth . " - To : " . $emonth;
                // echo "transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'";
            }
        }
        $html .= '
        <h3 align="center">Financial Report</h3>
        <table width="100%" >
            <tr>
                <td width="80%">
                    <table>

                        <tr>
                            <td width="15%">Processed By</td>
                                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($this->input->post('processed_by')) ? $this->user->get_by("id", $this->input->post('processed_by'))->user_lname : 'All') . '</td>
                            <td width="15%">Account</td>
                            <td width="5%">:</td>
                            <td width="25%">' . $account_name . '</td>
                        </tr>
                        <tr>
                            <td width="15%">Payment From / To</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($this->input->post('payment_person')) ? $this->input->post('payment_person') : 'All') . '</td>
                            <td width="15%">Avaliable Balance</td>
                            <td width="5%">:</td>
                            <td width="25%">' . $account_balance . '</td>
                        </tr>
                        <tr>
                            <td width="15%">Category</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($this->input->post('category')) ? $this->attributes->get_by('id', $this->input->post('category'))->title : 'All') . '</td>

                        </tr>
                        <tr>

                            <td width="15%">Deposit / Withdraw</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($this->input->post('payment_type')) ? $this->attributes->get_by('id', $this->input->post('payment_type'))->title : 'Both') . '</td>


                        </tr>
                    </table>
                </td>
                <td width="20%">
                    <table>
                        <tr>
                          <td></td>
                          <td align="right">MMK</td>
                          <td align="right">USD</td>
                        </tr>
                        <tr>
                          <td>Deposit </td>
                          <td id="depositAmountMMK" align="right">' . number_format($deposit_total_mmk) . '</td>
                          <td id="depositAmountUSD" align="right">' . number_format($deposit_total_usd) . '</td>
                        </tr>
                        <tr>
                          <td>Withdraw </td>
                          <td id="withdrawAmountMMK" align="right">' . number_format($withdraw_total_mmk) . '</td>
                          <td id="withdrawAmountUSD" align="right">' . number_format($withdraw_total_usd) . '</td>
                        </tr>
                    </table>
                </td>
            </tr>


        </table>

        <p class="cus">' . $date_range_text . '</p>';

//         <td width="15%">Deposit Total</td>
        //                             <td width="5%">:</td>
        //                             <td width="25%">'.$currency.number_format($deposit_total).'</td>
        // <td width="15%">Withdraw Total</td>
        //                             <td width="5%">:</td>
        //                             <td width="25%">'.$currency.number_format($withdraw_total).'</td>
        // Set some content to print
        $html .= '<table width="100%" >
                <thead width="100%">
                    <tr>
                        <th class="result-th" width="10%">Processed At</th>
                        <th class="result-th" width="10%">Transaction Date</th>
                        <th class="result-th" width="10%">Category</th>
                        <th class="result-th" width="17.5%">Description</th>
                        <th class="result-th" width="10%">Process By</th>
                        <th class="result-th" width="10%">Account</th>
                        <th class="result-th" width="7.5%">Payment To/From</th>
                        <th class="result-th" width="7.5%">Deposit</th>
                        <th class="result-th" width="7.5%">Withdraw</th>
                        <th class="result-th" width="10%">Final Balance</th>
                    </tr>
                </thead>';
        $html .= '<tbody width="100%">';
        // $pgno= "this page " .  . " of " . $pdf->getaliasnbpages();
        //
        //
        //
        //
        //             <td>'.($info->payment_type == 4417) ? number_format($info->amount) : '-'.'</td>
        //             <td>'.($info->payment_type == 4416) ? number_format($info->amount) : '-'.'</td>
        //             <td>'.number_format($info->final_balance).'</td>
        foreach ($list as $key => $info) {
            // if($pdf->PageNo()>1){
            $html .= '<tr nobr="true" style="font-size: xx-small;' . (($key % 2 == 0) ? 'background-color:#efefef' : 'background-color:#FFFFFF') . ';border:1px solid #eeeeee;line-height:20px">
                        <td class="result-td" width="10%">' . date('m-d-Y / h:i', $info->created_at) . '</td>
                        <td class="result-td" width="10%">' . date('m-d-Y', $info->transaction_date) . '</td>
                        <td class="result-td" width="10%">' . $this->attributes->get_by('id', $info->category_id)->title . '</td>
                        <td class="result-td" width="17.5%">' . $info->description . '</td>
                        <td class="result-td" width="10%">' . 
                        (($info->source_from=="flight" || !isset($info->user_lname))?
                            $this->airline_user->get_by('id',$info->created_by)->name:$info->user_lname)
                         . '</td>
                        <td class="result-td" width="10%">' . $info->account_name . '</td>
                        <td class="result-td" width="7.5%">' . $info->payment_person . '</td>
                        <td class="result-td" align="right" width="7.5%">' . (($info->payment_type == 4417) ? number_format($info->amount) : '-') . '</td>
                        <td class="result-td" align="right" width="7.5%">' . (($info->payment_type == 4416) ? number_format($info->amount) : '-') . '</td>
                        <td class="result-td" align="right" width="10%">' . number_format($info->final_balance) . '</td>
                    </tr>';
            // }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        // echo $html;
        // Print text using writeHTMLCell()

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $pdf->SetAutoPageBreak(true, 0);
        $path = BASEPATH . '../';
        // echo $path;
        // Supply a filename including the .pdf extension
        $filename = 'financial_report_' . time() . '.pdf';

        // Create the full path
        $full_path = $path . '/' . $filename;
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output($full_path, 'F');
        return $filename;

        //============================================================+
        // END OF FILE
        //============================================================+

    }

    public function orderReport($list)
    {

        // // create new PDF document
        // $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // // set document information
        // $pdf->SetCreator(PDF_CREATOR);
        // $pdf->SetAuthor('Sonic Star Rental');
        // $pdf->SetTitle('Order Report - Print ' . date('Y-m-d'));
        // $pdf->SetSubject('');
        // $pdf->SetKeywords('');

        // // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', array(0, 0, 0), array(255, 255, 255));
        // $pdf->setFooterData(array(0, 64, 0), array(186, 34, 30));

        // // set header and footer fonts
        // $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', 20));
        // $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // // set default monospaced font
        // $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // // set margins
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // // set auto page breaks
        // $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // // set image scale factor
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // // set some language-dependent strings (optional)
        // if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        //     require_once dirname(__FILE__) . '/lang/eng.php';
        //     $pdf->setLanguageArray($l);
        // }

        // // ---------------------------------------------------------

        // // set default font subsetting mode
        // $pdf->setFontSubsetting(true);

        // // Set font
        // // dejavusans is a UTF-8 Unicode font, if you only need to
        // // print standard ASCII chars, you can use core fonts like
        // // helvetica or times to reduce file size.
        // $pdf->SetFont('dejavusans', '', 14, '', true);

        // // Add a page
        // // This method has several options, check the source code documentation for more information.
        // $pdf->AddPage('L', 'A4');
        $html='<img src="https://www.sonicstartravel.com/flight/public/imgs/sonic_star_logo.png" alt="" style="width: 200px;"">';
//         $html = <<<EOF
//         <style>
//             .result-th{
//                 color:#ffffff;
//                 background-color: #BA221E;
//                 font-size: 10pt;
//                 text-align: center;
//             }
//             .result-td{
//                 font-size: 8pt;
//                 border-left: 1px solid #efefef;
//                 border-right: 1px solid #efefef;
//             }
//             td{
//                 font-size: 8pt;
//             }
//             .cus{
//                 font-size: 11pt;
//                 line-height:13px;
//             }
//             h3{
//                 line-height:0px;
//             }

//             .sub-result-td{
//                 font-size: 8pt;
//                 border-left: 1px solid #efefef;
//                 border-right: 1px solid #efefef;
//                 border-top: 1px solid #7b7b7b;
//                 border-bottom: 1px solid #000000;
//             }

//         </style>


// EOF;
        // set text shadow effect
        // $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        $querylist = $this->rmodels->get_all_order_datatable();

        $total_rental_fees   = 0;
        
        $paid_vendor_fees   = 0;
        $total_vendor_fees   = 0;
        $total_agent_fees    = 0;
        $total_agent_paid    = 0;
        $total_customer_paid    = 0;
        $total_fuel_expense  = 0;
        $total_toll_expense  = 0;
        $total_other_expense = 0;
        $total_driver_ot     = 0;
        $total_driver_bonus  = 0;
        $total_net_income    = 0;
        $_total_net_income    = 0;
        $total_profit        = 0;
        $total_driver_salary = 0;
        $_driver_bonus       = 0;
        $___driver_bonus     = 0;
        $__vendor_expense    = 0;
        $agents    = $this->user->get_many_by("user_role", "agent");
        foreach ($querylist as $info) {
            if($info->status!="Cancelled"){
                $total_rental_fees += ($info->amount+($info->driver_ot_fees*$info->ot_hours));
                $total_vendor_fees += $info->vendor_fees;
            
                $paid_vendor_fees += $info->vendor_total;
                $total_toll_expense += $info->toll_expense_total;
                $total_fuel_expense += $info->fuel_expense_total;
                $total_other_expense += $info->other_expense_total;
                $total_customer_paid += $info->customer_total;
                $total_agent_paid += $info->agent_total;
                $pickup_date = date('m/d/Y', strtotime($info->pickup_date));
                $drop_date   = date('m/d/Y', strtotime($info->drop_date));
                $timeDiff    = strtotime($info->drop_date) - strtotime($info->pickup_date);
                $diffDays    = ceil($timeDiff / 86400);

                $orderinfo = $this->orders->get_order($info->order_id);
                
                
                foreach ($agents as $k => $agent) {
                    if ($orderinfo->user_id == $agent->id) {
                        $total_agent_fees+=$info->agent_fees;   
                    }
                }
                $total_driver_ot += ($info->driver_ot_fees * $info->ot_hours);
                $net = 0;
                // if ($info->status == "Completed") {
                //     $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                // } else {
                //     $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                //     // echo $info->order_id." -> ".$net."\n";
                // }
                if ($info->status == "Completed") {
                    $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                } else {
                        if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                            // $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                            $net = (($info->amount+($info->driver_ot_fees*$info->ot_hours))-$info->agent_fees) - $info->vendor_fees;
                        }else{
                            $net = ($info->amount+($info->driver_ot_fees*$info->ot_hours)) - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? ((1500 * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                        }
                    
                }

                if ($info->type_of_rent == 38 || $info->type_of_rent == 37) {
                    if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                        $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                    }else{
                        $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + ($info->type_of_rent != 38? ($diffDays * $info->driver_fees):0) + (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                    }
                }



                $_total_net_income+=$net;
                $__driver_bonus   = 0;
                $_vendor_expense  =0;
                if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
                    //  $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                    // $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
                    // $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                    // $_vendor_expense +=$driver_bonus;
                    // $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;


                    $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                    $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
                    $total_driver_bonus   += ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                    $__driver_bonus = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                    // $_vendor_expense +=$__driver_bonus;
                }

                $__vendor_expense+=(($info->user_lname != "Sonic Star"  && $info->driver_from==1)? ($_vendor_expense) : 0);
                if($info->type_of_rent != 38){
                    $total_net_income+=(($info->user_lname != "Sonic Star"  && $info->driver_from==1)? ($net+$_vendor_expense) : $net);

                }else{
                    $total_net_income+=$net;

                }

                $_driver_bonus += (($info->driver_from==1 && $info->user_lname != "Sonic Star")?0:$__driver_bonus);
                $___driver_bonus += (($info->driver_from==1 && $info->user_lname != "Sonic Star")?$__driver_bonus:0);
                $total_driver_salary+=$info->driver_salary;
            }
        }
        // -$total_driver_bonus
        $total_profit=$_total_net_income-$total_driver_salary-$_driver_bonus;
        $total_net_income=$total_net_income+$___driver_bonus;
        $total_vendor_fees = $total_vendor_fees-($__vendor_expense+$___driver_bonus);
        $date_range_text = "";

        if (!empty($_GET['date_type'])) {
            $dt = $_GET['date_type'];
            if ($dt == "from_to") {
                $from_date       = $_GET['from_date'];
                $to_date         = $_GET['to_date'];
                $date_range_text = "Order Date : " . $from_date . " - " . $to_date;

            } else if ($dt == "cday") {
                $date_range_text = "Order Date : " . date('Y-m-d');
            } else if ($dt == "pday") {
                $date_range_text = "Order Date : " . date('Y-m-d', strtotime("-1 days"));
            } else if ($dt == "cmonth") {
                $cmonth          = date('Y-m-01');
                $lastday         = date('t', strtotime($cmonth));
                $emonth          = date('Y-m-' . $lastday);
                $date_range_text = "Order Date : From : " . $cmonth . " - To : " . $emonth;
                // $this->db->where("transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'");
                // echo "transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'";
            } else if ($dt == "pmonth") {
                $pmonth  = date('Y-m-1', strtotime("-1 months"));
                $lastday = date('t', strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // echo $pmonth."<br/>";
                $emonth = date('Y-m-' . $lastday, strtotime(date('M Y-m', strtotime('-' . date('d') . ' days'))));
                // $this->db->where("transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'");
                $date_range_text = "Order Date : From : " . $pmonth . " - To : " . $emonth;
                // echo "transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'";
            }
        }
        $pickup_date  = "";
        $dropoff_date = "";

        if (!empty($_GET['pickup_date_from']) && !empty($_GET['pickup_date_to'])) {
            $pickup_date = "| Pick-Up Date : " . date('Y-m-d', strtotime($_GET['pickup_date_from'])) . " - " . date('Y-m-d', strtotime($_GET['pickup_date_to']));
        } else if (!empty($_GET['pickup_date_from'])) {
            $pickup_date = "| Pick-Up Date : " . date('Y-m-d', strtotime($_GET['pickup_date_from']));
        } else if (!empty($_GET['pickup_date_to'])) {
            $pickup_date = "| Pick-Up Date : " . date('Y-m-d', strtotime($_GET['pickup_date_to']));
        }

        if (!empty($_GET['dropoff_date_from']) && !empty($_GET['dropoff_date_to'])) {
            $dropoff_date = "| Drop-Off Date : " . date('Y-m-d', strtotime($_GET['dropoff_date_from'])) . " - " . date('Y-m-d', strtotime($_GET['dropoff_date_to']));
        } else if (!empty($_GET['dropoff_date_from'])) {
            $dropoff_date = "| Drop-Off Date : " . date('Y-m-d', strtotime($_GET['dropoff_date_from']));
        } else if (!empty($_GET['dropoff_date_to'])) {
            $dropoff_date = "| Drop-Off Date : " . date('Y-m-d', strtotime($_GET['dropoff_date_to']));
        }

        $user_company="";
        if(!empty($_GET['vendor']) && $_GET['vendor']!="only-vendor") {
            $user_company= $this->user->get_by("id", $_GET['vendor'])->user_company;
        }else if(!empty($_GET['vendor']) && $_GET['vendor']=="only-vendor") {
            $user_company= "Only Vendor";
        }else{
            $user_company= "All";
        }
        $html .= '

        <h3 align="center">Order Report</h3>
        <table width="100%">
            <tr>
                <td width="50%">
                    <table>

                        <tr>
                            <td width="20%">Rent Type</td>
                            <td width="5%">:</td>';
                            // if($this->input->post('rent_type'))
                            // {
                                // if($this->input->post('rent_type')=="full-day" || $this->input->post('rent_type')=="half-day"){
                                //     $html .= '<td width="25%">' . ucfirst($this->input->post('rent_type')) .'</td>';
                                // }else{
                            $html .= '<td width="25%">' . (!empty($_GET['rent_type']) ? $this->attributes->get_by("id", $_GET['rent_type'])->title : 'All').
                                (!empty($_GET['trip_type'])? ' ('.ucfirst($_GET['trip_type']).')' :'')
                             . '</td>';         
                                // }
                            // }
        
         $html .=       '<td width="20%">Driver</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_GET['driver']) ? ucfirst($_GET['driver']) : 'All') . '</td>
                        </tr>
                        <tr>
                            <td width="20%">Client</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_GET['client']) ? $_GET['client'] : 'All') . '</td>
                            <td width="20%">Vendor</td>
                            <td width="5%">:</td>
                            <td width="25%">' . $user_company. '</td>
                        </tr>
                        <tr>
                            <td width="20%">Order Status</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_GET['order_status']) ? $_GET['order_status'] : 'All') . '</td>
                            <td width="20%">Customer Status</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_GET['customer_status']) ? ucfirst($_GET['customer_status']) : 'All') . '</td>
                        </tr>
                        <tr>
                            <td width="20%">Vendor Status</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_GET['vendor_status']) ? ucfirst($_GET['vendor_status']) : 'All') . '</td>
                            <td width="20%">Agent Status</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_GET['agent_status']) ? ucfirst($_GET['agent_status']) : 'All') . '</td>
                        </tr>

                    </table>
                </td>
                <td width="50%">
                    <table>
                        <tr>
                            <td width="24%">Total Rental Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_rental_fees) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Total Fuel Cost</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_fuel_expense) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Vendor Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_vendor_fees) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Total Toll Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_toll_expense) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Agent Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_agent_fees) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Other Expense</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_other_expense) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Driver OT Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_driver_ot) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Total Driver Bonus</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_driver_bonus) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Vendor Paid</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($paid_vendor_fees) . '</td>
                            <td width="1%"></td>
                            <td width="24%">Total Customer Paid</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_customer_paid) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Agent Paid</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_agent_paid) . '</td>
                            <td width="1%"></td>
                            <td width="24%">Total Profit</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_profit) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Net</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_net_income) . '</td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


        <p class="cus"><span style="font-size:8pt">' . $date_range_text . ' ' . $pickup_date . $dropoff_date . '</span></p>';

        $html .= '<table width="100%" >
                <thead width="100%">
                    <tr>
                        <th class="result-th" width="5%">Serial No</th>
                        <th class="result-th" width="10%">Trip Record Name</th>
                        <th class="result-th" width="15%">Status</th>
                        <th class="result-th" width="10%">Supplier Name</th>
                        <th class="result-th" width="5%">Trip Type</th>
                        <th class="result-th" width="10%">Client</th>
                        <th class="result-th" width="5%">Car Info</th>
                        <th class="result-th" width="5%">Driver</th>
                        <th class="result-th" width="10%">Driver Cost</th>
                        <th class="result-th" width="7.5%">Trip Start Date</th>
                        <th class="result-th" width="7.5%">Trip End<br/>Date</th>
                        <th class="result-th" width="10%">Fees</th>
                    </tr>
                </thead>';
        $html .= '<tbody width="100%">';
        $suppliers = $this->user->get_many_by("user_role", "supplier");
        $agents    = $this->user->get_many_by("user_role", "agent");
        foreach ($list as $key => $info) {
            $vendor_status = "";
            $agent_status  = "";
            $order_status  = "Order:";
            $fees          = "Rental:" . number_format($info->amount+($info->driver_ot_fees*$info->ot_hours));
            if ($info->status == "Completed") {
                $order_status .= '<font color="white" style="background-color: blue">' . $info->status . '</font>';
            } else if ($info->status == "Confirmed") {
                $order_status .= '<font style="background-color: #00c0ef">' . $info->status . '</font>';
            } else if ($info->status == "Cancelled") {
                $order_status .= '<font color="white" style="background-color: red">' . $info->status . '</font>';
            } else {
                $order_status .= '<font style="background-color: Yellow">' . $info->status . '</font>';
            }

            $customer_status = ($info->customer_payment_status == "complete") ? '<br/>Customer: <font color="white"  style="background-color: blue">Complete</font>' : '<br/>Customer: <font style="background-color: Yellow">Pending</font>';

            $orderinfo = $this->orders->get_order($info->order_id);
            $_vendor_expense1=0;
            // echo $info->driver_from;
            // if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
            //     // echo "hello";
            //     $_vendor_expense1=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
            //     $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense1 :$net;
            //     $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
            //     $_vendor_expense1 +=$driver_bonus;
            //     // $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
            //     // $wages="<br/>Wages:" . number_format($diffDays * $info->driver_fees);
            //     // $row[]          = "OT:" . number_format($info->driver_ot_fees * $info->ot_hours) . $wages . "<br/>Bonus:" . number_format($driver_bonus);
            // }
            
            foreach ($suppliers as $k => $supplier) {
                if ($orderinfo->supplier_id == $supplier->id) {
                    if ($supplier->user_lname != 'Sonic Star') {
                        $vendor_status = "<br/>Vendor: " . ($info->vendor_payment_status == "complete" ? '<font color="white"  style="background-color: blue">Complete</font>' : '<font style="background-color: Yellow">Pending</font>') . "\n";
                        // $fees .= '<br/><font color="white" style="background-color: blue">Vendor:' . number_format($info->vendor_fees - $_vendor_expense1).'</font>';
                        
                    }
                }
            }
            foreach ($agents as $k => $agent) {
                if ($orderinfo->user_id == $agent->id) {
                    $agent_status = "<br/>Agents: " . ($info->agent_payment_status == "complete" ? '<font color="white"  style="background-color: blue">Complete</font>' : '<font style="background-color: Yellow">Pending</font>') . "\n";
                    $fees .= "<br/>Agent:" . number_format($info->agent_fees);
                }
            }

            $pickup_date = date('m/d/Y', strtotime($info->pickup_date));
            $drop_date   = date('m/d/Y', strtotime($info->drop_date));
            $timeDiff    = strtotime($info->drop_date) - strtotime($info->pickup_date);
            $diffDays    = ceil($timeDiff / 86400);
            // if ($info->status == "Completed") {
            //     $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            // } else {
            //     $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            // }
            // if ($info->type_of_rent==38 || $info->type_of_rent==37) {
            //     $net=$info->amount - ($info->vendor_fees+$info->agent_fees+$info->fuel_expense_total+$info->toll_expense_total+$info->other_expense_total+ (($info->type_of_rent==38 || $info->type_of_rent==37)? $info->driver_salary : 0));
            //     // echo $net;
            // }
            $net=0;
            if ($info->status == "Completed") {
                $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            } else {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = (($info->amount+($info->driver_ot_fees*$info->ot_hours))-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = ($info->amount+($info->driver_ot_fees*$info->ot_hours)) - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? ((1500 * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                }
            }
            if ($info->type_of_rent == 38 || $info->type_of_rent == 37) {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + ($info->type_of_rent != 38? ($diffDays * $info->driver_fees):0) + (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                }
            }
            // if($pdf->PageNo()>1){
            $html .= '<tr nobr="true" style="font-size: xx-small;' . (($key % 2 == 0) ? 'background-color:#efefef' : 'background-color:#FFFFFF') . ';border:1px solid #eeeeee;line-height:200%">
                        <td class="result-td" width="5%"> ' . $info->order_id . '</td>
                        <td class="result-td" width="10%"> ' . $info->trip_record_name . '</td>
                        <td class="result-td" width="15%">' . $order_status . $customer_status . $vendor_status . $agent_status . '</td>
                        <td class="result-td" width="10%">' . $info->user_fname . ' ' . $info->user_lname . '</td>';
        
            if($info->rent_type==36){
                if($info->airport_status=="pickup" || $info->airport_status=="dropoff"){
                    $html .= '<td class="result-td" align="center" width="5%"> Airport '.ucfirst($info->airport_status). '</td>';
                    // $row[] = "Airport - ".ucfirst($info->airport_status);
                }else if($info->airport_status=="highway"){
                    $html .= '<td class="result-td" align="center" width="5%">' . $this->attributes->get_by('id', $info->rent_type)->title ."<br/>".ucfirst($info->airport_status). '</td>';
                
                }else{
                    $html .= '<td class="result-td" align="center" width="5%">' . $this->attributes->get_by('id', $info->rent_type)->title ."<br/>".ucfirst($info->daily_status). '</td>';
                }
            }else{
                 $html .= '<td class="result-td" align="center" width="5%">' . $this->attributes->get_by('id', $info->rent_type)->title . '</td>';
            }
            $html .= '<td class="result-td" width="10%">' . $info->name . '</td>
                        <td class="result-td" align="center" width="5%">' . $info->car_number . '<br/>' . $this->attributes->get_by('id', $info->model)->title . '</td>
                        <td class="result-td" width="5%">' . $info->driver_name . '</td>';
            $driver_bonus   = 0;
            $driver_expense = 0;
            // if ($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") {
            //     $driver_bonus = ($net > 0) ? $net * $info->driver_percentage / 100 : 0;
            //     $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
            $_vendor_expense=0;
            if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
                $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
                $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                $_vendor_expense +=$driver_bonus;
                $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
                $wages="<br/>Wages:" . number_format($diffDays * $info->driver_fees);
                $ot="<br/>CarOT:" . number_format(($info->driver_ot_fees * $info->ot_hours)-(1500 * $info->ot_hours));
                $html .= '<td class="result-td" align="left" width="10%">' . "OT:" . number_format(1500 * $info->ot_hours) .$ot. $wages."<br/>Bonus:" . number_format($driver_bonus) . '</td>';
            }else if($info->type_of_rent==38 || $info->type_of_rent==37){
                $driver_expense=$info->driver_salary;
                $html .= '<td class="result-td" align="left" width="10%">Driver Salary: '.number_format($info->driver_salary).'</td>';
                // $row[] = "Driver Salary:".;
            } else {
                $html .= '<td class="result-td" align="center" width="10%"></td>';
            }
            foreach ($suppliers as $k => $supplier) {
                if ($orderinfo->supplier_id == $supplier->id) {
                    if ($supplier->user_lname != 'Sonic Star') {
                        // $vendor_status = "<br/>Vendor: " . ($info->vendor_payment_status == "complete" ? '<font color="white"  style="background-color: blue">Complete</font>' : '<font style="background-color: Yellow">Pending</font>') . "\n";
                        $fees .= '<br/><font color="white" style="background-color: blue">Vendor:' . number_format($info->vendor_fees - $_vendor_expense).'</font>';
                        
                    }
                }
            }
            if($info->type_of_rent != 38){
                $fees .= "<br/>Net:".(($info->user_lname != "Sonic Star"  && $info->driver_from==1)? number_format($net+$_vendor_expense) : number_format($net));
            }else{
                $fees .= "<br/>Net:".number_format($net);
            }
            // $fees .= "<br/>Net:".(($info->user_lname != "Sonic Star"  && $info->driver_from==1)? number_format($net+$_vendor_expense) : number_format($net));
            $html .= '<td class="result-td" align="center" width="7.5%">' . date('D d M, Y', strtotime($info->pickup_date)) . '</td>
                        <td class="result-td" align="center" width="7.5%">' . date('D d M, Y', strtotime($info->drop_date)) . '</td>
                        <td class="result-td" align="right" width="10%">' . $fees . '</td>
                    </tr>';

            $html .= '<tr nobr="true" style="font-size: xx-small;' . (($key % 2 == 0) ? 'background-color:#efefef' : 'background-color:#FFFFFF') . ';border:1px solid #eeeeee;line-height:200%">
                            <td class="sub-result-td" colspan="11">';
            $html .= 'Paid by Customer :' . (!empty($info->customer_total) ? number_format($info->customer_total) . " MMK" : 0);

            $agent_fees    = 0;
            $vendor_fees   = 0;
            $fuel_expense  = 0;
            $toll_expense  = 0;
            $other_expense = 0;

            if ($info->user_lname != 'Sonic Star') {
                $vendor_fees = $info->vendor_total;
                $html .= ' | Paid to Vendor :' . (!empty($info->vendor_total) ? number_format($info->vendor_total) . " MMK" : 0);
            }
            // if(!empty($info->user_id) || $info->user_id!=""){

            $is_agent = $this->user->get_by('id', $info->user_id);
            if (isset($is_agent) && $is_agent->user_role == "agent") {
                $agent_fees = $info->agent_total;
                $html .= ' | Paid to Agent :' . (!empty($info->agent_total) ? number_format($info->agent_total) . " MMK" : 0);
            }

            // if ($info->user_lname == 'Sonic Star') {
                $fuel_expense = $info->fuel_expense_total;
                $html .= ' | Fuel Expense :' . (!empty($info->fuel_expense_total) ? number_format($info->fuel_expense_total) . " MMK" : 0);
            // }
            // $html.="<br/>";
            $toll_expense  = $info->toll_expense_total;
            $other_expense = $info->other_expense_total;
            if (($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star" || $info->driver_from==1)) || $info->type_of_rent == 38  || $info->type_of_rent==37) {
                $html .= ' | Driver Cost :' . number_format($driver_expense);

            }
            

            $html .= ' | Toll Fees :' . (!empty($info->toll_expense_total) ? number_format($info->toll_expense_total) . " MMK" : 0) . ' | Other Expense :' . (!empty($info->other_expense_total) ? number_format($info->other_expense_total) . " MMK" : 0);
            if($info->user_lname != "Sonic Star" && $info->driver_from==1){
                $html.=' | Vendor Fees : '.number_format($info->vendor_fees);
            }
            $html .= '   </td><td class="sub-result-td" align="center" ' . ($info->status == "Completed" ? 'style="background-color: blue;"' : 'style="background-color: yellow;"') . '>';
            if ($info->status == "Completed") {

                $html .= '<font color="white" > Profit : ' . number_format($net -$info->driver_salary - (($info->driver_from==1 && $info->user_lname != "Sonic Star")?0:$driver_bonus)) . '</font>';
            } else {
                $html .= '<font color="black" > Profit : ' . number_format($net -$info->driver_salary - (($info->driver_from==1 && $info->user_lname != "Sonic Star")?0:$driver_bonus)) . '</font>';
            }
            $html .= '</td></tr>';
            // }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        // echo $html;
        return $html;
        // Print text using writeHTMLCell()

        // $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // $pdf->SetAutoPageBreak(true, 0);
        // $path = BASEPATH . '../';
        // // echo $path;
        // // Supply a filename including the .pdf extension
        // $filename = 'order_report_' . time() . '.pdf';

        // // Create the full path
        // $full_path = $path . '/' . $filename;
        // // ---------------------------------------------------------

        // // Close and output PDF document
        // // This method has several options, check the source code documentation for more information.
        // $pdf->Output($full_path, 'F');
        // return $filename;

        //============================================================+
        // END OF FILE
        //============================================================+
    }
     public function vorderReport($list)
    {

        // // create new PDF document
        // $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // // set document information
        // $pdf->SetCreator(PDF_CREATOR);
        // $pdf->SetAuthor('Sonic Star Rental');
        // $pdf->SetTitle('Order Report - Print ' . date('Y-m-d'));
        // $pdf->SetSubject('');
        // $pdf->SetKeywords('');

        // // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', array(0, 0, 0), array(255, 255, 255));
        // $pdf->setFooterData(array(0, 64, 0), array(186, 34, 30));

        // // set header and footer fonts
        // $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', 20));
        // $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // // set default monospaced font
        // $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // // set margins
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // // set auto page breaks
        // $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // // set image scale factor
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // // set some language-dependent strings (optional)
        // if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        //     require_once dirname(__FILE__) . '/lang/eng.php';
        //     $pdf->setLanguageArray($l);
        // }

        // // ---------------------------------------------------------

        // // set default font subsetting mode
        // $pdf->setFontSubsetting(true);

        // // Set font
        // // dejavusans is a UTF-8 Unicode font, if you only need to
        // // print standard ASCII chars, you can use core fonts like
        // // helvetica or times to reduce file size.
        // $pdf->SetFont('dejavusans', '', 14, '', true);

        // // Add a page
        // // This method has several options, check the source code documentation for more information.
        // $pdf->AddPage('L', 'A4');
        $html='';
//         $html = <<<EOF
//         <style>
//             .result-th{
//                 color:#ffffff;
//                 background-color: #BA221E;
//                 font-size: 10pt;
//                 text-align: center;
//             }
//             .result-td{
//                 font-size: 8pt;
//                 border-left: 1px solid #efefef;
//                 border-right: 1px solid #efefef;
//             }
//             td{
//                 font-size: 8pt;
//             }
//             .cus{
//                 font-size: 11pt;
//                 line-height:13px;
//             }
//             h3{
//                 line-height:0px;
//             }

//             .sub-result-td{
//                 font-size: 8pt;
//                 border-left: 1px solid #efefef;
//                 border-right: 1px solid #efefef;
//                 border-top: 1px solid #7b7b7b;
//                 border-bottom: 1px solid #000000;
//             }

//         </style>


// EOF;
        // set text shadow effect
        // $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        $querylist = $this->rmodels->get_all_order_datatable();

        $total_rental_fees   = 0;
        $paid_vendor_fees    = 0;
        $total_vendor_fees   = 0;
        $total_agent_fees    = 0;
        $total_agent_paid    = 0;
        $total_customer_paid = 0;
        $total_fuel_expense  = 0;
        $total_toll_expense  = 0;
        $total_other_expense = 0;
        $total_driver_ot     = 0;
        $total_driver_bonus  = 0;
        $total_net_income    = 0;
        $total_profit        = 0;
        $vendor_expense=0;
        $agents    = $this->user->get_many_by("user_role", "agent");
        foreach ($querylist as $info) {
            if($info->status!="Cancelled" && ($info->user_lname != "Sonic Star"  || $info->driver_from!=1)){
                $total_rental_fees += $info->amount;
                $total_vendor_fees += $info->vendor_fees;
            }


            $paid_vendor_fees += $info->vendor_total;
            $total_toll_expense += $info->toll_expense_total;
            $total_fuel_expense += $info->fuel_expense_total;
            $total_other_expense += $info->other_expense_total;
            $total_customer_paid += $info->customer_total;
            $total_agent_paid += $info->agent_total;
            $pickup_date = date('m/d/Y', strtotime($info->pickup_date));
            $drop_date   = date('m/d/Y', strtotime($info->drop_date));
            $timeDiff    = strtotime($info->drop_date) - strtotime($info->pickup_date);
            $diffDays    = ceil($timeDiff / 86400);

            $orderinfo = $this->orders->get_order($info->order_id);
            
            
            foreach ($agents as $k => $agent) {
                if ($orderinfo->user_id == $agent->id) {
                    $total_agent_fees+=$info->agent_fees;
                    
                }
            }
            $total_driver_ot += ($info->driver_ot_fees * $info->ot_hours);
            $net = 0;
            // if ($info->status == "Completed") {
            //     $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            // } else {
            //     $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            //     // echo $info->order_id." -> ".$net."\n";
            // }
            if ($info->status == "Completed") {
                $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            } else {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                }
            }
            if ($info->type_of_rent == 38 || $info->type_of_rent == 37) {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + ($info->type_of_rent != 38? ($diffDays * $info->driver_fees):0) + (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                }
            }
            $total_net_income+=$net;
            // if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
            // // if ($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") {
            //     $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
            //     $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
            //     $total_driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;

            //     // $total_driver_bonus += ($net > 0) ? $net * $info->driver_percentage / 100 : 0;
            // }


            $_vendor_expense=0;
            if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
                $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
                $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                $_vendor_expense += $driver_bonus;
                $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
                // $wages="<br/>Wages:" . number_format($diffDays * $info->driver_fees);
                // $html .= '<td class="result-td" align="left" width="10%">' . "OT:" . number_format($info->driver_ot_fees * $info->ot_hours) . $wages."<br/>Bonus:" . number_format($driver_bonus) . '</td>';
            }
                $vendor_expense+=($info->vendor_fees - $_vendor_expense);
        }
        $total_profit=$total_net_income;

        $date_range_text = "";

        if (!empty($this->input->post('date_type'))) {
            $dt = $this->input->post('date_type');
            if ($dt == "from_to") {
                $from_date       = $this->input->post('from_date');
                $to_date         = $this->input->post('to_date');
                $date_range_text = "Order Date : " . $from_date . " - " . $to_date;

            } else if ($dt == "cday") {
                $date_range_text = "Order Date : " . date('Y-m-d');
            } else if ($dt == "pday") {
                $date_range_text = "Order Date : " . date('Y-m-d', strtotime("-1 days"));
            } else if ($dt == "cmonth") {
                $cmonth          = date('Y-m-01');
                $lastday         = date('t', strtotime($cmonth));
                $emonth          = date('Y-m-' . $lastday);
                $date_range_text = "Order Date : From : " . $cmonth . " - To : " . $emonth;
                // $this->db->where("transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'");
                // echo "transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'";
            } else if ($dt == "pmonth") {
                $pmonth  = date('Y-m-1', strtotime("-1 months"));
                $lastday = date('t', strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // echo $pmonth."<br/>";
                $emonth = date('Y-m-' . $lastday, strtotime(date('M Y-m', strtotime('-' . date('d') . ' days'))));
                // $this->db->where("transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'");
                $date_range_text = "Order Date : From : " . $pmonth . " - To : " . $emonth;
                // echo "transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'";
            }
        }
        $pickup_date  = "";
        $dropoff_date = "";

        if (!empty($this->input->post('pickup_date_from')) && !empty($this->input->post('pickup_date_to'))) {
            $pickup_date = "| Pick-Up Date : " . date('Y-m-d', strtotime($this->input->post('pickup_date_from'))) . " - " . date('Y-m-d', strtotime($this->input->post('pickup_date_to')));
        } else if (!empty($this->input->post('pickup_date_from'))) {
            $pickup_date = "| Pick-Up Date : " . date('Y-m-d', strtotime($this->input->post('pickup_date_from')));
        } else if (!empty($this->input->post('pickup_date_to'))) {
            $pickup_date = "| Pick-Up Date : " . date('Y-m-d', strtotime($this->input->post('pickup_date_to')));
        }

        if (!empty($this->input->post('dropoff_date_from')) && !empty($this->input->post('dropoff_date_to'))) {
            $dropoff_date = "| Drop-Off Date : " . date('Y-m-d', strtotime($this->input->post('dropoff_date_from'))) . " - " . date('Y-m-d', strtotime($this->input->post('dropoff_date_to')));
        } else if (!empty($this->input->post('dropoff_date_from'))) {
            $dropoff_date = "| Drop-Off Date : " . date('Y-m-d', strtotime($this->input->post('dropoff_date_from')));
        } else if (!empty($this->input->post('dropoff_date_to'))) {
            $dropoff_date = "| Drop-Off Date : " . date('Y-m-d', strtotime($this->input->post('dropoff_date_to')));
        }

        $user_company="";
        if(!empty($this->input->post('vendor')) && $this->input->post('vendor')!="only-vendor") {
            $user_company= $this->user->get_by("id", $this->input->post('vendor'))->user_company;
        }else if(!empty($this->input->post('vendor')) && $this->input->post('vendor')=="only-vendor") {
            $user_company= "Only Vendor";
        }else{
            $user_company= "All";
        }
        $html .= '

        <h3 align="center">Vendor Order Report</h3>
        <table width="100%">
            <tr>
                <td width="50%">
                    <table>

                        <tr>
                            <td width="20%">Rent Type</td>
                            <td width="5%">:</td>';
                            // if($this->input->post('rent_type'))
                            // {
                                // if($this->input->post('rent_type')=="full-day" || $this->input->post('rent_type')=="half-day"){
                                //     $html .= '<td width="25%">' . ucfirst($this->input->post('rent_type')) .'</td>';
                                // }else{
                            $html .= '<td width="25%">' . (!empty($_REQUEST['rent_type']) ? $this->attributes->get_by("id", $_REQUEST['rent_type'])->title : 'All') . '</td>';         
                                // }
                            // }
        
         $html .=       '<td width="20%">Driver</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_REQUEST['driver']) ? ucfirst($_REQUEST['driver']) : 'All') . '</td>
                        </tr>
                        <tr>
                            <td width="20%">Order Status</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_REQUEST['order_status']) ? $_REQUEST['order_status'] : 'All') . '</td>
                            <td width="20%">Vendor</td>
                            <td width="5%">:</td>
                            <td width="25%">' . $user_company. '</td>
                        </tr>
                        <tr>
                            <td width="20%">Vendor Status</td>
                            <td width="5%">:</td>
                            <td width="25%">' . (!empty($_REQUEST['vendor_status']) ? ucfirst($_REQUEST['vendor_status']) : 'All') . '</td>
                            
                        </tr>

                    </table>
                </td>
                <td width="50%">
                    <table>
                        <tr>
                            <td width="24%">Total Vendor Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_vendor_fees) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Total Fuel Cost</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_fuel_expense) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Driver OT Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_driver_ot) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Total Toll Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_toll_expense) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Driver OT Fees</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_driver_ot) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Other Expense</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_other_expense) . '</td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="24%">Total Vendor Pay</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($vendor_expense) . '</td>
                            <td width="1%">|</td>
                            <td width="24%">Total Driver Bonus</td>
                            <td width="1%">:</td>
                            <td width="24%" align="right">' . number_format($total_driver_bonus) . '</td>
                            <td width="1%"></td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>


        <p class="cus">' . $date_range_text . ' <span style="font-size:8pt">' . $pickup_date . $dropoff_date . '</span></p>';

                        // <th class="result-th" width="10%">Client</th>
        $html .= '<table width="100%" >
                <thead width="100%">
                    <tr>
                        <th class="result-th" width="5%">Serial No</th>
                        <th class="result-th" width="10%">Trip Record Name</th>
                        <th class="result-th" width="15%">Status</th>
                        <th class="result-th" width="10%">Supplier Name</th>
                        <th class="result-th" width="5%">Trip Type</th>
                        <th class="result-th" width="5%">Car Info</th>
                        <th class="result-th" width="5%">Driver</th>
                        <th class="result-th" width="10%">Driver Cost</th>
                        <th class="result-th" width="7.5%">Trip Start Date</th>
                        <th class="result-th" width="7.5%">Trip End<br/>Date</th>
                        <th class="result-th" width="10%">Fees</th>
                    </tr>
                </thead>';
        $html .= '<tbody width="100%">';
        $suppliers = $this->user->get_many_by("user_role", "supplier");
        $agents    = $this->user->get_many_by("user_role", "agent");
        foreach ($list as $key => $info) {
            $vendor_status = "";
            $agent_status  = "";
            $order_status  = "Order:";
            // $fees          = "Rental:" . number_format($info->amount);
            if ($info->status == "Completed") {
                $order_status .= '<font color="white" style="background-color: blue">' . $info->status . '</font>';
            } else if ($info->status == "Confirmed") {
                $order_status .= '<font style="background-color: #00c0ef">' . $info->status . '</font>';
            } else if ($info->status == "Cancelled") {
                $order_status .= '<font color="white" style="background-color: red">' . $info->status . '</font>';
            } else {
                $order_status .= '<font style="background-color: Yellow">' . $info->status . '</font>';
            }

            $customer_status = ($info->customer_payment_status == "complete") ? '<br/>Customer: <font color="white"  style="background-color: blue">Complete</font>' : '<br/>Customer: <font style="background-color: Yellow">Pending</font>';

            $orderinfo = $this->orders->get_order($info->order_id);
            foreach ($suppliers as $k => $supplier) {
                if ($orderinfo->supplier_id == $supplier->id) {
                    if ($supplier->user_lname != 'Sonic Star') {
                        $vendor_status = "<br/>Vendor: " . ($info->vendor_payment_status == "complete" ? '<font color="white"  style="background-color: blue">Complete</font>' : '<font style="background-color: Yellow">Pending</font>') . "\n";
                        // $fees .= "<br/>Vendor:" . number_format($info->vendor_fees);
                    }
                }
            }

            foreach ($agents as $k => $agent) {
                if ($orderinfo->user_id == $agent->id) {
                    $agent_status = "<br/>Agents: " . ($info->agent_payment_status == "complete" ? '<font color="white"  style="background-color: blue">Complete</font>' : '<font style="background-color: Yellow">Pending</font>') . "\n";
                    // $fees .= "<br/>Agent:" . number_format($info->agent_fees);
                }
            }

            $pickup_date = date('m/d/Y', strtotime($info->pickup_date));
            $drop_date   = date('m/d/Y', strtotime($info->drop_date));
            $timeDiff    = strtotime($info->drop_date) - strtotime($info->pickup_date);
            $diffDays    = ceil($timeDiff / 86400);
            // if ($info->status == "Completed") {
            //     $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            // } else {
            //     $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            // }
            // if ($info->type_of_rent==38 || $info->type_of_rent==37) {
            //     $net=$info->amount - ($info->vendor_fees+$info->agent_fees+$info->fuel_expense_total+$info->toll_expense_total+$info->other_expense_total+ (($info->type_of_rent==38 || $info->type_of_rent==37)? $info->driver_salary : 0));
            //     // echo $net;
            // }
            $net=0;
            if ($info->status == "Completed") {
                $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            } else {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                }
            }
            if ($info->type_of_rent == 38 || $info->type_of_rent == 37) {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + ($info->type_of_rent != 38? ($diffDays * $info->driver_fees):0) + (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                }
            }
            // if($pdf->PageNo()>1){
            $html .= '<tr nobr="true" style="font-size: xx-small;' . (($key % 2 == 0) ? 'background-color:#efefef' : 'background-color:#FFFFFF') . ';border:1px solid #eeeeee;line-height:200%">
                        <td class="result-td" width="5%"> ' . $info->order_id . '</td>
                        <td class="result-td" width="10%"> ' . $info->trip_record_name . '</td>
                        <td class="result-td" width="15%">' . $order_status . $customer_status . $vendor_status . $agent_status . '</td>
                        <td class="result-td" width="10%">' . $info->user_fname . ' ' . $info->user_lname . '</td>';
        
            if($info->rent_type==36){
                if($info->airport_status=="pickup" || $info->airport_status=="dropoff"){
                    $html .= '<td class="result-td" align="center" width="5%"> Airport '.ucfirst($info->airport_status). '</td>';
                    // $row[] = "Airport - ".ucfirst($info->airport_status);
                }else if($info->airport_status=="highway"){
                    $html .= '<td class="result-td" align="center" width="5%">' . $this->attributes->get_by('id', $info->rent_type)->title ."<br/>".ucfirst($info->airport_status). '</td>';
                
                }else{
                    $html .= '<td class="result-td" align="center" width="5%">' . $this->attributes->get_by('id', $info->rent_type)->title ."<br/>".ucfirst($info->daily_status). '</td>';
                }
            }else{
                 $html .= '<td class="result-td" align="center" width="5%">' . $this->attributes->get_by('id', $info->rent_type)->title . '</td>';
            }
            $html .= '
                        <td class="result-td" align="center" width="5%">' . $info->car_number . '<br/>' . $this->attributes->get_by('id', $info->model)->title . '</td>
                        <td class="result-td" width="5%">' . $info->driver_name . '</td>';
            $driver_bonus   = 0;
            $driver_expense = 0;
            // if ($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") {
            //     $driver_bonus = ($net > 0) ? $net * $info->driver_percentage / 100 : 0;
            //     $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
            $_vendor_expense=0;
            if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
                $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
                $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                $_vendor_expense +=$driver_bonus;
                $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
                $wages="<br/>Wages:" . number_format($diffDays * $info->driver_fees);
                $html .= '<td class="result-td" align="left" width="10%">' . "OT:" . number_format($info->driver_ot_fees * $info->ot_hours) . $wages."<br/>Bonus:" . number_format($driver_bonus) . '</td>';
            }else if($info->type_of_rent==38 || $info->type_of_rent==37){
                $driver_expense=$info->driver_salary;
                $html .= '<td class="result-td" align="left" width="10%">Driver Salary: '.number_format($info->driver_salary).'</td>';
                // $row[] = "Driver Salary:".;
            } else {
                $html .= '<td class="result-td" align="center" width="10%"></td>';
            }
            $html .= '<td class="result-td" align="center" width="7.5%">' . date('D d M, Y', strtotime($info->pickup_date)) . '</td>
                        <td class="result-td" align="center" width="7.5%">' . date('D d M, Y', strtotime($info->drop_date)) . '</td>
                        <td class="result-td" align="right" width="10%">
                        <b>Fee:</b> '.number_format($info->vendor_fees).'
                        <br/>
                        <b>Pay:</b> ' . number_format($info->vendor_fees - $_vendor_expense) . '</td>
                    </tr>';

            $html .= '<tr nobr="true" style="font-size: xx-small;' . (($key % 2 == 0) ? 'background-color:#efefef' : 'background-color:#FFFFFF') . ';border:1px solid #eeeeee;line-height:200%">
                            <td class="sub-result-td" colspan="11">';
            // $html .= 'Paid by Customer :' . (!empty($info->customer_total) ? number_format($info->customer_total) . " MMK" : 0);

            $agent_fees    = 0;
            $vendor_fees   = 0;
            $fuel_expense  = 0;
            $toll_expense  = 0;
            $other_expense = 0;

            if ($info->user_lname != 'Sonic Star') {
                $vendor_fees = $info->vendor_total;
                // $html .= ' | Paid to Vendor :' . (!empty($info->vendor_total) ? number_format($info->vendor_total) . " MMK" : 0);
            }
            // if(!empty($info->user_id) || $info->user_id!=""){

            $is_agent = $this->user->get_by('id', $info->user_id);
            if (isset($is_agent) && $is_agent->user_role == "agent") {
                $agent_fees = $info->agent_total;
                $html .= ' | Paid to Agent :' . (!empty($info->agent_total) ? number_format($info->agent_total) . " MMK" : 0);
            }

            // if ($info->user_lname == 'Sonic Star') {
                $fuel_expense = $info->fuel_expense_total;
                $html .= ' | Fuel Expense :' . (!empty($info->fuel_expense_total) ? number_format($info->fuel_expense_total) . " MMK" : 0);
            // }
            // $html.="<br/>";
            $toll_expense  = $info->toll_expense_total;
            $other_expense = $info->other_expense_total;
            if (($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star" || $info->driver_from==1)) || $info->type_of_rent == 38  || $info->type_of_rent==37) {
                $html .= ' | Driver Cost :' . number_format($driver_expense);
            }
            

            $html .= ' | Toll Fees :' . (!empty($info->toll_expense_total) ? number_format($info->toll_expense_total) . " MMK" : 0) . ' | Other Expense :' . (!empty($info->other_expense_total) ? number_format($info->other_expense_total) . " MMK" : 0);
            if($info->user_lname != "Sonic Star" && $info->driver_from==1){
                // $html.=' | <font color="white" style="background-color: red">Vendor Pay : '.number_format($info->vendor_fees - $_vendor_expense).'</font>';
            }
            // $html .= '   </td><td class="sub-result-td" align="center" ' . ($info->status == "Completed" ? 'style="background-color: blue;"' : 'style="background-color: yellow;"') . '>';
            // if ($info->status == "Completed") {

            //     $html .= '<font color="white" > Profit : ' . number_format($net) . '</font>';
            // } else {
            //     $html .= '<font color="black" > Profit : ' . number_format($net) . '</font>';
            // }
            $html .= '</td></tr>';
            // }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        // echo $html;
        return $html;
        // Print text using writeHTMLCell()

        // $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // $pdf->SetAutoPageBreak(true, 0);
        // $path = BASEPATH . '../';
        // // echo $path;
        // // Supply a filename including the .pdf extension
        // $filename = 'order_report_' . time() . '.pdf';

        // // Create the full path
        // $full_path = $path . '/' . $filename;
        // // ---------------------------------------------------------

        // // Close and output PDF document
        // // This method has several options, check the source code documentation for more information.
        // $pdf->Output($full_path, 'F');
        // return $filename;

        //============================================================+
        // END OF FILE
        //============================================================+
    }
    public function maintainReport($list)
    {
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->pdfInit($pdf,"Maintenance Report");
         $html = <<<EOF
        <style>
            .result-th{
                color:#ffffff;
                background-color: #BA221E;
                font-size: 10pt;
                text-align: center;
            }
            .result-td{
                font-size: 8pt;
                border-left: 1px solid #efefef;
                border-right: 1px solid #efefef;
            }
            td{
                font-size: 8pt;
            }
            .cus{
                font-size: 11pt;
                line-height:13px;
            }
            h3{
                line-height:0px;
            }
        </style>


EOF;

        $total_repair_cost   = 0;
        $total_paid=0;
        $total_recieved=0;
        // print "<pre/>";
        // print_r($querylist);
        $querylist = $this->rmodels->get_maintenance_datatables();

        foreach ($querylist as $info) {
            $total_repair_cost += $info->amount;
            if($info->paid=="complete"){
                $total_paid+=$info->amount;
            }
            if($info->recieved=="complete"){
                $total_recieved+=$info->amount;
            }
        }
        $date_range_text = "";

        if (!empty($this->input->post('dateType'))) {
            $dt = $this->input->post('dateType');
            if ($dt == "from_to") {
                $from_date       = $this->input->post('from_date');
                $to_date         = $this->input->post('to_date');
                $date_range_text = "Maintenance Date : " . $from_date . " - " . $to_date;

            } else if ($dt == "cday") {
                $date_range_text = "Maintenance Date : " . date('Y-m-d');
            } else if ($dt == "pday") {
                $date_range_text = "Maintenance Date : " . date('Y-m-d', strtotime("-1 days"));
            } else if ($dt == "cmonth") {
                $cmonth          = date('Y-m-01');
                $lastday         = date('t', strtotime($cmonth));
                $emonth          = date('Y-m-' . $lastday);
                $date_range_text = "Maintenance Date : From : " . $cmonth . " - To : " . $emonth;
                // $this->db->where("transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'");
                // echo "transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'";
            } else if ($dt == "pmonth") {
                $pmonth  = date('Y-m-1', strtotime("-1 months"));
                $lastday = date('t', strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // echo $pmonth."<br/>";
                $emonth = date('Y-m-' . $lastday, strtotime(date('M Y-m', strtotime('-' . date('d') . ' days'))));
                // $this->db->where("transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'");
                $date_range_text = "Maintenance Date : From : " . $pmonth . " - To : " . $emonth;
                // echo "transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'";
            }
        }
        $user_company="";
        if(!empty($this->input->post('user_id')) && $this->input->post('user_id')!="only-vendor") {
            $user_company= $this->user->get_by("id", $this->input->post('user_id'))->user_company;
        }else if(!empty($this->input->post('user_id')) && $this->input->post('user_id')=="only-vendor") {
            $user_company= "Only Vendor";
        }else{
            $user_company= "All";
        }
        $html .= '
        <h3 align="center">Maintenance Report</h3>
        <table width="100%" >
            <tr>
                <td width="60%">
                    <table>

                        <tr>
                            <td width="25%">Vendor</td>
                            <td width="5%">:</td>
                            <td width="20%">' . $user_company . '</td>
                            <td width="25%">Recieved From Vendor</td>
                            <td width="5%">:</td>
                            <td width="20%">'. (!empty($this->input->post('recieved')) ? ucfirst($this->input->post('recieved')) : 'All') .'</td>
                        </tr>
                        <tr>
                            <td width="25%">Workshop</td>
                            <td width="5%">:</td>
                            <td width="20%">'. (!empty($this->input->post('workshop_name')) ? ucfirst($this->input->post('workshop_name')) : 'All') .'</td>
                            <td width="25%">Paid To Workshop</td>
                            <td width="5%">:</td>
                            <td width="20%">'. (!empty($this->input->post('paid')) ? ucfirst($this->input->post('paid')) : 'All') .'</td>
                        </tr>
                        <tr>
                            <td width="25%">Car Tag</td>
                            <td width="5%">:</td>
                            <td width="20%">' . (!empty($this->input->post('car_tag')) ? $this->car_manager->get_by("id", $this->input->post('car_tag'))->car_number : 'All') . '</td>
                        </tr>
                        
                    </table>
                </td>
                <td width="40%">
                    <table>
                        <tr>
                            <td width="40%">Total Repair Cost</td>
                            <td width="10%">:</td>
                            <td width="25%" align="right">'.number_format($total_repair_cost).'</td>
                        </tr>
                        <tr>
                            <td width="40%">Total Recieved from Vendor</td>
                            <td width="10%">:</td>
                            <td width="25%" align="right">'.number_format($total_recieved).'</td>
                        </tr>
                        <tr>
                            <td width="40%">Total Paid to Workshop</td>
                            <td width="10%">:</td>
                            <td width="25%" align="right">'.number_format($total_paid).'</td>
                        </tr>
                    </table>
                </td>
            </tr>


        </table>

        <p class="cus">' . $date_range_text . '</p>';

        $html .= '<table width="100%" >
                <thead width="100%">
                    <tr>
                        <th class="result-th" width="10%">Car Type</th>
                        <th class="result-th" width="10%">Car Tag</th>
                        <th class="result-th" width="10%">Date</th>
                        <th class="result-th" width="20%">Description</th>
                        <th class="result-th" width="10%">Recieved From Vendor</th>
                        <th class="result-th" width="10%">Paid To Workshop</th>
                        <th class="result-th" width="10%">Workshop Name</th>
                        <th class="result-th" width="10%">Repaired By</th>
                        <th class="result-th" width="10%">Total Repaire Cost</th>
                    </tr>
                </thead>';
        $html .= '<tbody width="100%">';
        $attributesbyId = $this->car_manager->get_attributes();
        foreach ($list as $key => $info) {
        //     // if($pdf->PageNo()>1){
            $html .= '<tr nobr="true" style="font-size: xx-small;' . (($key % 2 == 0) ? 'background-color:#efefef' : 'background-color:#FFFFFF') . ';border:1px solid #eeeeee;line-height:20px">';
            
            $clist = $this->car_manager->get_car_detail($info->car_id);

            foreach ($clist as $cinfo) {
                $html .= '<td class="result-td" width="10%" align="center">' . (($cinfo->car_type > 0) ? $attributesbyId[$cinfo->car_type]['title'] : '') . '</td>
                        <td class="result-td" width="10%">' . $cinfo->car_number . '</td>';
            }
            $html .='<td class="result-td" width="10%">' . date('D d M, Y',$info->date) . '</td>
                        <td class="result-td" width="20%">' . $info->description . '</td>
                        <td class="result-td" width="10%">' . ucfirst($info->recieved) . '</td>
                        <td class="result-td" width="10%">' . ucfirst($info->paid) . '</td>
                        <td class="result-td" width="10%">' .$info->workshop_name.'</td>
                        <td class="result-td" width="10%">' .$info->repaired_by. '</td>
                        <td class="result-td" width="10%" align="right">' . number_format($info->amount) . '</td>
                    </tr>';
        //     // }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        // echo $html;
        // Print text using writeHTMLCell()

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $pdf->SetAutoPageBreak(true, 0);
        $path = BASEPATH . '../';
        // echo $path;
        // Supply a filename including the .pdf extension
        $filename = 'financial_report_' . time() . '.pdf';

        // Create the full path
        $full_path = $path . '/' . $filename;
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output($full_path, 'F');
        return $filename;

        //============================================================+
        // END OF FILE
        //============================================================+

    }


    public function fuelReport($list)
    {
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->pdfInit($pdf,"Fuel Report");
         $html = <<<EOF
        <style>
            .result-th{
                color:#ffffff;
                background-color: #BA221E;
                font-size: 10pt;
                text-align: center;
            }
            .result-td{
                font-size: 8pt;
                border-left: 1px solid #efefef;
                border-right: 1px solid #efefef;
            }
            td{
                font-size: 8pt;
            }
            .cus{
                font-size: 11pt;
                line-height:13px;
            }
            h3{
                line-height:0px;
            }
        </style>


EOF;

        $total_amount = 0;
        // print "<pre/>";
        // print_r($querylist);
        $querylist = $this->rmodels->get_fuel_datatables();

        foreach ($querylist as $info) {
            $total_amount += $info->amount;
            // if($info->paid=="complete"){
            //     $total_paid+=$info->amount;
            // }
            // if($info->recieved=="complete"){
            //     $total_recieved+=$info->amount;
            // }
        }

        $date_range_text = "";

        if (!empty($this->input->post('dateType'))) {
            $dt = $this->input->post('dateType');
            if ($dt == "from_to") {
                $from_date       = $this->input->post('from_date');
                $to_date         = $this->input->post('to_date');
                $date_range_text = "Maintenance Date : " . $from_date . " - " . $to_date;

            } else if ($dt == "cday") {
                $date_range_text = "Maintenance Date : " . date('Y-m-d');
            } else if ($dt == "pday") {
                $date_range_text = "Maintenance Date : " . date('Y-m-d', strtotime("-1 days"));
            } else if ($dt == "cmonth") {
                $cmonth          = date('Y-m-01');
                $lastday         = date('t', strtotime($cmonth));
                $emonth          = date('Y-m-' . $lastday);
                $date_range_text = "Maintenance Date : From : " . $cmonth . " - To : " . $emonth;
                // $this->db->where("transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'");
                // echo "transations_recs.transaction_date BETWEEN '$cmonth' AND '$emonth'";
            } else if ($dt == "pmonth") {
                $pmonth  = date('Y-m-1', strtotime("-1 months"));
                $lastday = date('t', strtotime(date('Y-m', strtotime('-' . date('d') . ' days'))));
                // echo $pmonth."<br/>";
                $emonth = date('Y-m-' . $lastday, strtotime(date('M Y-m', strtotime('-' . date('d') . ' days'))));
                // $this->db->where("transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'");
                $date_range_text = "Maintenance Date : From : " . $pmonth . " - To : " . $emonth;
                // echo "transations_recs.transaction_date BETWEEN '$pmonth' AND '$emonth'";
            }
        }
        $html .= '
        <h3 align="center">Fuel Report</h3>
        <table width="100%" >
            <tr>
                <td width="60%">
                    <table>

                        <tr>
                            <td width="25%">Car Tag</td>
                            <td width="5%">:</td>
                            <td width="20%">' . (!empty($this->input->post('car_tag')) ? $this->user->get_by("id", $this->input->post('car_tag'))->user_company : 'All') . '</td>
                            <td width="25%">Driver</td>
                            <td width="5%">:</td>
                            <td width="20%">'. (!empty($this->input->post('driver')) ? ucfirst($this->input->post('driver')) : 'All') .'</td>
                        </tr>
                        <tr>
                            <td width="25%">Gas Station</td>
                            <td width="5%">:</td>
                            <td width="20%">'. (!empty($this->input->post('gas_station')) ? ucfirst($this->input->post('gas_station')) : 'All') .'</td>
                            <td width="25%">Payment Type</td>
                            <td width="5%">:</td>
                            <td width="20%">'. (!empty($this->input->post('payment_type')) ? ucfirst($this->input->post('payment_type')) : 'All') .'</td>
                        </tr>
                        <tr>
                            <td width="25%">Payment Status</td>
                            <td width="5%">:</td>
                            <td width="20%">' . (!empty($this->input->post('payment_status')) ? ucfirst($this->input->post('payment_status')) : 'All') . '</td>
                        </tr>
                        
                    </table>
                </td>
                <td width="40%">
                    <table>
                        <tr>
                            <td width="40%">Total Amount</td>
                            <td width="10%">:</td>
                            <td width="25%" align="right">'.number_format($total_amount).'</td>
                        </tr>
                    </table>
                </td>
            </tr>


        </table>

        <p class="cus">' . $date_range_text . '</p>';

        $html .= '<table width="100%">
                    <thead width="100%">
                        <tr>
                            <th class="result-th" width="10%">Vendor</th>
                            <th class="result-th" width="7.5%">Car Tag</th>
                            <th class="result-th" width="7.5%">Shop Name</th>
                            <th class="result-th" width="10%">Car Operator Name</th>
                            <th class="result-th" width="7.5%">Payment Type</th>
                            <th class="result-th" width="7.5%">Status</th>
                            <th class="result-th" width="7.5%">Type Of Use</th>
                            <th class="result-th" width="10%">Date</th>
                            <th class="result-th" width="10%">Liter</th>
                            <th class="result-th" width="10%">Description</th>
                            <th class="result-th" width="7.5%">Comments</th>
                            <th class="result-th" width="7.5%">Amount</th>
                        </tr>
                    </thead>';
        $html .= '<tbody width="100%">';
        $attributesbyId = $this->car_manager->get_attributes();
        foreach ($list as $key => $info) {
        //     // if($pdf->PageNo()>1){
            $html .= '<tr nobr="true" style="font-size: xx-small;' . (($key % 2 == 0) ? 'background-color:#efefef' : 'background-color:#FFFFFF') . ';border:1px solid #eeeeee;line-height:20px">
                            <td class="result-td" width="10%">'.$info->user_fname.' '.$info->user_lname.'</td>
                            <td class="result-td" width="7.5%">'.$info->car_number.'</td>
                            <td class="result-td" width="7.5%">'.$info->shop_name.'</td>
                            <td class="result-td" width="10%">'.$info->operator_name.'</td>
                            <td class="result-td" width="7.5%">'.$info->purchased_by.'</td>
                            <td class="result-td" width="7.5%">'.ucfirst($info->status).'</td>
                            <td class="result-td" width="7.5%">'.ucfirst(str_replace("_", " ", $info->type_of_use)).'</td>
                            <td class="result-td" width="10%">'.date('D d M, Y',$info->date).'</td>
                            <td class="result-td" width="10%">'.$info->liter.'</td>
                            <td class="result-td" width="10%">'.$info->description.'</td>
                            <td class="result-td" width="7.5%">'.$info->comments.'</td>
                            <td class="result-td" width="7.5%">'.number_format($info->amount).'</td>

                    </tr>';
        //     // }
        }

        // $html .= '</tbody>';
        $html .= '</table>';
        // echo $html;
        // Print text using writeHTMLCell()

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $pdf->SetAutoPageBreak(true, 0);
        $path = BASEPATH . '../';
        // echo $path;
        // Supply a filename including the .pdf extension
        $filename = 'financial_report_' . time() . '.pdf';

        // Create the full path
        $full_path = $path . '/' . $filename;
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output($full_path, 'F');
        return $filename;

        //============================================================+
        // END OF FILE
        //============================================================+

    }
}
