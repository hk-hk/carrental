<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH."/third_party/PHPExcel/PHPExcel.php";
class Reports extends MY_Model{
	private $_ci;
    function __construct() {
        parent::__construct();
    	$this->_ci =& get_instance();
	 	// $this->_ci->load->model('Car_manager/Car_manager', 'car_manager');
	 	$this->_ci->load->model('Car_manager/Car_maintain', 'car_maintain');

	 	$this->_ci->load->model('Attributes/Attributes', 'attributes'); // Load User modal
	 	$role =  $this->session->userdata('cr_user_role'); 
        
    }

 	public function reportProperties($objPHPExcel,$title){
    	$objPHPExcel->getProperties()->setCreator("Sonic Star")
									 ->setLastModifiedBy("Sonic Star")
									 ->setTitle($title)
									 ->setSubject($title." Report")
									 ->setDescription("Sonic Star Travel Report")
									 ->setKeywords("Report File")
									 ->setCategory("Report");
	 	
		// $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
		

		
		// unset($styleArray);
    }
    public function setHeaderStyle($objPHPExcel,$from_to){
    	$style = array 	(
    						'font' => array(
								'size' => 13,
								'bold' => true,
								'color' => array('rgb' => 'FFFFFF')
							),
						    'borders' => array(
						        'allborders' => array(
						            'style' => PHPExcel_Style_Border::BORDER_THIN,
						            'color' => array('rgb' => 'd6d6d6')
						        )
						    ),
						    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BA221E')
					        ),
					        'alignment' => array(
					            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					        )
    					);

    	$objPHPExcel->getActiveSheet()->getStyle($from_to)->applyFromArray($style);
    	unset($style);
    }
    public function setRowStyle($objPHPExcel,$from_to,$isfill=false){
    	$style = array 	(
    						'font' => array(
								'size' => 11,
								'bold' => false
							),
						    'borders' => array(
						        'allborders' => array(
						            'style' => PHPExcel_Style_Border::BORDER_THIN,
						            'color' => array('rgb' => 'efefef')
						        )
						    ),
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => (!$isfill)? 'efefef' : 'FFFFFF')
					        )
    					);
    	$objPHPExcel->getActiveSheet()->getStyle($from_to)->applyFromArray($style);
    	
    	unset($style);
    }
    public function downloadHeader($objPHPExcel,$filename){
  		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment;filename="'.$filename.'.xls"');
		// header('Cache-Control: max-age=0');
		// // If you're serving to IE 9, then the following may be needed
		// header('Cache-Control: max-age=1');

		// // If you're serving to IE over SSL, then the following may be needed
		// // header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		// header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		// header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		// header ('Pragma: public'); // HTTP/1.0
    	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

		    $objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($worksheet));

		    $sheet = $objPHPExcel->getActiveSheet();
		    $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		    $cellIterator->setIterateOnlyExistingCells(true);
		    /** @var PHPExcel_Cell $cell */
		    foreach ($cellIterator as $cell) {
		        $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);

		    }
		}
		// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		// $objWriter->save($filename);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($filename);

    }

    public function maintenanceReport($list){
    	$objPHPExcel = new PHPExcel();
     	$attributesbyId = $this->car_manager->get_attributes();
    	
    	$this->reportProperties($objPHPExcel, 'Order');
    	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A1', 'Car Type')
			            ->setCellValue('B1', 'Car Tag')
			            ->setCellValue('C1', 'Date')
			            ->setCellValue('D1', 'Description')
			            ->setCellValue('E1', 'Repair Cost')
			            ->setCellValue('F1', 'Recieved From Vendor')
			            ->setCellValue('G1', 'Paid To Workshop')
			            ->setCellValue('H1', 'Workshop')
			            ->setCellValue('I1', 'Repaired By');
		$this->setHeaderStyle($objPHPExcel,'A1:I1');
        $rno=2;
        $data = array();
        $no   = 0;
        foreach ($list as $info) {
        	$clist = $this->car_manager->get_car_detail($info->car_id);
        	foreach ($clist as $cinfo) {
               $objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A'.$rno, ($cinfo->car_type > 0) ? $attributesbyId[$cinfo->car_type]['title'] : '')
			            ->setCellValue('B'.$rno, $cinfo->car_number);
            }
        	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('C'.$rno, date('D d M, Y',$info->date))
			            ->setCellValue('D'.$rno, $info->description)
			            ->setCellValue('E'.$rno, $info->amount)
			            ->setCellValue('F'.$rno, ($info->recieved=='pending')? 'Pending' : 'Complete')
			            ->setCellValue('G'.$rno, ($info->paid=='pending')? 'Pending' : 'Complete')
			            ->setCellValue('H'.$rno, $info->workshop_name)
			            ->setCellValue('I'.$rno, $info->repaired_by);
			$rno++;
        }
		$objPHPExcel->getActiveSheet()->setTitle("Car Maintain");
		$objPHPExcel->setActiveSheetIndex(0);
		$filename="Mainetenance_Report_".date('d_m_y').".xls";
		$this->downloadHeader($objPHPExcel,$filename);
		return $filename;
    }

    public function fuelReport($list){
    	$objPHPExcel = new PHPExcel();
     	$attributesbyId = $this->car_manager->get_attributes();
    	
    	$this->reportProperties($objPHPExcel, 'Fuel');
    	$objPHPExcel->setActiveSheetIndex(0)
    					->setCellValue('A1', '#')
			            ->setCellValue('B1', 'Gas Station')
			            ->setCellValue('C1', 'Payment Type')
			            ->setCellValue('D1', 'Status')
			            ->setCellValue('E1', 'Type Of Use')
			            ->setCellValue('F1', 'Date')
			            ->setCellValue('G1', 'Liter')
			            ->setCellValue('H1', 'Amount');
			            $rno=2;
		$this->setHeaderStyle($objPHPExcel,'A1:H1');
        $data = array();
        $no   = 0;
        foreach ($list as $info) {
        	$no++;
        	$clist = $this->car_manager->get_car_detail($info->car_id);
        	
        	$objPHPExcel->setActiveSheetIndex(0)
        				->setCellValue('A'.$rno, $no)
			            ->setCellValue('B'.$rno, $info->shop_name)
        				->setCellValue('C'.$rno, ucfirst($info->purchased_by))
			            ->setCellValue('D'.$rno, ucfirst($info->status))
			            ->setCellValue('E'.$rno, ucfirst(str_replace("_", " ", $info->type_of_use)))
			            ->setCellValue('F'.$rno, date('D d M, Y',$info->date))
			            ->setCellValue('G'.$rno, $info->liter)
			            ->setCellValue('H'.$rno, number_format($info->amount));
			$this->setRowStyle($objPHPExcel,'A'.$rno.':H'.$rno , ($rno%2==0)? true : false);
			$rno++;
        }
		$objPHPExcel->getActiveSheet()->setTitle("Car Maintain");
		$objPHPExcel->setActiveSheetIndex(0);
		$filename="Fuel_Report_".date('d_m_y').".xls";
		$this->downloadHeader($objPHPExcel,$filename);
		return $filename;
    }

    public function financialReport($list){
    	$objPHPExcel = new PHPExcel();
    	
    	$this->reportProperties($objPHPExcel, 'Financial');
    	$objPHPExcel->setActiveSheetIndex(0)
    					->setCellValue('A1', 'Processed At')
			            ->setCellValue('B1', 'Transaction Date')
			            ->setCellValue('C1', 'Category')
			            ->setCellValue('D1', 'Description')
			            ->setCellValue('E1', 'Process Type')
			            ->setCellValue('F1', 'Account')
			            ->setCellValue('G1', 'Payment To/From')
			            ->setCellValue('H1', 'Deposit')
			            ->setCellValue('I1', 'Withdraw')
			            ->setCellValue('J1', 'Final Balance');
			            $rno=2;
		$this->setHeaderStyle($objPHPExcel,'A1:J1');
		
        $data = array();
        foreach ($list as $info) {
        	// $no++;
        	// $clist = $this->car_manager->get_car_detail($info->car_id);
        	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A'.$rno, date('m-d-Y / h:i',$info->created_at))
			            ->setCellValue('B'.$rno, date('m-d-Y',$info->transaction_date))
			            ->setCellValue('C'.$rno, $this->attributes->get_by('id',$info->category_id)->title)
			            ->setCellValue('D'.$rno, $info->description)
			            ->setCellValue('E'.$rno, $info->user_lname)
			            ->setCellValue('F'.$rno, $info->account_name)
			            ->setCellValue('G'.$rno, $info->payment_person)
			            ->setCellValue('H'.$rno, ($info->payment_type==4417)? number_format($info->amount) : '-')
			            ->setCellValue('I'.$rno, ($info->payment_type==4416)? number_format($info->amount) : '-')
			            ->setCellValue('J'.$rno, number_format($info->final_balance));
			$this->setRowStyle($objPHPExcel,'A'.$rno.':J'.$rno , ($rno%2==0)? true : false);
			
			$rno++;
        }
		$objPHPExcel->getActiveSheet()->setTitle("Financial Report");
		$objPHPExcel->setActiveSheetIndex(0);
		$filename="Financial_Report_".date('d_m_y').".xls";
		// $this->downloadHeader($objPHPExcel,$filename);
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

		    $objPHPExcel->setActiveSheetIndex($objPHPExcel->getIndex($worksheet));

		    $sheet = $objPHPExcel->getActiveSheet();
		    $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
		    $cellIterator->setIterateOnlyExistingCells(true);
		    /** @var PHPExcel_Cell $cell */
		    // foreach ($cellIterator as $cell) {
		    // 	if($cell->getColumn()!="D"){
		    //     	$sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
		    // 	}

		    // }
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);


		$objPHPExcel->getActiveSheet()->getStyle('C2:C'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle('D2:D'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('J2:J'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:A'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('B1:B'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('D1:D'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('E1:E'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('F1:F'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('H1:H'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('I1:I'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('J1:J'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		// $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($filename);

		return $filename;
    }

    public function orderReport($list){
    	$objPHPExcel = new PHPExcel();
     	$attributesbyId = $this->car_manager->get_attributes();
    	
    	$this->reportProperties($objPHPExcel, 'Driver Bonus');
    	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A1', 'Serial No')
			            ->setCellValue('B1', 'Trip Record Name')
			            ->setCellValue('C1', 'Status')
			            ->setCellValue('D1', 'Supplier Name')
			            ->setCellValue('E1', 'Trip Type')
			            ->setCellValue('F1', 'Client')
			            ->setCellValue('G1', 'Driver')
			            ->setCellValue('H1', 'Car Tag')
			            ->setCellValue('I1', 'Car Type')
			            ->setCellValue('J1', 'Trip Start Date')
			            ->setCellValue('K1', 'Trip End Date')
			            ->setCellValue('L1', 'Driver Fees')
			            ->setCellValue('M1', 'Vendor Fees')
			            ->setCellValue('N1', 'Rental Charges')
			            // ->setCellValue('L1', 'Total Expense')
			            ->setCellValue('O1', 'Customer Payment')
			            ->setCellValue('P1', 'Vendor Payment')
			            ->setCellValue('Q1', 'Agent Payment')
			            ->setCellValue('R1', 'Fuel Expense')
			            ->setCellValue('S1', 'Toll Expense')
			            ->setCellValue('T1', 'Other Expense');
			            $rno=2;
        $this->setHeaderStyle($objPHPExcel,'A1:T1');
        $data = array();
        $no   = 0;
        $suppliers = $this->user->get_many_by("user_role", "supplier");
        $agents = $this->user->get_many_by("user_role", "agent");
        foreach ($list as $info) {
        	$vendor_fees="";
            $vendor_status="";
            $agent_status="";
            $order_status="Order:".$info->status."\n" ;
            $customer_status=($info->customer_payment_status=="complete")? "Customer:Complete\n" : "Customer:Pending\n" ;
            $orderinfo = $this->orders->get_order($info->order_id);
            foreach ($suppliers as $k => $supplier){
                if ($orderinfo->supplier_id == $supplier->id){
                    if ($supplier->user_lname != 'Sonic Star'){
                        $vendor_status="Vendor:".$info->vendor_payment_status."\n";
                        $vendor_fees=number_format($info->vendor_fees);
                    }
                }
            }

            foreach ($agents as $k => $agent){
                if ($orderinfo->user_id == $agent->id){
                    $agent_status="Agents:".$info->agent_payment_status."\n";
                }
            }

            if($info->rent_type==36){
                if($info->airport_status=="pickup" || $info->airport_status=="dropoff"){
			        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$rno, "Airport - ".ucfirst($info->airport_status));
                }else if($info->airport_status=="highway"){
			        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$rno, $this->attributes->get_by('id',$info->rent_type)->title." - ".ucfirst($info->airport_status));
                }else{
			        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$rno, $this->attributes->get_by('id',$info->rent_type)->title." - ".ucfirst($info->daily_status));
                }
            }else{
		        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$rno, $this->attributes->get_by('id',$info->rent_type)->title);
            }

            $pickup_date = date('m/d/Y 00:00:00', strtotime($info->pickup_date));
            $drop_date   = date('m/d/Y 23:59:59', strtotime($info->drop_date));
            $timeDiff    = strtotime($drop_date) - strtotime($pickup_date);
            $diffDays    = round(abs($timeDiff / 86400));
            
            $net = 0;

            if ($info->status == "Completed") {
            	// $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            } else {
            	if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                }
                // $net = $value->amount - ($value->vendor_fees + $value->agent_fees + $value->fuel_expense_total + $value->toll_expense_total + $value->other_expense_total + (($value->type_of_rent == 36 && $value->user_lname == "Sonic Star") ? (($value->driver_ot_fees * $info->total_ot_hours) + ($diffDays * $value->driver_fees)) : 0));
            }
            // if ($value->type_of_rent == 36 && $value->user_lname == "Sonic Star") {
            //     $total_driver_bonus += ($net > 0) ? $net * $info->driver_percentage / 100 : 0;
            //     // $total_driver_bonus+=$net;
            // }
            // }
        	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A'.$rno, $info->order_id)
			            ->setCellValue('B'.$rno, $info->trip_record_name)
			            ->setCellValue('C'.$rno, $order_status.$customer_status.$vendor_status.$agent_status)
			            ->setCellValue('D'.$rno, $info->user_fname.' '.$info->user_lname)
			            ->setCellValue('F'.$rno, $info->name)
			            ->setCellValue('G'.$rno, $info->driver_name)
			            ->setCellValue('H'.$rno, $info->car_number)
			            ->setCellValue('I'.$rno, $this->attributes->get_by('id',$info->car_type)->title)
			            ->setCellValue('J'.$rno, date('D d M, Y',strtotime($info->pickup_date)))
			            ->setCellValue('K'.$rno, date('D d M, Y',strtotime($info->drop_date)))
			            
			            ->setCellValue('M'.$rno, $vendor_fees)
			            ->setCellValue('N'.$rno, number_format($info->amount))
			            ->setCellValue('O'.$rno, !empty($info->customer_total)? number_format($info->customer_total) : 0)
			            ->setCellValue('P'.$rno, !empty($info->vendor_total)? number_format($info->vendor_total) : 0)
			            ->setCellValue('Q'.$rno, !empty($info->agent_total)? number_format($info->agent_total) : 0)
			            ->setCellValue('R'.$rno, !empty($info->fuel_expense_total)? number_format($info->fuel_expense_total) : 0)
			            ->setCellValue('S'.$rno, !empty($info->toll_expense_total)? number_format($info->toll_expense_total) : 0)
			            ->setCellValue('T'.$rno, !empty($info->other_expense_total)? number_format($info->other_expense_total) : 0);
            
            if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
                $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
                $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                $_vendor_expense +=$driver_bonus;
                $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
                $wages="\nWages:" . number_format($diffDays * $info->driver_fees);
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('L'.$rno, "OT:" . number_format($info->driver_ot_fees * $info->ot_hours) . $wages . "\nBonus:" . number_format($driver_bonus));
            }
                // $row[]          = "OT:" . number_format($value->driver_ot_fees * $value->ot_hours) . $wages . "<br/>Bonus:" . number_format($driver_bonus);
            $this->setRowStyle($objPHPExcel,'A'.$rno.':T'.$rno , ($rno%2==0)? true : false);

            $objPHPExcel->getActiveSheet()->getStyle('A1:A'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('B1:B'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('D1:D'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('E1:E'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('F1:F'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('H1:H'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('I1:I'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('J1:J'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('K1:K'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('L1:L'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('M1:M'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('N1:N'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('O1:O'.$rno)->getAlignment()->setVertical(
				PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('P1:P'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('Q1:Q'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('R1:R'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('S1:S'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('T1:T'.$rno)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setWrapText(true);

			$objPHPExcel->getActiveSheet()->getStyle('K2:K'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('L2:L'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('M2:M'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('N2:N'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('O2:O'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('P2:P'.$rno)->getAlignment()->setHorizontal(
				PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('Q2:Q'.$rno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			$rno++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(13);

		$objPHPExcel->getActiveSheet()->setTitle("Order Report");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);

		$filename="Order_Report_".date('d_m_y').".xls";
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($filename);

		return $filename;
		// $this->downloadHeader($objPHPExcel,$filename);
		// return $filename;
    }

    public function driverbonusReport($list){
    	$objPHPExcel = new PHPExcel();
     	$attributesbyId = $this->car_manager->get_attributes();
    	
    	$this->reportProperties($objPHPExcel, 'Driver Bonus');
    	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A1', 'Driver')
			            ->setCellValue('B1', 'Client')
			            ->setCellValue('C1', 'No Of Trip')
			            ->setCellValue('D1', 'Total OT Hours')
			            ->setCellValue('E1', 'Total Driver OT Fees')
			            ->setCellValue('F1', 'Total Driver Bonus');
        $this->setHeaderStyle($objPHPExcel,'A1:F1');
        $querylist = $this->rmodels->get_order_rec();
        $rno=2;
        $data = array();
        $no   = 0;
        foreach ($list as $info) {
        	$total_rental_fees   = 0;
            $total_vendor_fees   = 0;
            $total_agent_fees    = 0;
            $total_fuel_expense  = 0;
            $total_toll_expense  = 0;
            $total_other_expense = 0;
            $total_driver_ot     = 0;
            $total_driver_bonus  = 0;
        	// $clist = $this->car_manager->get_car_detail($info->car_id);
        	// foreach ($clist as $cinfo) {
         //       $objPHPExcel->setActiveSheetIndex(0)
			      //       // ->setCellValue('A'.$rno, ($cinfo->car_type > 0) ? $attributesbyId[$cinfo->car_type]['title'] : '')
			      //       ->setCellValue('F'.$rno, $cinfo->car_number);
         //    }
        	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A'.$rno, $info->user_lname)
			            ->setCellValue('B'.$rno, $this->input->post('client')? $this->input->post('client') : "All")
			            ->setCellValue('C'.$rno, $info->no_of_trip)
			            ->setCellValue('D'.$rno, $info->total_ot_hours ." Hours");
    	 	// $row[] = $info->user_lname;
       //      $row[] = $this->input->post('client')? $this->input->post('client') : "All";
       //      $row[] = $info->no_of_trip;
       //      $row[] = $info->total_ot_hours ." Hours";
        	foreach ($querylist as $key => $value) {
                # code...driver_name
            	if($value->driver_name==$info->user_lname){
                    $total_rental_fees += $value->customer_total;
                    // $total_rental_fees += $info->amount;
                    $total_vendor_fees += $value->vendor_total;
                    $total_agent_fees += $value->agent_total;
                    $total_fuel_expense += $value->fuel_expense_total;
                    $total_other_expense += $value->other_expense_total;
                    $total_toll_expense += $value->toll_expense_total;
                    $total_driver_ot += ($value->driver_ot_fees * $info->total_ot_hours);

                    $pickup_date = date('m/d/Y', strtotime($value->pickup_date));
                    $drop_date   = date('m/d/Y', strtotime($value->drop_date));
                    $timeDiff    = strtotime($value->drop_date) - strtotime($value->pickup_date);
                    $diffDays    = ceil($timeDiff / 86400);

                    $net = 0;
                    if ($value->status == "Completed") {
                        $net = $value->customer_total - ($value->vendor_total + $value->agent_total + $value->fuel_expense_total + $value->toll_expense_total + $value->other_expense_total + (($value->type_of_rent == 36 && $value->user_lname == "Sonic Star") ? (($value->driver_ot_fees * $info->total_ot_hours) + ($diffDays * $value->driver_fees)) : 0));
                    } else {
                        $net = $value->amount - ($value->vendor_fees + $value->agent_fees + $value->fuel_expense_total + $value->toll_expense_total + $value->other_expense_total + (($value->type_of_rent == 36 && $value->user_lname == "Sonic Star") ? (($value->driver_ot_fees * $info->total_ot_hours) + ($diffDays * $value->driver_fees)) : 0));
                    }
                    if ($value->type_of_rent == 36 && $value->user_lname == "Sonic Star") {
                        $total_driver_bonus += ($net > 0) ? $net * $info->driver_percentage / 100 : 0;
                        // $total_driver_bonus+=$net;
                    }
                }
            }
            // $row[] = $total_driver_ot;
            // $row[] = $total_driver_bonus;
        	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('E'.$rno, $total_driver_ot)
			            ->setCellValue('F'.$rno, $total_driver_bonus);
           	$this->setRowStyle($objPHPExcel,'A'.$rno.':F'.$rno , ($rno%2==0)? true : false);
			$rno++;
        }
		$objPHPExcel->getActiveSheet()->setTitle("Driver Bunuse");
		$objPHPExcel->setActiveSheetIndex(0);
		$filename="Driver_Bonus_Report_".date('d_m_y').".xls";
		$this->downloadHeader($objPHPExcel,$filename);
		return $filename;
    }

   

}
