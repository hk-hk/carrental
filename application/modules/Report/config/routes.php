<?php

$route[CRM_VAR.'/related_car_tag/(:any)']     = 'Report/Report/related_car_tag/$1';

// $route[CRM_VAR.'/reports']     = 'Report/Report/index';

$route[CRM_VAR.'/removefile/(:any)']     = 'Report/Report/removefile/$1';

$route[CRM_VAR.'/order_report']     = 'Report/Report/order';
$route[CRM_VAR.'/order_report_list']     = 'Report/Report/order_list';
$route[CRM_VAR.'/order_report_output']     = 'Report/Report/order_report';
$route[CRM_VAR.'/order_report_print']     = 'Report/Report/order_report_print';
$route[CRM_VAR.'/order_vreport_print']     = 'Report/Report/order_vreport_print';

$route[CRM_VAR.'/maintenance_report']     = 'Report/Report/maintenance';
$route[CRM_VAR.'/maintenance_report_list']     = 'Report/Report/maintenance_list';
$route[CRM_VAR.'/maintenance_report_output']     = 'Report/Report/maintenance_report';
$route[CRM_VAR.'/maintenance_report_print']     = 'Report/Report/maintenance_report_print';

$route[CRM_VAR.'/fuel_report']     = 'Report/Report/fuel';
$route[CRM_VAR.'/fuel_report_list']     = 'Report/Report/fuel_list';
$route[CRM_VAR.'/fuel_report_output']     = 'Report/Report/fuel_report';
$route[CRM_VAR.'/fuel_report_print']     = 'Report/Report/fuel_report_print';

$route[CRM_VAR.'/driver_bonus_report']     = 'Report/Report/driver_bonus';
$route[CRM_VAR.'/driver_bonus_report_list']     = 'Report/Report/driver_bonus_list';
$route[CRM_VAR.'/driver_bonus_report_output']     = 'Report/Report/driver_bonus_report';

$route[CRM_VAR.'/financial_report']     = 'Report/Report/financial';
$route[CRM_VAR.'/financial_report_list']     = 'Report/Report/financial_list';
$route[CRM_VAR.'/financial_report_output']     = 'Report/Report/financial_report_output';
$route[CRM_VAR.'/financial_report_print']     = 'Report/Report/financial_report_print';
