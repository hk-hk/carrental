<?php
/***

*

* Created by: CIS

* This is for authorized user's only.

* This file contains all Property's related functions.

*

***/
defined('BASEPATH') or exit('No direct script access allowed');
// require_once APPPATH."/third_party/PHPExcel/PHPExcel.php";
class Report extends MY_Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $this->isAuthorized();
        parent::__construct();
        $this->data['module'] = 'Report'; // Load Users module
        $this->load->model('Reports', 'reports');
        $this->load->model('RModels', 'rmodels');
        $this->load->model('PDFModels', 'pdfmodels');
        $this->load->model('PDFModels', 'rmodels');
        $this->load->model('AUserModels', 'airline_user');
        $this->load->model('Car_manager/Car_manager', 'car_manager'); // Load User modal
        $this->load->model('Car_manager/Car_maintain', 'car_maintain'); // Load User modal
        $this->load->model('Attributes/Attributes', 'attributes'); // Load User modal
        $this->load->model('Users/User', 'user');
        $this->load->model('Accounts/Accounts', 'accounts');
        $this->load->model('Order/Orders', 'orders');
        $role               = $this->session->userdata('cr_user_role');
        $this->data['role'] = $role;
        if ($role != 'supplier') {
            $this->data['suppliers'] = $this->car_manager->get_car_suppliers();
        }
    }
    public function index()
    {
        // $objPHPExcel;
        // $result = $this->car_maintain->_get_datatables_query();
        // print "<pre/>";
        // print_r($result);
        // echo base_url();
        // file_put_contents("Mainetenance_Report_13_06_18.xlsx", fopen(base_url()."Mainetenance_Report_13_06_18.xlsx", 'r'));
    }
    public function removefile($filename)
    {
        unlink($filename . '.xlsx');
        // echo $filename;
    }
    public function order()
    {
        $this->isAuthorized();
        $this->data['car_tags']  = $this->car_manager->get_carnos();
        $this->data['customers'] = $this->user->get_many_by("user_role", "customer");
        $this->data['agents']    = $this->user->get_many_by("user_role", "agent");
        $this->data['drivers']   = $this->user->get_many_by('user_role', 'driver');
        $this->data['view']      = 'order'; //Load all user's list page
        $this->layout->admin($this->data);
    }
    public function order_report()
    {
        $list = $this->rmodels->get_order_datatable();
        echo $this->reports->orderReport($list);
    }
    public function order_report_print()
    {
        // echo "hello";
        // exit();
        // print_r($_REQUEST);
        $list = $this->rmodels->get_order_datatable();
        // print_r($list);
        // exit();
        $html= $this->pdfmodels->orderReport($list);
        $this->data['html'] = $html;
        // $this->data['view'] = 'print';
        $this->load->view('print',$this->data);
        // $this->view->admin($this->data);
        // echo $html;
        // echo "hi";
        // echo $this->pdfmodels->pdfout();
    }
    public function order_vreport_print()
    {
        $list = $this->rmodels->get_order_datatable();
        $html= $this->pdfmodels->vorderReport($list);
        $this->data['html'] = $html;
        $this->load->view('print',$this->data);
    }
    public function order_list()
    {
        $list      = $this->rmodels->get_order_datatable();
        $data      = array();
        $suppliers = $this->user->get_many_by("user_role", "supplier");
        $agents    = $this->user->get_many_by("user_role", "agent");
        foreach ($list as $info) {
            $row             = array();
            $order_history = json_decode($info->order_history);
            $currency      = strtoupper($order_history->currency);
            $row[]           = "<a target='_blank' href='" . site_url(CRM_VAR) . "/order/view/" . $info->order_id . "'>" . $info->order_id . "</a><br/><a href='".site_url(CRM_VAR) ."/order/duplicate/" .$info->order_id."'>Populate</a>";
            $row[]           = $info->trip_record_name;
            $vendor_status   = "";
            $agent_status    = "";
            $order_status    = "\nOrder:" . $info->status;
            $customer_status = ($info->customer_payment_status == "complete") ? "<br/>Customer:Complete" : "<br/>Customer:Pending";
            $orderinfo       = $this->orders->get_order($info->order_id);
            $fees            = $currency."Rental:" . number_format($info->amount+($info->ot_hours*$info->driver_ot_fees));
            $pickup_date = date('m/d/Y 00:00:00', strtotime($info->pickup_date));
            $drop_date   = date('m/d/Y 23:59:59', strtotime($info->drop_date));
            $timeDiff    = strtotime($drop_date) - strtotime($pickup_date);
            $diffDays    = round(abs($timeDiff / 86400));
            
            // $_vendor_expense1=0;
            // // echo $info->driver_from;
            // if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
            //     // echo "hello";
            //     $_vendor_expense1=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
            //     $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense1 :$net;
            //     $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
            //     $_vendor_expense1 +=$driver_bonus;
            //     // $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;
            //     // $wages="<br/>Wages:" . number_format($diffDays * $info->driver_fees);
            //     // $row[]          = "OT:" . number_format($info->driver_ot_fees * $info->ot_hours) . $wages . "<br/>Bonus:" . number_format($driver_bonus);
            // }
            foreach ($suppliers as $k => $supplier) {
                if ($orderinfo->supplier_id == $supplier->id) {
                    if ($supplier->user_lname != 'Sonic Star') {
                        $vendor_status = "<br/>Vendor:" . $info->vendor_payment_status;

                        // $fees .= "<br/>Vendor:" . number_format($info->vendor_fees-$_vendor_expense);
                    }
                }
            }
            

            $is_agent = "not_agent";
            foreach ($agents as $k => $agent) {
                if ($orderinfo->user_id == $agent->id) {
                    $agent_status = "<br/>Agents:" . $info->agent_payment_status;
                    $fees .= "<br/>Agent:" . number_format($info->agent_fees);
                    $is_agent = "is_agent";
                }
            }
            

            $row[]       = $order_status . $customer_status . $vendor_status . $agent_status;
            $row[]       = $info->user_fname . ' ' . $info->user_lname;
            if ($info->rent_type == 36) {
                if ($info->airport_status == "pickup" || $info->airport_status == "dropoff") {
                    $row[] = "Airport - " . ucfirst($info->airport_status);
                } else if ($info->airport_status == "highway") {
                    $row[] = $this->attributes->get_by('id', $info->rent_type)->title . " - " . ucfirst($info->airport_status);
                } else {
                    $row[] = $this->attributes->get_by('id', $info->rent_type)->title . " - " . ucfirst($info->daily_status);
                }
            } else {
                $row[] = $this->attributes->get_by('id', $info->rent_type)->title;
            }
            $row[]          = $info->name . "<br/>" . $info->phone;
            $row[]          = $info->car_number . "<br/>" . $this->attributes->get_by('id', $info->model)->title;
            $row[]          = $info->driver_name;
            $driver_bonus   = 0;
            $driver_expense = 0;
            if ($info->status == "Completed") {
                $net = ($info->customer_total + ($info->ot_hours*$info->driver_ot_fees)) - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
            } else {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = ($info->amount+($info->driver_ot_fees*$info->ot_hours))- ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? ((1500 * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
                     
                }
            }
            if ($info->type_of_rent == 38 || $info->type_of_rent == 37) {
                if($info->user_lname != "Sonic Star"  && $info->driver_from==1){
                    $net = ($info->amount-$info->agent_fees) - $info->vendor_fees;
                }else{
                    $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + ($info->type_of_rent != 38? ($diffDays * $info->driver_fees):0) + (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                }
            }
            $_vendor_expense=0;
            // echo $info->driver_from;
            if ($info->type_of_rent == 36 && ($info->user_lname == "Sonic Star"  || $info->driver_from==1)) {
                // echo "hello";
                $_vendor_expense=($info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total +($info->driver_ot_fees * $info->ot_hours)+($diffDays * $info->driver_fees)+ (($info->type_of_rent == 38 || $info->type_of_rent == 37) ? $info->driver_salary : 0));
                $_net=($info->user_lname != "Sonic Star"  && $info->driver_from==1)? $info->vendor_fees - $_vendor_expense :$net;
                $driver_bonus   = ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                $_vendor_expense +=$driver_bonus;
                $driver_expense = ($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees) + $driver_bonus;

                if($info->user_lname == "Sonic Star"){
                    $wages="<br/>Wages:" . number_format($diffDays * $info->driver_fees);
                $ot="<br/>CarOT:" . number_format(($info->driver_ot_fees * $info->ot_hours)-(1500 * $info->ot_hours));
                $row[]          = "OT:" . number_format(1500 * $info->ot_hours) .$ot. $wages . "<br/>Bonus:" . number_format($driver_bonus);
  
            } else{
                $row [] ="";
            }
              

            } else if ($info->type_of_rent == 38 || $info->type_of_rent == 37) {
                // echo "hello 2";
                $driver_expense = $info->driver_salary;
                $row[]          = "Driver Salary: " . number_format($info->driver_salary);
            } else {
                // echo "hello 3";
                $row[] = "";
            }
            foreach ($suppliers as $k => $supplier) {
                if ($orderinfo->supplier_id == $supplier->id) {
                    if ($supplier->user_lname != 'Sonic Star') {
                        // $vendor_status = "<br/>Vendor:" . $info->vendor_payment_status;

                        $fees .= "<br/>Vendor:" . number_format($info->vendor_fees-$_vendor_expense);
                    }
                }
            }
            if($info->type_of_rent != 38){
                $fees .= "<br/>Net:".(($info->user_lname != "Sonic Star"  && $info->driver_from==1)? number_format($net+$_vendor_expense) : number_format($net));
            }else{
                $fees .= "<br/>Net:".number_format($net);
            }
            $row[]  = date('d-M-Y h:iA', strtotime($info->pickup_date));
            $row[]  = date('d-M-Y h:iA', strtotime($info->drop_date));
            $row[]  = $fees;
            // $dof =$info->calc_driver_ot_fees * $info->driver_ot_hour;
            // $row[] = number_format($info->vendor_fees);
            $row[]  = !empty($info->customer_total) ? number_format($info->customer_total) : 0;
            $row[]  = !empty($info->fuel_expense_total) ? number_format($info->fuel_expense_total) : 0;
            $row[]  = !empty($info->agent_total) ? number_format($info->agent_total) : 0;
            $row[]  = !empty($info->vendor_total) ? number_format($info->vendor_total) : 0;
            $row[]  = !empty($info->toll_expense_total) ? number_format($info->toll_expense_total) : 0;
            $row[]  = !empty($info->other_expense_total) ? number_format($info->other_expense_total) : 0;
            // $is_agent=$this->user->get_by('id',$info->user_id);
            $row[]  = $is_agent;
            $row[]  = number_format($net -$info->driver_salary- (($info->driver_from==1 && $info->user_lname != "Sonic Star")?0:$driver_bonus));
            $row[]  = $info->status;
            $row[]  = $info->type_of_rent;
            $row[]  = number_format($driver_expense);
            $row[]  = ($info->user_lname != "Sonic Star"  && $info->driver_from==1)? number_format($net+$_vendor_expense) : number_format($net);
            $row[]  = ($info->user_lname != "Sonic Star"  && $info->driver_from==1)? number_format($info->vendor_fees) : 0;
            // if($info->type_of_rent==36){
            //     $row[] = $info->type_of_rent;
            // }
            // if(isset($is_agent) && $is_agent->user_role=="agent"){
            //     $row[] = 'is_agent';
            // }else{
            //     $row[] = 'not_agent';
            // }
            // number_format($info->fuel_slip + $info->fuel_cash + $info->toll_expense + $info->other_expense + $dof + $info->driver_fees)
            // $row[] = 0;//number_format($info->profit);
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($data),
            "recordsFiltered" => $this->rmodels->count_order_filtered(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
    public function maintenance()
    {
        $this->isAuthorized();
        $this->data['car_tags']  = $this->car_manager->get_carnos();
        $this->data['workshops'] = $this->attributes->get_many_by_order('parent_id', 4435);
        $this->data['view']      = 'maintenance'; //Load all user's list page
        $this->layout->admin($this->data);
    }
    public function related_car_tag($id)
    {
        // print "<pre/>";
        echo json_encode($this->car_manager->get_many_by('user_id', $id));
    }
    public function maintenance_report()
    {
        $list = $this->rmodels->get_maintenance_datatables();
        echo $this->reports->maintenanceReport($list);
    }
    public function maintenance_report_print()
    {
        $list = $this->rmodels->get_maintenance_datatables();
        echo $this->pdfmodels->maintainReport($list);
    }
    public function maintenance_list()
    {
        $attributesbyId = $this->car_manager->get_attributes();
        $list           = $this->rmodels->get_maintenance_datatables();
        $data           = array();
        $no             = 0;
        foreach ($list as $info) {
            $show = '';
            $no++;
            $row   = array();
            $row[] = $no;
            $clist = $this->car_manager->get_car_detail($info->car_id);
            $userId="";
            foreach ($clist as $cinfo) {
                $row[] = ($cinfo->car_type > 0) ? $attributesbyId[$cinfo->car_type]['title'] : '';
                $row[] = $cinfo->car_number;
                // $row[] = ($cinfo->model>0)?$attributesbyId[$cinfo->model]['title']:'';
                $userId = $this->car_manager->get_car_supplier_detail($cinfo->user_id);
                // echo $userId->user_lname;
                // $row[]  = ($cinfo->user_id > 0) ? $userId->user_fname . ' ' . $userId->user_lname : '';
                // $row[] = ($cinfo->no_of_passanger>0)?$attributesbyId[$cinfo->no_of_passanger]['title']:'';
            }
            $row[]  = date('D d M, Y', $info->date);
            $row[]  = $info->description;
            $row[]  = $info->comments;
            $row[]  = $info->amount;
            $row[]  = ($info->recieved == 'pending') ? ($userId->user_lname=="Sonic Star")? '-' :'Pending' : 'Complete';
            $row[]  = ($info->paid == 'pending') ? 'Pending' : 'Complete';
            $row[]  = $info->workshop_name;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->car_maintain->count_all(),
            "recordsFiltered" => $this->rmodels->count_maintain_filtered(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
        // print "<pre/>";
        // print_r($output);
    }
    public function fuel()
    {
        $this->isAuthorized();
        $this->data['drivers']     = $this->user->get_many_by('user_role', 'driver');
        $this->data['car_tags']    = $this->car_manager->get_carnos();
        $this->data['gas_tations'] = $this->attributes->get_many_by_order('parent_id', 4430);
        $this->data['view']        = 'fuel'; //Load all user's list page
        $this->layout->admin($this->data);
    }
    public function fuel_list()
    {
        $this->checkPermission('car_list');
        $attributesbyId = $this->car_manager->get_attributes();
        // $list=$this->car_oilslip->get_datatables();
        $list           = $this->rmodels->get_fuel_datatables();
        $data           = array();
        $no             = 0;
        foreach ($list as $info) {
            // $show = '';
            $no++;
            $row    = array();
            $row[]  = $info->user_fname . ' ' . $info->user_lname;
            $row[]  = $info->car_number;
            $row[]  = $info->shop_name;
            $row[]  = $info->operator_name;
            $row[]  = $info->purchased_by;
            $row[]  = ucfirst($info->status);
            $row[]  = ucfirst(str_replace("_", " ", $info->type_of_use));
            $row[]  = date('D d M, Y', $info->date);
            $row[]  = $info->liter;
            $row[]  = $info->description;
            $row[]  = $info->comments;
            $row[]  = number_format($info->amount);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($data),
            "recordsFiltered" => $this->rmodels->count_fuel_filtered(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
        // print "<pre/>";
        // print_r($data);
    }
    public function fuel_report()
    {
        $list = $this->rmodels->get_fuel_datatables();
        echo $this->reports->fuelReport($list);
    }
    public function fuel_report_print()
    {
        $list = $this->rmodels->get_fuel_datatables();
        echo $this->pdfmodels->fuelReport($list);
    }
    public function driver_bonus()
    {
        $this->isAuthorized();
        $this->data['drivers']   = $this->user->get_many_by('user_role', 'driver');
        $this->data['customers'] = $this->user->get_many_by("user_active", "YES");
        $this->data['view']      = 'driver'; //Load all user's list page
        $this->layout->admin($this->data);
    }
    public function driver_bonus_list()
    {
        $querylist = $this->rmodels->get_order_rec();
        // print "<pre/>";
        // print_r($querylist);
        // $total_rental_fees   = 0;
        // $total_vendor_fees   = 0;
        // $total_agent_fees    = 0;
        // $total_fuel_expense  = 0;
        // $total_toll_expense  = 0;
        // $total_other_expense = 0;
        // $total_driver_ot     = 0;
        // $total_driver_bonus  = 0;
        // print "<pre/>";
        // print_r($querylist);
        // foreach ($querylist as $info) {
        //     $total_rental_fees += $info->amount;
        //     $total_vendor_fees += $info->vendor_total;
        //     $total_agent_fees += $info->agent_total;
        //     $total_fuel_expense += $info->fuel_expense_total;
        //     $total_other_expense += $info->other_expense_total;
        //     $pickup_date = date('m/d/Y', strtotime($info->pickup_date));
        //     $drop_date   = date('m/d/Y', strtotime($info->drop_date));
        //     $timeDiff    = strtotime($info->drop_date) - strtotime($info->pickup_date);
        //     $diffDays    = ceil($timeDiff / 86400);
        //     $total_driver_ot += ($info->driver_ot_fees * $info->ot_hours);
        //     $net = 0;
        //     if ($info->status == "Completed") {
        //         $net = $info->customer_total - ($info->vendor_total + $info->agent_total + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
        //     } else {
        //         $net = $info->amount - ($info->vendor_fees + $info->agent_fees + $info->fuel_expense_total + $info->toll_expense_total + $info->other_expense_total + (($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") ? (($info->driver_ot_fees * $info->ot_hours) + ($diffDays * $info->driver_fees)) : 0));
        //     }
        //     if ($info->type_of_rent == 36 && $info->user_lname == "Sonic Star") {
        //         $total_driver_bonus += ($net > 0) ? $net * $info->driver_percentage / 100 : 0;
        //     }
        // }
        $list      = $this->rmodels->get_driver_datatable();
        $data      = array();
        foreach ($list as $info) {
            $total_rental_fees   = 0;
            $total_vendor_fees   = 0;
            $total_agent_fees    = 0;
            $total_fuel_expense  = 0;
            $total_toll_expense  = 0;
            $total_other_expense = 0;
            $total_driver_ot     = 0;
            $total_driver_bonus  = 0;
            $row                 = array();
            $row[]               = $info->user_lname;
            $row[]               = $this->input->post('client') ? $this->input->post('client') : "All";
            $row[]               = $info->no_of_trip;
            $row[]               = $info->total_ot_hours . " Hours";
            foreach ($querylist as $key => $value) {

                # code...driver_name
                if ($value->driver_name == $info->user_lname) {
                    $total_rental_fees += $value->customer_total;
                    // $total_rental_fees += $info->amount;
                    $total_vendor_fees += $value->vendor_total;
                    $total_agent_fees += $value->agent_total;
                    $total_fuel_expense += $value->fuel_expense_total;
                    $total_other_expense += $value->other_expense_total;
                    $total_toll_expense += $value->toll_expense_total;
                    $total_driver_ot = ($value->driver_ot_fees * $info->total_ot_hours);
                    $pickup_date = date('m/d/Y', strtotime($value->pickup_date));
                    $drop_date   = date('m/d/Y', strtotime($value->drop_date));
                    $timeDiff    = strtotime($value->drop_date) - strtotime($value->pickup_date);
                    $diffDays    = ceil($timeDiff / 86400);
                    $net         = 0;
                    if ($value->status == "Completed") {
                        $net = $value->customer_total - ($value->vendor_total + $value->agent_total + $value->fuel_expense_total + $value->toll_expense_total + $value->other_expense_total + (($value->type_of_rent == 36 && $value->user_lname == "Sonic Star") ? (($value->driver_ot_fees * $info->total_ot_hours) + ($diffDays * $value->driver_fees)) : 0));
                    } else {
                        $net = $value->amount - ($value->vendor_fees + $value->agent_fees + $value->fuel_expense_total + $value->toll_expense_total + $value->other_expense_total + (($value->type_of_rent == 36 && $value->user_lname == "Sonic Star") ? (($value->driver_ot_fees * $info->total_ot_hours) + ($diffDays * $value->driver_fees)) : 0));
                    }
                    if ($value->type_of_rent == 36 && ($value->user_lname == "Sonic Star" || $value->driver_from==1)) {
                        $_net=($value->user_lname != "Sonic Star"  && $value->driver_from==1)? $value->vendor_fees - ($value->agent_fees + $value->fuel_expense_total + $value->toll_expense_total + $value->other_expense_total +($value->driver_ot_fees * $value->ot_hours)+($diffDays * $value->driver_fees)+ (($value->type_of_rent == 38 || $value->type_of_rent == 37) ? $value->driver_salary : 0)) :$net;
                            // $driver_bonus   = ($_net > 0) ? $_net * $value->driver_percentage / 100 : 0;
                        $total_driver_bonus += ($_net > 0) ? $_net * $info->driver_percentage / 100 : 0;
                        // echo $info->user_lname." -> ".$_net."\n";
                        // $total_driver_bonus+=$net;
                    }
                }
            }
            $row[]  = $total_driver_ot;
            $row[]  = number_format($total_driver_bonus);
            if($total_driver_bonus!=0){

                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => count($data),
            "recordsFiltered" => '',
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
    public function financial()
    {
        $this->isAuthorized();
        $this->data['customers']        = $this->user->get_many_by("user_active", "YES");
        $this->data['processed_by']     = $this->user->get_all();
        $this->data['air_processed_by'] = $this->airline_user->get_all();
        $this->data['payment_persons']  = $this->attributes->get_many_by_order('parent_id', 4443);
        $this->data['workshops']        = $this->attributes->get_many_by_order('parent_id', 4435);
        $this->data['gas_tations']      = $this->attributes->get_many_by_order('parent_id', 4430);
        $this->data['customers']        = $this->user->get_many_by('user_role', 'customer');
        $this->data['drivers']          = $this->user->get_many_by('user_role', 'driver');
        $this->data['agents']           = $this->user->get_many_by('user_role', 'agent');
        $this->data['suppliers']        = $this->user->get_many_by('user_role', 'supplier');
        $accounts                       = $this->accounts->get_all_with_bank();
        $this->data['accounts']         = $accounts;
        $this->data['categories']       = $this->attributes->get_many_by('parent_id', '4418');
        $list                           = $this->rmodels->get_financial_datatable();
        $banklist                       = array();
        $mmktotal                       = 0;
        $usdtotal                       = 0;
        foreach ($accounts as $key => $value) {
            if ($value->account_bank) {
                $next = $key + 1;
                if ($value->account_type == 1) {
                    $mmktotal += $value->account_price;
                }
                if ($value->account_type == 2) {
                    $usdtotal += $value->account_dollar;
                }
                if (!($accounts[$key]->account_bank == $accounts[(count($accounts) != $next) ? $next : $key]->account_bank && $next != count($accounts))) {
                    $banklist[$value->ums_bank]['mmk'] = $mmktotal;
                    $banklist[$value->ums_bank]['usd'] = $usdtotal;
                    $mmktotal                          = 0;
                    $usdtotal                          = 0;
                }
            }
        }
        $this->data['banklist'] = $banklist;
        // $this->data['deposit_total']=$deposit_total;
        // $this->data['withdraw_total']=$withdraw_total;
        $this->data['view']     = 'financial'; //Load all user's list page
        $this->layout->admin($this->data);
    }
    public function financial_report_output()
    {
        $list = $this->rmodels->get_financial_datatable();
        echo $this->reports->financialReport($list);
    }
    public function financial_report_print()
    {
        $list = $this->rmodels->get_financial_datatable();
        echo $this->pdfmodels->financialReport($list);
    }
    public function financial_list()
    {
        $querylist          = $this->rmodels->get_all_financial_datatable();
        $deposit_total_mmk  = 0;
        $withdraw_total_mmk = 0;
        $deposit_total_usd  = 0;
        $withdraw_total_usd = 0;
        // print "<pre/>";
        // print_r($querylist);
        foreach ($querylist as $info) {
            if ($info->cash_type == 1) {
                $deposit_total_mmk += ($info->payment_type == 4417) ? $info->amount : 0;
                $withdraw_total_mmk += ($info->payment_type == 4416) ? $info->amount : 0;
            }
            if ($info->cash_type == 2) {
                $deposit_total_usd += ($info->payment_type == 4417) ? $info->amount : 0;
                $withdraw_total_usd += ($info->payment_type == 4416) ? $info->amount : 0;
            }
        }
        $list = $this->rmodels->get_financial_datatable();
        $data = array();
        $no   = 0;
        foreach ($list as $info) {
            $no++;
            $row = array();
            if ($info->linked_id == 0) {
                $attachment = !empty($info->file_attachments) ? ' <i class="fa fa-paperclip"></i>' : '';
                $row[]      = "<a data-toggle='modal' data-target='#in-out-model' onclick='tran_load(\"$info->id\")'>" . date('m-d-Y / h:i', $info->created_at) . $attachment . "</a>";
            } else if ($info->category_id == 4440) {
                $row[] = "<a target='_blank' href='" . site_url(CRM_VAR) . "/oil_slip/" . $info->linked_id . "'>" . date('m-d-Y / h:i', $info->created_at) . "</a>";
            } else if ($info->category_id == 4434) {
                $row[] = "<a target='_blank' href='" . site_url(CRM_VAR) . "/car_maintenance/" . $info->linked_id . "'>" . date('m-d-Y / h:i', $info->created_at) . "</a>";
            } else if ($info->category_id == 4429) {
                $row[] = "<a target='_blank' href='" . site_url(CRM_VAR) . "/order/view/" . $info->linked_id . "'>" . date('m-d-Y / h:i', $info->created_at) . "</a>";
            } else if ($info->category_id == 4441) {
                $row[] = "<a target='_blank' href='" . site_url(CRM_VAR) . "/order/expenses/" . $info->linked_id . "'>" . date('m-d-Y / h:i', $info->created_at) . "</a>";
            } else {
                $row[] = date('m-d-Y / h:i', $info->created_at);
            }
            $row[] = ($info->transaction_date > 0) ? date('m-d-Y', $info->transaction_date) : '-';
            $row[] = $this->attributes->get_by('id', $info->category_id)->title;
            $row[] = $info->description;
            if ($info->source_from == "flight" || !isset($info->user_lname)) {
                $row[] = $this->airline_user->get_by('id', $info->created_by)->name;
            } else {
                $row[] = $info->user_lname;
            }
            $row[]       = $info->account_name;
            $row[]       = $info->payment_person;
            $paymentType = $this->attributes->get_by('id', $info->payment_type)->title;
            $row[]       = ($paymentType == "Deposit") ? number_format($info->amount, 2) : '-';
            $row[]       = ($paymentType == "Withdraw") ? number_format($info->amount, 2) : '-';
            $row[]       = number_format($info->final_balance);
            $data[]      = $row;
        }
        $count  = count($data);
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $count,
            "recordsFiltered" => $this->rmodels->count_financial_filtered(),
            "data" => $data,
            "depositAmountMMK" => number_format($deposit_total_mmk),
            "withdrawAmountMMK" => number_format($withdraw_total_mmk),
            "depositAmountUSD" => number_format($deposit_total_usd),
            "withdrawAmountUSD" => number_format($withdraw_total_usd)
        );
        //output to json format
        echo json_encode($output);
        // $output = array(
        //     "draw"            => $_POST['draw'],
        //     "recordsTotal"    => $this->car_maintain->count_all(),
        //     "recordsFiltered" => $this->car_maintain->count_all(),
        //     "data"            => $this->rmodels->get_financial_datatable(),
        // );
        // echo json_encode($output);
        // print "<pre/>";
        // print_r($data);
    }
    public function driver_bonus_report()
    {
        $list = $this->rmodels->get_driver_datatable();
        echo $this->reports->driverbonusReport($list);
    }
}
