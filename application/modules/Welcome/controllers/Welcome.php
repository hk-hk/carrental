<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->module("Layout");
	}

	public function index()
	{
		$this->layout->admin();
		
	}
}
