<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_maintain extends MY_Model {

	public $_table = 'cr_maintenance'; // database table
//public $before_create = array( 'timestamps' );
    public $column_order = array(null,'car_number','title','user_lname','date','workshop_name','description','comments','amount','paid',null);  
     //set column field database for datatable orderable

    public $column_search = array('car_id'); 
     //set column field database for datatable searchable 
    public $order = array('cr_maintenance.id' => 'asc'); // default order

        //validations rules
        public $config = array(
        array(
                'field' => 'car_id',
                'label' => 'Select Car',
                'rules' => 'required'
            )
        );

     public function __construct() {

        parent::__construct();

    }


    function _get_datatables_query() // dynamic search query of. user.
    {
        // echo json_encode($_POST);
        $this->db->select("cr_maintenance.*");
        $this->db->select("cr_maintenance.id as mid");
        $this->db->select("cr_users.user_fname");
        $this->db->select("cr_users.user_lname");
        // $this->db->select("cr_manage_attributes.*");
        $this->db->select("cr_manage_cars.*");
        $this->db->select("cr_manage_attributes.title as maker");
        $this->db->from('cr_maintenance');
        // $this->db->select("IF(cr_users.user_company!='',cr_users.user_company,cr_users.user_fname) as supplier,cr_users.profile_image as profile_image");
        // $this->db->join('`cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
        $this->db->join('cr_manage_cars`', '`cr_manage_cars`.`id` = `cr_maintenance`.`car_id`','inner');
        $this->db->join('cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
        $this->db->join('cr_manage_attributes`', '`cr_manage_attributes`.`id` = `cr_manage_cars`.`maker`','inner');
        //$this->db->join('`cr_orders`', '`cr_orders`.`car_id` = `cr_manage_cars`.`id`','left');
        // $this->db->where('cr_users.user_active', 'YES');
        $this->db->where('cr_users.user_role', 'supplier');
        if($this->input->post('car_tag'))
        {
            $this->db->where('cr_manage_cars.id', $this->input->post('car_tag'));
        }
        if($this->input->post('user_id'))
        {
            $this->db->where('cr_manage_cars.user_id', $this->input->post('user_id'));
        }
        if($this->input->post('maker'))
        {
            $this->db->where('cr_manage_attributes.id', $this->input->post('maker'));
        }
        if($this->input->post('workshop_name'))
        {
            $this->db->like('cr_maintenance.workshop_name', $this->input->post('workshop_name'));
        }
        if($this->input->post('paid'))
        {
            $this->db->where('cr_maintenance.paid',$this->input->post('paid'));
        }
        if($this->input->post('recieved'))
        {
            $this->db->where('cr_maintenance.recieved',$this->input->post('recieved'));
        }

    
        if($this->input->post('date_type'))
        {
            $dt=$this->input->post('date_type');
            if($dt=="from_to"){
                $fromto=$dt=$this->input->post('fromto');
                $this->db->where("cr_maintenance.date BETWEEN ".strtotime($fromto['from'])." AND ".strtotime($fromto['to']));
            }else if($dt=="cday"){
                $this->db->where('cr_maintenance.date',strtotime(date('m/d/y')));
            }else if($dt=="pday"){
                $this->db->where('cr_maintenance.date',strtotime(date('m/d/y',strtotime("-1 days"))));
            }else if($dt=="cmonth"){
                $cmonth= strtotime(date('M Y'));
                $lastday= date('t',strtotime($cmonth));
                $emonth= strtotime(date($lastday.' M Y'));
                $this->db->where("cr_maintenance.date BETWEEN $cmonth AND $emonth");
            }else if($dt=="pmonth"){
                $pmonth= strtotime(date('M Y',strtotime("-1 months")));
                $lastday= date('t',strtotime($pmonth));
                $emonth= strtotime(date($lastday.' M Y',strtotime("-1 months")));
                $this->db->where("cr_maintenance.date BETWEEN $pmonth AND $emonth");
            }else{
                $this->db->where('cr_maintenance.date','');
            }
        }
        // $this->db->order_by('cr_maintenance.date','desc');
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else{
                $this->db->order_by('mid', 'DESC');
            }

            
            if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
    }  
    function get_datatables()
    { 
        $this->_get_datatables_query();
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function get_details($carid) {
        $this->_get_datatables_query();
        // $this->db->select('*');
        // $this->db->from('cr_maintenance as mcm');
        $this->db->where('car_id',$carid);
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        // $carinfo = $this->db->get()->result();
        $query = $this->db->get();
        return $query->result();
    }

   
}
