<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_oilslip extends MY_Model {

	public $_table = 'cr_oil_slip'; // database table
//public $before_create = array( 'timestamps' );
    public $column_order = array(null,'date','car_id','shop_name','operator_name','liter','amount','status','type_of_use','comments',null);
     //set column field database for datatable orderable

    public $column_search = array('car_id'); 
     //set column field database for datatable searchable 
    public $order = array('cr_oil_slip.id' => 'asc'); // default order

        //validations rules
        public $config = array(
        array(
                'field' => 'car_id',
                'label' => 'Select Car',
                'rules' => 'required'
            )
        );

     public function __construct() {

        parent::__construct();

    }

    public function get_details($carid) {
        $this->_get_datatables_query();
        $this->db->where('car_id',$carid);
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();    
       // echo $this->db->last_query();die("dgfdfgs");
        return $query->result();
        // $this->db->select('*');
        // // $this->db->select('mc.id as car_id');   
        // $this->db->from('cr_oil_slip as cos');
        // $carinfo = $this->db->get()->result();
        // return $carinfo;
    }

    function _get_datatables_query() // dynamic search query of. user.
    {
        $this->db->select("cr_oil_slip.*");
        // $this->db->select("cr_manage_attributes.*");
        $this->db->from('cr_oil_slip');
        // $this->db->select("IF(cr_users.user_company!='',cr_users.user_company,cr_users.user_fname) as supplier,cr_users.profile_image as profile_image");
        // $this->db->join('`cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
        //$this->db->join('`cr_orders`', '`cr_orders`.`car_id` = `cr_manage_cars`.`id`','left');
        // $this->db->where('cr_users.user_active', 'YES');
        // $this->db->where('cr_users.user_role', 'supplier');
        if($this->input->post('car_id'))
        {
            $this->db->where('car_id', $this->input->post('car_id'));
            // echo ""
        }
        // $this->db->order_by('id','desc');
        if($this->input->post('order_by')){
            $this->db->order_by($this->input->post('order_by'), 'DESC');
        }else{
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else{
                $this->db->order_by('id', 'DESC');
            } 
            if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
        
    }  
    function get_datatables()
    { 
        $this->_get_datatables_query();
        if(isset($_POST['length']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();    
       // echo $this->db->last_query();die("dgfdfgs");
        return $query->result();
    }

}
