<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Car_manager extends MY_Model {

	public $_table = 'cr_manage_cars'; // database table
	//public $before_create = array( 'timestamps' );
        public $column_order = array(null, 'car_number','maker','model','short_desc','no_of_passanger',null);
         //set column field database for datatable orderable

        public $column_search = array('title');
         //set column field database for datatable searchable
        public $order = array('cr_manage_cars.id' => 'asc'); // default order

            //validations rules
            public $config = array(
            array(
                    'field' => 'maker',
                    'label' => 'Select Maker',
                    'rules' => 'required'
                )
            );

         public function __construct() {

            parent::__construct();

        }


        public function BokedCars($car_id){
            $from_date = $this->session->userdata('booking_from_date');
            $to_date = $this->session->userdata('booking_to_date');
            $booking_id = $this->input->post('booking_id', TRUE);
            $this->db->from('cr_supplier_booking');
            if($booking_id>0){
                $this->db->where('id!=',$booking_id);
            }
            $this->db->where(array('car_id'=>$car_id));
            //$this->db->group_start();
                $this->db->group_start();
                //$this->db->where("from_date BETWEEN '$from_date' AND '$to_date'", NULL, FALSE );
                //$this->db->where("to_date BETWEEN '$from_date' AND '$to_date'", NULL, FALSE );
                $this->db->where("'$from_date' BETWEEN from_date AND to_date", NULL, FALSE );
                $this->db->or_where("'$to_date' BETWEEN from_date AND to_date", NULL, FALSE );

                $this->db->group_end();
            $bookingInfo = $this->db->get()->result();
            //echo $this->db->last_query();print_r($bookingInfo);die;
            if(!empty($bookingInfo)){
                return $bookingInfo;
            }else{
                $this->db->from('cr_orders');
                $this->db->where('status!=','Cancelled');
                $this->db->where(array('car_id'=>$car_id));
                //$this->db->group_start();
                    $this->db->group_start();
                        //$this->db->where("pickup_date BETWEEN '$from_date' AND '$to_date'", NULL, FALSE );
                        //$this->db->where("drop_date BETWEEN '$from_date' AND '$to_date'", NULL, FALSE );
                        $this->db->where("'$from_date' BETWEEN pickup_date AND drop_date", NULL, FALSE );
                        $this->db->or_where("'$to_date' BETWEEN pickup_date AND drop_date", NULL, FALSE );
                    $this->db->group_end();

                $bookingInfo = $this->db->get()->result();
                //echo $this->db->last_query();print_r($bookingInfo);die;
                return $bookingInfo;
            }

        }

        public function BokedCars_old($car_id){
            $from_date = $this->session->userdata('booking_from_date');
            $to_date = $this->session->userdata('booking_to_date');
            $booking_id = $this->input->post('booking_id', TRUE);
            $this->db->from('cr_supplier_booking');
            if($booking_id>0){
                $this->db->where('id!=',$booking_id);
            }
            $this->db->where(array('car_id'=>$car_id));
            //$this->db->group_start();
                $this->db->group_start();
                $this->db->where("'$from_date' BETWEEN from_date AND to_date", NULL, FALSE );
                $this->db->or_where("'$to_date' BETWEEN from_date AND to_date", NULL, FALSE );

                $this->db->group_end();
            $bookingInfo = $this->db->get()->result();
            //echo $this->db->last_query();die;
            if(!empty($bookingInfo)){
                return $bookingInfo;
            }else{
                $this->db->from('cr_orders');
                $this->db->where('status!=','Cancelled');
                $this->db->where(array('car_id'=>$car_id));
                //$this->db->group_start();
                    $this->db->group_start();
                        $this->db->where("'$from_date' BETWEEN pickup_date AND drop_date", NULL, FALSE );
                        $this->db->or_where("'$to_date' BETWEEN pickup_date AND drop_date", NULL, FALSE );
                    $this->db->group_end();

                $bookingInfo = $this->db->get()->result();
                //echo $this->db->last_query();die;
                return $bookingInfo;
            }

        }

        private function _get_attr_datatables_query($is_admin) // dynamic search query of. user.
        {

            //fuel_type, car_type, rent_type, no_of_passanger
            //Get Suppier information for car
            //$this->BokedCars();

            $from_date = $this->session->userdata('booking_from_date');
            $to_date = $this->session->userdata('booking_to_date');

            $this->db->select("cr_manage_cars.*");
            $this->db->select("IF(cr_users.user_company!='',cr_users.user_company,cr_users.user_fname) as supplier,cr_users.profile_image as profile_image");
            $this->db->join('`cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
            //$this->db->join('`cr_orders`', '`cr_orders`.`car_id` = `cr_manage_cars`.`id`','left');
            $this->db->where('cr_users.user_active', 'YES');
            $this->db->where('cr_users.user_role', 'supplier');
            //Restricted car which already booked
            if(!$is_admin || $is_admin=='nopage'){
            //this statement working for front end
            $this->db->where('`cr_manage_cars`.`id` NOT IN (SELECT DISTINCT `car_id` FROM `cr_orders` WHERE ( "'.$from_date.'" BETWEEN cr_orders.pickup_date AND cr_orders.drop_date OR "'.$to_date.'" BETWEEN cr_orders.pickup_date AND cr_orders.drop_date ) AND status!="Cancelled")', NULL, FALSE);
            //this statement working for front end
            $this->db->where('`cr_manage_cars`.`id` NOT IN (SELECT DISTINCT `car_id` FROM `cr_supplier_booking` WHERE ( "'.$from_date.'" BETWEEN cr_supplier_booking.from_date AND cr_supplier_booking.to_date OR "'.$to_date.'" BETWEEN cr_supplier_booking.from_date AND cr_supplier_booking.to_date ))', NULL, FALSE);
            }
            //this statement working for ----
            //$this->db->where('`cr_manage_cars`.`id` NOT IN (SELECT DISTINCT `car_id` FROM `cr_orders` WHERE ( cr_orders.pickup_date BETWEEN "'.$from_date.'" AND "'.$to_date.'" OR cr_orders.drop_date BETWEEN "'.$from_date.'" AND "'.$to_date.'" ) AND status!="Cancelled")', NULL, FALSE);
            //Restricted car which already booked by supplier

            //this statement working for ----
            //$this->db->where('`cr_manage_cars`.`id` NOT IN (SELECT DISTINCT `car_id` FROM `cr_supplier_booking` WHERE ( cr_supplier_booking.from_date BETWEEN "'.$from_date.'" AND "'.$to_date.'" OR cr_supplier_booking.to_date BETWEEN "'.$from_date.'" AND "'.$to_date.'" ))', NULL, FALSE);

            //$this->db->where("SELECT `car_id` FROM `cr_orders` WHERE ( '2017-10-19 08:00:00' BETWEEN cr_orders.pickup_date AND cr_orders.drop_date AND '2017-10-20 18:00:00' BETWEEN cr_orders.pickup_date AND cr_orders.drop_date ) group BY car_id ORDER BY `cr_orders`.`order_id` DESC", NULL, FALSE);


            //End Get Supplier
//            $this->db->group_start();
//                    $this->db->where("'$from_date' NOT BETWEEN cr_orders.pickup_date AND cr_orders.drop_date", NULL, FALSE );
//                    $this->db->where("'$to_date' NOT BETWEEN cr_orders.pickup_date AND cr_orders.drop_date", NULL, FALSE );
//            $this->db->group_end();


            if($this->input->post('title'))
            {
                $this->db->like('title', $this->input->post('title'));
            }
            if($this->input->post('fuel_type'))
            {
                $this->db->where('fuel_type', $this->input->post('fuel_type'));
            }
            //Set car Type Filter
            $car_type = '';
            if($this->input->post('car_type')){
                $car_type1 = explode(',', $this->input->post('car_type'));
                if($car_type1[0]>0)
                    $car_type = (count($car_type1)>1 && $car_type1)?$car_type1:$car_type1[0];
            }
            if(!is_array($car_type) && $car_type>0){
                $this->db->where('car_type', $car_type);
            }else if (is_array($car_type) && count($car_type)>1){
                $this->db->group_start();
                    foreach ($car_type as $val){
                        $this->db->or_where('car_type', $val);
                    }
                $this->db->group_end();
            }
            if($this->input->post('car_company'))
            {
                $this->db->where('user_id', $this->input->post('car_company'));
            }
            //End Set car Type Filter
            /*if($this->input->post('rent_type')){
                $this->db->where('rent_type', $this->input->post('rent_type'));
            }*/
            if($this->input->post('car_city')){
                $this->db->where('car_city', $this->input->post('car_city'));
            }
            if($this->input->post('maker')){
                $this->db->where('maker', $this->input->post('maker'));
            }
            $rent_types = $this->input->post('rent_type');
            if($rent_types)
            {
                // echo $rent_types;
                if(is_array($rent_types) && count($rent_types)>0){
                    $this->db->group_start();
                    foreach ($rent_types as $val){
                        $this->db->like('rent_type', '"'.$val.'"');
                    }
                    $this->db->group_end();
                }else{
                    $this->db->like('rent_type', '"'.$rent_types.'"');
                }
                //print_r($dd);die;
                $this->db->like('rent_type', '"'.$this->input->post('rent_type').'"');
            }
            $role =  $this->session->userdata('cr_user_role');
            if($role=='supplier' && (!$this->input->post('user_id')) && $is_admin){
                $this->db->where('user_id', $this->session->userdata('crm_user_id'));
            }
            if($this->input->post('user_id')){
                $this->db->where('user_id', $this->input->post('user_id'));
            }
            if($this->input->post('no_of_passanger'))
            {
                $this->db->where('no_of_passanger', $this->input->post('no_of_passanger'));
            }
            $this->db->where_not_in('cr_manage_cars.status', '2');
            if(!$is_admin && $this->session->userdata('is_half_day')=='yes'){
                $this->db->where('half_day_avail', '1');
            }

            $this->db->from($this->_table);

            $i = 0;

            foreach ($this->column_search as $item) // loop column
            {
                if(isset($_POST['search']['value']) && $_POST['search']['value']) // if datatable send POST for search
                {
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
            $this->db->group_by('cr_manage_cars.id');
            if($this->input->post('order_by')){
                if($this->input->post('order_by')=='user_id'){

                    $this->db->order_by($this->input->post('order_by'), 'ASC');
                }else{
                    $this->db->order_by($this->input->post('order_by'), 'DESC');
                    // echo "hi";
                }
            }else{
                if(isset($_POST['order'])) // here order processing
                {
                    $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
                }
                else if(isset($this->order))
                {
                    $order = $this->order;
                    $this->db->order_by(key($order), $order[key($order)]);
                }
            }
        }

    function get_datatables($is_admin = TRUE)
    {
        $this->_get_attr_datatables_query($is_admin);
        if(!$is_admin){
            $noofRecords = 10;
            $page =  (isset($_POST['cpage']))?($_POST['cpage']-1):0;
            $start = ($page*$noofRecords);

            $this->db->where_not_in('cr_manage_cars.status', '0');
            $this->db->limit($noofRecords, $start);
        }else if($is_admin!='nopage'){

        }else{
            if(isset($_POST['length']) && $_POST['length'] != -1){
                $this->db->limit($_POST['length'], $_POST['start']);
            }
        }
        $query = $this->db->get();
        // print "<pre/>";
        // print_r($query->result());
        // exit();
        //echo $this->db->last_query();die('sdfdf');
        return $query->result();
    }

    function count_filtered($is_admin = TRUE) // Get total row count of search query
    {
        $this->_get_attr_datatables_query($is_admin = TRUE);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_odering($parentid = 0)
    {
        $this->db->from($this->_table);
        $this->db->select('ordering');
        $this->db->where('parent_id', $parentid);
        $this->db->order_by('ordering','DESC');
        $this->db->limit('1');
        $query = $this->db->get();
        $fielval = $query->row();
        return $fielval->ordering+1;
    }
    function get_car_suppliers()
    {
        $this->db->from('cr_users');
        $this->db->where(array('user_role'=>'supplier','user_active'=>'YES'));
        $this->db->group_by('id');
        $query = $this->db->get();
        $suppliers = $query->result();
        return $suppliers;
    }
    public function get_carname($carid) {
        $this->db->select('ma.title as maker,ma1.title as model');
        $this->db->from('cr_manage_cars as mc');
        $this->db->join('cr_manage_attributes as ma', 'mc.maker = ma.id','inner');
        $this->db->join('cr_manage_attributes as ma1', 'mc.model = ma1.id','inner');
        $this->db->where('mc.id',$carid);
        $carinfo = $this->db->get()->row();
        if(is_object($carinfo)){
            return $carinfo->maker.'-'.$carinfo->model;
        }else{
            return false;
        }
    }

    public function get_all_carname() {
        $this->db->select('mc.id, ma.title as maker,ma1.title as model');
        $this->db->from('cr_manage_cars as mc');
        $this->db->join('cr_manage_attributes as ma', 'mc.maker = ma.id','inner');
        $this->db->join('cr_manage_attributes as ma1', 'mc.model = ma1.id','inner');
        $carinfo = $this->db->get()->result();
        $all_names = array();
        foreach ($carinfo as $key => $value) {
            $all_names[$value->id] = $value->maker.'-'.$value->model;
        }
        return $all_names;
    }


    public function get_cartag($carid) {
        $this->db->select('mc.car_number as car_tag');
        $this->db->from('cr_manage_cars as mc');
        $this->db->where('mc.id',$carid);
        $carinfo = $this->db->get()->row();
        return $carinfo->car_tag;
    }

   public function getDestinations($city_id){
        $sql = "SELECT dest.id, dest.title, dest.short_desc, city.short_desc as title_mya FROM `cr_manage_attributes` as dest
				INNER JOIN `cr_manage_attributes` as city ON `dest`.`title`=`city`.`title` AND `city`.`parent_id`=70
				WHERE `dest`.`parent_id`=$city_id AND `dest`.`status`='1'  GROUP BY `dest`.`title` ORDER BY `city`.`id`";
        $res = $this->db->query($sql);
        return $res->result_object();
    }
    //Get Myanmar language title for destinations.
    public function getMyaDestinations($city_dest_ids){
        $titles = array();
        foreach ($city_dest_ids as $desti_id){
            if($desti_id>0){
                $sql = "SELECT title FROM `cr_manage_attributes` WHERE `id`=$desti_id";
                $res = $this->db->query($sql);
                $data = $res->result_object();
                if(!empty($data)){ $data = $data[0];
                    $sql = "SELECT * FROM `cr_manage_attributes` WHERE `title`='".$data->title."' AND parent_id=70 LIMIT 1";
                    $res = $this->db->query($sql);
                    $data = $res->result_object();
                    if(!empty($data)){ $data = $data[0];
                        $titles[$desti_id] = $data->short_desc;
                    }
                }
            }
        }
        return $titles;

    }
    function get_fuel_price($car_type_id)
    {
        $this->db->from('cr_car_type_fuel_price');
        $this->db->where(array('car_type_id'=>$car_type_id));
        $query = $this->db->get();
        $fuel_price = $query->row();
        //$fuel_price = isset($fuel_price->fuel_price)?$fuel_price->fuel_price:FALSE;
        $fuel_price = isset($fuel_price->fuel_price)?$fuel_price:FALSE;
        return $fuel_price;
    }

    public function get_carnos() {
        $this->db->select('mc.car_number as car_tag');
        $this->db->select('mc.id as car_id');
        $this->db->from('cr_manage_cars as mc');
        // $this->db->where('mc.id',$carid);
        $carinfo = $this->db->get()->result();
        return $carinfo;
    }
    public function get_car_detail($carid) {
        $this->db->select('*');
        // $this->db->select('mc.id as car_id');
        $this->db->from('cr_manage_cars as mc');
        $this->db->where('mc.id',$carid);
        $carinfo = $this->db->get()->result();
        return $carinfo;
    }
    function get_car_supplier_detail($supid)
    {
        $this->db->from('cr_users');
        // $this->db->where(array('user_role'=>'supplier','user_active'=>'YES'));
        // $this->db->group_by('id');
        $this->db->where('id',$supid);
        $query = $this->db->get();
        $supplier = $query->result()[0];
        return $supplier;
    }
     public function update_cars($val,$data){
        // $valid = true;
        $data  = $this->trigger('before_update', $data);

        if ($data !== false) {
            $result = $this->_database
                ->where('model',$val)
                ->set($data)
                ->update($this->_table);
            $this->trigger('after_update', array($data, $result));
            return $result;
        } else {
            return false;
        }
    }
}
