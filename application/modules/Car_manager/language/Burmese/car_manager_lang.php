<?php
defined('BASEPATH') OR exit('ယခုစာမ်က္နာအား ၾကည့္ရႈခြင့္မျပဳပါ');

//MultyDays Popup in City
$lang['CAR_PER_DAY_HOURS_HEADING'] = 'ေန႔စဥ္သံုးမည့္အခ်ိန္အားေရြးခ်ယ္ပါ';
$lang['CAR_PER_DAY_HOURS_SUBMIT_BTN'] = 'ႏွိပ္ပါ';
//MultyDays Popup in Highway PDDP=PER_DAY_DESTINATION_POPUP

$lang['ေန႔စဥ္ဦးတည္ရာေနရာ'] = 'ေန႔စဥ္ ခရီးအား ေရြးခ်ယ္ပါ ';
$lang['CAR_PDDP_FROM'] = 'ေန႔ရက္ွ';
$lang['CAR_PDDP_TO'] = 'သြားမည့္ၿမိဳ႕';
$lang['CAR_PDDP_PICKUP_TIME'] = ' ၾကိဳရမည့္အခ်ိန္' ;
$lang['CAR_PDDP_DROPOFF_TIME'] = 'ပို႔ရမည့္အခ်ိန္';
$lang['CAR_PER_DAY_DESTINATION_SUBMIT_BTN'] = ' ႏွိပ္ပါ';

//Car listing page
$lang['CAR_LIST_SORT_BY'] = 'အစဥ္အားျဖင့္';
$lang['CAR_LIST_SORT_BY_NEWEST'] = 'အသစ္ဝင္ေသာကားမ်ား';
$lang['CAR_LIST_SORT_BY_TOP_RATED'] = 'အျမင့္ဆံုးသတ္မွတ္ခ်က္';
$lang['CAR_LIST_SORT_BY_SUPPLIER'] = 'ကားအငွားကုမၸဏီမ်ား';

$lang['CAR_FILTER_HEADING'] = 'ကားရွာရန္';
$lang['CAR_FILTER_BY_DAILY'] = 'ရက္ အငွား';
$lang['CAR_FILTER_BY_WEEKLY'] = 'အပါတ္စဥ္ အငွား';
$lang['CAR_FILTER_BY_MONTHLY'] = 'လ အငွား';
$lang['CAR_FILTER_PICKUP_CITY'] = 'ၾကိဳရမည့္ ျမိဳ႕';
$lang['CAR_FILTER_IN_CITY'] = ' ျမိဳ႕တြင္း';
$lang['CAR_FILTER_HIGHWAY'] = ' ျမိဳ႕ျပင္ခရီး';
$lang['CAR_FILTER_DESTINATION'] = 'သြားမည့္ၿမိဳ႕';
$lang['CAR_FILTER_PICKUP_DATE_TIME'] = 'ၾကိဳရမည့္ ေန႔၊ အခ်ိန္';
$lang['CAR_FILTER_DROPOFF_DATE_TIME'] = 'ျပန္မည့္ ေန႔၊ အခ်ိန္';
$lang['CAR_FILTER_PICKUP_DATE'] = 'ၾကိဳရမည့္ ေန႔';
$lang['CAR_FILTER_NO_OF_WEEK'] = 'ပါတ္';
$lang['CAR_FILTER_NO_OF_MONTH'] = 'လ စုုစုုေပါင္း';
$lang['CAR_FILTER_REFINE_SEARCH_BTN'] = ' ျပန္ရွာရန္';
$lang['CAR_FILTER_FUEL_TYPE'] = 'ဆီအမ်ိဳးအစား';
$lang['CAR_FILTER_PASSENGERS'] = 'လူအေရအတြက္';
$lang['CAR_FILTER_CAR_TYPE'] = 'ကားအမ်ိဳးအစား';
$lang['CAR_FILTER_CAR_COMPANY'] = 'ကားအငွားကုမၸဏီမ်ား';
$lang['CAR_FILTER_SELECT_OPTION'] = 'ေရြးခ်ယ္ပါ';
$lang['CAR_FILTER_RESTART'] = 'အစမွျပန္ေရြးပါ';
$lang['CAR_FILTER_EDIT'] = 'ျပင္ဆင္ရန္';
$lang['CAR_FILTER_PICKUP_DATE'] = 'ၾကိဳရမည့္ေန႔';
$lang['CAR_FILTER_DROPOFF_DATE'] = 'ျပန္မည့္ေန႔';

$lang['FILTER_TEXT_PRICE_FOR_A_DAY'] = ' တစ္ရက္စာ ေစ်းႏႈန္း';
$lang['FILTER_TEXT_PRICE_FOR_A_WEEK'] = ' တစ္ပါတ္စာ ေစ်းႏႈန္း';
$lang['FILTER_TEXT_PRICE_FOR_A_MONTH'] = ' တစ္လစာ  ေစ်းႏႈန္း';
$lang['FILTER_TEXT_PRICE'] = ' ေစ်း ';
$lang['FILTER_TEXT_DAY'] = ' ရက္';
$lang['FILTER_TEXT_DAYS'] = ' ရက္';
$lang['FILTER_TEXT_WEEK'] = ' ပါတ္';
$lang['FILTER_TEXT_WEEKS'] = ' ပါတ္';
$lang['FILTER_TEXT_MONTH'] = ' လ';
$lang['FILTER_TEXT_MONTHS'] = ' လ';
$lang['CAR_FILTER_NO_OF_MONTHS'] = 'လ';
$lang['BOOKING_INFO_FROM_TIME'] = 'အခ်ိန္မွ';
$lang['BOOKING_INFO_TO_TIME'] = 'အခ်ိန္သို႔';
$lang['BOOKING_INFO_HOURS_IN_A_DAY'] = 'နာရီ';
$lang['BOOKING_INFO_HOURS_IN_A_DAY'] = 'နာရီ';
$lang['BOOKING_INFO_FROM'] = 'မွ';
$lang['BOOKING_INFO_TO'] = 'သို႔';
$lang['BOOKING_INFO_DISTANCE'] = 'အကြာအေ၀း';
//Checkout page
$lang['BOOKING_SUMMARY_OF_CHARGES'] = 'အက်ဥ္းခ်ဳပ္ ေစ်းနႈန္း';
$lang['BOOKING_SUMMARY_PICKUP'] = 'ၾကိဳရန္';
$lang['BOOKING_TEXT_FEATURES'] = 'Features';
$lang['BOOKING_TEXT_RATING'] = 'အဆင့္သတ္မွတ္ခ်က္';
$lang['BOOKING_TEXT_YOUR_INFORMATION'] = 'သင့္ အခ်က္အလက္';
$lang['BOOKING_TEXT_SIGN_IN'] = 'အေကာင့္သိုု႔၀င္ရန္';
$lang['BOOKING_TEXT_REGISTER'] = 'အေကာင့္သစ္ဖြင့္ရန္';
$lang['BOOKING_TEXT_REGISTER1'] = 'Customer အသစ္အတြက္';
$lang['CHECKOUT_FF_TITLE'] = 'ေခါင္းစဥ္';
$lang['CHECKOUT_FF_TITLE_OPTION1'] = 'ဦး';
$lang['CHECKOUT_FF_TITLE_OPTION3'] = 'ဒေါ်';
$lang['CHECKOUT_FF_TITLE_OPTION2'] = 'ေဒၚ';
$lang['CHECKOUT_FF_FULL_NAME'] = 'နာမည္ အျပည့္အစံု';
$lang['CHECKOUT_FF_EMAIL'] = 'အီးေမးလ္';
$lang['CHECKOUT_FF_PHONE_NO'] = 'ဖုန္းနံပါတ္';
$lang['CHECKOUT_FF_PAYMENT_OPTION1'] = 'ေငြသား';
$lang['CHECKOUT_FF_PAYMENT_OPTION2'] = 'Paypal ေငြေပးေခ်မႈ';
$lang['CHECKOUT_FF_PAYMENT_OPTION3'] = 'ဘဏ္ေငြလြဲ';
$lang['CHECKOUT_FF_BOOK_NOW_BTN'] = ' ေအာ္ဒါလုပ္မည္';
$lang['BRUDCRUMS_HOME'] = 'ပင္မ';
$lang['BRUDCRUMS_CAR_LISTING'] = 'ကားစာရင္းမ်ား';
$lang['BRUDCRUMS_CAR_DETAILS'] = 'ကားအေသးစိတ္အခ်က္အလက္';
$lang['CAR_PRICE_PER_WEEK'] = 'တစ္ပါတ္စာေစ်းႏႈန္း';
$lang['CAR_TEXT_PRICE_FOR'] = 'ေစ်းႏႈန္း အတြက္ ';
$lang['CAR_TEXT_PRICE_FOR'] = 'ေစ်းႏႈန္း အတြက္ ';


//Extra lang keywords

$lang['CAR_TEXT_CHARGES'] = ' က်သင့္ေငြ';
$lang['CAR_TEXT_DISCOUNT'] = ' ေလ်ာ့ေစ်း';
$lang['CAR_TEXT_TAX'] = 'အခြန္';
$lang['CAR_TEXT_TOTAL_AMOUNT'] = 'စုစုေပါင္းက်သင့္ေငြ';
$lang['CAR_TEXT_PACK_ALL_INCLUSIVE'] = 'အၿပီးအစီး ၀န္ေဆာင္မႈ';
$lang['CAR_TEXT_PACK_BASIC'] = '႐ိုုး႐ိုုး ၀န္ေဆာင္မႈ';
$lang['CAR_TEXT_FULL_DAY'] = 'တစ္ရက္အျပည့္ ';
$lang['CAR_TEXT_DESTINATION'] = 'သြားမည့္ၿမိဳ႕';
$lang['CAR_TEXT_HIGHWAY_PERCENTAGE'] = 'Highway Percentage';
$lang['CAR_TEXT_ADDITIONAL_HOURS_CHARGES'] = 'အခ်ိန္ပိုု က်သင့္ေငြ';
$lang['CAR_TEXT_HALF_DAY'] = ' ေန႔တစ္၀က္ ';
$lang['CAR_TEXT_INCLUSIVE'] = 'အၿပီးအစီး';
$lang['CAR_TEXT_BASIC'] = ' ႐ိုုး႐ိုုး'; 
$lang['TOTAL_AMOUNT_BY_ADMIN'] = ' Total Amount By Admin';
$lang['DISCOUNT_TEXT'] = ' ဒီကားအတြက္ေလ်ာ့ေစ်း';

$lang['PRICE_FOR_PICKUP_TEXT'] = 'Price for Pickup';
$lang['PRICE_FOR_DROPOFF_TEXT'] = 'Price for Dropoff';

//Login page
$lang['LOGIN_HEADING'] = 'အေကာင့္သိုု႔၀င္ရန္';
$lang['LOGIN_EMAIL'] = 'အီးေမးလ္';
$lang['LOGIN_PASSWORD'] = ' လွ်ိဳ႕၀ွက္နံပါတ္';
$lang['LOGIN_TEXT_BTN'] = ' အေကာင့္သိုု႔၀င္ရန္';
$lang['LOGIN_TEXT_FORGOT_PASSWORD'] = 'သင့္ လွ်ိဳ႕၀ွက္နံပါတ္ ေမ့ေနပါသလား?';
$lang['LOGIN_TEXT_OR'] = 'သို႔မဟုတ္';
$lang['LOGIN_TEXT_REGISTER'] = 'အေကာင့္သစ္ဖြင့္ရန္';

$lang['BREADCRUMBS_HOME'] = 'မူလေနရာ';
$lang['BREADCRUMBS_LOGIN'] = 'အေကာင့္သိုု႔၀င္ရန္';
$lang['LOGIN_MSG_INVALID'] = 'အေထာက္အထား မျပည့္စံုပါ!!';

//Get password page
$lang['LOST_PASS_HEADING'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အသစ္ ေျပာင္းမည္';
$lang['LOST_FIELD_PASS'] = ' လွ်ိဳ႕၀ွက္နံပါတ္';
$lang['LOST_FIELD_CON_PASS'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အတည္ျပဳပါ';
$lang['LOST_TEXT_SUBMIT_BTN'] = 'ေျပာင္းမည္';

$lang['CHANGE_PASS_INVALID_CODE'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ မွားေနပါသည္၊ ေနာက္တစ္ၾကိမ္ ထပ္လုပ္ပါ !';
$lang['BREADCRUMBS_CHANGE_PASS'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အသစ္ ေျပာင္းမည္';
$lang['CHANGE_PASS_SUCCESSFULLY'] = 'သင့္ လွ်ိဳ႕၀ွက္နံပါတ္ ေျပာင္းလဲျခင္း ေအာင္ျမင္ပါသည္';

//Forgot Password
$lang['FORGOT_PASS_HEADING'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ ေမ့ေနပါသလား';
$lang['FORGOT_EMAIL_FIELD'] = 'အီးေမးလ္';
$lang['FORGOT_SUBMIT_BUTTON'] = 'တင္သြင္းမည္';

$lang['BREADCRUMBS_FORGOT_PASS'] = 'လွ်ိဳ႕၀ွက္ကုဒ္ ေပ်ာက္ေနသည္';
$lang['FORGOT_PASS_INVALID_USER'] = 'အသံုးျပဳသူ သက္ေသခံ မဟုတ္ပါ';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL'] = 'လွ်ိဳ႕၀ွက္ကုဒ္ ေျပာင္းလဲျခင္း လင့္ အား သင့္အီးေမးလ္သို႔ပို႔လိုက္ပါျပီ၊ ေက်းဇူးျပဳ၍စစ္ၾကည့္ပါ';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL_ISSUE'] = 'အီးေမးလ္ ပို႔ျခင္းအဆင္မေျပပါ၊ ေနာက္တစ္ၾကိမ္ၾကိဳးစား ၾကည့္ပါ';

//Profile Page
$lang['MENU_MY_BOOKING'] = 'ကၽြႏု္ပ္၏ ဘြတ္ကင္';
$lang['MENU_LOGOUT'] = 'ထြက္မည္';
$lang['PROFILE_PAGE_HEADING'] = 'သင့္ကိုယ္ေရးကိုယ္တာ အခ်က္အလက္အား  ျပဳျပင္မည္';
$lang['PROFILE_FIELD_TITLE'] = 'အေၾကာင္းအရာ';
$lang['PROFILE_FIELD_TITLE_OPTION1'] = ' ဦး';
$lang['PROFILE_FIELD_TITLE_OPTION2'] = 'ေဒၚ';
$lang['PROFILE_FIELD_TITLE_OPTION3'] = 'ဒေါ်';
$lang['PROFILE_FIELD_FULL_NAME'] = 'နာမည္အျပည့္အစံု';
$lang['PROFILE_FIELD_COMPANY_NAME'] = 'ကုမၸဏီ နာမည္';
$lang['PROFILE_FIELD_POSITION'] = 'ရာထူး';
$lang['PROFILE_FIELD_EMAIL'] = 'အီးေမးလ္';
$lang['PROFILE_FIELD_PASSWORD'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ ';
$lang['PROFILE_FIELD_CON_PASSWORD'] = 'လွ်ိဳ႕၀ွက္နံပါတ္ အတည္ျပဳပါ';
$lang['PROFILE_FIELD_ADDRESS'] = 'လိပ္စာ';
$lang['PROFILE_FIELD_COUNTRY'] = ' ႏိုင္ငံ';
$lang['PROFILE_FIELD_TELEPHONE'] = 'တယ္လီဖုန္း နံပါတ္';
$lang['PROFILE_FIELD_PROFILE_IMAGE'] = 'ကိုယ္ေရးကိုယ္တာ ဓါတ္ပံု';
$lang['PROFILE_FIELD_EDIT_BTN'] = ' ျပဳျပင္မည္';
$lang['BREADCRUMBS_MY_PROFILE'] = 'မိမိ၏ စာမ်က္ႏွာ';
$lang['PROFILE_UPDATE_SUCCESSFULLY'] = ' ျပင္ဆင္ျခင္းအာင္ျမင္ပါသည္';

//Registration page
$lang['REGISTER_PAGE_HEADING'] = 'ယခုပဲ အေကာင့္ဖြင့္လိုုက္ပါ';
$lang['REGISTER_FIELD_SBT_BTN'] = 'အေကာင့္ဖြင္႔ရန္';

$lang['BREADCRUMBS_REGISTER'] = 'အေကာင့္ဖြင္႔ရန္';
$lang['REGISTER_INVALID_SUBMITION'] = 'တင္သြင္းျခင္း ပုံစံမွားေနသည္!!';
$lang['REGISTER_INVALID_PROFILE_IMG'] = 'ကိုယ္ေရးကိုယ္တာ ဓာတ္ပံု မွန္ကန္သည္';
$lang['REGISTER_SUCCESSFULL'] = 'မွတ္ပံုတင္ျခင္းေအာင္ျမင္ပါသည္၊ သင္ယခုခ်က္ျခင္း ၀င္ႏုိင္ပါျပီ!!';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS'] = 'ဤသည်ကိုအီးမေးလ်ပြီးသားကြှနျုပျတို့နှငျ့အတူမှတ်ပုံတင် !! ကျေးဇူးပြု';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS1'] = 'စကားဝှက်ကိုရ';

?>
