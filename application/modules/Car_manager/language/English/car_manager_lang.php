<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//MultyDays Popup in City
$lang['CAR_PER_DAY_HOURS_HEADING'] = 'Choose hours for each Day';
$lang['CAR_PER_DAY_HOURS_SUBMIT_BTN'] = 'Apply';
//MultyDays Popup in Highway PDDP=PER_DAY_DESTINATION_POPUP
$lang['CAR_PER_DAY_DESTINATION_HEADING'] = 'Choose destination for each Day';
$lang['CAR_PDDP_FROM'] = 'Date';
$lang['CAR_PDDP_TO'] = 'Destination';
$lang['CAR_PDDP_PICKUP_TIME'] = 'Pickup Time';
$lang['CAR_PDDP_DROPOFF_TIME'] = 'Dropoff Time';
$lang['CAR_PER_DAY_DESTINATION_SUBMIT_BTN'] = 'Apply';

//Car listing page
$lang['CAR_LIST_SORT_BY'] = 'Sort By';
$lang['CAR_LIST_SORT_BY_NEWEST'] = 'Newest';
$lang['CAR_LIST_SORT_BY_TOP_RATED'] = 'Top Rated';
$lang['CAR_LIST_SORT_BY_SUPPLIER'] = 'Supplier';

$lang['CAR_FILTER_HEADING'] = 'Vehicles search';
$lang['CAR_FILTER_BY_DAILY'] = 'Daily';
$lang['CAR_FILTER_BY_WEEKLY'] = 'Weekly';
$lang['CAR_FILTER_BY_MONTHLY'] = 'Monthly';
$lang['CAR_FILTER_PICKUP_CITY'] = 'Pick Up City';
$lang['CAR_FILTER_IN_CITY'] = 'In City';
$lang['CAR_FILTER_HIGHWAY'] = 'Highway';
$lang['CAR_FILTER_DESTINATION'] = 'Destinated location';
$lang['CAR_FILTER_PICKUP_DATE_TIME'] = 'Pickup Date & Time';
$lang['CAR_FILTER_DROPOFF_DATE_TIME'] = 'DropOff Date & Time';
$lang['CAR_FILTER_PICKUP_DATE'] = 'Pickup Date';
$lang['CAR_FILTER_NO_OF_WEEK'] = 'Weeks';
$lang['CAR_FILTER_NO_OF_MONTH'] = 'No of months';
$lang['CAR_FILTER_REFINE_SEARCH_BTN'] = 'Refine Search';
$lang['CAR_FILTER_CAR_TYPE'] = 'Car Type';
$lang['CAR_FILTER_CAR_COMPANY'] = 'Car Rental Company';
$lang['CAR_FILTER_FUEL_TYPE'] = 'Fuel type';
$lang['CAR_FILTER_PASSENGERS'] = 'Passengers';
$lang['CAR_FILTER_CAR_TYPE'] = 'Car Type';
$lang['CAR_FILTER_SELECT_OPTION'] = 'Any';
$lang['CAR_FILTER_RESTART'] = 'Search Again';
$lang['CAR_FILTER_EDIT'] = 'Modify Search';

$lang['FILTER_TEXT_PRICE_FOR_A_DAY'] = ' Price for 1 Day';
$lang['FILTER_TEXT_PRICE_FOR_A_WEEK'] = ' Price 1 Week';
$lang['FILTER_TEXT_PRICE_FOR_A_MONTH'] = ' Price 1 Month';
$lang['FILTER_TEXT_PRICE'] = ' Price ';
$lang['FILTER_TEXT_DAY'] = ' Day';
$lang['FILTER_TEXT_DAYS'] = ' Days';
$lang['FILTER_TEXT_WEEK'] = ' Week';
$lang['FILTER_TEXT_WEEKS'] = ' Weeks';
$lang['FILTER_TEXT_MONTH'] = ' Month';
$lang['FILTER_TEXT_MONTHS'] = ' Months';
$lang['CAR_FILTER_NO_OF_MONTHS'] = 'Months';
$lang['BOOKING_INFO_FROM_TIME'] = 'From Time';
$lang['BOOKING_INFO_TO_TIME'] = 'To Time';
$lang['BOOKING_INFO_HOURS_IN_A_DAY'] = 'Hours';
$lang['BOOKING_INFO_HOURS_IN_A_DAY'] = 'Hours';
$lang['BOOKING_INFO_FROM'] = 'From';
$lang['BOOKING_INFO_TO'] = 'To';
$lang['BOOKING_INFO_DISTANCE'] = 'Distance';
//Checkout page
$lang['BOOKING_SUMMARY_OF_CHARGES'] = 'Summary of charges';
$lang['BOOKING_SUMMARY_PICKUP'] = 'Pick Up';
$lang['BOOKING_TEXT_FEATURES'] = 'Features';
$lang['BOOKING_TEXT_RATING'] = 'Rating';
$lang['BOOKING_TEXT_YOUR_INFORMATION'] = 'your information';
$lang['BOOKING_TEXT_SIGN_IN'] = 'Sign in';
$lang['BOOKING_TEXT_REGISTER'] = 'Register';
$lang['BOOKING_TEXT_REGISTER1'] = 'for new customer';
$lang['CHECKOUT_FF_TITLE'] = 'Title';
$lang['CHECKOUT_FF_TITLE_OPTION1'] = 'Mr.';
$lang['CHECKOUT_FF_TITLE_OPTION3'] = 'Ms.';
$lang['CHECKOUT_FF_TITLE_OPTION2'] = 'Mrs.';
$lang['CHECKOUT_FF_FULL_NAME'] = 'Full Name';
$lang['CHECKOUT_FF_EMAIL'] = 'Email';
$lang['CHECKOUT_FF_PHONE_NO'] = 'Phone no';
$lang['CHECKOUT_FF_PAYMENT_OPTION1'] = 'Cash';
$lang['CHECKOUT_FF_PAYMENT_OPTION2'] = 'Paypal';
$lang['CHECKOUT_FF_PAYMENT_OPTION3'] = 'Bank Transfer';
$lang['CHECKOUT_FF_BOOK_NOW_BTN'] = 'Book Now';


$lang['BRUDCRUMS_HOME'] = 'Home';
$lang['BRUDCRUMS_CAR_LISTING'] = 'Car Listing';
$lang['BRUDCRUMS_CAR_DETAILS'] = 'Car Details';
$lang['CAR_PRICE_PER_WEEK'] = 'Price Per Week';
$lang['CAR_TEXT_PRICE_FOR'] = 'Price for ';
$lang['CAR_TEXT_PRICE_FOR'] = 'Price for ';

//Extra lang keywords

$lang['CAR_TEXT_CHARGES'] = ' Charges';
$lang['CAR_TEXT_DISCOUNT'] = ' Discount';
$lang['CAR_TEXT_TAX'] = 'Tax';
$lang['CAR_TEXT_TOTAL_AMOUNT'] = 'Total Amount';
$lang['CAR_TEXT_PACK_ALL_INCLUSIVE'] = 'All Inclusive Package';
$lang['CAR_TEXT_PACK_BASIC'] = 'Basic Package';
$lang['CAR_TEXT_FULL_DAY'] = 'Full Day ';
$lang['CAR_TEXT_DESTINATION'] = 'Destination';
$lang['CAR_TEXT_HIGHWAY_PERCENTAGE'] = 'Highway Percentage';
$lang['CAR_TEXT_ADDITIONAL_HOURS_CHARGES'] = 'Addition hours charge';
$lang['CAR_TEXT_HALF_DAY'] = 'Half-day ';
$lang['CAR_TEXT_INCLUSIVE'] = 'INC';
$lang['CAR_TEXT_BASIC'] = ' Basic'; 
$lang['TOTAL_AMOUNT_BY_ADMIN'] = ' Total Amount By Admin';
$lang['DISCOUNT_TEXT'] = ' DISCOUNT ON THIS CAR';
$lang['PRICE_FOR_PICKUP_TEXT'] = 'Price for Pickup';
$lang['PRICE_FOR_DROPOFF_TEXT'] = 'Price for Dropoff';


//User Login
//Login page
$lang['LOGIN_HEADING'] = 'Login';
$lang['LOGIN_EMAIL'] = 'Email';
$lang['LOGIN_PASSWORD'] = 'Password';
$lang['LOGIN_TEXT_BTN'] = 'Login';
$lang['LOGIN_TEXT_FORGOT_PASSWORD'] = 'Lost your password?';
$lang['LOGIN_TEXT_OR'] = 'or';
$lang['LOGIN_TEXT_REGISTER'] = 'Register';

$lang['BREADCRUMBS_HOME'] = 'Home';
$lang['BREADCRUMBS_LOGIN'] = 'Login';
$lang['LOGIN_MSG_INVALID'] = 'Invalid Credentials!!';

//Get password page
$lang['LOST_PASS_HEADING'] = 'Change New Password';
$lang['LOST_FIELD_PASS'] = 'Password';
$lang['LOST_FIELD_CON_PASS'] = 'Confirm Password';
$lang['LOST_TEXT_SUBMIT_BTN'] = 'change';

$lang['CHANGE_PASS_INVALID_CODE'] = 'This is not a valid code for change password. Please try again!';
$lang['BREADCRUMBS_CHANGE_PASS'] = 'Change New Password';
$lang['CHANGE_PASS_SUCCESSFULLY'] = 'Your Password has been changed sucessfully.';

//Forgot Password
$lang['FORGOT_PASS_HEADING'] = 'Lost Password';
$lang['FORGOT_EMAIL_FIELD'] = 'Email';
$lang['FORGOT_SUBMIT_BUTTON'] = 'Submit';

$lang['BREADCRUMBS_FORGOT_PASS'] = 'Lost Password';
$lang['FORGOT_PASS_INVALID_USER'] = 'This is not a valid User ID';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL'] = 'Change password link sent to your email. Please check it.';
$lang['FORGOT_PASS_PASS_LINK_SENT_TO_EMAIL_ISSUE'] = 'Please try again.. Email sending have some issues.';

//Profile Page
$lang['MENU_MY_BOOKING'] = 'My Booking';
$lang['MENU_LOGOUT'] = 'Logout';
$lang['PROFILE_PAGE_HEADING'] = 'Update Profile';
$lang['PROFILE_FIELD_TITLE'] = 'Title';
$lang['PROFILE_FIELD_TITLE_OPTION1'] = 'Mr.';
$lang['PROFILE_FIELD_TITLE_OPTION2'] = 'Mrs.';
$lang['PROFILE_FIELD_TITLE_OPTION3'] = 'Ms.';
$lang['PROFILE_FIELD_FULL_NAME'] = 'Full Name';
$lang['PROFILE_FIELD_COMPANY_NAME'] = 'Company Name';
$lang['PROFILE_FIELD_POSITION'] = 'Title / Position';
$lang['PROFILE_FIELD_EMAIL'] = 'Email';
$lang['PROFILE_FIELD_PASSWORD'] = 'Password';
$lang['PROFILE_FIELD_CON_PASSWORD'] = 'Confirm Password';
$lang['PROFILE_FIELD_ADDRESS'] = 'Address';
$lang['PROFILE_FIELD_COUNTRY'] = 'Country';
$lang['PROFILE_FIELD_TELEPHONE'] = 'Telephone No.';
$lang['PROFILE_FIELD_PROFILE_IMAGE'] = 'Profile Image';
$lang['PROFILE_FIELD_EDIT_BTN'] = 'Update Profile';
$lang['BREADCRUMBS_MY_PROFILE'] = 'My Profile';
$lang['PROFILE_UPDATE_SUCCESSFULLY'] = 'Update Successfully.';

//Registration page
$lang['REGISTER_PAGE_HEADING'] = 'sign up now';
$lang['REGISTER_FIELD_SBT_BTN'] = 'Sign up';

$lang['BREADCRUMBS_REGISTER'] = 'Sign up';
$lang['REGISTER_INVALID_SUBMITION'] = 'Invalid Form Submition!!';
$lang['REGISTER_INVALID_PROFILE_IMG'] = 'Valid Profile Image';
$lang['REGISTER_SUCCESSFULL'] = 'Register Successfully. You can login now!!';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS'] = 'This email already registered with us!! Please ';
$lang['REGISTER_EMAIL_ALREADY_USED_MGS1'] = 'Get Password';



?>

