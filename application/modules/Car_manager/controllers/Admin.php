<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->isAuthorized(); // Check user authorization
        $this->data['module'] = 'Car_manager'; // Load Users Module
        $this->load->model('Car_manager', 'car_manager'); // Load User modal
        $this->load->model('Car_maintain', 'car_maintain'); // Load Car Maintain modal
        $this->load->model('Car_oilslip', 'car_oilslip'); // Load Car Oilslip modal
        $this->load->model('Accounts/Accounts', 'account'); // Load Account modal
        $this->load->model('Users/User', 'user');
        $this->load->model('Formula','formula');
        $this->load->model('Attributes/Attributes', 'attributes');
        $this->load->model('Transation/Transations', 'transations');
        $role               = $this->session->userdata('cr_user_role');
        $this->data['role'] = $role;
        if ($role != 'supplier') {
            $this->data['suppliers'] = $this->car_manager->get_car_suppliers();
        }
        $this->session->set_userdata('active_language', 'en');
    }

    public function index()
    {
        if ($this->session->userdata('crm_user_logged') === true) {
            // check user is logged in or not
            if ($this->session->userdata('crm_user_id') > 0) {
                redirect(CRM_VAR . '/dashboard'); // if user is logged in then, Load user's dashoboard page.
            }
        }
        $this->load->view('Login'); //if user is not logged then, Load user's Login page
    }

    public function add_car($id = 0) //user add/edit function.

    {

        $this->isAuthorized(); // check user is authoried or not

        //$this->load->model('ToolModel');
        //$this->car_manager->get_odering();
        // print "<pre/>";
        // print_r($_POST);
        // exit();

        $this->data['car_attributes'] = $this->car_manager->get_attributes(array(2, 3, 4, 35, 39, 42, 45, 70), true);
        //print_r($this->data['car_attributes']);die;
        $fields = $this->getFields(true);
        foreach ($fields as $k) {
            $carinfo[$k] = '';
        }
        $carinfo['id']                   = $id;
        $carinfo['user_id']              = '';
        $this->data['carinfo']           = (object) $carinfo;
        $this->data['formFields']        = $this->getFields();
        $this->data['fields_attributes'] = $this->getAttributes();
        //print_r($this->data['fields_attributes']);

        if ($id > 0) // Check for edit user
        {
            $carinfo = $this->car_manager->get_by('id', $id);

            if (empty($carinfo) || ($this->data['role'] == 'supplier' && $carinfo->user_id != $this->session->userdata('crm_user_id'))) {
                // Set message,If user id is invalid
                $this->set_msg_flash(['msg' => 'This is not a valid car ID', 'typ' => 2]);
                redirect(CRM_VAR . '/car_list');
            }
            $this->data['carinfo'] = $carinfo;
        }
        // print "<pre/>";
        // print_r($carinfo);
        // exit();
        $this->data['view'] = 'addCar'; // Load add/edit user form
        $this->form_validation->set_rules($this->car_manager->config); // set validation rules
        if ($id > 0 && $this->form_validation->run($this) == false) // check validation for edit user
        {
            $this->layout->admin($this->data);

        } else if ($id == 0 && $this->form_validation->run() == false) // check validation for add user
        {
            $this->layout->admin($this->data);

        } else {

            $postData = array();
            foreach ($fields as $key) {
                $fieldval = $this->input->post($key, true);
                if (is_array($fieldval) && count($fieldval) > 0) {
                    $postData[$key] = json_encode($fieldval);
                } else {
                    $postData[$key] = $fieldval;
                }
            }
            $user_id = $this->input->post('user_id', true);
            if ($user_id < 1) {
                $postData['user_id'] = $this->session->userdata('crm_user_id');
            } else {
                $postData['user_id'] = $user_id;
            }

            $postData['hightway_rate'] = json_encode($this->input->post('hwr'));
            $postData['fuel_rate'] = json_encode($this->input->post('fr'));
            $postData['extra_night'] = json_encode($this->input->post('en'));


            /*car images by khushboo 1443*/
            $thumb_array = array(
                0 => array("height" => '250', "width" => '350'),
                1 => array("height" => '54', "width" => '72'),
            );
            //$thumb_array = array();
            if ($id > 0) {
                //for edit
                if (isset($_FILES['car_image']['name'])) {
                    if ($_FILES['car_image']['name'][0] == '') {
                        $postData["car_images"] = $carinfo->car_images;
                    } else {

                        if (count($_FILES['car_image']['name']) < 10) {
                            $banner_result = $this->multiple_upload('car_image', 'car', $thumb_array, './uploads/car');
                            $old_files     = json_decode($carinfo->car_images);
                            if (!is_array($old_files)) {
                                //$old_files = array();
                            }
                            /*if(!empty($old_files)){
                            foreach ($old_files as $key => $value) {
                            unlink("./uploads/car/".$value);
                            }
                            }*/
                            if (is_array($old_files) && is_array($banner_result['file_data'])) {
                                $postData["car_images"] = json_encode(array_merge($old_files, $banner_result['file_data']));
                            } else {
                                if (is_array($banner_result['file_data'])) {
                                    $postData["car_images"] = json_encode($banner_result['file_data']);
                                }
                            }
                            if ($banner_result['error'] != 0) {
                                $this->form_validation->set_rules("car_image[]", "All Valid Car Image", "required");
                            }
                        }
                    }
                }
            } else {
                //for add
                //multiple image
                if (isset($_FILES['car_image']['name'])) {
                    if ($_FILES['car_image']['name'][0] == '') {
                        $postData["car_images"] = json_decode(array());
                    } else {
                        if (count($_FILES['car_image']['name']) < 5) {
                            $banner_result          = $this->multiple_upload('car_image', 'car', $thumb_array, './uploads/car');
                            $postData["car_images"] = json_encode($banner_result['file_data']);
                            if ($banner_result['error'] != 0) {
                                $this->form_validation->set_rules("car_image[]", "All Valid Car Image", "required");
                            }
                        }
                    }
                }
            }

            /*car images by khushboo 1443*/
            // print_r($postData);
            // exit();
            //print_r($carinfo);die;
            if ($id > 0) {
                $this->checkPermission('update_car');
                if ($this->car_manager->update($id, $postData)) {
                    // Upade user's information
                    $this->set_msg_flash(['msg' => 'Car information Updated!!', 'typ' => 1]);
                    redirect(CRM_VAR . '/car_list');
                } else {redirect(CRM_VAR . '/add_car/' . $id);}
            } else {
                $this->checkPermission('add_car');
                if ($this->car_manager->insert($postData)) {
                    // Add new user
                    $this->set_msg_flash(['msg' => 'Car information Added!!', 'typ' => 1]);
                    redirect(CRM_VAR . '/add_car');
                } else {redirect(CRM_VAR . '/add_car');}
            }
        }
    }

    public function delete_image()
    {
        $id      = $this->input->post('id');
        $image   = $this->input->post('image');
        $carinfo = $this->car_manager->get_by('id', $id);
        if (!empty($carinfo)) {
            $old_files = json_decode($carinfo->car_images);
            $new_files = array();
            foreach ($old_files as $key => $value) {
                if ($value == $image) {
                    if (file_exists("./uploads/car/" . $value)) {
                        unlink("./uploads/car/" . $value);
                    }
                    if (file_exists("./uploads/car/thumb/100X100/" . $value)) {
                        unlink("./uploads/car/thumb/100X100/" . $value);
                    }
                    if (file_exists("./uploads/car/thumb/500X400/" . $value)) {
                        unlink("./uploads/car/thumb/500X400/" . $value);
                    }
                } else {
                    $new_files[] = $value;
                }
            }
            $car_images = json_encode($new_files);
            $postData   = array("car_images" => $car_images);
            if ($this->car_manager->update($id, $postData)) {
                echo 1;
            } else {
                echo 2;
            }
        }
    }
    public function listCar()
    {
        $this->checkPermission('car_list');
        $this->data['car_attributes']    = $this->car_manager->get_attributes(array(2, 3, 4, 35, 39, 42, 45), true);
        $this->data['parent_attributes'] = $this->car_manager->get_attributes(array(0), true);
        $this->data['view']              = 'car_list'; //Load all user's list page
        //die("sdfsdf");
        $this->layout->admin($this->data);
    }

    public function ajax_list_car() //function for load all user's list in ajax datatable

    {
        $this->is_ajax(); // checking for ajax request
        $this->checkPermission('car_list');
        $attributesbyId = $this->car_manager->get_attributes();
        $list           = $this->car_manager->get_datatables();
        //'title','short_desc','parent_id','ordering','status'
        $data         = array();
        $no           = $_POST['start'];
        $attrStatuses = array('0' => 'Deactive', '1' => 'Active', '2' => 'Deleted');
        foreach ($list as $info) {
            $stat = ($info->status == '0') ? 'Deactive' : 'Active';
            $show = '';
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = $info->car_number;
            $row[] = ($info->maker > 0) ? $attributesbyId[$info->maker]['title'] : '';
            $row[] = ($info->model > 0) ? $attributesbyId[$info->model]['title'] : '';
            $row[] = $info->short_desc;
            $row[] = ($info->no_of_passanger > 0) ? $attributesbyId[$info->no_of_passanger]['title'] : '';
            //$row[] = ($info->parent_id!=0)?$infoibutesbyId[$attr->parent_id]['title']:'Parent';
            //$row[] = $info->ordering;
            $isActive = ($info->status != '1') ? 'btn-danger' : 'btn-success';
            $btntxt   = $attrStatuses[$info->status]; //($info->user_active == 'NO')? 'Active' : 'Deactive';
            $status   = ($info->status == '0') ? '1' : '0';
            $dataID   = base64_encode('ID_' . $info->id);
            $row[]    = '<span data-parent="' . $dataID . '"><a class="btn ' . $isActive . ' btn-xs change-status" data-id="' . $dataID . '" data_status="' . $status . '">' . $btntxt . '</a></span>';
            if (false) // Check for manager user.
            {
                $row[] = $show . '<a href="' . site_url(CRM_VAR) . '/add_car/' . $info->id . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;';
            } else {
                //
                $row[] = $show . '<a href="' . site_url(CRM_VAR) . '/car_list/' . $info->id . '"><button class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></button></a>&nbsp;<a href="' . site_url(CRM_VAR) . '/add_car/' . $info->id . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;<button class="btn btn-danger btn-sm change-status" data_status="2" data-id="' . $dataID . '"><i class="fa fa-remove"></i></button>';

            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->car_manager->count_by(array('status' => '1')),
            "recordsFiltered" => $this->car_manager->count_filtered(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_update_car_status($uid, $status) // update user's status by ajax

    {
        $this->is_ajax();
        $this->isAuthorized();
        $this->checkPermission('update_car');
        $id  = str_replace('ID_', '', base64_decode($uid));
        $res = $this->car_manager->update($id, array('status' => $status));
        if ($res) {$output = 'Updated successfully';} else { $output = 'Please try again.';}
        echo $output;
        die();
    }
    public function ajax_delete_car($uid)
    {
        $this->is_ajax();
        $this->isAuthorized();
        $this->checkPermission('update_car');
        $id  = str_replace('ID_', '', base64_decode($uid));
        $res = $this->car_manager->delete($id);
        if ($res) {$output = 'Updated successfully';} else { $output = 'Please try again.';}
        echo $output . $res;
        die();
    }
    /*city_rate_b, city_rate_ai , city_hourly_rate_b, city_hourly_rate_ai, hightway_rate_b, hightway_rate_ai, half_day_avail
     */
    public function getFields($formKey = false)
    {
        //airport_pickup , airport_pickoff, weekely_rate, monthly_rate,
        $fields = array(
            'rent_type'            => array('type' => 'checkbox', 'label' => 'Rent Type', 'placeholder' => 'Rent Type', 'class' => 'form-control', 'required' => true, 'multiple' => true, 'default' => 36),
            'maker'                => array('type' => 'select', 'label' => 'Maker', 'placeholder' => 'Maker', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'model'                => array('type' => 'select', 'label' => 'Model', 'placeholder' => 'Model', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'model_year'           => array('type' => 'select', 'label' => 'Year', 'placeholder' => 'Year', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'car_city'             => array('type' => 'select', 'label' => 'City', 'placeholder' => 'City', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'car_type'             => array('type' => 'select', 'label' => 'Car Type', 'placeholder' => 'Car Type', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'fuel_type'            => array('type' => 'select', 'label' => 'Fuel Type', 'placeholder' => 'Fuel Type', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'no_of_passanger'      => array('type' => 'select', 'label' => 'No Of Passanger', 'placeholder' => 'No Of Passanger', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'car_condition'        => array('type' => 'select', 'label' => 'Condition', 'placeholder' => 'Condition', 'class' => 'form-control', 'required' => true, 'multiple' => false),
            'car_number'           => array('type' => 'text', 'label' => 'Car Number', 'placeholder' => 'Car Number', 'class' => 'form-control', 'required' => true),
            'city_rate_b'          => array('type' => 'number', 'label' => 'City Rate (Basic) (Car+Driver)', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => true),
            'city_rate_ai'         => array('type' => 'number', 'label' => 'City Rate (All Inclusive) (Car+Driver+Fuel)', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => true),
            'city_hourly_rate_b'   => array('type' => 'number', 'label' => 'City Halfday Rate (Basic) (Car+Driver)', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => true),
            'city_hourly_rate_ai'  => array('type' => 'number', 'label' => 'City Halfday Rate (All Inclusive) (Car+Driver+Fuel)', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => true),
            'hightway_rate_ai'     => array('type' => 'number', 'label' => 'Highway Calculation Rate', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => 'false'),
            'hightway_rate_b'      => array('type' => 'number', 'label' => 'Highway Basic Rate', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => 'false'),
            'highway_discount'     => array('type' => 'number', 'label' => 'Highway discount', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => 'false'),
            'half_day_avail'       => array('type' => 'radio', 'label' => 'Half Day Available', 'placeholder' => 'Half Day Available', 'class' => 'form-control', 'required' => '', 'default' => 1),
            'airport_pickup_rate'  => array('type' => 'number', 'label' => 'Airport Pick Up Rate', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => true),
            'airport_pickoff_rate' => array('type' => 'number', 'label' => 'Airport Drop Off Rate', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => true),
            'weekely_rate'         => array('type' => 'number', 'label' => 'Weekely Rate', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => 'false'),
            'monthly_rate'         => array('type' => 'number', 'label' => 'Monthly Rate', 'placeholder' => '0.00', 'class' => 'form-control', 'required' => 'false'),
            'short_desc'           => array('type' => 'text', 'label' => 'Description', 'placeholder' => 'Description', 'class' => 'form-control', 'required' => 'false'),
            'status'               => array('type' => 'radio', 'label' => 'Status', 'placeholder' => 'Status', 'class' => 'form-control', 'required' => true, 'default' => 1),
        );
        if ($formKey) {
            return array_keys($fields);
        } else {
            return $fields;
        }
    }
    public function getYears()
    {
        $years = array();
        for ($i = 1990; $i <= date("Y"); $i++) {
            $years[] = array('id' => $i, 'title' => $i);
        }
        return $years;
    }

    public function getAttributes()
    {
        $attributes = array(
            'status'            => array(
                array('id' => '1', 'title' => 'Publish'),
                array('id' => '0', 'title' => 'Un Publish'),
            ),
            'half_day_avail'    => array(
                array('id' => '1', 'title' => 'YES'),
                array('id' => '0', 'title' => 'NO'),
            ),
            'fuel_type'         => $this->data['car_attributes'][3],
            'airport_locations' => $this->data['car_attributes'][2],
            'car_type'          => $this->data['car_attributes'][4],
            'rent_type'         => $this->data['car_attributes'][35],
            'car_condition'     => $this->data['car_attributes'][39],
            'no_of_passanger'   => $this->data['car_attributes'][42],
            'maker'             => $this->data['car_attributes'][45],
            'car_city'          => $this->data['car_attributes'][70],
            'model'             => array('' => 'select Model'),
            'model_year'        => $this->getYears(),

        );
        return $attributes;
    }

/*car images by khushboo 1443*/

    //get model list
    public function get_models()
    {
        $maker  = $this->input->post('maker') ? $this->input->post('maker') : 0;
        $model  = $this->input->post('model') ? $this->input->post('model') : 0;
        $models = $this->car_manager->get_attributes(array($maker), true);
        $html   = '<option value=""';
        if ($model == 0) {$html .= ' selected';}
        $html .= '>Select Model</options>';
        if (isset($models[$maker])) {
            foreach ($models[$maker] as $key => $value) {
                $html .= '<option value="' . $value['id'] . '"';
                if ($model == $value['id']) {$html .= ' selected';}
                $html .= '>' . $value['title'] . '</options>';
            }
        }
        echo $html;
    }

// Start Module from Stoneland IT Solution

    public function car_maintenance($id = null)
    {
        // $this->isAuthorized(); // check user is authoried or not
        $this->data['car_tags']          = $this->car_manager->get_carnos();
        $this->data['car_attributes']    = $this->car_manager->get_attributes(array(2, 3, 4, 35, 39, 42, 45), true);
        $this->data['parent_attributes'] = $this->car_manager->get_attributes(array(0), true);
        $this->data['workshops']         = $this->attributes->get_many_by_order('parent_id', 4435);
        $this->data['view']              = 'car_maintenance'; //Load all user's list page
        $this->data['drivers']           = $this->user->get_many_by('user_role', 'driver');
        $this->data['maintain_detail']   = array();
        if ($id == null) {
            $this->data['isEdit'] = false;
        } else {
            $this->data['isEdit']          = true;
            $this->data['mid']             = $id;
            $this->data['maintain_detail'] = $this->car_maintain->get_by('id', $id);
            $this->data['car_detail'] = $this->car_manager->get_by('id', $this->data['maintain_detail']->car_id);
        }
        $this->layout->admin($this->data);
    }
    public function store_maintain($id = null)
    {
        $postData['car_id'] = $this->input->post('car_tag');
        $postData['date']   = strtotime($this->input->post('maintain_date'));
        // echo date("jS F, Y", $postData['date']);
        $postData['workshop_name'] = $this->input->post('workshop_name');
        $postData['amount']        = $this->input->post('amount');
        $postData['paid']          = $this->input->post('paid_to_workshop');
        $postData['recieved']      = $this->input->post('recieved_from_vendor');
        $postData['repaired_by']   = $this->input->post('repaired_by');
        $postData['description']   = $this->input->post('description');
        $postData['comments']      = $this->input->post('comments');
        // exit();

        $thumb_array = array(
            0 => array("height" => '250', "width" => '350'),
            1 => array("height" => '54', "width" => '72'),
        );

        if (!empty($_FILES['upload']['name'][0])) {
            if (count($_FILES['upload']['name']) < 10) {
                $banner_result = $this->multiple_upload('upload', 'file_attachments', $thumb_array, './uploads/file_attachments');
                $old_files = array();
                if (is_array($old_files) && is_array($banner_result['file_data'])) {
                    $postData["file_attachments"] = json_encode(array_merge($old_files, $banner_result['file_data']));
                } else {
                    if (is_array($banner_result['file_data'])) {
                        $postData["file_attachments"] = json_encode($banner_result['file_data']);
                    }
                }
                if ($banner_result['error'] != 0) {
                    $this->form_validation->set_rules("upload[]", "All Valid upload Image", "required");
                }
            }
        }

        
        
        $mt_insert_id="";
        if ($id == null) {
            $mt_insert_id=$this->car_maintain->insert($postData);
            // redirect('admin/car_maintenance/');
        }

        $cashAccount = $this->account->get_by('account_id', 35);

        $transactionData['category_id']     = $this->attributes->get_by('title', 'Car Maintenance')->id;
        $transactionData['cash_type']       = 1;
        $transactionData['amount']          = $this->input->post('amount');
        $transactionData['account_balance'] = $cashAccount->account_price;
        $transactionData['linked_id']       = !empty($mt_insert_id)? $mt_insert_id : $id;
        $transactionData['description']     = $this->input->post('description');
        $transactionData['created_at']      = time();
        $transactionData['created_by']      = $this->session->userdata("crm_user_id");
        $transactionData['account_id']      = $cashAccount->account_id;
        // print "<pre/>";
        // print_r($postData);
        // $cashupdate = array();
        // print "<pre/>";
        // $maintain_detail = ($id != null) ? $this->car_maintain->get_by('id', $id) : [];
        $maintain_detail = $this->car_maintain->get_by('id', !empty($mt_insert_id)? $mt_insert_id : $id);
        if ($postData['paid'] == "complete"){
            if((isset($maintain_detail->paid) && $maintain_detail->paid == "pending") || ($id==null)){
                $transactionData['payment_person']=$this->input->post('workshop_name');
                $transactionData['payment_type']  = 4416;
                $cashupdate['account_price']      = $cashAccount->account_price - $this->input->post('amount');
                $transactionData['final_balance'] = $cashupdate['account_price'];
                $transactionData['transaction_date'] = strtotime($this->input->post('maintain_date'));
                $this->transations->insert($transactionData);
                $this->account->update($cashAccount->account_id, $cashupdate);
            }

        }
        if ($postData['recieved'] == "complete"){
             $cashAccount = $this->account->get_by('account_id', 35);
            // echo "Complete recieved data <br/>";
            if((isset($maintain_detail->recieved) && $maintain_detail->recieved == "pending") || ($id==null)){
                // $transactionData['payment_person']=$this->input->post('workshop_name');
                $vendor=$this->user->get_by('id',$this->input->post('suser_id'));
                $transactionData['payment_person']=$vendor->user_fname.' '.$vendor->user_lname;
                $transactionData['payment_type']  = 4417;
                $cashupdate['account_price']      = $cashAccount->account_price + $this->input->post('amount');
                $transactionData['final_balance'] = $cashupdate['account_price'];
                $transactionData['transaction_date'] = strtotime($this->input->post('maintain_date'));
                $this->transations->insert($transactionData);
                $this->account->update($cashAccount->account_id, $cashupdate);
            }
        }

        if($id!=null) {
            $this->car_maintain->update($id, $postData);
        }
       
        // print_r($transactionData);
        redirect('admin/car_maintenance/' . $id);
    }

    // function update_maintain($id){
    //      // echo "<pre/>";
    //      // print_r($_POST);
    //      $postData['car_id']         = $_POST['car_tag'];
    //      $postData['date']           = $_POST['maintain_date'];
    //      $postData['workshop_name']  = $_POST['workshop_name'];
    //      $postData['amount']         = $_POST['amount'];
    //      $postData['paid']           = $_POST['status'];
    //      $postData['description']    = $_POST['description'];
    //      $postData['comments']       = $_POST['comments'];

    //      // $this->car_maintain->insert($postData);
    //      $this->car_maintain->update($id,$postData);

    //      redirect('admin/car_maintenance');
    // }
    public function car_detail_ajax()
    {
        $this->is_ajax();
        $car_tag        = $_POST['car_tag'];
        $attributesbyId = $this->car_manager->get_attributes();
        $list           = $this->car_manager->get_car_detail($car_tag);

        //'title','short_desc','parent_id','ordering','status'
        $data = array();
        // $no = 10;
        $attrStatuses = array('0' => 'Deactive', '1' => 'Active', '2' => 'Deleted');
        foreach ($list as $info) {
            $stat = ($info->status == '0') ? 'Deactive' : 'Active';
            $show = '';
            // $no++;
            $row = array();
            // $row[] = $no;cr_maintenance
            $row[]  = $info->car_number;
            $row[]  = ($info->maker > 0) ? $attributesbyId[$info->maker]['title'] : '';
            $row[]  = ($info->model > 0) ? $attributesbyId[$info->model]['title'] : '';
            $userId = $this->car_manager->get_car_supplier_detail($info->user_id);
            $row[]  = ($info->user_id > 0) ? $userId->user_fname . ' ' . $userId->user_lname : '';
            $row[]  = $info->short_desc;
            $row[]  = ($info->no_of_passanger > 0) ? $attributesbyId[$info->no_of_passanger]['title'] : '';
            //$row[] = ($info->parent_id!=0)?$infoibutesbyId[$attr->parent_id]['title']:'Parent';
            //$row[] = $info->ordering;
            $isActive = ($info->status != '1') ? 'btn-danger' : 'btn-success';
            $btntxt   = $attrStatuses[$info->status]; //($info->user_active == 'NO')? 'Active' : 'Deactive';
            $status   = ($info->status == '0') ? '1' : '0';
            $dataID   = base64_encode('ID_' . $info->id);
            $data[]   = $row;
        }
        echo json_encode($data);
    }

    public function car_detail($car_id)
    {
        $this->data['view']  = 'car_detail'; //Load all user's list page
        $this->data['carId'] = $car_id;

        // print "<pre/>";
        // print_r($this->data['maintain']);
        $this->layout->admin($this->data);
    }
    public function car_maintenance_delete($cm_id)
    {
        $this->car_maintain->delete($cm_id);
        redirect('admin/car_maintenance');
    }
    public function ajax_list_maintains() //function for load all user's list in ajax datatable
    {
        $this->is_ajax(); // checking for ajax request
        // $this->checkPermission('car_list');
        $attributesbyId = $this->car_manager->get_attributes();
        $list           = $this->car_maintain->get_datatables();

        $data = array();
        $no   = 0;
        foreach ($list as $info) {
            $show = '';
            $no++;
            $row = array();

            $row[] = '<a href="' . site_url(CRM_VAR) . '/car_maintenance/' . $info->mid . '">'.$info->mid.'</a>&nbsp;';
            $clist = $this->car_manager->get_car_detail($info->car_id);
            $iscompany=false;
            foreach ($clist as $cinfo) {
                $row[]  = $cinfo->car_number;
                $row[]  = ($cinfo->maker > 0) ? $attributesbyId[$cinfo->maker]['title'] : '';
                $userId = $this->car_manager->get_car_supplier_detail($cinfo->user_id);
                $row[]  = ($cinfo->user_id > 0) ? $userId->user_fname . ' ' . $userId->user_lname : '';
                $iscompany=$userId->user_lname=="Sonic Star";

            }
            $row[] = date('D d M, Y', $info->date);
            $row[] = $info->workshop_name;
            $row[] = $info->description;
            $row[] = $info->comments;
            $row[] = $info->amount;
            $row[] = $info->paid;
            $row[] = $iscompany? "-" : $info->recieved;
            if (false) // Check for manager user.
            {
                $row[] = $show . '<a href="' . site_url(CRM_VAR) . '/car_maintenance/' . $info->mid . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;';
            } else {
                // <a href="'.site_url(CRM_VAR).'/car_list/'.$info->id.'"><button class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></button></a>&nbsp;
                $row[] = $show . '<a href="' . site_url(CRM_VAR) . '/car_maintenance/' . $info->mid . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;
                <form method="POST" action="' . site_url(CRM_VAR) . '/car_maintenance_delete/' . $info->mid . '" accept-charset="UTF-8" style="display:inline"><button class="btn btn-danger btn-sm change-status" type="submit" data_status="2" onclick="return confirm(\'Confirm delete?\')"><i class="fa fa-remove"></i></button></form>';

            }
            $iscompany=false;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->car_maintain->count_all(),
            "recordsFiltered" => $this->car_maintain->count_all(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_list_car_maintain() //function for load all user's list in ajax datatable

    {
        $this->is_ajax(); // checking for ajax request
        // $this->checkPermission('car_list');
        $attributesbyId = $this->car_manager->get_attributes();
        $car_tag        = $_POST['car_id'];
        $list           = $this->car_maintain->get_details($car_tag);
        // $list = $this->car_maintain->get_datatables();

        $data = array();
        $no   = 0;
        foreach ($list as $info) {
            $show = '';
            $no++;
            $row = array();

            $row[] = $no;
            $clist = $this->car_manager->get_car_detail($info->car_id);

            foreach ($clist as $cinfo) {
                $row[]  = $cinfo->car_number;
                $row[]  = ($cinfo->maker > 0) ? $attributesbyId[$cinfo->maker]['title'] : '';
                $userId = $this->car_manager->get_car_supplier_detail($cinfo->user_id);
                $row[]  = ($cinfo->user_id > 0) ? $userId->user_fname . ' ' . $userId->user_lname : '';
            }
            $row[] = date('D d M, Y', $info->date);
            $row[] = $info->workshop_name;
            $row[] = $info->description;
            $row[] = $info->comments;
            $row[] = $info->amount;
            $row[] = ($info->paid == 'no') ? 'Unpaid' : 'Paid';
            if (false) // Check for manager user.
            {
                $row[] = $show . '<a href="' . site_url(CRM_VAR) . '/car_maintenance/' . $info->mid . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;';
            } else {
                // <a href="'.site_url(CRM_VAR).'/car_list/'.$info->id.'"><button class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></button></a>&nbsp;
                $row[] = $show . '<a href="' . site_url(CRM_VAR) . '/car_maintenance/' . $info->mid . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;
                <form method="POST" action="' . site_url(CRM_VAR) . '/car_maintenance_delete/' . $info->mid . '" accept-charset="UTF-8" style="display:inline"><button class="btn btn-danger btn-sm change-status" type="submit" data_status="2" onclick="return confirm(\'Confirm delete?\')"><i class="fa fa-remove"></i></button></form>';

            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->car_maintain->count_all(),
            "recordsFiltered" => $this->car_maintain->count_all(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
        // // $this->is_ajax(); // checking for ajax request
        // $this->checkPermission('car_list');
        // $car_tag=$_POST['car_id'];
        // $list=$this->car_maintain->get_details($car_tag);
        // $data = array();
        // $no = 0;
        // foreach ($list as $info) {
        //     $show = '';
        //     $no++;
        //     $row = array();
        //     $row[] = $no;
        //     $row[] = date('d/m/Y',$info->date);
        //     $row[] = $info->workshop_name;
        //     $row[] = $info->description;
        //     $row[] = $info->comments;
        //     $row[] = $info->amount;
        //     $row[] = $info->paid;
        //     $data[] = $row;
        // }

        // $output = array(
        //     "draw" => $_POST['draw'],
        //     "recordsTotal" => $this->car_maintain->count_all(),
        //     "recordsFiltered" => $this->car_maintain->count_all(),
        //     "data" => $data,
        // );
        // //output to json format
        // echo json_encode($output);
    }

    public function oil_slip($id = null)
    {
        // $this->isAuthorized(); // check user is authoried or not
        $this->data['car_tags'] = $this->car_manager->get_carnos();
        $this->data['gas_tations'] = $this->attributes->get_many_by_order('parent_id', 4430);
        $this->data['view']     = 'oil_slip'; //Load all user's list page
        $this->data['drivers']           = $this->user->get_many_by('user_role', 'driver');
        if ($id == null) {
            $this->data['isEdit'] = false;
        } else {
            $this->data['isEdit']     = true;
            $this->data['oid']        = $id;
            $this->data['oil_detail'] = $this->car_oilslip->get_by('id', $id);
            $this->data['car_detail'] = $this->car_manager->get_by('id', $this->data['oil_detail']->car_id);
        }
        // print "<pre/>";
        // print_r($this->data);
        $this->layout->admin($this->data);
    }
    public function store_oil_slip($id = null)
    {
        // print "<pre/>";
        // print_r($_POST);
        // exit();
        $postData['car_id']        = $this->input->post('car_tag');
        $postData['date']          = strtotime($this->input->post('slip_date'));
        $postData['slip_no']       = $this->input->post('slip_no');
        $postData['shop_name']     = $this->input->post('shop_name');
        $postData['operator_name'] = $this->input->post('car_operator_name');
        $postData['purchased_by']  = $this->input->post('purchased_by');
        $postData['liter']         = $this->input->post('liter');
        $postData['amount']        = $this->input->post('amount');
        $postData['status']        = $this->input->post('status');
        $postData['type_of_use']   = $this->input->post('type_of_use');
        $postData['comments']      = $this->input->post('comments');
        $postData['description']      = $this->input->post('description');
        $postData['order_id']      = $this->input->post('order_id');
        // $this->car_oilslip->insert($postData);
        
        $os_insert_id="";
        if ($id == null) {
            $os_insert_id=$this->car_oilslip->insert($postData);
            // redirect('admin/oil_slip');
        } else {
            $this->car_oilslip->update($id, $postData);
            
        }

        $cashAccount = $this->account->get_by('account_id', 35);
        $transactionData['category_id']     = $this->attributes->get_by('title', 'Fuel Expense')->id;
        $transactionData['cash_type']       = 1;
        $transactionData['amount']          = $this->input->post('amount');
        $transactionData['account_balance'] = $cashAccount->account_price;
        $transactionData['linked_id']       = !empty($os_insert_id)?$os_insert_id:$id;
        $transactionData['description']     = $this->input->post('description');
        $transactionData['created_at']      = time();
        $transactionData['created_by']      = $this->session->userdata("crm_user_id");
        $transactionData['account_id']      = $cashAccount->account_id;
        $cashupdate = array();
        if($postData['status']=="complete"){
            // $vendor=$this->user->get_by('id',$this->input->post('user_id'));
            $transactionData['payment_person']= $this->input->post('shop_name');
            $transactionData['payment_type']  = 4416;
            $cashupdate['account_price']      = $cashAccount->account_price - $this->input->post('amount');
            $transactionData['final_balance'] = $cashupdate['account_price'];
            $this->transations->insert($transactionData);
            $this->account->update($cashAccount->account_id, $cashupdate);
        }

        redirect('admin/oil_slip/' . $id);
        // redirect('admin/oil_slip');
    }
    public function oil_slip_delete($os_id)
    {
        $this->car_oilslip->delete($os_id);
        redirect('admin/oil_slip');
    }
    public function ajax_list_oil_slip() //function for load all user's list in ajax datatable

    {
        // $this->is_ajax(); // checking for ajax request
        // $this->checkPermission('car_list');
        $attributesbyId = $this->car_manager->get_attributes();
        $list           = $this->car_oilslip->get_datatables();
        $data           = array();
        $no             = 0;
        foreach ($list as $info) {
            $show = '';
            $no++;
            $row   = array();
            $row[] = '<a href="' . site_url(CRM_VAR) . '/oil_slip/' . $info->id . '">'.$info->id.'</a>&nbsp;';
            $row[] = date('D d M, Y', $info->date);
            $clist = $this->car_manager->get_car_detail($info->car_id);
            foreach ($clist as $cinfo) {
                $row[] = $cinfo->car_number;
                // $row[] = ($cinfo->maker>0)?$attributesbyId[$cinfo->maker]['title']:'';
                // $row[] = ($cinfo->model>0)?$attributesbyId[$cinfo->model]['title']:'';
                // $userId=$this->car_manager->get_car_supplier_detail($cinfo->user_id);
                // $row[] = ($cinfo->user_id>0)?$userId->user_fname.' '.$userId->user_lname:'';

                // $row[] = ($cinfo->no_of_passanger>0)?$attributesbyId[$cinfo->no_of_passanger]['title']:'';
            }
            // $row[] = $info->date;
            // $row[] = $info->slip_no;
            $row[] = $info->shop_name;
            $row[] = $info->operator_name;
            $row[] = $info->liter;
            $row[] = $info->amount;
            $row[] = $info->status;
            $row[] = $info->type_of_use;
            $row[] = $info->description;

            $row[] = $info->comments;
            if (false) // Check for manager user.
            {
                $row[] = $show . '<a href="' . site_url(CRM_VAR) . '/oil_slip/' . $info->id . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;';
            } else {
                // <a href="'.site_url(CRM_VAR).'/car_list/'.$info->id.'"><button class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></button></a>&nbsp;
                $delbtn='<form method="POST" action="' . site_url(CRM_VAR) . '/oil_slip_delete/' . $info->id . '" accept-charset="UTF-8" style="display:inline"><button class="btn btn-danger btn-sm change-status" type="submit" data_status="2" onclick="return confirm(\'Confirm delete?\')"><i class="fa fa-remove"></i></button></form>';

                $btns=$show . '<a href="' . site_url(CRM_VAR) . '/oil_slip/' . $info->id . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;'.($info->status=="pending"? $delbtn : '');

                $row[] = $btns;

            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->car_oilslip->count_all(),
            "recordsFiltered" => $this->car_oilslip->count_all(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);

        // print "<pre/>";
        // print_r($list);
    }
    public function ajax_list_car_oil_slip() //function for load all user's list in ajax datatable

    {

        $this->checkPermission('car_list');
        $car_tag        = $_POST['car_id'];
        $attributesbyId = $this->car_manager->get_attributes();
        // $list=$this->car_oilslip->get_datatables();
        $list = $this->car_oilslip->get_details($car_tag);
        $data = array();
        $no   = 0;
        foreach ($list as $info) {
            $show = '';
            $no++;
            $row   = array();
            $row[] = $no;
            $row[] = date('D d M, Y', $info->date);
            $clist = $this->car_manager->get_car_detail($info->car_id);
            foreach ($clist as $cinfo) {
                $row[] = $cinfo->car_number;
                // $row[] = ($cinfo->maker>0)?$attributesbyId[$cinfo->maker]['title']:'';
                // $row[] = ($cinfo->model>0)?$attributesbyId[$cinfo->model]['title']:'';
                // $userId=$this->car_manager->get_car_supplier_detail($cinfo->user_id);
                // $row[] = ($cinfo->user_id>0)?$userId->user_fname.' '.$userId->user_lname:'';

                // $row[] = ($cinfo->no_of_passanger>0)?$attributesbyId[$cinfo->no_of_passanger]['title']:'';
            }
            // $row[] = $info->date;
            // $row[] = $info->slip_no;
            $row[] = $info->shop_name;
            $row[] = $info->operator_name;
            $row[] = $info->liter;
            $row[] = $info->amount;
            $row[] = $info->status;
            $row[] = $info->type_of_use;
            $row[] = $info->comments;

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->car_oilslip->count_all(),
            "recordsFiltered" => $this->car_oilslip->count_all(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function car_formula(){
        // $this->isAuthorized(); // check user is authoried or not
        
        $this->isAuthorized(); // check user is authoried or not

        //$this->load->model('ToolModel');
        //$this->car_manager->get_odering();
        // print "<pre/>";
        // print_r($_POST);
        // exit();

        $this->data['car_attributes'] = $this->car_manager->get_attributes(array(2, 3, 4, 35, 39, 42, 45, 70), true);
        //print_r($this->data['car_attributes']);die;
        $fields = $this->getFields(true);
        foreach ($fields as $k) {
            $carinfo[$k] = '';
        }
        // $carinfo['id']                   = "";
        $carinfo['user_id']              = '';
        $this->data['carinfo']           = (object) $carinfo;
        $this->data['formFields']        = $this->getFields();
        $this->data['fields_attributes'] = $this->getAttributes();
        //print_r($this->data['fields_attributes']);

        // if ($id > 0) // Check for edit user
        // {
        //     $carinfo = $this->car_manager->get_by('id', $id);

        //     if (empty($carinfo) || ($this->data['role'] == 'supplier' && $carinfo->user_id != $this->session->userdata('crm_user_id'))) {
        //         // Set message,If user id is invalid
        //         $this->set_msg_flash(['msg' => 'This is not a valid car ID', 'typ' => 2]);
        //         redirect(CRM_VAR . '/car_list');
        //     }
        //     $this->data['carinfo'] = $carinfo;
        // }
        // print "<pre/>";
        // print_r($carinfo);
        // exit();
        $this->data['view'] = 'formula'; // Load add/edit user form
        $this->layout->admin($this->data);
    }

    public function get_formula($mid){
        echo json_encode($this->formula->get($mid));
    }

    public function modify_formula(){
        $this->isAuthorized();
        $postData['id'] = $this->input->post('model');
        $postData['model_id'] = $this->input->post('model');
        $postData['hightway_rate'] = json_encode($this->input->post('hwr'));
        $postData['fuel_rate'] = json_encode($this->input->post('fu'));
        $postData['extra_night'] = json_encode($this->input->post('en'));
        // print "<pre/>";
        // print_r($postData);
        $this->formula->delete($postData['model_id']);
        $this->formula->insert($postData);
        unset($postData['id']);
        unset($postData['model_id']);
        if ($this->car_manager->update_cars($this->input->post('model'), $postData)) {
            // Upade user's information
            $this->set_msg_flash(['msg' => 'Car Formula Updated!!', 'typ' => 1]);
            redirect(CRM_VAR . '/car_formula');
        }
    }
}
