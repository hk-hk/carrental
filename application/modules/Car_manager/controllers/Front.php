<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MY_Controller {
    public function __construct() {
        $this->data['module'] = 'Users'; // Load Users module
         $this->load->model('Users/User', 'user');
        $this->data['module'] = 'Car_manager'; // Load Users module
        $this->load->model('Car_manager', 'car_manager'); // Load User modal
        $this->load->model('Attributes/Attributes', 'attributes'); // Load User modal
        $this->load->model('Rating/Ratings', 'ratings'); // Load User modal
        $this->lang->load($this->data['module'].'/car_manager', $this->getActiveLanguage());
        parent::__construct();

    }
    public function car_details($url,$package='basic'){
        $this->data['page_title'] = getLang('BRUDCRUMS_CAR_DETAILS');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=> getLang('BRUDCRUMS_HOME')),
                                        array('url'=>site_url('cars'),'title'=> getLang('BRUDCRUMS_CAR_LISTING')),
                                        array('url'=>'','title'=> getLang('BRUDCRUMS_CAR_DETAILS')),
                                    );

        $this->data['car_attributes'] = $this->car_manager->get_attributes(array(2,3,4,35,39,42,70),TRUE);
        //print_r($this->data['car_attributes']);die;
        $makers = $this->car_manager->get_attributes(array(45,70));
        $parentsIds = array_merge(array(2,3,4,35,39,42,45,70),array_keys($makers));
        $attributesbyId = $this->car_manager->get_attributes($parentsIds);

        $this->data['is_admin_sp'] = (in_array($this->session->userdata("cr_user_role"),array('admin','service_provider'))?true:false);
        $packageurl = ($package!='basic')?'/inc':'';
        $this->session->set_userdata('car_url',$url.$packageurl);
        //Set Search Fields
        $car_id = explode("-",$url,2)[0];
        $car_slug = explode("-",$url,2)[1];
        //If car already booked then stop booking process
        $is_booked = $this->car_manager->BokedCars($car_id);
        if(count($is_booked)>0){
            redirect(site_url());
        }
        //END If car already booked then stop booking process
        $carname = $this->car_manager->get_carname($car_id);
        if(!$carname){
            redirect(site_url());
        }
        $price = $this->calculatePrice($car_id,'inc',$attributesbyId,TRUE);

        $this->data['extra_css'] = array('lightgallery.css','lightslider.css','jquery.timepicker.css','bootstrap-select.css');
        $this->data['extra_js'] = array('lightgallery.js','lightslider.js','jquery.timepicker.js','bootstrap-select.js',site_url('fb_login.js'));
        // $this->data['extra_js'] = array('jquery.flagstrap.min.js',);

        $rent_type = $this->session->userdata('rent_type');
        if($rent_type=='')
            $rent_type = 36; //Set Default Rent type

        $car_city = $this->session->userdata('car_city');
        $this->data['car_city'] = ($car_city>0)?$car_city:89; //Set By Default yagon City
        $this->data['rent_type'] = $rent_type;

        $site_user_logged = $this->session->userdata('crm_user_logged');
        if($site_user_logged){
            $userInfo = $this->user->get_by('id', $this->session->userdata('crm_user_id'));
            $this->data['userInfo'] = $userInfo;
        }else{
            $this->data['userInfo'] = (object) array('id'=>'','user_fname'=>'','user_lname'=>'','user_email'=>'','user_company'=>'','user_title'=>'','user_tel'=>'');
        }
        $get_postData = $this->session->userdata('search_data_'.$rent_type);
        $postData = $this->manageSearchPost($get_postData,$rent_type);

        if(empty($postData)){
            redirect(site_url());
        }
        if($rent_type==36){
            $fromDate = strtotime(($postData['pickup_date']));
            $toDate = strtotime(($postData['dropoff_date']));
            if($fromDate==$toDate){ //Reset data for multidays with InCity & Highway if trip is for one day.
                $this->session->set_userdata('datewise','');
            }
        }


        $carinfo = $this->car_manager->get_by('id', $car_id);

        //print_r($postData);
        $incity = (isset($postData['incity']) && $postData['incity']!='')?$postData['incity']:'city';
        $airport = (isset($postData['airport']) && $postData['airport']!='')?$postData['airport']:'';
        $calculationData = array();
        $calculationData['carinfo'] = $carinfo;

        if($package=='inc')
            $inclusive = TRUE;
        else
            $inclusive = FALSE;

        $pricingRules = $this->getPricingRules($car_id);
        $bookingHistory['pricingRules'] = $pricingRules;
        $bookingHistory['inclusive'] = $inclusive;
        //print_r($pricingRules['TaxPercentage']);die;


        //print_r($highwayRate);die($postData['destination']);
        //$currency = '<span class="currency-box">'.."</span>";
        $booking_price = '';
        $TaxPercentage = $pricingRules['TaxPercentage'];
        $DiscountPercentage = $pricingRules['DiscountPercentage'];
        $this->session->set_userdata('from_car_booking',base_url('car/'.$url.'/'.$package));
     	$this->data['discount_on_car'] = false;
        if($rent_type==37){
            //print_r($postData['no_of_weeks']);die;
            $PriceText = '<br/><span class="currency-box">Price Per Week</span>';
            $carinfos['price_default'] = getNumFormat($carinfo->weekely_rate,$this->currency).$PriceText;
            $no_of_weeks = $postData['no_of_weeks'];
            $no_of_weeksText = ($no_of_weeks>1)?$no_of_weeks.getLang('FILTER_TEXT_WEEKS'):$no_of_weeks.getLang('FILTER_TEXT_WEEK');
            $bookingPrice = ($carinfo->weekely_rate*$no_of_weeks);
            $tracking[]= array('title'=>$no_of_weeksText.getLang('CAR_TEXT_CHARGES'),'amount'=>getNumFormat($bookingPrice));
            $DiscountedAmount = (($bookingPrice*$DiscountPercentage)/100);
            //echo $DiscountedAmount;die;
            if($DiscountedAmount>0){
                $bookingPrice -=$DiscountedAmount;
                $tracking[]= array('title'=>getLang('CAR_TEXT_DISCOUNT')." $DiscountPercentage%",'amount'=>getNumFormat($DiscountedAmount));

            }
            $tax = (($bookingPrice*$TaxPercentage)/100);
            $tracking[] = array('title'=>getLang('CAR_TEXT_TAX')." $TaxPercentage%",'amount'=>getNumFormat($tax));
            $tracking[] = array('title'=>getLang('CAR_TEXT_TOTAL_AMOUNT'),'amount'=>getNumFormat(($bookingPrice+$tax)));
            $trackPrices = $tracking;
            $bookingPrice +=$tax;
            $bookingHistory['no_of_weeks'] = $no_of_weeks;
            $booking_price = $bookingPrice;
            //print_r($trackPrices);die;
        }else if($rent_type==38){
            $PriceText = '<br/><span class="currency-box">Price Per Month</span>';
            $carinfos['price_default'] = getNumFormat($carinfo->monthly_rate,$this->currency).$PriceText;
            $no_of_month = $postData['no_of_month'];
            $no_of_monthText = ($no_of_month>1)?$no_of_month.getLang('FILTER_TEXT_MONTHS'):$no_of_month.getLang('FILTER_TEXT_MONTH');
            $bookingPrice = ($carinfo->monthly_rate*$no_of_month);
            $tracking[]= array('title'=>"$no_of_monthText ".getLang('CAR_TEXT_CHARGES'),'amount'=>getNumFormat($bookingPrice));
            $DiscountedAmount = (($bookingPrice*$DiscountPercentage)/100);
            //echo $DiscountedAmount;die;
            if($DiscountedAmount>0){
                $bookingPrice -=$DiscountedAmount;
                $tracking[]= array('title'=>getLang('CAR_TEXT_DISCOUNT')." $DiscountPercentage%",'amount'=>getNumFormat($DiscountedAmount));

            }
            $tax = (($bookingPrice*$TaxPercentage)/100);
            $tracking[] = array('title'=>getLang('CAR_TEXT_TAX')." $TaxPercentage%",'amount'=>getNumFormat($tax));
            $tracking[] = array('title'=>getLang('CAR_TEXT_TOTAL_AMOUNT'),'amount'=>getNumFormat(($bookingPrice+$tax)));
            $trackPrices = $tracking;
            $bookingPrice +=$tax;
            $bookingHistory['no_of_months'] = $no_of_month;
            $booking_price = $bookingPrice;
            //print_r($trackPrices);die;
        }else if($airport!=''){
            if($airport=='pickup'){
                $textAirport = getLang('PRICE_FOR_PICKUP_TEXT');
                $bookingPrice = $carinfo->airport_pickup_rate;
            }else{
                $textAirport = getLang('PRICE_FOR_DROPOFF_TEXT');
                $bookingPrice = $carinfo->airport_pickoff_rate;
            }
            $PriceText = '<br/><span class="currency-box">Price from '.$textAirport.'</span>';
            $carinfos['price_default'] = getNumFormat($bookingPrice,$this->currency).$PriceText;

            $tracking[]= array('title'=>$textAirport,'amount'=>getNumFormat($bookingPrice));
            $DiscountedAmount = (($bookingPrice*$DiscountPercentage)/100);
            if($DiscountedAmount>0){
                $bookingPrice -=$DiscountedAmount;
                $tracking[]= array('title'=>getLang('CAR_TEXT_DISCOUNT')." $DiscountPercentage%",'amount'=>getNumFormat($DiscountedAmount));

            }
            $tax = (($bookingPrice*$TaxPercentage)/100);
            $tracking[] = array('title'=>getLang('CAR_TEXT_TAX')." $TaxPercentage%",'amount'=>getNumFormat($tax));
            $tracking[] = array('title'=>getLang('CAR_TEXT_TOTAL_AMOUNT'),'amount'=>getNumFormat(($bookingPrice+$tax)));
            $trackPrices = $tracking;
            $bookingPrice +=$tax;
            //$bookingHistory['no_of_months'] = $no_of_month;
            $booking_price = $bookingPrice;
        }else{
            $fromdate = date("Y-m-d h:i A",(strtotime($postData['pickup_date']." ".$postData['pickup_time'])));
            $todate = date("Y-m-d h:i A",(strtotime($postData['dropoff_date']." ".$postData['dropoff_time'])));

            $multiDatesWithCity = $this->session->userdata('datewise');
            $dates = array(); // No of Days
            if($incity=='in_city' && count($multiDatesWithCity)>1){
                $dates = $this->multiDatesWithInCityFilter('duration');
                $this->data['hoursperDay'] = $dates;
            }else{
                $this->data['hoursperDay'] = array();
                $dates = $this->getDatesDiff($fromdate, $todate);
            }
            if($incity=='highway'){
                $destination = ($postData['destination']);
                $highwayDistance = (isset($attributesbyId[$destination]['short_desc'])?$attributesbyId[$destination]['short_desc']:1);

                if(count($dates)>0){    //highway Distance divided in Trip days
                    $noOfDays = count($dates);
                    $highwayDistance = (($highwayDistance*2)/$noOfDays);//for round trip multiply by 2
                }
                $calculationData['highwayDistance'] = ($highwayDistance>=1)?$highwayDistance:1;
                $datewiseDestinations = $this->session->userdata('datewiseDestinations');

                if(is_array($datewiseDestinations) && count($datewiseDestinations)>1){
                    $this->data['datewiseDestinations'] = $datewiseDestinations;
                }else{
                    $this->data['datewiseDestinations'] = array();
                }
            }
            //Manage INCITY CAL
            $calculationData['incity'] = $incity;
            $calculationData['inclusive'] = $inclusive;
            $calculationData['pricingRules'] = $pricingRules;
            $calculationData['dates'] = $dates;
            if($incity=='in_city'){
                $newData = $this->manageInCityCal($calculationData);

            }else{
                $newData = $this->manageHighwayCal($calculationData);
                $pricingRules['DiscountPercentage'] = $carinfo->highway_discount; //Set Discount only for highway from car info
                $this->data['discount_on_car'] = ($carinfo->highway_discount>0)?$carinfo->highway_discount:false;
            }
            //Manage INCITY CAL
            $newData = $this->manageDiscountTax($pricingRules,$newData);
            extract($newData);
            //print_r($newData);die();
            $PriceText = '<br/><span class="currency-box">Price Per Day</span>';
            if($inclusive){
                $carinfos['price_default'] = getNumFormat($carinfo->city_rate_ai).$PriceText;
            }else{
                $carinfos['price_default'] = getNumFormat($carinfo->city_rate_b).$PriceText;
            }
        }
        $datewiseDestinations = $this->session->userdata('datewiseDestinations');
        if(is_array($datewiseDestinations) && count($datewiseDestinations)>=1){
            $this->data['datewiseDestinations'] = $datewiseDestinations;
        }else{
            $this->data['datewiseDestinations'] = array();
        }

        $this->data['bookingPrice'] = getNumFormat($bookingPrice,TRUE);
        $this->data['trackPrices'] = $trackPrices;
        $this->data['searchData'] = $postData;

        $this->data['currency'] = $this->currency;
        //print_r($postData);die;
        //End Search fields


        $this->data['view'] = 'front/car_details';
        $this->data['attributesbyId'] = $attributesbyId;


        $carinfo = $this->car_manager->get_by('id', $car_id);

        $carinfos['car_id'] = $carinfo->id;
        //Set Features
        $carinfos['features'][] = $carname;
        $carinfos['features'][] = $attributesbyId[$carinfo->car_type]['title'];
        $carinfos['features'][] = $attributesbyId[$carinfo->fuel_type]['title'];
        $carinfos['features'][] = $attributesbyId[$carinfo->no_of_passanger]['title'];
        //Set Car Images
        $images = json_decode($carinfo->car_images);
        if(!isset($images[0]) || $images[0]=='')
                $images[0] = 'default.jpg'; //Set Default car Image.
         $carinfos['images']= $images;
        $termsOptions = $this->getTermsOptions();

        //print_r($termsOptions[187]);die;
        $isHighway = $this->checkIsHighway($postData);
        if($airport!=''){
            $carinfos['package'] = getLang('CAR_TEXT_PACK_ALL_INCLUSIVE');
            ksort($termsOptions[4386]);
            $carinfos['pack_feature'] = $termsOptions[4386];
        }else{
            if($isHighway){
                if($inclusive){
                    $carinfos['package'] = getLang('CAR_TEXT_PACK_ALL_INCLUSIVE');
                    ksort($termsOptions[15]);
                    $carinfos['pack_feature'] = $termsOptions[15];
                }else{
                    $carinfos['package'] = getLang('CAR_TEXT_PACK_BASIC');
                    ksort($termsOptions[186]);
                    $carinfos['pack_feature'] = $termsOptions[186];
                }
            }else{
                if($inclusive){
                    $carinfos['package'] = getLang('CAR_TEXT_PACK_ALL_INCLUSIVE');
                    ksort($termsOptions[187]);
                    $carinfos['pack_feature'] = $termsOptions[187];
                }else{
                    $carinfos['package'] = getLang('CAR_TEXT_PACK_BASIC');
                    if($rent_type==37){
                            ksort($termsOptions[4379]);
                            $carinfos['pack_feature'] = $termsOptions[4379];
                    }else if($rent_type==38){
                            ksort($termsOptions[4378]);
                            $carinfos['pack_feature'] = $termsOptions[4378];
                    }else{
                            ksort($termsOptions[188]);
                            $carinfos['pack_feature'] = $termsOptions[188];
                    }
                }
            }
        }


        $carinfos['ratings']= $carinfo->ratings;
        $carinfos['supplier_id']= $carinfo->user_id;
        $carinfos['carname']= $carname;
        $carinfos['car_demol_img']= $attributesbyId[$carinfo->model]['short_desc'];

        $UserInfo = $this->user->get_by('id', $carinfo->user_id);
        //print_r($UserInfo);die;
        $carinfos['supplier']= ($UserInfo->user_company!='')?$UserInfo->user_company:$UserInfo->user_fname;
        $carinfos['profile_image']= ($UserInfo->profile_image!='')?$UserInfo->profile_image:'user_default.png';


        $this->data['datewise'] = $this->session->userdata('datewise');
        //print_r($this->data['datewise']);die;
        $this->data['carinfo'] = $carinfos;
        $carinfo = array('car_id'=>$carinfos['car_id'],'carname'=>$carinfos['carname'],'price_default'=>$carinfos['price_default'],'supplier_id'=>$carinfo->user_id);
        $setHistoryInfo = array('car_city','rent_type','hoursperDay','datewiseDestinations','bookingPrice','trackPrices','searchData','currency','datewise');
        foreach ($setHistoryInfo as $section){
            if(isset($this->data[$section]))
                $bookingHistory[$section] = $this->data[$section];
            else
                continue;
        }
        //die($airport);
        $this->data['airport'] = $airport;
        $bookingHistory['carinfo'] = $carinfo;
        $this->session->set_userdata('booking_info',$bookingHistory);
        if(isset($this->data['userInfo']->user_role)){

            $user_role=$this->data['userInfo']->user_role;
            if($user_role=='admin' || $user_role=='service_provider'){
                $this->data['customers']=$this->user->get_role_customers('customer');
                $this->data['agents']=$this->user->get_role_customers('agent');
            }
        }
        $this->layout->front($this->data);
    }



    public function car_list(){

        $this->data['page_title'] = getLang('BRUDCRUMS_CAR_LISTING');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=>getLang('BRUDCRUMS_HOME')),
                                        array('url'=>'','title'=>getLang('BRUDCRUMS_CAR_LISTING')),
                                    );
        //Set Search Fields
        $this->data['extra_css'] = array('lightgallery.css','lightslider.css','jquery.timepicker.css','select2.min.css','select2.css');
        $this->data['extra_js'] = array('lightgallery.js','lightslider.js','jquery.timepicker.js','select2.min.js','select2.js','custom_car_listing.js');

        $rentTypes = array(36=>'daily',37=>'weekly',38=>'monthly');
        $rent_type = $this->input->post('rent_type', TRUE);
        if(!$rent_type){
            $rent_type = $this->session->userdata('rent_type');
        }
        if($rent_type=='')
            $rent_type = 36; //Set Default Rent type
        $this->data['rent_type'] = $rent_type;
        $car_city = $this->input->post('car_city', TRUE);
        if(!$car_city){
            $car_city = $this->session->userdata('car_city');
        }else{
            $this->session->set_userdata('car_city',$car_city);
        }
        $this->data['car_city'] = $car_city;
        $postData = array();
        if($rent_type>0){
            if(isset($_REQUEST[$rentTypes[$rent_type]])){
                $get_postData = $this->input->post($rentTypes[$rent_type], TRUE);
                $postData = $this->manageSearchPost($get_postData,$rent_type);

                $this->session->set_userdata('rent_type',$rent_type);
                $hoursperDay = $this->multiDatesWithInCityFilter(false);
                if(is_array($hoursperDay) && count($hoursperDay)>0){

                    $pickup = current($hoursperDay);
                    $dropoff = end($hoursperDay);
                    $postData['pickup_time'] = $pickup['from'];
                    $postData['dropoff_time'] = $dropoff['to'];
                    // print_r($postData);
                }
                $this->session->set_userdata('search_data_'.$rent_type,$postData);
            }else{
                $get_postData = $this->session->userdata('search_data_'.$rent_type);
                $postData = $this->manageSearchPost($get_postData,$rent_type);
            }
        }else{
            $this->data['rent_type'] = $rent_type;
        }
        $airport = '';
        if($rent_type==36){
            $airport = ((isset($postData['airport']) && $postData['airport']!='')?$postData['airport']:'');
            //print_r($postData);die;
            if($postData['incity']=='highway'){
                $this->data['daily']['destination'] = $postData['destination'];
                $datewiseDestinations = $this->session->userdata('datewiseDestinations');
                //print_r($datewiseDestinations);die("sdf");
                if(is_array($datewiseDestinations) && count($datewiseDestinations)>=1){
                    $firstDay = current($datewiseDestinations);
                    $this->data['daily']['destination'] = $firstDay['to'];
                    $this->data['datewiseDestinations'] = $datewiseDestinations;
                }else{
                    //$datewiseDestinations1 = $this->set_multi_destination($postData);
                    //$this->data['datewiseDestinations'] = $datewiseDestinations1;
                }
            }
            if($postData['pickup_date']==$postData['dropoff_date']){
                $pickupdate = $postData['pickup_date']." ".$postData['pickup_time'];
                $dropoff_date = $postData['pickup_date']." ".$postData['dropoff_time'];
                $strtoHours = strtotime($dropoff_date) - strtotime($pickupdate);
                $hours = (int)($strtoHours/3600);
                $minutes = (int)(($strtoHours%3600)/60);
                if($hours<0 || $minutes<0){
                    $postData['pickup_time'] = "8:00am";
                    $postData['dropoff_time'] = "6:00pm";
                }
            }
        }else if($rent_type==37){
            $postData['dropoff_date']=date('m/d/Y',strtotime("+".$postData['no_of_weeks']." weeks"));
        }else if($rent_type==38){
            $postData['dropoff_date']=date('m/d/Y',strtotime("+".$postData['no_of_month']." months"));
        }
        $filters = $this->session->userdata('filters');
        $postData = $this->manageSearchPost($postData,$rent_type);
        $this->data['searchData'] = $postData;
        $this->data['filters'] = $filters;
        $this->data['airport'] = $airport;


        //End Search fields



        $multiDatesWithCity = $this->session->userdata('datewise');
        if(is_array($multiDatesWithCity) && count($multiDatesWithCity)>0){
            $this->data['multiDatesWithCity'] = $multiDatesWithCity;
        }else{
            $this->data['multiDatesWithCity'] = array();
        }
        // $this->data['in_city']=$postData['incity'];
        $this->data['car_attributes'] = $this->car_manager->get_attributes(array(2,3,4,35,39,42,70),TRUE);
        $this->data['view'] = 'front/car_list';
        $attributesbyId = $this->car_manager->get_attributes();
        $this->data['attributesbyId'] = $attributesbyId;
        $this->data['suppliers'] = $this->car_manager->get_car_suppliers();
        // print "<pre/>";
        // // print_r($_POST);
        // print_r($this->data['']);
        // exit();
        $this->layout->front($this->data);
    }

    public function car_search(){
        $rentTypes = array('daily'=>36,'weekly'=>37,'monthly'=>38);
        $rent_type = $this->input->post('rent_type');
        $postData = $this->input->post($rent_type);
        $postData['rent_type'] = $rentTypes[$rent_type];
        $this->session->set_userdata($newdata);
         print_r($postData);die($rent_type);
    }


    public function cars_list(){

        //Set Search Fields
        $rentTypes = array(36=>'daily',37=>'weekly',38=>'monthly');
        $rent_type = $this->input->post('rent_type', TRUE);
        if(!$rent_type){
            $rent_type = $this->session->userdata('rent_type');
        }


        $filters['fuel_type'] = $this->input->post('fuel_type', TRUE);
        $filters['no_of_passanger'] = $this->input->post('no_of_passanger', TRUE);
        $filters['car_type'] = $this->input->post('car_type', TRUE);
        $filters['order_by'] = $this->input->post('order_by', TRUE);
        $this->session->set_userdata('filters',$filters);
        if($rent_type>0){
            // echo $_REQUEST[$rentTypes[$rent_type]];

            // print_r($_REQUEST);
            if(isset($_REQUEST[$rentTypes[$rent_type]])){
                $get_postData = $this->input->post($rentTypes[$rent_type], TRUE);

                $postData = $this->manageSearchPost($get_postData,$rent_type);
                $this->session->set_userdata('rent_type',$rent_type);
                $this->session->set_userdata('search_data_'.$rent_type,$postData);
            }else{
                $postData = $this->session->userdata('search_data_'.$rent_type);
            }
        }else{
            $get_postData = $this->input->post($rentTypes[36], TRUE);
            $postData = $this->manageSearchPost($get_postData,36);
        }

        //Reset Datewise from and To time for multiday with IN City Option
        if(isset($postData['incity'])){
            if($postData['incity']=='highway' || (strtotime($postData['pickup_date'])==strtotime($postData['dropoff_date']))){
                $this->session->set_userdata('datewise',array());
            }
        }
        //print_r();die;
        //End Search fields
        $this->data['rent_type'] = $rent_type;
        $car_city = $this->input->post('car_city', TRUE);
        if($car_city){
            $this->session->set_userdata('car_city',$car_city);
        }
        $datewise = $this->session->userdata('datewise');
        $datewiseDestinations = $this->session->userdata('datewiseDestinations');
        $noOfDays = 0;
        if(is_array($datewise) && count($datewise)>1){
            $noOfDays = count($datewise);
        }else if (is_array($datewiseDestinations) && count($datewiseDestinations)>1){
            $noOfDays = (count($datewiseDestinations)-2);
        }
        //print_r(($datewiseDestinations));die;
        $prodList = $this->car_manager->get_datatables(FALSE);
        //echo $this->db->last_query();die;
        //$attributesbyId = $this->car_manager->get_attributes();
        $makers = $this->car_manager->get_attributes(array(45,70));
        //print_r(array_keys($makers));die;
        $parentsIds = array_merge(array(2,3,4,35,39,42,45,70),array_keys($makers));
        $attributesbyId = $this->car_manager->get_attributes($parentsIds);
        //ksort($attributesbyId);
        //print_r($attributesbyId);die;
        $data['pages'] = array(1,2,3,4);
        $data['attributesbyId'] = $attributesbyId;
        $pdlis = array();
        $isHighway = $this->checkIsHighway($postData);
        $termsOptions = $this->getTermsOptions();
        $airport = ((isset($postData['airport']) && $postData['airport']!='')?$postData['airport']:'');
        //print_r($postData);die;
        //$currency = '<span class="currency-box">'.$this->currency."</span>";
        foreach ($prodList as $k=>$p){
            // echo $rent_type;
            $row = array();
            //print_r($attributesbyId);
            //print_r($attributesbyId);die($p->car_type);
            $row['car_id'] = $p->id;


            $row['carname']= $attributesbyId[$p->maker]['title']."-".$attributesbyId[$p->model]['title'];
            //Car Features
            $row['features'][] = $attributesbyId[$p->maker]['title']."-".$attributesbyId[$p->model]['title'];
            $row['features'][] = $attributesbyId[$p->car_type]['title'];
            $row['features'][] = $attributesbyId[$p->fuel_type]['title'];
            $row['features'][] = $attributesbyId[$p->no_of_passanger]['title'];

            //Car Images
            $images = json_decode($p->car_images);
            if(!isset($images[0]) || $images[0]=='')
                $images[0] = 'default.jpg';
            $row['images']= $images;
            //Set Supplier name and image
            $row['supplier']= $p->supplier;
            $row['supplier_id']= $p->user_id;
            $row['profile_image']= ($p->profile_image!='')?$p->profile_image:'user_default.png';

            if($rent_type==36){
                // echo "here we go! 36";
                if($airport!=''){
                    if($airport=='pickup'){
                        $textAirport = getLang('PRICE_FOR_PICKUP_TEXT');//"Pickup";
                        $bookingPrice = $p->airport_pickup_rate;
                    }else{
                        $textAirport = getLang('PRICE_FOR_DROPOFF_TEXT');
                        $bookingPrice = $p->airport_pickoff_rate;
                    }
                    $PriceText = '<br/><span class="currency-box">'.$textAirport.'</span>';
                    $row['price_default_inclusive'] = getNumFormat($bookingPrice).$PriceText;
                    ksort($termsOptions[4386]);
                    $termsOptions[4386] = array_values($termsOptions[4386]);
                    $row['premium'] = $termsOptions[4386];
                    $row['basic'] = array();
                    $row['discount_on_car'] = false;
                }else{
                    //Set Prices
                    $price_default_basic = $this->calculatePrice($p->id,'',$attributesbyId,TRUE);
                    $price_default_inclusive = $this->calculatePrice($p->id,'inc',$attributesbyId,TRUE);
                    if($noOfDays>1){
                        $PriceText = '<br/><span class="currency-box">'.getLang('CAR_TEXT_PRICE_FOR'). $noOfDays .getLang('FILTER_TEXT_DAYS').'</span>';
                    }else{
                        $minutes = $this->getDateDiffPickDrop($postData);
                        if($minutes<=360){
                             $PriceText = '<br/><span class="currency-box">'.getLang('CAR_TEXT_PRICE_FOR').getLang('CAR_TEXT_HALF_DAY').'</span>';
                        }else{
                             $PriceText = '<br/><span class="currency-box">'.getLang('FILTER_TEXT_PRICE_FOR_A_DAY').'</span>';
                        }

                    }
                    $row['price_default_basic'] = getNumFormat($price_default_basic).$PriceText;
                    $row['price_default_inclusive'] = getNumFormat($price_default_inclusive).$PriceText;
                    if($isHighway){
                        ksort($termsOptions[15]);
                        ksort($termsOptions[186]);
                        $termsOptions[15] = array_values($termsOptions[15]);
                        $termsOptions[186] = array_values($termsOptions[186]);
                        $row['premium'] = $termsOptions[15];
                        $row['basic'] = $termsOptions[186];
                        $row['discount_on_car'] = ($p->highway_discount>0)?$p->highway_discount:false;
                    }else{
                        ksort($termsOptions[187]);
                        ksort($termsOptions[188]);
                        $termsOptions[187] = array_values($termsOptions[187]);
                        $termsOptions[188] = array_values($termsOptions[188]);
                        $row['premium'] = $termsOptions[187];
                        $row['basic'] = $termsOptions[188];
                        $row['discount_on_car'] = false;
                    }
                }
            }else if($rent_type==37){
                // echo "here we go! 37";
                $noOfWeeks = $postData['no_of_weeks'];
                if($noOfWeeks>1)
                    $PriceText = '<br/><span class="currency-box">'.getLang('FILTER_TEXT_PRICE').$noOfWeeks.getLang('FILTER_TEXT_WEEKS').' </span>';
                else
                    $PriceText = '<br/><span class="currency-box">'.getLang('FILTER_TEXT_PRICE_FOR_A_WEEK').'</span>';
                $row['premium'] = array();
                ksort($termsOptions[4379]);
                $termsOptions[4379] = array_values($termsOptions[4379]);
                $row['basic'] = $termsOptions[4379];
                //Set Prices
                $row['price_default_basic'] = getNumFormat(($noOfWeeks*$p->weekely_rate)).$PriceText;
            }else if($rent_type==38){
                $no_of_month = $postData['no_of_month'];
                if($no_of_month>1)
                    $PriceText = '<br/><span class="currency-box">'.getLang('FILTER_TEXT_PRICE').$no_of_month.getLang('FILTER_TEXT_MONTHS').'</span>';
                else
                    $PriceText = '<br/><span class="currency-box">'.getLang('FILTER_TEXT_PRICE_FOR_A_MONTH').'</span>';
                $row['premium'] = array();
                //Set Prices
                $row['price_default_basic'] = getNumFormat(($no_of_month*$p->monthly_rate)).$PriceText;
                ksort($termsOptions[4378]);
                $termsOptions[4378] = array_values($termsOptions[4378]);
                $row['basic'] = $termsOptions[4378];
            }
            //$row['packages'] = $packages;

            $prices['basic_p'] = getNumFormat($p->city_rate_b);
            $prices['all_inc_p'] = getNumFormat($p->city_rate_ai);
            //$row['prices'] = $prices;
            $row['rating'] = $p->ratings;
            $row['num_of_reviews'] = $this->ratings->num_of_reviews($p->id);
            $pdlis[] = $row;
        }

        // usort($pdlis, function($a,$b) {
        //     return $a['supplier_id'] <=> $b['supplier_id'];
        // });
        // print "<pre/>";
        // print_r($pdlis);
        // exit();
        $recordsFiltered = $prodList = $this->car_manager->get_datatables('nopage');//$this->car_manager->count_filtered(FALSE);
        $recordsFiltered = count($recordsFiltered);
        //print_r($recordsFiltered);die;
        $pages = ($recordsFiltered>10)?(ceil($recordsFiltered/10)):1;
        $cpage = $_POST['cpage'];
        $output = array(
                            "draw" => (isset($_POST['draw'])?$_POST['draw']:1),
                            "recordsTotal" => $recordsFiltered,
                            "recordsFiltered" => $pages,
                            "cpage" => $cpage,
                            "list" => $pdlis
                );
        //print_r($pdlis);die;
        //echo $this->load->view('Car_manager/front/ajax_car', TRUE);
        echo json_encode($output);die;
    }

    public function get_destination($city_id=''){
        if($city_id==''){
            $city = $this->input->post('city');
        }else{
            $city = $city_id;
        }
        $destinations = $this->car_manager->getDestinations($city);
        $i = 0;
        $destinationsData = array();
        $activeLanguage = $this->session->userdata('active_language');
        foreach ($destinations as $k=>$val){
            $destinationsData[$i]['id'] = $val->id;
            if($activeLanguage=='mk'){
                $destinationsData[$i]['title'] = ($val->title_mya!='')?$val->title_mya:$val->title;
            }else{
                $destinationsData[$i]['title'] = $val->title;
            }
            $destinationsData[$i]['short_desc'] = $val->short_desc;
        $i++;
        }
        $data = $this->session->userdata('search_data_36');
       if($city_id==''){
            echo json_encode($destinationsData);
       }else{

            $data = $destinationsData;
            return $data;
       }
    }
    public function set_multi_destination($data=array()){
        // print_r($_POST);
        if(count($data)>1){
            $from_city = $this->session->userdata('car_city');
            $to_destination = $data['destination'];
            $city_dest_ids = array($from_city,$to_destination);
            $index = date('Y-m-d', strtotime($data['pickup_date']));
            $pickup_date = date('Y-m-d', strtotime($data['pickup_date']));
            $distance = $this->getDistanceByCityDest($from_city,$to_destination);
            $distance = ($distance*2);
            $dataInfo[$index]['distance'] = $distance;
            $dataInfo[$index]['from'] = $from_city;
            $dataInfo[$index]['to'] = $to_destination;
            $titles = $this->car_manager->get_attributes($city_dest_ids,'title');
            //print_r($titles);die;
            $dataInfo['titles'] = $titles;
            $dataInfo['total_distance'] = $distance;
            // $this->session->set_userdata('datewise','');
            // $this->session->set_userdata('datewiseDestinations',$dataInfo);
            return $dataInfo;
        }else{
            $from_city = $_POST['from'];
            $to_destination = $_POST['to'];
            $city_dest_ids = array_merge($from_city,$to_destination);
            $city_dest_ids = array_values(array_unique($city_dest_ids));
            $index = date('Y-m-d', strtotime($_POST['pickup_date']));
            $pickup_date = date('Y-m-d', strtotime($_POST['pickup_date']));//$_POST['pickup_date'];
            $i=0;
            $distances = 0;
            foreach ($from_city as $k=>$city){
                if($i==0){
                    $index = date('Y-m-d', strtotime($pickup_date));
                }else{
                   $index = date('Y-m-d', strtotime("+$i day", strtotime($pickup_date)));
                }
                //Set for each day from & To destination with their distance
                $distance = $this->getDistanceByCityDest($city,$to_destination[$k]);
                $data[$index]['distance'] = $distance;
                $data[$index]['from'] = $city;
                $data[$index]['to'] = $to_destination[$k];
                $i++;
                if(is_numeric($distance)){
                    $distances+=$distance;
                }else{
                    $distances +=0;
                }

            }
            $activeLanguage = $this->session->userdata('active_language');
            if($activeLanguage=='mk'){
                $titles = $this->car_manager->getMyaDestinations($city_dest_ids);
            }else{
                $titles = $this->car_manager->get_attributes($city_dest_ids,'title');
            }


        //     //print_r($titles);die;
            $data['titles'] = $titles;
            $data['total_distance'] = $distances;
            // print_r($data);
            $this->session->set_userdata('datewise','');
            $this->session->set_userdata('datewiseDestinations',$data);
            echo json_encode($data);
        }
    }
    //Apply each day hours selection for datewise.
    public function set_multidays(){

        $hoursFrom = $_POST['pickup_time']['from'];
        $hoursTo = $_POST['pickup_time']['to'];

        //print_r($hoursFrom);die;
        $index = date('Y-m-d', strtotime($_POST['pickup_date']));
        $pickup_date = date('Y-m-d', strtotime($_POST['pickup_date']));//$_POST['pickup_date'];
        $i=0;
        foreach ($hoursFrom as $k=>$fromHours){
            if($i==0){
                $index = date('Y-m-d', strtotime($pickup_date));
            }else{
               $index = date('Y-m-d', strtotime("+$i day", strtotime($pickup_date)));
            }
            $fromTime = $index." ".$fromHours;
            $toTime = $index." ".$hoursTo[$k];
            $strtoHours = strtotime($toTime) - strtotime($fromTime);
            $hours = (int)($strtoHours/3600);
            $minutes = (int)(($strtoHours%3600)/60);
            if($hours<0 || $minutes<0){
                if(date("Y-m-d",strtotime($fromTime))== date("Y-m-d")){
                    $fromHours = date("h:ia",strtotime(date("h:ia"))+3600);
                    $hoursTo[$k] = "11:30pm";
                    $strtoHours = strtotime($hoursTo[$k]) - strtotime($fromHours);
                    $hours = (int)($strtoHours/3600);
                    $minutes = (int)(($strtoHours%3600)/60);
                }else{
                    $hours = '10';
                    $minutes = "00";
                    $fromHours = "8:00am";
                    $hoursTo[$k] = "6:00pm";
                }

            }else{
                if($minutes==0)
                    $minutes = "00";
            }
            //Set for each day from & To time and duration of time and used date in indexing
            $data[$index]['duration'] = $hours.":".$minutes;
            $data[$index]['from'] = $fromHours;
            $data[$index]['to'] = $hoursTo[$k];
            $i++;
        }//die;
        $this->session->set_userdata('datewiseDestinations','');
        $this->session->set_userdata('datewise',$data);
        echo json_encode($data);
    }
    // Load popup for each day hours selection
    public function set_dates(){
        $pickup_date = (isset($_POST['pickup_date']) && $this->validateDate($_POST['pickup_date'])?$_POST['pickup_date']:date("m/d/Y"));
        $drop_date = (isset($_POST['drop_date']) && $this->validateDate($_POST['drop_date'])?$_POST['drop_date']:date("m/d/Y"));
        $datewise = $this->session->userdata('datewise');
        $diff = strtotime($drop_date) - strtotime($pickup_date);

        $days = (int)($diff/86400);
        $data = array();
        $dates = array();
        for($i=0;$i<=$days;$i++){
            if($i==0){
                $index = date('Y-m-d', strtotime($pickup_date));
            }else{
                $index = date('Y-m-d', strtotime("+$i day", strtotime($pickup_date)));
            }
            $dates[$index]['date'] = date("M d,Y",strtotime($index));
            if(is_array($datewise) && count($datewise)>0){
                $dates[$index]['from'] = ((isset($datewise[$index]['from']) && $this->validateDate($index))?$datewise[$index]['from']:'');
                $dates[$index]['to'] = ((isset($datewise[$index]['to']) && $this->validateDate($index))?$datewise[$index]['to']:'');
            }else{
                $dates[$index]['from'] = '';
                $dates[$index]['to'] = '';
            }
        }
        $data['dates'] = $dates;
        $data['counts'] = count($dates);
        echo json_encode($data);
    }
    //Get City id by Destination string.
    public function getCityByDestination($des = '',$jsonType=TRUE){
        if($des==''){
            $des = $_POST['dest_text'];
        }
        $activeLanguage = $this->session->userdata('active_language');
        if($activeLanguage=='mk'){
            $this->db->select(array('id','short_desc as title'));
            $this->db->from('cr_manage_attributes');
            $this->db->where(array('short_desc'=>$des,'parent_id'=>'70'));
        }else{
            $this->db->select(array('id','title'));
            $this->db->from('cr_manage_attributes');
            $this->db->where(array('title'=>$des,'parent_id'=>'70'));
        }
        $this->db->limit('1');
        $query = $this->db->get();
        $data = $query->row();
        if($jsonType){
            if(!empty($data)){
                echo json_encode($data);die;
            }else{
                //$data = array('id'=>'','title'=>'');
                echo '1';die;
            }
        }else{ return $data; }
    }
    //Manage City & destination on change destination in multi destination select popup
    public function set_destinationsOnChange(){
        $pickup_date = date('m/d/Y', strtotime($_POST['pickup_date']));
        $drop_date = $_POST['drop_date'];
        $city_id = $_POST['car_city_daily'];
        $city_text = $_POST['city_text'];
        $keep_city_id = $city_id;
        // $dest_id_selected = $_POST['dest_id'];
        $dest_id_selected = $this->session->userdata('datewiseDestinations');
        // print_r($dest_id_selected);
        $dest_text = $_POST['dest_text'];

        $step = $_POST['step']; //car remove this row

        $keep_city_text = $_POST['pickupCity'];
        //$datewise = $this->session->userdata('datewise');
        $destinations = $this->get_destination($keep_city_id);
        $diff = strtotime($drop_date) - strtotime($pickup_date);
        $days = (int)($diff/86400);
        //echo $days;die($drop_date."##".$pickup_date);
        $dates = array();
        for($i=0;$i<=$days;$i++){
            if($i==0){
                $index = date('Y-m-d', strtotime($pickup_date));


            }else{
                $destinations = $this->get_destination($city_id); //Get destinations by city ID.
                $index = date('Y-m-d', strtotime("+$i day", strtotime($pickup_date)));
            }
            if($i==($days)){
                $destinations = $this->get_destination($city_id);
                $Newdestinations = array();
                foreach ($destinations as $k=>$dest){
                    if($dest['title']==$keep_city_text){
                        $dest_id_selected = $k;
                        $Newdestinations[$k] = $dest;
                    }
                }$destinations = $Newdestinations;
                //json_encode($destinations);die;
            }else{
                $destinations = $this->get_destination($city_id);
            }
            $dates[$index]['date'] = date("M d,Y",strtotime($index));
            $dates[$index]['from'] = $city_id;
            $dates[$index]['from_title'] = $city_text;
            $dates[$index]['to'] = $destinations;
            $dates[$index]['to_selected'] = $dest_id_selected;
            // echo $dest_id_selected;
        }
        $data['dates'] = $dates;
        $data['counts'] = count($dates);
        echo json_encode($data);
    }
    //Load multi destination popup
    public function set_destinations(){

        $step = $_POST['step'];
        // echo $step;
        if($step>0){
            $this->set_destinationsOnChange();
        }else{
            // echo "here we go";
            $pickup_date = $_POST['pickup_date'];
            $drop_date = $_POST['drop_date'];
            $city_id = $_POST['car_city_daily'];
            $keep_city_id = $city_id;
            $dest_id_selected = $_POST['dest_id'];
            $dest_text = $_POST['dest_text'];
            $city_text = $_POST['city_text'];
            $step = $_POST['step'];
            $keep_city_text = $city_text;

            $datewiseDestinations = $this->session->userdata('datewiseDestinations');
            $destinations = $this->get_destination($keep_city_id);
            $diff = strtotime($drop_date) - strtotime($pickup_date);
            $days = (int)($diff/86400);

                // print_r($datewiseDestinations);
            // print_r($datewiseDestinations);
            // echo $days;
            // die($drop_date."##".$pickup_date);
            $dates = array();
            for($i=0;$i<=$days;$i++){

                if($i==0){
                    $index = date('Y-m-d', strtotime($pickup_date));
                }else if ($i==1){
                    if($dest_id_selected>0){
                        $dest_id_selected = '';
                        $index = date('Y-m-d', strtotime("+$i day", strtotime($pickup_date)));
                        $cityinfo = $this->getCityByDestination($dest_text,FALSE);//Get City info by Destination text.
                        //print_r($cityinfo);die;
                        $city_id = $cityinfo->id;
                        $city_text = $cityinfo->title;
                        $destinations = $this->get_destination($city_id); //Get destinations by city ID.
                    }
                }else{
                    $destinations = $this->get_destination($city_id); //Get destinations by city ID.
                    $index = date('Y-m-d', strtotime("+$i day", strtotime($pickup_date)));
                }
                if($i==($days)){
                    $destinations = $this->get_destination($city_id);

                    $Newdestinations = array();
                    foreach ($destinations as $k=>$dest){
                        if($dest['title']==$keep_city_text){
                            $dest_id_selected = $k;
                            $Newdestinations[$k] = $dest;
                        }
                    }$destinations = $Newdestinations;
                    //json_encode($destinations);die;
                }else{
                    $destinations = $this->get_destination($city_id);
                }
                // print_r($_POST);
                // else{
                //             $dest_id_selected = $k;
                //             echo $keep_city_text."<br/>";
                //         }
                // echo "-";
                // print_r($datewiseDestinations);
                // echo "\n----diff-".($days+1);
                // echo "\n";
                // echo count($datewiseDestinations)-2;



                $dates[$index]['date'] = date("M d,Y",strtotime($index));
                $dates[$index]['from'] = $city_id;
                $dates[$index]['from_title'] = $city_text;
                $dates[$index]['to'] = $destinations;
                $dates[$index]['to_selected'] = $dest_id_selected;
                // print_r($_POST);
                if(count($datewiseDestinations)>0 && !empty($datewiseDestinations)){
                    // unset($datewiseDestinations['titles']);
                    // unset($datewiseDestinations['total_distance']);

                    if(($days+1)==(count($datewiseDestinations)-2)){
                        foreach ($datewiseDestinations as $index => $value) {
                            if (isset($value['from'])) {
                                $dates[$index]['date'] = date("M d,Y",strtotime($index));
                                $dates[$index]['from'] = $value['from'];
                                $dates[$index]['from_title'] = $this->attributes->get_by('id',$value['from'])->short_desc;
                                $dest = $this->get_destination($value['from']);
                                $dates[$index]['to'] = $dest;
                                $dates[$index]['to_selected'] = $value['to'];
                                // echo $value['to']."\n";
                            }
                        }
                    }
                    // else if(($days)<(count($datewiseDestinations))){


                    //     $newArray = array_keys($datewiseDestinations);
                    //     // unset($newArray[count($newArray)-1]);
                    //     // unset($newArray[count($newArray)-2]);
                    //     // $sub=(count($newArray)-2)-($days+1);
                    //         // echo "Hi";
                    //     // echo "da - ".($days+1)."\n";
                    //     // echo "na - ".(count($newArray)-2)."\n";
                    //     // echo $sub;
                    //     // echo count($newArray);

                    //     // print_r($newArray);

                    //     for ($na=0; $na < ($days); $na++) {
                    //         # code...
                    //         // if (isset($datewiseDestinations[$newArray[$na]]['from'])) {
                    //             // echo $datewiseDestinations[$newArray[$na]]['from'];;
                    //             $dates[$newArray[$na]]['date'] = date("M d,Y",strtotime($newArray[$na]));
                    //             $dates[$newArray[$na]]['from'] = $datewiseDestinations[$newArray[$na]]['from'];
                    //             $dates[$newArray[$na]]['from_title'] = $this->attributes->get_by('id',$datewiseDestinations[$newArray[$na]]['from'])->short_desc;
                    //             $dest = $this->get_destination($datewiseDestinations[$newArray[$na]]['from']);
                    //             $dates[$newArray[$na]]['to'] = $dest;
                    //             $dates[$newArray[$na]]['to_selected'] = $datewiseDestinations[$newArray[$na]]['to'];
                    //             // echo $na.' - '.$newArray[$na]."\n";
                    //             // echo $datewiseDestinations[$newArray[$na]]['to']."\n";
                    //         // }
                    //     }

                    //     // $dates[$newArray[count($newArray)-1]]['date'] = date("M d,Y",strtotime($drop_date));
                    //     // $dates[$newArray[count($newArray)-1]]['from'] = $datewiseDestinations[$newArray[count($newArray)-1]]['from'];
                    //     // $dates[$newArray[count($newArray)-1]]['from_title'] = $this->attributes->get_by('id',$datewiseDestinations[$newArray[count($newArray)-1]]['from'])->short_desc;
                    //     // $dest = $this->get_destination($datewiseDestinations[$newArray[count($newArray)-1]]['from']);
                    //     // $dates[$newArray[count($newArray)-1]]['to'] = $dest;
                    //     // $dates[$newArray[count($newArray)-1]]['to_selected'] = $datewiseDestinations[$newArray[count($newArray)-1]]['to'];
                    //     // echo "hi";
                    //     // $sub=($days+1)-(count($datewiseDestinations)-2);
                    //     // $dates_av=array_values($datewiseDestinations);

                    //     // // print_r($dates);
                    //     // // echo $sub;
                    //     // // print_r($newArray);
                    //     // for ($i2=0; $i2 <= $sub; $i2++) {
                    //     //     // echo "hi";
                    //     //     // echo $newArray[$i2];
                    //     //     // print_r($datewiseDestinations[$i2]);
                    //     // }
                    // }
                    // }
                    // $this->session->set_userdata('datewiseDestinations','');
                }
               // print_r($datewiseDestinations);

            }
            $data['dates'] = $dates;
            $data['counts'] = count($dates);
            echo json_encode($data);
        }
    }
    private function multiDatesWithInCityFilter($key){
        $multiDatesWithCity = $this->session->userdata('datewise');
        if(is_array($multiDatesWithCity) && count($multiDatesWithCity)>0){
            foreach ($multiDatesWithCity as $date=>$info){
                switch ($key){
                    case 'duration':
                        $dates[$date] = $info['duration'];
                        break;
                    case 'from':
                        $dates[$date] = $info['from'];
                        break;
                    case 'to':
                        $dates[$date] = $info['to'];
                        break;
                    default:
                       $dates[$date] = $info;
                }
            }
            return $dates;
        }else{
            return FALSE;
        }
    }

    private function dailyFullDayPriceCal($calculationData){
        extract($calculationData);
        extract($pricingRules);
        //print_r($calculationData);die;
        $extraHrs = ($hours[0]>10)?($hours[0]-10):0;
        $minuts = (($extraHrs*60)+$hours[1]);
        $extraHrs = ceil(($minuts/60));
        $is_halfDay = FALSE;
        if($hours[0]<=6 || ($hours[0]==6 && $hours[1]>0)){
            $full_halfDayText = getLang("CAR_TEXT_HALF_DAY");
        }else{
            $full_halfDayText = getLang('CAR_TEXT_FULL_DAY');
        }
        if($inclusive){
            $carPrice = $carinfo->city_rate_ai;
            $packageType = getLang('CAR_TEXT_INCLUSIVE');
        }else{
            $carPrice = $carinfo->city_rate_b;
            $packageType = getLang('CAR_TEXT_BASIC');
        }
        if($incity=='highway'){
            $trackStep = array(
                            array('title'=>$full_halfDayText.$packageType,'amount'=>getNumFormat(($carPrice))),
                            array('title'=>getLang('CAR_TEXT_DESTINATION'),'amount'=>getNumFormat(($highwayDistance*$FuelPrice))),
                            array('title'=>getLang('CAR_TEXT_HIGHWAY_PERCENTAGE'),
                                    'amount'=>getNumFormat((($carPrice * $carinfo->hightway_rate_b)/100))),
                            );
            $bookingPrice = (($carPrice)+($highwayDistance*$FuelPrice)+((($carPrice * $carinfo->hightway_rate_b)/100)));
            if($extraHrs>0){
                //$trackStep[] =array('title'=>"Overtime cost ",'amount'=>(($carPrice * 30)/100));
                $trackStep[] =array('title'=>getLang('CAR_TEXT_ADDITIONAL_HOURS_CHARGES'),'amount'=>getNumFormat(((($carPrice * 10)/100)*$extraHrs)));
                //$extraCharge = ((($carPrice * 30)/100)+((($carPrice * 10)/100)*$extraHrs));
                $extraCharge =((($carPrice * 10)/100)*$extraHrs);
                $bookingPrice+= $extraCharge;
            }
            $trackPrices = $trackStep;
        }else{
            $trackStep = array(array('title'=>$full_halfDayText.$packageType,'amount'=>getNumFormat($carPrice)));
            $bookingPrice = $carPrice;
            if($extraHrs>0){
                //$trackStep[] =array('title'=>"Overtime cost ",'amount'=>(($carPrice * 30)/100));
                $trackStep[] =array('title'=>getLang('CAR_TEXT_ADDITIONAL_HOURS_CHARGES'),'amount'=>getNumFormat(((($carPrice * 10)/100)*$extraHrs)));

                //$extraCharge = ((($carPrice * 30)/100)+((($carPrice * 10)/100)*$extraHrs));
                $extraCharge = (((($carPrice * 10)/100)*$extraHrs));
                $bookingPrice+= $extraCharge;
            }
            $trackPrices = $trackStep;
        }

        $data['bookingPrice'] = $bookingPrice;
        $data['trackPrices'] = $trackPrices;
        return $data;
    }
    private function dailyHalfDayPriceCal($calculationData){
        extract($calculationData);
        if($inclusive){
            $halfDayRate = $carinfo->city_hourly_rate_ai;
            $packageType = getLang('CAR_TEXT_INCLUSIVE');
        }else{
            $halfDayRate = $carinfo->city_hourly_rate_b;
            $packageType = getLang('CAR_TEXT_BASIC');
        }
        if($incity=='highway'){
            $trackPrices = array(
                            array('title'=>getLang('CAR_TEXT_HALF_DAY').$packageType,'amount'=>getNumFormat($halfDayRate)),
                            array('title'=>getLang('CAR_TEXT_DESTINATION')." $highwayDistance*$FuelPrice",'amount'=>getNumFormat(($highwayDistance*$FuelPrice))),
                            array('title'=>getLang('CAR_TEXT_HIGHWAY_PERCENTAGE')." ($halfDayRate*$carinfo->hightway_rate_b)/100",
                                    'amount'=>(($halfDayRate * $carinfo->hightway_rate_b)/100)),
                            );
            $bookingPrice = (($halfDayRate)+($highwayDistance*$FuelPrice)+((($halfDayRate * $carinfo->hightway_rate_b)/100)));
        }else{
            $trackPrices = array(array('title'=>getLang('CAR_TEXT_HALF_DAY').$packageType,'amount'=>getNumFormat($halfDayRate)));
            $bookingPrice = ($halfDayRate);
        }

        $data['bookingPrice'] = $bookingPrice;
        $data['trackPrices'] = $trackPrices;
        return $data;
    }
    private function manageDiscountTax($pricingRules, $newData){
        //print_r($newData);die;
        extract($newData);
        $booking_price = array_sum($booking_price);

        $DiscountPercentage = $pricingRules['DiscountPercentage'];
        $DiscountedAmount = (($booking_price*$DiscountPercentage)/100);
        //echo $DiscountedAmount;die;
        if($DiscountedAmount>0){
            $booking_price -=$DiscountedAmount;
            $tracking[]= array('title'=>getLang('CAR_TEXT_DISCOUNT')." $DiscountPercentage%",'amount'=>getNumFormat($DiscountedAmount));
            $trackPrices = $tracking;
        }
        $TaxPercentage = $pricingRules['TaxPercentage'];
        $tax = (($booking_price*$TaxPercentage)/100);
        //if($tax>0){
            $booking_price +=$tax;
            $tracking[]= array('title'=>getLang('CAR_TEXT_TAX')." $TaxPercentage%",'amount'=>getNumFormat($tax));
            $tracking[]= array('title'=>getLang('CAR_TEXT_TOTAL_AMOUNT'),'amount'=>getNumFormat(($booking_price)));
            $trackPrices = $tracking;
        //}
        $data['bookingPrice'] = $booking_price;
        $data['trackPrices'] = $trackPrices;
        return $data;
    }
    private function manageInCityCal($calculationData){
        extract($calculationData);
        $newData = array();
        foreach ($dates as $k=>$date){
            $bookingPrice = 0;
            $hours = explode(":",$date);
            $calculationData['hours'] = $hours;
            $minuts = (($hours[0]*60)+$hours[1]);
            //If time more than 5 hours then rate will apply full time.
            if($minuts<=300){
                $calculated = $this->dailyHalfDayPriceCal($calculationData);
                extract($calculated);
            }else{
                $calculated = $this->dailyFullDayPriceCal($calculationData);
                extract($calculated);
            }
            $newData['booking_price'][] = $bookingPrice;
            $newData['tracking'][] = $trackPrices;
        }
        return $newData;
    }
    private function manageHighwayCal($calculationData){
         // print_r($calculationData);die;

        extract($calculationData);
        $datewiseDestinations = $this->session->userdata('datewiseDestinations');
       //print_r($datewiseDestinations);

        if(is_array($datewiseDestinations) && count($datewiseDestinations)>2){
            $titles = $datewiseDestinations['titles'];
            unset($datewiseDestinations['titles']);

            $total_distance = $datewiseDestinations['total_distance'];
            unset($datewiseDestinations['total_distance']);
            $dayOfTrip = count($datewiseDestinations);
            $dayOfTripInText = $dayOfTrip." Days";
        }else{
            $total_distance = isset($highwayDistance)?$highwayDistance:0;
            $dayOfTrip = 1;
            $dayOfTripInText = $dayOfTrip." Day";
            //echo $total_distance;die("total_distance");
        }
        $highwayPercentage = $pricingRules['highwayPercentage'];
        $FuelPrice = $pricingRules['FuelPrice'];
        $tollPrice = $pricingRules['tollPrice'];
        $DailyMiles = $pricingRules['DailyMiles'];
        $driverMealsCharge = $pricingRules['driverMealsCharge'];
        $accommodationCharge = $pricingRules['accommodationCharge'];
        //echo "AddPercentage: (($total_distance)/($dayOfTrip*$highwayPercentage))<br>";
        $AdditionalPercentage = (($total_distance)/($dayOfTrip*$highwayPercentage));
        $fuelr=json_decode($carinfo->fuel_rate,true);
        $hwr=json_decode($carinfo->hightway_rate,true);
        $en=json_decode($carinfo->extra_night,true);
        $carPrice = "";
        if($inclusive){
        	//$driverMealsCharge; $accommodationCharge
            if(0<$total_distance && $total_distance<=75){
                $carPrice = $hwr[0];
            }elseif(76<=$total_distance && $total_distance<=200){
                $carPrice = $hwr[1];
            }elseif(201<=$total_distance && $total_distance<=500){
                $carPrice = $hwr[2];
            }elseif(501<=$total_distance){
                $carPrice = $hwr[3];
            }
            //echo "AddPerPrice : (($carPrice*$AdditionalPercentage)/100)<br>";
            $AdditionalPercentagePrice = (($carPrice*$AdditionalPercentage)/100);
            $packageType = getLang('CAR_TEXT_INCLUSIVE');
            $data111 = array($carPrice,$dayOfTrip,$total_distance,$FuelPrice,$tollPrice,$driverMealsCharge,$accommodationCharge);

            //(Highway Basic Rate X Days)+(( Total Miles of the trip X Fuel On Car Type)+Toll )+ ((D{river Meals+Accommodation) X Days)

            if(0<$total_distance && $total_distance<=75){
                $fpr = $fuelr[0];
            }elseif(76<=$total_distance && $total_distance<=200){
                $fpr = $fuelr[1];
            }elseif(201<=$total_distance && $total_distance<=500){
                $fpr = $fuelr[2];
            }elseif(501<=$total_distance){
                $fpr = $fuelr[3];
            }

            $price = ($carPrice*$dayOfTrip)+($total_distance*$fpr)+($driverMealsCharge*$dayOfTrip);

            if($dayOfTrip==2){
            	$price = $carPrice+($total_distance*$fpr)+(($driverMealsCharge*$dayOfTrip)+$accommodationCharge);

            	// echo $carPrice."\n";
            	// echo $driverMealsCharge*$dayOfTrip."\n";
            	// echo $accommodationCharge."\n";
            	// echo $FuelPrice."\n";
            	// echo $FuelPrice."\n";
            	// echo $total_distance*$FuelPrice."\n";
                if(0<$total_distance && $total_distance<=75){
                    $price = $price+$en[0];
                }elseif(76<=$total_distance && $total_distance<=200){
                    $price = $price+$en[1];
                }elseif(201<=$total_distance && $total_distance<=500){
                    $price = $price+$en[2];
                }elseif(501<=$total_distance){
                    $price = $price+$en[3];
                }

            	// echo "_______________\n";
            }
            if($dayOfTrip>2){
                $totalday=$dayOfTrip-1;
                // echo ($driverMealsCharge+$accommodationCharge)*$dayOfTrip;
                if(0<$total_distance && $total_distance<=75){
                    $price = ($carPrice)+($en[0]*$totalday)+((($total_distance*$fpr))+(($driverMealsCharge*$dayOfTrip)+($accommodationCharge*$totalday)));
                }elseif(76<=$total_distance && $total_distance<=200){
                    $price = ($carPrice)+($en[1]*$totalday)+((($total_distance*$fpr))+(($driverMealsCharge*$dayOfTrip)+($accommodationCharge*$totalday)));
                }elseif(201<=$total_distance && $total_distance<=500){
                    $price = ($carPrice)+($en[2]*$totalday)+((($total_distance*$fpr))+(($driverMealsCharge*$dayOfTrip)+($accommodationCharge*$totalday)));
                }elseif(501<=$total_distance){
                    $price = ($carPrice)+($en[3]*$totalday)+((($total_distance*$fpr))+(($driverMealsCharge*$dayOfTrip)+($accommodationCharge*$totalday)));
                }
            }



            //echo $price;die('inclusive');
            //echo "Price : ((($total_distance+($dayOfTrip*$DailyMiles))*$FuelPrice)+(($carPrice+$AdditionalPercentagePrice)*$dayOfTrip))";die;
            //$price = ((($total_distance+($dayOfTrip*$DailyMiles))*$FuelPrice)+(($carPrice+$AdditionalPercentagePrice)*$dayOfTrip));
            //$price = ($carPrice*$dayOfTrip)+((($dayOfTrip*$DailyMiles)*$FuelPrice)+);
        }else{
            $carPrice = "$carinfo->hightway_rate_b";
            /*
            $AdditionalPercentagePrice = (($carPrice*$AdditionalPercentage)/100);


            $price = (($carPrice+$AdditionalPercentagePrice)*$dayOfTrip);
            $dataa = array($total_distance,$dayOfTrip,$highwayPercentage,$AdditionalPercentage,$AdditionalPercentagePrice,$price);
            */
            $packageType = getLang('CAR_TEXT_BASIC');

            if(0<$total_distance && $total_distance<=75){
                $carPrice = $hwr[0];
            }elseif(76<=$total_distance && $total_distance<=200){
                $carPrice = $hwr[1];
            }elseif(201<=$total_distance && $total_distance<=500){
                $carPrice = $hwr[2];
            }elseif(501<=$total_distance){
                $carPrice = $hwr[3];
            }
            //echo "AddPerPrice : (($carPrice*$AdditionalPercentage)/100)<br>";
            $AdditionalPercentagePrice = (($carPrice*$AdditionalPercentage)/100);
            $packageType = getLang('CAR_TEXT_INCLUSIVE');
            $data111 = array($carPrice,$dayOfTrip,$total_distance,$FuelPrice,$tollPrice,$driverMealsCharge,$accommodationCharge);

            //(Highway Basic Rate X Days)+(( Total Miles of the trip X Fuel On Car Type)+Toll )+ ((D{river Meals+Accommodation) X Days)
            $price = ($carPrice*$dayOfTrip);
            // $price = (($carPrice*$dayOfTrip)+(($total_distance*$FuelPrice)+$tollPrice)+(($driverMealsCharge+$accommodationCharge)*$dayOfTrip));

            if($dayOfTrip==2){

                if(0<$total_distance && $total_distance<=75){
                    $price = $price-$en[0];
                }elseif(76<=$total_distance && $total_distance<=200){
                    $price = $price-$en[1];
                }elseif(201<=$total_distance && $total_distance<=500){
                    $price = $price-$en[2];
                }elseif(501<=$total_distance){
                    $price = $price-$en[3];
                }
            }
            if($dayOfTrip>2){
                $totalday=$dayOfTrip-2;
                if(0<$total_distance && $total_distance<=75){
                    $price = ($carPrice*2)+($en[0]*$totalday);
                }elseif(76<=$total_distance && $total_distance<=200){
                    $price = ($carPrice*2)+($en[1]*$totalday);
                }elseif(201<=$total_distance && $total_distance<=500){
                    $price = ($carPrice*2)+($en[2]*$totalday);
                }elseif(501<=$total_distance){
                    $price = ($carPrice*2)+($en[3]*$totalday);
                }
            }

            //print_r($dataa);die;
            //echo $price;die('Basic');
        }
        $trackPrices = array(array('title'=>getLang('CAR_FILTER_HIGHWAY')." $dayOfTripInText ".$packageType,'amount'=>getNumFormat($price)));
        $bookingPrice[] = $price;
        $data['booking_price'] = $bookingPrice;
        $data['tracking'][] = $trackPrices;
        //print_r($data);die;
        return $data;
    }
    private function validateDate($date){
        if (false === strtotime($date)) {
            return false;
        }else{
            return TRUE;
        }
//        echo strtotime($date);
//        $d = DateTime::createFromFormat('Y-m-d', $date);print_r($d);die("fff");
//        return ($d && $d->format('Y-m-d') === $date);
    }
    private function manageSearchPost($postData,$rentType=36){
        $currentDateTime = date("Y-m-d H:i:s");
        //echo $currentDateTime;die;
        if(isset($postData['pickup_date']))
            $fromDate = strtotime(($postData['pickup_date']));
        else
            $fromDate = strtotime(date('m/d/Y'));
        if(isset($postData['dropoff_date']))
            $toDate = strtotime(($postData['dropoff_date']));
        else
            $toDate = $fromDate;
        if(($fromDate==$toDate) && $rentType!=36){ //Reset data for multidays with InCity & Highway if trip is for one day.
            $this->session->set_userdata('datewise','');
            $this->session->set_userdata('datewiseDestinations','');
        }
        switch ($rentType){
        case 36:
            $postData['airport'] = ((isset($postData['airport']) && $postData['airport']!='')?$postData['airport']:'');
            $postData['incity'] = ((isset($postData['incity']) && $postData['incity']!='')?$postData['incity']:'incity');
            $postData['pickup_date'] = ((isset($postData['pickup_date']) && $this->validateDate($postData['pickup_date']))?$postData['pickup_date']:date('m/d/Y'));
            $postData['dropoff_date'] = ((isset($postData['dropoff_date']) && $this->validateDate($postData['dropoff_date']))?$postData['dropoff_date']:date('m/d/Y'));
            $postData['pickup_time'] = ((isset($postData['pickup_time']) && $postData['pickup_time']!='')?$postData['pickup_time']:'8:00am');
            $postData['dropoff_time'] = ((isset($postData['dropoff_time']) && $postData['dropoff_time']!='')?$postData['dropoff_time']:'6:00pm');
            if(strtotime($postData['pickup_date'])>strtotime($postData['dropoff_date'])){
                $postData['dropoff_date'] = $postData['pickup_date'];
            }
            $pickupdate = $postData['pickup_date']." ".$postData['pickup_time'];
            $dropoff_date = $postData['pickup_date']." ".$postData['dropoff_time'];
            $strtoHours = strtotime($dropoff_date) - strtotime($pickupdate);
            $minutess = (int)($strtoHours/60);
            if($minutess<301){ //echo "$$".$minutess; die($minutess);
                $this->session->set_userdata('is_half_day','yes');
            }else{
                $this->session->set_userdata('is_half_day','');
            }
            if($postData['pickup_date']==$postData['dropoff_date']){
                if($postData['incity']=='highway'){
                    $this->set_multi_destination($postData);
                }else{
                    $this->session->set_userdata('datewiseDestinations','');
                }
                $hours = (int)($strtoHours/3600);
                $minutes = (int)(($strtoHours%3600)/60);
                if($hours<0 || $minutes<0){
                    $postData['pickup_time'] = "8:00am";
                    $postData['dropoff_time'] = "6:00pm";
                }
            }
            if(strtotime($currentDateTime)> strtotime($postData['pickup_date']." ".$postData['pickup_time'])){
                $postData['pickup_date'] = date('m/d/Y', strtotime($currentDateTime)+3600);
                $postData['pickup_time'] = date('h:ia', strtotime($currentDateTime)+3600);
            }
            if(strtotime($postData['pickup_date']." ".$postData['pickup_time'])>strtotime($postData['dropoff_date']." ".$postData['dropoff_time'])){
                $currentDateTime = (strtotime($postData['pickup_date']." ".$postData['pickup_time'])+36000);
                //echo date('m/d/Y', ($currentDateTime));
                //echo $postData['pickup_date']." ".$postData['pickup_time']; die;
                $postData['dropoff_date'] = date('m/d/Y', ($currentDateTime));
                $postData['dropoff_time'] = date('h:ia', ($currentDateTime));
            }
            //Set dates car is available or not
            $this->session->set_userdata('booking_from_date',date('Y-m-d H:i:s', strtotime($postData['pickup_date'].' '.$postData['pickup_time'])));
            $this->session->set_userdata('booking_to_date',date('Y-m-d H:i:s', strtotime($postData['dropoff_date'].' '.$postData['dropoff_time'])));
            //print_r($postData);die;
            break;
        case 37:
            $this->session->set_userdata('is_half_day','');
            $postData['pickup_date'] = ((isset($postData['pickup_date']) && $this->validateDate($postData['pickup_date']))?$postData['pickup_date']:date('m/d/Y'));
            $postData['pickup_time'] = ((isset($postData['pickup_time']) && $postData['pickup_time']!='')?$postData['pickup_time']:'8:00am');
            $postData['no_of_weeks'] = ((isset($postData['no_of_weeks']) && $postData['no_of_weeks']!='')?$postData['no_of_weeks']:'1');
            //Set dates car is available or not
            $this->session->set_userdata('booking_from_date',date('Y-m-d H:i:s', strtotime($postData['pickup_date'].' '.$postData['pickup_time'])));
            $no_of_weeks = $postData['no_of_weeks'];
            $this->session->set_userdata('booking_to_date',date('Y-m-d H:i:s', strtotime("+$no_of_weeks week", strtotime($postData['pickup_date'].' '.$postData['pickup_time']))));
            break;
        case 38:
            $this->session->set_userdata('is_half_day','');
            $postData['pickup_date'] = ((isset($postData['pickup_date']) && $this->validateDate($postData['pickup_date']))?$postData['pickup_date']:date('m/d/Y'));
            $postData['pickup_time'] = ((isset($postData['pickup_time']) && $postData['pickup_time']!='')?$postData['pickup_time']:'8:00am');
            $postData['no_of_month'] = ((isset($postData['no_of_month']) && $postData['no_of_month']!='')?$postData['no_of_month']:'1');
            $this->session->set_userdata('booking_from_date',date('Y-m-d H:i:s', strtotime($postData['pickup_date'].' '.$postData['pickup_time'])));
            $no_of_month = $postData['no_of_month'];
            $this->session->set_userdata('booking_to_date',date('Y-m-d H:i:s',strtotime("+$no_of_month month", strtotime($postData['pickup_date'].' '.$postData['pickup_time']))));
            break;
        }
        return $postData;

    }
    private function fuelPriceBasedOnCarType($car_id){
        $carinfo = $this->car_manager->get_by('id', $car_id);
        if(isset($carinfo->car_type) && $carinfo->car_type>0){
            return $this->car_manager->get_fuel_price($carinfo->car_type);
        }else{ return false; }
    }
    private function getPricingRules($car_id = FALSE,$hwd=null){
        $pricingRules=array();
        if($hwd!=null){
            $carinfo = $this->car_manager->get_by('id', $car_id);
            $fuelPrice = $this->fuelPriceBasedOnCarType($car_id);

            $hwr=json_decode($carinfo->hightway_rate,true);
            $fr=json_decode($carinfo->fuel_rate,true);
            $en=json_decode($carinfo->extra_night,true);
            if(0<$hwd && $hwd<=75){
                $pricingRules['FuelPrice']=$fr[0];
            }elseif(76<=$hwd && $hwd<=200){
                $pricingRules['FuelPrice']=$fr[1];
            }elseif(201<=$hwd && $hwd<=500){
                $pricingRules['FuelPrice']=$fr[2];
            }elseif(501<=$hwd){
                $pricingRules['FuelPrice']=$fr[3];
            }

            $pricingRules['tollPrice'] = $fuelPrice->toll_fee;
            // if(501<$hwd){
            // }
            $pricingRules['DailyMiles'] = (isset($this->data['attributesByid'][156]['short_desc'])?$this->data['attributesByid'][156]['short_desc']:0);
            $pricingRules['TaxPercentage'] = (isset($this->data['attributesByid'][157]['short_desc'])?$this->data['attributesByid'][157]['short_desc']:0);
            $pricingRules['DiscountPercentage'] = (isset($this->data['attributesByid'][158]['short_desc'])?$this->data['attributesByid'][158]['short_desc']:0);
            $pricingRules['highwayPercentage'] = (isset($this->data['attributesByid'][179]['short_desc'])?$this->data['attributesByid'][179]['short_desc']:0);
            $pricingRules['driverMealsCharge'] = (isset($this->data['attributesByid'][4384]['short_desc'])?$this->data['attributesByid'][4384]['short_desc']:0);
            $pricingRules['accommodationCharge'] = (isset($this->data['attributesByid'][4385]['short_desc'])?$this->data['attributesByid'][4385]['short_desc']:0);
        }else{
        //Set Fuel Price
            $fuelPrice = $this->fuelPriceBasedOnCarType($car_id);
            if(is_object($fuelPrice)){
                $pricingRules['FuelPrice'] = $fuelPrice->fuel_price;
                $pricingRules['tollPrice'] = $fuelPrice->toll_fee;
            }else{
                $pricingRules['FuelPrice'] = (isset($this->data['attributesByid'][92]['short_desc'])?$this->data['attributesByid'][92]['short_desc']:0);
                $pricingRules['tollPrice'] = 0;
            }
            $pricingRules['DailyMiles'] = (isset($this->data['attributesByid'][156]['short_desc'])?$this->data['attributesByid'][156]['short_desc']:0);
            $pricingRules['TaxPercentage'] = (isset($this->data['attributesByid'][157]['short_desc'])?$this->data['attributesByid'][157]['short_desc']:0);
            $pricingRules['DiscountPercentage'] = (isset($this->data['attributesByid'][158]['short_desc'])?$this->data['attributesByid'][158]['short_desc']:0);
            $pricingRules['highwayPercentage'] = (isset($this->data['attributesByid'][179]['short_desc'])?$this->data['attributesByid'][179]['short_desc']:0);
            $pricingRules['driverMealsCharge'] = (isset($this->data['attributesByid'][4384]['short_desc'])?$this->data['attributesByid'][4384]['short_desc']:0);
            $pricingRules['accommodationCharge'] = (isset($this->data['attributesByid'][4385]['short_desc'])?$this->data['attributesByid'][4385]['short_desc']:0);

        }
        return $pricingRules;
    }
    private function calculatePrice($car_id,$package='inc',$attributesbyId,$onlyBooking=false){

        $carinfo = $this->car_manager->get_by('id', $car_id);
        //print_r($carinfo);die;
        $calculationData['carinfo'] = $carinfo;
        $rent_type = $this->session->userdata('rent_type');
        $get_postData = $this->session->userdata('search_data_'.$rent_type);
        $postData = $this->manageSearchPost($get_postData,$rent_type);
        $incity = (isset($postData['incity']) && $postData['incity']!='')?$postData['incity']:'city';
        $airport = (isset($postData['airport']) && $postData['airport']!='')?$postData['airport']:'';
        if($package=='inc')
            $inclusive = TRUE;
        else
            $inclusive = FALSE;
        $pricingRules = $this->getPricingRules($car_id);

        if(!isset($postData['pickup_date'])){
            $postData['pickup_date'] = date("m/d/Y");
        }
        if(!isset($postData['dropoff_date'])){
            $postData['dropoff_date'] = date("m/d/Y");
        }
        if(!isset($postData['pickup_time'])){
            $postData['pickup_time'] = date("g:ia");
        }
        if(!isset($postData['dropoff_time'])){
            $postData['dropoff_time'] = date("g:ia");
        }
        $fromdate = date("Y-m-d h:i A",(strtotime($postData['pickup_date']." ".$postData['pickup_time'])));
        $todate = date("Y-m-d h:i A",(strtotime($postData['dropoff_date']." ".$postData['dropoff_time'])));

        $multiDatesWithCity = $this->session->userdata('datewise');
        $dates = array(); // No of Days
        if($incity=='in_city' && count($multiDatesWithCity)>1){
            $dates = $this->multiDatesWithInCityFilter('duration');
            $this->data['hoursperDay'] = $dates;
        }else{
            $this->data['hoursperDay'] = array();
            $dates = $this->getDatesDiff($fromdate, $todate);
        }
        if($incity=='highway'){
            $destination = ($postData['destination']);
            // echo "hi";
            $highwayDistance = (isset($attributesbyId[$destination]['short_desc'])?$attributesbyId[$destination]['short_desc']:1);

            //print_r($attributesbyId[$destination]);die;
            if(count($dates)>0){    //highway Distance divided in Trip days
                $noOfDays = count($dates);
                $highwayDistance = (($highwayDistance*2)/$noOfDays);//for round trip multiply by 2
                $pricingRules = $this->getPricingRules($car_id,$highwayDistance);
                // echo $highwayDistance."##".$noOfDays;die;
            }
            $calculationData['highwayDistance'] = ($highwayDistance>=1)?$highwayDistance:1;
            $datewiseDestinations = $this->session->userdata('datewiseDestinations');
            if(is_array($datewiseDestinations) && count($datewiseDestinations)>1){
                $this->data['datewiseDestinations'] = $datewiseDestinations;
            }else{
                $this->data['datewiseDestinations'] = array();
            }
        }
        //print_r($dates);die;
        //Manage INCITY CAL
        $calculationData['incity'] = $incity;
        $calculationData['inclusive'] = $inclusive;
        $calculationData['pricingRules'] = $pricingRules;
        $calculationData['dates'] = $dates;
        // asd
        // print_r($calculationData);
        // die;
        if($incity=='in_city'){
            $newData = $this->manageInCityCal($calculationData);
        }else{
            $newData = $this->manageHighwayCal($calculationData);
        }
        if($onlyBooking){
            if(isset($newData['booking_price']))
                return array_sum($newData['booking_price']);
            else
                return false;
        }else{
            return $newData;
        }
    }
    private function checkIsHighway($postData){
        if(isset($postData['incity']) && $postData['incity']=='highway'){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    private function getTermsOptions(){
        $features = $this->car_manager->get_attributes(array(15,186,187,188,4378,4379,4386),TRUE);

        foreach ($features as $k=>$val){
            foreach ($val as $key=>$title){
                $featuresTerms[$k][$key] = $title['title'];
            }
        }
        return $featuresTerms;
    }
    private function getDatesDiff($fromDate, $todate){
        $date[0] = $fromDate;
        $dateend = $todate;

        $diff = strtotime($dateend) - strtotime($date[0]);
        $days = (int)($diff/86400);
        $dates = array();
        //die($fromDate."#".$todate."##".$diff."##".$days);
        for($i=0; $i<=$days; $i++){
            $currentdate = $date[$i];
            $date[$i+1] = date('Y-m-d H:i:s', strtotime('+1 day', strtotime(date('Y-m-d', strtotime($date[$i])))));
            if($i==$days){
                $datediff = strtotime($dateend) - strtotime($currentdate);
            }else{
                $datediff = strtotime($date[$i+1]) - strtotime($currentdate);
            }
            $hours = (int)($datediff/3600);
            $minutes = (int)(($datediff%3600)/60);
            $seconds = (int)(($datediff%3600)%60);
            if($hours>24){
                $dates[date("Y-m-d",strtotime($date[$i]))] = "24:0:0";
                $dates[date("Y-m-d",strtotime($date[$i+1]))] = ($hours-24).":".$minutes.":".$seconds;
            }else{
                $dates[date("Y-m-d",strtotime($date[$i+1]))] = $hours.":".$minutes.":".$seconds;
            }
            //$dates[$i] = $hours.":".$minutes.":".$seconds;
        }
        //print_r($dates);die;
        return $dates;
    }
    private function getDistanceByCityDest($city_id,$dest_id){
        $destinations = $this->car_manager->get_attributes(array($city_id));
        if(isset($destinations[$dest_id])){
            return $destinations[$dest_id]['short_desc'];
        }else{
            return '0';
        }
    }
    private function getDateDiffPickDrop($postData,$type='minutes'){
        $fromTime = $postData['pickup_date']." ".$postData['pickup_time'];
        $toTime = $postData['dropoff_date']." ".$postData['dropoff_time'];
        $strtoHours = strtotime($toTime) - strtotime($fromTime);
        if($type=='minutes'){
            return $minutes = (int)(($strtoHours)/60);
        }else if($type=='hours'){
            return $hours = (int)($strtoHours/3600);
        }
    }

    public function get_cutomer_detail($cid){
        $this->is_ajax();
        $this->isAuthorized();
        $cdata=$this->user->get_by('id',$cid);
        $this->checkPermission('listuser');
        echo json_encode($cdata);
        // print "<pre/>";
        // print_r($cdata);

    }
}
