<div class="modal fade pickup-modal" id="multiLocations" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
                <div class="modal-header form-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <span class="search-icon">
                        <img src="<?php echo base_url("/public/front/images/car-icon.png"); ?>" alt="">
                    </span>
                    
                    <h3 class="modal-title"><?php echo getLang('CAR_PER_DAY_DESTINATION_HEADING')?></h3>
                </div>
                <div class="modal-body highway-multiple-box">
                    <form action="<?php echo site_url('set-multi-destinations'); ?>" method="post" id="frm_daily_multi_dest">
                        <input type="hidden" name="pickup_date" id="pickup_date_id" />
                        <div class="highway-multiple-form">
                        	<div class="highway-multiplw-left">
                            	<h5><?php echo getLang('CAR_PDDP_FROM');?></h5>
                            </div>
                        	<div class="highway-multiplw-right">
                            	<h5><?php echo getLang('CAR_PDDP_TO');?></h5>
                            </div>
                        </div>    
                        <div id="multi_locations"></div>
                        <div class="highway-multiple-form pickup-dropof-time">
                        	<div class="highway-multiplw-left">
                                <label><?php echo getLang('CAR_PDDP_PICKUP_TIME');?></label>
                                <input type="text" class="timepicker time-box" id="daily_pickup_time_pop" name="pickup_time" 
                                       placeholder="<?php echo getLang('CAR_PDDP_PICKUP_TIME');?>" required="required" />
                            </div>
                        	<div class="highway-multiplw-right">
                            	<label><?php echo getLang('CAR_PDDP_DROPOFF_TIME');?></label>
                                <input type="text" class="timepicker time-box" id="daily_dropoff_time_pop" name="pickup_time" 
                                       placeholder="<?php echo getLang('CAR_PDDP_DROPOFF_TIME');?>" required="required" />
                            </div>
                        </div>
                        
                     </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default applymultiDestination"><?php echo getLang('CAR_PER_DAY_DESTINATION_SUBMIT_BTN')?></button>
                </div>
        </div><!-- Modal Body -->
    </div>
</div>