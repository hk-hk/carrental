
<section class="car-listing-box common-padding">
<div class="container">
<div class="row">
    <?php  if($this->session->flashdata('typ') && $this->session->flashdata('msg')){ ?>
        <div class="alert alert-<?php echo $this->session->flashdata('typ');?>">
            <?php echo $this->session->flashdata('msg');?>
        </div>
    <?php } ?>
    <div class="col-sm-3 pull-right">
        <div class="aside-listing checkout-summery">
            <div id="CatagoryTabs" class="cost-details"> 
                <h2><?php echo getLang('BOOKING_SUMMARY_OF_CHARGES')?></h2>
                <div class="charges-summery">
                    <?php //echo "<pre>";print_r($trackPrices);echo "</pre>";?>
                    <ul>
                        <?php 
                        $nextDay = 0;
                        foreach ($trackPrices as $k=>$item){ 
                            if(!isset($item['title'])){ 
                                $ii=0;
                                $pickup_date = ($nextDay!=0)?(strtotime($searchData['pickup_date'])+$nextDay):strtotime($searchData['pickup_date']);
                                foreach ($item as $it){ 
                                    if($ii==0){ 
                                        $hoursText = '';
                                        if(count($hoursperDay)>0){
                                            $dateby = date("Y-m-d",$pickup_date);
                                            $hoursText = (isset($hoursperDay[$dateby])?"(".$hoursperDay[$dateby].' Hours)':'');
                                        }
                                        echo "<span class='date-heading'>".date("D,F d,Y",$pickup_date)." ".$hoursText."</span>"; $ii=1;
                                    }
                                    ?> 
                                    <li>
                                        <span class="text-left"><?php echo $it['title'];?></span>
                                        <span class="text-right"><?php echo $it['amount'];?></span>
                                    </li>
                                <?php } $nextDay = ($nextDay+86400);?>
                            <?php }else{  ?>
                                <li>
                                    <span class="text-left"><?php echo $item['title'];?></span>
                                    <span class="text-right"><?php echo $item['amount'];?></span>
                                </li>
                            <?php }
                            }
                            if($is_admin_sp){ ?>
                            <li class="total-by-admin">
                                <span class="text-left"><?php echo getLang('TOTAL_AMOUNT_BY_ADMIN');?></span>
                                <span class="text-right"><input type="number" step="0.1" class="input-summary" id="total_by_admin" /></span>
                            </li>
                            <?php } ?>
                    </ul>
                </div>
                <?php if($airport=='' || ($airport!='dropoff' && $airport!='')){?>
                <div class="charges-summery">
                    <h5><?php echo getLang('BOOKING_SUMMARY_PICKUP');?></h5>
                    <?php //print_r($attributesbyId);?>
                    <ul>
                        <li><?php echo date("D,F d,Y",strtotime($searchData['pickup_date']))?></li>
                        <li><?php echo $searchData['pickup_time'];?></li>
                        <li><?php echo $attributesbyId[$car_city]['title']?></li>
                    </ul>
                </div>
                <?php } ?>
                <?php if($airport=='dropoff'){?>
                <div class="charges-summery">
                    <h5>Drop off</h5>
                    <ul>
                        <li><?php echo date("D,F d,Y",strtotime($searchData['dropoff_date']))?></li>
                        <li><?php echo $searchData['dropoff_time'];?></li>
                        <li><?php echo $attributesbyId[$car_city]['title']?></li>
                    </ul>
                </div>
                <?php } ?>
                <?php if(isset($datewiseDestinations) && count($datewiseDestinations)>0 && $airport==''){  
                        $titles = $datewiseDestinations['titles']; 
                        unset($datewiseDestinations['titles']);
                        unset($datewiseDestinations['total_distance']);
                        ?>
                     <table style="margin:20px; width: 80%" class="each-day-hourly">
                <?php foreach ($datewiseDestinations as $date=>$val){?>
                    <tr class="date-heading">
                        <th colspan="3"><?php echo date("D,M d,Y",strtotime($date));?></th>
                    </tr>
                    <tr>
                        <th><?php echo getLang('BOOKING_INFO_FROM');?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_TO');?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_DISTANCE');?>:</th>
                    </tr>
                    <tr>
                        <td><?php echo (isset($titles[$val['from']])?$titles[$val['from']]:'none');?></td>
                        <td><?php echo (isset($titles[$val['to']])?$titles[$val['to']]:'none');?></td>
                        <td><?php echo $val['distance'];?></td>
                    </tr>
                <?php } ?>
                </table>
                <?php } ?>
            </div>
        </div>
    </div>

     <div class="col-sm-9">
        <div class="checkout-details">
            <div class="listing-box" id="lightgallery">
                <div class="car-image-box">
                    <ul class="img-area">
                        <?php  $image = $carinfo['images'][0];?>
                        <li>
                            <img class="img-itm" src="<?php echo base_url('uploads/car/thumb/350X250/'.$image)?>">
                        </li>
                    </ul>
                    
                </div>
                <div class="car-packages">
                     <div class="package-box">
                        <div class="basic-package-innr">
                           <h4><?php echo getLang('BOOKING_TEXT_FEATURES')?></h4>
                            <ul>
                                <?php foreach ($carinfo['features'] as $feature){ ?>
                                <li><?php echo $feature;?></li>
                                <?php } ?>
                            </ul>
                        </div>
                        
                        <div class="basic-package-innr">
                           <h4><?php echo $carinfo['package']?></h4>
                            <ul>
                                <?php foreach ($carinfo['pack_feature'] as $feature){ ?>
                                <li><?php echo $feature;?></li>
                                <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                <div class="package-box package-bottom-box">
                    <div class="car-logo">
                        <img src="<?php echo base_url('uploads/user/'.$carinfo['profile_image'])?>" title="<?php echo $carinfo['supplier']; ?>" alt="<?php echo $carinfo['supplier']; ?>" />
                    </div>
                    <?php //} ?>
                    <div class="retaing-box">
                        <label><?php echo getLang('BOOKING_TEXT_RATING');?> : </label>
                        <?php for($i=1;$i<=5;$i++){
                            if($i<=$carinfo['ratings']){ ?>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            <?php }else{ ?>
                                <i class="fa fa-star-o" aria-hidden="true"></i> 
                        <?php }
                        }?>
                    </div>
                </div>  
                <?php if($discount_on_car>0){ ?>
                    <div class="package-box package-bottom-box car-discount">
                        <label><?php echo $discount_on_car;?>%   </label><?php echo getLang('DISCOUNT_TEXT'); ?>
                    </div>
                <?php } ?>
                      
                </div>
            </div>
        </div>

            <?php if ($userInfo->id!=''): ?>
        <div class="your-info-box">
            <?php 
            if($this->active_currency=='mmk'){
              $logourls = 'mpu_logo.jpg';
            }else{
              $logourls = 'visa_mastercard.png';
            }
            $currentURL = urlencode(current_url());
            //print_r($userInfo);
            ?>
            <div style="padding-bottom: 30px;">
                
                <div class="form-group">
                    <h2 class="black section-title common-title pull-left"><?php echo getLang('BOOKING_TEXT_YOUR_INFORMATION');?></h2>
                </div>
                <?php
                    $user_role=false; 
                    if(isset($userInfo->user_role)){
                        $user_role=($userInfo->user_role=='admin' || $userInfo->user_role=='service_provider')? true : false;
                    }
                ?>
                <?php if($user_role): ?>
                    <div class="form-group">
                        <select class="selectpicker form-control" id="user_id" data-live-search="true">
                            <optgroup label="Agents">
                            <?php foreach ($agents as $key => $value): ?>
                                <option data-subtext="- Agents" value="<?php echo $value->id; ?>"><?php echo $value->user_fname.' '.$value->user_lname; ?></option>
                                
                            <?php endforeach ?>
                            </optgroup>
                            <optgroup label="Customer">
                            <?php foreach ($customers as $key => $value): ?>
                                <option data-subtext="- Customers" value="<?php echo $value->id; ?>"><?php echo $value->user_fname.' '.$value->user_lname; ?></option>
                                
                            <?php endforeach ?>
                            </optgroup>
                        </select>
                        <span><a href="<?php echo site_url("admin/adduser")?>"><?php echo getLang('BOOKING_TEXT_REGISTER')?></a> 
                        <?php echo getLang('BOOKING_TEXT_REGISTER1')?></span>
                    </div>

                <?php endif ?>
            </div>
            <?php if($userInfo->id==''){ ?>
            <span><a href="<?php echo site_url("login?req=").$currentURL?>"><?php echo getLang('BOOKING_TEXT_SIGN_IN');?></a>  & 
                    <a href="<?php echo site_url("register?req=".$currentURL)?>"><?php echo getLang('BOOKING_TEXT_REGISTER')?></a> 
                    <?php echo getLang('BOOKING_TEXT_REGISTER1')?></span>
            <?php } ?>
            
            <?php echo form_open('book_now');?>
                <?php if($user_role): ?>
                    <input type="hidden" name="order_for_user_id" value="" placeholder="">
                <?php endif ?>
                <div class="form-group">
                  <label class="name-filed"><?php echo getLang('CHECKOUT_FF_TITLE');?></label>
                    <div class="input-block">
                        <?php if (!$user_role): ?>
                            
                        <select class="input-fill2" name="user_fname">
                            <option value="Mr." <?= ($userInfo->user_fname=="Mr.")? "selected" : "" ?>><?php echo getLang('CHECKOUT_FF_TITLE_OPTION1');?></option>
                            <option value="Ms." <?= ($userInfo->user_fname=="Ms.")? "selected" : "" ?>><?php echo getLang('CHECKOUT_FF_TITLE_OPTION3');?></option>
                            <option value="Mrs." <?= ($userInfo->user_fname=="Mrs.")? "selected" : "" ?>><?php echo getLang('CHECKOUT_FF_TITLE_OPTION2');?></option>
                        </select>
                        <?php else: ?>
                            <input class="input-fill2" name="user_fname" type="text" placeholder="<?php echo getLang('CHECKOUT_FF_TITLE');?>" value="<?php echo (!$user_role)? $userInfo->user_fname: '' ?>" required="true" <?php echo (!$user_role)? '' : 'readonly' ?>>    
                        <?php endif ?>
                    </div>
                </div>

                <div class="form-group">
                  <label class="name-filed"><?php echo getLang('CHECKOUT_FF_FULL_NAME');?></label>
                    <div class="input-block">
                        <input class="input-fill2" name="user_lname" type="text" placeholder="<?php echo getLang('CHECKOUT_FF_FULL_NAME');?>" value="<?php echo (!$user_role)? $userInfo->user_lname: '' ?>" required="true" <?php echo (!$user_role)? '' : 'readonly' ?>>
                    </div>
                </div>

                <div class="form-group">
                  <label class="name-filed"><?php echo getLang('CHECKOUT_FF_EMAIL');?></label>
                    <div class="input-block">
                        <input class="input-fill2" name="user_email" type="email"  placeholder="<?php echo getLang('CHECKOUT_FF_EMAIL');?>" value="<?php echo (!$user_role)? $userInfo->user_email: '' ?>" required="true" <?php echo (!$user_role)? 'required' : 'readonly' ?>>
                        <?php if($is_admin_sp){ ?>
                        <input type="number" step="0.1" name="admin_amount" id="admin_amount" value="" style="display:none;"/>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                  <label class="name-filed"><?php echo getLang('CHECKOUT_FF_PHONE_NO');?>.</label>
                    <div class="input-block">
                        <input class="input-fill2" type="number" name="phone" placeholder="<?php echo getLang('CHECKOUT_FF_PHONE_NO');?>" required="true" value="<?php echo (!$user_role)? $userInfo->user_tel: '' ?>" <?php echo (!$user_role)? 'required' : 'readonly' ?>>
                    </div>
                </div>
                <div class="form-group payment-methox-box">
                    <div class="input-block">
                    	<input type="radio" name="payment" id="Cash" checked="checked" value="cash" />
                        <label for="Cash"><?php echo getLang('CHECKOUT_FF_PAYMENT_OPTION1');?></label>
                     </div> 

                    <?php if($active_currency=='usd'){ ?>
                     <div class="input-block">
                    	<input type="radio" name="payment" id="Paypal" value="paypal" />
                        <label for="Paypal" title="<?php echo getLang('CHECKOUT_FF_PAYMENT_OPTION2');?>"><img src="<?php echo base_url('public/front/images/paypal.png');?>" /></label>
                     </div>
                     <?php } ?> 
                     <div class="input-block">
                    	<input type="radio" name="payment" id="myanmar-bank" value="bank_transfer" />
                        <label for="myanmar-bank" title="<?php echo getLang('CHECKOUT_FF_PAYMENT_OPTION3');?>"><img class="plogo-image" src="<?php echo base_url('public/front/images/'.$logourls);?>" /></label>
                     </div> 
                </div>

                <div class="continue-btn">
                  <button class="more"><?php echo getLang('CHECKOUT_FF_BOOK_NOW_BTN');?></button>
                </div>
            <?php echo form_close(); ?>
           
        </div>
            <?php endif ?>
         <?php if ($userInfo->id==""): ?>
                <section class="login-form common-padding">
    <!-- <div class="container"> -->
        <div class="row">
            <div class="col-md-6">
                <div class="sign-in-box">
                    <?php
                    if ($this->session->flashdata('typ')):
                        switch ($this->session->flashdata('typ')) {
                            case 1:
                                $put = 'alert-success';
                                break;
                            case 2:
                                $put = 'alert-warning';
                                break;
                            case 3:
                                $put = 'alert-danger';
                                break;
                        }
                        ?>
                        <div class="alert <?php echo $put; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i></h4>
                        <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="login_info_block">
                        <h2 class="black section-title common-title"><span><?php echo getLang('LOGIN_HEADING'); ?></span></h2>
                        <form method="post" action="<?= site_url('login') ?>">
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('LOGIN_EMAIL'); ?></label>
                                <div class="input-block"> 
                                    <span class="input-icon"><i class="fa fa-envelope"></i></span>
                                    <input class="input-fill2" type="email" required="required" name="user_email" id="user_email" placeholder="<?php echo getLang('LOGIN_EMAIL'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="name-filed"><?php echo getLang('LOGIN_PASSWORD'); ?></label>
                                <div class="input-block"> <span class="input-icon"><i class="fa fa-lock"></i></span>
                                    <input class="input-fill2" type="password" required="required" name="user_pass" id="user_pass" placeholder="<?php echo getLang('LOGIN_PASSWORD'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="login_from" value="login_from">
                                <input type="submit" name="login" value="<?php echo getLang('LOGIN_TEXT_BTN'); ?>" class="more login-btn">
                                <div class="lost-password"><a href="<?php echo site_url(); ?>lost_password"><?php echo getLang('LOGIN_TEXT_FORGOT_PASSWORD'); ?></a></div>
                            </div>
                        </form>
                    </div>
                    

            </div>

            </div>
            <div class="col-md-6">
                <div class="sign-in-box">
                    
                    <h2 class="black section-title common-title"><span>New Customer?</span></h2>
                <br>
                <br>
                <br>
                <br>
                
                <?php if(true){?>
                    <div class="or-box"><span> </span></div>
                    <br>
                    <div class="form-group fb-custom-botton">
                        <fb:login-button size="large"
                          scope="public_profile,email"
                          onlogin="checkLoginState();">
                          Login with Facebook
                        </fb:login-button>
                    </div>
                    <br>
                    <?php } ?>
                    <div class="or-box"><span><?php echo getLang('LOGIN_TEXT_OR') ?></span></div>
                    <br>
                    <div class="form-group">
                        <a href="<?php echo site_url(); ?>register"><button type="button" class="more signup-btn"><?php echo getLang('LOGIN_TEXT_REGISTER'); ?></button></a>
                    </div>
                    <div class="lost-password"><a href="<?php echo site_url(); ?>register">Register as a new customer!</a></div>
                </div>
                </div>
        </div>
    <!-- </div>  -->
</section>

            <?php endif ?>
    </div>

    </div>
</div>
</section>
<style type="text/css">.plogo-image{width: 90px; height: 32px;}</style>
<script type="text/javascript">
    $('#user_id').on('change', function() {
      // alert( this.value );
        $.get('<?php echo site_url("get_customer_detail/") ?>'+this.value,function(data){
            console.log(data);
            var obj=$.parseJSON(data);
            $('input[name="order_for_user_id"]').val(obj.id);
            $('input[name="user_fname"]').val(obj.user_fname);
            $('input[name="user_lname"]').val(obj.user_lname);
            $('input[name="user_email"]').val(obj.user_email);
            $('input[name="phone"]').val(obj.user_tel);
        });
    })
</script>