<div class="modal fade pickup-modal" id="EditPickUp" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header form-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="search-icon">
              <img src="<?php echo base_url("/public/front/images/car-icon.png");?>" alt="">
          </span>
          <h3 class="modal-title"><?php echo getLang('CAR_PER_DAY_HOURS_HEADING')?></h3>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url('set-multidays'); ?>" method="post" id="frm_daily_hours">
            <div id="selecting-day"></div>
        </form>
      </div><!-- Modal Body -->
      <div class="modal-footer">
          <button type="submit" class="btn btn-default applyDays"><?php echo getLang('CAR_PER_DAY_HOURS_SUBMIT_BTN')?></button>
      </div>
    </div>
  </div>
</div>
