<?php
//print_r($searchData);die($rent_type);
if ($rent_type == 36) {
    $daily = $searchData;
} else if ($rent_type == 37) {
    $weekly = $searchData;
} else if ($rent_type == 38) {
    $monthly = $searchData;
}
//ksort($car_attributes[70]);
$daily['destination'] = isset($daily['destination']) ? $daily['destination'] : '';
$daily['pickup_date'] = isset($daily['pickup_date']) ? $daily['pickup_date'] : date("m/d/Y");
$daily['pickup_time'] = isset($daily['pickup_time']) ? $daily['pickup_time'] : date("h:ia");
$daily['dropoff_date'] = isset($daily['dropoff_date']) ? $daily['dropoff_date'] : date("m/d/Y");
$daily['dropoff_time'] = isset($daily['dropoff_time']) ? $daily['dropoff_time'] : date("h:ia");

$weekly['pickup_date'] = isset($weekly['pickup_date']) ? $weekly['pickup_date'] : date("m/d/Y");
$weekly['pickup_time'] = isset($weekly['pickup_time']) ? $weekly['pickup_time'] : date("h:ia");
$weekly['no_of_weeks'] = isset($weekly['no_of_weeks']) ? $weekly['no_of_weeks'] : '';
$weekly['dropoff_date'] = isset($weekly['dropoff_date']) ? $weekly['dropoff_date'] : date("m/d/Y");

$monthly['pickup_date'] = isset($monthly['pickup_date']) ? $monthly['pickup_date'] : date("m/d/Y");
$monthly['pickup_time'] = isset($monthly['pickup_time']) ? $monthly['pickup_time'] : date("h:ia");
$monthly['no_of_month'] = isset($monthly['no_of_month']) ? $monthly['no_of_month'] : '';
$monthly['dropoff_date'] = isset($monthly['dropoff_date']) ? $monthly['dropoff_date'] : date("m/d/Y");
?>

    <section class="car-listing-box common-padding">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            .affix {
                top: 0px;
                width: 850px;
                z-index: 100;
            }
            
            .affix + .container-fluid {
                padding-top: 70px;
            }
            
            #myDIV {
                border: 1px solid black;
                background-color: lightblue;
                width: 200px;
                height: 210px;
            }
            
            @media only screen and (max-width: 600px) {
                .affix {
                    width: 90%;
                }
            }
        </style>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 pull-right">
                    <div class="sorting-box" data-spy="affix" data-offset-top="199">
                        <div class="tab-pane daily-tab <?php echo ($rent_type == 36) ? 'active' : ''; ?>" id="Daily">
                            <form action="<?php echo site_url('cars'); ?>" method="post" id="frm_daily" >
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="loation-box hidden">
                                        <label>
                                            <?php echo getLang('CAR_FILTER_PICKUP_CITY')?>
                                        </label>
                                        <?php if (isset($car_attributes[70])) { ?>
                                            <div class="pickup-location-input">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <select name="car_city" id="car_city_daily" class="city_select select2">

                                                    <?php
                                        foreach ($car_attributes[70] as $city) {
                                            if ($car_city == $city['id']) {
                                                $selected = 'selected="selected"';
                                            } else {
                                                $selected = '';
                                            }
                                            ?>
                                                        <option value="<?php echo $city['id']; ?>" <?php echo $selected; ?>>
                                                            <?php echo $city['title']; ?>
                                                        </option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                            <?php } ?>

                                                <div class="pickup-option-box in_city_highway">
                                                    <?php
                                $highway = (isset($daily['incity']) && $daily['incity'] == 'highway') ? 'checked="checked"' : '';
                                $in_city = ((isset($daily['incity']) && $daily['incity'] == 'in_city') || $highway=='') ? 'checked="checked"' : '';

                                $displayDesination = ($highway != '') ? 'display:block' : '';
                                ?>
                                                        <ul>
                                                            <li>
                                                                <input type="radio" name="daily[incity]" <?php echo $in_city; ?> id="city" value="in_city" />
                                                                <label for="city">
                                                                    <?php echo getLang('CAR_FILTER_IN_CITY')?>
                                                                </label>
                                                            </li>
                                                            <li id="HighwayClick">
                                                                <input type="radio" name="daily[incity]" id="Highway" <?php echo $highway; ?> value="highway"/>
                                                                <label for="Highway">
                                                                    <?php echo getLang('CAR_FILTER_HIGHWAY')?>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                </div>
                                    </div>
                                    <!-- / Location 1 / -->
                                    <div class="loation-box hidden" id="desitanted-location" style="<?php echo $displayDesination; ?>">
                                        <label>
                                            <?php echo getLang('CAR_FILTER_DESTINATION')?>:</label>
                                        <div class="pickup-location-input">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <select name="daily[destination]" id="destination" class="select2"></select>
                                            <!-- <input type="text" name="daily[destination]" value="<?php echo $daily['destination']; ?>" placeholder="Destinated Location" /> -->
                                        </div>
                                    </div>
                                    <!-- / Location 2 / -->

                                    <div class="air_pickup row">
                                        <div class="col-sm-4">
                                            <label>
                                                <?php echo getLang('CAR_FILTER_PICKUP_DATE_TIME')?>:</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="pickup-location-input">
                                                <div class="date-boxi">
                                                    <input type="text" id="daily_pickup_date" class="required" name="daily[pickup_date]" value="<?php echo $daily['pickup_date']; ?>" placeholder="Start Date">
                                                </div>
                                                <div class="time-boxi">
                                                    <input type="text" class="timepicker" id="daily_pickup_time" name="daily[pickup_time]" value="<?php echo $daily['pickup_time']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="air_dropoff row">
                                        <div class="col-sm-4">
                                            <label>
                                                <?php echo getLang('CAR_FILTER_DROPOFF_DATE_TIME')?>:</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="pickup-location-input">
                                                <?php if ($rent_type==36): ?>
                                                    <div class="date-boxi">
                                                        <input type="text" id="daily_dropoff_date" name="daily[dropoff_date]" placeholder="Drop Off Date" value="<?php echo $daily['dropoff_date']; ?>">
                                                    </div>
                                                <?php endif ?>
                                                <?php if ($rent_type==37): ?>
                                                    <div class="date-boxi">
                                                        <input type="text" id="daily_dropoff_date" name="daily[dropoff_date]" placeholder="Drop Off Date" value="<?php echo $weekly['dropoff_date']; ?>">
                                                    </div>
                                                <?php endif ?>
                                                <?php if ($rent_type==38): ?>
                                                    <div class="date-boxi">
                                                        <input type="text" id="daily_dropoff_date" name="daily[dropoff_date]" placeholder="Drop Off Date" value="<?php echo $monthly['dropoff_date']; ?>">
                                                    </div>
                                                <?php endif ?>
                                                <div class="time-boxi">
                                                    <input type="text" class="timepicker" id="daily_dropoff_time" name="daily[dropoff_time]" value="<?php echo $daily['dropoff_time']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <label><?php echo getLang('CAR_LIST_SORT_BY');?> :</label>
                                        </div>
                                        <div class="col-md-6">

                                            <?php $order_by = ($filters['order_by']=='ratings')?$filters['order_by']:'id'?>
                                                <select id="ordering" class="call_ajax select2">
                                                    <option value="ratings" <?php echo ($order_by=='ratings' )? 'selected=selected': ''?>>
                                                        <?php echo getLang('CAR_LIST_SORT_BY_TOP_RATED');?>
                                                    </option>
                                                    <option value="id" <?php echo ($order_by=='id' )? 'selected=selected': ''?>>
                                                        <?php echo getLang('CAR_LIST_SORT_BY_NEWEST');?>
                                                    </option>
                                                    <option value="user_id" <?php echo ($order_by=='user_id' )? 'selected=selected': ''?>>
                                                        <?php echo getLang('CAR_LIST_SORT_BY_SUPPLIER');?>
                                                    </option>
                                                </select>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        
                                        <?php echo $this->load->view('car_filter_sortby');?>
                                    </div>
                                </div>

                                    
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <?php echo $this->load->view('car_filter');?>
                    <div class="col-sm-9 pull-right">
                        <div class="car-listing-inner">
                            <div class="" id="listss"></div>
                        </div>
                        <?php if(true){ ?>
                            <div class="pagination pagination-box" id="paging"></div>
                            <input type="hidden" name="cpage" value="1" id="cpage" />
                            <?php } ?>
                    </div>
            </div>
        </div>
    </section>
    <script>
        var airportOption = '<?php echo $searchData['
        airport '];?>';
        if (airportOption != '') {
            $(".in_city_highway").hide('slow');
            $("#desitanted-location").hide('slow');
            $(".each-day-hourly").hide('slow');
            if (airportOption == 'pickup') {
                $('#pickup').prop('checked', true);
                $(".air_dropoff").hide('slow');
                $(".air_pickup").show('slow');
            }
            if (airportOption == 'dropoff') {
                $('#dropoff').prop('checked', true);
                $(".air_pickup").hide('slow');
                $(".air_dropoff").show('slow');
            }

        }

        function myFunction() {
            document.getElementById("myDIV").style.overflowY = "scroll";
        }
    </script>
    <?php $this->load->view("multi_days_selection");?>
        <?php $this->load->view("multi_locations_selection");?>