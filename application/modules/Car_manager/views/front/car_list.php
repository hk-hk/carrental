<section class="car-listing-box common-padding">
    <div class="container">
        <div class="row">
            <?php echo $this->load->view('car_filter'); ?>
            <div class="col-sm-9">
                <!-- <div> -->
                    <div class="sorting-box">
                        <div class="row">
                            
                        
                            <div class="col-sm-2">
                                <label><?php echo getLang('CAR_FILTER_CAR_COMPANY')?>:</label>
                            </div>
                            <div class="col-sm-4">
                                <select id="car_company" name="car_company" class="call_ajax select2">
                                    <option value="0"><?php echo getLang('CAR_FILTER_SELECT_OPTION')?></option>
                                    <?php foreach ($suppliers as $supplier) {
                                        // if($filters['car_company']== $supplier['id']){ ?>
                                            <option value="<?php echo $supplier->id; ?>"><?php echo $supplier->user_company; ?></option>
                                        <?php 
                                    } ?>
                                </select>
                                
                            </div>
                            <div class="col-sm-2">
                                <label><?php echo getLang('CAR_LIST_SORT_BY'); ?> :</label>
                            </div>
                            <div class="col-sm-4">
                                <?php $order_by = ($filters['order_by'] == 'ratings') ? $filters['order_by'] : 'user_id'?>
                                <select id="ordering" class="call_ajax select2">
                                    <option value="user_id" <?php echo ($order_by == 'user_id') ? 'selected=selected' : '' ?>>
                                        <?php echo getLang('CAR_LIST_SORT_BY_SUPPLIER'); ?>
                                    </option>

                                    <option value="id" <?php echo ($order_by == 'id') ? 'selected=selected' : '' ?>>
                                        <?php echo getLang('CAR_LIST_SORT_BY_NEWEST'); ?></option>
                                    <option value="ratings" <?php echo ($order_by == 'ratings') ? 'selected=selected' : '' ?>>
                                        <?php echo getLang('CAR_LIST_SORT_BY_TOP_RATED'); ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                <!-- </div> -->
                <div class="car-listing-inner">
                    <div class="" id="listss"></div>
                </div>
                <?php if (true) {?>
                <div class="pagination pagination-box" id="paging"></div>
                <input type="hidden" name="cpage" value="1" id="cpage" />
                <?php }?>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("multi_days_selection");?>
<?php $this->load->view("multi_locations_selection");?>

