<?php
//print_r($searchData);die($rent_type);
if ($rent_type == 36) {
    $daily = $searchData;
} else if ($rent_type == 37) {
    $weekly = $searchData;
} else if ($rent_type == 38) {
    $monthly = $searchData;
}
//ksort($car_attributes[70]);
$daily['destination'] = isset($daily['destination']) ? $daily['destination'] : '';
$daily['pickup_date'] = isset($daily['pickup_date']) ? $daily['pickup_date'] : date("m/d/Y");
$daily['pickup_time'] = isset($daily['pickup_time']) ? $daily['pickup_time'] : date("h:ia");
$daily['dropoff_date'] = isset($daily['dropoff_date']) ? $daily['dropoff_date'] : date("m/d/Y");
$daily['dropoff_time'] = isset($daily['dropoff_time']) ? $daily['dropoff_time'] : date("h:ia");

$weekly['pickup_date'] = isset($weekly['pickup_date']) ? $weekly['pickup_date'] : date("m/d/Y");
$weekly['pickup_time'] = isset($weekly['pickup_time']) ? $weekly['pickup_time'] : date("h:ia");
$weekly['no_of_weeks'] = isset($weekly['no_of_weeks']) ? $weekly['no_of_weeks'] : '';

$monthly['pickup_date'] = isset($monthly['pickup_date']) ? $monthly['pickup_date'] : date("m/d/Y");
$monthly['pickup_time'] = isset($monthly['pickup_time']) ? $monthly['pickup_time'] : date("h:ia");
$monthly['no_of_month'] = isset($monthly['no_of_month']) ? $monthly['no_of_month'] : '';
?>


<div class="col-sm-6">
    <label><?php echo getLang('CAR_FILTER_CAR_TYPE')?>:</label>
</div>
<div class="col-sm-6">

    <select id="car_type" name="car_type" class="call_ajax select2">
        <option value="0"><?php echo getLang('CAR_FILTER_SELECT_OPTION')?></option>
        <?php foreach ($car_attributes[4] as $cartype) {
            if($filters['car_type']== $cartype['id']){ ?>
                <option value="<?php echo $cartype['id']; ?>" selected="selected" ><?php echo $cartype['title']; ?></option>
            <?php }else{ ?>
                <option value="<?php echo $cartype['id']; ?>"><?php echo $cartype['title']; ?></option>
            <?php }
        } ?>
    </select>
</div>



<script>
    var airportOption = '<?php echo $searchData['airport'];?>';
    if (airportOption!='') {
        $(".in_city_highway").hide('slow');
        $("#desitanted-location").hide('slow');
        $(".each-day-hourly").hide('slow');
        if(airportOption=='pickup'){
            $('#pickup').prop('checked', true);
            $(".air_dropoff").hide('slow');
            $(".air_pickup").show('slow');
        }
        if(airportOption=='dropoff'){
            $('#dropoff').prop('checked', true);
            $(".air_pickup").hide('slow');
            $(".air_dropoff").show('slow');
        }

    }
</script>
