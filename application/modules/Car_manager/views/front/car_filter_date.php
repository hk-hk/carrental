<?php
//print_r($searchData);die($rent_type);
if ($rent_type == 36) {
    $daily = $searchData;
} else if ($rent_type == 37) {
    $weekly = $searchData;
} else if ($rent_type == 38) {
    $monthly = $searchData;
}
//ksort($car_attributes[70]);
$daily['destination'] = isset($daily['destination']) ? $daily['destination'] : '';
$daily['pickup_date'] = isset($daily['pickup_date']) ? $daily['pickup_date'] : date("m/d/Y");
$daily['pickup_time'] = isset($daily['pickup_time']) ? $daily['pickup_time'] : date("h:ia");
$daily['dropoff_date'] = isset($daily['dropoff_date']) ? $daily['dropoff_date'] : date("m/d/Y");
$daily['dropoff_time'] = isset($daily['dropoff_time']) ? $daily['dropoff_time'] : date("h:ia");

$weekly['pickup_date'] = isset($weekly['pickup_date']) ? $weekly['pickup_date'] : date("m/d/Y");
$weekly['pickup_time'] = isset($weekly['pickup_time']) ? $weekly['pickup_time'] : date("h:ia");
$weekly['no_of_weeks'] = isset($weekly['no_of_weeks']) ? $weekly['no_of_weeks'] : '';

$monthly['pickup_date'] = isset($monthly['pickup_date']) ? $monthly['pickup_date'] : date("m/d/Y");
$monthly['pickup_time'] = isset($monthly['pickup_time']) ? $monthly['pickup_time'] : date("h:ia");
$monthly['no_of_month'] = isset($monthly['no_of_month']) ? $monthly['no_of_month'] : '';
?>

<div class="tab-pane daily-tab <?php echo ($rent_type == 36) ? 'active' : ''; ?>" id="Daily">
    <form action="<?php echo site_url('cars'); ?>" method="post" id="frm_daily">
        <div class="loation-box hidden">
            <label><?php echo getLang('CAR_FILTER_PICKUP_CITY')?></label>
            <?php if (isset($car_attributes[70])) { ?>
                <div class="pickup-location-input">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <select name="car_city" id="car_city_daily" class="city_select select2" >

                        <?php
                        foreach ($car_attributes[70] as $city) {
                            if ($car_city == $city['id']) {
                                $selected = 'selected="selected"';
                            } else {
                                $selected = '';
                            }
                            ?>
                            <option value="<?php echo $city['id']; ?>" <?php echo $selected; ?>><?php echo $city['title']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            <?php } ?>

            <div class="pickup-option-box in_city_highway">
                <?php
                $highway = (isset($daily['incity']) && $daily['incity'] == 'highway') ? 'checked="checked"' : '';
                $in_city = ((isset($daily['incity']) && $daily['incity'] == 'in_city') || $highway=='') ? 'checked="checked"' : '';

                $displayDesination = ($highway != '') ? 'display:block' : '';
                ?>
                <ul>
                    <li>
                        <input type="radio" name="daily[incity]" <?php echo $in_city; ?> id="city" value="in_city" />
                        <label for="city"><?php echo getLang('CAR_FILTER_IN_CITY')?></label>
                    </li>
                    <li id="HighwayClick">
                        <input type="radio" name="daily[incity]" id="Highway" <?php echo $highway; ?> value="highway"/>
                        <label for="Highway"><?php echo getLang('CAR_FILTER_HIGHWAY')?></label>
                    </li>
                </ul>
            </div>
        </div><!-- / Location 1 / -->
        <div class="loation-box hidden" id="desitanted-location" style="<?php echo $displayDesination; ?>">
            <label><?php echo getLang('CAR_FILTER_DESTINATION')?>:</label>
            <div class="pickup-location-input">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <select name="daily[destination]" id="destination" class="select2" ></select>
                <!-- <input type="text" name="daily[destination]" value="<?php echo $daily['destination']; ?>" placeholder="Destinated Location" /> -->
            </div>
        </div><!-- / Location 2 / -->

        <div class="air_pickup">
            <div class="col-sm-2">
            <label><?php echo getLang('CAR_FILTER_PICKUP_DATE_TIME')?>:</label>
            </div>
            <div class="col-sm-4">
            <div class="pickup-location-input">
                <div class="date-boxi">
                    <input type="text" id="daily_pickup_date" class="required"  name="daily[pickup_date]" value="<?php echo $daily['pickup_date']; ?>" placeholder="Start Date">
                </div>
                <div class="time-boxi">
                    <input type="text" class="timepicker" id="daily_pickup_time" name="daily[pickup_time]" value="<?php echo $daily['pickup_time']; ?>">
                </div>
            </div>
            </div>
        </div><!-- / Location 3 / -->

    </form>
</div>
<!--<div class="loation-box">-->
<!--    <label>--><?php //echo getLang('CAR_FILTER_CAR_TYPE')?><!--:</label>-->
<!--    <div class="sortby-option">-->
<!--        <ul>-->
<!--            --><?php //$selectedCartypes = explode(",", $filters['car_type']);
//            foreach ($car_attributes[4] as $cartype) {
//                if(in_array($cartype['id'], $selectedCartypes)){
//                    $checked = "checked=checked";
//                }else{
//                    $checked = '';
//                }
//                ?>
<!--                <li>-->
<!--                    <input type="checkbox" --><?php //echo $checked;?><!-- class="call_ajax" name="car_type" id="car_type_--><?php //echo $cartype['id']; ?><!--" value="--><?php //echo $cartype['id']; ?><!--" />-->
<!--                    <label for="car_type_--><?php //echo $cartype['id']; ?><!--">--><?php //echo $cartype['title']; ?><!--</label>-->
<!--                </li>-->
<!--            --><?php //} ?>
<!--        </ul>-->
<!--    </div>-->
<!--</div>-->


<script>
    var airportOption = '<?php echo $searchData['airport'];?>';
    if (airportOption!='') {
        $(".in_city_highway").hide('slow');
        $("#desitanted-location").hide('slow');
        $(".each-day-hourly").hide('slow');
        if(airportOption=='pickup'){
            $('#pickup').prop('checked', true);
            $(".air_dropoff").hide('slow');
            $(".air_pickup").show('slow');
        }
        if(airportOption=='dropoff'){
            $('#dropoff').prop('checked', true);
            $(".air_pickup").hide('slow');
            $(".air_dropoff").show('slow');
        }

    }
</script>
