<?php
//print_r($searchData);die($rent_type);
if ($rent_type == 36) {
    $daily = $searchData;
} else if ($rent_type == 37) {
    $weekly = $searchData;
} else if ($rent_type == 38) {
    $monthly = $searchData;
}
//ksort($car_attributes[70]); 
$daily['destination'] = isset($daily['destination']) ? $daily['destination'] : '';
$daily['pickup_date'] = isset($daily['pickup_date']) ? $daily['pickup_date'] : date("m/d/Y");
$daily['pickup_time'] = isset($daily['pickup_time']) ? $daily['pickup_time'] : date("h:ia");
$daily['dropoff_date'] = isset($daily['dropoff_date']) ? $daily['dropoff_date'] : date("m/d/Y");
$daily['dropoff_time'] = isset($daily['dropoff_time']) ? $daily['dropoff_time'] : date("h:ia");

$weekly['pickup_date'] = isset($weekly['pickup_date']) ? $weekly['pickup_date'] : date("m/d/Y");
$weekly['pickup_time'] = isset($weekly['pickup_time']) ? $weekly['pickup_time'] : date("h:ia");
$weekly['no_of_weeks'] = isset($weekly['no_of_weeks']) ? $weekly['no_of_weeks'] : '';

$monthly['pickup_date'] = isset($monthly['pickup_date']) ? $monthly['pickup_date'] : date("m/d/Y");
$monthly['pickup_time'] = isset($monthly['pickup_time']) ? $monthly['pickup_time'] : date("h:ia");
$monthly['no_of_month'] = isset($monthly['no_of_month']) ? $monthly['no_of_month'] : '';
?>
<script type="text/javascript">
    
    SELECTED_CITY="<?= $car_city ?>";
</script>
<div class="col-sm-3" >
    <div class="aside-listing">
        <div id="CatagoryTabs" class="search-category"> 
            <h2><?php echo getLang('CAR_FILTER_HEADING');?></h2>
            <ul class="nav nav-tabs">
                <li id="36" class="rent_type <?php echo ($rent_type == 36) ? 'active' : ''; ?>" >
                    <a href="#Daily" data-toggle="tab"><?php echo getLang('CAR_FILTER_BY_DAILY')?></a>
                </li>
             <!-- <li id="37" class="rent_type <?php echo ($rent_type == 37) ? 'active' : ''; ?>">
                    <a href="#Weekly" data-toggle="tab"><?php echo getLang('CAR_FILTER_BY_WEEKLY')?></a>
                </li> -->
               <!--  <li id="38" class="rent_type <?php echo ($rent_type == 38) ? 'active' : ''; ?>">
                    <a href="#Monthly" data-toggle="tab"><?php echo getLang('CAR_FILTER_BY_MONTHLY')?></a>
                    <input type="hidden" name="rent_type" value="<?php echo $rent_type; ?>" id="selected_rent_type" />
                </li> -->
                <input type="hidden" name="rent_type" value="<?php echo $rent_type; ?>" id="selected_rent_type" />

            </ul>
            <div class="tab-content">
                <div class="tab-pane daily-tab <?php echo ($rent_type == 36) ? 'active' : ''; ?>" id="Daily">
                    <form action="<?php echo site_url('cars'); ?>" method="post" id="frm_daily">
                        <div class="loation-box">
                            <label><?php echo getLang('CAR_FILTER_PICKUP_CITY')?></label>
                            <?php if (isset($car_attributes[70])) { ?>
                                <div class="pickup-location-input">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <select name="car_city" id="car_city_daily" class="city_select select2" >
                                        
                                        <?php
                                        foreach ($car_attributes[70] as $city) {
                                            if ($car_city == $city['id']) {
                                                $selected = 'selected="selected"';
                                            } else {
                                                $selected = '';
                                            }
                                            ?>
                                            <option value="<?php echo $city['id']; ?>" <?php echo $selected; ?>><?php echo $city['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                            <div class="pickup-option-box-air">
                                <ul>
                                    <li>
                                        <input type="checkbox" name="daily[airport]" id="pickup" class="airport_active" value="pickup" />
                                        <label for="pickup"><?php echo getLang('AIRPORT_PICKUP_OPTION');?></label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="daily[airport]" id="dropoff"  class="airport_active"  value="dropoff"/>
                                        <label for="dropoff"><?php echo getLang('AIRPORT_DROPOFF_OPTION');?></label>
                                    </li>
                                </ul>
                            </div>
                            <div class="pickup-option-box in_city_highway">
                                <?php
                                $highway = (isset($daily['incity']) && $daily['incity'] == 'highway') ? 'checked="checked"' : '';
                                $in_city = ((isset($daily['incity']) && $daily['incity'] == 'in_city') || $highway=='') ? 'checked="checked"' : '';
                                
                                $displayDesination = ($highway != '') ? 'display:block' : '';
                                ?>
                                <ul>
                                    <li>
                                        <input type="radio" name="daily[incity]" <?php echo $in_city; ?> id="city" value="in_city" />
                                        <label for="city"><?php echo getLang('CAR_FILTER_IN_CITY')?></label>
                                    </li>
                                    <li id="HighwayClick">
                                        <input type="radio" name="daily[incity]" id="Highway" <?php echo $highway; ?> value="highway"/>
                                        <label for="Highway"><?php echo getLang('CAR_FILTER_HIGHWAY')?></label>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- / Location 1 / -->
                        <div class="loation-box" id="desitanted-location" style="<?php echo $displayDesination; ?>">
                            <label><?php echo getLang('CAR_FILTER_DESTINATION')?>:</label>
                            <div class="pickup-location-input">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <select name="daily[destination]" id="destination" class="select2" ></select>
                                <!-- <input type="text" name="daily[destination]" value="<?php echo $daily['destination']; ?>" placeholder="Destinated Location" /> -->
                            </div>
                        </div><!-- / Location 2 / -->
                        <div class="loation-box air_pickup">
                            <label><?php echo getLang('CAR_FILTER_PICKUP_DATE_TIME')?>:</label>
                            <div class="pickup-location-input">
                                <div class="date-box">
                                    <input type="text" id="daily_pickup_date" class="required"  name="daily[pickup_date]" value="<?php echo $daily['pickup_date']; ?>" placeholder="Start Date">
                                </div>
                                <div class="time-box">
                                    <input type="text" class="timepicker" id="daily_pickup_time" name="daily[pickup_time]" value="<?php echo $daily['pickup_time']; ?>">
                                </div>
                            </div>
                        </div><!-- / Location 3 / -->
                        <div class="loation-box air_dropoff">
                            <label><?php echo getLang('CAR_FILTER_DROPOFF_DATE_TIME')?>:</label>
                            <div class="pickup-location-input">
                                <div class="date-box">
                                    <input type="text" id="daily_dropoff_date" name="daily[dropoff_date]" placeholder="Drop Off Date" value="<?php echo $daily['dropoff_date']; ?>">
                                </div>
                                <div class="time-box">
                                    <input type="text" class="timepicker" id="daily_dropoff_time" name="daily[dropoff_time]" value="<?php echo $daily['dropoff_time']; ?>">
                                </div>
                            </div>
                        </div><!-- / Location 4 / -->
                        <div class="search-submit">
                            <input type="button" name="" class="refine_search" value="<?php echo getLang('CAR_FILTER_REFINE_SEARCH_BTN')?>" />
                        </div>
                    </form>
                </div><!-- Daily Section -->

                <div class="tab-pane <?php echo ($rent_type == 37) ? 'active' : ''; ?>" id="Weekly">
                    <form action="<?php echo site_url('ajax_cars'); ?>" method="post" id="frm_weekly">
                        <div class="loation-box">
                            <?php
                            $weekly['pickup_date'] = isset($weekly['pickup_date']) ? $weekly['pickup_date'] : '';
                            $weekly['pickup_time'] = isset($weekly['pickup_time']) ? $weekly['pickup_time'] : '';
                            $weekly['no_of_weeks'] = isset($weekly['no_of_weeks']) ? $weekly['no_of_weeks'] : '';
                            ?>
                            <label><?php echo getLang('CAR_FILTER_PICKUP_CITY')?></label>
                            <?php if (isset($car_attributes[70])) { ?>
                                <div class="pickup-location-input">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <select name="car_city" id="car_city_weekly" class="city_select select2" >
                                        
                                        <?php
                                        foreach ($car_attributes[70] as $city) {
                                            if ($car_city == $city['id']) {
                                                $selected = 'selected="selected"';
                                            } else {
                                                $selected = '';
                                            }
                                            ?>
                                            <option value="<?php echo $city['id']; ?>" <?php echo $selected ?>><?php echo $city['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                        </div><!-- / Location 1 / -->
                        <div class="loation-box">
                            <label><?php echo getLang('CAR_FILTER_PICKUP_DATE')?>:</label>
                            <div class="pickup-location-input">
                                <div class="date-box">
                                    <input type="text" id="weekly_pickup_date" name="weekly[pickup_date]" placeholder="Start Date" value="<?php echo $weekly['pickup_date']; ?>">
                                </div>
                                <div class="time-box">
                                    <input type="text" class="timepicker" name="weekly[pickup_time]" value="<?php echo $weekly['pickup_time']; ?>">
                                </div>
                            </div>
                        </div><!-- / Location 2 / -->
                        <div class="loation-box">
                            <label><?php echo getLang('CAR_FILTER_NO_OF_WEEK')?>:</label>
                            <div class="pickup-location-input pickup-select" >
                                <select id="" name="weekly[no_of_weeks]">
                                    <option value="1" <?php echo ($weekly['no_of_weeks'] == 1) ? 'selected="selected"' : ''; ?>>1 <?php echo getLang('FILTER_TEXT_WEEK')?></option>
                                    <option value="2" <?php echo ($weekly['no_of_weeks'] == 2) ? 'selected="selected"' : ''; ?>>2 <?php echo getLang('FILTER_TEXT_WEEKS')?></option>
                                    <option value="3" <?php echo ($weekly['no_of_weeks'] == 3) ? 'selected="selected"' : ''; ?>>3 <?php echo getLang('FILTER_TEXT_WEEKS')?></option>
                                    <option value="4" <?php echo ($weekly['no_of_weeks'] == 4) ? 'selected="selected"' : ''; ?>>4 <?php echo getLang('FILTER_TEXT_WEEKS')?></option>
                                </select>
                            </div>
                        </div><!-- / Location 3 / -->   
                        <div class="search-submit">
                            <input type="button" name="" class="refine_search" value="<?php echo getLang('CAR_FILTER_REFINE_SEARCH_BTN')?>" />
                            
                        </div>
                    </form>
                </div><!-- Weekly Section -->

                <div class="tab-pane <?php echo ($rent_type == 38) ? 'active' : ''; ?>" id="Monthly">
                    <form action="<?php echo site_url('cars'); ?>" method="post" id="frm_monthly">
                        <div class="loation-box">
                            <label><?php echo getLang('CAR_FILTER_PICKUP_CITY')?></label>
                                <?php if (isset($car_attributes[70])) { ?>
                                <div class="pickup-location-input">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <select name="car_city" id="car_city_monthly" class="select2" >
                                        
                                        <?php
                                        foreach ($car_attributes[70] as $city) {
                                            if ($car_city == $city['id']) {
                                                $selected = 'selected="selected"';
                                            } else {
                                                $selected = '';
                                            }
                                            ?>
                                            <option value="<?php echo $city['id']; ?>" <?php echo $selected; ?>><?php echo $city['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <?php } ?>
                        </div><!-- / Location 1 / -->
                        <div class="loation-box">
                            <label><?php echo getLang('CAR_FILTER_PICKUP_DATE')?>:</label>
                            <div class="pickup-location-input">
                                <div class="date-box">
                                    <input type="text" id="monthly_pickup_date" name="monthly[pickup_date]" placeholder="Start Date" value="<?php echo $monthly['pickup_date'] ?>">
                                </div>
                                <div class="time-box">
                                    <input type="text" class="timepicker" name="monthly[pickup_time]" value="<?php echo $monthly['pickup_time']; ?>">
                                </div>
                            </div>
                        </div><!-- / Location 2 / -->
                        <div class="loation-box">
                            <label><?php echo getLang('CAR_FILTER_NO_OF_MONTHS')?>:</label>
                            <div class="pickup-location-input pickup-select">
                                <select id="" name="monthly[no_of_month]">
                                    <?php for ($i = 1; $i <=12; $i++) {
                                        $monthText = ($i == 1) ? ' Month' : " Months"
                                        ?>
                                        <option value="<?php echo $i; ?>" <?php echo ($monthly['no_of_month'] == $i) ? 'selected="selected"' : ''; ?>><?php echo $i . $monthText; ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><!-- / Location 3 / -->   
                        <div class="search-submit">
                            <input type="button" name="" class="refine_search" value="<?php echo getLang('CAR_FILTER_REFINE_SEARCH_BTN')?>" />
                        </div>
                    </form>
                </div><!-- Monhtly Section -->
                <?php if(is_array($multiDatesWithCity) && count($multiDatesWithCity)>1){ ?>
                <table class="each-day-hourly">
                <?php foreach ($multiDatesWithCity as $date=>$val){?>
                    <tr class="date-heading">
                        <th><?php echo date("D,M d,Y",strtotime($date));?></th>
                    </tr>
                    <tr>
                        <th><?php echo getLang('BOOKING_INFO_FROM_TIME')?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_TO_TIME')?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_HOURS_IN_A_DAY')?>: </th>
                    </tr>
                    <tr>
                        <td><?php echo $val['from'];?></td>
                        <td><?php echo $val['to'];?></td>
                        <td><?php echo $val['duration'];?></td>
                    </tr>
                <?php } ?>
                </table>
                <?php }else if(isset($datewiseDestinations)){ 
                        $titles = $datewiseDestinations['titles']; 
                        unset($datewiseDestinations['titles']);
                        unset($datewiseDestinations['total_distance']);
                        ?>
                     <table class="each-day-hourly">
                <?php foreach ($datewiseDestinations as $date=>$val){?>
                    <tr class="date-heading">
                        <th colspan="3"><?php echo date("D,M d,Y",strtotime($date));?></th>
                    </tr>
                    <tr>
                        <th><?php echo getLang('BOOKING_INFO_FROM')?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_TO')?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_DISTANCE')?>:</th>
                    </tr>
                    <tr>
                        <td><?php echo (isset($titles[$val['from']])?$titles[$val['from']]:'none');?></td>
                        <td><?php echo (isset($titles[$val['to']])?$titles[$val['to']]:'none');?></td>
                        <td><?php echo $val['distance'];?></td>
                    </tr>
                <?php } ?>
                </table>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="aside-listing hide-sortby-mobile">
        <div class="sortby-fetaure">
            <div class="loation-box">
                <label><?php echo getLang('CAR_FILTER_FUEL_TYPE')?>:</label>
                <div class="pickup-location-input">
                    <select id="fuel_type_id" class="call_ajax">
                        <option value="0"><?php echo getLang('CAR_FILTER_SELECT_OPTION')?></option>
                        <?php foreach ($car_attributes[3] as $fueltype) { 
                            if($filters['fuel_type']==$fueltype['id']){ ?>
                                <option value="<?php echo $fueltype['id']; ?>" selected="selected"><?php echo $fueltype['title']; ?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $fueltype['id']; ?>"><?php echo $fueltype['title']; ?></option>
                            <?php }
                            } ?>
                    </select>
                </div>
            </div>
            <div class="loation-box">
                <label><?php echo getLang('CAR_FILTER_PASSENGERS')?>:</label>
                <div class="pickup-location-input">
                    <select id="no_of_passenger_id" class="call_ajax">
                        <option value="0"><?php echo getLang('CAR_FILTER_SELECT_OPTION')?></option>
                            <?php foreach ($car_attributes[42] as $noofpass) { 
                                if($filters['no_of_passanger']== $noofpass['id']){ ?>
                                    <option value="<?php echo $noofpass['id']; ?>" selected="selected"><?php echo $noofpass['title']; ?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $noofpass['id']; ?>"><?php echo $noofpass['title']; ?></option>
                            <?php }    
                                } ?>
                    </select>
                </div>
            </div>
            <div class="loation-box">
                <label><?php echo getLang('CAR_FILTER_CAR_TYPE')?>:</label>
                <div class="sortby-option">

                    <select id="car_type" name="car_type" class="call_ajax select2">
                        <option value="0"><?php echo getLang('CAR_FILTER_SELECT_OPTION')?></option>
                        <?php foreach ($car_attributes[4] as $cartype) {
                            if($filters['car_type']== $cartype['id']){ ?>
                                <option value="<?php echo $cartype['id']; ?>" selected="selected" ><?php echo $cartype['title']; ?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $cartype['id']; ?>"><?php echo $cartype['title']; ?></option>
                            <?php }
                        } ?>
                    </select>

                    <!-- <ul>
                        <?php $selectedCartypes = explode(",", $filters['car_type']);
                        foreach ($car_attributes[4] as $cartype) {
                            if(in_array($cartype['id'], $selectedCartypes)){
                                $checked = "checked=checked";
                            }else{
                                $checked = '';
                            }
                            ?>
                            <li>
                                <input type="checkbox" <?php echo $checked;?> class="call_ajax" name="car_type" id="car_type_<?php echo $cartype['id']; ?>" value="<?php echo $cartype['id']; ?>" />
                                <label for="car_type_<?php echo $cartype['id']; ?>"><?php echo $cartype['title']; ?></label> 
                            </li>
                        <?php } ?>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var airportOption = '<?php echo $searchData['airport'];?>';
if (airportOption!='') {
    $(".in_city_highway").hide('slow');
    $("#desitanted-location").hide('slow');
    $(".each-day-hourly").hide('slow');
    if(airportOption=='pickup'){
        $('#pickup').prop('checked', true);
        $(".air_dropoff").hide('slow');
        $(".air_pickup").show('slow');
    }
    if(airportOption=='dropoff'){
        $('#dropoff').prop('checked', true);
        $(".air_pickup").hide('slow');
        $(".air_dropoff").show('slow');
    }

}    
</script>