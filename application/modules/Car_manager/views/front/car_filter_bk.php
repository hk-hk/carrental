<?php
//print_r($searchData);die($rent_type);
if ($rent_type == 36) {
    $daily = $searchData;
} else if ($rent_type == 37) {
    $weekly = $searchData;
} else if ($rent_type == 38) {
    $monthly = $searchData;
}
//ksort($car_attributes[70]); 
$daily['destination'] = isset($daily['destination']) ? $daily['destination'] : '';
$daily['pickup_date'] = isset($daily['pickup_date']) ? $daily['pickup_date'] : date("m/d/Y");
$daily['pickup_time'] = isset($daily['pickup_time']) ? $daily['pickup_time'] : date("h:ia");
$daily['dropoff_date'] = isset($daily['dropoff_date']) ? $daily['dropoff_date'] : date("m/d/Y");
$daily['dropoff_time'] = isset($daily['dropoff_time']) ? $daily['dropoff_time'] : date("h:ia");

$weekly['pickup_date'] = isset($weekly['pickup_date']) ? $weekly['pickup_date'] : date("m/d/Y");
$weekly['pickup_time'] = isset($weekly['pickup_time']) ? $weekly['pickup_time'] : date("h:ia");
$weekly['no_of_weeks'] = isset($weekly['no_of_weeks']) ? $weekly['no_of_weeks'] : '';

$monthly['pickup_date'] = isset($monthly['pickup_date']) ? $monthly['pickup_date'] : date("m/d/Y");
$monthly['pickup_time'] = isset($monthly['pickup_time']) ? $monthly['pickup_time'] : date("h:ia");
$monthly['no_of_month'] = isset($monthly['no_of_month']) ? $monthly['no_of_month'] : '';
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="col-sm-3 pull-left">
    <div class="aside-listing hide-sortby-mobile">
        <div id="CatagoryTabs" class="search-category"> 

            <div class="tab-content">
                <!-- Daily Section -->
<input type="hidden" name="rent_type" value="<?php echo $rent_type; ?>" id="selected_rent_type" />
                <!-- Weekly Section -->
                <input style="display: none" type="radio" id="city" value="in_city"/>
                <input style="display: none" type="radio" id="Highway" value="highway" />
                <!-- Monhtly Section -->
                <?php if(is_array($multiDatesWithCity) && count($multiDatesWithCity)>1){ ?>
                <table class="each-day-hourly">
                <?php foreach ($multiDatesWithCity as $date=>$val){?>
                    <tr class="date-heading">
                        <th><?php echo date("D,M d,Y",strtotime($date));?></th>
                    </tr>
                    <tr>
                        <th><?php echo getLang('BOOKING_INFO_FROM_TIME')?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_TO_TIME')?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_HOURS_IN_A_DAY')?>: </th>
                    </tr>
                    <tr>
                        <td><?php echo $val['from'];?></td>
                        <td><?php echo $val['to'];?></td>
                        <td><?php echo $val['duration'];?></td>
                    </tr>
                <?php } ?>
                </table>
                <?php }else if(isset($datewiseDestinations)){ 
                        $titles = $datewiseDestinations['titles']; 
                        unset($datewiseDestinations['titles']);
                        unset($datewiseDestinations['total_distance']);
                        ?>
                     <table class="each-day-hourly">
                <?php foreach ($datewiseDestinations as $date=>$val){?>
                    <tr class="date-heading">
                        <th colspan="3"><?php echo date("D,M d,Y",strtotime($date));?></th>
                    </tr>
                    <tr>
                        <th><?php echo getLang('BOOKING_INFO_FROM')?>: </th>
                        <th><?php echo getLang('BOOKING_INFO_TO')?>: </th>

                    </tr>
                    <tr>
                        <td><?php echo (isset($titles[$val['from']])?$titles[$val['from']]:'none');?></td>
                        <td><?php echo (isset($titles[$val['to']])?$titles[$val['to']]:'none');?></td>

                    </tr>
                <?php } ?>
                </table>
                <?php } ?>
                <div class="search-submit">
                    <input type="button" id="refilter_btn" value="<?php echo getLang('CAR_FILTER_EDIT');?>" />
                </div>
                <div class="search-submit">
                    <input type="button" id="" onclick="location.href='<?php echo base_url();?>'" value="<?php echo getLang('CAR_FILTER_RESTART');?>" />
                </div>
            </div>
        </div>
    </div>
    <div class="aside-listing hide-sortby-mobile">
        <!-- <div >  -->

            <div class="tab-content">
                <!-- <div class="row"> -->
                    <div class="search-company">
                        <label><?php echo getLang('CAR_FILTER_CAR_COMPANY')?>:</label>
                        <select id="car_company" name="car_company" class="call_ajax select2">
                            <option value="0"><?php echo getLang('CAR_FILTER_SELECT_OPTION')?></option>
                            <?php foreach ($suppliers as $supplier) {
                                // if($filters['car_company']== $supplier['id']){ ?>
                                    <option value="<?php echo $supplier->id; ?>"><?php echo $supplier->user_company; ?></option>
                                <?php 
                            } ?>
                        </select>
                    </div>
                <!-- </div> -->
                
            </div>
        <!-- </div> -->
    </div>
</div>

<script>
var airportOption = '<?php echo $searchData['airport'];?>';
if (airportOption!='') {
    $(".in_city_highway").hide('slow');
    $("#desitanted-location").hide('slow');
    $(".each-day-hourly").hide('slow');
    if(airportOption=='pickup'){
        $('#pickup').prop('checked', true);
        $(".air_dropoff").hide('slow');
        $(".air_pickup").show('slow');
    }
    if(airportOption=='dropoff'){
        $('#dropoff').prop('checked', true);
        $(".air_pickup").hide('slow');
        $(".air_dropoff").show('slow');
    }

}    
</script>
