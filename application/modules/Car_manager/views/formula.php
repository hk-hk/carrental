<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-7">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">
              Car Formula
            </h3>
            <a style="float: right;" href="<?php echo site_url(CRM_VAR.'/car_list');?>">Back to list</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
				
	    <?php $attributes = array('id' => 'addcar-form','class'=>'form-horizontal'); ?>
			<?php echo form_open_multipart(CRM_VAR.'/formula',$attributes) ?>
	  
      <?php echo validation_errors();
      //print_r($carinfo);die;
      ?>
          <div class="box-body">
            <?php if($role !='supplier') { ?>
             
        <?php }else{ ?>
              <input type="hidden" name="user_id" value="<?php echo $carinfo->user_id;?>" />
        <?php } ?>
        <?php foreach ($formFields as $key=>$field){
          if($key=="maker" || $key=="model"){ 
                $required = (isset($field['required']) && $field['required'])?'required':'';
            ?>
            <div class="form-group">
                <label for="<?php echo $key; ?>" class="col-sm-2 control-label"><?php echo $field['label']; ?></label>
                <div class="col-sm-10">
                    <?php if(in_array($field['type'],array('text','email','password'))){ ?>
                        <!-- <input type="<?php echo $field['type'];?>" class="<?php echo $field['class']; ?>" <?php echo $required; ?> id="<?php echo $key; ?>" name="<?php echo $key; ?>"  -->
                               placeholder="<?php echo $field['placeholder']; ?>" value='<?php echo set_value($key,$carinfo->$key);?>'>
                    <?php }else if($field['type']=='number'){ ?>
                        <!-- <input type="<?php echo $field['type'];?>"  step='0.01' class="<?php echo $field['class']; ?>" <?php echo $required; ?> id="<?php echo $key; ?>" name="<?php echo $key; ?>"  -->
                               placeholder="<?php echo $field['placeholder']; ?>" value='<?php echo set_value($key,$carinfo->$key);?>'>
                    <?php } else if($field['type']=='select'  && isset ($fields_attributes[$key])){ 
                        $selectedOptions = json_decode($carinfo->$key);
                        $multiple = (isset($field['multiple']) && $field['multiple']==TRUE)?'multiple':'';
                        $multiplename = (isset($field['multiple']) && $field['multiple']==TRUE)?'[]':'';
                        ?>
                        <select name="<?php echo $key.$multiplename;?>" id="<?php echo $key;?>" <?php echo $required; ?> class="<?php echo $field['class']; ?>" <?php echo $multiple;?>>
                            <option value="">Select <?php echo $field['label']; ?></option>
                            <?php 
                                if(isset($fields_attributes[$key]) && is_array($fields_attributes[$key])){
                                    
                                    foreach ($fields_attributes[$key] as $option){ 
                                    if(isset($option['title'])){ 
                                        if((is_array($selectedOptions) && in_array($option['id'],$selectedOptions)) || ($option['id']==$selectedOptions)) {?>
                                            <option value="<?php echo $option['id']?>" selected="selected"><?php echo $option['title']?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $option['id']?>"><?php echo $option['title']?></option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } 
                                }else{  ?>
                                       <option value="">Attributes options not set for <?php echo $field['label']; ?></option>
                                <?php } ?>
                        </select>
                    <?php } else if($field['type']=='radio' && isset ($fields_attributes[$key])){ 
                        foreach ($fields_attributes[$key] as $option){ 
                            if($option['id']==$carinfo->$key){ ?>
                                <!-- <input type="radio" name="<?php echo $key?>" id="<?php echo $key.$option['id']?>" <?php echo $required;?> checked="checked" value="<?php echo $option['id']?>"> -->
                            <?php }else if(isset ($field['default']) && $field['default']==$option['id']){ ?>
                                <!-- <input type="radio" name="<?php echo $key?>" id="<?php echo $key.$option['id']?>" <?php echo $required;?> checked="checked" value="<?php echo $option['id']?>"> -->
                            <?php } else { ?>
                                <!-- <input type="radio" name="<?php echo $key?>" id="<?php echo $key.$option['id']?>" <?php echo $required;?> value="<?php echo $option['id']?>"> -->
                            <?php } ?>
                            <span class="attr_title"><?php echo $option['title']?></span>
                    <?php } 
                        } else if($field['type']=='checkbox' && isset ($fields_attributes[$key])){ 
                            $checkedoption = json_decode($carinfo->$key); 
                            //print_r($checkedoption);
                            if(!is_array($checkedoption) || count($checkedoption)<1){
                              $checkedoption[] = $field['default'];
                            }
                            //$field['default'];
                            foreach ($fields_attributes[$key] as $option){ 
                                if((is_array($checkedoption) && in_array($option['id'],$checkedoption))) { ?>
                                    <!-- <input type="checkbox" name="<?php echo $key?>[]" id="<?php echo $key.$option['id']?>" checked="checked" value="<?php echo $option['id']?>"> -->
                                <?php }else{  ?>
                                    <!-- <input type="checkbox" name="<?php echo $key?>[]" id="<?php echo $key.$option['id']?>" value="<?php echo $option['id']?>"> -->
                                <?php } ?>
                            <!-- <span class="attr_title"><?php echo $option['title']?></span> -->
                        <?php } 
                        } ?>
                </div>
            </div>
        <?php }} ?>
           

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button id="submit" type="submit" class="btn btn-info">Modify</button>           
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="col-md-12" id="formula_box">
      <table class="table table-bordered">
        <thead>
          <th width="20%"># Mile</th>
          <th width="20%">0 - 75</th>
          <th width="20%">76 - 200</th>
          <th width="20%">201 - 500</th>
          <th width="20%">501 - Above</th>
        </thead>
        <tr>
          <td>HW Rate</td>
          <td class="active"><input type="text" name="hwr[]" class="form-control hwr" id="hwr1" value=""/></td>
          <td class="success"><input type="text" name="hwr[]" class="form-control hwr" id="hwr2" value=""/></td>
          <td class="warning"><input type="text" name="hwr[]" class="form-control hwr" id="hwr3" value=""/></td>
          <td class="danger"><input type="text" name="hwr[]" class="form-control hwr" id="hwr4" value=""/></td>
        </tr>
        <tr>
          <td>Fual Rate</td>
          <td class="active"><input type="text" name="fu[]" class="form-control" id="fu1" value=""/></td>
          <td class="success"><input type="text" name="fu[]" class="form-control" id="fu2" value=""/></td>
          <td class="warning"><input type="text" name="fu[]" class="form-control" id="fu3" value=""/></td>
          <td class="danger"><input type="text" name="fu[]" class="form-control" id="fu4" value=""/></td>
        </tr>
        <tr>
          <td>Extra Night</td>
          <td class="active"><input type="text" name="en[]" class="form-control" id="en1" value=""/></td>
          <td class="success"><input type="text" name="en[]" class="form-control" id="en2" value=""/></td>
          <td class="warning"><input type="text" name="en[]" class="form-control" id="en3" value=""/></td>
          <td class="danger"><input type="text" name="en[]" class="form-control" id="en4" value=""/></td>
        </tr>
       

      </table>
    </div>
      <!-- /.box -->
    </div>
   
         <?php echo form_close(); ?>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
    $(document).ready(function(){
      $('#formula_box').hide();
            $("#addattr-form").validate({
              rules: {
                                    title: "required",

                                    /*user_state  : "required",
                                    user_city   : "required",*/
              },
              messages: {
                    user_fname  : "Please enter Title",
              },
              errorClass: "my-error-class",
              errorElement: "span", // default is 'label'
              errorPlacement: function(error, element) {
                error.insertAfter(element);
              },
            });


    function get_model(maker){
      var model = <?php echo $mdl = $carinfo->model == ''?0:$carinfo->model;?>;
      if(maker != ''){
        $.ajax({
          type: 'POST',
          url: "<?php echo site_url(CRM_VAR.'/get_models')?>",
          data: {maker:maker,model:model},
          dataType: "text",
          success: function(resultData) { $("#model").html(resultData); }
        });
      }
    }
    get_model($("#maker").val())
    $("#maker").change(function(){
      get_model($(this).val());

    });
    $('#model').change(function(){
      // console.log($(this).val());
      $.get("<?= site_url(CRM_VAR.'/formula') ?>/"+$(this).val(),function(data){
        if(data!="null"){

          var obj=$.parseJSON(data);
          // $.each(obj,function(k,v){
            if(typeof obj['hightway_rate'] !== 'undefined'){

              $('.hwr').each(function(k,v){
                // ;
                $('#hwr'+(k+1)).val($.parseJSON(obj['hightway_rate'])[k]);
                $('#fu'+(k+1)).val($.parseJSON(obj['fuel_rate'])[k]);
                $('#en'+(k+1)).val($.parseJSON(obj['extra_night'])[k]);
              });
            }
          // });
        }else{
          $('.hwr').each(function(k,v){
            $('#hwr'+(k+1)).val("");
            $('#fu'+(k+1)).val("");
            $('#en'+(k+1)).val("");
          });
        }
      });
      $('#formula_box').show();
    });
	});


</script>
