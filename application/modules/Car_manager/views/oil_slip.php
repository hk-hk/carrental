<?php
if(!$isEdit){
$tab1='active';
$tab2='';
}else{
$tab1='';
$tab2='active';
}
// print_r($maintain_detail);
?>
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="<?= $tab1 ?>"><a href="#tab_1" data-toggle="tab">Records List</a></li>
          <li class="<?= $tab2 ?>"><a href="#tab_2" data-toggle="tab">Fuel Expense Information</a></li>
        </ul>
        <div class="tab-content">
          <!-- general form elements -->
          <!-- <div class="box box-info"> -->
          <div class="tab-pane <?= $tab1 ?>" id="tab_1">
            <form id="form-filter" class="form-horizontal">
              
              
              <?php if($role !='supplier') { ?>
              <div class="form-group">
                <label for="user_id" class="col-sm-2 control-label">Supplier</label>
                <div class="col-sm-4">
                  <select name="car_id" id="car_id" class="form-control">
                        <option value="0">Select Car</option>
                        <?php
                        foreach($car_tags as $val){ ?>
                        <option value="<?php echo $val->car_id; ?>"><?php echo $val->car_tag; ?></option>
                        <?php } ?>
                      </select>
                </div>
              </div>
              <?php } ?>
              
              
              <div class="form-group">
                <label for="LastName" class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                  <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                </div>
              </div>
            </form>
            <table id="oiltable" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Date</th>
                  <th>Car Tag</th>
                  <th>Shop Name</th>
                  <th>Car Operator Name</th>
                  <th>Liter</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Type Of Use</th>
                  <th>Description</th>
                  <th>Comments</th>
                  <th>Action</th>

                </tr>
              </thead>
              <tbody>
              </tbody>
              
            </table>
          </div>
          <div class="tab-pane <?= $tab2 ?>" id="tab_2">
            <!-- /.box-header -->
            <!-- form start -->
            <?php
            $site_url= ($isEdit)? '/update_oil_slip_rec/'.$oid : '/save_oil_slip_rec/';
            ?>
            <form action="<?php echo site_url(CRM_VAR.$site_url) ?>" class="form-horizontal" method="post">
              <div class="box-body">
                <!-- <h3 class="box-title">Filter</h3> -->

                <div class="col-md-6">

                  <div class="to-disabled">
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                    <div class="col-md-8">
                      <!-- <input type="text" class="form-control" id="car_owner" disabled> -->
                      <select name="user_id" id="user_id" class="form-control selectpicker" data-live-search="true" required="">
                        <option value="">Select Supplier</option>
                        <?php
                        foreach ($suppliers as $option){
                        $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                        <?php if (!empty($oil_detail) && $car_detail->user_id==$option->id): ?>
                          
                          <option value="<?php echo $option->id?>" selected><?php echo $optionText ?></option>
                        <?php else: ?>
                          <option value="<?php echo $option->id?>" ><?php echo $optionText ?></option>
                        <?php endif ?>
                        <?php } ?>
                      </select>
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Car Tag</label>
                    <div class="col-md-8">
                      <!-- <input type="text" class="form-control" id="title"> -->
                      <select name="car_tag" id="car_tag" class="form-control" required="">
                        <option value="">Select Car</option>
                        <?php
                        foreach($car_tags as $val){ ?>
                          <?php if (!empty($oil_detail->car_id) && $oil_detail->car_id==$val->car_id): ?>
                            <option value="<?php echo $val->car_id; ?>" selected><?php echo $val->car_tag; ?></option>
                          <?php else: ?>
                            <option value="<?php echo $val->car_id; ?>"><?php echo $val->car_tag; ?></option>
                          <?php endif ?>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                 
                  
                 
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Car Operator Name</label>
                    <div class="col-md-8">
                      <select name="car_operator_name" id="car_operator_name" class="form-control selectpicker" data-live-search="true" required="">
                        <option value="">Select Car Operator</option>}
                        option
                      <?php foreach ($drivers as $key => $value): ?>
                        <?php if (!empty($oil_detail->operator_name) && $oil_detail->operator_name==$value->user_lname): ?>
                          <option value="<?= $value->user_lname ?>" selected><?= $value->user_lname ?></option>
                        <?php else: ?>
                          <option value="<?= $value->user_lname ?>"><?= $value->user_lname ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
                      </select>
                      <span><a href="<?=site_url(CRM_VAR . '/adduser?redirect='.site_url(CRM_VAR.'/oil_slip'))?>" >Add New Driver</a></span>
                      <!-- <input type="text" class="form-control" name="car_operator_name" required="" value="<?= !empty($oil_detail->operator_name)? $oil_detail->operator_name : '' ?>"> -->
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Date</label>
                    <div class="col-md-8">
                      <!-- <input type="text" class="form-control" id="title"> -->
                      <input type="text" class="form-control datepicker" placeholder="Choose Date" name="slip_date" id="slip_date" value="<?= !empty($oil_detail->date)? date('m/d/Y',$oil_detail->date) : '' ?>" required="" />
                    </div>
                  </div>
                   <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Gas Station</label>
                    <div class="col-md-8">
                      <select name="shop_name" class="form-control" required="">
                        <option value="">Select Gas Station</option>
                        <?php foreach ($gas_tations as $key => $value): ?>
                          <?php if (!empty($oil_detail) && $value->title==$oil_detail->shop_name): ?>
                            
                            <option value="<?= $value->title ?>" selected><?= $value->title ?></option>
                          <?php else: ?>
                            <option value="<?= $value->title ?>"><?= $value->title ?></option>
                          <?php endif ?>
                        <?php endforeach ?>
                      </select>
                      <!-- <input type="text" class="form-control" name="shop_name" required="" value="<?= !empty($oil_detail->shop_name)? $oil_detail->shop_name : '' ?>"> -->
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Purchased By</label>
                    <div class="col-md-8">
                      <select class="form-control" name="purchased_by" required="" id='purchased_by'>
                        <option value="">Select Purchased By</option>
                        <option value="cash" <?= (!empty($oil_detail) && $oil_detail->purchased_by=="cash")? 'selected' : '' ?>>Cash</option>
                        <option value="credit" <?= (!empty($oil_detail) && $oil_detail->purchased_by=="credit")? 'selected' : '' ?>>Credit</option>
                      </select>
                      <!-- <input type="text" class="form-control" name="type_of_use" value="<?= !empty($oil_detail->type_of_use)? $oil_detail->type_of_use : '' ?>"> -->
                    </div>
                  </div>
                  
                  <div class="form-group" id="slip_no_dev">
                    <label for="FirstName" class="col-md-4 control-label">Slip No</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="slip_no" id="slip_no" required="" value="<?= !empty($oil_detail->slip_no)? $oil_detail->slip_no : '' ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Liter</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="liter" required="" value="<?= !empty($oil_detail->liter)? $oil_detail->liter : '' ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Amount</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control number-format" name="amount" required="" value="<?= !empty($oil_detail->amount)? $oil_detail->amount : '' ?>">
                    </div>
                  </div>

                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Status</label>
                    <div class="col-md-8">
                      <?php if (!empty($oil_detail->status) && $oil_detail->status=="complete"): ?>
                        <p class="form-control">Complete</p>
                       <?php else: ?>
                           <select class="form-control" name="status" required="" id='status'>
                        <!-- <option value="">Select Status</option> -->
                        <option value="pending">Pending</option>
                        <option value="complete">Complete</option>
                      </select>
                       <?php endif ?>


                     
                      <!-- <input type="text" class="form-control" name="type_of_use" value="<?= !empty($oil_detail->type_of_use)? $oil_detail->type_of_use : '' ?>"> -->
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Purchased By</label>
                    <div class="col-md-8">
                      <input type="radio" name="status" value="cash" id="Cash" checked=""> <label for="Cash">Cash</label>
                      <input type="radio" name="status" value="credit" id="Credit"> <label for="Credit">Credit</label>
                    </div>
                  </div> -->
                  
                  <!-- <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Unpaid</label>
                    <div class="col-md-8">
                      <input type="checkbox" id="title">
                    </div>
                  </div> -->
                </div>
                <div class="col-md-6">
                  <div class="to-disabled">
                  
                  
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Maker</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" id="vendor" disabled>
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Car Type</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" id="car_type" disabled>
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Type Of Use</label>
                    <div class="col-md-8">
                      <?php if (!empty($oil_detail)): ?>
                        <input type="hidden" value="<?= ($oil_detail->type_of_use=="office_use")? 'office_use' : 'order' ?>">
                      <?php else: ?>
                        <input type="hidden" value="office_use">
                      <?php endif ?>
                      <select class="form-control" name="type_of_use" required="" id='type_of_use'>
                        <!-- <option value="">Select Type of use</option> -->
                        <option value="office_use" <?= (!empty($oil_detail) && $oil_detail->type_of_use=="office_use")? 'selected' : '' ?>>Office Use</option>
                        <option value="order" <?= (!empty($oil_detail) && $oil_detail->type_of_use=="order")? 'selected' : '' ?>>Order</option>
                      </select>
                      <!-- <input type="text" class="form-control" name="type_of_use" value="<?= !empty($oil_detail->type_of_use)? $oil_detail->type_of_use : '' ?>"> -->
                    </div>
                  </div>
                  <div class="form-group" id="order_id_dev" style="display: none">
                    <label for="FirstName" class="col-md-4 control-label">Order ID</label>
                    <div class="col-md-8">
                      <input type="number" name="order_id" class="form-control" id="order_id" required="" value="<?= !empty($oil_detail->order_id)? $oil_detail->order_id : '' ?>"  readonly>
                      
                      <span><a href="<?= !empty($oil_detail->order_id)? site_url().'admin/order/view/'.$oil_detail->order_id : '' ?>">Go to order detail</a></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Description</label>
                    <div class="col-md-8">
                      <textarea class="form-control" name="description" id="description" required=""><?= !empty($oil_detail->description)? $oil_detail->description : '' ?></textarea>
                      <!-- <input type="text" class="form-control" id="title"> -->
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Comments</label>
                    <div class="col-md-8">
                      <textarea class="form-control" name="comments" ><?= !empty($oil_detail->comments)? $oil_detail->comments : '' ?></textarea>
                      <!-- <input type="text" class="form-control" id="title"> -->
                    </div>
                  </div>
                  <?php if ($isEdit): ?>
                    <div class="form-group">
                      <div class="col-xs-12 col-md-12">
                        <button type="button" class="btn btn-info pull-right" onclick="create_new('<?php echo site_url(CRM_VAR.'/oil_slip')?>')">Create New Record</button>
                      </div>
                    </div>
                  <?php endif ?>
                  </div>
                </div>
                
                <!-- <div class="table-responsive">
                  <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Number</th>
                        <th>Maker</th>
                        <th>Model</th>
                        <th>Details</th>
                        <th>No. Of Passenger</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    
                    <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Number</th>
                      <th>Maker</th>
                      <th>Model</th>
                      <th>Details</th>
                      <th>No. Of Passenger</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                    </tfoot>
                  </table>
                </div> -->
              </div>
              <div class="box-footer">

                <button id="submit" type="submit" class="btn btn-info pull-right" onclick="return confirm('Do you want <?= ($isEdit)? 'Update' : 'Save' ?>?')"><?= ($isEdit)? 'Update' : 'Save' ?></button>
              </div>
            </form>
          </div>
          
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
    </div>
    <!-- /.row -->
  </div>
</section>

<?php if(TRUE){?>
<script type="text/javascript">
var table;
$(document).ready(function() {
  table = $('#oiltable').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/ajax_list_oil_slip')?>",
            "type": "POST",
            "data": function ( data ) {         
                data.car_id = $('#car_id').val();            
                // data.car_type = $('#fcar_type').val();              
                // data.maker = $('#maker').val();            
            }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,11 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
    
    var vendor_text="";
    var car_tag="";
    var car_operator_name="";
    var get_car_tag="<?= (!empty($maintain_detail->car_id))? $maintain_detail->car_id : '' ?>";

    function description_add_text(){
      $('#description').val(vendor_text+"\n"+car_tag+"\n"+car_operator_name+"\n");
    }

    $('#user_id').change(function(){
        // console.log(this.val());
        $("#user_id option:selected" ).each(function() {
          // console.log("$(this).val()");
          if($( "#user_id option:selected" ).val()!=""){
            vendor_text="Vendor : "+$(this).html();
          }else{
            vendor_text="";
          }
          description_add_text();
          $.get("<?php echo site_url(CRM_VAR.'/related_car_tag/')?>"+$(this).val(),function(data){
            var html="";
            html="<option value=''>Select Car</option>";
            var obj=$.parseJSON(data);
            $.each(obj,function(k,v){
              html+="<option value='"+v['id']+"'>"+v['car_number']+"</option>";
            });
              // console.log(html);
            $('#car_tag').html(html);
          });
        });
      });
    function load(){
      // console.log(this.val());
      $( "#car_tag option:selected" ).each(function() {

        $.post("<?php echo site_url(CRM_VAR.'/car_detail')?>",{'car_tag':$(this).val()},function(data){
          // console.log(data);
          var obj=$.parseJSON(data);
          $.each(obj,function(k,v){
            // console.log(k);
            $('#vendor').val(v[1]);
            $('#car_type').val(v[2]);
            $('#car_owner').val(v[3]);
          });
        });
      });
    }
    $('#order_id_dev').hide();
    $('#order_id').attr("disabled",true);

    function selecttypeuse(val){
      console.log(val);
      if(val=="order"){
        // $('#order_id_dev').show();
        $('#order_id').attr("disabled",false);
      }else{
        $('#order_id_dev').hide();
        $('#order_id').attr("disabled",true);
        // $('#order_id').val("");
      }
    }
    selecttypeuse($("#type_of_use option:selected").val());
    $( "#type_of_use" ).change(function(){
        selecttypeuse($(this).val());
    });

    function selectslip(val){
      if(val=="credit"){
        $('#slip_no_dev').show();
        $('#slip_no').attr("disabled",false);
      }else{
        $('#slip_no_dev').hide();
        $('#slip_no').attr("disabled",true);
        // $('#slip_no').val("");
      }
    }
    $('#slip_no_dev').hide();
    $('#slip_no').attr("disabled",true);
    selectslip($("#purchased_by option:selected").val());
    $( "#purchased_by" ).change(function(){
      selectslip($(this).val());
    });
    $('#car_operator_name').change(function(){
      if($( "#car_operator_name option:selected" ).val()!=""){
          car_operator_name="Car Operator Name : "+$( "#car_operator_name option:selected" ).html();
        }else{
          car_operator_name="";
        }
        description_add_text();
    });
    $('#car_tag').change(function(){
    // console.log($(this).val());
      if($( "#car_tag option:selected" ).val()!=""){
        car_tag="Car Tag : "+$( "#car_tag option:selected" ).html();
      }else{
        car_tag="";
      }
      description_add_text();
      load();
    });

    $('.datepicker').datepicker({
        autoclose: true
    });
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    <?php if ($isEdit): ?>
      load();
    <?php endif ?>
    <?php if (!empty($oil_detail->status) && $oil_detail->status=="complete"): ?>
      $('.to-disabled input').attr('disabled',true);
      $('.to-disabled select').attr('disabled',true);
      $('.to-disabled textarea').attr('disabled',true);
      $('.box-footer').hide();
    <?php endif ?>
});
</script>
<?php } ?>