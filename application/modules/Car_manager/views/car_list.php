<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Cars</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    <?php //print_r($parent_attributes);?>
          <div class="box-body">
          <h3 class="box-title">Filter</h3>
          <form id="form-filter" class="form-horizontal">
                   
                    <div class="form-group">
                        <label for="FirstName" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="title">
                        </div>
                    </div>
                    <?php if($role !='supplier') { ?>
                    <div class="form-group">
                      <label for="user_id" class="col-sm-2 control-label">Supplier</label>
                      <div class="col-sm-4">
                          <select name="user_id" id="user_id" class="form-control">
                                  <option value="">Select Supplier</option>
                                  <?php  
                                      foreach ($suppliers as $option){ 
                                          $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname;
                                          if($option->id== $carinfo->user_id) {  ?>
                                              <option value="<?php echo $option->id?>" selected="selected"><?php echo $optionText?></option>
                                          <?php }else{ ?>
                                              <option value="<?php echo $option->id?>"><?php echo $optionText ?></option>
                                          <?php } ?>
                                  <?php } ?>
                              </select>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="maker" class="col-sm-2 control-label">Maker</label>
                        <div class="col-sm-4">
                          <select name="maker" id="maker" class="form-control">
                              <option value="0">Select Maker</option>
                            <?php
                            foreach($car_attributes[45] as $val){ ?>
                                <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fuel_type" class="col-sm-2 control-label">Fuel Type</label>
                        <div class="col-sm-4">
                          <select name="fuel_type" id="fuel_type" class="form-control">
                              <option value="0">Select Fuel Type</option>
                            <?php
                            foreach($car_attributes[3] as $val){ ?>
                                <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="car_type" class="col-sm-2 control-label">Car Type</label>
                        <div class="col-sm-4">
                          <select name="car_type" id="car_type" class="form-control">
                              <option value="0">Select Car Type</option>
                            <?php
                            foreach($car_attributes[4] as $val){ ?>
                                <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                            <?php }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="rent_type" class="col-sm-2 control-label">Rent Type</label>
                        <div class="col-sm-4">
                          <select name="rent_type" id="rent_type" class="form-control">
                              <option value="0">Select Rent Type</option>
                            <?php
                            foreach($car_attributes[35] as $val){ ?>
                                <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                            <?php }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="no_of_passanger" class="col-sm-2 control-label">No Of Passenger</label>
                        <div class="col-sm-4">
                          <select name="no_of_passanger" id="no_of_passanger" class="form-control">
                              <option value="0">Select No Of Passenger</option>
                            <?php
                            foreach($car_attributes[42] as $val){ ?>
                                <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                            <?php }
                            ?>
                          </select>
                        </div>
                      </div>
                   
                    <div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                            <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </form>
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Number</th>
                    <th>Maker</th>
                    <th>Model</th>
                    <th>Details</th>
                    <th>No. Of Passenger</th>
		    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Number</th>
                    <th>Maker</th>
                    <th>Model</th>
                    <th>Details</th>
                    <th>No. Of Passenger</th>
		    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
        </div>
          </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<?php if(TRUE){?>
<script type="text/javascript">
 
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/car_list_ajax')?>",
            "type": "POST",
            "data": function ( data ) {
                data.fuel_type = $('#fuel_type').val();            
                data.title = $('#title').val();            
                data.user_id = $('#user_id').val();            
                data.car_type = $('#car_type').val();            
                data.rent_type = $('#rent_type').val();            
                data.maker = $('#maker').val();            
                data.no_of_passanger = $('#no_of_passanger').val();            
                console.log(data);
            }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,7 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
    $(document).on("click",".change-status" ,function() {
        var status = $(this).attr("data_status");
        if(status=='2'){ 
            var chk_c = $(this).attr("data-id");
        }else{
            var chk_p = $(this).parent().attr("data-parent");
            var chk_c = $(this).attr("data-id");
            var status = $(this).attr("data_status");						
            if (chk_p === chk_c) {
                if ($( this ).hasClass( "btn-danger" ) ) {
                    $( this ).removeClass("btn-danger");
                    $( this ).addClass("btn-success");
                }else {
                    $( this ).addClass("btn-danger");
                    $( this ).removeClass("btn-success");
                }
            }
            
        } 
        $.ajax({
                url:"<?php echo site_url(CRM_VAR.'/ajax_update_car_status/')?>"+chk_c+"/"+status,
                data:"",
                type:"GET",
            success:function (data) {
                    alert(data);
            //table.ajax.reload();
            }
        });
               
    }); 
    
    
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });
    
});
 
</script>
<?php } ?>
