<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Car Detail</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="form-horizontal" >
          <div class="box-body">
            <!-- <h3 class="box-title">Filter</h3> -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="FirstName" class="col-md-4 control-label">Car Tag</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" id="car_tag" disabled="">
                </div>
              </div>
              <div class="form-group">
                <label for="FirstName" class="col-md-4 control-label">Maker</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" id="vendor" disabled>
                  
                </div>
              </div>
              
              
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="FirstName" class="col-md-4 control-label">Car Type</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" id="car_type" disabled>
                  
                </div>
              </div>
              <div class="form-group">
                <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" id="car_owner" disabled>
                  
                </div>
              </div>
            </div>
            
          </div>
          
        </div>
        <div class="box-footer">
          <a href="<?= site_url(CRM_VAR.'/add_car/'.$carId) ?>" class="btn btn-info pull-right">Edit</a>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Maintenances</h3>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>No</th>
                    <th>Car Tag</th>
                    <th>Maker</th>
                    <th>Vendor</th>
                    <th>Date</th>
                    <th>Workshop Name</th>
                    <th>Description</th>
                    <th>Comments</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Car Tag</th>
                  <th>Maker</th>
                  <th>Vendor</th>
                  <th>Date</th>
                  <th>Workshop Name</th>
                  <th>Description</th>
                  <th>Comments</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Oil Slips</h3>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
            <table id="oiltable" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Date</th>
                  <th>Car Tag</th>
                  <th>Shop Name</th>
                  <th>Car Operator Name</th>
                  <th>Liter</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Type Of Use</th>
                  <th>Comments</th>
                  <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody>
              </tbody>
              
              <tfoot>
              <tr>
                <th>No</th>
                <th>Date</th>
                <th>Car Tag</th>
                <th>Shop Name</th>
                <th>Car Operator Name</th>
                <th>Liter</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Type Of Use</th>
                <th>Comments</th>
                <!-- <th>Action</th> -->
              </tr>
              </tfoot>
              <!-- <thead>
                <tr>
                  <th>No</th>
                  <th>Date</th>
                  <th>Slip No</th>
                  <th>Shop Name</th>
                  <th>Car Operator Name</th>
                  <th>Liter</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Type Of Use</th>
                  <th>Comments</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              
              <tfoot>
              <tr>
                <th>No</th>
                <th>Date</th>
                <th>Slip No</th>
                <th>Shop Name</th>
                <th>Car Operator Name</th>
                <th>Liter</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Type Of Use</th>
                <th>Comments</th>
              </tr>
              </tfoot> -->
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box -->
  </div>
  <!--/.col (left) -->
</div>
<!-- /.row -->
</section>
<?php if(TRUE){?>
<script type="text/javascript">
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/ajax_list_car_maintain')?>",
            "type": "POST",
            "data" : {'car_id':<?= $carId ?>}
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,6 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
    table = $('#oiltable').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/ajax_list_car_oil_slip')?>",
            "type": "POST",
            "data" : {'car_id':<?= $carId ?>}
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,9 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
    // $('#car_tag').change(function(){
      // console.log(this.val());
      // $( "select option:selected" ).each(function() {
        // console.log($(this).val());
        $.post("<?php echo site_url(CRM_VAR.'/car_detail')?>",{'car_tag':<?= $carId ?>},function(data){
          // console.log(data);
          var obj=$.parseJSON(data);
          $.each(obj,function(k,v){
            // console.log(k);
            $('#car_tag').val(v[0]);
            $('#vendor').val(v[1]);
            $('#car_type').val(v[2]);
            $('#car_owner').val(v[3]);
          });
        });
      // });
    // });

    $('.datepicker').datepicker({
        autoclose: true
    });
});
</script>
<?php } ?>