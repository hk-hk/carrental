<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-7">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">
            <?php if(!empty($carinfo)) { echo 'Edit Car Form'; }
            else { echo 'Add Car Form'; } ?>
            </h3>
            <a style="float: right;" href="<?php echo site_url(CRM_VAR.'/car_list');?>">Back to list</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
				
	    <?php $attributes = array('id' => 'addcar-form','class'=>'form-horizontal'); ?>
			<?php if(!empty($carinfo)) { echo form_open_multipart(CRM_VAR.'/add_car/'.$carinfo->id,$attributes); }
                            else { echo form_open_multipart(CRM_VAR.'/add_car',$attributes); } ?>
	  
      <?php echo validation_errors();
      //print_r($carinfo);die;
      ?>
          <div class="box-body">
            <?php if($role !='supplier') { ?>
              <div class="form-group">
                <label for="user_id" class="col-sm-2 control-label">Select Supplier</label>
                <div class="col-sm-10">
                    <select name="user_id" id="user_id" class="form-control">
                            <option value="">Select Supplier</option>
                            <?php  
                                foreach ($suppliers as $option){ 
                                    $companyName = ($option->user_company!='')?$option->user_company:$option->user_fname;
                                    if($option->id== $carinfo->user_id) {?>
                                        <option value="<?php echo $option->id?>" selected="selected"><?php echo $companyName;?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $option->id?>"><?php echo $companyName; ?></option>
                                    <?php } ?>
                            <?php } ?>
                        </select>
                </div>
            </div>
        <?php }else{ ?>
              <input type="hidden" name="user_id" value="<?php echo $carinfo->user_id;?>" />
        <?php } ?>
        <?php foreach ($formFields as $key=>$field){ 
                $required = (isset($field['required']) && $field['required'])?'required':'';
            ?>
            <div class="form-group">
                <label for="<?php echo $key; ?>" class="col-sm-2 control-label"><?php echo $field['label']; ?></label>
                <div class="col-sm-10">
                    <?php if(in_array($field['type'],array('text','email','password'))){ ?>
                        <input type="<?php echo $field['type'];?>" class="<?php echo $field['class']; ?>" <?php echo $required; ?> id="<?php echo $key; ?>" name="<?php echo $key; ?>" 
                               placeholder="<?php echo $field['placeholder']; ?>" value='<?php echo set_value($key,$carinfo->$key);?>'>
                    <?php }else if($field['type']=='number'){ ?>
                        <input type="<?php echo $field['type'];?>"  step='0.01' class="<?php echo $field['class']; ?>" <?php echo $required; ?> id="<?php echo $key; ?>" name="<?php echo $key; ?>" 
                               placeholder="<?php echo $field['placeholder']; ?>" value='<?php echo set_value($key,$carinfo->$key);?>'>
                    <?php } else if($field['type']=='select'  && isset ($fields_attributes[$key])){ 
                        $selectedOptions = json_decode($carinfo->$key);
                        $multiple = (isset($field['multiple']) && $field['multiple']==TRUE)?'multiple':'';
                        $multiplename = (isset($field['multiple']) && $field['multiple']==TRUE)?'[]':'';
                        ?>
                        <select name="<?php echo $key.$multiplename;?>" id="<?php echo $key;?>" <?php echo $required; ?> class="<?php echo $field['class']; ?>" <?php echo $multiple;?>>
                            <option value="">Select <?php echo $field['label']; ?></option>
                            <?php 
                                if(isset($fields_attributes[$key]) && is_array($fields_attributes[$key])){
                                    
                                    foreach ($fields_attributes[$key] as $option){ 
                                    if(isset($option['title'])){ 
                                        if((is_array($selectedOptions) && in_array($option['id'],$selectedOptions)) || ($option['id']==$selectedOptions)) {?>
                                            <option value="<?php echo $option['id']?>" selected="selected"><?php echo $option['title']?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $option['id']?>"><?php echo $option['title']?></option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } 
                                }else{  ?>
                                       <option value="">Attributes options not set for <?php echo $field['label']; ?></option>
                                <?php } ?>
                        </select>
                    <?php } else if($field['type']=='radio' && isset ($fields_attributes[$key])){ 
                        foreach ($fields_attributes[$key] as $option){ 
                            if($option['id']==$carinfo->$key){ ?>
                                <input type="radio" name="<?php echo $key?>" id="<?php echo $key.$option['id']?>" <?php echo $required;?> checked="checked" value="<?php echo $option['id']?>">
                            <?php }else if(isset ($field['default']) && $field['default']==$option['id']){ ?>
                                <input type="radio" name="<?php echo $key?>" id="<?php echo $key.$option['id']?>" <?php echo $required;?> checked="checked" value="<?php echo $option['id']?>">
                            <?php } else { ?>
                                <input type="radio" name="<?php echo $key?>" id="<?php echo $key.$option['id']?>" <?php echo $required;?> value="<?php echo $option['id']?>">
                            <?php } ?>
                            <span class="attr_title"><?php echo $option['title']?></span>
                    <?php } 
                        } else if($field['type']=='checkbox' && isset ($fields_attributes[$key])){ 
                            $checkedoption = json_decode($carinfo->$key); 
                            //print_r($checkedoption);
                            if(!is_array($checkedoption) || count($checkedoption)<1){
                              $checkedoption[] = $field['default'];
                            }
                            //$field['default'];
                            foreach ($fields_attributes[$key] as $option){ 
                                if((is_array($checkedoption) && in_array($option['id'],$checkedoption))) { ?>
                                    <input type="checkbox" name="<?php echo $key?>[]" id="<?php echo $key.$option['id']?>" checked="checked" value="<?php echo $option['id']?>">
                                <?php }else{  ?>
                                    <input type="checkbox" name="<?php echo $key?>[]" id="<?php echo $key.$option['id']?>" value="<?php echo $option['id']?>">
                                <?php } ?>
                            <span class="attr_title"><?php echo $option['title']?></span>
                        <?php } 
                        } ?>
                </div>
            </div>
        <?php } ?>
              
            <!-- multiple image /*car images by khushboo 1443*/ -->
            <div class="form-group">
                <label for="car_image" class="col-sm-2 control-label">Car Images</label>
                <div class="col-sm-10">
                <input type="file" class="" id="car_image" name="car_image[]" value="" multiple >

                </div>
               
            </div>
            <div class="form-group">
              <label for="car_image" class="col-sm-2 control-label"></label>
               <div class="col-sm-10">
                  <input type="hidden" id="len_old" value="<?php if(isset($carinfo->car_images)){ $old_car_images = json_decode($carinfo->car_images); echo count($old_car_images);}else{echo 0;}?>">
                  <?php if(isset($carinfo->car_images)){ $old_car_images = json_decode($carinfo->car_images);
                           if(!empty($old_car_images)){ $i = 1;
                            foreach ($old_car_images as $key => $value) {
                              ?>
                              <img id="image_<?php echo $key;?>" height="100px" width="100px" src="<?php echo site_url();?>uploads/car/<?php echo $value;?>">
                              <span imagename = "<?php echo $value;?>" id="del_<?php echo $key; ?>" count="<?php echo $key; ?>" class="fa fa-trash delete_image" style="cursor: pointer;"></span>
                              <?php 
                            }
                           }}
                            ?>
                </div>
            </div>
            <!-- multiple image /*car images by khushboo 1443*/ -->
            

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <?php if($carinfo->id>0) { ?> <button id="submit" type="submit" class="btn btn-info">Edit Car</button> <?php }
            else { ?> <button id="submit" type="submit" class="btn btn-info">Add Car</button> <?php } ?>           
          </div>
          <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <?php if($carinfo->id>0){ ?>
    <div class="col-md-5">
      <table class="table table-bordered">
        <thead>
          <th width="20%"># Mile</th>
          <th width="20%">0 - 75</th>
          <th width="20%">76 - 200</th>
          <th width="20%">201 - 500</th>
          <th width="20%">501 - Above</th>
        </thead>
        <?php
          $hwr=json_decode($carinfo->hightway_rate,true);
          $fr=json_decode($carinfo->fuel_rate,true);
          $en=json_decode($carinfo->extra_night,true);
        ?>
        <tr>
          <td>HW Rate</td>
          <td class="active"><input type="text" name="hwr[]" class="form-control" value="<?= $hwr[0] ?>"/></td>
          <td class="success"><input type="text" name="hwr[]" class="form-control" value="<?= $hwr[1] ?>"/></td>
          <td class="warning"><input type="text" name="hwr[]" class="form-control" value="<?= $hwr[2] ?>"/></td>
          <td class="danger"><input type="text" name="hwr[]" class="form-control" value="<?= $hwr[3] ?>"/></td>
        </tr>
        <tr>
          <td>Fual Rate</td>
          <td class="active"><input type="text" name="fr[]" class="form-control" value="<?= $fr[0] ?>"/></td>
          <td class="success"><input type="text" name="fr[]" class="form-control" value="<?= $fr[1] ?>"/></td>
          <td class="warning"><input type="text" name="fr[]" class="form-control" value="<?= $fr[2] ?>"/></td>
          <td class="danger"><input type="text" name="fr[]" class="form-control" value="<?= $fr[3] ?>"/></td>
        </tr>
        <tr>
          <td>Extra Night</td>
          <td class="active"><input type="text" name="en[]" class="form-control" value="<?= $en[0] ?>"/></td>
          <td class="success"><input type="text" name="en[]" class="form-control" value="<?= $en[1] ?>"/></td>
          <td class="warning"><input type="text" name="en[]" class="form-control" value="<?= $en[2] ?>"/></td>
          <td class="danger"><input type="text" name="en[]" class="form-control" value="<?= $en[3] ?>"/></td>
        </tr>
       

      </table>
    </div>

       <?php } ?>
         <?php echo form_close(); ?>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
    $(document).ready(function(){
            $("#addattr-form").validate({
              rules: {
                                    title: "required",

                                    /*user_state  : "required",
                                    user_city   : "required",*/
              },
              messages: {
                    user_fname  : "Please enter Title",
              },
              errorClass: "my-error-class",
              errorElement: "span", // default is 'label'
              errorPlacement: function(error, element) {
                error.insertAfter(element);
              },
            });

    $(".choose-country").change(function() {
        var pcall = $(this);
        $.ajax({
          url:"<?php echo base_url('Tool/getStates'); ?>",
          data:{country:pcall.val()},
          type:'GET',
          dataType:"JSON",
          success:function( response ) {
            $.each(response.records,function(i,s) {
                $("#user_state").append("<option value='"+s.id_state+"'>"+s.name_state+"</option>");
            });
          }
        })
    });
    $(".delete_image").click(function(){
      var res = confirm('Are you sure ?');
      if(res){
        var count = jQuery(this).attr('count');
        var image = jQuery(this).attr('imagename');
        var id = <?php echo $carinfo->id;?>;
        $.ajax({
          type: 'POST',
          url: "<?php echo site_url(CRM_VAR.'/car/delete_image')?>",
          data: {image:image,id:id},
          dataType: "text",
          success: function(resultData) {
            if(resultData){
              $("#del_"+count).hide();
              $("#image_"+count).remove();
              $("#len_old").val($("#len_old").val() - 1);
            }
            else{
              alert("Error in deleting image!");
            }
          }
        });
      }
    });
    $("#car_image").change(function(){
      var len = parseInt(document.getElementById("car_image").files.length) + parseInt(document.getElementById("len_old").value);
      if(len > 10)
      {
         alert("You can not select more than 10 images.");
         $("#car_image").val('');
         return false;
      }
    });

    function get_model(maker){
      var model = <?php echo $mdl = $carinfo->model == ''?0:$carinfo->model;?>;
      if(maker != ''){
        $.ajax({
          type: 'POST',
          url: "<?php echo site_url(CRM_VAR.'/get_models')?>",
          data: {maker:maker,model:model},
          dataType: "text",
          success: function(resultData) { $("#model").html(resultData); }
        });
      }
    }
    get_model($("#maker").val())
    $("#maker").change(function(){
      get_model($(this).val());
    });

	});
</script>
