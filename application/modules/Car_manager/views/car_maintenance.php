<?php
if(!$isEdit){
$tab1='active';
$tab2='';
}else{
$tab1='';
$tab2='active';
}
// print_r($maintain_detail);
?>
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="<?= $tab1 ?>"><a href="#tab_1" data-toggle="tab">Records List</a></li>
          <li class="<?= $tab2 ?>"><a href="#tab_2" data-toggle="tab">Car Maintenance Record Information</a></li>
        </ul>
        <div class="tab-content">
          <!-- /.tab-pane -->
          <div class="tab-pane <?= $tab1 ?>" id="tab_1">
            <form id="form-filter" class="form-horizontal">
              
              
              <?php if($role !='supplier') { ?>
              <div class="form-group">
                <label for="user_id" class="col-sm-2 control-label">Supplier</label>
                <div class="col-sm-4">
                  <select name="user_id" id="user_id" class="form-control selectpicker" data-live-search="true" required="">
                    <option value="">Select Supplier</option>
                    <?php
                    foreach ($suppliers as $option){
                    $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname;?>
                    
                    <option value="<?php echo $option->id?>"><?php echo $optionText ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <?php } ?>
              <div class="form-group">
                <label for="maker" class="col-sm-2 control-label">Maker</label>
                <div class="col-sm-4">
                  <select name="maker" id="maker" class="form-control selectpicker" data-live-search="true">
                    <option value="0">Select Maker</option>
                    <?php
                    foreach($car_attributes[45] as $val){ ?>
                    <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="car_type" class="col-sm-2 control-label">Workshop Name</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="workshop_name">
                </div>
              </div>
              
              <div class="form-group">
                <label for="LastName" class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                  <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                </div>
              </div>
            </form>
            <div class="table-responsive">
              <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Car Tag</th>
                    <th>Maker</th>
                    <th>Vendor</th>
                    <th>Date</th>
                    <th>Workshop Name</th>
                    <th>Description</th>
                    <th>Comments</th>
                    <th>Amount</th>
                    <th>Paid</th>
                    <th>Recieved</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Car Tag</th>
                  <th>Maker</th>
                  <th>Vendor</th>
                  <th>Date</th>
                  <th>Workshop Name</th>
                  <th>Description</th>
                  <th>Comments</th>
                  <th>Amount</th>
                  <th>Paid</th>
                  <th>Recieved</th>
                  <th>Actions</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <div class="tab-pane <?= $tab2 ?>" id="tab_2">

            <!-- general form elements -->
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Car Maintenance Record Information</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <?php
            $site_url= ($isEdit)? '/update_maintain_rec/'.$mid : '/save_maintain_rec/';
            ?>
            <form action="<?php echo site_url(CRM_VAR.$site_url) ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <!-- <h3 class="box-title">Filter</h3> -->
                <div class="col-md-6">
                  <div class="to-disabled">
                    <div class="form-group">
                      <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                      <div class="col-md-8">
                        <!-- <input type="text" class="form-control" id="car_owner" disabled> -->
                        <select name="suser_id" id="suser_id" class="form-control selectpicker" data-live-search="true" required="">
                          <option value="">Select Supplier</option>
                          <?php
                          foreach ($suppliers as $option){
                          $optionText = ($option->user_company!='')?$option->user_company:$option->user_fname; ?>
                          <?php if (!empty($maintain_detail) && $car_detail->user_id==$option->id): ?>
                            <option value="<?php echo $option->id?>" selected><?php echo $optionText ?></option>
                          <?php else: ?>
                          <option value="<?php echo $option->id?>"><?php echo $optionText ?></option>
                          <?php endif ?>
                          <?php } ?>
                        </select>
                        
                      </div>
                    </div>
                    <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Car Tag</label>
                    <div class="col-md-8">
                      <!-- <input type="text" class="form-control" id="title"> -->
                      <select name="car_tag" id="car_tag" class="form-control" required="">
                        <option>Select Car</option>
                        <?php
                        foreach($car_tags as $val){ ?>
                        <?php if (!empty($maintain_detail->car_id) &&  $maintain_detail->car_id==$val->car_id): ?>
                        <option value="<?php echo $val->car_id; ?>" selected><?php echo $val->car_tag; ?></option>
                        <?php else: ?>
                        <option value="<?php echo $val->car_id; ?>"><?php echo $val->car_tag; ?></option>
                        <?php endif ?>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                    <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Repaired By</label>
                    <div class="col-md-8">
                      <select name="repaired_by" id="repaired_by" class="form-control selectpicker" data-live-search="true" required="">
                        <option value="">Select Repaired By</option>
                      <?php foreach ($drivers as $key => $value): ?>
                        <?php if (!empty($maintain_detail->repaired_by) && $maintain_detail->repaired_by==$value->user_lname): ?>
                          <option value="<?= $value->user_lname ?>" selected><?= $value->user_lname ?></option>
                        <?php else: ?>
                          <option value="<?= $value->user_lname ?>"><?= $value->user_lname ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
                      </select>
                      <span><a href="<?=site_url(CRM_VAR . '/adduser?redirect='.site_url(CRM_VAR.'/car_maintenance'))?>" >Add New Driver</a></span>
                      <!-- <input type="text" class="form-control" name="repaired_by" value="<?= !empty($maintain_detail->repaired_by)? $maintain_detail->repaired_by : '' ?>"> -->
                      
                    </div>
                  </div>
                    
                    <div class="form-group">
                      <label for="FirstName" class="col-md-4 control-label">Maintainence Date</label>
                      <div class="col-md-8">
                        <!-- <input type="text" class="form-control" id="title"> -->
                        <input type="text" class="form-control datepicker" placeholder="Choose Date" name="maintain_date" id="maintain_date" value="<?= !empty($maintain_detail->date)? date('m/d/Y',$maintain_detail->date) : '' ?>" required="" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="FirstName" class="col-md-4 control-label">Workshop Name</label>
                      <div class="col-md-8">
                        <select name="workshop_name" class="form-control" required="">
                          <option value="">Select Workshop</option>
                          <?php foreach ($workshops as $key => $value): ?>
                            <?php if (!empty($maintain_detail) && $maintain_detail->workshop_name==$value->title): ?>
                              <option value="<?= $value->title ?>" selected><?= $value->title ?></option>  
                            <?php else: ?>

                              <option value="<?= $value->title ?>"><?= $value->title ?></option>
                            <?php endif ?>
                          <?php endforeach ?>
                        </select>
                        <!-- <input type="text" class="form-control" name="workshop_name" value="<?= !empty($maintain_detail->workshop_name)? $maintain_detail->workshop_name : '' ?>"> -->
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="FirstName" class="col-md-4 control-label">Amount</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control number-format" name="amount" value="<?= !empty($maintain_detail->amount)? $maintain_detail->amount : '' ?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Payment To Workshop</label>
                    <div class="col-md-8">
                       <?php if (!empty($maintain_detail->paid) && $maintain_detail->paid=="complete"): ?>
                        <p class="form-control">Complete</p>
                        <input type="hidden" name="paid_to_workshop" value="complete">
                       <?php else: ?>
                        <select name="paid_to_workshop" class="form-control" <?= (!empty($maintain_detail->paid) && $maintain_detail->paid=="complete")? 'readonly' : '' ?>>
                          <option value="pending">pending</option>
                          <option value="complete" <?= (!empty($maintain_detail->paid) && $maintain_detail->paid=="complete")? 'selected' : '' ?>>Complete</option>
                        </select>
                       <?php endif ?>
                    </div>
                  </div>
                  <div class="form-group" id="recieved_from_vendor">
                    <label for="FirstName" class="col-md-4 control-label">Recieved From Vendor</label>
                    <div class="col-md-8">
                      <?php if (!empty($maintain_detail->recieved) && $maintain_detail->recieved=="complete"): ?>
                        <p class="form-control">Complete</p>
                        <input type="hidden" name="recieved_from_vendor" value="complete">
                       <?php else: ?>
                          <select name="recieved_from_vendor" class="form-control" <?= (!empty($maintain_detail->recieved) && $maintain_detail->recieved=="complete")? 'readonly' : '' ?>>
                            <option value="pending">pending</option>
                            <option value="complete" <?= (!empty($maintain_detail->recieved) && $maintain_detail->recieved=="complete")? 'selected' : '' ?>>Complete</option>
                          </select>
                       <?php endif ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="to-disabled">
                  
                  <div class="form-group">
                      <label for="FirstName" class="col-md-4 control-label">Maker</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control" id="vendor" disabled>
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="FirstName" class="col-md-4 control-label">Car Type</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control" id="car_type" disabled>
                        
                      </div>
                    </div>
                  
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Description</label>
                    <div class="col-md-8">
                      <textarea class="form-control"  id="description" name="description" required=""><?= !empty($maintain_detail->description)? $maintain_detail->description : '' ?></textarea>
                      <!-- <input type="text" class="form-control" id="title"> -->
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Comments</label>
                    <div class="col-md-8">
                      <textarea class="form-control" name="comments" ><?= !empty($maintain_detail->comments)? $maintain_detail->comments : '' ?></textarea>
                      <!-- <input type="text" class="form-control" id="title"> -->
                    </div>
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="FirstName" class="col-md-4 control-label">Upload</label>
                    <div class="col-md-8">
                      <input type="file" name="upload[]" value="" multiple>
                    </div>
                  </div>

                  <?php if ($isEdit): ?>
                    <div class="form-group">
                      <div class="col-xs-12 col-md-12">
                        <button type="button" class="btn btn-info pull-right" onclick="create_new('<?php echo site_url(CRM_VAR.'/car_maintenance')?>')">Create New Record</button>
                      </div>
                    </div>
                  <?php endif ?>
                  <?php //file_attachments ?>
                  <?php if (!empty($maintain_detail->file_attachments)): ?>
                    
                  <div class="form-group">
                    <?php foreach (json_decode($maintain_detail->file_attachments) as $key => $value): ?>
                      <div class="col-xs-3 col-md-2 pull-right">
                        <a href="#" class="thumbnail"  data-toggle="modal" data-target="#lightbox">
                          <img src="<?= site_url().'uploads/file_attachments/thumb/350X250/'.$value ?>" alt="<?= $value ?>" height="100px" width="100px">
                        </a>
                      </div>
                    <?php endforeach ?>
                  </div>
                  <?php endif ?>

                </div>
                
              </div>
              <div class="box-footer">
                <button id="submit" type="submit" class="btn btn-info pull-right" onclick="return confirm('Do you want <?= ($isEdit)? 'Update' : 'Save' ?>?')""><?= ($isEdit)? 'Update' : 'Save' ?></button>
              </div>
            </form>
          </div>
          
          <!-- /.box -->
        </div>
      </div>
      <!--/.col (left) -->
    </div>
    <!-- /.row -->
  </section>

<?php if(TRUE){?>
<script type="text/javascript">
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/ajax_list_maintains')?>",
            "type": "POST",
            "data": function ( data ) {         
                data.user_id = $('#user_id').val();            
                data.maker = $('#maker').val();            
                data.workshop_name = $('#workshop_name').val();
                // console.log($('#workshop_name').val());
            }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,11 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });

    var vendor_text="";
    var car_tag="";
    var repaired_by="";
    var get_car_tag="<?= (!empty($maintain_detail->car_id))? $maintain_detail->car_id : '' ?>";
    function description_add_text(){
      // description+=text+"\n";
      $('#description').val(vendor_text+"\n"+car_tag+"\n"+repaired_by+"\n");
    }
    function selected_recieved(){
       if($("#suser_id option:selected").html()=="Sonic Star Travel"){
          
          $("#recieved_from_vendor").hide();
        }else{
          $("#recieved_from_vendor").show();
        }
    }

    
    selected_recieved();
    function load_vendor(){
       // console.log();
        selected_recieved();
        $("#suser_id option:selected" ).each(function() {
          // console.log("$(this).val()");
          if($( "#suser_id option:selected" ).val()!=""){
            vendor_text="Vendor : "+$(this).html();
          }else{
            vendor_text="";
          }
          description_add_text();
          $.get("<?php echo site_url(CRM_VAR.'/related_car_tag/')?>"+$(this).val(),function(data){
            var html="";
            html="<option value=''>Select Car</option>";
            var obj=$.parseJSON(data);

            $.each(obj,function(k,v){
              if(v['id']==get_car_tag){
                html+="<option value='"+v['id']+"' selected>"+v['car_number']+"</option>";

              }else{
                html+="<option value='"+v['id']+"'>"+v['car_number']+"</option>";                
              }
            });
              // console.log(html);
            $('#car_tag').html(html);
          });
        });
    }
    // load_vendor();
    $('#suser_id').change(function(){
        load_vendor();
      });
    function load(){
      $( "#car_tag option:selected" ).each(function() {
        // console.log($(this).val());
        
        $.post("<?php echo site_url(CRM_VAR.'/car_detail')?>",{'car_tag':$(this).val()},function(data){
          // console.log(data);
          var obj=$.parseJSON(data);
          $.each(obj,function(k,v){
            // console.log(k);
            $('#vendor').val(v[1]);
            $('#car_type').val(v[2]);
            $('#car_owner').val(v[3]);
          });
        });
      });
    }
    function load_repaired_by(){
      $('#repaired_by option:selected').each(function(){
        if($( "#repaired_by option:selected" ).val()!=""){
          repaired_by="Repaired By : "+$(this).html();
        }else{
          repaired_by="";
        }
        description_add_text();
      });
    }
    // load_repaired_by();
    $('#repaired_by').change(function(){
      load_repaired_by();
    });
    
    $('#workshop_name').keyup(function(){
      // load();
      table.ajax.reload(null,false);
    });
    $('#car_tag').change(function(){
      // console.log(this.val());
      if($( "#car_tag option:selected" ).val()!=""){
        car_tag="Car Tag : "+$( "#car_tag option:selected" ).html();
      }else{
        car_tag="";
      }
      description_add_text();
      load();
    });

    $('.datepicker').datepicker({
        autoclose: true
    });
    // $('.datepicker').datepicker({
    //     autoclose: true
    // });
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    <?php if ($isEdit): ?>
      load();
    <?php endif ?>
    
});
  <?php if (!empty($maintain_detail)): ?>
    
    <?php if ($maintain_detail->paid=="complete" || $maintain_detail->recieved=="complete"): ?>
      $('.to-disabled input').attr('readonly',true);
      $('.to-disabled select').attr('readonly',true);
      $('.to-disabled textarea').attr('readonly',true);
    <?php endif ?>
    <?php if ($maintain_detail->paid=="complete" && $maintain_detail->recieved=="complete"): ?>
      $('.box-footer').hide();
    <?php endif ?>
  <?php endif ?>
</script>
<?php } ?>