<?php
$route[CRM_VAR.'/car_list']     = 'Car_manager/Admin/listCar';
$route[CRM_VAR.'/car_list_ajax'] = 'Car_manager/Admin/ajax_list_car';
$route[CRM_VAR.'/add_car']      = 'Car_manager/Admin/add_car';
$route[CRM_VAR.'/add_car/(:num)'] = 'Car_manager/Admin/add_car/$1';
$route[CRM_VAR.'/ajax_update_car_status/(:any)/(:any)'] = 'Car_manager/Admin/ajax_update_car_status/$1/$2';
$route[CRM_VAR.'/ajax_delete_car/(:any)'] = 'Car_manager/Admin/ajax_delete_car/$1';
$route[CRM_VAR.'/get_models']     = 'Car_manager/Admin/get_models';
$route[CRM_VAR.'/formula/(:num)']     = 'Car_manager/Admin/get_formula/$1';
$route[CRM_VAR.'/car/delete_image']     = 'Car_manager/Admin/delete_image';
$route[CRM_VAR.'/car_formula']     = 'Car_manager/Admin/car_formula';
$route[CRM_VAR.'/formula']     = 'Car_manager/Admin/modify_formula';
$route[CRM_VAR.'/car_maintenance']      = 'Car_manager/Admin/car_maintenance';
$route[CRM_VAR.'/car_maintenance/(:any)']      = 'Car_manager/Admin/car_maintenance/$1';
$route[CRM_VAR.'/car_maintenance_delete/(:any)']      = 'Car_manager/Admin/car_maintenance_delete/$1';

$route[CRM_VAR.'/car_detail']      = 'Car_manager/Admin/car_detail_ajax';
$route[CRM_VAR.'/save_maintain_rec']      = 'Car_manager/Admin/store_maintain';
$route[CRM_VAR.'/update_maintain_rec/(:any)']      = 'Car_manager/Admin/store_maintain/$1';

$route[CRM_VAR.'/car_list/(:num)'] = 'Car_manager/Admin/car_detail/$1';
$route[CRM_VAR.'/ajax_list_car_maintain'] = 'Car_manager/Admin/ajax_list_car_maintain';

$route[CRM_VAR.'/oil_slip'] = 'Car_manager/Admin/oil_slip';
$route[CRM_VAR.'/oil_slip/(:any)'] = 'Car_manager/Admin/oil_slip/$1';
$route[CRM_VAR.'/save_oil_slip_rec']      = 'Car_manager/Admin/store_oil_slip';
$route[CRM_VAR.'/update_oil_slip_rec/(:any)']      = 'Car_manager/Admin/store_oil_slip/$1';
$route[CRM_VAR.'/oil_slip_delete/(:any)']      = 'Car_manager/Admin/oil_slip_delete/$1';

$route[CRM_VAR.'/ajax_list_car_oil_slip'] = 'Car_manager/Admin/ajax_list_car_oil_slip';
$route[CRM_VAR.'/ajax_list_oil_slip'] = 'Car_manager/Admin/ajax_list_oil_slip';
$route[CRM_VAR.'/ajax_list_maintains'] = 'Car_manager/Admin/ajax_list_maintains';

// $route[CRM_VAR.'/payment_to_workshop/(:any)'] = 'Car_manager/Admin/payment_to_workshop/$1';

$route['cars']     = 'Car_manager/Front/car_list';
$route['car/(:any)']     = 'Car_manager/Front/car_details/$1';
$route['car/(:any)/(:any)']     = 'Car_manager/Front/car_details/$1/$2';
$route['ajax_cars']     = 'Car_manager/Front/cars_list';
$route['car_search']     = 'Car_manager/Front/car_search';

$route['get_destination']     = 'Car_manager/Front/get_destination';
$route['set-multidays']     = 'Car_manager/Front/set_multidays';
$route['set_dates']     = 'Car_manager/Front/set_dates';
$route['set-destinations']     = 'Car_manager/Front/set_destinations';
$route['get-cityby-destination']     = 'Car_manager/Front/getCityByDestination';
$route['set-multi-destinations']     = 'Car_manager/Front/set_multi_destination';

$route['get_customer_detail/(:any)'] = 'Car_manager/Front/get_cutomer_detail/$1';