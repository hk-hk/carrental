<?php

$route[CRM_VAR.'/article/list']     = 'Articles/Article/listArticle'; //listing of Article
$route[CRM_VAR.'/article/ajax_article_list'] = 'Articles/Article/ajax_article_list'; //ajax call for terminal data for datatable
$route[CRM_VAR.'/article/add']              = 'Articles/Article/add'; //adding article
$route[CRM_VAR.'/article/add/(:num)']       = 'Articles/Article/add/$1'; //article editing form
$route[CRM_VAR.'/article/delete/(:num)']       = 'Articles/Article/delete/$1'; //article delete (change status)
$route[CRM_VAR.'/article/change_status/(:num)']       = 'Articles/Article/change_status/$1'; //article delete (change status)


$route['about-us']       = 'Articles/Article/about_us'; //article editing form
$route['faq']       = 'Articles/Article/faq'; //article editing form
$route['cpage/(:any)']       = 'Articles/Article/cms_pages/$1'; //article editing form

$route['contact-us']      = 'Articles/Article/contact_us';

$route['attractive-places']      = 'Articles/Article/attractive_places'; // attractive places
$route['attractive-places/(:num)']       = 'Articles/Article/attractive_place_detail/$1'; //  attractive places detail
// $route['attractive-places/(:num)']       = 'Articles/Article/attractive_place_detail/$1'; //  attractive places detail
$route[CRM_VAR.'/delete_image']     = 'Articles/Article/delete_image';