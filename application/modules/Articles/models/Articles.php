<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends MY_Model {

	public $_table = 'cr_articles'; // database table
	public $primary_key = 'id'; // primary key
	public $before_create = array( 'timestamps' );
	public $column_order = array(null, null,'title','url',null); //set column field database for datatable orderable	
	public $column_search = array('title','cat_id'); //set column field database for datatable searchable 	
	public $order = array('id' => 'asc'); // default order
	
	public $config = array(         // Set validation for add/edit form.
     array(
                'field' => 'url',
                'label' => 'URL',
                'rules' => 'required'
     ),
     array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required'
     ),
	array(
                'field' => 'content',
                'label' => 'Content',
                'rules' => 'required',
               
     ),
	
	
	);

	
  protected function timestamps($terminal)
    {
        $terminal['created_date'] = date('Y-m-d H:i:s');
        return $terminal;
    }
    
  private function _get_datatables_query() // dynamic query for search terminal
    {
        
        if($this->input->post('title'))
        {
            $this->db->where('title', $this->input->post('title'));
        }
        if($this->input->post('cat_id'))
        {
            $this->db->where('cat_id', $this->input->post('cat_id'));
        }
        $this->db->where_not_in('status', array(2));
        $this->db->select('*');
        $this->db->from($this->_table);
        
        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();      
        return $query->result();
    }

    function count_filtered() // Get total row count of search query
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    
}
