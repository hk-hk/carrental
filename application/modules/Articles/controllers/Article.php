<?php
/***
 *
 * Created by: CIS
 * This is for authorized user's only.
 * This file contains all Property's related functions.
 *
 ***/
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "/libraries/recaptchalib.php";
class Article extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->data['module'] = 'Articles';
        $this->load->model('Articles', 'articles');
        $this->lang->load($this->data['module'] . '/articles', $this->getActiveLanguage());
        //$this->load->model('Objects', 'objects');
        // $this->load->model('ToolModel');
        // $this->load->library('fpdf');
    }
    public function index()
    {

    }
    /* list of article */
    public function listArticle()
    {
        $this->isAuthorized();
        $this->data['content_category'] = $this->articles->get_attributes(array(30), true);
        $this->data['view']             = 'listArticle';
        $this->layout->admin($this->data);
    }
    /* list of article */

    /* ajax call for article table */

    public function ajax_article_list() //list of all terminals for ajax datatables

    {
        $this->isAuthorized();
        $this->is_ajax();
        //$this->articles->belongs = $this->session->userdata('crm_user_id'); //Logged in user id
        $list = $this->articles->get_datatables();

        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $term) {
            $show = '<div id="article_' . $term->id . '" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content">
    <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Terminal User Information</h4></div><div class="modal-body">
    <table class="table table-striped popup">
    <tbody >
    <tr><td>Title:</td><td>' . $term->title . '</td></tr>
    <tr><td>Content:</td><td>' . $term->content . '</td></tr>
    </tbody>
    </table>
    </div></div></div></div>';
            $no++;
            $row   = array();
            $row[] = $no;
            if ($term->featured_img != '') {
                $row[] = "<img alt='No Image' src='" . site_url() . "uploads/article/thumb/100X100/" . $term->featured_img . "' height='100px' width='100px' />";
            } else { $row[] = '';}
            $row[]    = $term->title;
            $row[]    = $term->url;
            $isActive = ($term->status == 0) ? 'btn-danger' : 'btn-success';
            $btntxt   = ($term->status == 1) ? 'Active' : 'Deactive';
            $row[]    = '<span><a title="Change Status" onclick="return(confirm(\'Are you Sure ?\'))" href="' . site_url(CRM_VAR) . '/article/change_status/' . $term->id . '" class="btn ' . $isActive . ' btn-xs change-status">' . $btntxt . '</a></span>';

            //$row[] = $term->status == 1?"Active":"Inactive";
            $row[] = $show . '<!-- <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#article_' . $term->id . '">View</button>&nbsp; --><a href="' . site_url(CRM_VAR) . '/article/add/' . $term->id . '"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;<a onclick="return(confirm(\'Are you Sure ?\'))" href="' . site_url(CRM_VAR) . '/article/delete/' . $term->id . '"><button class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></button></a>';

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->articles->count_all(),
            "recordsFiltered" => $this->articles->count_filtered(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /* ajax call for article table*/

    /* Add or Edit Article */

    public function add($id = 0)
    {
        $cat_for_banner = array("48", "73");
        $this->isAuthorized();
        $this->data['view']        = 'addArticle';
        $this->data['articleinfo'] = (object) array('id' => '', 'cat_id' => '', 'url' => '', 'title' => '', 'title_mya' => '', 'content' => '', 'content_mya' => '', 'short_description' => '', 'short_description_mya' => '', 'featured_img' => '', 'banner_images' => json_encode(array()));

        $this->data['content_category'] = $this->articles->get_attributes(array(30), true);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $post_data = array(
                "url"                   => str_replace(" ", "-", strtolower($this->input->post('url', true))),
                "cat_id"                => $this->input->post('cat_id', true),
                "title"                 => $this->input->post('title', true),
                "title_mya"             => $this->input->post('title_mya', true),
                "content"               => $this->input->post('content', true),
                "content_mya"           => $this->input->post('content_mya', true),
                "short_description"     => $this->input->post('short_description', true),
                "short_description_mya" => $this->input->post('short_description_mya', true),
                "status"                => 1,
            );
        }
        $thumb_array = array(
            0 => array("height" => '100', "width" => '100'),
            1 => array("height" => '510', "width" => '1170'),
        );
        $thumb = array(
            0 => array("height" => '100', "width" => '100'),
        );
        $image_upload = array(
            "file_name"   => "featured_image",
            "upload_path" => "./uploads/article",
            "prefix"      => "article",
            "thumb_array" => $thumb,
        );
        if ($id > 0) // Check for edit Article
        {
            $articleinfo = $this->articles->get_by('id', $id);
            if (empty($articleinfo)) {
                // Set message,If article id is invalid
                $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
                redirect(CRM_VAR . '/article/list');
            }
            $this->data['articleinfo'] = $articleinfo;
            /* image validation for edit */

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if ($_FILES['featured_image']['name'] == '') {
                    $post_data["featured_img"] = $articleinfo->featured_img;
                } else {
                    $image = $this->uploadImage($image_upload);

                    if (!$image) {
                        $this->form_validation->set_rules("featured_image", "Valid Featured Image", "required");
                    } else {
                        if (file_exists("./uploads/article/" . $articleinfo->featured_img)) {
                            unlink("./uploads/article/" . $articleinfo->featured_img);
                        }
                        if (file_exists("./uploads/article/thumb/100X100/" . $articleinfo->featured_img)) {
                            unlink("./uploads/article/thumb/100X100/" . $articleinfo->featured_img);
                        }
                        $post_data["featured_img"] = $image; // we can have file path by indexing 'file_path'
                    }
                }
            }
            /* image validation for edit */

            /* image validation for edit */
            //if($this->input->post('cat_id', true) == 48){
            if (in_array($this->input->post('cat_id', true), $cat_for_banner)) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if ($_FILES['banner_img']['name'][0] == '') {
                        $post_data["banner_images"] = $articleinfo->banner_images;
                    } else {
                        $banner_result = $this->multiple_upload('banner_img', 'article_banner', $thumb_array, './uploads/article/banner');
                        $old_files     = json_decode($articleinfo->banner_images);
                        /*if(!empty($old_files)){
                        foreach ($old_files as $key => $value) {
                        if(file_exists("./uploads/article/banner/".$value)){
                        unlink("./uploads/article/banner/".$value);
                        }
                        if(file_exists("./uploads/article/banner/thumb/100X100/".$value)){
                        unlink("./uploads/article/banner/thumb/100X100/".$value);
                        }
                        if(file_exists("./uploads/article/banner/thumb/1170X510/".$value)){
                        unlink("./uploads/article/banner/thumb/1170X510/".$value);
                        }
                        }
                        }*/
                        $post_data["banner_images"] = json_encode(array_merge($old_files, $banner_result['file_data']));
                        if ($banner_result['error'] != 0) {
                            $this->form_validation->set_rules("banner_img", "All Valid Banner Image", "required");
                        }
                    }
                }
            }
        } else {
            /* image validation for add */
            if (isset($_FILES['featured_image']['name'])) {
                if ($_FILES['featured_image']['name'] != '') {
                    $image = $this->uploadImage($image_upload);
                    if (!$image) {
                        $this->form_validation->set_rules("featured_image", "Valid Featured Image", "required");
                    } else {
                        $post_data["featured_img"] = $image; // we can have file path by indexing 'file_path'
                    }
                }}
            /* image validation for add */
            //if($this->input->post('cat_id', true) == 48){
            if (in_array($this->input->post('cat_id', true), $cat_for_banner)) {
                if (isset($_FILES['banner_img']['name']) && $_FILES['banner_img']['name'] != '') {
                    if ($_FILES['banner_img']['name'][0] == '') {
                        $post_data["banner_images"] = json_encode(array());
                    } else {
                        $banner_result              = $this->multiple_upload('banner_img', 'article_banner', $thumb_array, './uploads/article/banner');
                        $post_data["banner_images"] = json_encode($banner_result['file_data']);
                        if ($banner_result['error'] != 0) {
                            $this->form_validation->set_rules("banner_img", "All Valid Banner Image", "required");
                        }
                    }
                }
            }
        }
        /*if(filter_var($this->input->post('url' ,true), FILTER_VALIDATE_URL) == FALSE){
        $this->form_validation->set_rules("valid_url", "Valid URL", "required");
        }*/
        $this->form_validation->set_rules($this->articles->config);
        if ($id > 0 && $this->form_validation->run() == false) // check validation for edit article
        { $this->layout->admin($this->data);} else if ($id == 0 && $this->form_validation->run() == false) // check validation for add article
        {
            $this->layout->admin($this->data);
        } else {
            if ($id > 0) {
                if ($this->articles->update($id, $post_data)) {
                    // Upade article's information
                    $this->set_msg_flash(['msg' => 'Article Updated!!', 'typ' => 1]);
                    redirect(CRM_VAR . '/article/list');
                } else {redirect(CRM_VAR . '/article/add/' . $id);}

            } else {
                if ($this->articles->insert($post_data)) {
                    // Add new article
                    $this->set_msg_flash(['msg' => 'Article Added!!', 'typ' => 1]);
                    redirect(CRM_VAR . '/article/add');
                } else {redirect(CRM_VAR . '/article/add');}
            }
        }

    }

    /* Add or Edit Article */

    /* delete article (change status yo 2 for delete) */
    public function delete($id = 0)
    {
        $this->isAuthorized();
        $articleinfo = $this->articles->get_by('id', $id);
        if (empty($articleinfo)) {
            // Set message,If article id is invalid
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/article/list');
        } else {
            if ($this->articles->update($id, array("status" => 2))) {
                // delete change status
                $this->set_msg_flash(['msg' => 'Article Deleted!!', 'typ' => 1]);
                redirect(CRM_VAR . '/article/list');
            }
        }
    }

    /* delete particular image */
    public function delete_image()
    {
        $id          = $this->input->post('id');
        $image       = $this->input->post('image');
        $articleinfo = $this->articles->get_by('id', $id);
        if (!empty($articleinfo)) {
            $old_files = json_decode($articleinfo->banner_images);
            $new_files = array();
            foreach ($old_files as $key => $value) {
                if ($value == $image) {
                    if (file_exists("./uploads/article/banner/" . $value)) {
                        unlink("./uploads/article/banner/" . $value);
                    }
                    if (file_exists("./uploads/article/banner/thumb/100X100/" . $value)) {
                        unlink("./uploads/article/banner/thumb/100X100/" . $value);
                    }
                    if (file_exists("./uploads/article/banner/thumb/1170X510/" . $value)) {
                        unlink("./uploads/article/banner/thumb/1170X510/" . $value);
                    }
                } else {
                    $new_files[] = $value;
                }
            }
            $banner_images = json_encode($new_files);
            $postData      = array("banner_images" => $banner_images);
            if ($this->articles->update($id, $postData)) {
                echo 1;
            } else {
                echo 2;
            }
        }
    }

    /* change status of article */
    public function change_status($id = 0)
    {
        $this->isAuthorized();
        $articleinfo = $this->articles->get_by('id', $id);
        if (empty($articleinfo)) {
            // Set message,If article id is invalid
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/article/list');
        } else {
            $new_status = $articleinfo->status == 1 ? 0 : 1;
            if ($this->articles->update($id, array("status" => $new_status))) {
                // delete change status
                $this->set_msg_flash(['msg' => 'Article Deleted!!', 'typ' => 1]);
                redirect(CRM_VAR . '/article/list');
            }
        }
    }

    /* check Image */
    public function getMimeType($filename)
    {
        $mimetype = false;
        if (function_exists('finfo_fopen')) {
            // open with FileInfo
        } elseif (function_exists('getimagesize')) {
            // open with GD
        } elseif (function_exists('exif_imagetype')) {
            // open with EXIF
        } elseif (function_exists('mime_content_type')) {
            $mimetype = mime_content_type($filename);
        }
        return $mimetype;
    }
    /* check Image */

    public function about_us()
    {
        $who_we_are            = $this->articles->get_by('url', 'who-we-are');
        $what_we_do            = $this->articles->get_by('url', 'what-we-do');
        $this->data['drivers'] = $this->articles->get_many_by('cat_id', '26'); //get Drivers
        //print_r($drivers);die;
        $this->data['who_we_are']  = $who_we_are;
        $this->data['what_we_do']  = $what_we_do;
        $this->data['page_title']  = "About Us";
        $this->data['breadcrumbs'] = array(
            array('url' => site_url(), 'title' => 'Home'),
            array('url' => '', 'title' => "About Us"),
        );
        $this->data['rental_statics'] = $this->articles->get_attributes(array(79), true);

        $this->data['view'] = 'about_us';
        $this->layout->front($this->data);
    }
    public function cms_pages($url)
    {
        $pageInfo = $this->articles->get_by(array('url' => $url, 'status' => '1')); //get Drivers
        //print_r($pageInfo);die;
        if (empty($pageInfo)) {
            redirect(site_url());
        }
        $activeLanguage = $this->session->userdata('active_language');
        if ($activeLanguage == 'mk') {
            $pageInfo->title = $pageInfo->title_mya;
        }
        $this->data['page_title']  = $pageInfo->title;
        $this->data['breadcrumbs'] = array(
            array('url' => site_url(), 'title' => getLang('BREADCRUMS_HOME')),
            array('url' => '', 'title' => $pageInfo->title),
        );
        $this->data['view']     = 'cms_pages';
        $this->data['pageInfo'] = $pageInfo;
        $this->layout->front($this->data);
    }
    public function faq()
    {
        $this->data['faq_list'] = $this->articles->get_many_by(array('cat_id' => 31, 'status' => '1')); //get Drivers
        //print_r($drivers);die;
        $this->data['page_title']  = getLang('BREADCRUMS_FAQ');
        $this->data['breadcrumbs'] = array(
            array('url' => site_url(), 'title' => getLang('BREADCRUMS_HOME')),
            array('url' => '', 'title' => getLang('BREADCRUMS_FAQ')),
        );
        $this->data['view'] = 'faq';
        $this->layout->front($this->data);
    }

    public function attractive_places()
    {

        $this->data['extra_js'] = array('owl.carousel.min.js', 'wow.js');

        $this->data['places_list'] = $this->articles->get_many_by(array('cat_id' => 48, 'status' => '1')); //get attractive
        //print_r($this->data['places_list']);die;
        $this->data['page_title']  = getLang('BREADCRUMS_ATTRACTIVE_PLACES');
        $this->data['breadcrumbs'] = array(
            array('url' => site_url(), 'title' => getLang('BREADCRUMS_HOME')),
            array('url' => '', 'title' => getLang('BREADCRUMS_ATTRACTIVE_PLACES')),
        );
        $this->data['view'] = 'attractive_places';
        $this->layout->front($this->data);
    }

    public function attractive_place_detail($id = '')
    {
      // print "<pre/>";
      // print_r($_POST);
      // exit();
      $placeinfo               = $this->articles->get_by('id', $id);
      $this->data['placeinfo'] = $placeinfo;
      $this->data['extra_css'] = array('flags.css', 'jquery.timepicker.css');
      $this->data['extra_js']  = array('owl.carousel.min.js', 'wow.js', 'jquery.timepicker.js');
      $this->data['page_title']  = $placeinfo->title;
      $this->data['view'] = 'attractive_place_detail';
      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules("first_name", getLang('CONTACT_FIRST_NAME'), "required");
            // $this->form_validation->set_rules("your_email", getLang('CONTACT_EMAIL'), "required|valid_email");
            $this->form_validation->set_rules("subject", getLang('CONTACT_SUBJECT'), "required");
            $this->form_validation->set_rules("message", getLang('CONTACT_YOUR_MESSAGE'), "required");
            $this->form_validation->set_rules("phone", getLang('CONTACT_PHONE'), "numeric");

            if ($this->form_validation->run() == false) {
                $this->set_msg_flash([
                    'msg' => getLang('CONTACT_INVALID_FORM_SUBMITION'),
                    'typ' => 3,
                ]);
            } else {
                $secret    = "6LfblG0UAAAAANzGd4cRfeokfw3RnATbIG12t7BQ";
                $response  = null;
                $reCaptcha = new ReCaptcha($secret);
                if ($_POST["g-recaptcha-response"]) {
                    $response = $reCaptcha->verifyResponse(
                        $_SERVER["REMOTE_ADDR"],
                        $_POST["g-recaptcha-response"]
                    );
                }
                // echo $message;
                // exit();
                // if(true){
                if ($response != null && $response->success) {
                    $message="Travel Date: ".$this->input->post('travel_date').
                    "<br/>Passenger:<br/>".
                        "Adult: ".$this->input->post('adult')."<br/>".
                        "Child: ".$this->input->post('child')."<br/>".
                        "Infant: ".$this->input->post('infant').
                    "<br/>Email: ".$this->input->post('your_email').
                    "<br/>Phone: ".$this->input->post('phone').
                    "<br/>".$this->input->post('message');

                    $emailbody = file_get_contents("./templates/contact_email.html"); //mail template path
                    $emailbody = str_replace("{CUSTOMER_NAME}", $this->input->post('first_name'), $emailbody);
                    $emailbody = str_replace("{SITE}", base_url(), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_EMAIL}", $this->input->post('your_email'), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_PHONE}", $this->input->post('phone'), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_SUBJECT}", $this->input->post('subject'), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_MESSAGE}", $message, $emailbody);
                    $to_email  = 'tour@sonicstartravel.com';
                    // $bcc_email  = array('tin@sonicstartravel.com', 'ye@sonicstartravel.com', 'thitmincho@gmail.com');

                    if (!empty($this->input->post('tour_package'))) {
                        $subject = "Tour Enquiry: " . $this->input->post('first_name') . ", " . $this->input->post('subject');
                    } else {
                        $subject = "New Enquiry";
                    }
                    $mailData = array(
                        "to"      => $to_email,
                        "cc"      => 'ye@sonicstartravel.com',
                        "bcc"     => "tin@sonicstartravel.com",
                        "subject" => $subject,
                        "message" => $emailbody,
                    );
                    $result = $this->sendMail($mailData);
                    // print_r($result);
                    // exit();
                    if ($result) {
                        $this->set_msg_flash(['msg' => getLang('CONTACT_SUCCESSFULL_MESSAGE'), 'typ' => 1]);
                        echo "message Sent!";
                    } else {
                        $this->set_msg_flash(['msg' => 'Email Failed', 'typ' => 2]);
                        echo "message Not Sent!";
                    }
                    if(!empty($this->input->post('tour_package'))){
                      redirect(site_url().'attractive-places/'.$this->input->post('tour_package'));
                    }
                }
            }
      }else{
        
        
        $activeLanguage          = $this->session->userdata('active_language');
       
        if (empty($placeinfo)) {redirect(CRM_VAR . '/attractive-places');}
        if ($activeLanguage == 'mk') {
            $placeinfo->title = ($placeinfo->title_mya != '') ? $placeinfo->title_mya : $placeinfo->title;
        
        }
        $this->data['breadcrumbs'] = array(
            array('url' => site_url(), 'title' => getLang('BREADCRUMS_HOME')),
            array('url' => site_url('attractive-places'), 'title' => getLang('BREADCRUMS_ATTRACTIVE_PLACES')),
            array('url' => '', 'title' => $placeinfo->title),
       
        );
      }
        $this->layout->front($this->data);
        // print_r($placeinfo);
    }

    public function test($value = '')
    {
        $this->data['view'] = 'test';
        $this->layout->front($this->data);
    }

    public function contact_us()
    {
        $this->data['extra_css']   = array('flags.css');
        $this->data['extra_js']    = array('owl.carousel.min.js', 'wow.js');
        $this->data['page_title']  = getLang('CONTACT_CONTACT_US');
        $this->data['breadcrumbs'] = array(
            array('url' => site_url(), 'title' => getLang('BREADCRUMS_HOME')),
            array('url' => '', 'title' => getLang('BREADCRUMS_CONTACT_US')),
        );
        $this->data['view']        = 'contact_us';
        $this->data['contactInfo'] = $this->articles->get_attributes(array(50), true);
        $this->data['socialIcons'] = $this->articles->get_attributes(array(51), true);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules("your_name", getLang('CONTACT_YOUR_NAME'), "required");
            $this->form_validation->set_rules("your_email", getLang('CONTACT_EMAIL'), "required|valid_email");
            $this->form_validation->set_rules("subject", getLang('CONTACT_SUBJECT'), "required");
            $this->form_validation->set_rules("message", getLang('CONTACT_YOUR_MESSAGE'), "required");
            $this->form_validation->set_rules("phone", getLang('CONTACT_PHONE'), "numeric");

            if ($this->form_validation->run() == false) {
                $this->set_msg_flash([
                    'msg' => getLang('CONTACT_INVALID_FORM_SUBMITION'),
                    'typ' => 3,
                ]);
            } else {
                $secret    = "6LfblG0UAAAAANzGd4cRfeokfw3RnATbIG12t7BQ";
                $response  = null;
                $reCaptcha = new ReCaptcha($secret);
                if ($_POST["g-recaptcha-response"]) {
                    $response = $reCaptcha->verifyResponse(
                        $_SERVER["REMOTE_ADDR"],
                        $_POST["g-recaptcha-response"]
                    );
                }
                if ($response != null && $response->success) {
                    $emailbody = file_get_contents("./templates/contact_email.html"); //mail template path
                    $emailbody = str_replace("{CUSTOMER_NAME}", $this->input->post('your_name'), $emailbody);
                    $emailbody = str_replace("{SITE}", base_url(), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_EMAIL}", $this->input->post('your_email'), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_PHONE}", $this->input->post('phone'), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_SUBJECT}", $this->input->post('subject'), $emailbody);
                    $emailbody = str_replace("{CUSTOMER_MESSAGE}", $this->input->post('message'), $emailbody);
                    $to_email  = 'naylinaung.nla.xo@gmail.com';
                    // $bcc_email  = array('tin@sonicstartravel.com', 'ye@sonicstartravel.com', 'thitmincho@gmail.com');

                    if (!empty($this->input->post('tour_package'))) {
                        $subject = "Tour Enquiry: " . $this->input->post('your_name') . ", " . $this->input->post('subject');
                    } else {
                        $subject = "New Enquiry";
                    }
                    $mailData = array(
                        "to"      => $to_email,
                        "cc"      => 'ye@sonicstartravel.com',
                        "bcc"     => "tin@sonicstartravel.com",
                        "subject" => $subject,
                        "message" => $emailbody,
                    );
                    $result = $this->sendMail($mailData);

                    if ($result) {
                        $this->set_msg_flash(['msg' => getLang('CONTACT_SUCCESSFULL_MESSAGE'), 'typ' => 1]);
                        echo "message Sent!";
                    } else {
                        $this->set_msg_flash(['msg' => 'Email Failed', 'typ' => 2]);
                        echo "message Not Sent!";
                    }
                    if(!empty($this->input->post('tour_package'))){
                      redirect(site_url().'attractive-places/'.$this->input->post('tour_package'));
                    }else{
                      redirect(site_url().'contact-us');
                    }
                }
            }
        }
        $this->layout->front($this->data);
        //echo "contact_us";
    }

}
