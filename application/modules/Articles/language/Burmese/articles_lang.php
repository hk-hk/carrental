<?php
defined('BASEPATH') OR exit('ယခုစာမ်က္နာအား ၾကည့္ရႈခြင့္မျပဳပါ');


$lang['CONTACT_PAGE_HEADING'] = 'ဆက္သြယ္ႏိုုင္ပါတယ္';

$lang['PACKAGE_PAGE_HEADING'] = 'ခရီးစဥ္ စံုစမ္းရန္';

$lang['CONTACT_YOUR_NAME'] = 'သင့္ နာမည္';

$lang['CONTACT_EMAIL'] = 'အီးေမးလ္';

$lang['CONTACT_PHONE'] = 'ဖုန္း';

$lang['CONTACT_SUBJECT'] = 'အေၾကာင္းအရာ';

$lang['CONTACT_YOUR_MESSAGE'] = 'သင့္္ အေၾကာင္းအရာ';

$lang['CONTACT_SUBMIT_BTN'] = 'တင္သြင္းရန္';

$lang['CONTACT_SOCIAL'] = 'လူမႈေရး';

$lang['CONTACT_PHONE'] = 'ဖုန္း';

$lang['CONTACT_ADDRESS'] = 'လိပ္စာ';

$lang['CONTACT_INVALID_FORM_SUBMITION'] = 'တင္သြင္းသည့္ပံုစံမမွန္ကန္ပါ';

$lang['CONTACT_SUCCESSFULL_MESSAGE'] = 'သင္၏ေမးလ္ကို လက္ခံရရိွပါသည္။ ကၽြန္ေတာ္တို႔မၾကာခင္ ဆက္သြယ္ပါမည္။';


$lang['BREADCRUMS_HOME'] = 'မူလစာမ်က္ႏွာ';

$lang['BREADCRUMS_FAQ'] = 'အျမဲေမးေလ့ရိွေသာေမးခြန္းမ်ား';

$lang['BREADCRUMS_ATTRACTIVE_PLACES'] = 'ခရီးစဥ္မ်ား';

$lang['BREADCRUMS_CONTACT_US'] = 'ဆက္သြယ္ရန္';

$lang['CONTACT_NOTE'] = 'ခရီးစဥ္မွတ္ခ်က္';
$lang['CONTACT_TRAVEL_DATE'] = 'ခရီးထြက္လိုသည့္ ေန႔ရက္';
$lang['CONTACT_PASSENGER_NO'] = 'လူဦးေရ';
?>