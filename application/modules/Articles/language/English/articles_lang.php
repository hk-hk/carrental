<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['CONTACT_PAGE_HEADING'] = 'GET IN TOUCH';
$lang['PACKAGE_PAGE_HEADING'] = 'Tour Enquiry';
$lang['CONTACT_YOUR_NAME'] = 'Your Name';
$lang['CONTACT_EMAIL'] = 'Email';
$lang['CONTACT_PHONE'] = 'Phone';
$lang['CONTACT_SUBJECT'] = 'Subject';
$lang['CONTACT_YOUR_MESSAGE'] = 'Your Message';
$lang['CONTACT_SUBMIT_BTN'] = 'Submit';
$lang['CONTACT_SOCIAL'] = 'Social';
$lang['CONTACT_PHONE'] = 'Phone';
$lang['CONTACT_ADDRESS'] = 'Address';
$lang['CONTACT_INVALID_FORM_SUBMITION'] = 'Invalid Form Submition!!';
$lang['CONTACT_SUCCESSFULL_MESSAGE'] = 'We have recieved your mail. We will contact you soon!!';
$lang['CONTACT_FIRST_NAME'] = 'First Name';
$lang['CONTACT_LAST_NAME'] = 'Last Name';
$lang['CONTACT_ARRIVAL_DATE'] = 'Arrival Date';
$lang['CONTACT_DEPARTURE_DATE'] = 'Departure Date';
$lang['CONTACT_ADULT_NO'] = 'Adult';
$lang['CONTACT_CHILD_NO'] = 'Child';
$lang['CONTACT_INFANT_NO'] = 'Infant';
$lang['CHOOSE_TOUR_PACKAGE'] = 'Please Choose Tour Package';


$lang['BREADCRUMS_HOME'] = 'Home';
$lang['BREADCRUMS_FAQ'] = 'FAQ';
$lang['BREADCRUMS_ATTRACTIVE_PLACES'] = 'Tour Packages';
$lang['BREADCRUMS_CONTACT_US'] = 'Contact Us';

$lang['CONTACT_NOTE'] = 'Note';
$lang['CONTACT_TRAVEL_DATE'] = 'Travel Date';
$lang['CONTACT_PASSENGER_NO'] = 'Number of Passenger';

