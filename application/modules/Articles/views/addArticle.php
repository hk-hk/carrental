<link rel="stylesheet" href="<?php echo CRM_VAR;?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR;?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
  <style>
      #features_chosen {
      width: 100% !important;
     }
  .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
  </style>
 
<section class="content">
   <div class="row">
      <!-- left column -->
      <div class="col-md-12">
         <!-- general form elements -->
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">Add Article</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $attributes = array('id' => 'addarticle-form','class'=>'form-horizontal'); ?>
            <?php if(!empty($articleinfo)) { echo form_open_multipart(CRM_VAR.'/article/add/'.$articleinfo->id,$attributes); }
               else { echo form_open_multipart(CRM_VAR.'/article/add',$attributes); } ?>
            <?php //echo form_open(CRM_VAR.'/property/add',$attributes);?>
            <?php echo validation_errors(); ?>
            <div class="box-body">
                <div class="row">

                  <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail4" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <select name="cat_id" id="cat_id" class="form-control">
                                <option value="0">Select</option>
                              <?php
                                foreach($content_category[30] as $val){ ?>
                                  <option value="<?php echo $val['id']; ?>" <?php if($val['id']===$articleinfo->cat_id) {echo "selected";}?> ><?php echo $val['title']; ?></option>
                                    <?php 
                                }
                                ?>
                            </select>
                           
                        </div>  
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail1" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control pull-right" id="title" name="title" value="<?php if(!empty($articleinfo) && isset($articleinfo->title)){ echo $articleinfo->title;}else{ echo set_value('title');} ?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Page Slug</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control pull-right" id="url" name="url" value="<?php if(!empty($articleinfo) && isset($articleinfo->url)){ echo $articleinfo->url;}else{ echo set_value('url');} ?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-10">
                          <?php echo site_url('cpage/'); ?><span id="url_slug"></span>
                        </div>  
                     </div>
                  </div>
                  
                  <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail1" class="col-sm-2 control-label">Title In Myanmar</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control pull-right" id="title" name="title_mya" value="<?php echo $articleinfo->title_mya; ?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Short Description</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control pull-right" id="short_description" name="short_description" value="<?php if(!empty($articleinfo) && isset($articleinfo->short_description)){ echo $articleinfo->short_description;}else{ echo set_value('short_description');} ?>">
                        </div>
                     </div>
                  </div>
                    <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Short Description In Myanmar</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control pull-right" id="short_description" name="short_description_mya" value="<?php  echo $articleinfo->short_description_mya; ?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Content</label>
                    <div class="col-sm-9">
                       <textarea id="editor1" rows="10" cols="80" name="content"><?php if(!empty($articleinfo) && isset($articleinfo->content)){ echo $articleinfo->content;}else{ echo set_value('content');}?></textarea>
                    </div>
                    </div>
                  </div>
                    <div class="col-md-12">
                    <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Content In Myanmar</label>
                    <div class="col-sm-9">
                       <textarea id="editor2" rows="10" cols="80" name="content_mya"><?php echo $articleinfo->content_mya?></textarea>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Featured Image</label>
                        <div class="col-sm-10">
                           <input type="file" class="" id="featured_image" name="featured_image" value="">
                           <?php if($articleinfo->featured_img != ''){ 
                            ?><img height="100px" width="100px" src="<?php echo site_url();?>uploads/article/thumb/100X100/<?php echo $articleinfo->featured_img;?>"><?php 
                            } ?>
                        </div>
                     </div>
                  </div>

                  <div class="col-md-12" id="attactive_places_filed" style="display: none;">
                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Banner Images</label>
                        <div class="col-sm-10">
                           <input type="file" class="" id="banner_img" name="banner_img[]" value="" multiple>


                           <input type="hidden" id="len_old" value="<?php if(isset($articleinfo->banner_images)){ $old_banner_images = json_decode($articleinfo->banner_images); echo count($old_banner_images);}?>">
                           <?php $old_banner_images = json_decode($articleinfo->banner_images);
                           if(!empty($old_banner_images)){$i = 1;
                            foreach ($old_banner_images as $key => $value) {
                              ?><img id="image_<?php echo $key;?>" height="100px" width="100px" src="<?php echo site_url();?>uploads/article/banner/thumb/100X100/<?php echo $value;?>">
                              <span imagename = "<?php echo $value;?>" id="del_<?php echo $key; ?>" count="<?php echo $key; ?>" class="fa fa-trash delete_image" style="cursor: pointer;"></span>
                              <?php 
                            }
                           }
                            ?>
                        </div>
                     </div>
                  </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-info" name="save">
               <?php if(!empty($articleinfo)) { echo 'Edit Article'; }
                  else { echo 'Add Article'; } ?></button>
            </div>
            <!-- /.box-footer -->
            <?php echo form_close(); ?>
         </div>
         <!-- /.box -->
      </div>
      <!--/.col (left) -->
   </div>
   <!-- /.row -->
</section>
<script type="text/javascript">
   $(document).ready(function(){
      $("#url_slug").html("<?php if(!empty($articleinfo) && isset($articleinfo->url)){ echo $articleinfo->url;}else{ echo set_value('url');} ?>");
      $("#url").keyup(function(){
        $("#url").val($(this).val().toLowerCase().replace(/ /g, "-").replace(/,/g, ""));
        $("#url_slug").html($(this).val().toLowerCase().replace(/ /g, "-").replace(/,/g, ""));
      });
      $("#title").blur(function(){
        $("#url").val($(this).val().toLowerCase().replace(/ /g, "-").replace(/,/g, ""));
        $("#url_slug").html($(this).val().toLowerCase().replace(/ /g, "-").replace(/,/g, ""));
      });

   $('.datepicker').datepicker({
         autoclose: true
       });
     $("#addarticle-form").validate({
       rules: {
         title: "required",
         content: "required",
         //featured_image: "required",
       },
       messages: {
       
       title   : "Please enter title",
       content     : "Please enter content",
       },
       errorClass: "my-error-class",
       errorElement: "span", // default is 'label'
       errorPlacement: function(error, element) {
         error.insertAfter(element);
       },
     });
     <?php if($articleinfo->id){?> 
     $(".delete_image").click(function(){
      var res = confirm('Are you sure ?');
      if(res){
        var count = jQuery(this).attr('count');
        var image = jQuery(this).attr('imagename');
        var id = <?php echo $articleinfo->id;?>;
        $.ajax({
          type: 'POST',
          url: "<?php echo site_url(CRM_VAR.'/delete_image')?>",
          data: {image:image,id:id},
          dataType: "text",
          success: function(resultData) { 
            if(resultData){
              $("#del_"+count).hide();
              $("#image_"+count).remove();
              $("#len_old").val($("#len_old").val() - 1);
            }
            else{
              alert("Error in deleting image!");
            }
          }
        });
      }
    });
     <?php }?>

   });

   $("#banner_img").change(function(){
      var len = parseInt(document.getElementById("banner_img").files.length) + parseInt(document.getElementById("len_old").value);
      if(len > 10)
      {
         alert("You can not select more than 10 images.");
         $("#banner_img").val('');
         return false;
      }
    });
   
</script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
   $(function () {
     // Replace the <textarea id="editor1"> with a CKEditor
     // instance, using default configuration.
     CKEDITOR.replace('editor1', {
        allowedContent: {
          // Allow all content.
          $1: {
            elements: CKEDITOR.dtd,
            attributes: true,
            styles: true,
            classes: true
          }
        }/*,
        disallowedContent: ''*/
      });
       CKEDITOR.replace('editor2', {
        allowedContent: {
          // Allow all content.
          $1: {
            elements: CKEDITOR.dtd,
            attributes: true,
            styles: true,
            classes: true
          }
        }/*,
        disallowedContent: ''*/
      });
     //bootstrap WYSIHTML5 - text editor
     $(".textarea").wysihtml5();



   });

</script>

<script src="<?php echo site_url('public/plugins/choosen/chosen.jquery.min.js'); ?>"></script>
<script type="text/javascript">
   //$("#features").chosen();
   $("#features").chosen().change(function(){
    $("#p_features").val($(this).val());
    console.log($(this).val());
   });
</script>

<script type="text/javascript">
                    $(document).ready(function(){
                      function check_category($cat){
                        var arr = [ "48","73" ];
                        if(jQuery.inArray( $cat, arr ) >= 0){
                            $("#attactive_places_filed").show();
                        }
                        else{
                          $("#attactive_places_filed").hide();
                        }
                      }
                      check_category($("#cat_id").val());
                      $("#cat_id").change(function(){
                        check_category($(this).val());
                      });
                    });
                  </script>