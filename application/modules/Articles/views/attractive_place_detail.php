<section class="places-image-slider common-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
               <div class=" owl-carousel">
                        <?php 
                        $activeLanguage = $this->session->userdata('active_language');
                        $banner_images = json_decode($placeinfo->banner_images);
                        if(!empty($banner_images)){
                        foreach ($banner_images as $key => $value) {
                            if($activeLanguage=='mk'){
                                $placeinfo->title = ($placeinfo->title_mya!='')?$placeinfo->title_mya:$placeinfo->title;
                                $placeinfo->short_description = ($placeinfo->short_description_mya!='')?$placeinfo->short_description_mya:$placeinfo->short_description;
                                $placeinfo->content = ($placeinfo->content_mya!='')?$placeinfo->content_mya:$placeinfo->content;
                            }
                            ?>
                            <div class="item">
                                <img src="<?php echo base_url();?>uploads/article/banner/thumb/1170X510/<?php echo $value; ?>">
                            </div>
                        <?php }} ?>
    <!-- 
                        
                        <div class="item">
                            <img src="<?php echo site_url();?>public/front/images/place-bnner.jpg">
                        </div>
                        <div class="item">
                            <img src="<?php echo site_url();?>public/front/images/place-bnner.jpg">
                        </div> -->
               </div>
               <!-- <div class="col-sm-12"> -->
                <div class="sign-in-box">
                    <div class="login_info_block">
                        <h2 class="black section-title common-title"><span><?php echo getLang('PACKAGE_PAGE_HEADING'); ?></span></h2>
                        <?php
                        if ($this->session->flashdata('typ')):
                            switch ($this->session->flashdata('typ')) {
                                case 1:
                                    $put = 'alert-success';
                                    break;
                                case 2:
                                    $put = 'alert-warning';
                                    break;
                                case 3:
                                    $put = 'alert-danger';
                                    break;
                            }
                            ?>
                            <div class="alert <?php echo $put; ?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> !</h4>
                            <?php echo $this->session->flashdata('msg'); ?>
                            </div>
                        <?php endif; ?>
                        <form action="<?php echo site_url().'attractive-places/'.$placeinfo->id; ?>" method="post">
                            <div class="form-box">
                                <div class="row">
                                        
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="name-filed"><?php echo getLang('CONTACT_FIRST_NAME'); ?></label>&nbsp;<?php echo form_error('first_name', '<span class="error">', '</span>'); ?>
                                            <div class="input-block"> 
                                                <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                                <input class="input-fill2" type="text" name="first_name" id="first_name" required="required" value="<?php echo set_value('first_name'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="name-filed"><?php echo getLang('CONTACT_LAST_NAME'); ?></label>&nbsp;<?php echo form_error('last_name', '<span class="error">', '</span>'); ?>
                                            <div class="input-block"> 
                                                <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                                <input class="input-fill2" type="text" name="last_name" id="last_name" required="required" value="<?php echo set_value('last_name'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('CONTACT_EMAIL'); ?></label>&nbsp;<?php echo form_error('your_email', '<span class="error">', '</span>'); ?>
                                    <div class="input-block"> <span class="input-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="your_email" id="your_email" required="required" value="<?php echo set_value('your_email'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('CONTACT_PHONE'); ?></label>&nbsp;<?php echo form_error('phone', '<span class="error">', '</span>'); ?>
                                    <div class="input-block"> <span class="input-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="phone" id="phone" value="<?php echo set_value('phone'); ?>">
                                    </div>
                                </div>

                                <input class="input-fill2" type="hidden" name="subject" id="subject" required="required" value="<?php echo $placeinfo->title;?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="name-filed"><?php echo getLang('CONTACT_ARRIVAL_DATE'); ?></label>
                                            <div class="input-block"> <span class="input-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                <input class="input-fill2 datepicker" name="arrival_date"  type="text" id="arrival_date" onkeyup="set_message()">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="name-filed"><?php echo getLang('CONTACT_DEPARTURE_DATE'); ?></label>
                                            <div class="input-block"> <span class="input-icon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                <input class="input-fill2 datepicker" name="departure_date"  type="text" id="departure_date" onkeyup="set_message()">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="name-filed"><?php echo getLang('CONTACT_ADULT_NO'); ?></label>
                                        <div class="input-block"> <span class="input-icon"><i class="fa fa-group" aria-hidden="true"></i></span>
                                            <input class="input-fill2 number-format" name="adult" type="number" id="adult" value="" onkeyup="set_message()">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="name-filed"><?php echo getLang('CONTACT_CHILD_NO'); ?></label>
                                        <div class="input-block"> <span class="input-icon"><i class="fa fa-group" aria-hidden="true"></i></span>
                                            <input class="input-fill2 number-format" name="child" type="number" id="child" value="" onkeyup="set_message()">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="name-filed"><?php echo getLang('CONTACT_INFANT_NO'); ?></label>
                                        <div class="input-block"> <span class="input-icon"><i class="fa fa-group" aria-hidden="true"></i></span>
                                            <input class="input-fill2 number-format" name="infant" type="number" id="infant" value="" onkeyup="set_message()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="right-form-box" > -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                            
                                            <label class="name-filed"><?php echo getLang('CHOOSE_TOUR_PACKAGE'); ?></label>&nbsp;<?php echo form_error('message', '<span class="error">', '</span>'); ?>
                                            <select class="form-control">
                                                <option><?php echo getLang('CHOOSE_TOUR_PACKAGE'); ?></option>
                                                <option value="None">None</option>
                                                <option value="Sonic Star Package Tour">Sonic Star Package Tour</option>
                                                <option value="Pricate or Tailor-made Tour">Pricate or Tailor-made Tour</option>
                                            </select>
                                            <!-- <div class="input-block textarea-box"> <span class="input-icon"><i class="fa fa-commenting" aria-hidden="true"></i></span> -->
                                            <!-- </div> -->
                                        </div>

                                    </div>
                                </div>
                            <!-- </div> -->
                            <div class="right-form-box" style="float: none;width: 100%;" >
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="name-filed"><?php echo getLang('CONTACT_NOTE'); ?></label>&nbsp;<?php echo form_error('message', '<span class="error">', '</span>'); ?>
                                            <div class="input-block textarea-box"> <span class="input-icon"><i class="fa fa-commenting" aria-hidden="true"></i></span>
                                                <textarea class="input-fill2" name="message" required="required"><?php echo set_value('message'); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="tour_package" value="<?= $placeinfo->id ?>">
                            <div align="center" class="g-recaptcha" data-sitekey="6LfblG0UAAAAACwoJpa5Ax5dpai8mJRleH2_p7NU"></div>
                            <br/>
                            <div class="form-group submit-box">
                                <button class="more login-btn"><?php echo getLang('CONTACT_SUBMIT_BTN'); ?></button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            <!-- </div> -->
           </div>
           <div class="col-md-6 text-justify">
            <div class="place-slider-txt">
                    <h2 class="black section-title common-title"><?php echo $placeinfo->title;?></h2>
                    <p><?php echo $placeinfo->short_description;?></p>
               </div>
               <!-- <div class="row"> -->
                <?php echo $placeinfo->content; ?>
            <!-- </div> -->
           </div>
        </div>
    </div> 
</section><!-- Slider Box -->
<section class="place-detail-box common-padding graish-bg">
    <div class="container">
            
        </div> 
</section>

</section><!-- Tourist Box -->
<link rel="stylesheet" href="<?php echo base_url('public/plugins/datepicker/datepicker3.css');?>">
<script src="<?php echo base_url('public/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
<script type="text/javascript">
    function set_message(){
        // travel_date=$('#travel_date').val();
        // passenger_no=$('#passenger_no').val();
        // message=$('#message').val();
        // $('#main_message').val();
        // main_message="Travel Date: "+travel_date+"<br/>Number Of Passenger: "+passenger_no+"<br/>"+message;

        // $('#main_message').val(main_message);
        
    }
    $('.datepicker').datepicker({
        autoclose: true
    });
</script>