<style type="text/css">
    .error{
        color: #ED1C24;
    }
</style>
<section class="login-form common-padding">
    <div class="container">
        <div class="row">            
            <div class="col-sm-12">
                <div class="sign-in-box">
                    <div class="login_info_block">
                        <h2 class="black section-title common-title"><span><?php echo getLang('CONTACT_PAGE_HEADING'); ?></span></h2>
                        <?php
                        if ($this->session->flashdata('typ')):
                            switch ($this->session->flashdata('typ')) {
                                case 1:
                                    $put = 'alert-success';
                                    break;
                                case 2:
                                    $put = 'alert-warning';
                                    break;
                                case 3:
                                    $put = 'alert-danger';
                                    break;
                            }
                            ?>
                            <div class="alert <?php echo $put; ?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> !</h4>
                            <?php echo $this->session->flashdata('msg'); ?>
                            </div>
                        <?php endif; ?>
                        <form action="<?php echo site_url(); ?>contact-us" method="post">
                            <div class="left-form-box">
                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('CONTACT_YOUR_NAME'); ?></label>&nbsp;<?php echo form_error('your_name', '<span class="error">', '</span>'); ?>
                                    <div class="input-block"> 
                                        <span class="input-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="your_name" id="your_name" required="required" value="<?php echo set_value('your_name'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('CONTACT_EMAIL'); ?></label>&nbsp;<?php echo form_error('your_email', '<span class="error">', '</span>'); ?>
                                    <div class="input-block"> <span class="input-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="your_email" id="your_email" required="required" value="<?php echo set_value('your_email'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('CONTACT_PHONE'); ?></label>&nbsp;<?php echo form_error('phone', '<span class="error">', '</span>'); ?>
                                    <div class="input-block"> <span class="input-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="phone" id="phone" value="<?php echo set_value('phone'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('CONTACT_SUBJECT'); ?></label>&nbsp;<?php echo form_error('subject', '<span class="error">', '</span>'); ?>
                                    <div class="input-block"> <span class="input-icon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                        <input class="input-fill2" type="text" name="subject" id="subject" required="required" value="<?php echo set_value('subject'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="right-form-box">
                                <div class="form-group">
                                    <label class="name-filed"><?php echo getLang('CONTACT_YOUR_MESSAGE'); ?></label>&nbsp;<?php echo form_error('message', '<span class="error">', '</span>'); ?>
                                    <div class="input-block textarea-box"> <span class="input-icon"><i class="fa fa-commenting" aria-hidden="true"></i></span>
                                        <textarea class="input-fill2" name="message" id="message" required="required"><?php echo set_value('message'); ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group submit-box">
                                <div align="center" class="g-recaptcha" data-sitekey="6LfblG0UAAAAACwoJpa5Ax5dpai8mJRleH2_p7NU"></div>
                                <br/>
                                <button class="more login-btn"><?php echo getLang('CONTACT_SUBMIT_BTN'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section><!-- Contact Form -->

<section class="contact-detail-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="contact-detail-inner">
                    <h4><?php echo getLang('CONTACT_SOCIAL');?></h4>

                    <ul>
                        <?php if (!empty($socialIcons[51][1])) { ?><li><a href="<?php echo $socialIcons[51][1]['short_desc'] ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li><?php } ?>
                        <?php if (!empty($socialIcons[51][2])) { ?><li><a href="<?php echo $socialIcons[51][2]['short_desc'] ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li><?php } ?>
                        <?php if (!empty($socialIcons[51][3])) { ?><li><a href="<?php echo $socialIcons[51][3]['short_desc'] ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></li><?php } ?>
                        <?php if (!empty($socialIcons[51][4])) { ?><li><a href="<?php echo $socialIcons[51][4]['short_desc'] ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li><?php } ?>
<?php if (!empty($socialIcons[51][5])) { ?><li><a href="<?php echo $socialIcons[51][5]['short_desc'] ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li><?php } ?>
                    </ul>
                </div>
<?php if (!empty($contactInfo[50][1])) { ?>
                    <div class="contact-detail-inner">
                        <h4><?php echo getLang('CONTACT_PHONE');?></h4>
                        <div class="contact-info">
                            <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <p><?php echo $contactInfo[50][1]['short_desc'] ?></p>
                        </div>
                    </div>
                <?php } ?>
<?php if (!empty($contactInfo[50][2])) { ?>
                    <div class="contact-detail-inner">
                        <h4><?php echo getLang('');?>Email</h4>
                        <div class="contact-info">
                            <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            <p><?php echo $contactInfo[50][2]['short_desc'] ?></p>
                        </div>
                    </div>
                <?php } ?>
<?php if (!empty($contactInfo[50][3])) { ?>
                    <div class="contact-detail-inner">
                        <h4><?php echo getLang('CONTACT_ADDRESS');?></h4>
                        <div class="contact-info">
                            <span><i class="fa fa-phone" aria-hidden="true"></i></span><p>
    <?php echo $contactInfo[50][3]['short_desc'] ?></p>
                        </div>
                    </div>
<?php } ?>
            </div>
            <div class="col-sm-9">
                <div class="map-box" id="map" style="width:848px;height:400px;background:white"></div>
            </div>
        </div>
    </div>
</section>
<?php $address = (isset($contactInfo[50][4226]['short_desc'])?$contactInfo[50][4226]['short_desc']:"2 Moe Kaung Rd, Yangon, Myanmar (Burma)");?>
<script>
    function myMap() {
        var geocoder;
        var map;
        var address = "<?php echo $address; ?>";
        function initialize() {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(43.7836595, -79.4907382);
            var myOptions = {
                zoom: 14,
                center: latlng,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                },
                navigationControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            if (geocoder) {
                geocoder.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                            map.setCenter(results[0].geometry.location);

                            var infowindow = new google.maps.InfoWindow({
                                content: '<b>' + address + '</b>',
                                size: new google.maps.Size(150, 50)
                            });

                            var marker = new google.maps.Marker({
                                position: results[0].geometry.location,
                                map: map,
                                title: address
                            });
                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map, marker);
                            });

                        } else {
                            alert("No results found");
                        }
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJFZ4f-5K_tqImrSPdoG2KV9Pd3NIyK-g&callback=myMap"></script>