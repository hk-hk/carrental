<?php 

$activeLanguage = $this->session->userdata('active_language');

if($activeLanguage=='mk'){

    $who_we_are->title = ($who_we_are->title_mya!='')?$who_we_are->title_mya:$who_we_are->title;

    $who_we_are->content = ($who_we_are->content_mya!='')?$who_we_are->content_mya:$who_we_are->content;

    

    $what_we_do->title = ($what_we_do->title_mya!='')?$what_we_do->title_mya:$what_we_do->title;

    $what_we_do->content = ($what_we_do->content_mya!='')?$what_we_do->content_mya:$what_we_do->content;

}

        ?> 

<section class="about-box">

    <div class="about-inner">

        	<div class="col-sm-6 no_padding">

            <div class="about-txt pull-right">

              <h2 class="black section-title common-title"><?php echo $who_we_are->title; ?></h2>

              <?php echo $who_we_are->content; ?>

            </div>

          </div>

          <div class="col-sm-6 no_padding">

            <div class="about-image about-img1">

            </div>

          </div>

    </div>  

    <div class="about-inner">

          <div class="col-sm-6 no_padding">

              <div class="about-image about-img2">

              </div>

          </div> 

          <div class="col-sm-6 no_padding">

            <div class="about-txt">

              <h2 class="black section-title common-title"><?php echo $what_we_do->title; ?></h2>

             <?php echo $what_we_do->content; ?>

            </div>

          </div>

    </div>

</section><!-- About Box -->
<!--
<section class="count-renal-cars">

  <div class="container">

    <div class="row">

      

      <?php 

      if(isset($rental_statics[79])){

        foreach ($rental_statics[79] as $key => $stat) { ?>

          <div class="counter col-sm-3">

            <h2 class="timer count-title count-number" data-to="<?php echo $stat['short_desc']; ?>" data-speed="1500"></h2>

             <p class="count-text "><?php echo $stat['title']; ?></p>

          </div>

        <?php }

      }

      ?>

    </div>

  </div>

</section> Cars Counter -->



<?php //print_r($drivers);?>

<section class="our-drivers">

  <div class="container">

    <div class="row">

      <h2 class="section-title black text-center">Our Services</h2>

      <?php foreach ($drivers as $driver){  ?>

      <div class="col-sm-4">

        <div class="driver-box-innr">

          <div class="driver-image">

            <img src="uploads/article/<?php echo $driver->featured_img; ?>">

          </div>

          <div class="driver-detail">

            <h3><?php echo $driver->title; ?></h3>

            <?php echo $driver->content; ?>

          </div>

        </div>

      </div>

      <?php } ?>

    </div>

  </div>

</section>





