<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Articles</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    
          <div class="box-body">
          <h3 class="box-title">Filter</h3>
          <form id="form-filter" class="form-horizontal">
            <div class="row">
              <div class="form-group col-md-6">
                  <label for="LastName" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="title">
                  </div>
              </div>
              <div class="form-group col-md-6">
                  <label for="LastName" class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-10">
                      <select name="cat_id" id="cat_id" class="form-control">
                                <option value="0">Select</option>
                              <?php
                                foreach($content_category[30] as $val){ ?>
                                    <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                                <?php } ?>
                            </select>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                  <label for="LastName" class="col-sm-1 control-label"></label>
                  <div class="col-sm-11">
                      <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                      <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                  </div>
              </div>
            </div>
          </form>
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Featured Image</th>
                    <th>Title</th>
                    <th>Page Slug</th>
                    <!-- <th>Created Date</th>
                    <th>Modified Date</th> -->
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Featured Image</th>
                    <th>Title</th>
                    <th>Page Slug</th>
                    <!-- <th>Created Date</th>
                    <th>Modified Date</th> -->
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
          </div>
          </div>
          <!-- /.box-body -->
         <!--  <div class="box-footer">
            <button type="submit" class="btn btn-info">Add User</button>
          </div> -->
          <!-- /.box-footer -->
         
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
 
var table;
 
$(document).ready(function() {
 $('.datepicker').datepicker({
         autoclose: true
       });
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/article/ajax_article_list')?>",
            "type": "POST",
            "data": function (data) {		
                data.title = $('#title').val();
                data.cat_id = $('#cat_id').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    }); 
 
});
 
</script>