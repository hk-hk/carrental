<section class="attractive-place-box common-padding">
	<div class="container">
    	<div class="row">
            <?php foreach ($places_list as $key => $value) {
            ?>
            <div class="col-sm-4">
                <a href="<?php echo site_url();?>attractive-places/<?php echo $value->id;?>">
                <div class="places-box">
                    <?php if($value->featured_img!=''){?>
                    <div class="attraction-img">
                    <img src="<?php echo base_url();?>uploads/article/<?php echo $value->featured_img; ?>" alt="" />
                    </div>
                    <?php }?>
                    <div class="attraction-txt">
                    <?php $activeLanguage = $this->session->userdata('active_language');
                        if($activeLanguage=='mk'){
                            $value->title = ($value->title_mya!='')?$value->title_mya:$value->title;
                        }
                    ?>    
                    <?php 
                    $title = explode(',', $value->title);
                    ?>
                        <h5><?php if(isset($title[0]))echo $title[0];?></h5>
                        <p><?php if(isset($title[1]))echo $title[1];?></p>
                    </div>
                </div>    
                </a>
            </div>
            <?php 
            } ?>
            
            <?php if(count($places_list)>9){ ?>
            <div class="load-more-button col-sm-12">
                 <a href="javascript:void(0);" id="loadMore">Load More</a>
            </div>
            <?php } ?>
        </div>
    </div> 
</section>
