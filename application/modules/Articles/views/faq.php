<section class="faq-box common-padding">
	<div class="container">
    	<div class="row">
       <div class="col-sm-12 faq-page">
        <div class="faq-block">
            <div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group accordion-inq">
            <?php 
            $activeLanguage = $this->session->userdata('active_language');
            $i=0; foreach ($faq_list as $faq){ 
            if($activeLanguage=='mk'){
                $faq->title = ($faq->title_mya!='')?$faq->title_mya:$faq->title;
                $faq->content = ($faq->content_mya!='')?$faq->content_mya:$faq->content;
            }
                ?>
            <div class="panel panel-default">
              <div id="heading<?php echo $faq->id; ?>" role="tab" class="panel-heading">
                <h4 class="panel-title"> <a aria-controls="collapse<?php echo $faq->id; ?>" aria-expanded="true" href="#collapse<?php echo $faq->id; ?>" data-parent="#accordion" data-toggle="collapse" class="collapsed"> <?php echo $faq->title; ?> </a> </h4>
              </div>
              <div aria-labelledby="heading<?php echo $faq->id; ?>" role="tabpanel" class="panel-collapse collapse<?php echo ($i==0)?'in':'';?>" id="collapse<?php echo $faq->id; ?>">
                  <div class="panel-body">
                      <?php echo $faq->content; ?>
                  </div>
              </div>
            </div>
            <?php $i++; } ?>
            </div>
          
        </div>
      </div>
            
    </div> 
    </div>
</section>


