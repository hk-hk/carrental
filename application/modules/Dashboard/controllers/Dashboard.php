<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['module'] = 'Dashboard';
        $this->load->model('Dashboards' , 'dashboards');
        $this->load->model('Users/User','user'); // Load User modal
                $this->session->set_userdata('active_language','en');
    }

    public function index()
    {
        $this->isAuthorized(); // is authenticated user
                //$this->checkPermission('dashboard');
        $view = 'index'; // default file
                $role = $this->session->userdata('cr_user_role');
                //echo $role;die;
                if($role=='customer'){
                    redirect(site_url());
                }
                switch($role) { // check user type
                    case 'admin':   $view = $role.'_dashboard'; // super admin
                                    $this->data['count_users'] = 11;//$this->user->count_all();

                    break;

                    case 'finance_admin':   $view = 'admin_dashboard'; // super admin
                                    $this->data['count_users'] = 11;//$this->user->count_all();
                    case 'finance':   $view = 'admin_dashboard'; // super admin
                                    $this->data['count_users'] = 11;//$this->user->count_all();
                    break;
                    case 'tour_admin':
                                    redirect(site_url('admin/article/list')); // super admin
                                    $this->data['count_users'] = 11;//$this->user->count_all();
                    break;
                    case 'agent':       $view = $role.'_dashboard'; // admin
                                                $this->data['count_users'] = 6;
                    break;
                    case 'supplier':        $view = $role.'_dashboard'; // manager
                                                $this->data['count_users'] = $this->dashboards->get_supplier_cars();
                    break;
                    case 'service_provider':    $view = $role.'_dashboard'; // agent
                                                $this->data['count_users'] = 12;
                    break;
                    case 'customer':    $view = $role.'_dashboard'; // agent
                                                $this->data['count_users'] = 12;
                    break;
        }

                $this->data['current_orders'] = $this->dashboards->get_orders("current_orders");
                $this->data['orders_in_24'] = $this->dashboards->get_orders("orders_in_24");
                $this->data['orders_in_72'] = $this->dashboards->get_orders("orders_in_72");
                $this->data['new_orders'] = $this->dashboards->get_orders("new_orders");
                $this->data['completed_orders'] = $this->dashboards->get_orders("completed_orders");
                $this->data['monthly_orders'] = $this->dashboards->get_orders("monthly_orders");
                $this->data['other_orders'] = $this->dashboards->get_orders("other_orders");

                //fixed By MMA
                $this->data['today_pickup_orders'] = $this->dashboards->get_orders("today_pickup_orders");
                $this->data['today_reservations'] = $this->dashboards->get_orders("today_reservations");
                $this->data['today_orders_confirmed'] = $this->dashboards->get_orders("today_orders_confirmed");
                $this->data['today_orders_cancelled'] = $this->dashboards->get_orders("today_orders_cancelled");
                $this->data['pending_orders'] = $this->dashboards->get_orders("pending_orders");
                $this->data['confirmed_orders'] = $this->dashboards->get_orders("confirmed_orders");
                $this->data['tomorrow_pickup'] = $this->dashboards->get_orders('tomorrow_pickup');

                $this->data['dashboard_attribute'] = array(
                    'today_reservation' => count($this->dashboards->get_dashboard_attribute('today_reservation')),
                    'today_pickup' => count($this->dashboards->get_dashboard_attribute('today_pickup')),
                    'today_dropoff' => count($this->dashboards->get_dashboard_attribute('today_dropoff')),
                    'tomorrow_pickup' => count($this->dashboards->get_dashboard_attribute('tomorrow_pickup')),
                    'payment_pending' => count($this->dashboards->get_dashboard_attribute('payment_pending')),

                     //fixed By MMA
                    'today_pickup_orders' => count($this->dashboards->get_orders('today_pickup_orders')),
                    'today_reservations' => count($this->dashboards->get_orders('today_reservations')),
                    'today_orders_confirmed' => count($this->dashboards->get_orders('today_orders_confirmed')),
                    'today_orders_cancelled' => count($this->dashboards->get_orders('today_orders_cancelled')),
                    'pending_orders' => count($this->dashboards->get_orders('pending_orders')),
                    'confirmed_orders' => count($this->dashboards->get_orders('confirmed_orders')),
                    'tomorrow_pickup' => count($this->dashboards->get_orders('tomorrow_pickup')),

                );
        $this->data['inner_view'] = $view;
        $this->data['users'] = $this->user->get_many_by("user_role", "supplier");
        $this->data['view'] = 'index';

        $this->layout->admin($this->data);
    }
}
