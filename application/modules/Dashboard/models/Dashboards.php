<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboards extends CI_Model {

    public function index()
    {
        echo "hello";
        $this->load->view('welcome_message');
    }

    public function get_supplier_cars(){
            $userid = $this->session->userdata('crm_user_id');
            $query = "SELECT COUNT(id) as no_of_cars FROM `cr_manage_cars` WHERE `user_id`='".$userid."' && status!=2";
            $res = $this->db->query($query);
            $row = $res->row();
            if(is_object($row))
                return $row->no_of_cars;
            else
                return 0;
        }
    public function get_dashboard_attribute($condition)
    {
                $cr_user_role = $this->session->userdata('cr_user_role');
                $addWhere = '';
                if($cr_user_role=='supplier'){
                    $crm_user_id = $this->session->userdata('crm_user_id');
                    $addWhere = ' AND usr.id='.$crm_user_id;
                }else if($cr_user_role=='agent'){
                    $crm_user_id = $this->session->userdata('crm_user_id');
                    $addWhere = ' AND a.user_id='.$crm_user_id;
                }

        $query = "SELECT a.order_id FROM cr_orders as a
        LEFT JOIN cr_order_detail as b on a.order_id = b.order_id
        LEFT JOIN cr_payment_info as c on a.order_id = c.order_id
                LEFT JOIN cr_manage_cars as car on car.id = a.car_id
                LEFT JOIN cr_users as usr on usr.id = car.user_id ";
        switch ($condition) {
            case 'today_reservation':
                $query = $query.'WHERE DATE(a.booking_date) =  CURRENT_DATE()';
                break;
            case 'today_pickup':
                $query = $query.'WHERE DATE(a.pickup_date) = CURRENT_DATE()';
                break;
            case 'today_dropoff':
                $query = $query.'WHERE DATE(a.drop_date) = CURRENT_DATE()';
                break;
            case 'tomorrow_reservation':
                $query = $query.'WHERE DATE(a.pickup_date) = CURRENT_DATE() + INTERVAL 1 DAY';
                break;
            case 'payment_pending':
                $query = $query.'WHERE c.status = "Pending"';
                break;

            default:
                break;
        }
        $query = $query.$addWhere." GROUP BY a.order_id";
                //echo $query;die;
        $res = $this->db->query($query);
        return $res->result_object();
    }

    public function get_orders($condition) // Get total row count of search query
    {

        $query = "SELECT a.*,b.*, c.user_fname, c.user_lname, c.user_tel FROM cr_orders as a
        LEFT JOIN cr_users as c on a.user_id = c.id
        LEFT JOIN cr_order_detail as b on a.order_id = b.order_id ";
        /*
        case 'current_orders':
            $query = $query.'WHERE DATE_FORMAT(a.pickup_date,"%Y-%m-%d") = CURDATE() and a.status != "Cancelled"';
            break;
        case 'orders_in_24':
            $query = $query.'WHERE `pickup_date`>=DATE_ADD(NOW(), INTERVAL 1 DAY) AND `pickup_date`<=DATE_ADD(NOW(), INTERVAL 2 DAY)';
            break;
        case 'orders_in_72':
            $query = $query.'WHERE `pickup_date`>=DATE_ADD(NOW(), INTERVAL 2 DAY) AND `pickup_date`<=DATE_ADD(NOW(), INTERVAL 3 DAY)';
            break;
        case 'new_orders':
            $query = $query.'WHERE DATE_FORMAT(a.booking_date,"%Y-%m-%d") = CURDATE() and a.status != "Cancelled"';
            break;
        case 'completed_orders':
            $query = $query.'WHERE status = "Completed" AND (drop_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW())';
            break;
        case 'other_orders':
            $query = $query.'WHERE a.status = "Cancelled" AND (a.drop_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND DATE_ADD(NOW(), INTERVAL 30 DAY))';
            break;
        */
        switch ($condition) {


            case 'current_orders':
                $query = $query.'WHERE a.status = "Confirmed"';
                break;
            case 'orders_in_24':
                $query = $query.'WHERE `pickup_date`>=DATE_ADD(NOW(), INTERVAL 1 DAY) AND `pickup_date`<=DATE_ADD(NOW(), INTERVAL 2 DAY) AND a.status = "Confirmed"';
                break;
            case 'monthly_orders':
                $query = $query.'WHERE `rent_type`="38"';
                break;
            case 'orders_in_72':
                $query = $query.'WHERE `pickup_date`>=DATE_ADD(NOW(), INTERVAL 2 DAY) AND `pickup_date`<=DATE_ADD(NOW(), INTERVAL 3 DAY) AND a.status = "Confirmed"';
                break;
            case 'new_orders':
                $query = $query.'WHERE DATE_FORMAT(a.booking_date,"%Y-%m-%d") = CURDATE() or a.status = "Pending"';
                break;
            case 'completed_orders':
                $query = $query.'WHERE status = "Completed"';
                break;
            case 'other_orders':
                $query = $query.'WHERE a.status = "Cancelled"';
                break;

            //Fixed by MMA

            case 'today_pickup_orders' :
                $query = $query.'WHERE DATE_FORMAT(a.pickup_date,"%Y-%m-%d") = DATE(NOW()) and a.status = "Confirmed"';
                break;

            case 'today_reservations' :
                $query = $query.'WHERE DATE_FORMAT(a.booking_date,"%Y-%m-%d") = DATE(NOW())';
                break;


            case 'today_orders_confirmed' :
                $query = $query.'WHERE DATE_FORMAT(a.booking_date,"%Y-%m-%d") = DATE(NOW()) and a.status = "Confirmed"';
                break;

            case 'today_orders_cancelled' :
                $query = $query.'WHERE DATE_FORMAT(a.booking_date,"%Y-%m-%d") = DATE(NOW()) and a.status = "Cancelled"';
                break;


            case 'pending_orders':
                $query = $query.'WHERE a.status = "Pending"';
                break;

            case 'confirmed_orders':
                $query = $query.'WHERE a.status = "Confirmed"';
                break;

            case 'tomorrow_pickup':
                $query = $query.'WHERE DATE_FORMAT(a.pickup_date,"%Y-%m-%d") = DATE_ADD(DATE(NOW()), INTERVAL 1 DAY) and a.status != "Cancelled"';
                break;


            default:
                break;
        }
        if('monthly_orders' == $condition){
            //echo $query;die;
        }
        $res = $this->db->query($query);
        return $res->result_object();
    }
}


/*

current order       :   SELECT * FROM cr_orders WHERE cr_orders.pickup_date <= NOW() AND cr_orders.drop_date >= NOW()

last 24 hrs         :   SELECT * FROM cr_orders WHERE cr_orders.pickup_date > DATE_SUB(CURDATE(), INTERVAL 1 DAY)

from 24-72 hrs      :   SELECT * FROM cr_orders WHERE cr_orders.pickup_date < DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND cr_orders.pickup_date > DATE_SUB(CURDATE(), INTERVAL 3 DAY)

bookings in 48hrs   :   SELECT * FROM cr_orders WHERE cr_orders.booking_date > DATE_SUB(CURDATE(), INTERVAL 2 DAY)

completed order in 1 mnth   : SELECT * FROM cr_orders WHERE status = 'Completed' AND (drop_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW())


cancelled order in 1 mnth previous and before :
SELECT * FROM cr_orders WHERE status = 'Cancelled' AND (drop_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND DATE_ADD(NOW(), INTERVAL 30 DAY))



*/
