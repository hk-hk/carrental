<style>
   .box_new {
     background: #ffffff none repeat scroll 0 0;
     border-radius: 3px;
     border-top: 3px solid #d2d6de;
     box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
     margin-bottom: 20px;
     overflow: scroll;
     position: relative;
     /*width: 90%;*/
     height: 300px;
   }
   .box-new-header.with-border {
     background-color: #3c8dbc;
     border-bottom: 1px solid #f4f4f4;
     color: white;
     padding: 0 0 0 10px;
     /*width: 90%;*/
   }
   td{
    font-weight: 400;
   }

.container-fluid {
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto; }
  .card {
    margin-bottom: 30px;
    border: 0px;
    border-radius: 0.625rem;
    box-shadow: 6px 11px 41px -28px #a99de7;
}

.gradient-3 {
    color: #fff !important;
}

.card .card-body {
    padding: 1.88rem 1.81rem;
}

.card-title {

    font-size: 18px;
    font-weight: 500;
    line-height: 18px;
    margin-bottom: 0.75rem;
    height: 20px;

}
.text-white {
    color: #fff !important;
}

.d-inline-block {
    display: inline-block !important;
}
.gradient-1, .dropdown-mega-menu .ext-link.link-1 a, .morris-hover, .datamaps-hoverover {
  background-image: linear-gradient(230deg, #759bff, #843cf6); }

.gradient-2, .dropdown-mega-menu .ext-link.link-3 a {
  background-image: linear-gradient(230deg, #fc5286, #fbaaa2); }

.gradient-3, .dropdown-mega-menu .ext-link.link-2 a, .header-right .icons .user-img .activity {
  background-image: linear-gradient(230deg, #ffc480, #ff763b); }

.gradient-4, .sidebar-right .nav-tabs .nav-item .nav-link.active::after, .sidebar-right .nav-tabs .nav-item .nav-link.active span i::before {
  background-image: linear-gradient(230deg, #0e4cfd, #6a8eff); }

.gradient-5 {
  background-image: linear-gradient(to right, #f83600 0%, #f9d423 100%); }

.gradient-6 {
  background-image: linear-gradient(135deg, #97ABFF 10%, #123597 100%); }

.gradient-7 {
  background-image: linear-gradient(135deg, #3C8CE7 10%, #00EAFF 100%); }

.gradient-8 {
  background-image: linear-gradient(135deg, #EE9AE5 10%, #5961F9 100%); }

.gradient-anchor, .gradient-9 {
  background-image: linear-gradient(to right, #b8cbb8 0%, #b8cbb8 0%, #b465da 0%, #cf6cc9 33%, #ee609c 66%, #ee609c 100%); }

.gradient-anchor {
  -webkit-background-clip: text !important;
  -webkit-text-fill-color: transparent !important; }

/*!
</style>
<div class="container-fluid">
<div class="d-flex .flex-sm-column">
 <div class="col-lg-2 col-sm-2">
      <div class="card gradient-7">
          <div class="card-body">
              <h3 class="card-title text-white">Today Pick-up</h3>
              <div class="d-inline-block">
                  <h2 class="text-white"><?php echo $dashboard_attribute['today_pickup_orders']; ?>
                  </h2>
              </div>
        </div>
      </div>
  </div>
  <div class="col-lg-2 col-sm-2">
      <div class="card gradient-1">
          <div class="card-body">
              <h3 class="card-title text-white">Today Reservation</h3>
              <div class="d-inline-block">
                  <h2 class="text-white"><?php echo $dashboard_attribute['today_reservations']; ?></h2>
<!--                                   <p class="text-white mb-0">Jan - March 2019</p>
-->                              </div>
          </div>
      </div>
  </div>

  <div class="col-lg-2 col-sm-2">
      <div class="card gradient-4">
          <div class="card-body">
              <h3 class="card-title text-white">Today Confirmed</h3>
              <div class="d-inline-block">
                  <h2 class="text-white"><?php echo $dashboard_attribute['today_orders_confirmed']; ?></h2>
<!--                                   <p class="text-white mb-0">Jan - March 2019</p>
-->                              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-2 col-sm-2">
      <div class="card gradient-8">
          <div class="card-body">
              <h3 class="card-title text-white">Tomorrow Pickup</h3>
              <div class="d-inline-block">
                  <h2 class="text-white"><?php echo $dashboard_attribute['tomorrow_pickup']; ?></h2>
<!--                                   <p class="text-white mb-0">Jan - March 2019</p>
-->                              </div>
          </div>
      </div>
  </div>

   <div class="col-lg-2 col-sm-2">
      <div class="card gradient-3">
          <div class="card-body">
              <h3 class="card-title text-white">All Confirmed</h3>
              <div class="d-inline-block">
                  <h2 class="text-white"><?php echo $dashboard_attribute['confirmed_orders']; ?></h2>
<!--                                   <p class="text-white mb-0">Jan - March 2019</p>
-->                              </div>
              <span class="float-right display-5 opacity-5"></span>
          </div>
      </div>
  </div>

   <div class="col-lg-2 col-sm-2">
      <div class="card gradient-5">
          <div class="card-body">
              <h3 class="card-title text-white">All Pending</h3>
              <div class="d-inline-block">
                  <h2 class="text-white"><?php echo $dashboard_attribute['pending_orders']; ?></h2>
<!--                                   <p class="text-white mb-0">Jan - March 2019</p>
-->                              </div>
              <span class="float-right display-5 opacity-5"></span>
          </div>
      </div>
  </div>



   </div>
</div>

<!--     <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-aqua">
          <div class="inner">
              <h3>&nbsp;</h3>
             <p>Booking Calendar</p>
          </div>
          <div class="icon">
             <i class="ion ion-bag"></i>
          </div>
         <a href="<?php echo site_url(CRM_VAR.'/suppliers'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-aqua">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['today_reservation']; ?></h3>
             <p>New reservation today</p>
          </div>
          <div class="icon">
             <i class="ion ion-bag"></i>
          </div>
         <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-green">
          <div class="inner">
             <p><strong><?php echo $dashboard_attribute['today_pickup']; ?></strong> Pickups today</p><br>
             <p><strong><?php echo $dashboard_attribute['today_dropoff']; ?></strong> Drop offs today</p>
          </div>
          <div class="icon">
             <i class="ion ion-stats-bars"></i>
          </div>
          <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-yellow">
            <div class="inner">
             <h3><?php echo $dashboard_attribute['tomorrow_reservation']; ?></h3>
             <p>Reservation Tomorrow</p>
          </div>
          <div class="icon">
             <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>

      <div class="col-lg-3 col-xs-6">
       <div class="card gradient-3">
          <div class="card-body">
              <h3 class="card-title text-white">New Customers</h3>
              <div class="d-inline-block">
                  <h2 class="text-white">4565</h2>
                  <p class="text-white mb-0">Jan - March 2019</p>
              </div>
              <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
          </div>
      </div>
    </div> -->
    <!-- <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-red">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['payment_pending']; ?></h3>
             <p>Payment Pendigs</p>
          </div>
          <div class="icon">
             <i class="ion ion-pie-graph"></i>
          </div>
           <a href="<?php echo site_url(CRM_VAR.'/order/list/Pending'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
</div> -->



<div class="row">
  <div class="col-md-6">
    <!--- box --->
    <div class="box-new-header with-border">
        <h3 class="box-title"><!-- New/Pending Orders -->Today Pick-up</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example5" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Trip Record Name</th>
                    <th>Vendor Name</th>
                    <th>Vendor Amount</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>

                    <?php if(!empty($today_pickup_orders)) { ?>

                    <?php foreach ($today_pickup_orders as $k => $order1) {

                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $order1->trip_record_name; ?></td>
                    <td> <?php foreach ($users as $k => $user) {
                      ?>
                      <?php if ($order1->supplier_id == $user->id) {
                        $__vendor_tel=$user->user_tel;
                        echo $user->user_fname . " " . $user->user_lname;
                      }?>
                      <?php
                    }?></td>

                    <td><?php echo $order1->vendor_fees; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
    <!--- End --->

        <!-- /.box-body -->
     </div>
     <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title"><!-- Orders in 72 Hrs -->Today Confirmed
</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example4" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
<!--                   <tbody>
                  <?php if(!empty($orders_in_72)) { ?>

                    <?php foreach ($orders_in_72 as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody> -->

                <tbody>
                  <?php if(!empty($today_orders_confirmed)) { ?>

                    <?php foreach ($today_orders_confirmed as $k => $order1) {

                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody>

                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
<!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title"><!-- Cancelled Orders -->All Confirmed Orders
</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example7" class="table table-bordered table-hover">
<!--                 <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead> -->

                         <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>

                   <tbody>
                  <?php if(!empty($confirmed_orders)) { ?>

                    <?php foreach ($confirmed_orders as $k => $order1) {
                    $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);

                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody>
                <!--   <tbody>
                  <?php if(!empty($other_orders)) { ?>

                    <?php foreach ($other_orders as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);

                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ',');; ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody>
 -->

                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->



     <!-- /.box -->
     <!-- /.box -->
<!--      <div class="box-new-header with-border">
        <h3 class="box-title">Monthly Orders</h3>
     </div> -->
<!--      <div class="box_new">
 -->        <!-- /.box-header -->
<!--         <div class="box-body">
 --><!--            <table id="example8" class="table table-bordered table-hover">
 --> <!--                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead> -->
                  <!-- <tbody>
                  <?php if(!empty($monthly_orders)) { ?>

                    <?php foreach ($monthly_orders as $k => $order1) {
                        //if($order1->order_id==1098){
                         // print_r($order1);die;
                        //}
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = $order1->booking_amount;//(isset($history->bookingPrice)?$history->bookingPrice:'1.00');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);                    ?>
                     <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ',');; ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody> -->
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
<!--             </table>
 -->
<!-- </div> -->
        <!-- /.box-body -->
<!--      </div>
 -->     <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-6">
     <div class="box-new-header with-border">
        <h3 class="box-title"><!-- Orders in 24 Hrs -->Today Reservation</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example3" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>


                  <?php if(!empty($today_reservations)) { ?>

                    <?php foreach ($today_reservations as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = $order1->booking_amount;//(isset($history->bookingPrice)?$history->bookingPrice:'1.00');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody>


                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title"><!-- Confirmed Orders -->Tomorrow Pickup
</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>

                  <tbody>
                  <?php if(!empty($tomorrow_pickup)) { ?>

                    <?php foreach ($tomorrow_pickup as $k => $order1) {


                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody>

                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
       <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title"><!-- Reservation Orders -->All Pending Orders
</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example9" class="table table-bordered table-hover">


                  <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>

                   <tbody>
                  <?php if(!empty($pending_orders)) { ?>

                    <?php foreach ($pending_orders as $k => $order1) {


                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php
                    } ?>

                  <?php }?>
                  </tbody>

                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->





  </div>
  <!-- /.col -->
</div>

    <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example3').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example4').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example5').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example6').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example7').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example8').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example9').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example10').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
