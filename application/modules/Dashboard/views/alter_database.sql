ALTER TABLE `cr_payment_info` ADD `invoice_no` VARCHAR( 30 ) NOT NULL AFTER `order_id` ;


ALTER TABLE `cr_order_detail` ADD `usr_email` VARCHAR( 55 ) NOT NULL ;


 <!-- Wednesday 16 August 2017--->
 ALTER TABLE `cr_orders` CHANGE `booking_date` `booking_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP; 
ALTER TABLE `cr_users` CHANGE `user_role` `user_role` ENUM( 'admin', 'customer', 'agent', 'supplier', 'guest' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'customer';


<!--  Thursday 10 August 2017 --->
ALTER TABLE `cr_manage_cars` ADD `ratings` FLOAT( 10, 2 ) NULL DEFAULT '0';


<!--- Tuesday 18 July 2017 -->
ALTER TABLE `cr_manage_cars` ADD `car_city` INT( 11 ) NOT NULL ;

 
 <!--Monday 17 July 2017 -->

ALTER TABLE `cr_manage_cars` ADD `city_rate_b` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'City Rate (Basic) (Car+Driver)',
ADD `city_rate_ai` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'City Rate (All Inclusive) (Car+Driver+Fuel)',
ADD `city_hourly_rate_b` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'City Hourly Rate (Basic) (Car+Driver)',
ADD `city_hourly_rate_ai` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'City Hourly Rate (All Inclusive) (Car+Driver+Fuel)',
ADD `hightway_rate_b` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'Hightway Rate (Basic) (Car+Driver)',
ADD `hightway_rate_ai` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'Hightway Rate (All Inclusive) (Car+Driver+Fuel)',
ADD `half_day_avail` FLOAT( 10, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'Half Day Available?';


UPDATE `rtcisinl_car-rental-as`.`cr_manage_cars` SET `airport_pickup` = '0.00',`airport_pickoff` = '0.00' WHERE 1;
ALTER TABLE `cr_manage_cars` CHANGE `airport_pickup` `airport_pickup_rate` FLOAT( 10, 2 ) NOT NULL ;
ALTER TABLE `cr_manage_cars` CHANGE `airport_pickoff` `airport_pickoff_rate` FLOAT( 10, 2 ) NOT NULL ;