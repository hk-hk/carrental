<div class="row">

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
            <h3>&nbsp;</h3>
          <p>Booking calendar</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="<?php echo site_url(CRM_VAR.'/booking-calendar'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo $count_users; ?></h3>

          <p>Car Registered</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="<?php echo site_url(CRM_VAR.'/car_list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-aqua">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['today_reservation']; ?></h3>
             <p>New reservation today</p>
          </div>
          <div class="icon">
             <i class="ion ion-bag"></i>
          </div>
            <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-green">
          <div class="inner">
             <p><strong><?php echo $dashboard_attribute['today_pickup']; ?></strong> Pickups today</p><br>
             <p><strong><?php echo $dashboard_attribute['today_dropoff']; ?></strong> Drop offs today</p>
          </div>
          <div class="icon">
             <i class="ion ion-stats-bars"></i>
          </div>
           <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-red">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['payment_pending']; ?></h3>
             <p>Payment Pendigs</p>
          </div>
          <div class="icon">
             <i class="ion ion-pie-graph"></i>
          </div>
           <a href="<?php echo site_url(CRM_VAR.'/order/list/Pending');; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>

</div>

<div class="row">
<!-- Left col -->

<!-- right col -->
</div>