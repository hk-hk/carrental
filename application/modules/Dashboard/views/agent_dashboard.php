<div class="row">
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-aqua">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['today_reservation']; ?></h3>
             <p>New reservation today</p>
          </div>
          <div class="icon">
             <i class="ion ion-bag"></i>
          </div>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-green">
          <div class="inner">
             <p><strong><?php echo $dashboard_attribute['today_pickup']; ?></strong> Pickups today</p><br>
             <p><strong><?php echo $dashboard_attribute['today_dropoff']; ?></strong> Drop offs today</p>
          </div>
          <div class="icon">
             <i class="ion ion-stats-bars"></i>
          </div>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-red">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['payment_pending']; ?></h3>
             <p>Payment Pendigs</p>
          </div>
          <div class="icon">
             <i class="ion ion-pie-graph"></i>
          </div>
       </div>
    </div>

</div>

<div class="row">
<!-- Left col -->

<!-- right col -->
</div>