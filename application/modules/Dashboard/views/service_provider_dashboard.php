<style>
   .box_new {
     background: #ffffff none repeat scroll 0 0;
     border-radius: 3px;
     border-top: 3px solid #d2d6de;
     box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
     margin-bottom: 20px;
     overflow: scroll;
     position: relative;
     /*width: 90%;*/
     height: 300px;
   }
   .box-new-header.with-border {
     background-color: #3c8dbc;
     border-bottom: 1px solid #f4f4f4;
     color: white;
     padding: 0 0 0 10px;
     /*width: 90%;*/
   }
   td{
    font-weight: 400;
   }
</style>
<div class="row">
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-aqua">
          <div class="inner">
              <h3>&nbsp;</h3>
             <p>Booking Calendar</p>
          </div>
          <div class="icon">
             <i class="ion ion-bag"></i>
          </div>
         <a href="<?php echo site_url(CRM_VAR.'/suppliers'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-aqua">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['today_reservation']; ?></h3>
             <p>New reservation today</p>
          </div>
          <div class="icon">
             <i class="ion ion-bag"></i>
          </div>
         <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-green">
          <div class="inner">
             <p><strong><?php echo $dashboard_attribute['today_pickup']; ?></strong> Pickups today</p><br>
             <p><strong><?php echo $dashboard_attribute['today_dropoff']; ?></strong> Drop offs today</p>
          </div>
          <div class="icon">
             <i class="ion ion-stats-bars"></i>
          </div>
          <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-yellow">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['tomorrow_reservation']; ?></h3>
             <p>Reservation Tomorrow</p>
          </div>
          <div class="icon">
             <i class="ion ion-person-add"></i>
          </div>
          <a href="<?php echo site_url(CRM_VAR.'/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
    <!-- <div class="col-lg-3 col-xs-6">
       <div class="small-box bg-red">
          <div class="inner">
             <h3><?php echo $dashboard_attribute['payment_pending']; ?></h3>
             <p>Payment Pendigs</p>
          </div>
          <div class="icon">
             <i class="ion ion-pie-graph"></i>
          </div>
           <a href="<?php echo site_url(CRM_VAR.'/order/list/Pending'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
       </div>
    </div>
</div> -->

<div class="row">
  <div class="col-md-6">
    <!--- box --->
    <div class="box-new-header with-border">
        <h3 class="box-title">New/Pending Orders</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example5" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(!empty($new_orders)) { ?>
                  
                    <?php foreach ($new_orders as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php 
                    } ?>
                  
                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
    <!--- End --->
     
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title">Orders in 72 Hrs</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example4" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(!empty($orders_in_72)) { ?>
                  
                    <?php foreach ($orders_in_72 as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php 
                    } ?>
                  
                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title">Completed Orders</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example6" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(!empty($completed_orders)) { ?>
                 
                    <?php foreach ($completed_orders as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);                    ?>
                     <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ',');; ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php 
                    } ?>
                  
                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
     <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title">Monthly Orders</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example8" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(!empty($monthly_orders)) { ?>
                 
                    <?php foreach ($monthly_orders as $k => $order1) {
                        //if($order1->order_id==1098){
                         // print_r($order1);die;
                        //}
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = $order1->booking_amount;//(isset($history->bookingPrice)?$history->bookingPrice:'1.00');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);                    ?>
                     <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ',');; ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php 
                    } ?>
                  
                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-6">
     <div class="box-new-header with-border">
        <h3 class="box-title">Orders in 24 Hrs</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example3" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(!empty($orders_in_24)) { ?>
                  
                    <?php foreach ($orders_in_24 as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php 
                    } ?>
                  
                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title">Confirmed Orders</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(!empty($current_orders)) { ?>
                  
                    <?php foreach ($current_orders as $k => $order1) { 
                        
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ','); ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php 
                    } ?>
                  
                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
     <div class="box-new-header with-border">
        <h3 class="box-title">Cancelled Orders</h3>
     </div>
     <div class="box_new">
        <!-- /.box-header -->
        <div class="box-body">
           <table id="example7" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php if(!empty($other_orders)) { ?>
                  
                    <?php foreach ($other_orders as $k => $order1) {
                        $history = (json_decode($order1->order_history));
                        $bookingPrice = (isset($history->bookingPrice)?$history->bookingPrice:'');
                        if(is_object($history)){
                            $carname = isset($history->carinfo->carname)?$history->carinfo->carname:'';
                        }else{
                            $carname = '';
                        }
                        $currency = strtoupper($history->currency);
                        
                    ?>
                    <tr><td><a href="<?php echo site_url();?>admin/order/view/<?php echo $order1->order_id; ?>"><?php echo $order1->order_id; ?></a></td>
                    <td><?php echo $order1->name; ?></td>
                    <td><?php echo $order1->phone; ?></td>
                    <td><?php echo $carname; ?></td>
                    <td><?php echo $currency.number_format($bookingPrice, 0, ".", ',');; ?></td>
                    <td><?php echo $order1->pickup_date; ?></td>
                    <td><?php echo $order1->drop_date; ?></td></tr>
                    <?php 
                    } ?>
                  
                  <?php }?>
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Car Name</th>
                    <th>Booking Amount</th>
                    <th>Pickup Date/Time</th>
                    <th>Dropoff Date/Time</th>
                  </tr>
                </tfoot> -->
            </table>
        </div>
        <!-- /.box-body -->
     </div>
     <!-- /.box -->
  </div>
  <!-- /.col -->
</div>

    <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example3').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example4').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example5').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example6').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example7').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
        $('#example8').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
    