<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tool extends MY_Controller {

	public function getStates() {
		$this->load->model('ToolModel');
		$country = (int) $this->input->get('country',true);
		$data = $this->ToolModel->create_options('crm_states',array('id_country'=>$country),array('name_state'=>'ASC'));
		echo json_encode(array('records'=>$data));
		exit;
	}

	public function getCitySuggestion() {
		$this->load->model('ToolModel');
		$city = $this->input->get('term',true);
		$sql = "SELECT name_city FROM locations WHERE name_city LIKE '%".$city."%' GROUP BY name_city ORDER BY name_city";
		$data = $this->ToolModel->run_query($sql);
		$cities = [];
		if (count($data) > 0) {
			foreach ($data as $city) {
				$cities[]['label'] = $city['name_city'];
			}
		}
		//print_r($cities);
		//exit();
		echo json_encode($cities);
		exit;
	}
	public function getPcodeSuggestion() {

		$this->load->model('ToolModel');
		$pcode = $this->input->get('term',true);
		$sql = "SELECT postcode FROM locations WHERE postcode LIKE '%".$pcode."%' GROUP BY postcode ORDER BY postcode";
		$data = $this->ToolModel->run_query($sql);
		$postcodes = [];
		if (count($data) > 0) {
			foreach ($data as $code) {
				$postcodes[]['label'] = $code['postcode'];
			}
		}
		//print_r($cities);
		//exit();
		echo json_encode($postcodes);
		exit;
	}

	public function getTypes() {

		$this->load->model('ToolModel');
		$category = (int) $this->input->get('category',true);
		$sql = "SELECT id_type,name_type FROM object_types WHERE id_category=$category ORDER BY name_type";
		$data = $this->ToolModel->run_query($sql);

		echo json_encode(array('res'=>$data));
		exit;
	}

	

}