<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Account extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->isAuthorized(); // Check user authorization
        $this->data['module'] = 'Accounts'; // Load Users module

        $this->load->model('Accounts', 'account'); // Load User modal
        $this->load->model('Airlines', 'airline'); // Load User modal
        $this->load->model('Vendors', 'vendor'); // Load User modal
        $this->load->model('Agents', 'agent'); // Load User modal
        $this->load->model('Banks', 'banks'); // Load User modal
        $this->load->model('Transfers', 'transfers'); // Load User modal
        //
        $this->load->model('Users/User', 'users');
        $this->load->model('Attributes/Attributes', 'attributes');
        $this->load->model('Transation/Transations', 'transations');
        $role               = $this->session->userdata('cr_user_role');
        $this->data['role'] = $role;
        $this->session->set_userdata('active_language', 'mm');
    }

    public function accounts($id = null)
    {
        // $this->isAuthorized(); // check user is authoried or not

        $this->data['account_result'] = $this->account->get_by('account_number', 0);
        if ($id == 10) {
            $this->data['view']     = 'cashaccount'; //Load all user's list page
        } else {
            $this->data['banks']    = $this->banks->get_all();
            $this->data['accounts'] = $this->account->get_all_with_bank();
            
            $this->data['view']     = 'account'; //Load all user's list page
            if ($id == null) {
                $this->data['isEdit'] = false;
            } else {
                $this->data['account_detail'] = $this->account->get_by('account_id', $id);
                $this->data['isEdit']         = true;
            }
        }
        $this->layout->admin($this->data);
    }

    public function store_account($id = null)
    {
        // $postData['account_id']         = $this->input->post('accountname');
        $postData['account_name']   = $this->input->post('accountname');
        $postData['account_number'] = $this->input->post('accountnumber');
        $postData['account_type']   = $this->input->post('accounttype');
        if ($this->input->post('accounttype') == 1) {
            $postData['account_price'] = $this->input->post('accountprice');
        } else{
            $postData['account_dollar'] = $this->input->post('accountprice');
        }

        if($id==10){
            $postData['account_price'] = $this->input->post('accountprice');
            $postData['account_dollar'] = $this->input->post('accountdollar');
        }

        $postData['account_date'] = $this->input->post('accountdate');
        $postData['account_bank'] = $this->input->post('bank');

        if ($id == null) {
            $this->account->insert($postData);
        } else {
            $this->account->update($id, $postData);
        }
        redirect('admin/accounts');
    }
// $id = null
    public function transfer()
    {
        // $postData['account_id']         = $this->input->post('accountname');
        // $this->isAuthorized(); // check user is authoried or not

        $this->data['account_result'] = $this->account->get_by('account_number', 0);
        $this->data['banks']          = $this->banks->get_all();
        $accounts = $this->account->get_all_with_bank();
        $this->data['accounts'] = $accounts;
        // $this->data['accounts'] = $this->account->get_all();
        $banklist=array();
        $mmktotal=0;
        $usdtotal=0;
        foreach ($accounts as $key => $value) {
            if($value->account_bank){
                $next=$key+1;
                if($value->account_type==1){
                    $mmktotal+=$value->account_price;
                }
                if($value->account_type==2){
                    $usdtotal+=$value->account_dollar;
                }
                if(!($accounts[$key]->account_bank==$accounts[(count($accounts)!=$next)? $next : $key]->account_bank && $next!=count($accounts))){
                    $banklist[$value->ums_bank]['mmk']=$mmktotal;
                    $banklist[$value->ums_bank]['usd']=$usdtotal;
                    $mmktotal=0;
                    $usdtotal=0;
                }
            }
        }
        $this->data['banklist']=$banklist;
        // $account_detail               = $this->account->get_by('account_id', $id);
        // $this->data['account_detail'] = $account_detail;
        $this->data['view']           = 'transfer'; //Load all user's list page
        if (!empty($_POST)) {
            $account_detail = $this->account->get_by('account_id', $this->input->post('from_account'));
            $mainacc        = $this->account->get_by('account_id', $this->input->post('to_account'));
            
            $transferData['from_account'] = $this->input->post('from_account');
            $transferData['account_id'] = $this->input->post('to_account');
            $transferData['tran_date']  = date('d/m/Y');
            $cashtype=$mainacc->account_type;
            $transferAmount=0;
            $addbalance=0;
            $leavebalance=0;
            // if ($account_detail->account_type != $mainacc->account_type) {
                // $transferData['transfer_dollar']    = $this->input->post('transferamount_usd');
                // $leaveAccData['account_dollar'] = $account_detail->account_dollar - $transferData['transfer_dollar'];
                // $addAccData['account_dollar']   = $mainacc->account_dollar + $transferData['transfer_dollar'];
                // $transferAmount=$transferData['transfer_dollar'];
                // $addbalance=$mainacc->account_dollar;
                // $leavebalance=$account_detail->account_dollar;
                // $cashtype=$mainacc->account_dollar;

                $notexchanged_amount=0;
                if ($account_detail->account_type == 1) {
                    $transferData['transfer_amount']   = $this->input->post('transferamount_mmk');
                    $leaveAccData['account_price'] = $account_detail->account_price - $transferData['transfer_amount'];
                    $addAccData['account_price']   = $mainacc->account_price + $transferData['transfer_amount'];
                    $transferAmount=$transferData['transfer_amount'];
                    $addbalance=$mainacc->account_price;
                    $leavebalance=$account_detail->account_price;
                    // $cashtype=$mainacc->account_dollar;
                } elseif ($account_detail->account_type == 2) {
                    $transferData['transfer_dollar']    = $this->input->post('transferamount_usd');
                    $leaveAccData['account_dollar'] = $account_detail->account_dollar - $transferData['transfer_dollar'];
                    $addAccData['account_dollar']   = $mainacc->account_dollar + $transferData['transfer_dollar'];
                    $transferAmount=$transferData['transfer_dollar'];
                    $addbalance=$mainacc->account_dollar;
                    $leavebalance=$account_detail->account_dollar;
                    // $cashtype=$mainacc->account_dollar;
                }
                if ($account_detail->account_type > $mainacc->account_type) {
                    $transferData['transfer_amount']   = $this->input->post('transferamount_usd');
                    $leaveAccData['account_dollar'] = $account_detail->account_dollar - $transferData['transfer_dollar'];
                    $addAccData['account_price']   =  ($this->input->post('exchange_rate') * $transferData['transfer_amount']) + $mainacc->account_price;
                    unset($addAccData['account_dollar']);
                     // + $mainacc->account_price
                    $transferAmount=($this->input->post('exchange_rate') * $transferData['transfer_amount']);
                    $addbalance=$mainacc->account_price;
                    $notexchanged_amount=$this->input->post('transferamount_usd');

                    // $leavebalance=$account_detail->account_price;
                    // $cashtype=$mainacc->account_dollar;
                    // echo "USD - MMK";
                    // print_r($addAccData);
                } elseif ($account_detail->account_type < $mainacc->account_type) {
                    // echo "MMK - USD";
                    $transferData['transfer_amount']   = $this->input->post('transferamount_mmk');
                    $leaveAccData['account_price'] = $account_detail->account_price - $transferData['transfer_amount'];
                    $addAccData['account_dollar']   =  ceil($transferData['transfer_amount'] / $this->input->post('exchange_rate')) + $mainacc->account_dollar;
                    unset($addAccData['account_price']);
                    // $transferData['transfer_dollar']    = $this->input->post('transferamount_usd');
                    // $leaveAccData['account_dollar'] = $account_detail->account_dollar - $transferData['transfer_dollar'];
                    // $addAccData['account_dollar']   = $mainacc->account_dollar + $transferData['transfer_dollar'];
                    $transferAmount=ceil($transferData['transfer_amount'] / $this->input->post('exchange_rate'));
                    $addbalance=$mainacc->account_dollar;
                    $notexchanged_amount=$this->input->post('transferamount_mmk');
                    // $leavebalance=$account_detail->account_dollar;
                    // $cashtype=$mainacc->account_dollar;
                }
            $transfer_id=$this->transfers->insert($transferData);
            // $this->attributes->get_by('title', 'Withdraw')->id,
            $transationAddData=array(
                'category_id'       => $this->attributes->get_by('title', 'Internal Transfer')->id,
                'cash_type'         => $mainacc->account_type,
                'account_id'        => $mainacc->account_id,
                'amount'            => $transferAmount,
                'account_balance'   => $addbalance,
                'final_balance'     => $addbalance + $transferAmount,
                'linked_id'         => $transfer_id,
                'description'       => $this->input->post('comment'),
                'payment_type'      => $this->attributes->get_by('title', 'Deposit')->id,
                'created_at'        => time(),
                'created_by'        => $this->session->userdata("crm_user_id"),
                'transaction_date'  => strtotime($this->input->post('transaction_date'))
            );


            $transationLeaveData=array(
                'category_id'       => $this->attributes->get_by('title', 'Internal Transfer')->id,
                'cash_type'         => $account_detail->account_type,
                'account_id'        => $account_detail->account_id,
                'amount'            => ($notexchanged_amount!=0)? $notexchanged_amount : $transferAmount,
                'account_balance'   => $leavebalance,
                'final_balance'     => $leavebalance - (($notexchanged_amount!=0)? $notexchanged_amount : $transferAmount),
                'linked_id'         => $transfer_id,
                'description'       => $this->input->post('comment'),
                'payment_type'      => $this->attributes->get_by('title', 'Withdraw')->id,
                'created_at'        => time(),
                'created_by'        => $this->session->userdata("crm_user_id"),
                'transaction_date'  => strtotime($this->input->post('transaction_date'))
            );

            // $data['post_data'] = $_POST;
            // print "<pre/>";
            // // // print_r();
            // print_r($data);
            // // echo "transfer";
            // // print_r($transferData);
            // // echo "leave";
            // // print_r($leaveAccData);
            // // echo "add";
            // // print_r($addAccData);
            // // echo "mainacc";
            // // print_r($mainacc);
            // // echo "toacc";
            // // print_r($account_detail);
            
            // echo "Add trandata";
            // print_r($transationAddData);

            // echo "Leave trandata";
            // print_r($transationLeaveData);
            
            
            $this->account->update($account_detail->account_id, $leaveAccData);
            $this->account->update($mainacc->account_id, $addAccData);
            $this->transations->insert($transationAddData);
            $this->transations->insert($transationLeaveData);
            redirect('admin/transfer/' . $id);
        } else {

            $this->layout->admin($this->data);
        }
    }
    public function transaction($id)
    {
        // $this->isAuthorized();
        $account_detail               = $this->account->get_by('account_id', $id);
        $this->data['bank']           = $this->banks->get_by('ums_id', $account_detail->account_bank);
        $this->data['account_detail'] = $account_detail;
        $this->data['payment_type_withdraw']   = $this->attributes->get_by('title', 'Withdraw')->id;
        $this->data['payment_type_deposit']   = $this->attributes->get_by('title', 'Deposit')->id;
        $this->data['transactions']   = $this->transations->get_many_by_order('account_id', $account_detail->account_id);
        $this->data['view']           = 'transaction';
        // print "<pre/>";
        // print_r($this->data);
        $this->layout->admin($this->data);
    }

    public function inout(){
        $this->data['account_result'] = $this->account->get_by('account_number', 0);
        $this->data['banks']          = $this->banks->get_all();
        $accounts = $this->account->get_all_with_bank();
        $this->data['accounts'] = $accounts;
        $this->data['categories'] = $this->attributes->get_many_by_order('parent_id', 4418);
        $banklist=array();
        $mmktotal=0;
        $usdtotal=0;
        foreach ($accounts as $key => $value) {
            if($value->account_bank){
                $next=$key+1;
                if($value->account_type==1){
                    $mmktotal+=$value->account_price;
                }
                if($value->account_type==2){
                    $usdtotal+=$value->account_dollar;
                }
                if(!($accounts[$key]->account_bank==$accounts[(count($accounts)!=$next)? $next : $key]->account_bank && $next!=count($accounts))){
                    $banklist[$value->ums_bank]['mmk']=$mmktotal;
                    $banklist[$value->ums_bank]['usd']=$usdtotal;
                    $mmktotal=0;
                    $usdtotal=0;
                }
            }
        }
        $this->data['banklist']=$banklist;

        $this->data['payment_type_withdraw']   = $this->attributes->get_by('title', 'Withdraw')->id;
        $this->data['payment_type_deposit']   = $this->attributes->get_by('title', 'Deposit')->id;
        $this->data['users']           = $this->attributes->get_many_by_order('parent_id',4443);
        $this->data['view']           = 'inout';
        
        // print "<pre/>";
        // print_r($this->data['users']);
        $this->layout->admin($this->data);   
    }

    public function store_inout(){

        $account=$this->account->get_by('account_id',$this->input->post('account'));
        $inoutData['category_id']=$this->input->post('category');
        $inoutData['cash_type']=$account->account_type;
        $inoutData['amount']=$this->input->post('amount');
        if($account->account_type==1){
            $inoutData['account_balance']=$account->account_price;
        }else{
            $inoutData['account_balance']=$account->account_dollar;
        }

        // $inoutData['linked_id']=$this->input->post('order_id');
        $inoutData['description']=$this->input->post('description');
        $inoutData['payment_person']=$this->input->post('person_name');
        $inoutData['payment_type']=$this->input->post('payment_type');
        $inoutData['created_at']=time();
        $inoutData['transaction_date']=strtotime($this->input->post('transaction_date'));
        $inoutData['created_by']=$this->session->userdata("crm_user_id");
        $inoutData['account_id']=$account->account_id;

        $persondata=$this->attributes->get_by("title",$inoutData['payment_person']);
        if(empty($persondata->title)){
            // echo $persondata->title;
            $personAdd['title'] = $inoutData['payment_person'];
            $personAdd['parent_id'] = 4443;
            $personAdd['status'] = 1;
            $this->attributes->insert($personAdd);
        }


        $accountUpdate=array();
        if($account->account_type==1){

            if($inoutData['payment_type']==4416){
                $accountUpdate['account_price'] = $account->account_price - $this->input->post('amount');
                // $accountUpdate['account_price'] = $account->account_price;
            }else if($inoutData['payment_type']==4417){
                $accountUpdate['account_price'] = $account->account_price + $this->input->post('amount');
            }
            $inoutData['final_balance']=$accountUpdate['account_price'];
        }else{

            if($inoutData['payment_type']==4416){
                $accountUpdate['account_dollar'] = $account->account_dollar - $this->input->post('amount');
                // $accountUpdate['account_dollar'] = $account->account_dollar;
            }else if($inoutData['payment_type']==4417){
                $accountUpdate['account_dollar'] = $account->account_dollar + $this->input->post('amount');
            }
            $inoutData['final_balance']=$accountUpdate['account_dollar'];
        }
        
        $thumb_array = array(
            0 => array("height" => '250', "width" => '350'),
            1 => array("height" => '54', "width" => '72'),
        );

        if (!empty($_FILES['upload']['name'][0])) {
            if (count($_FILES['upload']['name']) < 10) {
                $banner_result = $this->multiple_upload('upload', 'file_attachments', $thumb_array, './uploads/file_attachments');
                $old_files = array();
                if (is_array($old_files) && is_array($banner_result['file_data'])) {
                    $inoutData["file_attachments"] = json_encode(array_merge($old_files, $banner_result['file_data']));
                } else {
                    if (is_array($banner_result['file_data'])) {
                        $inoutData["file_attachments"] = json_encode($banner_result['file_data']);
                    }
                }
                if ($banner_result['error'] != 0) {
                    $this->form_validation->set_rules("upload[]", "All Valid upload Image", "required");
                }
            }
        }

        $this->account->update($account->account_id, $accountUpdate);
        $this->transations->insert($inoutData);
        redirect(site_url() . CRM_VAR . '/inout'); 
        // print "<pre/>";
        // print_r($inoutData);
    }
    public function airline($id){
        $airlines = $this->airline->get_by('id',$id);
        echo json_encode($airlines);
    }
    public function vendor($id){
        $vendors = $this->vendor->get_by('id',$id);
        echo json_encode($vendors);
    }
    public function prepaid_vendor_transfer(){
        
        $this->data['account_result'] = $this->account->get_by('account_number', 0);
        $this->data['banks']          = $this->banks->get_all();
        $accounts = $this->account->get_all_with_bank();
        $this->data['accounts'] = $accounts;
        $vendors = $this->vendor->getVendors(true);
        $banklist=array();
        $mmktotal=0;
        $usdtotal=0;
        foreach ($accounts as $key => $value) {
            if($value->account_bank){
                $next=$key+1;
                if($value->account_type==1){
                    $mmktotal+=$value->account_price;
                }
                if($value->account_type==2){
                    $usdtotal+=$value->account_dollar;
                }
                if(!($accounts[$key]->account_bank==$accounts[(count($accounts)!=$next)? $next : $key]->account_bank && $next!=count($accounts))){
                    $banklist[$value->ums_bank]['mmk']=$mmktotal;
                    $banklist[$value->ums_bank]['usd']=$usdtotal;
                    $mmktotal=0;
                    $usdtotal=0;
                }
            }
        }
        $this->data['banklist']=$banklist;
        $this->data['vendors']=$vendors;
        $this->data['expend_vendors']=$this->vendor->getVendors();
        $this->data['view']           = 'prepaid_vendor_transfer'; //Load all user's list page
        if (!empty($_POST)) {

            $mainacc    = $this->account->get_by('account_id', $this->input->post('from_account'));
            $vendor    = $this->vendor->get_by('id', $this->input->post('to_account'));
            
            $cashtype=$mainacc->account_type;
            $transferAmount=0;
            // $addbalance=0;
            $leavebalance=0;
            $vendorAdd=array();
                // $notexchanged_amount=0;
                if ($cashtype == 1) {
                //     $transferData['transfer_amount']   = $this->input->post('transferamount_mmk');
                    $transferAmount=$this->input->post('transfer_amount');
                    $leaveAccData['account_price'] = $mainacc->account_price - $transferAmount;
                    $vendorAdd['balance_mmk']   = $vendor->balance_mmk-$transferAmount;
                //     $addbalance=$mainacc->account_price;
                    
                 
                    $leavebalance=$mainacc->account_price;
                //     // $cashtype=$mainacc->account_dollar;
                } elseif ($cashtype == 2) {
                //     $transferData['transfer_dollar']    = $this->input->post('transferamount_usd');
                    $transferAmount=$this->input->post('transfer_amount');
                    $leaveAccData['account_dollar'] = $mainacc->account_dollar - $transferAmount;
                    
                    $vendorAdd['balance_usd']   = $vendor->balance_usd-$transferAmount;
                //     $addbalance=$mainacc->account_dollar;
                    $leavebalance=$mainacc->account_dollar;
                //     // $cashtype=$mainacc->account_dollar;
                }
            $this->vendor->update($this->input->post('to_account'),$vendorAdd);
           
            $transationLeaveData=array(
                'category_id'       => $this->attributes->get_by('title', 'Air Ticket Vendor Payment')->id,
                'cash_type'         => $cashtype,
                'account_id'        => $mainacc->account_id,
                'amount'            => $transferAmount,
                'account_balance'   => $leavebalance,
                'final_balance'     => $leavebalance - $transferAmount,
                'linked_id'         => $this->input->post('to_account'),
                'description'       => $this->input->post('comment'),
                'payment_type'      => $this->attributes->get_by('title', 'Withdraw')->id,
                'created_at'        => time(),
                'payment_person '   => $vendor->vendor_name, 
                'created_by'        => $this->session->userdata("crm_user_id"),
                'transaction_date'  => strtotime($this->input->post('transaction_date'))
            );

            
            $this->account->update($mainacc->account_id, $leaveAccData);
            // $this->account->update($mainacc->account_id, $addAccData);
            // $this->transations->insert($transationAddData);
            $this->transations->insert($transationLeaveData);
            // print "<pre/>";
            // print_r($_POST);
            // exit();
            redirect('admin/prepaid_vendor_transfer/' . $id);
        } else {

            $this->layout->admin($this->data);
        }
    }
    public function agent($id){
        $agents = $this->agent->get_by('id',$id);
        echo json_encode($agents);
    }
    public function prepaid_agent_transfer(){
        
        $this->data['account_result'] = $this->account->get_by('account_number', 0);
        $this->data['banks']          = $this->banks->get_all();
        $accounts = $this->account->get_all_with_bank();
        $this->data['accounts'] = $accounts;
        $agents = $this->agent->getAgents(true);

        $banklist=array();
        $mmktotal=0;
        $usdtotal=0;
        foreach ($accounts as $key => $value) {
            if($value->account_bank){
                $next=$key+1;
                if($value->account_type==1){
                    $mmktotal+=$value->account_price;
                }
                if($value->account_type==2){
                    $usdtotal+=$value->account_dollar;
                }
                if(!($accounts[$key]->account_bank==$accounts[(count($accounts)!=$next)? $next : $key]->account_bank && $next!=count($accounts))){
                    $banklist[$value->ums_bank]['mmk']=$mmktotal;
                    $banklist[$value->ums_bank]['usd']=$usdtotal;
                    $mmktotal=0;
                    $usdtotal=0;
                }
            }
        }
        $this->data['banklist']=$banklist;
        $this->data['agents']=$agents;

        $this->data['expend_agents']=$this->agent->getAgents();
        $this->data['view']           = 'prepaid_agent_transfer'; //Load all user's list page
        if (!empty($_POST)) {

            $mainacc    = $this->account->get_by('account_id', $this->input->post('from_account'));
            $agent    = $this->agent->get_by('id', $this->input->post('to_account'));
            
            $cashtype=$mainacc->account_type;
            $transferAmount=0;
            // $addbalance=0;
            $leavebalance=0;
            $agentAdd=array();
                // $notexchanged_amount=0;
                if ($cashtype == 1) {
                //     $transferData['transfer_amount']   = $this->input->post('transferamount_mmk');
                    $transferAmount=$this->input->post('transfer_amount');
                    $leaveAccData['account_price'] = $mainacc->account_price - $transferAmount;
                    $agentAdd['balance_mmk']   = $agent->balance_mmk-$transferAmount;
                //     $addbalance=$mainacc->account_price;
                    
                 
                    $leavebalance=$mainacc->account_price;
                //     // $cashtype=$mainacc->account_dollar;
                } elseif ($cashtype == 2) {
                //     $transferData['transfer_dollar']    = $this->input->post('transferamount_usd');
                    $transferAmount=$this->input->post('transfer_amount');
                    $leaveAccData['account_dollar'] = $mainacc->account_dollar - $transferAmount;
                    
                    $agentAdd['balance_usd']   = $agent->balance_usd-$transferAmount;
                //     $addbalance=$mainacc->account_dollar;
                    $leavebalance=$mainacc->account_dollar;
                //     // $cashtype=$mainacc->account_dollar;
                }
            $this->agent->update($this->input->post('to_account'),$agentAdd);
           
            $transationLeaveData=array(
                'category_id'       => $this->attributes->get_by('title', 'Air Ticket Prepaid Agent Payment')->id,
                'cash_type'         => $cashtype,
                'account_id'        => $mainacc->account_id,
                'amount'            => $transferAmount,
                'account_balance'   => $leavebalance,
                'final_balance'     => $leavebalance - $transferAmount,
                'linked_id'         => $this->input->post('to_account'),
                'description'       => $this->input->post('comment'),
                'payment_type'      => $this->attributes->get_by('title', 'Withdraw')->id,
                'created_at'        => time(),
                'payment_person '   => $agent->agent_name, 
                'created_by'        => $this->session->userdata("crm_user_id"),
                'transaction_date'  => strtotime($this->input->post('transaction_date'))
            );

            
            $this->account->update($mainacc->account_id, $leaveAccData);
            // $this->account->update($mainacc->account_id, $addAccData);
            // $this->transations->insert($transationAddData);
            $this->transations->insert($transationLeaveData);
            // print "<pre/>";
            // print_r($_POST);
            // exit();
            redirect('admin/prepaid_agent_transfer/' . $id);
        } else {

            $this->layout->admin($this->data);
        }
    }
    public function airline_transfer()
    {
        
        $this->data['account_result'] = $this->account->get_by('account_number', 0);
        $this->data['banks']          = $this->banks->get_all();
        $accounts = $this->account->get_all_with_bank();
        $this->data['accounts'] = $accounts;
        $airlines = $this->airline->getAirlines(true);
        // $this->data['accounts'] = $this->account->get_all();
        $banklist=array();
        $mmktotal=0;
        $usdtotal=0;
        foreach ($accounts as $key => $value) {
            if($value->account_bank){
                $next=$key+1;
                if($value->account_type==1){
                    $mmktotal+=$value->account_price;
                }
                if($value->account_type==2){
                    $usdtotal+=$value->account_dollar;
                }
                if(!($accounts[$key]->account_bank==$accounts[(count($accounts)!=$next)? $next : $key]->account_bank && $next!=count($accounts))){
                    $banklist[$value->ums_bank]['mmk']=$mmktotal;
                    $banklist[$value->ums_bank]['usd']=$usdtotal;
                    $mmktotal=0;
                    $usdtotal=0;
                }
            }
        }
        $this->data['banklist']=$banklist;
        $this->data['airlines']=$airlines;
        $this->data['expend_airlines']=$this->airline->getAirlines();
        $this->data['view']           = 'airline_transfer'; //Load all user's list page
        if (!empty($_POST)) {

            $mainacc    = $this->account->get_by('account_id', $this->input->post('from_account'));
            $airline    = $this->airline->get_by('id', $this->input->post('to_account'));
            
            // $transferData['from_account'] = $this->input->post('from_account');
            // $transferData['account_id'] = $this->input->post('to_account');
            // $transferData['tran_date']  = date('d/m/Y');
            $cashtype=$mainacc->account_type;
            $transferAmount=0;
            // $addbalance=0;
            $leavebalance=0;
            $airlineAdd=array();
                // $notexchanged_amount=0;
                if ($cashtype == 1) {
                //     $transferData['transfer_amount']   = $this->input->post('transferamount_mmk');
                    $transferAmount=$this->input->post('transfer_amount');
                    $leaveAccData['account_price'] = $mainacc->account_price - $transferAmount;
                    $airlineAdd['deposit_amount_mmk']   = $airline->deposit_amount_mmk+$transferAmount;
                //     $addbalance=$mainacc->account_price;
                    
                 
                    $leavebalance=$mainacc->account_price;
                //     // $cashtype=$mainacc->account_dollar;
                } elseif ($cashtype == 2) {
                //     $transferData['transfer_dollar']    = $this->input->post('transferamount_usd');
                    $transferAmount=$this->input->post('transfer_amount');
                    $leaveAccData['account_dollar'] = $mainacc->account_dollar - $transferAmount;
                    
                    $airlineAdd['deposit_amount_usd']   = $airline->deposit_amount_usd+$transferAmount;
                //     $addbalance=$mainacc->account_dollar;
                    $leavebalance=$mainacc->account_dollar;
                //     // $cashtype=$mainacc->account_dollar;
                }
                // if ($account_detail->account_type > $mainacc->account_type) {
                //     $transferData['transfer_amount']   = $this->input->post('transferamount_usd');
                //     $leaveAccData['account_dollar'] = $account_detail->account_dollar - $transferData['transfer_dollar'];
                //     $addAccData['account_price']   =  ($this->input->post('exchange_rate') * $transferData['transfer_amount']) + $mainacc->account_price;
                //     unset($addAccData['account_dollar']);
                //      // + $mainacc->account_price
                //     $transferAmount=($this->input->post('exchange_rate') * $transferData['transfer_amount']);
                //     $addbalance=$mainacc->account_price;
                //     $notexchanged_amount=$this->input->post('transferamount_usd');

                //     // $leavebalance=$account_detail->account_price;
                //     // $cashtype=$mainacc->account_dollar;
                //     // echo "USD - MMK";
                //     // print_r($addAccData);
                // } elseif ($account_detail->account_type < $mainacc->account_type) {
                //     // echo "MMK - USD";
                //     $transferData['transfer_amount']   = $this->input->post('transferamount_mmk');
                //     $leaveAccData['account_price'] = $account_detail->account_price - $transferData['transfer_amount'];
                //     $addAccData['account_dollar']   =  ceil($transferData['transfer_amount'] / $this->input->post('exchange_rate')) + $mainacc->account_dollar;
                //     unset($addAccData['account_price']);
                //     // $transferData['transfer_dollar']    = $this->input->post('transferamount_usd');
                //     // $leaveAccData['account_dollar'] = $account_detail->account_dollar - $transferData['transfer_dollar'];
                //     // $addAccData['account_dollar']   = $mainacc->account_dollar + $transferData['transfer_dollar'];
                //     $transferAmount=ceil($transferData['transfer_amount'] / $this->input->post('exchange_rate'));
                //     $addbalance=$mainacc->account_dollar;
                //     $notexchanged_amount=$this->input->post('transferamount_mmk');
                //     // $leavebalance=$account_detail->account_dollar;
                //     // $cashtype=$mainacc->account_dollar;
                // }
            // $transfer_id=
                // $airlineAdd['airline_name']="Air Asia";
                // print_r($airlineAdd);
            $this->airline->update($this->input->post('to_account'),$airlineAdd);
            // $this->attributes->get_by('title', 'Withdraw')->id,
            // $transationAddData=array(
            //     'category_id'       => $this->attributes->get_by('title', 'Internal Transfer')->id,
            //     'cash_type'         => $cashtype,
            //     'account_id'        => $mainacc->account_id,
            //     'amount'            => $transferAmount,
            //     'account_balance'   => $addbalance,
            //     'final_balance'     => $addbalance + $transferAmount,
            //     'linked_id'         => $transfer_id,
            //     'description'       => $this->input->post('comment'),
            //     'payment_type'      => $this->attributes->get_by('title', 'Deposit')->id,
            //     'created_at'        => time(),
            //     'created_by'        => $this->session->userdata("crm_user_id"),
            //     'transaction_date'  => strtotime($this->input->post('transaction_date'))
            // );
            

            $transationLeaveData=array(
                'category_id'       => $this->attributes->get_by('title', 'Air Lines Deposit')->id,
                'cash_type'         => $cashtype,
                'account_id'        => $mainacc->account_id,
                'amount'            => $transferAmount,
                'account_balance'   => $leavebalance,
                'final_balance'     => $leavebalance - $transferAmount,
                'linked_id'         => $this->input->post('to_account'),
                'description'       => $this->input->post('comment'),
                'payment_type'      => $this->attributes->get_by('title', 'Withdraw')->id,
                'created_at'        => time(),
                'payment_person '   => $airline->airline_name, 
                'created_by'        => $this->session->userdata("crm_user_id"),
                'transaction_date'  => strtotime($this->input->post('transaction_date'))
            );

            
            $this->account->update($mainacc->account_id, $leaveAccData);
            // $this->account->update($mainacc->account_id, $addAccData);
            // $this->transations->insert($transationAddData);
            $this->transations->insert($transationLeaveData);
            // print "<pre/>";
            // print_r($_POST);
            // exit();
            redirect('admin/airline_transfer/' . $id);
        } else {

            $this->layout->admin($this->data);
        }
    }
}
