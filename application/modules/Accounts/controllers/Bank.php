<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bank extends MY_Controller
{
	public function __construct() {
		parent::__construct(); 
        // $this->isAuthorized(); // Check user authorization
        $this->data['module'] = 'Accounts'; // Load Users module

		$this->load->model('Banks', 'banks'); // Load User modal
        // 
        $role =  $this->session->userdata('cr_user_role'); 
        $this->data['role'] = $role;
        $this->session->set_userdata('active_language','mm');
	}

	function banks($id=null){
        // $this->isAuthorized(); // check user is authoried or not
        
        $this->data['view'] = 'bank'; //Load all user's list page
        if($id==null){
            $this->data['isEdit']=false;
        }else{
            $this->data['bank_detail'] = $this->banks->get_by('ums_id',$id);
            $this->data['isEdit']=true;
        }
        $this->layout->admin($this->data);		
	}

    function store_bank($id=null){
        
        $postData['ums_bank'] = $this->input->post('bank_name');
        
        // exit();
        if($id==null){
            $this->banks->insert($postData);

            redirect('admin/banks');
        }else{
            $this->banks->update($id,$postData);    
            redirect('admin/banks/'.$id);
        }

   }
   function delete_bank($id=null){
        $this->banks->delete($id);
        redirect('admin/banks');
   }

    function banks_list(){
        // $this->checkPermission('car_list');
        $list = $this->banks->get_all();  
        $data = array();
        $no = 0;
        foreach ($list as $info) {
            $show = '';
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $info->ums_bank;
            if(FALSE) // Check for manager user.
            { 
                $row[] = $show.'<a href="'.site_url(CRM_VAR).'/banks/'.$info->ums_id.'"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;';
            }
            else{
                // <a href="'.site_url(CRM_VAR).'/car_list/'.$info->ums_id.'"><button class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></button></a>&nbsp;
                $row[] = $show.'<a href="'.site_url(CRM_VAR).'/banks/'.$info->ums_id.'"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;
                <form method="POST" action="'.site_url(CRM_VAR).'/delete_bank/'.$info->ums_id.'" accept-charset="UTF-8" style="display:inline"><button class="btn btn-danger btn-sm change-status" type="submit" data_status="2" onclick="return confirm(\'Confirm delete?\')"><i class="fa fa-remove"></i></button></form>';
                
            }
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->banks->count_all(),
            "recordsFiltered" => $this->banks->count_all(),
            "data" => $data,
        );
        echo json_encode($output);
    }
}