<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Transaction</a></li>
                </ul>
                <div class="tab-content">
                    
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <th>Account Name (<?= ($account_detail->account_type==1)? 'MMK' : '$' ?>)</th>
                                        <td><?= $account_detail->account_name ?></td>
                                    </tr>
                                    <tr>
                                        <th>Account Number</th>
                                        <td><?= $account_detail->account_number ?></td>
                                    </tr>
                                    <tr>
                                        <th>Account Price (<?= ($account_detail->account_type==1)? 'MMK' : '$' ?>)</th>
                                        <td><?= ($account_detail->account_type==1)? $account_detail->account_price : $account_detail->account_dollar ?></td>
                                    </tr>
                                    <tr>
                                        <th>Bank</th>
                                        <td><?= $bank->ums_bank ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Transfer Date</th>
                                            <th>Deposit Amount (<?= ($account_detail->account_type==1)? 'MMK' : '$' ?>)</th>
                                            <th>Withdraw Amount (<?= ($account_detail->account_type==1)? 'MMK' : '$' ?>)</th>
                                            <th>Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($transactions as $key => $value): ?>
                                        <tr>
                                            
                                            <td><?= date('d/m/Y h:i',$value->created_at) ?></td>
                                            <td>
                                                <?php if ($value->payment_type == $payment_type_deposit): ?>
                                                    <?= $value->amount  ?>
                                                <?php else: ?>
                                                    -
                                                <?php endif ?>
                                            </td>
                                            <td>
                                                <?php if ($value->payment_type == $payment_type_withdraw): ?>
                                                    <?= $value->amount  ?>
                                                <?php else: ?>
                                                    -
                                                <?php endif ?>
                                            </td>
                                            <td><?= $value->final_balance ?></td>
                                        </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        // $('#dataTables-example').DataTable({
        //         responsive: true
        // });
    });
</script>