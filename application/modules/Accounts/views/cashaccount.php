<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Cash Account</a></li>
                </ul>
                <div class="tab-content">
                    <!-- general form elements -->
                    <!-- <div class="box box-info"> -->
                    <div class="tab-pane active" id="tab_1">
                        <form role="form" action="<?=site_url(CRM_VAR . '/update_account/' . $account_result->account_id)?>" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Account's Name:</label>
                                    <input class="form-control" placeholder="Account Name" name="accountname" value="<?php echo $account_result->account_name; ?>">
                                    
                                </div>
                                <div class="form-group">
                                    <label>Total Amount(MMK):</label>
                                    <!-- name="accountprice"  -->
                                    <input type="hidden" id="accp" value="<?php echo $account_result->account_price;?>">
                                    <input class="form-control" placeholder="Total Amount" name="accountprice" id="accountprice" value="<?php echo $account_result->account_price;?>" readonly>
                                    <input type="number" class="form-control" placeholder="Amount To Add Price" id="addmmk" value="" onkeyup="changeamount()">
                                </div>
                                <div class="form-group">
                                    <label>Total Amount($):</label>
                                    <input type="hidden" id="accd" value="<?php echo $account_result->account_dollar;?>">
                                    <!-- name="accountdollar"  -->
                                    <input class="form-control" placeholder="Total Amount" name="accountdollar" id="accountdollar" value="<?php echo $account_result->account_dollar;?>" readonly>
                                    <input type="number" class="form-control" placeholder="Amount To Add Price" id="addusd" value="" onkeyup="changeamount()">
                                </div>
                                

                                <div class="form-group">
                                    <label>Account Date</label>
                                    <input class="form-control" placeholder="Account Date" name="accountdate" value="<?php echo $account_result->account_date;?>" readonly>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button id="submit" type="submit" class="btn btn-info pull-right">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    var accp=$('#accp').val();
    var accd=$('#accd').val();
    function changeamount(){
        if($('#addmmk').val()>0){
            $('#accountprice').val(parseInt(accp)+parseInt($('#addmmk').val()));

        }else{
            $('#accountprice').val(accp);
        }
        if($('#addusd').val()>0){
            $('#accountdollar').val(parseInt(accd)+parseInt($('#addusd').val()));
        }else{
            $('#accountdollar').val(accd);
        }
    }
</script>