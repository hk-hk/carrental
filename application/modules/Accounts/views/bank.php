<?php
if(!$isEdit){
$tab1='active';
$tab2='';
}else{
$tab1='';
$tab2='active';
}
// print_r($maintain_detail);
?>
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_1" data-toggle="tab">Bank List</a></li>
        </ul>
        <div class="tab-content">
          <!-- general form elements -->
          <!-- <div class="box box-info"> -->
          <div class="tab-pane active" id="tab_1">
            <?php
              $site_url= ($isEdit)? '/update_bank/'.$bank_detail->ums_id : '/save_bank/';
            ?>
            <form id="form-filter" class="form-horizontal" action="<?php echo site_url(CRM_VAR.$site_url) ?>" method="post">
              
              <?php if($role !='supplier') { ?>
              <div class="form-group">
                <label for="user_id" class="col-sm-2 control-label">Bank Name</label>
                <div class="col-sm-4">
                    <input type="text" name="bank_name" class="form-control" value="<?= !empty($bank_detail->ums_bank)? $bank_detail->ums_bank: '';  ?>">
                </div>
              </div>
              <?php } ?>
              <!-- <div class="form-group">
                <label for="car_type" class="col-sm-2 control-label">Car Type</label>
                <div class="col-sm-4">
                  <select name="car_type" id="fcar_type" class="form-control">
                    <option value="0">Select Car Type</option>
                    <?php
                    foreach($car_attributes[4] as $val){ ?>
                    <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                    <?php }
                    ?>
                  </select>
                </div>
              </div> -->
              
              <div class="form-group">
                <label for="LastName" class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <button type="submit" id="btn-filter" class="btn btn-primary"><?= ($isEdit)? 'Update' : 'Save' ?></button>
                  <!-- <button type="button" id="btn-reset" class="btn btn-default">Reset</button> -->
                </div>
              </div>
            </form>
            <table id="oiltable" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Bank Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              
              <tfoot>
              <tr>
                <th>No</th>
                <th>Bank Name</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
        
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
    </div>
    <!-- /.row -->
  </div>
</section>

<?php if(TRUE){?>
<script type="text/javascript">
var table;
$(document).ready(function() {
  table = $('#oiltable').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/banks_list')?>",
            "type": "POST"
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,2 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
  
});
</script>
<?php } ?>