<?php
if (!$isEdit) {
$tab1 = 'active';
$tab2 = '';
} else {
$tab1 = '';
$tab2 = 'active';
}
// print_r($maintain_detail);
?>
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="<?=$tab1?>"><a href="#tab_1" data-toggle="tab">Account List</a></li>
          <li class="<?=$tab2?>">
            <a href="#tab_2" data-toggle="tab">
              <?php if ($isEdit): ?>
                Update Bank Account
              <?php else: ?>
                Create New Bank Account
              <?php endif ?>
            </a>
          </li>
        </ul>
        <div class="tab-content">
          <!-- general form elements -->
          <!-- <div class="box box-info"> -->
          <div class="tab-pane <?=$tab1?>" id="tab_1">
           
            <table id="oiltable" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Account Name</th>
                  <th>Account Number</th>
                  <th>Account Price</th>
                  <th>Bank</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($accounts as $key => $value): ?>
                <?php if ($value->account_number != 0): ?>
                <tr>
                  <td><?=$value->account_name?><?=($value->account_type == 1) ? ' (MMK)' : ' ($)'?></td>
                  <td><?=$value->account_number?></td>
                  <td><?=($value->account_type == 1) ? $value->account_price : $value->account_dollar?></td>
                  <td>
                    <?php foreach ($banks as $k => $v): ?>
                    <?php if ($v->ums_id == $value->account_bank): ?>
                    <?=$v->ums_bank?>
                    <?php endif?>
                    <?php endforeach?>
                  </td>
                  <td><?=$value->account_date?></td>
                  <td><a href="<?=site_url(CRM_VAR . '/transaction/' . $value->account_id)?>">Transation</a> | <a href="<?=site_url(CRM_VAR . '/accounts/' . $value->account_id)?>">Edit</a> 
                    <!-- | <a href="">Delete</a></td> -->
                </tr>
                <?php endif?>
                <?php endforeach?>
              </tbody>
              <tfoot>
              <tr>
                <th>Account Name</th>
                <th>Account Number</th>
                <th>Account Price</th>
                <th>Bank</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <div class="tab-pane <?=$tab2?>" id="tab_2">
            <?php
            $site_url = ($isEdit) ? '/update_account/' . $account_detail->account_id : '/save_account/';
            ?>
            <form role="form" action="<?=site_url(CRM_VAR . $site_url)?>" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label>Account's Name:</label>
                  <input class="form-control" placeholder="Account Name" name="accountname" value="<?=isset($account_detail) ? $account_detail->account_name : '';?>" required="required">
                </div>
                <div class="form-group">
                  <label>Account Number:</label>
                  <input class="form-control" placeholder="Account Number" name="accountnumber"  value="<?=isset($account_detail) ? $account_detail->account_number : '';?>" required="required">
                </div>
                <div class="form-group">
                  <label>Account Type:</label>
                  <!-- <input class="form-control" placeholder="Account Number" name="accountnumber"  value="<?=isset($account_detail) ? $account_detail->account_number : '';?>" required="required"> -->
                  <label><input type="radio" name="accounttype" value="1" <?=isset($account_detail) &&  $account_detail->account_type==1 ? 'checked' : '';?> > MMK </label>
                  <label><input type="radio" name="accounttype" value="2" <?=isset($account_detail) &&  $account_detail->account_type==2 ? 'checked' : '';?> > USD </label>
                </div>
                <?php if (isset($account_detail) && $account_detail->account_type == 1) {?>
                <div class="form-group">
                  <label>Account Price(MMK):</label>
                  <input class="form-control" placeholder="Account Price" name="accountprice" value="<?=isset($account_detail) ? $account_detail->account_price : '';?>" required="required">
                </div>
                <?php } else if (isset($account_detail) && $account_detail->account_type == 2) {?>
                <div class="form-group">
                  <label>Account Price($):</label>
                  <input class="form-control" placeholder="Account Price" name="accountprice" value="<?=isset($account_detail) ? $account_detail->account_dollar : '';?>" required="required">
                </div>
                <?php }else{?>
                <div class="form-group">
                  <label>Account Price:</label>
                  <input class="form-control" placeholder="Account Price" name="accountprice" value="" required="required">
                </div>
                <?php }?>
                <div class="form-group">
                  <label>Account Date:</label>
                  <input class="form-control datepicker" placeholder="Account Date" name="accountdate" value="<?=isset($account_detail) ? $account_detail->account_date : '';?>">
                </div>
                <div class="form-group">
                  <label>Banks</label>
                  <select name="bank" id="select" class="form-control">
                    <option value="0">Please select</option>
                    <?php foreach ($banks as $key => $value): ?>
                    <?php if ($value->ums_id == $account_detail->account_bank): ?>
                    <option value="<?php echo $value->ums_id ?>" selected="selected"><?php echo $value->ums_bank; ?></option>
                    <?php else: ?>
                    <option value="<?php echo $value->ums_id ?>"><?php echo $value->ums_bank; ?></option>
                    <?php endif?>
                    <?php endforeach?>
                  </select>
                  <!-- <input class="form-control" placeholder="Account Date" name="accountdate" value="<?php echo $account_detail->account_date; ?>" readonly="readonly"> -->
                </div>
              </div>
              <div class="box-footer">
                <button id="submit" type="submit" class="btn btn-info pull-right"><?=($isEdit) ? 'Update' : 'Save'?></button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
    </div>
    <!-- /.row -->
  </div>
</section>
<?php if (true): ?>
<script type="text/javascript">
$(document).ready(function() {
$('.datepicker').datepicker({
autoclose: true
});
});
</script>
<?php endif?>