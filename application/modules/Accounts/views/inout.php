<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
<style>
.control-label{
text-align: left !important;
text-decoration: none;
}
.values{
font-weight: initial !important;
}
.fa .fa-star {
color: #ff0000 !important;
}
</style>

<!-- rating Pop-up -->
<section class="content">
  <!-- /.row -->
 
  <div class="row">
    <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Current Available Balance - MMK Total: USD Total: </h3>
            </div>
            <div class="box-body">

              <div class="table-responsive">
               <table cellspacing="0" width="100%">
                  <tbody>

                    <tr>
                      <?php foreach ($banklist as $key => $value): ?>
                          <td>
                            <table  class="table table-bordered">
                              <tbody>
                                <tr class="text-center">
                                  <td colspan="2"><?=$key?></td>
                                </tr>
                                <tr>
                                  <td>MMK</td>
                                  <td><?=$value['mmk'] ? '<font color="green">' . $value['mmk'] . '</font>' : '<font color="red">No Balance</font>'?></td>
                                </tr>
                                <tr>
                                  <td>USD</td>
                                  <td><?=$value['usd'] ? '<font color="green">' . $value['usd'] . '</font>' : '<font color="red">No Balance</font>'?></td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                      <?php endforeach?>


                    </tr>


                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    <div class="col-md-12">
      <div class="box box-warning ">   <!--  -->
      <div class="box-header with-border">
        <h3 class="box-title">General Transations (Income / Expense)</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <form action="<?php echo site_url(CRM_VAR . '/store_inout') ?>" class="form-horizontal" method="post" id="payment_form" enctype="multipart/form-data">
        <!-- <input type="hidden" name="order_id" value="<?=''?>"> -->
        <div class="box-body">
          <!-- <h3 class="box-title">Filter</h3> -->
          <!--  -->
          <div id="own_expense">
            <div class="row">
              <div class="col-md-6">
                <!-- <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" placeholder="Choose Date" name="maintain_date" id="maintain_date" value="<?=!empty($maintain_detail->date) ? $maintain_detail->date : ''?>" required="" />
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="category" class="col-md-4 control-label">General Category</label>
                  <div class="col-md-8">
                    <!-- <input type="hidden" name="category" id="tt"> -->
                    <select class="form-control selectpicker" id="category" name="category"  data-live-search="true">
                      <?php foreach ($categories as $key => $value): ?>
                        <option value="<?= $value->id ?>"><?= $value->title ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="payment_type" class="col-md-4 control-label">Payment Type</label>
                  <div class="col-md-8">
                    <!-- <input type="hidden" name="payment_type" id="tt"> -->
                    <select class="form-control" id="payment_type" name="payment_type">
                      <option value="<?= $payment_type_withdraw ?>">Withdraw (Expense)</option>
                      <option value="<?= $payment_type_deposit ?>">Deposit (Income)</option>
                    </select>
                  </div>
                </div>

               <div class="form-group">
                  <label for="account" class="col-md-4 control-label">Account</label>
                  <div class="col-md-8">
                    <!-- <input type="hidden" name="account" id="tt"> -->
                    <select class="form-control" id="account" name="account">
                      <?php foreach ($accounts as $key => $value): ?>
                        <?php if ($value->account_id==31): ?>
                          <option value="<?= $value->account_id ?>" selected><?= $value->account_name ?></option>
                        <?php else: ?>
                          <option value="<?= $value->account_id ?>"><?= $value->account_name ?></option>
                        <?php endif ?>
                      <?php endforeach ?>
                      <!-- <option value="<?= $payment_type_deposit ?>">Deposit (Payback)</option> -->
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="amount" class="col-md-4 control-label">Amount</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="amount" name="amount" min="1" value="" required="">
                  </div>
                </div>
                
                
                
              </div>
              <div class="col-md-6">
               
                <div class="form-group">
                  <label for="account" class="col-md-4 control-label">Pay To / Recieved From</label>
                  <div class="col-md-8">
                    <select class="form-control  selectpicker" id="person_name"  data-live-search="true" required="">
                      <option value="">Please Select</option>
                      <option value="other">Other</option>
                      <?php foreach ($users as $key => $value): ?>
                        <option value="<?= $value->id ?>"><?= $value->title ?></option>
                      <?php endforeach ?>

                    </select>
                    

                  </div>
                </div>

                <div class="form-group" id="other_div">
                  <label for="account" class="col-md-4 control-label"></label>
                  <div class="col-md-8">
                    <input type="text" id="other" class="form-control" name="person_name" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Transaction Date" class="col-md-4 control-label">Transaction Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" name="transaction_date" value="<?= date('m/d/Y') ?>" required="">
                  </div>
                </div>

                <div class="form-group">
                  <label for="description" class="col-md-4 control-label">Description</label>
                  <div class="col-md-8">
                    <textarea name="description" id="description" class="form-control" required=""></textarea>
                    <!-- <input type="number" class="form-control" id="exchange_amount" name="exchange_amount" min="0" value="0" readonly=""> -->
                  </div>
                </div>
                <div class="form-group">
                    <label for="upload" class="col-md-4 control-label">Upload</label>
                    <div class="col-md-8">
                      <input type="file" name="upload[]" value="" multiple>
                    </div>
                  </div>
              </div>
              
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="pull-right">
            
            <button type="submit" class="btn btn-info" onclick="return confirm('Please Check Again! Are you sure to submit?')">Submit</button>
            
          </div>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">List</h3>
        </div>
        <div class="box-body">
          
          <div class="table-responsive">
           <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <!-- <th>No</th> -->
                  <th>Created At</th>
                  <th>Transaction Date</th>
                  <th>Category</th>
                  <th>Description</th>
                  <th>Process By</th>
                  <th>Account</th>
                  <th>Payment To/From</th>
                  <!-- th -->
                  <th>Deposit</th>
                  <th>Withdraw</th>
                  <th >Final Balance</th>
                  <!-- <th>Cash MMK Balance</th> -->
                  <!-- <th>Cash $ Balance</th> -->
                  
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
              
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
</section>

<?php if(TRUE){?>
<script type="text/javascript">
  $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/financial_report_list')?>",
            "type": "POST",
            "data": function(data){
                console.log(data);       
                // data.no_of_passanger = $('#no_of_passanger').val();
            }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,8 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        {
              "targets": [8,7,6],
              "className": "text-right",
         }
        ],
 
    });

    

    function selected_person(){
      $("#person_name option:selected" ).each(function() {
        if($(this).val()=="other"){
          $('#other').val("");
          $('#other_div').show();
        }else{
          $('#other').val($(this).html());
          $('#other_div').hide();
        }
        // console.log();
      });
    }
    $('#other_div').hide();
    $('#person_name').change(function(){
      selected_person();
    });
    selected_person();
});
</script>
<?php } ?>
<?php if (true): ?>
<script type="text/javascript">
$(document).ready(function() {
$('.datepicker').datepicker({
autoclose: true
});
});


</script>
<?php endif?>