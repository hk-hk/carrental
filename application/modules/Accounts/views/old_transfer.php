<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Transfer</a></li>
                </ul>
                <div class="tab-content">
                    <!-- general form elements -->
                    <!-- <div class="box box-info"> -->
                    <div class="tab-pane active" id="tab_1">
                        <form role="form" action="<?=site_url(CRM_VAR . '/transfer/'.$account_detail->account_id)?>" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Account's Name:</label>
                                    <input class="form-control" placeholder="Account Name" name="accountname" value="<?php echo $account_detail->account_name; ?>" readonly>
                                    
                                </div>
                                <div class="form-group">
                                    <label>Account Number:</label>
                                    <input class="form-control" placeholder="Account Number" name="accountnumber"  value="<?php echo $account_detail->account_number;?>" readonly>
                                </div>
                                <?php if($account_detail->account_type==1){?>
                                <div class="form-group">
                                    <label>Account Price(MMK):</label>
                                    <input class="form-control" placeholder="Account Price" name="accountprice" value="<?php echo $account_detail->account_price;?>" readonly>
                                </div>
                                <?php }
                                else if($account_detail->account_type==2){?>
                                <div class="form-group">
                                    <label>Account Price($):</label>
                                    <input class="form-control" placeholder="Account Price" name="accountprice" value="<?php echo $account_detail->account_dollar;?>" readonly>
                                </div>
                                <?php }?>
                                <div class="form-group">
                                    <label>Banks</label>
                                    <!-- <select name="bank" id="select" class="form-control"> -->
                                    <!-- <option value="0">Please select</option> -->
                                    <?php foreach ($banks as $key => $value): ?>
                                    <?php if ($value->ums_id==$account_detail->account_bank): ?>
                                    <input value="<?php echo $value->ums_bank;?>" class="form-control" readonly>
                                    <?php endif ?>
                                    <?php endforeach ?>
                                    <!-- </select> -->
                                    <!-- <input class="form-control" placeholder="Account Date" name="accountdate" value="<?php echo $account_detail->account_date;?>" readonly="readonly"> -->
                                </div>

                                <div class="form-group">
                                    <label>Transfer Amount</label>
                                    <input class="form-control" placeholder="Transfer Amount" name="transferamount">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button id="submit" type="submit" class="btn btn-info pull-right">Transfer</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>