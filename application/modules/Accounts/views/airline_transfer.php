<form role="form" action="<?=site_url(CRM_VAR . '/airline_transfer/')?>" method="post" id="airline_form">
<section class="content">
    <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Current Available Balance - MMK Total: USD Total: </h3>
            </div>
            <div class="box-body">

              <div class="table-responsive">
               <table cellspacing="0" width="100%">
                  <tbody>

                    <tr>
                      <?php foreach ($banklist as $key => $value): ?>
                          <td>
                            <table  class="table table-bordered">
                              <tbody>
                                <tr class="text-center">
                                  <td colspan="2"><?=$key?></td>
                                </tr>
                                <tr>
                                  <td>MMK</td>
                                  <td><?=$value['mmk'] ? '<font color="green">' . $value['mmk'] . '</font>' : '<font color="red">No Balance</font>'?></td>
                                </tr>
                                <tr>
                                  <td>USD</td>
                                  <td><?=$value['usd'] ? '<font color="green">' . $value['usd'] . '</font>' : '<font color="red">No Balance</font>'?></td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                      <?php endforeach?>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Airlines Credit Balances </h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">EXPAND <i class="fa fa-plus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body collapse">

              <div class="table-responsive">
               <table cellspacing="0" width="100%">
                  <tbody>

                    <?php  $rn=0; ?>
                            <?php foreach ($expend_airlines as $key => $value): ?>
                            <tr>
                                <?php for($r=0; $r< 5; $r++): ?>
                                    <?php if($rn < count($expend_airlines)){ ?>
                                            
                                            <td width="16.66%">
                                                <table  class="table table-bordered" style="margin-bottom: 5px;">
                                                    <thead style="background-color: #e9ecef">
                                                        <!-- <tr  height="50px"> -->
                                                            <th class="text-center" colspan="2" style="vertical-align: middle;">
                                                                <strong ><?= $expend_airlines[$rn]->airline_name  ?></strong>
                                                            </th>
                                                        <!-- </tr>    -->
                                                    </thead>
                                                  <tbody>
                                                    
                                                    <tr>
                                                        <td>MMK</td>
                                                        <td align="right" 
                                                        <?php if(!empty($airlines[$rn]->deposit_amount_mmk) && $airlines[$rn]->deposit_amount_mmk!=0) 
                                                            if($airlines[$rn]->deposit_amount_mmk >= 0 && $airlines[$rn]->deposit_amount_mmk <= 500000 || $airlines[$rn]->deposit_amount_mmk < 0){
                                                                echo 'bgcolor="red"';
                                                            }
                                                            elseif($airlines[$rn]->deposit_amount_mmk <= 1000000){
                                                                echo 'bgcolor="orange"';
                                                            }
                                                            
                                                        
                                                        ?>
                                                            <strong>    
                                                                <?= number_format($airlines[$rn]->deposit_amount_mmk) ?>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                      <td>USD</td>
                                                        <td align="right" 
                                                        <?php if(!empty(number_format($airlines[$rn]->deposit_amount_usd)))
                                                            if($airlines[$rn]->deposit_amount_usd >= 0 && $airlines[$rn]->deposit_amount_usd <= 1000 || $airlines[$rn]->deposit_amount_usd < 0){
                                                                echo 'bgcolor="red"';
                                                            }
                                                            elseif($airlines[$rn]->deposit_amount_usd <= 2000){
                                                                echo 'bgcolor="orange"';
                                                            }
                                                        ?>
                                                            <strong>
                                                                <?= number_format($airlines[$rn]->deposit_amount_usd,2) ?>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                            </td>
                                            <?php $rn++; ?>
                                    <?php }else{
                                    break; 
                                     } ?>
                                <?php endfor ?>
                            </tr>
                            <?php endforeach ?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>








        <!-- left column -->
        <div class="col-md-4">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Account</a></li>
                </ul>
                <div class="tab-content">
                    <!-- general form elements -->
                    <!-- <div class="box box-info"> -->
                    <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>From Account</label>
                                    <select class="form-control" name="from_account" id="main_account_info">
                                    <?php foreach ($accounts as $key => $value): ?>
                                        <?php if ($value->account_id != 10): ?>
                                            <option value="<?php echo $value->account_id; ?>"><?php echo $value->account_name; ?> (<?=($value->account_type == 1) ? 'MMK' : '$'?>)</option>
                                        <?php endif?>
                                    <?php endforeach?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Account Number:</label>
                                    <input class="form-control" placeholder="Account Number" name="accountnumber"  value="" id="main_account_number" readonly>
                                </div>

                                <!-- <div class="form-group">
                                    <label>Banks</label>
                                    <input value="" class="form-control" placeholder="Banks" id="main_account_bank_name" readonly>

                                </div> -->
                                <div class="form-group" id="main_mmk_div">
                                    <label>Account Balance(MMK):</label>
                                    <input class="form-control number-format" placeholder="Account Balance (MMK)" id="main_account_balance_mmk" value="" readonly>
                                </div>
                                <div class="form-group" id="main_usd_div">
                                    <label>Account Balance($):</label>
                                    <input class="form-control number-format" placeholder="Account Balance (USD)" id="main_account_balance_usd" value="" readonly>
                                </div>
                                <input type="hidden" id="main_account_type">
                                <div class="form-group">
                                    <label>In Account Balance</label>
                                    <input class="form-control number-format" placeholder="In Account Balance" id="in_account_balance" readonly="">
                                    <span>Calculate from your withdraw amount.</span>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Transfer To Airline</a></li>
                </ul>
                <div class="tab-content">
                    <!-- general form elements -->
                    <!-- <div class="box box-info"> -->
                    <div class="tab-pane active" id="tab_1">

                            <div class="box-body">
                                
                                <div class="form-group">
                                    <label>Transfer To</label>
                                    <select class="form-control" name="to_account" id="airlineinfo">
                                    <?php foreach ($airlines as $key => $value): ?>
                                        
                                            <option value="<?php echo $value->id; ?>"><?php echo $value->airline_name; ?></option>
                                    <?php endforeach?>
                                    </select>
                                </div>
                                <div class="form-group" style="display: none">
                                  <label for="transfer_type" class="col-md-12 control-label">Transfer Type</label>
                                  <!-- <div class="col-md-8"> -->
                                    <input type="hidden" name="transfer_type" id="tt">
                                    <select class="form-control" id="transfer_type" >
                                      <!-- <option value="0">Both</option> -->
                                      <option value="1">MMK</option>
                                      <option value="2">USD</option>
                                    </select>
                                  <!-- </div> -->
                                </div>
                                <div class="form-group">
                                    <label>Code Number:</label>
                                    <input class="form-control" placeholder="Code Number" name="codenumber"  value="" id="code_number" readonly>
                                </div>

                                <!-- <div class="form-group">
                                    <label>Banks</label>
                                    <input value="" class="form-control" placeholder="Banks" id="sub_account_bank_name" readonly>

                                </div> -->
                                <div class="form-group" id="sub_mmk_div">
                                    <label>Credit Amount (MMK):</label>
                                    <input class="form-control number-format" placeholder="Credit Amount (MMK)" id="credit_amount_mmk" value="" readonly>
                                </div>
                                <div class="form-group" id="sub_usd_div">
                                    <label>Credit Amount (USD):</label>
                                    <input class="form-control number-format" placeholder="Credit Amount (USD)" id="credit_amount_usd" value="" readonly>
                                </div>
                            </div>
                            <div class="box-footer">
                            </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-md-4">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Transfer Amount</a></li>
                </ul>
                <div class="tab-content">
                    <!-- general form elements -->
                    <!-- <div class="box box-info"> -->
                    <div class="tab-pane active" id="tab_1">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Transaction Date</label>
                                    <input type="text" name="transaction_date" class="form-control datepicker" value="<?= date('m/d/Y') ?>" placeholder="">
                                </div>
                                
                                <div class="form-group">
                                    <label>Transfer Amount</label>
                                    <input type="text" class="form-control number-format" placeholder="Transfer Amount" name="transfer_amount" id="transfer_amount" onkeyup="updateBalance()" value="0">
                                    <span style="color: red" class="amount-message"></span>
                                </div>
                                

                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="comment" placeholder="Enter Description" id="description" class="form-control" required=""></textarea>
                                </div>
                                <input type="hidden" id="sub_account_type">
                            </div>
                            <div class="box-footer">
                                <button id="submit" type="submit" onclick="return confirm('Do you want really want to transfer this transation?')" class="btn btn-info pull-right">Transfer</button>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">List</h3>
            </div>
            <div class="box-body">
              
              <div class="table-responsive">
               <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <!-- <th>No</th> -->
                      <th>Created At</th>
                      <th>Transaction Date</th>
                      <th>Category</th>
                      <th>Description</th>
                      <th>Process By</th>
                      <th>Account</th>
                      <th>Payment To/From</th>
                      <th>Deposit</th>
                      <th>Withdraw</th>
                      <th >Final Balance</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>
</form>
<script type="text/javascript">
    $(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/financial_report_list')?>",
            "type": "POST",
            "data": function(data){
                console.log(data);       
                // data.no_of_passanger = $('#no_of_passanger').val();
            }
        },
                //, fuel_type, car_type, rent_type, no_of_passanger
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,8 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        {
              "targets": [8,7,6],
              "className": "text-right",
         }
        ],
 
    });
    
$('.datepicker').datepicker({
    autoclose: true
});
});
function main_acc_info(id){
    if(id==null){
        id=$("#main_account_info option:selected" ).val();
    }

    $.post('<?=site_url(CRM_VAR . '/accountinfo/')?>'+id,function(data){
        var obj=$.parseJSON(data);
        $('#main_account_number').val(obj['account_number']);
        $('#main_account_bank_name').val(obj['account_bank']);
        $('#main_account_balance_usd').val(obj['account_dollar']);
        $('#main_account_balance_mmk').val(obj['account_price']);
        $('#main_account_type').val(obj['account_type']);
        $('#exchange_amount').val("0");
        $('#in_account_balance').val("0");
        payCondition($('#main_account_type').val(),$('#sub_account_type').val());
    });
}


function sub_acc_info(id){
    if(id==null){
        id=$("#airlineinfo option:selected" ).val();
    }
    $('#transfer_type').attr('disabled',true);
    // $("#airlineinfo option:selected" ).each(function() {
        // console.log(id);
        $.post('<?=site_url(CRM_VAR . '/airlineinfo/')?>'+id,function(data){
            console.log(data);
            var obj=$.parseJSON(data);
            
            $('#code_number').val(obj['code_number']);
            $('#credit_amount_mmk').val(obj['deposit_amount_mmk']);
            $('#credit_amount_usd').val(obj['deposit_amount_usd']);
            
            payCondition($('#main_account_type').val(),$('#sub_account_type').val());
        });
      // payCondition($(this).val());
    // });
}
var main_account_info="";
var airlineinfo="";
function set_description(){
    $('#description').val(main_account_info+" TO "+airlineinfo);
}
main_account_info=$('#main_account_info option:selected').html();
airlineinfo=$('#airlineinfo option:selected').html();
set_description();
$('#main_account_info').change(function(){
    main_account_info=$('#main_account_info option:selected').html();
    set_description();
    main_acc_info($(this).val());
});
$('#airlineinfo').change(function(){
    airlineinfo=$('#airlineinfo option:selected').html();
      // console.log(this.val());
    set_description();
    sub_acc_info($(this).val());
    
});
$('#transfer_type').change(function(){
    $("#transfer_type option:selected" ).each(function() {
      console.log($(this).val());
      payCondition($('#main_account_type').val(),$(this).val());
    });
  });

function payCondition(macc_type,acc_type){
    $('#transferamount_mmk').val("0");
    $('#transferamount_usd').val("0");
    $('#exchange_rate').val("0");
    var account=$('#airlineinfo option:selected').val();

    // console.log(account);

// payCondition($('#main_account_type').val(),$('#transfer_type').val());
    
    if(macc_type==1){
        $('#usd_input').hide();
        $('#mmk_input').show();
        $('#main_usd_div').hide();
        $('#main_mmk_div').show();
    }else if(macc_type==2){
        $('#usd_input').show();
        $('#mmk_input').hide();
        $('#main_mmk_div').hide();
        $('#main_usd_div').show();
    }

    if(macc_type==acc_type){
        $('#exchange').hide();
    }else{
        $('#exchange').show();
    }

  }
function updateBalance(){

    var transfer_amount_mmk=$('#transferamount_mmk').val();
    var transfer_amount_usd=$('#transferamount_usd').val();
    var main_type=$('#main_account_type').val();
    var sub_type=$('#sub_account_type').val();

    if(main_type>sub_type){
        $('#exchange_amount').val(parseInt(transfer_amount_usd*$('#exchange_rate').val()));
        $('#in_account_balance').val(parseInt($('#main_account_balance_usd').val()) -transfer_amount_usd);
        
    }else if(main_type<sub_type){
        $('#exchange_amount').val(Math.ceil(parseInt(transfer_amount_mmk) / parseInt($('#exchange_rate').val())));
        $('#in_account_balance').val(parseInt($('#main_account_balance_mmk').val()) - transfer_amount_mmk);
    }else{
        if(main_type==1){
            $('#in_account_balance').val(parseInt($('#main_account_balance_mmk').val()) -transfer_amount_mmk);
        }
        if(main_type==2){
            $('#in_account_balance').val(parseInt($('#main_account_balance_usd').val()) -transfer_amount_usd);
        }
        // alert();
        // main_account_balance_mmk
        if(parseInt($('#main_account_balance_mmk').val())<transfer_amount_mmk || parseInt($('#main_account_balance_usd').val())<transfer_amount_usd){
            $('.amount-message').html("You enter more then account balance!");
            $('#transferamount_mmk').val("0");
            $('#transferamount_usd').val("0");
            $('#in_account_balance').val("0");
        }else{
            $('.amount-message').html("");
        }
    }
    
}
$('#airline_form').submit(function(){
    // if($('#transfer_amount').val()>0){
        return true;
    // }else{
        // alert("Please check your transfer amount!");
        // return false;
    // }
});
main_acc_info();
sub_acc_info();
</script>