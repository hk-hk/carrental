<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends MY_Model {

	public $_table = 'vendors'; // database table
	public $primary_key   = 'id'; 
 	public function __construct() {

        parent::__construct();

    }

    public function getVendors($showall=false){
    	$this->db->select("*");
    	$this->db->from("vendors");
        $this->db->where('id !=',2,false);
        if(!$showall){
            $this->db->where('balance_mmk !=',0,false);
            $this->db->or_where('balance_usd !=',0,false);
        }
    	
    	$query = $this->db->get();
        return $query->result();
    }

}
