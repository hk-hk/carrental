<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Airlines extends MY_Model {

	public $_table = 'airlines'; // database table
	public $primary_key   = 'id'; 
 	public function __construct() {

        parent::__construct();

    }

    public function getAirlines($showall=false){
    	$this->db->select("*");
    	$this->db->from("airlines");
        if(!$showall){
    	   $this->db->where('deleted_at =','null',false);
            $this->db->where('deposit_amount_mmk !=',0,false);
            $this->db->or_where('deposit_amount_usd !=',0,false);
        }
    	
    	$query = $this->db->get();
        return $query->result();
    }

}
