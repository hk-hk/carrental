<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends MY_Model {

	public $_table = 'ums_account'; // database table
	public $primary_key   = 'account_id';
    public function __construct() {
        parent::__construct();
    }

    public function get_all_with_bank(){
    	$this->db->select('ums_account.*');
    	$this->db->select('ums_bank.ums_bank');
    	$this->db->from('ums_account');
    	$this->db->join('ums_bank','ums_bank.ums_id = ums_account.account_bank','left');
    	$this->db->order_by('ums_bank.ums_bank','asc');
    	$query = $this->db->get();
    	return $query->result();
		// INNER JOIN sonicstar_db.ums_bank ON sonicstar_db. = sonicstar_db.
    }

}
