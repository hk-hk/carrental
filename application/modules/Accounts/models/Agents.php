<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agents extends MY_Model {

	public $_table = 'agents'; // database table
	public $primary_key   = 'id'; 
 	public function __construct() {

        parent::__construct();

    }

    public function getAgents($showall=false){
    	$this->db->select("*");
    	$this->db->from("agents");
        $this->db->where('id !=',2,false);
        $this->db->where('agent_type =','prepaid');
        if(!$showall){
            $this->db->where('balance_mmk !=',0,false);
            $this->db->or_where('balance_usd !=',0,false);
        }
    	
    	$query = $this->db->get();
        return $query->result();
    }

}
