<?php

$route[CRM_VAR.'/banks']     = 'Accounts/Bank/banks'; 
$route[CRM_VAR.'/banks/(:any)']     = 'Accounts/Bank/banks/$1'; 
$route[CRM_VAR.'/banks_list']     = 'Accounts/Bank/banks_list'; 
$route[CRM_VAR.'/save_bank']     = 'Accounts/Bank/store_bank'; 
$route[CRM_VAR.'/update_bank/(:any)']     = 'Accounts/Bank/store_bank/$1'; 
$route[CRM_VAR.'/delete_bank/(:any)']     = 'Accounts/Bank/delete_bank/$1'; 


$route[CRM_VAR.'/accounts']     = 'Accounts/Account/accounts'; 
$route[CRM_VAR.'/accounts/(:any)']     = 'Accounts/Account/accounts/$1'; 
$route[CRM_VAR.'/save_account']     = 'Accounts/Account/store_account';
$route[CRM_VAR.'/update_account/(:any)']     = 'Accounts/Account/store_account/$1';

$route[CRM_VAR.'/transfer']     = 'Accounts/Account/transfer';
$route[CRM_VAR.'/airlineinfo/(:any)']     = 'Accounts/Account/airline/$1';
$route[CRM_VAR.'/airline_transfer']     = 'Accounts/Account/airline_transfer';

$route[CRM_VAR.'/vendorinfo/(:any)']     = 'Accounts/Account/vendor/$1';
$route[CRM_VAR.'/prepaid_vendor_transfer']     = 'Accounts/Account/prepaid_vendor_transfer';
$route[CRM_VAR.'/agentinfo/(:any)']     = 'Accounts/Account/agent/$1';
$route[CRM_VAR.'/prepaid_agent_transfer']     = 'Accounts/Account/prepaid_agent_transfer';

$route[CRM_VAR.'/transaction/(:any)']     = 'Accounts/Account/transaction/$1';

$route[CRM_VAR.'/inout']     = 'Accounts/Account/inout'; 
$route[CRM_VAR.'/store_inout']     = 'Accounts/Account/store_inout'; 