<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <link rel="stylesheet" href="<?php echo base_url("Calendar/");?>css/monthly.css">
	<style type="text/css">
		
		#mycalendar {
			width: 100%;
			margin: 2em auto 0 auto;
			max-width: 80em;
			border: 1px solid #666;
		}
                .manage-btn button {float: right; }
	</style>
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Choose Supplier for booking calendar</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    
        <div class="box-body">
            <div class="row">
                <form action="<?php echo site_url(CRM_VAR.'/suppliers')?>" id="active_supplier" name="active_supplier" method="post" >
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-3">
                        <label for="recipient-name" class="form-control-label">Select Supplier:</label>
                        <select class="form-control" id="supplier_id" name="active_supplier"  onchange="return changeSupplier();">
                            <option value="">Select Supplier</option>
                            <?php foreach ($suppliers as $k=>$val){ 
                                $val->user_company = ($val->user_company!='')?$val->user_company:$val->user_lname;
                                if($active_supplier == $val->id){?>
                            <option value="<?php echo $val->id;?>" selected="selected"><?php echo $val->user_company;?></option>
                                <?php }else{ ?>
                              <option value="<?php echo $val->id;?>"><?php echo $val->user_company;?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
        </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
</div>
  <!-- /.row -->
</section>
<script>
function changeSupplier(){
    document.getElementById("active_supplier").submit();
}
</script>