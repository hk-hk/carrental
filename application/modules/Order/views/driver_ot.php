<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
<style>
.control-label{
text-align: left !important;
text-decoration: none;
}
.values{
font-weight: initial !important;
}
.fa .fa-star {
color: #ff0000 !important;
}
</style>
<!-- Cancellation Pop-up -->
<div id="cancellation" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Resason for cancellation first</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/cancle/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputEmail4" class="control-label">Reason For Cancellation : </label>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="cancleReason" rows="3" placeholder="reason for cancellation..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Cancellation">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Cancellation Pop-up -->
<!-- rating Pop-up -->
<div id="rating" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Give Feedback </h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/submit_feedback/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Rating : </label>
              </div>
              <div class="form-group">
                <!-- 1<input type="radio" name="rating" value="1" required="required">
                2<input type="radio" name="rating" value="2" required="required">
                3<input type="radio" name="rating" value="3" required="required">
                4<input type="radio" name="rating" value="4" required="required">
                5<input type="radio" name="rating" value="5" required="required"> -->
                <input type="hidden" name="rating" id="review" required="required">
                <div class="cmt-lower">
                  <i id="star_1" class="fa fa-star-o starrattings"></i>
                  <i id="star_2" class="fa fa-star-o starrattings"></i>
                  <i id="star_3" class="fa fa-star-o starrattings"></i>
                  <i id="star_4" class="fa fa-star-o starrattings"></i>
                  <i id="star_5" class="fa fa-star-o starrattings"></i>
                </div>
                <script>
                jQuery( document ).ready(function() {
                jQuery(".starrattings").on("mouseover",function(){
                var startt=jQuery(this).attr("id");
                var starid=startt.split("_");
                starid=starid[1];
                for(var i=1;i<=5;i++){
                if(jQuery("#star_"+i).attr("class")=='fa fa-star starrattings'){
                jQuery("#star_"+i).removeClass("fa fa-star starrattings");
                jQuery("#star_"+i).addClass("fa fa-star-o starrattings");
                }
                }
                // alert(starid);
                for(var i=1;i<=starid;i++){
                jQuery("#star_"+i).removeClass("fa fa-star-o starrattings");
                jQuery("#star_"+i).addClass("fa fa-star starrattings");
                }
                jQuery("#review").val(starid);
                });
                });
                </script>
              </div>
              <div class="form-group">
                <label>Comment : </label>
              </div>
              <div class="form-group">
                <input type="hidden" name="order_id" required="required" value="<?php echo $orderinfo->order_id; ?>">
                <textarea class="form-control" name="comment" rows="3" placeholder="Comment..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Feedback">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- rating Pop-up -->
<section class="content">

  
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning ">   <!--  -->
      <div class="box-header with-border">
        <h3 class="box-title">Toll & Other Expense</h3>
        <div class="box-tools pull-right">
          <a href="<?php echo site_url(); ?>admin/order/view/<?php echo $orderinfo->order_id; ?>" class="btn btn-default"> Exit

          </a>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <form action="<?php echo site_url(CRM_VAR . '/order/driver_ot_insert/') ?>" class="form-horizontal" method="post" id="payment_form">
        <input type="hidden" name="order_id" value="<?=$orderinfo->order_id?>">
        <div class="box-body">
          <!-- <h3 class="box-title">Filter</h3> -->
          <!--  -->
          <div id="own_expense">
            <div class="row">
              <div class="col-md-6">
                <!-- <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" placeholder="Choose Date" name="maintain_date" id="maintain_date" value="<?=!empty($maintain_detail->date) ? $maintain_detail->date : ''?>" required="" />
                  </div>
                </div> -->
                <!-- <input type="hidden" class="form-control" id="driver" name="driver" value="<?php echo $orderinfo->driver_name; ?>" readonly> -->
                <!-- <input type="hidden" class="form-control" id="car_id" name="car_id" value="<?php echo $orderinfo->car_id; ?>" readonly> --><!-- 
                <div class="form-group">
                  <label for="category" class="col-md-4 control-label">Category</label>
                  <div class="col-md-8">
                    <select class="form-control" id="category" name="category" required="">
                      <option value="">Select Category</option>
                      <option value="4451">Toll Expense</option>
                      <option value="4452">Other Expense</option>
                    </select>
                  </div>
                </div>
                 -->
                <div class="form-group">
                  <label for="payment_person" class="col-md-4 control-label">Driver</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="payment_person" name="payment_person" placeholder="No Driver Defined" value="<?php echo $orderinfo->driver_name; ?>" readonly="" required>
                    <span><a href="<?=site_url(CRM_VAR . '/order/edit/'.$orderinfo->order_id)?>" >Assign Driver</a></span>
                    <!-- <span></span> -->
                  </div>
                </div>
               <!--  <div class="form-group">
                  <label for="amount" class="col-md-4 control-label">Amount (MMK)</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="amount" name="amount" min="0" required="">
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="date" class="col-md-4 control-label">Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" value="<?= date('m/d/Y') ?>" name="date" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="ot_hours" class="col-md-4 control-label">OT Hours</label>
                  <div class="col-md-8">
                    <input type="number" name="ot_hours" id="ot_hours" class="form-control" required=""/>
                    <!-- <input type="number" class="form-control" id="exchange_amount" name="exchange_amount" min="0" value="0" readonly=""> -->
                  </div>
                </div>
                
              </div>
              <div class="col-md-6">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                      <thead>
                          <tr>
                              <th>Created At</th>
                              <th>Date</th>
                              <th>OT Hours</th>
                              <!-- <th>Amount MMK</th> -->
                              <!-- <th>Balance</th> -->
                          </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($ot_hours_data as $key => $value): ?>
                            
                            <tr>
                              <td><?= date('d, M Y',$value->created_at) ?></td> 
                              <td><?= date('d, M Y',$value->ot_date) ?></td> 
                              <td><?= $value->ot_hours ?></td> 
                            </tr>
                          <?php endforeach ?>
                      </tbody>
                      
                  </table>
              </div>
              
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="pull-right">
            <!-- <div class="form-group">
                  <div class="col-md-12"> -->
                    <button type="submit" class="btn btn-info pull-right" id="btntransfer" onclick="return confirm('Are you sure to submit?')">Submit</button>
                  <!-- </div>
                </div> -->
            <!-- <button type="submit" id="complete_order" name="complete_order" value="yes" class="btn btn-info">Complete Order and Transfer</button> -->
            
          </div>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
</section>

<script type="text/javascript">
$(document).ready(function() {
$('.datepicker').datepicker({
autoclose: true
});
});
</script>