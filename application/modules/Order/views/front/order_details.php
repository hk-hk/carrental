<div class="container">
    <section class="place-detail-box common-padding graish-bg clearfix order_details">
        <?php
        if ($this->session->flashdata('typ')):
            switch ($this->session->flashdata('typ')) {
                case 1:
                    $put = 'alert-success';
                    break;
                case 2:
                    $put = 'alert-warning';
                    break;
                case 3:
                    $put = 'alert-danger';
                    break;
            }
            ?>
                <div class="alert <?php echo $put; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('msg'); ?>
                </div>
        <?php endif; ?>
        <div class="row order-details">
            <div class="col-sm-6">
                <div class="row">
                    <?php $order_history = json_decode($order_details->order_history); ?>
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_ORDER_ID'); ?></label>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $order_details->order_id?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_CUST_NAME'); ?></label>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $order_details->user_fname." ".$order_details->user_lname?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_SUPP_NAME'); ?></label>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $order_details->supl_name?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_CAR_TYPE'); ?></label>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $order_details->carname?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_CAR_TAG'); ?></label>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $order_details->car_number?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_DRIVER_NAME'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $order_details->driver_name?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_DRIVER_PHONE'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $order_details->driver_phone?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_PICKUP_DATE'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo date("M d,Y",  strtotime($order_details->pickup_date));?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_PICK_UP_TIME'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo date("H:i A",  strtotime($order_details->pickup_date));?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_DROPOFF_DATE'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo date("M d,Y",  strtotime($order_details->drop_date));?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_DROP_OFF_TIME'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo date("H:i A",  strtotime($order_details->drop_date));?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_PICKUP_PLACE'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $order_details->pickup_city?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_PAYMENT_TYPE'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $order_details->paymethod;?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_TOTAL_AMOUNT'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $order_history->currency.$order_details->booking_amount;?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="col-sm-6 control-label"><?php echo getLang('ORDER_ORDER_STATUS'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $order_details->status;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="rows">
            <div class="col-sm-6">
                
                  <?php 
                  $trackPrices = $order_history->trackPrices;
                  $searchData = $order_history->searchData;
                  $currency = $this->currency;
                ?>
                <h2>Summary of charges</h2>
                <div class="charges-summery">
                    <?php //echo "<pre>";print_r($trackPrices);echo "</pre>";?>
                    <ul>
                        <?php 
                        foreach ($trackPrices as $k=>$item){ 
                                if(!isset($item->title)){ 
                                    foreach ($item as $it){ 
                                        ?> 
                                        <li>
                                            <span class="text-left"><?php echo $it->title;?></span>
                                            <span class="text-right"><?php echo ($it->amount);?></span>
                                        </li>
                                    <?php } ?>
                                <?php }else{  ?>
                                    <li>
                                        <span class="text-left"><?php echo $item->title;?></span>
                                        <span class="text-right"><?php echo ($item->amount);?></span>
                                    </li>
                                <?php }
                            }?>
                        
                    </ul>
                </div>
              
            </div>
            <div class="col-sm-6">
                <?php  if($order_details->status=='Pending' || $order_details->status=='Completed'){ ?>
                    <div class="row">
                        <div class="col-sm-10">
                            <?php if($order_details->status=='Pending'){ ?>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#EditDropOff"><?php echo getLang('ORDER_ORDER_CANCEL')?></button>
                            <?php }else if(empty ($is_feedback_doneByCust)){ ?>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#giveFeedback"><?php echo getLang('ORDER_DETAILS_GIVE_FEEDBACK')?></button>
                            <?php }else{?>
                                <div class="row">
                                    <div class="col-sm-12"><h2><?php echo getLang('ORDER_DETAILS_FEEDBACK_SECTION_HEADING')?></h2></div>
                                    <div class="col-sm-12">
                                        <h3><?php echo getLang('ORDER_DETAILS_CUSTOMER_FEEDBACK')?></h3>
                                        <div class="cmt-lower">
                                        <?php echo getLang('ORDER_DETAILS_RATING_TEXT')?>  :
                                        <?php 
                                        for ($i=1;$i<=5;$i++) {
                                          if($i<=$is_feedback_doneByCust->rating){
                                            echo '<i class="fa fa-star starrattings"></i>';
                                          }
                                          else{
                                            echo '<i class="fa fa-star-o starrattings"></i>';
                                          }
                                        }
                                        ?>
                                        </div>
                                        <p><blockquote><?php echo $is_feedback_doneByCust->comment?></blockquote></p>
                                    </div>
                                    <?php if(!empty($is_feedback_doneBySupp) && !empty($is_feedback_doneByCust)){?>
                                    <div class="col-sm-12">
                                        <h3><?php echo getLang('ORDER_DETAILS_SUPPLIER_FEEDBACK')?></h3>
                                        <div class="cmt-lower">
                                        <?php echo getLang('ORDER_DETAILS_RATING_TEXT')?> :
                                        <?php 
                                        for ($i=1;$i<=5;$i++) {
                                          if($i<=$is_feedback_doneBySupp->rating){
                                            echo '<i class="fa fa-star starrattings"></i>';
                                          }
                                          else{
                                            echo '<i class="fa fa-star-o starrattings"></i>';
                                          }
                                        }
                                        ?>
                                        </div>
                                        <p><blockquote><?php echo $is_feedback_doneBySupp->comment?></blockquote></p>
                                    </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
            </div>
        </div>
    </section>
</div>
<!-- Dropoff Modal -->
<div class="modal fade pickup-modal" id="EditDropOff" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form action="<?php echo site_url('cancel-order/'.$order_details->order_id)?>" method="post">
          <div class="modal-header form-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <span class="search-icon">
                  <img src="<?php echo base_url("/public/front/images/car-icon.png"); ?>" alt="">
              </span>
              <h3 class="modal-title"><?php echo getLang('ORDER_BOOKING_CANCELLATION'); ?></h3>
          </div>
          <div class="modal-body">
              <div class="loation-box">
                  <label><?php echo getLang('ORDER_REASON_FOR_CANCEL'); ?>:</label>
                  <div class="pickup-location-input">
                      <textarea name="cancleReason" required="required" style="width: 80%" rows="5"></textarea>
                  </div>
              </div><!-- / Location 1 / -->

          </div><!-- Modal Body -->
          <div class="modal-footer">
              <button type="submit" class="btn btn-default"><?php echo getLang('ORDER_SUBMIT_CANCELLATION'); ?></button>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- Modal -->
<!-- Dropoff Modal -->
<div class="modal fade pickup-modal" id="giveFeedback" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form action="<?php echo site_url('submit-feedback/'.$order_details->order_id); ?>" method="post">
          <div class="modal-header form-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <span class="search-icon">
                  <img src="<?php echo base_url("/public/front/images/car-icon.png"); ?>" alt="">
              </span>
              <h3 class="modal-title"><?php echo 'Give Feedback to Trip vehicle'//getLang('ORDER_BOOKING_CANCELLATION'); ?></h3>
          </div>
          <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Rating : </label>  
              </div>
              <div class="form-group">
                <input type="hidden" name="rating" id="review" required="required">
                <div class="cmt-lower">
                    <i id="star_1" class="fa fa-star-o starrattings"></i>
                    <i id="star_2" class="fa fa-star-o starrattings"></i>
                    <i id="star_3" class="fa fa-star-o starrattings"></i>
                    <i id="star_4" class="fa fa-star-o starrattings"></i>
                    <i id="star_5" class="fa fa-star-o starrattings"></i>
                </div>
                <script> 
                jQuery( document ).ready(function() {
                jQuery(".starrattings").on("mouseover",function(){
                        var startt=jQuery(this).attr("id");
                        var starid=startt.split("_"); 
                        starid=starid[1];
                        
                        for(var i=1;i<=5;i++){
                            if(jQuery("#star_"+i).attr("class")=='fa fa-star starrattings'){
                                jQuery("#star_"+i).removeClass("fa fa-star starrattings");
                                jQuery("#star_"+i).addClass("fa fa-star-o starrattings");
                            }
                        }
                           // alert(starid);
                        for(var i=1;i<=starid;i++){
                                jQuery("#star_"+i).removeClass("fa fa-star-o starrattings");
                                jQuery("#star_"+i).addClass("fa fa-star starrattings");
                        }
                        jQuery("#review").val(starid);
                    });
                });
                </script>  
              </div>
              <div class="form-group">
                <label>Comment : </label>  
              </div>
              <div class="form-group">
                <input type="hidden" name="order_id" required="required" value="<?php echo $order_details->order_id; ?>">
                <textarea class="form-control" name="comment" rows="3" placeholder="Comment..." required="required"></textarea>  
              </div>
            </div>
          </div>
        </div><!-- Modal Body -->
          <div class="modal-footer">
              <button type="submit" class="btn btn-default"><?php echo 'Submit Feedback';//getLang('ORDER_SUBMIT_CANCELLATION'); ?></button>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- Modal -->
<style>
.row.order-details {
    line-height: 30px;
}
</style>