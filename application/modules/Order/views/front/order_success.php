<div class="container" style="min-height: 425px;">
    <section class="place-detail-box common-padding graish-bg order-sm">
        <div>
            <?php if($order_status == 'success') {?>
            <h4><?php echo getLang('ORDER_SUCCESS_THANKYOU_MSG');?></h4>
            <?php }else{ ?>
            <h4><?php echo getLang('ORDER_FAILED_THANKYOU_MSG');?></h4>
            <p><?php echo getLang('ORDER_FAILED_MSG');?><a href="<?php echo site_url("car/".$car_url);?>"><?php echo getLang('ORDER_REORDER_FAILED_ORDER_LINK')?></a></p>
            <?php } ?>
            <h4><?php echo getLang('ORDER_BOOKING_INFO_HEADING'); ?></h4>
            <?php echo $message;?>
        </div>
    </section>
</div>