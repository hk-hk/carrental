<div class="container" style="min-height: 425px;">
    <section class="place-detail-box common-padding graish-bg">
        <?php
        if ($this->session->flashdata('typ')):
            switch ($this->session->flashdata('typ')) {
                case 1:
                    $put = 'alert-success';
                    break;
                case 2:
                    $put = 'alert-warning';
                    break;
                case 3:
                    $put = 'alert-danger';
                    break;
            }
            ?>
                <div class="alert <?php echo $put; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('msg'); ?>
                </div>
        <?php endif; ?>
        <div class="">
            <div class="col-sm-12">
                <div class="login_info_block">
                    <ul class="sub-menu">
                        <li><a href="<?php echo site_url('profile');?>"><?php echo getLang('ORDER_MENU_MY_PROFILE'); ?></a></li>
                        <li><a href="<?php echo site_url('logout');?>"><?php echo getLang('ORDER_MENU_LOGOUT'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo getLang('ORDER_CAR_NAME'); ?></th>
                        <th><?php echo getLang('ORDER_BOOKING_AMOUNT'); ?></th>
                        <th><?php echo getLang('ORDER_PICKUP_DATE'); ?></th>
                        <th><?php echo getLang('ORDER_DROPOFF_DATE'); ?></th>
                        <th><?php echo getLang('ORDER_BOOKING_DATE'); ?></th>
                        <th><?php echo getLang('ORDER_ACTION'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($orders as $term) {
                        $order_history = json_decode($term->order_history);
                        $carinfo = $order_history->carinfo;
                        ?>
                        <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $carinfo->carname ?></td>
                            <td><?php echo $order_history->currency . $term->booking_amount ?></td>
                            <td><?php echo date("M d,Y H:i A", strtotime($term->pickup_date)) ?></td>
                            <td><?php echo date("M d,Y H:i A", strtotime($term->drop_date)) ?></td>
                            <td><?php echo date("M d,Y H:i A", strtotime($term->booking_date)) ?></td>
                            <td>
                                <a title="View Details" href="<?php echo site_url('myaccount/order_details/') . $term->order_id ?>"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                    <?php $i++;
                    } ?>
                </tbody>
            </table>
        </div>
    </section>
</div>
