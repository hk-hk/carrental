<div class="row">
      <!-- left column -->
      <div class="col-md-12">
         <!-- general form elements -->
         <div class="box box-info">
            <div class="box-header with-border" align="center">
               <h3 class="box-title">Order Detail
                <?php if (isset($orderinfo->order_by)): ?>
                  <?php if ($orderinfo->user_role == 'admin' || $orderinfo->user_role == 'service_provider'): ?>
                    (Order By - <?=$orderinfo->user_fname . ' ' . $orderinfo->user_lname?> | <?=ucfirst(str_replace('_', " ", $orderinfo->user_role))?> )
                  <?php elseif ($orderinfo->user_role == 'agent'): ?>
                    (Order By Agent )
                  <?php else: ?>
                    (Order By Customer )
                  <?php endif?>
                <?php endif?>
               </h3>
            </div>
            <div class="box-body">
              <?php
                    $order_history = json_decode($orderinfo->order_history);
                    $trackPrices                       = $order_history->trackPrices;
                    $searchData                        = $order_history->searchData;
                    $currency                          = $order_history->currency;
                    $__cartype='';
                    $__vendor_tel='';
              ?>
                <?php
//$currency = isset($orderinfo->order_history->currency)?$orderinfo->order_history->currency:'mmk';
if ($is_edit_action) {?>

                <div class="row">
                  <div class="col-md-12" align="left">

                      <?php if (($orderinfo->status != "Completed") || (isset($logged_user) && $logged_user->user_role == 'admin' && $orderinfo->status == "Completed")): ?>
                    <?php if (isset($logged_user) && ($logged_user->user_role == 'admin' || $logged_user->user_role == 'finance_admin' || $logged_user->user_role == 'finance')): ?>
                      <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" >
                          Expenses
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                          <?php foreach ($users as $k => $user): ?>
                            <?php if ($orderinfo->supplier_id == $user->id): ?>
                              <?php //if ($user->user_lname == 'Sonic Star'): ?>
                                <li><a href="<?php echo site_url(); ?>admin/order/fuel/<?php echo $orderinfo->order_id; ?>">Fuel</a></li>
                              <?php //endif?>

                              <li><a href="<?php echo site_url(); ?>admin/order/tollnother/<?php echo $orderinfo->order_id; ?>">Toll & Other Expense</a></li>

                              <?php if ($user->user_lname != 'Sonic Star'): ?>
                              <li><a href="<?php echo site_url(); ?>admin/order/vendor_fees/<?php echo $orderinfo->order_id; ?>">Vendor Fees</a></li>
                              <?php endif?>
                            <?php endif?>

                          <?php endforeach?>
                          <?php foreach ($agents as $k => $agent): ?>
                          <?php if ($orderinfo->user_id == $agent->id): ?>
                              <li><a href="<?php echo site_url(); ?>admin/order/agent_fees/<?php echo $orderinfo->order_id; ?>">Agent Fees</a></li>
                            <?php endif?>
                          <?php endforeach?>
                        </ul>
                    <!-- </div> -->
                      <a href="<?php echo site_url(); ?>admin/order/advance/<?php echo $orderinfo->order_id; ?>" class="btn btn-info" name="save" >Rental Advance</a>

                      <a href="<?php echo site_url(); ?>admin/order/payment/<?php echo $orderinfo->order_id; ?>" class="btn btn-info" name="save" >Customer Payment</a>
                    <!-- <div class="col-md-10" align="right"> -->

                    <?php endif?>
                    <a href="<?php echo site_url(); ?>admin/order/expenses/<?php echo $orderinfo->order_id; ?>" class="btn btn-info" name="save" >Trip Overview</a>
                    <a href="<?php echo site_url(); ?>admin/order/edit/<?php echo $orderinfo->order_id; ?>" class="btn btn-info" name="save" >Assign/Modify Order</a>
                     &nbsp;
                    <?php if ($orderinfo->status != "Cancelled" && $orderinfo->status != "Completed") {?>
                      <button data-toggle="modal" data-target="#cancellation" class="btn btn-warning" >Cancle Order</button>
                    <?php }?>

              <?php endif?>
                    <a href="#" onclick="MyWindow=window.open('<?php echo site_url(CRM_VAR.'/order/print/'.$orderinfo->order_id.'?title=invoice')?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=900,height=600'); return false;" class="btn btn-default">Print Invoice</a>
                    <a href="#" onclick="MyWindow=window.open('<?php echo site_url(CRM_VAR.'/order/print/'.$orderinfo->order_id.'?title=receipt')?>','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=900,height=600'); return false;" class="btn btn-default">Print Receipt</a>
                    <a href="<?php echo site_url(); ?>admin/order/driver_ot/<?php echo $orderinfo->order_id; ?>" class="btn btn-info" name="save" >Driver OT</a>
                    <a href="<?php echo site_url(); ?>admin/order/list" class="btn btn-default" name="save" >Exit</a>

                  <!-- </div> -->
                  </div>
                </div>

                <?php }?>

                <form id="addarticle-form" class="form-horizontal">
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-4">
                    <div class="row">
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Order ID</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->order_id; ?></label>
                            <input type="hidden" name="order_id" id="order_id" value="<?php echo $orderinfo->order_id; ?>">
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Trip Record Name</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->trip_record_name; ?></label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Customer Name</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->name; ?></label>
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $orderinfo->user_id; ?>">
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Customer Phone</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->phone; ?></label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Customer Email</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->usr_email; ?></label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Supplier Name</label>
                            <label class="col-sm-6 control-label values"><?php foreach ($users as $k => $user) {
    ?>
                                <?php if ($orderinfo->supplier_id == $user->id) {
                                  $__vendor_tel=$user->user_tel;
                                  echo $user->user_fname . " " . $user->user_lname;
                                }?>
                                <?php
}?></label>
                                <input type="hidden" name="supplier_id" id="supplier_id" value="<?php echo $orderinfo->supplier_id; ?>">
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Car Type</label>
                            <label class="col-sm-6 control-label values"><?php foreach ($cars as $kc => $car) {
    ?>
                                  <?php if ($orderinfo->car_id == $car->id) {
                                    $__cartype=$this->car_manager->get_carname($car->id);
                                    echo $this->car_manager->get_carname($car->id);}
                                    ?>
                                  <?php
}?>
                              </label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Car Tag</label>
                            <label class="col-sm-6 control-label values"><?php if ($orderinfo->car_id != 0) {if ($orderinfo->car_tag == '') {echo $this->car_manager->get_cartag($orderinfo->car_id);} else {echo $orderinfo->car_tag;}}?></label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Driver Name</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->driver_name; ?></label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Driver Phone</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->driver_phone; ?></label>
                         </div>
                      </div>
                      <?php
if ($orderinfo->user_role == 'agent') {
    ?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Agent ID</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->user_id; ?></label>
                         </div>
                      </div>
                      <?php
}
?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Type of Rent</label>
                            <label class="col-sm-6 control-label values">
                            <?php if ($orderinfo->type_of_rent == 36) {echo "Daily";}?>
                            <?php if ($orderinfo->type_of_rent == 37) {echo "Weekly";}?>
                            <?php if ($orderinfo->type_of_rent == 38) {echo "Monthly";}?>
                            </label>
                         </div>
                      </div>
                      <?php if (!isset($order_history->searchData->airport) && $orderinfo->type_of_rent == 36): ?>
                        <div class="col-md-12" id="daily-status">
                           <div class="form-group">
                              <label for="inputEmail4" class="col-sm-6 control-label">Daily Status</label>
                              <label class="col-sm-6 control-label values">
                                <?=ucfirst($orderinfo->daily_status)?>
                              </label>
                           </div>
                        </div>
                      <?php endif?>
                      <?php // if (isset($order_history->searchData->airport) && $order_history->searchData->airport != ""): ?>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="inputEmail4" class="col-sm-6 control-label">Trip Type</label>
                              <label class="col-sm-6 control-label values">
                                <?php if ($orderinfo->airport_status=="pickup" || $orderinfo->airport_status=="dropoff"): ?>
                                  <?= "Airport ".ucfirst($orderinfo->airport_status)?>
                                <?php elseif ($orderinfo->airport_status=="highway"): ?>
                                  <?= ucfirst($orderinfo->airport_status)?>
                                <?php else: ?>
                                  <?=ucfirst($orderinfo->airport_status)." - ".ucfirst($orderinfo->daily_status)?>
                                <?php endif ?>
                              </label>
                           </div>
                        </div>
                      <?php //endif?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Paid to</label>
                            <label class="col-sm-6 control-label values"><?php foreach ($users as $k => $user) {
    ?>
                                <?php if ($orderinfo->paid_to == $user->id) {echo $user->user_fname . " " . $user->user_lname;}?>
                                <?php
}?>
                              </label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Customer Confirmed</label>
                            <label class="col-sm-6 control-label values">
                             <?php if ($orderinfo->customer_confirmed == 1) {echo "<span class='btn btn-success'>YES</span>";} else {echo "<span class='btn btn-danger'>NO</span>";}?>
                          </label>
                         </div>
                      </div>



                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="row">
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Confirm ID</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->order_id; ?></label>
                         </div>
                      </div>
                      <?php
// print_r($orderinfo);echo date("Y-m-d H:i:s");
$pickup_date_time = explode(" ", $orderinfo->pickup_date);
?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Pick Up Date</label>
                            <label class="col-sm-6 control-label values"><?php echo date('d.m.Y (D)', strtotime($orderinfo->pickup_date)); ?></label>
                         </div>
                      </div>
                      <?php if ($orderinfo->type_of_rent == 38) {?>
                      <!-- <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">No Of Months</label>
                            <label class="col-sm-6 control-label values"><?php echo json_decode($orderinfo->order_history)->no_of_months ?> Months</label>
                         </div>
                      </div> -->

                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Drop Of Date</label>
                            <label class="col-sm-6 control-label values"><?php echo date('d.m.Y (D)', strtotime($orderinfo->drop_date)); ?></label>
                         </div>
                      </div>
                      <?php }?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Pick Up time</label>
                            <label class="col-sm-6 control-label values"><?php echo date('h:i A', strtotime($orderinfo->pickup_date)); ?></label>
                         </div>
                      </div>
                      <?php
if ($orderinfo->type_of_rent == 36) {
    $drop_date_time = explode(" ", $orderinfo->drop_date);
    ?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Drop off Date</label>
                            <label class="col-sm-6 control-label values"><?php echo date('d.m.Y (D)', strtotime($drop_date_time[0])); ?></label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Drop off Time</label>
                            <label class="col-sm-6 control-label values"><?php echo date('h:i A', strtotime($drop_date_time[1])); ?></label>
                         </div>
                      </div>
                      <?php }?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Pick Up Place</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->pl_title; ?></label>
                         </div>
                      </div>
                      <?php
if ($orderinfo->order_history != null || $orderinfo->order_history != '') {
    $order_history     = json_decode($orderinfo->order_history)->searchData;
    $order_historyInfo = json_decode($orderinfo->order_history);
    if ($orderinfo->type_of_rent == 36 && $orderinfo->airport_status  == 'highway') {?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Drop off Place</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->dl_title; ?></label>
                         </div>
                      </div>
                      <?php }}?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Order Date</label>
                            <label class="col-sm-6 control-label values"><?php echo date('d.m.Y (D)', strtotime($orderinfo->booking_date)); ?></label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Payment Type</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->payment_method; ?></label>
                        </div>
                      </div>

                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Driver OT Hours</label>
                            <label class="col-sm-6 control-label values"><?=$orderinfo->ot_hours?> Hours</label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Total Amount</label>
                            <label class="col-sm-6 control-label values"><?php echo $currency . ' ' . number_format($orderinfo->amount); ?></label>
                         </div>
                      </div>
                      <div class="col-md-12" style="display: none;">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Driver OT</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->ot_hours ?></label>
                         </div>
                      </div>
                      <div class="col-md-12" style="display: none;">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Driver OT Fees</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->driver_ot_fees ?></label>
                         </div>
                      </div>
                      <div class="col-md-12" style="display: none;">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Driver Daily Fees</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->driver_fees ?></label>
                         </div>
                      </div>
                      <div class="col-md-12" style="display: none;">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Driver Percentage</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->driver_percentage; ?> %</label>
                         </div>
                      </div>
                      <?php foreach ($agents as $k => $agent): ?>
                        <?php if ($orderinfo->user_id == $agent->id): ?>
                            <div class="col-md-12">
                               <div class="form-group">
                                  <label for="inputEmail4" class="col-sm-6 control-label">Agent Fees</label>
                                  <label class="col-sm-6 control-label values"><?php echo number_format($orderinfo->agent_fees); ?> </label>
                               </div>
                            </div>
                            <!-- <li><a href="<?php echo site_url(); ?>admin/order/agent_fees/<?php echo $orderinfo->order_id; ?>">Agent Fees</a></li> -->
                          <?php endif?>
                        <?php endforeach?>
                       <?php if ($orderinfo->type_of_rent == 38 || $orderinfo->type_of_rent == 37): ?>
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <label for="inputEmail4" class="col-sm-6 control-label">Driver Salary</label>
                                    <label class="col-sm-6 control-label values"><?=number_format($orderinfo->driver_salary)?> </label>
                                 </div>
                              </div>
                      <?php endif?>
                      <?php foreach ($users as $k => $user): ?>
                          <?php if ($orderinfo->supplier_id == $user->id): ?>
                            <?php if ($user->user_lname != 'Sonic Star'): ?>
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <label for="inputEmail4" class="col-sm-6 control-label">Vendor Fees</label>
                                    <label class="col-sm-6 control-label values"><?=number_format($orderinfo->vendor_fees)?></label>
                                 </div>
                              </div>

                            <div class="col-md-12">
                             <div class="form-group">
                                <label for="inputEmail4" class="col-sm-6 control-label">Vendor Payment Status</label>
                                <label class="col-sm-6 control-label values"><?php echo $orderinfo->vendor_payment_status; ?> </label>
                             </div>
                          </div>
                            <?php endif?>
                          <?php endif?>

                        <?php endforeach?>
                        <?php foreach ($agents as $k => $agent): ?>
                        <?php if ($orderinfo->user_id == $agent->id): ?>
                          <div class="col-md-12">
                             <div class="form-group">
                                <label for="inputEmail4" class="col-sm-6 control-label">Agent Payment Status</label>
                                <label class="col-sm-6 control-label values"><?php echo $orderinfo->agent_payment_status; ?> </label>
                             </div>
                          </div>

                            <!-- <li><a href="<?php echo site_url(); ?>admin/order/agent_fees/<?php echo $orderinfo->order_id; ?>">Agent Fees</a></li> -->
                          <?php endif?>
                        <?php endforeach?>

                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Order Status</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->status; ?> </label>
                         </div>
                      </div>



                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Customer Payment Status</label>
                            <label class="col-sm-6 control-label values"><?php echo $orderinfo->customer_payment_status; ?> </label>
                         </div>
                      </div>

                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-6 control-label">Supplier Confirmed</label>
                            <label class="col-sm-6 control-label values">
                               <?php if ($orderinfo->supplier_confirmed == 1) {echo "<span class='btn btn-success'>YES</span>";} else {echo "<span class='btn btn-danger'>NO</span>";}?>
                            </label>
                         </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2"></div>
                </div>
                </form>
            </div>
            <div class="box box-footer" align="right">
              <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default-customer">Send SMS to Customer</button>
              <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default-vendor">Send SMS to Vendor</button>
              <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default-driver">Send SMS to Driver</button>
            <?php if ($is_edit_action && $orderinfo->status == 'Confirmed') {?>
              <?php if ($orderinfo->supplier_confirmed == 1) {?><button data-to="supplier" type="button" class="btn btn-info confirmed_sendmail">Send Confirm Email to Supplier</button><?php }?>
              <?php if ($orderinfo->customer_confirmed == 1) {?><button data-to="customer" type="button" class="btn btn-info confirmed_sendmail">Send Confirm Email to Customer</button><?php }?>
            <?php }?>
            </div>
         </div>
         <!-- /.box -->
      </div>
      <!--/.col (left) -->
   </div>
    <div class="modal fade" id="modal-default-customer">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Send SMS to Customer</h4>
          </div>
          <div class="modal-body">
            <form id="customer-sms-form" class="form-horizontal" action="<?php echo site_url(); ?>admin/order/sms/<?= $orderinfo->order_id ?>" method="post">
                <div class="form-group">
                    <label for="exchange_rate" class="col-md-4 control-label">Phone Number</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" value="<?= $orderinfo->phone ?>" name="phone_no">
                    </div>
                </div>
                <div class="form-group">
                  <label for="exchange_amount" class="col-md-4 control-label">Message</label>
                  <div class="col-md-8">
                      <?php
                        $message =  '#'.$orderinfo->order_id."\n".
                                    trim($orderinfo->trip_record_name)."\n".
                                    'Pickup: '.date('d, M, Y', strtotime($orderinfo->pickup_date)).' - '.date('h:i A', strtotime($orderinfo->pickup_date))."\n".
                                    'Dropoff: '.date('d, M, Y', strtotime($orderinfo->drop_date)).' - '.date('h:i A', strtotime($orderinfo->drop_date))."\n".
                                    $__cartype."\n".
                                    "Driver Name: ".$orderinfo->driver_name."\n".
                                    "Driver Phone: ".$orderinfo->driver_phone."\n".
                                    'Amount: '.number_format($orderinfo->amount)." MMK\n".
                                    'Status: '.$orderinfo->status;
                      ?>
                      <textarea class="form-control" rows="10" name="sms_message"><?= $message ?></textarea>
                  </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="$('#customer-sms-form').submit()">Send Now</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-default-vendor">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Send SMS to Vendor</h4>
          </div>
          <div class="modal-body">
            <form id="vendor-sms-form" class="form-horizontal" action="<?php echo site_url(); ?>admin/order/sms/<?= $orderinfo->order_id ?>" method="post">
                <div class="form-group">
                    <label for="exchange_rate" class="col-md-4 control-label">Phone Number</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" value="<?= $__vendor_tel ?>" name="phone_no">
                    </div>
                </div>
                <div class="form-group">
                  <label for="exchange_amount" class="col-md-4 control-label">Message</label>
                  <div class="col-md-8">
                      <?php
                        $message =  '#'.$orderinfo->order_id."\n".
                                    trim($orderinfo->trip_record_name)."\n".
                                    "Customer Name: ".$orderinfo->name."\n".
                                    "Customer Phone: ".$orderinfo->phone."\n".
                                    'Pickup: '.date('d, M, Y', strtotime($orderinfo->pickup_date)).' - '.date('h:i A', strtotime($orderinfo->pickup_date))."\n".
                                    'Dropoff: '.date('d, M, Y', strtotime($orderinfo->drop_date)).' - '.date('h:i A', strtotime($orderinfo->drop_date))."\n".
                                    $__cartype."\n".
                                    'Amount: '.number_format($orderinfo->vendor_fees)." MMK\n".
                                    'Status: '.$orderinfo->status;
                      ?>
                      <textarea class="form-control" rows="10" name="sms_message"><?= $message ?></textarea>
                  </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="$('#vendor-sms-form').submit()">Send Now</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
<div class="modal fade" id="modal-default-driver">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Send SMS to Driver</h4>
          </div>
          <div class="modal-body">
            <form id="driver-sms-form" class="form-horizontal" action="<?php echo site_url(); ?>admin/order/sms/<?= $orderinfo->order_id ?>" method="post">
                <div class="form-group">
                    <label for="exchange_rate" class="col-md-4 control-label">Phone Number</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" value="<?= $orderinfo->driver_phone ?>" name="phone_no">
                    </div>
                </div>
                <div class="form-group">
                  <label for="exchange_amount" class="col-md-4 control-label">Message</label>
                  <div class="col-md-8">
                      <?php
                        $message =  '#'.$orderinfo->order_id."\n".
                                    trim($orderinfo->trip_record_name)."\n".
                                    "Customer Name: ".$orderinfo->name."\n".
                                    "Customer Phone: ".$orderinfo->phone."\n".
                                    'Pickup: '.date('d, M, Y', strtotime($orderinfo->pickup_date)).' - '.date('h:i A', strtotime($orderinfo->pickup_date))."\n".
                                    'Dropoff: '.date('d, M, Y', strtotime($orderinfo->drop_date)).' - '.date('h:i A', strtotime($orderinfo->drop_date))."\n".
                                    $__cartype."\n".
                                    'Amount: '.number_format($orderinfo->amount)." MMK\n".
                                    'Status: '.$orderinfo->status;
                      ?>
                      <textarea class="form-control" rows="10" name="sms_message"><?= $message ?></textarea>
                  </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="$('#driver-sms-form').submit()">Send Now</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
   <script type="text/javascript">
     var type_of_rent=<?=$orderinfo->type_of_rent?>;
     if(type_of_rent==36){
      $('#daily-status').show();
     }else{
      $('#daily-status').hide();
     }
   </script>
