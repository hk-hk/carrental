<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Orders</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    
          <div class="box-body">
          <h3 class="box-title">Filter</h3>
          <form id="form-filter" class="form-horizontal">
              <div class="row">
                  <?php //if($this->session->userdata('cr_user_role') != 'supplier' && $this->session->userdata('cr_user_role') != 'agent' || $this->session->userdata('cr_user_role') == 'customer'){ 
                    $user_role = $this->session->userdata('cr_user_role');
                    if(in_array($user_role,array('admin','service_provider'))){?>
                  <div class="col-md-6">
                      
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Supplier</label>
                              <div class="col-sm-8">
                                  <select id="supplier_id" name="supplier_id" class="form-control">
                                    <option value="0">Select Option</option>
                                    <?php foreach ($suppliers as $k => $supplier) {if($supplier->user_active == 'YES'){
                                    ?>
                                    <option value="<?php echo $supplier->id; ?>"><?php echo $supplier->user_fname." ".$supplier->user_lname; ?></option>
                                    <?php 
                                    }} ?>

                                  </select>
                              </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Agent</label>
                              <div class="col-sm-8">
                                  <select id="user_id" name="user_id" class="form-control">
                                    <option value="0">Select Option</option>
                                    <?php foreach ($agents as $k => $agent) {if($agent->user_active == 'YES'){
                                    ?>
                                    <option value="<?php echo $agent->id; ?>"><?php echo $agent->user_fname." ".$agent->user_lname; ?></option>
                                    <?php 
                                    }} ?>

                                  </select>
                              </div>
                          </div>
                        </div>
                        <?php if(FALSE){ ?>
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Customer</label>
                              <div class="col-sm-8">
                                  <select id="user_id" name="user_id" class="form-control">
                                    <option value="0">Select Option</option>
                                    <?php foreach ($customers as $k => $customer) {if($customer->user_active == 'YES'){
                                    ?>
                                    <option value="<?php echo $customer->id; ?>"><?php echo $customer->user_fname." ".$customer->user_lname; ?></option>
                                    <?php 
                                    }} ?>

                                  </select>
                              </div>
                          </div>
                        </div>
                      <?php } ?>
                        
                  </div>
                  <?php } ?>
<!--                  SELECT * FROM `cr_orders` WHERE (DATE_FORMAT(`pickup_date`,'%Y-%m-%d') between '2017-09-12' and '2017-09-12')-->
                  <div class="col-md-6">
                      <div class="row" style="margin-bottom: 5px;">
                          <div class="col-md-2">Order Date</div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="From Date" name="booking_date_from" id="booking_date_from" /></div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="To Date" name="booking_date_to" id="booking_date_to" /></div>
                      </div>
                      <div class="row" style="margin-bottom: 5px;">
                          <div class="col-md-2">Pickup Date</div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="From Date"  name="pickup_date_from" id="pickup_date_from" /></div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="To Date" name="pickup_date_to" id="pickup_date_to" /></div>
                      </div>
                      <div class="row">
                          <div class="col-md-2">Dropoff Date</div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="From Date" name="dropoff_date_from" id="dropoff_date_from" /></div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="To Date" name="dropoff_date_to" id="dropoff_date_to" /></div>
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                    <label for="LastName" class="col-sm-1 control-label"></label>
                    <div class="col-sm-11">
                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                        <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
              </div>
            
          </form>
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Order ID</th>
                    <th>Car Type</th>
                    <th>Supplier Name</th>
                    <th>Supplier Phone</th>
                    <?php if($user_role == 'supplier'){ ?>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <?php }else{ ?>
                    <th>Customer/Agent Name</th>
                    <th>Customer/Agent Phone</th>
                    <?php } ?>
                    <th>Pick Up Date</th>
                    <th>Drop-Off  Date</th>
                    <th>Booking Amount</th>
                    <?php if(in_array($user_role,array('supplier','admin'))){ ?>
                    <th>SonicStar Comm.</th>
                    <?php } ?>
                    <?php if(in_array($user_role,array('agent','admin'))){ ?>
                    <th>Agent Comm.</th>
                    <?php } ?>
                    <th>Supplier Amount</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Order ID</th>
                    <th>Car Type</th>
                    <th>Supplier Name</th>
                    <th>Supplier Phone</th>
                    <?php if($user_role == 'supplier'){ ?>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <?php }else{ ?>
                    <th>Customer/Agent Name</th>
                    <th>Customer/Agent Phone</th>
                    <?php } ?>
                    <th>Pick Up Date</th>
                    <th>Drop-Off  Date</th>
                    <th>Booking Amount</th>
                    <?php if(in_array($user_role,array('supplier','admin'))){ ?>
                    <th>SonicStar Comm.</th>
                    <?php } ?>
                    <?php if(in_array($user_role,array('agent','admin'))){ ?>
                    <th>Agent Comm.</th>
                    <?php } ?>
                    <th>Supplier Amount</th>
                </tr>
            </tfoot>
        </table>
        </div>
      </div>
          <!-- /.box-body -->
         <!--  <div class="box-footer">
            <button type="submit" class="btn btn-info">Add User</button>
          </div> -->
          <!-- /.box-footer -->
         
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
 
var table;
 
$(document).ready(function() {
    
    
 $('.datepicker').datepicker({
         autoclose: true
       });
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
        "ordering": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/ajax-reports')?>",
            "type": "POST",
            "data": function (data) {		
                <?php if($this->session->userdata('cr_user_role') != 'supplier' && $this->session->userdata('cr_user_role') != 'agent' || $this->session->userdata('cr_user_role') == 'customer'){ ?>   
                    data.supplier_id = $('#supplier_id').val();
                <?php } ?>
                data.user_id = $('#user_id').val();
                data.booking_date_from = $('#booking_date_from').val();
                data.booking_date_to = $('#booking_date_to').val();
                data.pickup_date_from = $('#pickup_date_from').val();
                data.pickup_date_to = $('#pickup_date_to').val();
                data.dropoff_date_from = $('#dropoff_date_from').val();
                data.dropoff_date_to = $('#dropoff_date_to').val();
                data.status = 'Completed';
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,7 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    }); 
 
});
 
</script>