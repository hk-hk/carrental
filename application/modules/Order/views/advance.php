<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
<style>
.control-label{
text-align: left !important;
text-decoration: none;
}
.values{
font-weight: initial !important;
}
.fa .fa-star {
color: #ff0000 !important;
}
</style>
<!-- Cancellation Pop-up -->
<div id="cancellation" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Resason for cancellation first</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/cancle/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputEmail4" class="control-label">Reason For Cancellation : </label>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="cancleReason" rows="3" placeholder="reason for cancellation..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Cancellation">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Cancellation Pop-up -->
<!-- rating Pop-up -->
<div id="rating" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Give Feedback </h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/submit_feedback/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Rating : </label>
              </div>
              <div class="form-group">
                <!-- 1<input type="radio" name="rating" value="1" required="required">
                2<input type="radio" name="rating" value="2" required="required">
                3<input type="radio" name="rating" value="3" required="required">
                4<input type="radio" name="rating" value="4" required="required">
                5<input type="radio" name="rating" value="5" required="required"> -->
                <input type="hidden" name="rating" id="review" required="required">
                <div class="cmt-lower">
                  <i id="star_1" class="fa fa-star-o starrattings"></i>
                  <i id="star_2" class="fa fa-star-o starrattings"></i>
                  <i id="star_3" class="fa fa-star-o starrattings"></i>
                  <i id="star_4" class="fa fa-star-o starrattings"></i>
                  <i id="star_5" class="fa fa-star-o starrattings"></i>
                </div>
                <script>
                jQuery( document ).ready(function() {
                jQuery(".starrattings").on("mouseover",function(){
                var startt=jQuery(this).attr("id");
                var starid=startt.split("_");
                starid=starid[1];
                for(var i=1;i<=5;i++){
                if(jQuery("#star_"+i).attr("class")=='fa fa-star starrattings'){
                jQuery("#star_"+i).removeClass("fa fa-star starrattings");
                jQuery("#star_"+i).addClass("fa fa-star-o starrattings");
                }
                }
                // alert(starid);
                for(var i=1;i<=starid;i++){
                jQuery("#star_"+i).removeClass("fa fa-star-o starrattings");
                jQuery("#star_"+i).addClass("fa fa-star starrattings");
                }
                jQuery("#review").val(starid);
                });
                });
                </script>
              </div>
              <div class="form-group">
                <label>Comment : </label>
              </div>
              <div class="form-group">
                <input type="hidden" name="order_id" required="required" value="<?php echo $orderinfo->order_id; ?>">
                <textarea class="form-control" name="comment" rows="3" placeholder="Comment..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Feedback">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- rating Pop-up -->
<section class="content">

  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border" align="center">
          <h3 class="box-title">Advance Expense Form</h3>
        </div>
        <div class="box-body">
          <?php $order_history = json_decode($orderinfo->order_history);
          $trackPrices                       = $order_history->trackPrices;
          $searchData                        = $order_history->searchData;
          $currency                          = $order_history->currency;
          ?>
          <?php
          //$currency = isset($orderinfo->order_history->currency)?$orderinfo->order_history->currency:'mmk';
          if ($is_edit_action) {?>
          <div class="row">
            <!-- <div class="col-md-8"></div> -->
            <div class="col-md-12" align="right">
              
              <a href="<?php echo site_url(); ?>admin/order/view/<?php echo $orderinfo->order_id; ?>" class="btn btn-default" name="save" >Exit</a>
            </div>
          </div>
          <?php }?>
          <form id="addarticle-form" class="form-horizontal"  style="text-align: left !important;">
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Order ID</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->order_id; ?></label>
                      <input type="hidden" name="order_id" id="order_id" value="<?php echo $orderinfo->order_id; ?>">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Customer Name</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->name; ?></label>
                      <input type="hidden" name="user_id" id="user_id" value="<?php echo $orderinfo->user_id; ?>">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Customer Phone</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->phone; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Customer Email</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->usr_email; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Supplier Name</label>
                      <label class="col-sm-6 control-label values"><?php foreach ($users as $k => $user) {
                        ?>
                        <?php if ($orderinfo->supplier_id == $user->id) {echo $user->user_fname . " " . $user->user_lname;}?>
                        <?php
                      }?></label>
                      <input type="hidden" name="supplier_id" id="supplier_id" value="<?php echo $orderinfo->supplier_id; ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Pick Up Place</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->pl_title; ?></label>
                    </div>
                  </div>
                  <?php
                  if ($orderinfo->order_history != null || $orderinfo->order_history != '') {
                  $order_history     = json_decode($orderinfo->order_history)->searchData;
                  $order_historyInfo = json_decode($orderinfo->order_history);
                  if ($orderinfo->type_of_rent == 36 && $order_history->incity == 'highway') {?>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Drop off Place</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->dl_title; ?></label>
                    </div>
                  </div>
                  <?php }}?>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Order Date</label>
                      <label class="col-sm-6 control-label values"><?php echo date('d.m.Y (D)', strtotime($orderinfo->booking_date)); ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Payment Type</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->payment_method; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Total Amount</label>
                      <label class="col-sm-6 control-label values"><?php echo $currency . $orderinfo->amount; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Order Status</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->status; ?> </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>
          </form>
        </div>
        <?php if ($is_edit_action && $orderinfo->status == 'Confirmed') {?>
        <div class="box box-footer" align="right">
          <?php if ($orderinfo->supplier_confirmed == 1) {?><button data-to="supplier" type="button" class="btn btn-info confirmed_sendmail">Send Confirm Email to Supplier</button><?php }?>
          <?php if ($orderinfo->customer_confirmed == 1) {?><button data-to="customer" type="button" class="btn btn-info confirmed_sendmail">Send Confirm Email to Customer</button><?php }?>
        </div>
        <?php }?>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
 
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning ">   <!--  -->
      <div class="box-header with-border">
        <h3 class="box-title">Advance (Withdraw /  Deposit)</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <form action="<?php echo site_url(CRM_VAR . '/order/store_advance/' . $orderinfo->order_id) ?>" class="form-horizontal" method="post" id="payment_form">
        <input type="hidden" name="order_id" value="<?=$orderinfo->order_id?>">
        <div class="box-body">
          <!-- <h3 class="box-title">Filter</h3> -->
          <!--  -->
          <div id="own_expense">
            <div class="row">
              <div class="col-md-6">
                <!-- <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" placeholder="Choose Date" name="maintain_date" id="maintain_date" value="<?=!empty($maintain_detail->date) ? $maintain_detail->date : ''?>" required="" />
                  </div>
                </div> -->
               
                <div class="form-group">
                  <label for="payment_type" class="col-md-4 control-label">Payment Type</label>
                  <div class="col-md-8">
                    <!-- <input type="hidden" name="payment_type" id="tt"> -->
                    <select class="form-control" id="payment_type" name="payment_type">
                      <option value="<?= $withdraw->id ?>">Withdraw</option>
                      <option value="<?= $deposit->id ?>">Deposit (Payback)</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="amount" class="col-md-4 control-label">Amount (MMK)</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="amount" name="amount" min="0" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="amount" class="col-md-4 control-label">Transation Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" value="<?= date('m/d/Y') ?>" name="transation_date" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="driver" class="col-md-4 control-label">Driver</label>
                  <div class="col-md-8">

                    <input type="text" class="form-control" id="driver" name="driver" placeholder="No Driver Defined" value="<?php echo $orderinfo->driver_name; ?>" readonly="" required="">
                    <span><a href="<?=site_url(CRM_VAR . '/order/edit/'.$orderinfo->order_id)?>" >Assign Driver</a></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-md-4 control-label">Description</label>
                  <div class="col-md-8">
                    <textarea name="description" id="description" class="form-control" required=""></textarea>
                    <!-- <input type="number" class="form-control" id="exchange_amount" name="exchange_amount" min="0" value="0" readonly=""> -->
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-info pull-right" id="btntransfer" onclick="return confirm('Are you sure to submit?')">Submit</button>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                      <thead>
                          <tr>
                              <th>Transfer Date</th>
                              <th>Withdraw Amount (<?= ($account_detail->account_type==1)? 'MMK' : '$' ?>)</th>
                              <th>Deposit Amount (<?= ($account_detail->account_type==1)? 'MMK' : '$' ?>)</th>
                              <!-- <th>Balance</th> -->
                          </tr>
                      </thead>
                      <tbody>
                          <?php 
                              $withdraw_total=0;
                              $deposit_total=0;
                           ?>
                          <?php foreach ($transactions as $key => $value): ?>
                          <tr>
                              <td><?= date('d/m/Y h:i',$value->transaction_date) ?></td>
                              <td align="right">
                                  <?php if ($value->payment_type == 4416): ?>
                                      <?= number_format($value->amount)  ?>
                                      <?php $withdraw_total+=$value->amount; ?>
                                  <?php else: ?>
                                      -
                                  <?php endif ?>
                              </td>
                              <td align="right">
                                  <?php if ($value->payment_type == 4417): ?>
                                      <?= number_format($value->amount)  ?>
                                      <?php $deposit_total+=$value->amount; ?>
                                  <?php else: ?>
                                      -
                                  <?php endif ?>
                              </td>
                              
                          </tr>
                          <?php endforeach ?>
                          <tr>
                            <td>Total (MMK)</td>
                            <td align="right"><?= number_format($deposit_total) ?></td>
                            <td align="right"><?= number_format($withdraw_total) ?></td>
                          </tr>
                      </tbody>
                      
                  </table>
              </div>
              
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="pull-right">
            
            <!-- <button type="submit" id="complete_order" name="complete_order" value="yes" class="btn btn-info">Complete Order and Transfer</button> -->
            
          </div>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
</section>
<script type="text/javascript">
   $('#diff_msg').hide();
   $('#complete_order').hide();
   var totalAmount=<?= $orderinfo->amount ?>;
  function payCondition(acc_type){
    $('#pm_mmk').val("0");
    $('#pm_usd').val("0");
    $('#exchange_rate').val("0");
    
    if(acc_type==0){
      $('#pm_mmk').attr('readonly',false);
      $('#pm_usd').attr('readonly',false);
      $('#exchange_rate').attr('readonly',false);
    }else if(acc_type==1){
      $('#pm_mmk').attr('readonly',false);
      $('#pm_usd').attr('readonly',true);
      $('#exchange_rate').attr('readonly',true);
    }else if(acc_type==2){
      $('#pm_mmk').attr('readonly',true);
      $('#pm_usd').attr('readonly',false);
      $('#exchange_rate').attr('readonly',false);
    }
     $('#tt').val(acc_type);
  }
  $('#transfer_type').change(function(){
      // console.log(this.val());
    $("#transfer_type option:selected" ).each(function() {
      // console.log("");
      payCondition($(this).val());
    });
  });
  $('#accountinfo').change(function(){
      // console.log(this.val());
    $('#transfer_type').attr('disabled',true);
    $("#accountinfo option:selected" ).each(function() {
      $.post('<?= site_url(CRM_VAR.'/accountinfo/') ?>'+$(this).val(),function(data){
        var obj=$.parseJSON(data);
        if(obj['account_id']==10){
          $('#transfer_type').attr('disabled',false);
        }
        payCondition(obj['account_type']);
      });
      // payCondition($(this).val());
    });
  });

  function calcDiff(){
    var mmk = parseInt($('#pm_mmk').val());
    var usd = parseInt($('#pm_usd').val()) * parseInt($('#exchange_rate').val());

    var paidTotal= parseInt($('#paid_amount').val()) + mmk + usd;
    var diff=totalAmount - paidTotal;
    if(diff<0){
      diff="+"+(Math.abs(diff));
      // diff=diff
    }
    if(diff>0){
      diff="-"+Math.abs(diff);
    }
    $('#difference').val(diff);
    $('#exchange_amount').val(usd);

    if((totalAmount - paidTotal) <= <?= (int)$diff->short_desc ?>){
      // alert("Hello");
      $('#diff_msg').show();
      $('#complete_order').show();
    }else{
      $('#diff_msg').hide();
      $('#complete_order').hide();
    }
  }

  calcDiff();
  <?php if ($orderinfo->status=="Completed"): ?>
      $("#payment_form :input").attr("disabled", true);
      $('#btntransfer').hide();
      
      $('#diff_msg').hide();
      $('#complete_order').hide();
  <?php endif ?>
</script>
<?php if (true): ?>
<script type="text/javascript">
$(document).ready(function() {
$('.datepicker').datepicker({
autoclose: true
});
});
</script>
<?php endif?>