<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
  <style>
   .control-label{
    text-align: left !important;
    text-decoration: none;
  }
  .values{
    font-weight: initial !important;
  }
  .fa .fa-star {
        color: #ff0000 !important;
    }
  </style>
<!-- Cancellation Pop-up -->
<div id="cancellation" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Resason for cancellation first</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/cancle/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputEmail4" class="control-label">Reason For Cancellation : </label>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="cancleReason" rows="3" placeholder="reason for cancellation..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Cancellation">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Cancellation Pop-up -->
<!-- rating Pop-up -->
<div id="rating" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Give Feedback </h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/submit_feedback/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Rating : </label>
              </div>
              <div class="form-group">
                <!-- 1<input type="radio" name="rating" value="1" required="required">
                2<input type="radio" name="rating" value="2" required="required">
                3<input type="radio" name="rating" value="3" required="required">
                4<input type="radio" name="rating" value="4" required="required">
                5<input type="radio" name="rating" value="5" required="required"> -->

                <input type="hidden" name="rating" id="review" required="required">
                <div class="cmt-lower">
                    <i id="star_1" class="fa fa-star-o starrattings"></i>
                    <i id="star_2" class="fa fa-star-o starrattings"></i>
                    <i id="star_3" class="fa fa-star-o starrattings"></i>
                    <i id="star_4" class="fa fa-star-o starrattings"></i>
                    <i id="star_5" class="fa fa-star-o starrattings"></i>
                </div>
                <script>
                jQuery( document ).ready(function() {
                jQuery(".starrattings").on("mouseover",function(){
                        var startt=jQuery(this).attr("id");
                        var starid=startt.split("_");
                        starid=starid[1];

                        for(var i=1;i<=5;i++){
                            if(jQuery("#star_"+i).attr("class")=='fa fa-star starrattings'){
                                jQuery("#star_"+i).removeClass("fa fa-star starrattings");
                                jQuery("#star_"+i).addClass("fa fa-star-o starrattings");
                            }
                        }
                           // alert(starid);
                        for(var i=1;i<=starid;i++){
                                jQuery("#star_"+i).removeClass("fa fa-star-o starrattings");
                                jQuery("#star_"+i).addClass("fa fa-star starrattings");
                        }
                        jQuery("#review").val(starid);
                    });
                });
                </script>
              </div>
              <div class="form-group">
                <label>Comment : </label>
              </div>
              <div class="form-group">
                <input type="hidden" name="order_id" required="required" value="<?php echo $orderinfo->order_id; ?>">
                <textarea class="form-control" name="comment" rows="3" placeholder="Comment..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Feedback">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- rating Pop-up -->
<section class="content">
   <?php include "order_detail.php";?>
   <!-- /.row -->
   <?php foreach ($users as $k => $user): ?>
              <?php if ($orderinfo->supplier_id == $user->id): ?>
                <?php if ($user->user_lname != 'Sonic Star'): ?>
   <!-- <div class="row">
      <div class="col-md-12">
        <div class="box box-warning ">
          <div class="box-header with-border">
            <h3 class="box-title">Trip Expenses History</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">

            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Vendor Paydate</th>
                  <th>Vendor Fees</th>
                  <th>Profit</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($expense_list as $key => $value): ?>
                  <tr class="<?=($value->is_latest == 1) ? 'info' : ''?>">
                    <td><?=$key + 1?></td>
                    <td><?=$value->vendor_paydate?></td>
                    <td><?=$value->vendor_fees?></td>
                    <td><?=$value->profit?></td>
                  </tr>
                <?php endforeach?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div> -->

                <?php endif?>
              <?php endif?>
            <?php endforeach?>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning ">   <!--  -->
          <div class="box-header with-border">
            <h3 class="box-title">Trip Expenses</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <form action="<?php echo site_url(CRM_VAR . '/order/store_expenses/' . $orderinfo->order_id . '/' . $expense_id) ?>" class="form-horizontal" method="post">
            <input type="hidden" name="order_id" value="<?=$orderinfo->order_id?>">
           <div class="box-body">
                  <!-- <h3 class="box-title">Filter</h3> -->
                   <!--  -->
                  <div id="own_expense">

                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                        <div class="col-md-8">
                          <?php foreach ($users as $k => $user): ?>
                              <?php if ($orderinfo->supplier_id == $user->id): ?>
                                <input type="text" class="form-control" value="<?=$user->user_fname . ' ' . $user->user_lname?>" name="vendor_name" readonly="">
                              <?php endif?>
                            <?php endforeach?>
                          <!-- <input type="text" class="form-control" id="title" value="" disabled=""> -->

                        </div>
                      </div>
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">OT Hours</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" id="ot_hour" name="ot_hour" min="0" value="<?= $orderinfo->ot_hours ?>" readonly>

                        </div>
                      </div>
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Driver OT Fees</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" id="driver_ot_fees" name="driver_ot_fees" readonly="" value="<?= $driver_ot_fees->short_desc*$orderinfo->ot_hours ?>">
                          <span>Driver OT Fees is calculated by ( <?=$driver_ot_fees->short_desc?> MMK ) per day</span>

                        </div>
                      </div>
                       <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Driver Fees</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" id="driver_fees" name="driver_fees" readonly="" value="0">
                          <span>Driver Fees is calculated by ( <?=$driver_fees->short_desc?> MMK ) per day</span>

                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Date</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control datepicker" placeholder="Choose Date" name="maintain_date" id="maintain_date" value="<?=!empty($maintain_detail->date) ? $maintain_detail->date : ''?>" required="" />
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Trip Start</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="od_start" value="<?php echo date('d.m.Y (D)', strtotime($orderinfo->pickup_date)); ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Trip End</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="od_end" value="<?php echo date('d.m.Y (D)', strtotime($orderinfo->drop_date)); ?>" readonly>
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label for="paydate" class="col-md-4 control-label">Paydate</label>
                        <div class="col-md-8">
                          <input type="input" class="form-control datepicker" id="paydate" value="<?=isset($trip_expense_detail->vendor_paydate) ? $trip_expense_detail->vendor_paydate : date('m/d/Y')?>" name="paydate">
                        </div>
                      </div> -->
                     <!--  <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Status</label>
                        <div class="col-md-8">
                          <input type="radio" name="status" value="0" id="unpaid" checked=""> <label for="unpaid">Unpaid</label>
                          <input type="radio" name="status" value="1" id="paid"> <label for="paid">Paid</label>
                        </div>
                      </div> -->
                    </div>
                    <div class="col-md-6">

                      <?php
                        $fuel_slip = 0;
                        $fuel_cash = 0;
                        foreach ($fuel_expenses as $key => $value) {
                            if ($value->purchased_by == "cash") {
                                $fuel_cash += $value->amount;
                            } else if ($value->purchased_by == "credit") {
                                $fuel_slip += $value->amount;
                            }
                        }

                        $toll  = 0;
                        $other = 0;

                        foreach ($expenses as $key => $value) {
                            if ($value->category_id == 4451) {
                                $toll += $value->amount;
                            } else if ($value->category_id == 4452) {
                                $other += $value->amount;
                            }
                        }

                        ?>
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Fuel - Slip</label>
                        <div class="col-md-8">


                          <input type="text" class="form-control number-format" id="fuel_slip" name="fuel_slip" min="0" value="<?=$fuel_slip?>" step="1000" onchange="totalexpense()" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Fuel - Cash</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control number-format" id="fuel_cash" name="fuel_cash" min="0" value="<?=$fuel_cash?>" step="1000" onchange="totalexpense()" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Toll Expense</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control number-format" id="toll_expense" name="toll_expense" min="0" value="<?=$toll?>" step="1000" onchange="totalexpense()" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Other Expense</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control number-format" id="other_expense" name="other_expense" min="0" value="<?=$other?>" step="1000" onchange="totalexpense()" readonly>
                        </div>
                      </div>
                      <?php if (isset($orderinfo->order_by) && $orderinfo->user_role == 'agent'): ?>
                        <?php $total_agent_fees = 0;?>
                        <?php foreach ($expense_list as $key => $value): ?>
                          <?php if ($value->pay_to == "agent") {
                              $total_agent_fees += $value->vendor_fees;
                          }?>
                        <?php endforeach?>
                        <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label"><?=($total_agent_fees != 0) ? 'Agent Fees' : 'Estimated Agent Fees'?></label>
                        <div class="col-md-8">
                          <?php foreach ($agents as $k => $agent): ?>
                      <?php if ($orderinfo->order_by == $agent->id): ?>
                          <input type="text" class="form-control number-format" id="agent_fees_expense" name="agent_fees_expense" min="0" value="<?=($total_agent_fees != 0) ? $total_agent_fees : ($orderinfo->amount * $agent->commission_rate / 100)?>" step="1000" onchange="totalexpense()" readonly>
                          <span>Calculated With Agent Commission Rate : <?=$agent->commission_rate?>%</span>
                      <?php endif?>
                      <?php endforeach?>
                        </div>
                      </div>
                      <?php endif?>
                      <!-- <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label">Comment</label>
                        <div class="col-md-8">
                          <textarea class="form-control" name="comment" ><?=isset($trip_expense_detail->comments) ? $trip_expense_detail->comments : ''?></textarea>
                        </div>
                      </div> -->

                    </div>

                    </div>

                  </div>
                  <div id="supplier_expense">
                    <div class="row">
                      <?php foreach ($users as $k => $user): ?>
                      <?php if ($orderinfo->supplier_id == $user->id): ?>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="FirstName" class="col-md-4 control-label">Rental Amount</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control number-format" value="<?=$orderinfo->amount?>" value="" disabled="">
                            <?php foreach ($users as $k => $user): ?>
                            <?php if ($orderinfo->supplier_id == $user->id): ?>
                            <!-- <span></span> -->
                            <span>Estimated Vendor Fees <font color="red"><?= number_format($orderinfo->amount - ($orderinfo->amount * $user->commission_rate / 100)) ?></font></span>
                            <?php endif?>
                        <?php endforeach?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" value="<?=$user->user_fname . ' ' . $user->user_lname?>" value="" disabled="">
                          </div>
                        </div>
<!--                         <div class="form-group">
                          <label for="vendor_fees" class="col-md-4 control-label">Vendor Fees</label>
                          <div class="col-md-8">

                            <input type="text" class="form-control number-format" id="vendor_fees" name="vendor_fees" min="0" value="<?= $orderinfo->amount - ($orderinfo->amount * $user->commission_rate / 100)?>" onchange="vtotalexpense()">
                            <span><?=$user->commission_rate?>% to Sonic Star and <?=100 - $user->commission_rate?>% to <?=$user->user_fname . ' ' . $user->user_lname?></span>
                          </div>
                        </div> -->


                        <?php foreach ($users as $k => $user): ?>
                            <?php if ($orderinfo->supplier_id == $user->id): ?>
                                <?php $total_vendor_fees = 0;?>
                                <?php if ($user->user_lname != 'Sonic Star'): ?>
                                  <?php foreach ($expense_list as $key => $value): ?>
                                    <?php if ($value->pay_to == "vendor") {
                                        $total_vendor_fees += $value->vendor_fees;
                                    }?>
                                  <?php endforeach?>
                                  <div class="form-group">
                                  <label for="FirstName" class="col-md-4 control-label"><?=($total_vendor_fees != 0) ? 'Vendor Fees' : 'Estimated Vendor Fees'?></label>
                                  <div class="col-md-8">
                                  <input type="text" class="form-control number-format" id="vendor_fees" name="vendor_fees" min="0" value="<?= $orderinfo->vendor_fees ?>" step="1000" onchange="totalexpense()" readonly>
                                  <!-- <span><?=$user->commission_rate?>% to Sonic Star and <?=100 - $user->commission_rate?>% to <?=$user->user_fname . ' ' . $user->user_lname?></span> -->
                                  </div>
                                  </div>
                                <?php endif?>

                            <?php endif?>
                        <?php endforeach?>

                        <?php if (isset($orderinfo->order_by) && $orderinfo->user_role == 'agent'): ?>
                        <?php $total_agent_fees = 0;?>
                        <?php foreach ($expense_list as $key => $value): ?>
                          <?php if ($value->pay_to == "agent") {
                              $total_agent_fees += $value->vendor_fees;
                          }?>
                        <?php endforeach?>
                        <div class="form-group">
                        <label for="FirstName" class="col-md-4 control-label"><?=($total_agent_fees != 0) ? 'Agent Fees' : 'Estimated Agent Fees'?></label>
                        <div class="col-md-8">
                          <?php foreach ($agents as $k => $agent): ?>
                      <?php if ($orderinfo->order_by == $agent->id): ?>
                          <input type="text" class="form-control number-format" id="agent_fees_expense" name="agent_fees_expense" min="0" value="<?=($total_agent_fees != 0) ? $total_agent_fees : ($orderinfo->amount * $agent->commission_rate / 100)?>" step="1000" onchange="totalexpense()" readonly>
                          <span>Calculated With Agent Commission Rate : <?=$agent->commission_rate?>%</span>
                      <?php endif?>
                      <?php endforeach?>
                        </div>
                      </div>
                      <?php endif?>
                        <!-- <div class="form-group">
                          <label for="vendor_paydate" class="col-md-4 control-label">Vendor Paydate</label>
                          <div class="col-md-8">
                            <input type="input" class="form-control datepicker" id="vendor_paydate" value="<?=isset($trip_expense_detail->vendor_paydate) ? $trip_expense_detail->vendor_paydate : ''?>" name="vendor_paydate">
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label for="vendor_profit" class="col-md-4 control-label">Sonic Star Profit</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control number-format" id="vendor_profit" name="vendor_profit" value="0" readonly="">
                          </div>
                        </div>

                        <!-- <div class="form-group">
                          <label for="status_vendor" class="col-md-4 control-label">Status</label>
                          <div class="col-md-8">
                            <select name="status_vendor" class="form-control">
                              <option value="pending">Pending</option>
                              <option value="complete">Complete</option>
                            </select>

                          </div>
                        </div> -->
                      </div>
                        <?php endif?>
                      <?php endforeach?>
                    </div>

                  </div>
                  <div id="detail">
                    <div class="row">
                      <div class="col-md-3">

                          <div class="form-group">
                            <label for="FirstName" class="col-md-4 control-label">Days Of Trip</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" id="days_of_trip" name="days_of_trip" readonly>

                            </div>
                          </div>

                      </div>
                        <div class="col-md-3">

                          <div class="form-group">
                            <label for="FirstName" class="col-md-4 control-label">Driver Bonus</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" id="driver_bonus" name="driver_bonus" value="0" readonly="">
                              <span>( <?=$driver_percentage->short_desc?> % ) of Net Income</span>

                            </div>
                          </div>
                      </div>

                      <div class="col-md-3">

                          <div class="form-group">
                            <label for="FirstName" class="col-md-4 control-label">Total Amount</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control number-format" id="total_amount" name="total_amount" value="<?php echo $orderinfo->amount; ?>" readonly="">

                            </div>
                          </div>
                      </div>
                      <div class="col-md-3">

                          <div class="form-group">
                            <label for="FirstName" class="col-md-4 control-label">Net Income</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control number-format" id="net_income" name="net_income" value="<?php echo $orderinfo->amount; ?>" readonly="">

                            </div>
                          </div>
                      </div>
                    </div>
                    <div class="row" >
                      <div class="col-md-3"><input type="hidden" name="calc_ot_driver_fees" value="<?=$driver_ot_fees->short_desc?>"></div>
                      <div class="col-md-3"><input type="hidden" name="calc_driver_fees" value="<?=$driver_fees->short_desc?>"></div>
                      <input type="hidden" name="calc_driver_percentage" value="<?=$driver_percentage->short_desc?>">
                      <div class="col-md-3">

                          <div class="form-group">
                            <label for="FirstName" class="col-md-4 control-label">Total Expense</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control number-format" id="total_expense" name="total_expense" value="0" readonly>

                            </div>
                          </div>
                      </div>
                      <div class="col-md-3">

                          <div class="form-group">
                            <label for="FirstName" class="col-md-4 control-label">Profit</label>
                            <div class="col-md-8">
                              <input type="text" class="form-control number-format" id="profit" name="profit" value="0" readonly>

                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--  <div class="box-footer">

                  <button type="submit" class="btn btn-info pull-right" id="btn-pay" onclick="return confirm('Do you want to <?=$btn?>?')"><?=$btn?></button>
                </div> -->
              </form>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>

    <?php if ($orderinfo->status == 'Completed') {
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning collapsed-box">   <!--  -->
          <div class="box-header with-border">
            <h3 class="box-title">Feedbacks</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <?php if (!$is_autorize_for_rating && $this->session->userdata("cr_user_role") != 'admin') {?>
            <button data-toggle="modal" data-target="#rating" class="btn btn-warning" >Give Feedback</button>
            <?php }?>
            <?php
if (!empty($order_ratings)) {
        ?>
            <div class="row">
              <?php
if ($this->session->userdata("cr_user_role") == 'admin') {
            foreach ($order_ratings as $key => $value) {
                ?>
                  <div class="col-md-6">
                    <h3><?php echo $value->rating_by; ?> Feedback</h3>
                     <div class="cmt-lower">
                                 Rating  :
                        <?php
for ($i = 1; $i <= 5; $i++) {
                    if ($i <= $value->rating) {
                        echo '<i id="star_' . $i . '" class="fa fa-star starrattings"></i>';
                    } else {
                        echo '<i id="star_' . $i . '" class="fa fa-star-o starrattings"></i>';
                    }
                }
                ?>
                              </div>

                    <!-- <?php echo $value->rating; ?><br> -->
                    Comment  : <?php echo $value->comment; ?><br>
                  </div>
                  <?php
}
        }
        if ($this->session->userdata("cr_user_role") == 'agent' || $this->session->userdata("cr_user_role") == 'customer') {
            foreach ($order_ratings as $key => $value) {
                if ($value->rating_by == 'Customer') {
                    ?>
                  <div class="col-md-6">
                    <h3><?php if ($value->user_id == $this->session->userdata("crm_user_id")) {echo "My";} else {echo $value->rating_by;}?> Feedback</h3>
                     <div class="cmt-lower">
                               Rating  :
                        <?php
for ($i = 1; $i <= 5; $i++) {
                        if ($i <= $value->rating) {
                            echo '<i id="star_' . $i . '" class="fa fa-star starrattings"></i>';
                        } else {
                            echo '<i id="star_' . $i . '" class="fa fa-star-o starrattings"></i>';
                        }
                    }
                    ?>
                            </div><!-- <?php echo $value->rating; ?><br> -->
                    Comment  : <?php echo $value->comment; ?><br>
                  </div>
                  <?php
} else {
                    if ($is_autorize_for_rating) {
                        ?>
                    <div class="col-md-6">
                      <h3><?php if ($value->user_id == $this->session->userdata("crm_user_id")) {echo "My";} else {echo $value->rating_by;}?> Feedback</h3>

                      <div class="cmt-lower">
                          Rating  :
                        <?php
for ($i = 1; $i <= 5; $i++) {
                            if ($i <= $value->rating) {
                                echo '<i id="star_' . $i . '" class="fa fa-star starrattings"></i>';
                            } else {
                                echo '<i id="star_' . $i . '" class="fa fa-star-o starrattings"></i>';
                            }
                        }
                        ?>
                      </div>
                      <!-- <?php echo $value->rating; ?><br> -->
                      Comment  : <?php echo $value->comment; ?><br>
                    </div>
                    <?php } else {
                        ?><div class="col-md-6"><?php echo $value->rating_by . " has given feedback. You can see after submitting your feedback."; ?></div><?php
}
                }
            }
        }
        if ($this->session->userdata("cr_user_role") == 'supplier') {
            foreach ($order_ratings as $key => $value) {
                if ($value->rating_by == 'Supplier') {
                    ?>
                  <div class="col-md-6">
                    <h3><?php if ($value->user_id == $this->session->userdata("crm_user_id")) {echo "My";} else {echo $value->rating_by;}?> Feedback</h3>

                    <div class="cmt-lower">
                         Rating  :
                        <?php
for ($i = 1; $i <= 5; $i++) {
                        if ($i <= $value->rating) {
                            echo '<i id="star_' . $i . '" class="fa fa-star starrattings"></i>';
                        } else {
                            echo '<i id="star_' . $i . '" class="fa fa-star-o starrattings"></i>';
                        }
                    }
                    ?>
                      </div>
                    <!-- <?php echo $value->rating; ?><br> -->
                    Comment  : <?php echo $value->comment; ?><br>
                  </div>
                  <?php
} else {
                    if ($is_autorize_for_rating) {
                        ?>
                    <div class="col-md-6">
                      <h3><?php if ($value->user_id == $this->session->userdata("crm_user_id")) {echo "My";} else {echo $value->rating_by;}?> Feedback</h3>

                      <div class="cmt-lower">
                        Rating  :
                        <?php
for ($i = 1; $i <= 5; $i++) {
                            if ($i <= $value->rating) {
                                echo '<i id="star_' . $i . '" class="fa fa-star starrattings"></i>';
                            } else {
                                echo '<i id="star_' . $i . '" class="fa fa-star-o starrattings"></i>';
                            }
                        }
                        ?>

                      </div>
                      <!-- <?php echo $value->rating; ?><br> -->
                      Comment  : <?php echo $value->comment; ?><br>
                    </div>
                    <?php } else {
                        ?><div class="col-md-6"><?php echo $value->rating_by . " has given feedback. You can see after submitting your feedback."; ?></div><?php
}
                }
            }
        }
        ?>

            </div>
            <?php
}
    ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <?php }?>
</section>


<script type="text/javascript">
  $('.datepicker').datepicker({
        autoclose: true
    });
  $(".confirmed_sendmail").click(function(){
      var res = confirm('Are you sure ?');
      if(res){
        var to = $(this).data()['to'];
        var user_id = $("#user_id").val();
        var supplier_id = $("#supplier_id").val();
        var order_id = $("#order_id").val();
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url(CRM_VAR . '/Order/send_confirmation_mail') ?>',
          data: {to:to,user_id:user_id,supplier_id:supplier_id,order_id:order_id},
          dataType: "text",
          success: function(resultData) {
            alert(resultData);
          }
        });
      }
    });
  var days_of_trip;
  var driver_fees=<?=$driver_fees->short_desc?>;
  var driver_ot_fees=<?=$driver_ot_fees->short_desc?>;
  function daysOfTrip(){
    var pickup_date = new Date("<?php echo date('m/d/Y', strtotime($orderinfo->pickup_date)); ?>");
    var drop_date = new Date("<?php echo date('m/d/Y', strtotime($orderinfo->drop_date)); ?>");
    // var timeDiff = Math.abs(drop_date.getTime() - pickup_date.getTime());
    var timeDiff=<?=strtotime($orderinfo->drop_date) - strtotime($orderinfo->pickup_date);?>;
    var diffDays = Math.ceil(timeDiff / 86400);
    // alert(diffDays);
    $('#days_of_trip').val(diffDays);
    $('#driver_fees').val(diffDays*driver_fees);
    days_of_trip=diffDays;
  }

  daysOfTrip();
  // $('#driver_ot_fees').val($('#ot_hour').value*driver_ot_fees);
  $('#ot_hour').keyup(function(){
    $('#driver_ot_fees').val(this.value*driver_ot_fees);
    totalexpense();
  });
  $("#ot_hour").bind('keyup mouseup', function () {
     $('#driver_ot_fees').val(this.value*driver_ot_fees);
     totalexpense();
  });

  // $("#fuel_slip").keyup(function());
  // $("#fuel_slip").bind('keyup mouseup', function ());

  // $("#fuel_cash").keyup(function());
  // $("#fuel_cash").bind('keyup mouseup', function ());

  // $("#toll_expense").keyup(function());
  // $("#toll_expense").bind('keyup mouseup', function ());

  // $("#other_expense").keyup(function());
  // $("#oth").bind('keyup mouseup', function ());

  function totalexpense(){
    var fuel_slip = parseInt($("#fuel_slip").val());
    var fuel_cash = parseInt($("#fuel_cash").val());
    var toll_expense = parseInt($("#toll_expense").val());
    var other_expense = parseInt($("#other_expense").val());
    var driver_ot = parseInt($("#driver_ot_fees").val());
    var driver_fees = parseInt($("#driver_fees").val());

    var total_expense= parseInt(fuel_slip + fuel_cash + toll_expense + other_expense + driver_ot + driver_fees);
    $('#total_expense').val(total_expense);

    var total_amount = parseInt($("#total_amount").val());
    var net_income = parseInt(total_amount - total_expense);

    $('#net_income').val(net_income);
    var driver_percentage=<?=$driver_percentage->short_desc?>;
    var driver_bonus = net_income * driver_percentage / 100 ;
    // console.log();
    $('#driver_bonus').val(driver_bonus);

    var profit= net_income-driver_bonus ;
    $('#profit').val(profit);
  }

  function vtotalexpense(){
    var vendor_fees = parseInt($("#vendor_fees").val());
    var total_amount = parseInt($("#total_amount").val());
    var profit= total_amount-vendor_fees ;
    $('#vendor_profit').val(profit);
  }
  totalexpense();
  vtotalexpense();
  <?php foreach ($users as $k => $user): ?>
    <?php if ($orderinfo->supplier_id == $user->id): ?>
        // alert("<?=$user->user_lname?>");
      <?php if ($user->user_lname == 'Sonic Star'): ?>
        $('#supplier_expense').hide();

      <?php else: ?>
        $('#own_expense').hide();
        $('#detail').hide();
        $('#vendor_fees').attr('required',true);
        $('#vendor_paydate').attr('required',true);
        $('#btn-pay').html("Pay to vendor");
        $('.box-footer').hide();
        $('#vendor_fees').attr('disabled',true);
        $('#vendor_paydate').attr('disabled',true);
        // $('').hide();
        $('#btn-pay').attr('disabled',true);


      <?php endif?>
    <?php endif?>
  <?php endforeach?>

</script>