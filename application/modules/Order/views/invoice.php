<!DOCTYPE html>
<!--
  Invoice template by invoicebus.com
  To customize this template consider following this guide https://invoicebus.com/how-to-create-invoice-template/
  This template is under Invoicebus Template License, see https://invoicebus.com/templates/license/
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Car Rental Invoice</title>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Invoicebus Invoice Template">
    <meta name="author" content="Invoicebus">

    <meta name="template-hash" content="baadb45704803c2d1a1ac3e569b757d5">
    
    <link rel="stylesheet" href="<?= base_url('public/invoice/css/template.css')?>">
    <link rel="stylesheet" href="<?= base_url('public/invoice/css/ib-crack-css.css')?>">
    <style type="text/css" media="screen">
      .ib_highlight_editable,
      .ib_open_data,
      #ib-save-data-btn,
      .ib_save_online,
      .ib_gray_link,
      .ib_top_separator,
      .ib_pull_right,
      .ib_invoicebus_love   {
        display: none;
      }
      #sums table tr td{
        padding: 2px 5px;
      }
      #copyright table tr td{
        font-size: 13px !important;
        font-weight: normal !important;
        text-align: center !important;
      }
      .footer {
          /*position: fixed;*/
          top: 50px;
          width: 100%;
      }
      #copyright {
          position: absolute;
          bottom: 0;
          padding-left: 40px;
          height: 200px;
      }
      #outer {
          position: relative;
          padding-bottom: 260px;
      }
    </style>
  </head>
  <body>
    <div id="container">
      <section id="memo">
        <div class="logo">
          <img src="<?=base_url('public/images/sonic_star_logo.png')?>" />
        </div>
        
        <div class="company-info">
          <div>Sonic Star Co.,Ltd</div>

          <br />
          
          <span>No.2# 2D, 1st Floor, Moe Kaung Road</span>
          <span>Yankin Tsp Yangon, Myanmar</span>

          <br />
          
          <span>Tel:09 426 999 300,</span>
          <span>rental@sonicstartravel.com, www.sonicstartravel.com</span>
        </div>

      </section>

        
      <section id="invoice-title-number" style="text-align: center">
        
        <span style="font-size: 25px"><?=strtoupper($_GET['title'])?> </span>
        <!-- <span id="number" style="font-size: 25px">#{{$order->id}}</span> -->
        
      </section>
      
      <!-- <div class="clearfix"></div> -->

     
      <section id="client-info"  style="float:right;margin-right: 60px;">
        <!-- <span>OFFICIAL RECEIPT</span> -->
        <table>
          <tbody>
            <tr>
              <td class="bold">Invoice No</td>
              <td width="10px"> : </td>
              <td>#<?= $orderinfo->order_id ?></td>
            </tr>
            
            <tr>
              <td class="bold">Date</td>
              <td width="10px"> : </td>
              <td><?= date('d D, M / Y') ?></td>
            </tr>
            <tr>
              <td class="bold">Customer Name</td>
              <td width="10px"> : </td>
              <td><?= $userinfo->user_fname.' '.$userinfo->user_lname ?></td>
            </tr>
            <tr>
              <td class="bold">Pickup Date</td>
              <td width="10px"> : </td>
              <td class="bold"><?= date('d, m Y H:i A',strtotime($orderinfo->pickup_date)) ?></td>
            </tr>
            <tr>
              <td class="bold">Dropoff Date</td>
              <td width="10px"> : </td>
              <td class="bold"><?= date('d, m Y H:i A',strtotime($orderinfo->drop_date)) ?></td>
            </tr>

          </tbody>
        </table>
        <!-- <div>
          <span class="bold">Doc No.: INV-{{$order->id}}</span>
        </div> -->
        
        <!-- <div>
          <span>Issue Date: {{date('d D, M / Y',$order->issue_date)}}</span>
        </div>
        
        <div>
          <span>Payment: {{ ucfirst($order->payment_type)}}</span>
        </div> -->
        
        <!-- <div>
          <span>Tour Code: INV-{{$order->id}}</span>
        </div> -->
        <!-- <div>
          <span>Travel Date: {{date('d D, M / Y',$order->departure_date)}}</span>
        </div> -->
      </section>
      
      <div class="clearfix"></div>
      
      <section id="items">
        
       
        <!-- <table cellpadding="0" cellspacing="0">
          
          <tr>
            <th>Origin/Dest;</th>
            <th>Flight No</th>
            <th>Date</th>
            <th>Time</th>
            <th>PNR</th>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          
        </table> -->
        <table border="1">
          <tbody>
            <tr>
              <!-- <th>#</th> -->
              <th>Qty</th>
              <th>Car Type</th>
              <th style="width: 30%;">Description</th>
              <th>Unit Price</th>
              <th>Total</th>
              <!-- <th>Amount(USD)</th> -->
            </tr>
            <tr>
              <!-- <td>#</td> -->
              <td>1</td>
              <td>
                <?php foreach ($cars as $kc => $car) { ?>
                  <?php
                  if ($orderinfo->car_id == $car->id) {
                    echo $this->car_manager->get_carname($car->id);
                  }
                  ?>
                <?php } ?>
              </td>
              <td><?= $orderinfo->trip_record_name ?> (<?php if ($orderinfo->type_of_rent == 36) {echo "Daily";}?><?php if ($orderinfo->type_of_rent == 37) {echo "Weekly";}?><?php if ($orderinfo->type_of_rent == 38) {echo "Monthly";}?>)</td>
              <td><?= number_format($orderinfo->booking_amount) ?></td>
              <td><?= number_format($orderinfo->booking_amount) ?></td>
            </tr>
            <?php if ($orderinfo->ot_hours>0): ?>
              
              <tr>
                <!-- <td>#</td> -->
                <td><?= $orderinfo->ot_hours ?></td>
                <td>-</td>
                <td>OT Hours</td>
                <td><?= $orderinfo->driver_ot_fees ?></td>
                <td><?= $orderinfo->driver_ot_fees * $orderinfo->ot_hours ?></td>

              </tr>
            <?php endif ?>
            <?php if ($orderinfo->no_of_zone>0): ?>
              
              <tr>
                <!-- <td>#</td> -->
                <td><?= $orderinfo->no_of_zone ?></td>
                <td>-</td>
                <td>Zone Fees</td>
                <td><?= $orderinfo->zone_fees ?></td>
                <td><?= $orderinfo->no_of_zone * $orderinfo->zone_fees ?></td>

              </tr>
            
            <!--   <tr>
                <td>#</td>
                <td>-</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr> -->
            <?php endif ?>
            <tr>
              <!-- <td></td> -->
              <td>-</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
              
            <tr>
              <td colspan="3" rowspan="3" style="text-align: left">
              <?php if ($orderinfo->ot_hours>0): ?>
                  <table>
                    <tr>
                      <th style="text-align: left">OT Hours</th>
                    </tr>
                  </table>
                  <?php foreach ($ot_hours_data as $key => $value): ?>
                    <?= date('d, M Y',$key) ?> - (<?= $value ?> Hours)<br/>
                  <?php endforeach ?>
              <?php endif ?>
              </td>
              <td style="border-bottom: none;"><b>Sub Total</b></td>
              <td><?= number_format($orderinfo->booking_amount + ($orderinfo->driver_ot_fees * $orderinfo->ot_hours)+$orderinfo->no_of_zone * $orderinfo->zone_fees) ?></td>
            </tr>
            <tr>
              <td style="border-bottom: none;"><b>Tax Included</b></td>
              <td>5%</td>
            </tr>
            <tr>
              <td style="border-bottom: none;"><b>Total</b></td>
              <td><?= number_format($orderinfo->booking_amount + ($orderinfo->driver_ot_fees * $orderinfo->ot_hours) + $orderinfo->no_of_zone * $orderinfo->zone_fees) ?></td>
            </tr>
          </tbody>
        </table>
        
      </section>
      
      <section id="sums">
      
       
        <div id="outer" >
          <!-- class="footer" -->
          <div id="copyright">
            <!-- <div class="col-md-4"> -->
            <table width="100%" style="margin-top: 80px">
              <!-- <tbody>
                <tr>
                  <td width="36.66%">
                    <br>
                    <br>
                    <br>
                    <p>_____________________</p>
                    <span>Customer stamp &<br>Signature</span>
                    
                  </td>
                  <td width="73.32%">
                    <br/>
                    <br>
                    <p style="text-align: right; padding-right: 80px">Date : <b><?= date('d M y') ?></b></p>
                    <p>___________________________________________________________</p>
                    <p  style="text-align: left; padding-left: 70px">Authorized By <strong></strong></span>
                  </td>
                  
                  
                </tr>
                
              </tbody> -->
              <tbody>
                <tr>
                  <td width="36.66%" align="center">
                    <p>For Sonic Star Travel</p>
                    <br>
                    <br>
                    <p>_____________________</p>
                    <span>Prepared By <strong><?= $auth_user->user_fname.' '.$auth_user->user_lname ?></strong></span>
                    
                  </td>
                  <td width="36.66%">
                    <br>
                    <br>
                    <br>
                    <p>_____________________</p>
                    <span>Verified By</span>
                    
                  </td>
                  <td width="36.66%">
                    <br>
                    <br>
                    <br>
                    <p>_____________________</p>
                    <span>Customer stamp &<br>Signature</span>
                    
                  </td>
                </tr>
                
              </tbody>
            </table>
            <!-- <hr style="padding-top: 50px;border-bottom: 1px solid #f1f1f1;"> -->
            <table width="100%">
              <tbody>
                <tr>
                  <td colspan="2"><strong>Sonic Star Co.,Ltd Bank Information</strong></td>
                </tr>
                <tr>
                  <td>
                    KBZ Bank Acc (USD)      - 26210-92620-03422-01<br/>
                    KBZ Bank Acc (MMK)      - 12610-31260-19073-01
                  </td>
                  <td>
                    CB Bank Acc (USD)      - 01171-01200-00022-1<br/>
                    CB Bank Acc (MMK)      - 01171-00500-00039-6
                  </td>
                  
                </tr>
                <tr>
                  <td>AYABank Acc (MMK)      - 0020-1030-1070-3322</td>
                  <td>
                    MAB Bank Acc (USD)      - 996-02-13-996021485-01-1<br/>
                    MAB Bank Acc (MMK)      - 002-01-63-996021485-01-7
                  </td>
                </tr>
                <tr>
                  <td colspan="2" style="padding-top: 20px;text-align: left !important" width="100%"><b>Payment Terms & Conditaions </b>: Payment to be settled within 2 weeks of this invoice received. After 30 days 5% interest will added to the total amount. After 60 days, a 10 % interest per month will be added to the total amount and over 90 days, a 15% interest per month will be added to the total amount. </td>
                </tr>
              </tbody>
            </table>
            <!-- </div>  -->
          </div>
        </div>
        <div class="clearfix"></div>
        
      </section>
      
      <!-- <div class="clearfix"></div>

      

      <div class="clearfix"></div>


      <div class="clearfix"></div> -->
    </div>
    <script src="<?= base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
    <script src="<?= base_url('public/invoice/generator.min.js') ?>"></script>
    <script src="<?= base_url('public/invoice/jquery.min.js') ?>"></script>
    <script src="<?= base_url('public/invoice/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('public/invoice/docs/save-state/save-state-en.js') ?>"></script>
    <script src="<?= base_url('public/invoice/docs/raw-data/raw-data-en.js') ?>"></script>
    <!-- <script type="text/javascript" src="//cdn.invoicebus.com/generator/generator.min.js?data=data.js"></script> -->
    <!-- <script src="http://cdn.invoicebus.com/generator/jquery.min.js"></script> -->
    <!-- <script src="http://cdn.invoicebus.com/generator/bootstrap.min.js"></script> -->
    <!-- <script src="http://cdn.invoicebus.com/generator/docs/save-state/save-state-en.js"></script> -->
    <!-- <script src="http://cdn.invoicebus.com/generator/docs/raw-data/raw-data-en.js"></script> -->


    <script type="text/javascript">
      
var ib_invoice_data = {
  
};

    </script>
  </body>
</html>

