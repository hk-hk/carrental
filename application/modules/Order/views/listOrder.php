<section class="content">
  <div class="row">
    
    <!-- left column -->
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Orders</h3>
        </div>
          <div class="box-body">
          <h3 class="box-title">Filter</h3>
          <form id="form-filter" class="form-horizontal">
              <div class="row">
                  <div class="col-md-6">
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Order ID</label>
                              <div class="col-sm-8">
                                  <input type="text" id="order_id" class="form-control" placeholder="Order ID">
                              </div>
                          </div>
                      </div>
                      <?php if($is_edit_action){ ?>
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Supplier</label>
                              <div class="col-sm-8">
                                  <select id="supplier_id" name="supplier_id" class="form-control selectpicker" data-live-search="true">
                                    <option value="0">Select Option</option>
                                    <?php foreach ($suppliers as $k => $supplier) {if($supplier->user_active == 'YES'){
                                    ?>
                                    <option value="<?php echo $supplier->id; ?>"><?php echo $supplier->user_fname." ".$supplier->user_lname; ?></option>
                                    <?php 
                                    }} ?>

                                  </select>
                              </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Customer</label>
                              <div class="col-sm-8">
                                  <select id="user_id" name="user_id" class="form-control selectpicker" data-live-search="true">
                                    <option value="0">Select Option</option>
                                    <?php foreach ($customers as $k => $customer) {
                                        if($customer->user_role=='agent')
                                            $optionLevel = ($customer->user_company!='')?ucfirst($customer->user_company):$customer->user_fname." ".$customer->user_lname;
                                        else
                                            $optionLevel = ucwords ($customer->user_fname." ".$customer->user_lname);
                                        ?>
                                    <option value="<?php echo $customer->id; ?>"><?php echo $optionLevel ?></option>
                                    <?php } ?>

                                  </select>
                              </div>
                          </div>
                        </div>
                        <?php } ?>
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Order Status</label>
                              <div class="col-sm-8">
                                  <select name="status" id="status" class="form-control">
                                    <option value="" <?php if($order_status == 0){echo "selected";} ?>>Select Option</option>
                                    <option value="Pending"  <?php if($order_status == "Pending"){echo "selected";} ?>>Pending</option>
                                    <option value="Confirmed"  <?php if($order_status == "Confirmed"){echo "selected";} ?>>Confirmed</option>
                                    <option value="Cancelled"  <?php if($order_status == "Cancelled"){echo "selected";} ?>>Cancelled</option>
                                    <option value="Completed"  <?php if($order_status == "Completed"){echo "selected";} ?>>Completed</option>
                                  </select>
                              </div>
                          </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="LastName" class="col-sm-4 control-label">Driver</label>
                                <div class="col-sm-8">
                                    <select name="driver" id="driver" class="form-control selectpicker" data-live-search="true">
                                        <option value="">Select Driver</option>
                                      <?php foreach ($drivers as $key => $value): ?>
                                        <option value="<?= $value->user_lname ?>"><?= $value->user_lname ?></option>
                                      <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="LastName" class="col-sm-4 control-label">Rent Type</label>
                                <div class="col-sm-8">
                                    <select name="rent_type" id="rent_type" class="form-control">
                                        <option value="">Select Rent Type</option>
                                        <option value="36">Daily</option>
                                        <option value="37">Weekly</option>
                                        <option value="38">Monthly</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12"  id="trip_type_div">
                           
                              <label for="inputEmail4" class="col-sm-4 control-label">Trip Type</label>
                              <div class="col-sm-8">
                                <select name="trip_type" id="trip_type" class="form-control" >
                                  <option value="">Select Option</option>
                                  <option value="pickup">Airport Pickup</option>
                                  <option value="dropoff">Airport Dropoff</option>
                                  <option value="incity">Incity</option>
                                  <option value="highway">Highway</option>

                                </select>
                              </div>
                        </div>
                        </div>
                        <div class="row">
                           <div class="form-group col-md-12"  id="daily_status_div">
                               
                                  <label for="inputEmail4" class="col-sm-4 control-label">Daily Status</label>
                                  <div class="col-sm-8">
                                    <select name="daily_status" id="daily_status" class="form-control" >
                                      <option value="">Select Option</option>
                                      <option value="full-day">Full-day</option>
                                      <option value="half-day">Half-day</option>

                                    </select>
                                  </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                              <label for="LastName" class="col-sm-4 control-label">Search Amount</label>
                              <div class="col-sm-8">
                                  <input type="text" id="search_amount" class="form-control" placeholder="Search Amount">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="row" style="margin-bottom: 5px;">
                          <div class="col-md-2">Order Date</div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="From Date" name="booking_date_from" id="booking_date_from" /></div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="To Date" name="booking_date_to" id="booking_date_to" /></div>
                      </div>
                      <div class="row" style="margin-bottom: 5px;">
                          <div class="col-md-2">Pickup Date</div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="From Date"  name="pickup_date_from" id="pickup_date_from" /></div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="To Date" name="pickup_date_to" id="pickup_date_to" /></div>
                      </div>
                      <div class="row" style="margin-bottom: 5px;">
                          <div class="col-md-2">Dropoff Date</div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="From Date" name="dropoff_date_from" id="dropoff_date_from" /></div>
                          <div class="col-md-4"><input type="text" class="form-control datepicker" placeholder="To Date" name="dropoff_date_to" id="dropoff_date_to" /></div>
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                    <label for="LastName" class="col-sm-1 control-label"></label>
                    <div class="col-sm-11">
                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                        <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
              </div>
          </form>
          <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Trip Record Name</th>
                    <th>Car No</th>
                    <?php //if($this->session->userdata('cr_user_role') == 'customer'){ ?>
                    <th>Supplier Name</th>
                    <th>Rental Type</th>
                    <?php //} else{ ?>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <?php //} ?>
                    <th>Pick Up Date/Time</th>
                    <th>Drop Off Date/Time</th>
                    <!-- <th>Drop Off Date/Time</th> -->
                    <th>Booking Amount</th>
                    <th>Order Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
            
        </table>
        </div>
      </div>
          <!-- /.box-body -->
         <!--  <div class="box-footer">
            <button type="submit" class="btn btn-info">Add User</button>
          </div> -->
          <!-- /.box-footer -->
         
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
 
var table;
 
$(document).ready(function() {
    
    $(document.body).on('click', '.delete_booking' ,function(){
        var conf =  confirm("Are you sure!! You want to delete this booking info");
        if(conf){ 
            var order_id = $(this).attr("order_id");
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url(CRM_VAR.'/order/delete')?>",
                data: {order_id:order_id},
                dataType: "JSON",
                success: function(resultData) {
                    alert(resultData.msg);
                    window.location.reload();
                }
            });
        }
    });
    
    function byRentType(rent_type) {
        if(rent_type == 36){
          $(".rent36").show();
          $("#trip_type_div").show();
          
        }else{
          $(".rent36").hide();
          $("#trip_type_div").hide();
          $("#trip_type").val("");
        }

      }
      function byTripType(trip_type){
        if(trip_type=="incity"){
          $("#daily_status_div").show();
        }else{
          $("#daily_status_div").hide();
          $("#daily_status").val("");
        }
      }

      var rent_type = $("#rent_type").val();
      byRentType(rent_type);
      $("#daily_status_div").hide();
      $("#rent_type").change(function(){
        byRentType($(this).val());
      });

      var trip_type = $("#trip_type").val();
      byTripType(trip_type);
      $("#trip_type").change(function(){
        byTripType($(this).val());
        
      });
 $('.datepicker').datepicker({
         autoclose: true
       });
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "searching": false,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url(CRM_VAR.'/order/ajax_order_list')?>",
            "type": "POST",
            "data": function (data) {		
                <?php if($this->session->userdata('cr_user_role') != 'supplier' && $this->session->userdata('cr_user_role') != 'agent' || $this->session->userdata('cr_user_role') == 'customer'){ ?>   
                    data.supplier_id = $('#supplier_id').val();
                <?php } ?>
                data.order_id = $('#order_id').val();
                data.user_id = $('#user_id').val();
                data.search_amount = $('#search_amount').val();
                data.driver = $('#driver').val();
                data.trip_type = $('#trip_type').val();
                data.daily_status = $('#daily_status').val();
                data.booking_date_from = $('#booking_date_from').val();
                data.booking_date_to = $('#booking_date_to').val();
                data.pickup_date_from = $('#pickup_date_from').val();
                data.pickup_date_to = $('#pickup_date_to').val();
                data.dropoff_date_from = $('#dropoff_date_from').val();
                data.dropoff_date_to = $('#dropoff_date_to').val();
                data.rent_type = $('#rent_type').val();
                data.status = $('#status').val();
                data.trip_expense_type = $('#trip_expense_type').val();
                
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,11 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload(null,false);  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    }); 
 
});
 
</script>