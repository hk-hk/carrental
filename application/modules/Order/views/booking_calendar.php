<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <link rel="stylesheet" href="<?php echo base_url("public/Calendar/");?>css/monthly.css">
	<style type="text/css">
		
		#mycalendar {
			width: 100%;
			margin: 2em auto 0 auto;
			max-width: 80em;
			border: 1px solid #666;
		}
                .manage-btn button {float: right; }
	</style>
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Booking Calendar</h3>
          <button style="float:right;" type="button" class="btn btn-primary" data-toggle="modal" id="add_booking_popup" data-whatever="@mdo">Add Booking</button>
          <?php if($this->session->userdata('cr_user_role')=='admin'){ ?>
          <button style="float:right;margin-right: 10px;" type="button" class="btn btn-primary" data-toggle="modal" id="change_supplier" data-whatever="@mdo">Change Supplier</button>
          <?php } ?>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
	    
        <div class="box-body">
            <div class="row">
                <form action="<?php echo site_url(CRM_VAR.'/booking-calendar')?>" id="active_car" name="active_car" method="post" >
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-3">
                        <label for="recipient-name" class="form-control-label"><?php echo (isset($suplier_name) && $suplier_name!='')?$suplier_name:''?> Cars:</label>
                        <select class="form-control" id="car_id" name="active_car"  onchange="return changeCar();">
                            <?php foreach ($supplier_cars as $k=>$val){ 
                                if($active_car==$k){?>
                            <option value="<?php echo $k;?>" selected="selected"><?php echo $val;?></option>
                                <?php }else{ ?>
                              <option value="<?php echo $k;?>"><?php echo $val;?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="monthly" id="mycalendar"></div>
                </div>
            </div>
        </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
</div>
  <!-- /.row -->
</section>


<div class="modal fade" id="add_booking" tabindex="-1" role="dialog" aria-labelledby="add_bookingLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      
    <div class="modal-content">
      <div class="modal-header">
        <button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h3>Manage Supplier Booking</h3>
      </div>
      <div class="modal-body">
            <div class="form-group"></div>
            <div class="form-group">
              <label for="car_id" class="form-control-label">Car:</label>
              <div id="car_name"></div>
              <input type="hidden" id="booking_id" value=""/>
            </div>
            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                          <label for="from_date" class="form-control-label">From Date:</label>
                          <input type="text" class="form-control datepicker req" id="from_date"  >
                      </div>
                </div>
                <div class="col-md-6">
                      <div class="input-group bootstrap-timepicker timepicker">
                          <label for="from_date" class="form-control-label">From Time:</label>
                          <input id="from_time" type="text" class="form-control input-small timepicker1 req" value="08:00 AM">
                      </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="to_date" class="form-control-label ">To Date:</label>
                        <input type="text" class="form-control datepicker req" id="to_date" required="true">
                    </div>
                </div>
                <div class="col-md-6">
                      <div class="input-group bootstrap-timepicker timepicker">
                          <label for="from_date" class="form-control-label">To Time:</label>
                          <input id="to_time" type="text" class="form-control input-small timepicker1 req" value="06:00 PM">
                      </div>
                </div>
            </div>
            
           
            
            <div class="form-group">
              <label for="booking_note" class="form-control-label">Message:</label>
              <textarea class="form-control" id="booking_note"></textarea>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-secondary" id="delete_booking" style="display:none;">Delete Booking</button>
        <button type="button" class="btn btn-primary" id="add_supplier_booking"></button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url("public/Calendar/");?>js/monthly.js"></script>
<script type="text/javascript">
function setupCalendar(){
    var car_id = $("#car_id").val();
    jQuery.ajax({
        type: 'POST',
        url: "<?php echo site_url(CRM_VAR.'/booking-calendar-ajax')?>",
        data: {car_id:car_id},
        dataType: "JSON",
        success: function(resultData) {
            jQuery('#mycalendar').monthly({
                mode: 'event',
                dataType: 'json',
                events: resultData
            });
        }
    });
}
function changeCar(){
    document.getElementById("active_car").submit();
}
function update_booking(id){
    $.ajax({
        type: 'POST',
        url: "<?php echo site_url(CRM_VAR.'/get-supplier-booking')?>",
        data: {booking_id:id},
        dataType: "JSON",
        success: function(resultData) {
            //console.log(resultData);
            //alert(resultData.id);
            $("#booking_id").val(resultData.id);
            $("#car_id").val(resultData.car_id);
            $("#from_date").val(resultData.from_date);
            $("#to_date").val(resultData.to_date);
            $("#booking_note").val(resultData.booking_note);
            $("#from_time").val(resultData.from_time);
            $("#to_time").val(resultData.to_time);
            $("#add_supplier_booking").text('Update Booking');
            $("#delete_booking").show();
            $('#add_booking').modal('show');
        }
    });
}
setupCalendar();

jQuery(function(){
    $("#car_name").html($("#car_id option:selected").text());
    $("#change_supplier").click(function(){
        window.location = "<?php echo site_url(CRM_VAR.'/suppliers')?>";
    });
    $("#delete_booking").click(function(){
        var conf =  confirm("Are you sure!! You want to delete this booking info");
        if(conf){ 
            var booking_id = $("#booking_id").val();
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url(CRM_VAR.'/delete-supplier-booking')?>",
                data: {booking_id:booking_id},
                dataType: "JSON",
                success: function(resultData) {
                    alert(resultData.msg);
                    window.location.reload();
                }
            });
        }
    });
    $("#add_booking_popup").click(function(){
        
        $("#add_supplier_booking").text('Add Booking');
        $("#delete_booking").hide();
        $("#booking_id").val('');
        $("#from_date").val('');
        $("#to_date").val('');
        $("#from_time").val('08:00 AM');
        $("#to_time").val('06:00 PM');
        $("#booking_note").val('');
        $('#add_booking').modal('show');
    });
    $("#add_supplier_booking").click(function(){
        var car_id = $("#car_id").val();
        var booking_id = $("#booking_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        var from_time = $("#from_time").val();
        var to_time = $("#to_time").val();
        var from_date1 = new Date(from_date);
        var to_date1 = new Date(to_date);
        var booking_note = $("#booking_note").val();
        if(from_date=='' || to_date=='' || from_time=='' || to_time=='' || booking_note==''){
            alert("All fields are required, So please fill");
        }else if(from_date1>to_date1){
            alert("From date must be less then To date");
        }else{
            
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url(CRM_VAR.'/add-supplier-booking')?>",
                data: {booking_id:booking_id,car_id:car_id,from_date:from_date,to_date:to_date,booking_note:booking_note,from_time:from_time,to_time:to_time},
                dataType: "JSON",
                success: function(resultData) {
                    alert(resultData.msg);
                    if(resultData.status){
                        $("#car_id").val('');
                        $("#booking_id").val('');
                        $("#from_date").val('');
                        $("#to_date").val('');
                        $("#from_time").val('');
                        $("#to_time").val('');
                        $("#booking_note").val('');
                        window.location.reload();
                    }
                }
            });
        }
    });
    $('.datepicker').datepicker({
       autoclose: true
     });
    $('#timepicker').timepicker('setTime', '12:45 AM');
    jQuery(".listed-event").on("click",function(){
       
    });
    $('.timepicker1').timepicker();
    $("[title]").removeAttr("title");
});    
</script>
<style>
 .datepicker.dropdown-menu { z-index:9999 !important;  }
 </style>
