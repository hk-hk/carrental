<link rel="stylesheet" href="<?php echo CRM_VAR;?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR;?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
  <style>
   .control-label{
    text-align: left !important;
    text-decoration: none;
  }
  .values{
    font-weight: initial !important;
  }
  .fa .fa-star {
        color: #ff0000 !important;
    }
  </style>
<!-- Cancellation Pop-up -->
<div id="cancellation" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Resason for cancellation first</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/cancle/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputEmail4" class="control-label">Reason For Cancellation : </label>  
              </div>
              <div class="form-group">
                <textarea class="form-control" name="cancleReason" rows="3" placeholder="reason for cancellation..." required="required"></textarea>  
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Cancellation">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> 
<!-- Cancellation Pop-up -->
<!-- rating Pop-up -->
<div id="rating" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Give Feedback </h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/submit_feedback/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Rating : </label>  
              </div>
              <div class="form-group">
                <!-- 1<input type="radio" name="rating" value="1" required="required">
                2<input type="radio" name="rating" value="2" required="required">
                3<input type="radio" name="rating" value="3" required="required">
                4<input type="radio" name="rating" value="4" required="required">
                5<input type="radio" name="rating" value="5" required="required"> -->

                <input type="hidden" name="rating" id="review" required="required">
                <div class="cmt-lower">
                    <i id="star_1" class="fa fa-star-o starrattings"></i>
                    <i id="star_2" class="fa fa-star-o starrattings"></i>
                    <i id="star_3" class="fa fa-star-o starrattings"></i>
                    <i id="star_4" class="fa fa-star-o starrattings"></i>
                    <i id="star_5" class="fa fa-star-o starrattings"></i>
                </div>
                <script> 
                jQuery( document ).ready(function() {
                jQuery(".starrattings").on("mouseover",function(){
                        var startt=jQuery(this).attr("id");
                        var starid=startt.split("_"); 
                        starid=starid[1];
                        
                        for(var i=1;i<=5;i++){
                            if(jQuery("#star_"+i).attr("class")=='fa fa-star starrattings'){
                                jQuery("#star_"+i).removeClass("fa fa-star starrattings");
                                jQuery("#star_"+i).addClass("fa fa-star-o starrattings");
                            }
                        }
                           // alert(starid);
                        for(var i=1;i<=starid;i++){
                                jQuery("#star_"+i).removeClass("fa fa-star-o starrattings");
                                jQuery("#star_"+i).addClass("fa fa-star starrattings");
                        }
                        jQuery("#review").val(starid);
                    });
                });
                </script>  
              </div>
              <div class="form-group">
                <label>Comment : </label>  
              </div>
              <div class="form-group">
                <input type="hidden" name="order_id" required="required" value="<?php echo $orderinfo->order_id; ?>">
                <textarea class="form-control" name="comment" rows="3" placeholder="Comment..." required="required"></textarea>  
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Feedback">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> 
<!-- rating Pop-up -->
<section class="content">
   <?php include("order_detail.php"); ?>
   <!-- /.row -->
   <div class="row">
      <div class="col-md-12">
        <div class="box box-warning collapsed-box">   <!--  -->
          <div class="box-header with-border">
            <h3 class="box-title">Order Summary</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            
            <div class="row">
              <div class="col-md-4">
                <h2>Summary of charges</h2>
                <div class="charges-summery">
                    <?php //echo "<pre>";print_r($trackPrices);echo "</pre>";?>
                    <ul>
                        <?php 
                        foreach ($trackPrices as $k=>$item){ 
                                if(!isset($item->title)){ 
                                    foreach ($item as $it){ 
                                        ?> 
                                        <li>
                                            <span class="text-left"><?php echo $it->title;?></span>
                                            <span class="text-right"><?php echo ($it->amount);?></span>
                                        </li>
                                    <?php } ?>
                                <?php }else{  ?>
                                    <li>
                                        <span class="text-left"><?php echo $item->title;?></span>
                                        <span class="text-right"><?php echo ($item->amount);?></span>
                                    </li>
                                <?php }
                            }?>
                        
                    </ul>
                </div>
              </div>
              <div class="col-md-4">
                <h2>Pickup</h2>
                  <?php 
                  echo date("D,M d,Y",strtotime($searchData->pickup_date))."<br>";
                  echo $searchData->pickup_time."<br>";
                  echo $this->orders->get_city_name($order_historyInfo->car_city)."<br>";
                  
                  ?>    
              </div>
              <?php 
                  $order_history1 = json_decode($orderinfo->order_history,true); 
                  $datewiseDestinations = $order_history1['datewiseDestinations'];
                  $multiDatesWithCity = $order_history1['datewise'];
              if((count($datewiseDestinations)>0 || (is_array($multiDatesWithCity) && count($multiDatesWithCity)>0)) && FALSE){?>
              <div class="col-md-4">
                <h2>Booking Summary </h2>
                 <?php 
                  $searchData1 = $order_history1['searchData'];
                  $currency = $order_history1['currency'];
                  $airport = isset($searchData1['airport'])?$searchData1['airport']:'';
                  $titles = isset($datewiseDestinations['titles'])?$datewiseDestinations['titles']:array(); 
                  //print_r($multiDatesWithCity);die;
                  ?>  
                <?php if(is_array($multiDatesWithCity) && count($multiDatesWithCity)>1){ ?>
                <table class="each-day-hourly">
                <?php foreach ($multiDatesWithCity as $date=>$val){?>
                    <tr class="date-heading">
                        <th><?php echo date("D,M d,Y",strtotime($date));?></th>
                    </tr>
                    <tr>
                        <th><?php echo ('From Time')?>: </th>
                        <th><?php echo ('To Time')?>: </th>
                        <th><?php echo ('Hours')?>: </th>
                    </tr>
                    <tr>
                        <td><?php echo $val['from'];?></td>
                        <td><?php echo $val['to'];?></td>
                        <td><?php echo $val['duration'];?></td>
                    </tr>
                <?php } ?>
                </table>
                <?php }else if(isset($datewiseDestinations)){ 
                        $titles = isset($datewiseDestinations['titles'])?$datewiseDestinations['titles']:array(); 
                        unset($datewiseDestinations['titles']);
                        unset($datewiseDestinations['total_distance']);
                        ?>
                     <table class="each-day-hourly">
                <?php foreach ($datewiseDestinations as $date=>$val){?>
                    <tr class="date-heading">
                        <th colspan="3"><?php echo date("D,M d,Y",strtotime($date));?></th>
                    </tr>
                    <tr>
                        <th><?php echo ('From')?>: </th>
                        <th><?php echo ('To')?>: </th>
                        <th><?php echo ('Distance')?>:</th>
                    </tr>
                    <tr>
                        <td><?php echo (isset($titles[$val['from']])?$titles[$val['from']]:'none');?></td>
                        <td><?php echo (isset($titles[$val['to']])?$titles[$val['to']]:'none');?></td>
                        <td><?php echo $val['distance'];?></td>
                    </tr>
                <?php } ?>
                </table>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <?php if($orderinfo->status == 'Completed'){ ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning collapsed-box">   <!--  -->
          <div class="box-header with-border">
            <h3 class="box-title">Feedbacks</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <?php if(!$is_autorize_for_rating && $this->session->userdata("cr_user_role") != 'admin'){ ?>
            <button data-toggle="modal" data-target="#rating" class="btn btn-warning" >Give Feedback</button>
            <?php } ?>
            <?php 
            if(!empty($order_ratings)){
            ?>
            <div class="row">
              <?php 
              if($this->session->userdata("cr_user_role") == 'admin'){
                foreach ($order_ratings as $key => $value) {
                  ?>
                  <div class="col-md-6">
                    <h3><?php echo $value->rating_by; ?> Feedback</h3>
                     <div class="cmt-lower">
                                 Rating  :
                        <?php 
                        for ($i=1;$i<=5;$i++) {
                          if($i<=$value->rating){
                            echo '<i id="star_'.$i.'" class="fa fa-star starrattings"></i>';
                          }
                          else{
                            echo '<i id="star_'.$i.'" class="fa fa-star-o starrattings"></i>';
                          }
                        }
                        ?>
                              </div>

                    <!-- <?php echo $value->rating;?><br> -->
                    Comment  : <?php echo $value->comment;?><br>
                  </div>
                  <?php 
                }
              }
              if($this->session->userdata("cr_user_role") == 'agent' || $this->session->userdata("cr_user_role") == 'customer'){
                foreach ($order_ratings as $key => $value) {
                  if($value->rating_by == 'Customer'){
                  ?>
                  <div class="col-md-6">
                    <h3><?php if($value->user_id == $this->session->userdata("crm_user_id")){echo "My";}else{echo $value->rating_by;} ?> Feedback</h3>
                     <div class="cmt-lower">
                               Rating  : 
                        <?php 
                        for ($i=1;$i<=5;$i++) {
                          if($i<=$value->rating){
                            echo '<i id="star_'.$i.'" class="fa fa-star starrattings"></i>';
                          }
                          else{
                            echo '<i id="star_'.$i.'" class="fa fa-star-o starrattings"></i>';
                          }
                        }
                        ?>
                            </div><!-- <?php echo $value->rating;?><br> -->
                    Comment  : <?php echo $value->comment;?><br>
                  </div>
                  <?php 
                  }
                  else{
                    if($is_autorize_for_rating){ ?>
                    <div class="col-md-6">
                      <h3><?php if($value->user_id == $this->session->userdata("crm_user_id")){echo "My";}else{echo $value->rating_by;} ?> Feedback</h3>
                      
                      <div class="cmt-lower">
                          Rating  :
                        <?php 
                        for ($i=1;$i<=5;$i++) {
                          if($i<=$value->rating){
                            echo '<i id="star_'.$i.'" class="fa fa-star starrattings"></i>';
                          }
                          else{
                            echo '<i id="star_'.$i.'" class="fa fa-star-o starrattings"></i>';
                          }
                        }
                        ?>
                      </div>
                      <!-- <?php echo $value->rating;?><br> -->
                      Comment  : <?php echo $value->comment;?><br>
                    </div>
                    <?php }
                    else{
                      ?><div class="col-md-6"><?php echo $value->rating_by." has given feedback. You can see after submitting your feedback."; ?></div><?php 
                    }
                  }
                }
              }
              if($this->session->userdata("cr_user_role") == 'supplier'){
                foreach ($order_ratings as $key => $value) {
                  if($value->rating_by == 'Supplier'){
                  ?>
                  <div class="col-md-6">
                    <h3><?php if($value->user_id == $this->session->userdata("crm_user_id")){echo "My";}else{echo $value->rating_by;} ?> Feedback</h3>
                    
                    <div class="cmt-lower">
                         Rating  :
                        <?php 
                        for ($i=1;$i<=5;$i++) {
                          if($i<=$value->rating){
                            echo '<i id="star_'.$i.'" class="fa fa-star starrattings"></i>';
                          }
                          else{
                            echo '<i id="star_'.$i.'" class="fa fa-star-o starrattings"></i>';
                          }
                        }
                        ?>
                      </div>
                    <!-- <?php echo $value->rating;?><br> -->
                    Comment  : <?php echo $value->comment;?><br>
                  </div>
                  <?php 
                  }
                  else{
                    if($is_autorize_for_rating){ ?>
                    <div class="col-md-6">
                      <h3><?php if($value->user_id == $this->session->userdata("crm_user_id")){echo "My";}else{echo $value->rating_by;} ?> Feedback</h3>
                      
                      <div class="cmt-lower">
                        Rating  :
                        <?php 
                        for ($i=1;$i<=5;$i++) {
                          if($i<=$value->rating){
                            echo '<i id="star_'.$i.'" class="fa fa-star starrattings"></i>';
                          }
                          else{
                            echo '<i id="star_'.$i.'" class="fa fa-star-o starrattings"></i>';
                          }
                        }
                        ?>

                      </div>
                      <!-- <?php echo $value->rating;?><br> -->
                      Comment  : <?php echo $value->comment;?><br>
                    </div>
                    <?php }
                    else{
                      ?><div class="col-md-6"><?php echo $value->rating_by." has given feedback. You can see after submitting your feedback."; ?></div><?php
                    }
                  }
                }
              }
              ?>

            </div>
            <?php 
            }
            ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <?php } ?>
</section>


<script type="text/javascript">
  $(".confirmed_sendmail").click(function(){
      var res = confirm('Are you sure ?');
      if(res){
        var to = $(this).data()['to'];
        var user_id = $("#user_id").val();
        var supplier_id = $("#supplier_id").val();
        var order_id = $("#order_id").val();
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url(CRM_VAR.'/Order/send_confirmation_mail')?>',
          data: {to:to,user_id:user_id,supplier_id:supplier_id,order_id:order_id},
          dataType: "text",
          success: function(resultData) { 
            alert(resultData);
          }
        });
      }
    });
</script>