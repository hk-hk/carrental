<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
  <style>
   .control-label{
    text-align: left !important;
    text-decoration: none;
  }
  .values{
    font-weight: initial !important;
  }
  .fa .fa-star {
        color: #ff0000 !important;
    }
  </style>

<!-- rating Pop-up -->
<section class="content">

   <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning ">   <!--  -->
          <div class="box-header with-border">
            <h3 class="box-title">Vendor Payment</h3>

            <div class="box-tools pull-right">
              <a href="<?php echo site_url(); ?>admin/order/view/<?php echo $orderinfo->order_id; ?>" class="btn btn-default"> Exit

          </a>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <form action="<?php echo site_url(CRM_VAR . '/order/store_expenses/' . $orderinfo->order_id . '/' . $expense_id) ?>" class="form-horizontal" method="post">
            <input type="hidden" name="order_id" value="<?=$orderinfo->order_id?>">
           <div class="box-body" id="vendor_infos">
                  <!-- <h3 class="box-title">Filter</h3> -->
                   <!--  -->


                    <div class="row">
                      <?php foreach ($users as $k => $user): ?>
                      <?php if ($orderinfo->supplier_id == $user->id): ?>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="order_amount" class="col-md-4 control-label">Order Amount</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control number-format" id="order_amount" name="order_amount" value="<?=$orderinfo->amount?>" readonly="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="FirstName" class="col-md-4 control-label">Vendor</label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="vendor_name" value="<?=$user->user_fname . ' ' . $user->user_lname?>" value="" readonly="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="vendor_fees" class="col-md-4 control-label">Vendor Fees</label>
                          <div class="col-md-8">

                            <!-- <input type="text" class="form-control number-format" id="vendor_fees" name="vendor_fees" min="0" value="<?=isset($trip_expense_detail->vendor_fees) ? $trip_expense_detail->vendor_fees : '0'?>" step="1000" onchange="vtotalexpense()"> -->
                            <input type="text" class="form-control number-format" id="vendor_fees" name="vendor_fees" min="0" value="<?= $orderinfo->vendor_fees ?>" onchange="vtotalexpense()" required>
                            <!-- <span><?=$user->commission_rate?>% to Sonic Star and <?=100 - $user->commission_rate?>% to <?=$user->user_fname . ' ' . $user->user_lname?></span> -->
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="vendor_paydate" class="col-md-4 control-label">Vendor Paydate</label>
                          <div class="col-md-8">
                            <input type="input" class="form-control datepicker" id="vendor_paydate" value="<?= date('m/d/Y')?>" name="vendor_paydate" required="">
                          </div>
                        </div>
                        <input type="hidden" class="form-control number-format" name="redirect" value="vendor_fees" readonly="">
                        <input type="hidden" class="form-control number-format" id="vendor_profit" name="vendor_profit" value="0" readonly="">
                        <input type="hidden" class="form-control" id="pay_to" name="pay_to" value="vendor" readonly="">
                        <button type="submit" class="btn btn-info pull-right" id="btn-pay" onclick="return confirm('Do you want to <?=$btn?>?')"><?=$btn?></button>
                        <!-- <div class="form-group">
                          <label for="vendor_profit" class="col-md-4 control-label">Sonic Star Profit</label>
                          <div class="col-md-8">
                          </div>
                        </div> -->

                        <!-- <div class="form-group">
                          <label for="status_vendor" class="col-md-4 control-label">Status</label>
                          <div class="col-md-8">
                            <select name="status_vendor" class="form-control">
                              <option value="pending">Pending</option>
                              <option value="complete">Complete</option>
                            </select>

                          </div>
                        </div> -->
                      </div>
                        <?php endif?>
                      <?php endforeach?>
                      <div class="col-md-6">
                      <table class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Vendor Paydate</th>
                  <th>Vendor Fees</th>
                  <!-- <th>Profit</th> -->
                </tr>
              </thead>
              <tbody>
                <?php $total = 0;?>
                <?php foreach ($expense_list as $key => $value): ?>
                  <?php if ($value->pay_to=="vendor"  ): ?>
                  <?php $total += $value->vendor_fees?>
                  <tr>
                    <td><?=$key + 1?></td>
                    <td><?=$value->vendor_paydate?></td>
                    <td align="right"><?= number_format($value->vendor_fees,2)?></td>
                    <!-- <td><?=$value->profit?></td> -->
                  </tr>
                  <?php endif?>
                <?php endforeach?>
                <tr>
                    <td>Total Amount</td>
                    <td colspan="2" align="right"><?=number_format($total,2)?></td>
                    <!-- <td><?=$value->profit?></td> -->
                  </tr>
              </tbody>
            </table>
                    </div>
                    </div>

                </div>

              </form>
          <!-- /.box-body -->
          <?php if (isset($trip_expense_detail->vendor_paydate)): ?>
            
          <div class="box-footer">
            <form action="<?php echo site_url(CRM_VAR . '/order/payment_complete/' . $orderinfo->order_id) ?>" method="post" accept-charset="utf-8">
                <input type="hidden" name="payment_complete" value="complete">
                <input type="hidden" name="pay_to" value="vendor">
                <input type="hidden" name="expense_id" value="<?=$trip_expense_detail->id?>">

                <button type="submit" class="btn btn-info pull-right" onclick="return confirm('Do you want to <?=$btn?>?')">Set Vendor Payment as Complete Status</button>
              </form>
          </div>
          <?php endif ?>
        </div>
        <!-- /.box -->
      </div>
    </div>
</section>


<script type="text/javascript">
  $('.datepicker').datepicker({
        autoclose: true
    });
  $(".confirmed_sendmail").click(function(){
      var res = confirm('Are you sure ?');
      if(res){
        var to = $(this).data()['to'];
        var user_id = $("#user_id").val();
        var supplier_id = $("#supplier_id").val();
        var order_id = $("#order_id").val();
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url(CRM_VAR . '/Order/send_confirmation_mail') ?>',
          data: {to:to,user_id:user_id,supplier_id:supplier_id,order_id:order_id},
          dataType: "text",
          success: function(resultData) {
            alert(resultData);
          }
        });
      }
    });




  function vtotalexpense(){
    var vendor_fees = parseInt($("#vendor_fees").val());
    var total_amount = parseInt($("#order_amount").val());
    var profit= total_amount-vendor_fees ;
    $('#vendor_profit').val(profit);
  }
  vtotalexpense();
  $('#own_expense').hide();
  $('#detail').hide();
  $('#vendor_fees').attr('required',true);
  $('#vendor_paydate').attr('required',true);
  $('#btn-pay').html("Pay to vendor");
  <?php if ($orderinfo->vendor_payment_status == "complete"): ?>
    $('#vendor_infos input').attr('disabled',true);
    $('#vendor_infos textarea').attr('disabled',true);
    $('#btn-pay').hide();
    $('.box-footer').hide();
    // $('#vendor_infos input').attr('disabled',true);
  <?php endif?>

</script>