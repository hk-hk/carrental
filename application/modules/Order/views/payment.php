<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
<style>
.control-label{
text-align: left !important;
text-decoration: none;
}
.values{
font-weight: initial !important;
}
.fa .fa-star {
color: #ff0000 !important;
}
</style>
<!-- Cancellation Pop-up -->
<div id="cancellation" class="modal fade" role="dialog">
  <div class="modal-dialog">f
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Resason for cancellation first</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/cancle/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="inputEmail4" class="control-label">Reason For Cancellation : </label>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="cancleReason" rows="3" placeholder="reason for cancellation..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Cancellation">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Cancellation Pop-up -->
<!-- rating Pop-up -->
<div id="rating" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Give Feedback </h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo site_url(); ?>admin/order/submit_feedback/<?php echo $orderinfo->order_id; ?>" method="post">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Rating : </label>
              </div>
              <div class="form-group">
                <!-- 1<input type="radio" name="rating" value="1" required="required">
                2<input type="radio" name="rating" value="2" required="required">
                3<input type="radio" name="rating" value="3" required="required">
                4<input type="radio" name="rating" value="4" required="required">
                5<input type="radio" name="rating" value="5" required="required"> -->
                <input type="hidden" name="rating" id="review" required="required">
                <div class="cmt-lower">
                  <i id="star_1" class="fa fa-star-o starrattings"></i>
                  <i id="star_2" class="fa fa-star-o starrattings"></i>
                  <i id="star_3" class="fa fa-star-o starrattings"></i>
                  <i id="star_4" class="fa fa-star-o starrattings"></i>
                  <i id="star_5" class="fa fa-star-o starrattings"></i>
                </div>
                <script>
                jQuery( document ).ready(function() {
                jQuery(".starrattings").on("mouseover",function(){
                var startt=jQuery(this).attr("id");
                var starid=startt.split("_");
                starid=starid[1];
                for(var i=1;i<=5;i++){
                if(jQuery("#star_"+i).attr("class")=='fa fa-star starrattings'){
                jQuery("#star_"+i).removeClass("fa fa-star starrattings");
                jQuery("#star_"+i).addClass("fa fa-star-o starrattings");
                }
                }
                // alert(starid);
                for(var i=1;i<=starid;i++){
                jQuery("#star_"+i).removeClass("fa fa-star-o starrattings");
                jQuery("#star_"+i).addClass("fa fa-star starrattings");
                }
                jQuery("#review").val(starid);
                });
                });
                </script>
              </div>
              <div class="form-group">
                <label>Comment : </label>
              </div>
              <div class="form-group">
                <input type="hidden" name="order_id" required="required" value="<?php echo $orderinfo->order_id; ?>">
                <textarea class="form-control" name="comment" rows="3" placeholder="Comment..." required="required"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info" name="submit" value="Submit Feedback">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- rating Pop-up -->
<section class="content">

  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-info">
        <div class="box-header with-border" align="center">
          <h3 class="box-title">Order Payment Form</h3>
        </div>
        <div class="box-body">
          <?php $order_history = json_decode($orderinfo->order_history);
          $trackPrices                       = $order_history->trackPrices;
          $searchData                        = $order_history->searchData;
          $currency                          = $order_history->currency;
          ?>
          <?php
          //$currency = isset($orderinfo->order_history->currency)?$orderinfo->order_history->currency:'mmk';
          if ($is_edit_action) {?>
          <div class="row">
            <!-- <div class="col-md-8"></div> -->
            <div class="col-md-12" align="right">
              <a href="<?php echo site_url(); ?>admin/order/view/<?php echo $orderinfo->order_id; ?>" class="btn btn-default" name="save" >Exit</a>
            </div>
          </div>
          <?php }?>
          <form id="addarticle-form" class="form-horizontal"  style="text-align: left !important;">
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Order ID</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->order_id; ?></label>
                      <input type="hidden" name="order_id" id="order_id" value="<?php echo $orderinfo->order_id; ?>">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Customer Name</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->name; ?></label>
                      <input type="hidden" name="user_id" id="user_id" value="<?php echo $orderinfo->user_id; ?>">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Customer Phone</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->phone; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Customer Email</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->usr_email; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Supplier Name</label>
                      <label class="col-sm-6 control-label values"><?php foreach ($users as $k => $user) {
                        ?>
                        <?php if ($orderinfo->supplier_id == $user->id) {echo $user->user_fname . " " . $user->user_lname;}?>
                        <?php
                      }?></label>
                      <input type="hidden" name="supplier_id" id="supplier_id" value="<?php echo $orderinfo->supplier_id; ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Pick Up Place</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->pl_title; ?></label>
                    </div>
                  </div>
                  <?php
                  if ($orderinfo->order_history != null || $orderinfo->order_history != '') {
                  $order_history     = json_decode($orderinfo->order_history)->searchData;
                  $order_historyInfo = json_decode($orderinfo->order_history);
                  if ($orderinfo->type_of_rent == 36 && $order_history->incity == 'highway') {?>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Drop off Place</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->dl_title; ?></label>
                    </div>
                  </div>
                  <?php }}?>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Order Date</label>
                      <label class="col-sm-6 control-label values"><?php echo date('d.m.Y (D)', strtotime($orderinfo->booking_date)); ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Payment Type</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->payment_method; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Total Amount</label>
                      <label class="col-sm-6 control-label values"><?php echo $currency . $orderinfo->amount; ?></label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputEmail4" class="col-sm-6 control-label">Order Status</label>
                      <label class="col-sm-6 control-label values"><?php echo $orderinfo->status; ?> </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>
          </form>
        </div>
        <?php if ($is_edit_action && $orderinfo->status == 'Confirmed') {?>
        <div class="box box-footer" align="right">
          <?php if ($orderinfo->supplier_confirmed == 1) {?><button data-to="supplier" type="button" class="btn btn-info confirmed_sendmail">Send Confirm Email to Supplier</button><?php }?>
          <?php if ($orderinfo->customer_confirmed == 1) {?><button data-to="customer" type="button" class="btn btn-info confirmed_sendmail">Send Confirm Email to Customer</button><?php }?>
        </div>
        <?php }?>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
  </div>
  <!-- /.row -->
  <div class="row">
      <div class="col-md-12">
        <div class="box box-warning ">   <!--  -->
          <div class="box-header with-border">
            <h3 class="box-title">Payments History</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Payment Date</th>
                  <th>Received Account</th>
                  <th>Transfer Type</th>
                  <th>Payment Amount (MMK)</th>
                  <th>Payment Amount (USD)</th>
                  <th>Exchange Rate</th>
                  <th>Paid Amount (MMK)</th>
                  <th>Created At</th>
                  <th>Created By</th>
                  <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody>
                <?php foreach ($paid_list as $key => $value): ?>
                  <tr>
                    <td><?= $key+1 ?></td>
                    <td><?= date('m/d/Y',$value->date) ?></td>
                    <td><?= $value->account_name ?></td>
                    <td>
                      <?php if ($value->trasfer == 1): ?>
                        MMK
                      <?php elseif($value->trasfer == 2): ?>  
                        USD
                      <?php else: ?>
                        Both
                      <?php endif ?>
                    </td>
                    <td><?= $value->pay_price ?></td>
                    <td><?= $value->pay_dollar ?></td>
                    <td><?= $value->one_dollar ?></td>
                    <td><?= $value->total_price ?></td>
                    <td><?= date('m/d/Y',$value->created_at) ?></td>
                    <td><?= $value->user_lname ?>(<?= $value->user_role ?>)</td>
                    <!-- <td><a href="" class="btn btn-primary" title="">Edit</a></td> -->
                  </tr>
                <?php endforeach ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Payment Date</th>
                  <th>Received Account</th>
                  <th>Transfer Type</th>
                  <th>Payment Amount (MMK)</th>
                  <th>Payment Amount (USD)</th>
                  <th>Exchange Rate</th>
                  <th>Paid Amount</th>
                  <th>Created At</th>
                  <th>Created By</th>
                  <!-- <th>Action</th> -->
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning ">   <!--  -->
      <div class="box-header with-border">
        <h3 class="box-title">Payment Transfer</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <form action="<?php echo site_url(CRM_VAR . '/order/store_payment/' . $orderinfo->order_id) ?>" class="form-horizontal" method="post" id="payment_form">
        <input type="hidden" name="order_id" value="<?=$orderinfo->order_id?>">
        <div class="box-body">
          <!-- <h3 class="box-title">Filter</h3> -->
          <!--  -->
          <div id="own_expense">
            <div class="row">
              <div class="col-md-6">
                <!-- <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" placeholder="Choose Date" name="maintain_date" id="maintain_date" value="<?=!empty($maintain_detail->date) ? $maintain_detail->date : ''?>" required="" />
                  </div>
                </div> -->
                <input type="hidden" name="customer_name" value="<?php echo $orderinfo->name; ?>">
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Accounts</label>
                  <div class="col-md-8">
                    <select class="form-control" name="account" id="accountinfo">
                      <?php foreach ($accounts as $key => $value): ?>
                        <?php if ($value->account_id==35): ?>
                          <option value="<?= $value->account_id ?>" selected><?= $value->account_name ?></option>
                        <?php else: ?>
                          <option value="<?= $value->account_id ?>"><?= $value->account_name ?></option>
                        <?php endif ?>
                      <?php endforeach?>
                    </select>
                  </div>
                </div>
                <div class="form-group" style="display: none">
                  <label for="transfer_type" class="col-md-4 control-label">Transfer Type</label>
                  <div class="col-md-8">
                    <input type="hidden" name="transfer_type" id="tt">
                    <select class="form-control" id="transfer_type" >
                      <option value="1">MMK</option>
                      <option value="0">Both</option>
                      <option value="2">USD</option>
                    </select>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="pm_mmk" class="col-md-4 control-label">Payment Amount (MMK)</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="pm_mmk" name="pm_mmk" min="0" value="0" onclick="calcDiff()" onkeyup="calcDiff()">
                  </div>
                </div>
                <div class="form-group">
                  <label for="pm_usd" class="col-md-4 control-label">Payment Amount (USD)</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="pm_usd" name="pm_usd" min="0" value="0" onclick="calcDiff()" onkeyup="calcDiff()">
                  </div>
                </div>
                <div class="form-group">
                  <label for="exchange_rate" class="col-md-4 control-label">Exchange Rate (USD)</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="exchange_rate" name="exchange_rate" min="0" value="0" onclick="calcDiff()" onkeyup="calcDiff()">
                  </div>
                </div>
                <div class="form-group">
                  <label for="exchange_amount" class="col-md-4 control-label">Exchanged Amount</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="exchange_amount" name="exchange_amount" min="0" value="0" readonly="">
                  </div>
                </div>
                
                <!--  <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Status</label>
                  <div class="col-md-8">
                    <input type="radio" name="status" value="0" id="unpaid" checked=""> <label for="unpaid">Unpaid</label>
                    <input type="radio" name="status" value="1" id="paid"> <label for="paid">Paid</label>
                  </div>
                </div> -->
              </div>
              <div class="col-md-6">
               <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Payment Date</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control datepicker" name="payment_date" value="<?= date('m/d/Y') ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Paid Amount</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="paid_amount" readonly="" value="<?= $paid_total_amount ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="FirstName" class="col-md-4 control-label">Difference</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control number-format" id="difference" name="diff" readonly="" value="<?= ($orderinfo->amount+($orderinfo->ot_hours*$orderinfo->driver_ot_fees)) - $paid_total_amount ?>">
                    <span style="color:orange" id="diff_msg">Do you want complete the order with this difference amount?</span>
                    
                    <!-- <span style="color:red">Hello</span> -->
                  </div>
                </div>
                
              </div>
              
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="">
            <button type="submit" class="btn btn-info pull-left" id="btntransfer" onclick="return confirm('Please Check Again! Are you sure to submit?')">Transfer</button>
            
            <button type="submit" id="complete_order" name="complete_order" value="yes" class="btn btn-info pull-right" onclick="return confirm('Please Check Again! Are you sure to COMPLETE?')">Set Customer Payment As Complete</button>
          </div>
        </div>
      </form>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
</section>
<script type="text/javascript">
   $('#diff_msg').hide();
   $('#complete_order').show();
   var totalAmount=<?= $orderinfo->amount+($orderinfo->ot_hours*$orderinfo->driver_ot_fees) ?>;
  function payCondition(acc_type){
    $('#pm_mmk').val("0");
    $('#pm_usd').val("0");
    $('#exchange_rate').val("0");
    
    if(acc_type==0){
      $('#pm_mmk').attr('readonly',false);
      $('#pm_usd').attr('readonly',false);
      $('#exchange_rate').attr('readonly',false);
    }else if(acc_type==1){
      $('#pm_mmk').attr('readonly',false);
      $('#pm_usd').attr('readonly',true);
      $('#exchange_rate').attr('readonly',true);
    }else if(acc_type==2){
      $('#pm_mmk').attr('readonly',true);
      $('#pm_usd').attr('readonly',false);
      $('#exchange_rate').attr('readonly',false);
    }
     $('#tt').val(acc_type);
  }
  $('#transfer_type').change(function(){
      // console.log(this.val());
    $("#transfer_type option:selected" ).each(function() {
      // console.log("");
      payCondition($(this).val());
    });
  });

  function accinfo(id=null){
    if(id==null){
      id=$("#accountinfo option:selected" ).val();

    // console.log("hi");
    }
    $.post('<?= site_url(CRM_VAR.'/accountinfo/') ?>'+id,function(data){
        var obj=$.parseJSON(data);
        if(obj['account_id']==10){
          $('#transfer_type').attr('disabled',false);
        }
        payCondition(obj['account_type']);
      });
  }
  $('#accountinfo').change(function(){
      // console.log(this.val());
    $('#transfer_type').attr('disabled',true);
    $("#accountinfo option:selected" ).each(function() {
      accinfo($(this).val());
      // payCondition($(this).val());
    });
  });
  accinfo();
  function calcDiff(){
    var mmk = parseInt($('#pm_mmk').val());
    var usd = parseInt($('#pm_usd').val()) * parseInt($('#exchange_rate').val());

    var paidTotal= parseInt($('#paid_amount').val()) + mmk + usd;
    var diff=totalAmount - paidTotal;
    if(diff<0){
      diff="+"+(Math.abs(diff));
      // diff=diff
    }
    if(diff>0){
      diff="-"+Math.abs(diff);
    }
    $('#difference').val(diff);
    $('#exchange_amount').val(usd);

    if((totalAmount - paidTotal) <= <?= (int)$diff->short_desc ?>){
      // alert("Hello");
      $('#diff_msg').show();
      
    }else{
      $('#diff_msg').hide();
      $('#complete_order').show();
    }
  }

  calcDiff();
  <?php if ($orderinfo->customer_payment_status=="complete"): ?>
      $("#payment_form :input").attr("disabled", true);
      $('#btntransfer').hide();
      $('.box-footer').hide();
      $('#diff_msg').hide();
      $('#complete_order').show();
  <?php endif ?>
</script>
<?php if (true): ?>
<script type="text/javascript">
$(document).ready(function() {
$('.datepicker').datepicker({
autoclose: true
});
});
</script>
<?php endif?>