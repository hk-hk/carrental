<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="<?php echo CRM_VAR; ?>/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="<?php echo site_url('public/plugins/choosen/chosen.min.css'); ?>">
  <style>
   .error{color :#DD4B39 ;}
  </style>
<?php
$vendor = false;
$agent  = false;
?>
<section class="content">
   <div class="row">
      <!-- left column -->
      <div class="col-md-12">
         <!-- general form elements -->
         <div class="box box-info">
            <div class="box-header with-border">
               <h3 class="box-title">Manage Order</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
$currency   = isset($orderinfo->order_history->currency) ? $orderinfo->order_history->currency : 'mmk';
$attributes = array('id' => 'addarticle-form', 'class' => 'form-horizontal');?>
            <?php if (!empty($orderinfo)) {
    echo form_open_multipart(CRM_VAR . '/order/edit/' . $orderinfo->order_id, $attributes);
    ?>
            <div class="box-body">
                <div class="row">
                  <div class="col-md-8"></div>
                  <div class="col-md-4" align="right">
                  <a href="<?php echo site_url(); ?>admin/order/view/<?php echo $orderinfo->order_id; ?>" class="btn btn-default" name="save" >Exit</a>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">

                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Order ID</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="order_id" id="order_id" disabled="disabled" value="<?php echo $orderinfo->order_id; ?>">
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Trip Record Name</label>
                            <div class="col-sm-8">
                              <textarea class="form-control" name="trip_record_name" id="trip_record_name" ><?php echo $orderinfo->trip_record_name; ?></textarea>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Customer Name</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control" name="customer_name" id="customer_name" value="<?php echo $orderinfo->name; ?>">
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Customer Phone</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control" name="customer_phone_number" id="customer_phone_number"   value="<?php echo $orderinfo->phone; ?>">
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Customer Email</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control" name="customer_email" id="customer_email"   value="<?php echo $orderinfo->usr_email; ?>">
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Supplier Name</label>
                            <div class="col-sm-8">
                              <?php if ($orderinfo->vendor_payment_status!="complete"): ?>
                                
                                <select required="required" id="supplier_id" name="supplier_id" class="form-control">
                                <option value="">Select Option</option>
                                <?php foreach ($users as $k => $user) { ?>
                                <option value="<?php echo $user->id; ?>" <?php if ($orderinfo->supplier_id == $user->id) {echo "selected";}?>><?php echo $user->user_company; ?></option>
                                <?php } ?>
                              </select>
                              <?php else: ?>
                                <?php foreach ($users as $key => $user): ?>
                                  <?php if ($orderinfo->supplier_id == $user->id): ?>
                                    <input type="hidden" name="supplier_id" value="<?php echo $user->id; ?>">
                                      <input type="text" name="" class="form-control" value="<?php echo $user->user_company; ?>" placeholder="" readonly>
                                      
                                  <?php endif ?>
                                <?php endforeach ?>
                              <?php endif ?>

                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Car Type</label>
                            <div class="col-sm-8">
                            <?php //echo "<pre>";  print_r($cars); echo "</pre>"; ?>
                              <select required="required" id="car_id" name="car_id" class="form-control" required="required">
                                <option value="">Select Option</option>
                                <?php foreach ($cars as $kc => $car) {
        if ($car->status != 2) {
            $car_name = $this->car_manager->get_carname($car->id);
            ?>
                                  <option value="<?php echo $car->id; ?>" <?php if ($orderinfo->car_id == $car->id) {echo "selected";}?>><?php echo $car_name . ' (' . $car->car_number . ')'; ?></option>
                                  <?php
}}?>
                              </select>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Car Tag</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="car_tag" id="car_tag" value="<?php if ($orderinfo->car_id != 0) {if ($orderinfo->car_tag == '') {echo $this->car_manager->get_cartag($orderinfo->car_id);} else {echo $orderinfo->car_tag;}}?>">
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Driver From</label>
                            <div class="col-sm-8">
                                <select name="driver_from" class="form-control" required=""> 
                                    <option>Please Select Option</option>
                                    <option value="1" <?= ($orderinfo->driver_from==1)? 'selected' :'' ?>>Sonicstar</option>
                                    <option value="2" <?= ($orderinfo->driver_from!=1)? 'selected' :'' ?>>Vendor</option>
                                </select>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Driver Name</label>
                            <div class="col-sm-8">
                              <select name="driver_name" class="form-control" id="driver_name">
                                <option value="">-</option>
                              <?php foreach ($drivers as $key => $value): ?>
                                <?php if ($orderinfo->driver_name == $value->user_lname): ?>
                                  <option value="<?=$value->user_lname?>" selected><?=$value->user_lname?></option>
                                <?php else: ?>
                                  <option value="<?=$value->user_lname?>"><?=$value->user_lname?></option>
                                <?php endif?>
                              <?php endforeach?>
                              </select>
                              <span><a href="<?=site_url(CRM_VAR . '/adduser?redirect=' . site_url(CRM_VAR . '/order/edit/' . $orderinfo->order_id))?>" >Add New Driver</a></span>
                              <!-- <input type="text" class="form-control" name="driver_name" id="driver_name" value="<?php echo $orderinfo->driver_name; ?>">                            -->
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Driver Phone</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="driver_phone" id="driver_phone" value="<?php echo $orderinfo->driver_phone; ?>" readonly>
                            </div>
                         </div>
                      </div>

                      <?php
if ($orderinfo->user_role == 'agent') {
        ?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Agent ID</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="agent_id" id="agent_id" value="<?php echo $orderinfo->user_id; ?>">
                            </div>
                         </div>
                      </div>
                      <?php
}
    ?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Type of Rent</label>
                            <div class="col-sm-8">
                              <select required="required" name="type_of_rent" id="type_of_rent" class="form-control" >
                                <option value="0">Select Option</option>
                                <option value="36" <?php if ($orderinfo->type_of_rent == 36) {echo "selected";}?>>Daily</option>
                                <option value="37" <?php if ($orderinfo->type_of_rent == 37) {echo "selected";}?>>Weekly</option>
                                <option value="38" <?php if ($orderinfo->type_of_rent == 38) {echo "selected";}?>>Monthly</option>
                               
                              </select>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12"  id="trip_type_div">
                           <div class="form-group">
                              <label for="inputEmail4" class="col-sm-4 control-label">Trip Type</label>
                              <div class="col-sm-8">
                                <select name="trip_type" id="trip_type" class="form-control" >
                                  <option value="">Select Option</option>
                                  <option value="pickup" <?php if ($orderinfo->airport_status =="pickup") {echo "selected";}?>>Airport Pickup</option>
                                  <option value="dropoff" <?php if ($orderinfo->airport_status =="dropoff") {echo "selected";}?>>Airport Dropoff</option>
                                  <option value="incity" <?php if ($orderinfo->airport_status =="incity") {echo "selected";}?>>Incity</option>
                                  <option value="highway" <?php if ($orderinfo->airport_status =="highway") {echo "selected";}?>>Highway</option>

                                </select>
                              </div>
                           </div>
                        </div>
                        
                      <div class="col-md-12"  id="daily_status_div">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Daily Status</label>
                            <div class="col-sm-8">
                              <select name="daily_status" id="daily_status" class="form-control" >
                                <option value="">Select Option</option>
                                <option value="full-day" <?= ($orderinfo->daily_status)=="full-day"? 'selected' : '' ?>>Full-day</option>
                                <option value="half-day" <?= ($orderinfo->daily_status)=="half-day"? 'selected' : '' ?>>Half-day</option>

                              </select>
                            </div>
                         </div>
                      </div>
                      <?php //if ($orderinfo->type_of_rent == 36): ?>
                        
                      <?php //endif ?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Paid to</label>
                            <div class="col-sm-8">
                              <input type="hidden" name="paid_to" id="paid_to" value="<?=$orderinfo->paid_to?>">
                              <input type="text" id="paid_to_value" class="form-control" readonly="">
                              <!-- <select name="paid_to" class="form-control" disabled="">
                                <option value="0">Select Option</option>
                                <?php foreach ($users as $k => $user) {
        ?>
                                <option value="<?php echo $user->id; ?>" <?php if ($orderinfo->paid_to == $user->id) {echo "selected";}?>><?php echo $user->user_lname; ?></option>
                                <?php
}?>
                              </select>                            -->
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Customer Confirmed</label>
                            <div class="col-sm-8">
                              <input type="radio" class="" name="customer_confirmed" value="1" <?php if ($orderinfo->customer_confirmed == 1) {echo "checked";}?> > YES
                              <input type="radio" class="" name="customer_confirmed" value="0" <?php if ($orderinfo->customer_confirmed == 0) {echo "checked";}?> > NO
                            </div>
                         </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Confirm ID</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="order_id" id="order_id" disabled="disabled" value="<?php echo $orderinfo->order_id; ?>">
                            </div>
                         </div>
                      </div>
                      <?php $pickup_date_time = explode(" ", $orderinfo->pickup_date); ?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Pick Up Date </label>
                            <div class="col-sm-8">
                              <input required="required" type="text" class="form-control datepicker" name="pickup_date" id="pickup_date" value="<?php echo date('m/d/Y',strtotime($orderinfo->pickup_date)) ?>">
                              <?php echo form_error('pickup_date', '<span class="error">', '</span>'); ?>
                            </div>
                         </div>
                      </div>
                       <?php if ($orderinfo->type_of_rent == 38) { ?>
                       <!-- <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">No Of Months </label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" value="<?php echo json_decode($orderinfo->order_history)->no_of_months ?> Months" readonly>
                            </div>
                         </div>
                      </div>-->
                       <!-- <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Drop Off Date</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control datepicker" name="drop_date" value="<?php echo date('m/d/Y',strtotime($orderinfo->drop_date)) ?>">
                            </div>
                         </div>
                      </div> -->
                      <?php } ?> 
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Pick Up time  </label>
                            <div class="col-sm-8">
                              <input required="required" type="text" class="form-control" name="pickup_time" id="pickup_time" value="<?php echo $pickup_date_time[1] ?>">
                              <?php echo form_error('pickup_time', '<span class="error">', '</span>'); ?>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12 ">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Drop off Date</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control datepicker" name="drop_date" id="drop_date" value="<?php echo date('m/d/Y',strtotime($orderinfo->drop_date)) ?>">
                            </div>
                         </div>
                      </div>
                      <?php
if ($orderinfo->type_of_rent == 36) {
        $drop_date_time = explode(" ", $orderinfo->drop_date);
        ?>
                      <div class="col-md-12 rent36">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Drop off Time</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control" name="drop_time" id="drop_time" value="<?php echo $drop_date_time[1]; ?>">
                            </div>
                         </div>
                      </div>
                      <?php }?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Pick Up Place  </label>
                            <div class="col-sm-8">
                              <select required="required" class="form-control" name="pickup_location" id="pickup_location">
                                <?php
foreach ($pickup_locations as $key => $value) {
        ?><option <?php if ($orderinfo->pickup_location == $value->id) {echo "selected";}?> value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option><?php
}
    ?>
                              </select>

                              <?php echo form_error('pickup_location', '<span class="error">', '</span>'); ?>
                            </div>
                         </div>
                      </div>
                      <?php
if ($orderinfo->order_history != null || $orderinfo->order_history != '') {
        $order_history = json_decode($orderinfo->order_history)->searchData;
        if ($orderinfo->type_of_rent == 36 && $orderinfo->airport_status == 'highway') {?>
                      <div class="col-md-12 rent36">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Drop off Place</label>
                            <div class="col-sm-8">
                              <select required="required" class="form-control" name="drop_location" id="drop_location">

                              </select>
                            </div>
                         </div>
                      </div>
                      <?php }}?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Order Date</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control" name="booking_date" id="booking_date" value="<?php echo $orderinfo->booking_date; ?>">
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Payment Type</label>
                            <div class="col-sm-8">
                                <select name="payment_method" class="form-control" required="required" id="payment_method" >
                                    <option value="COD" <?php if ($orderinfo->payment_method == "COD") {echo "selected";}?>>COD</option>
                                    <option value="MPU" <?php if ($orderinfo->payment_method == "MPU") {echo "selected";}?>>MPU</option>
                                    <option value="KBZ" <?php if ($orderinfo->payment_method == "KBZ") {echo "selected";}?>>KBZ</option>
                                    <option value="Paypal" <?php if ($orderinfo->payment_method == "Paypal") {echo "selected";}?>>Paypal</option>
                                </select>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Driver OT Rate</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control number-format" name="driver_ot_fees" id="driver_ot_fees" value="<?php echo $orderinfo->driver_ot_fees; ?>">
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Driver OT Hours</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control number-format" name="driver_ot_hours" id="driver_ot_hours" value="<?php echo $orderinfo->ot_hours; ?>">
                            </div>
                         </div>
                      </div>
                      <!-- <div class="row"> -->
                        
                        <div class="col-md-6">

                           <div class="form-group">
                              <label for="inputEmail4" class="col-sm-8 control-label">No; of Zone</label>
                              <div class="col-sm-4">
                                <input type="text" required="required" class="form-control number-format" name="no_of_zone" id="driver_ot_hours" value="<?php echo $orderinfo->no_of_zone; ?>">
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="inputEmail4" class="col-sm-5 control-label">Per Zone Fees</label>
                              <div class="col-sm-7">
                                <input type="text" required="required" class="form-control number-format" name="zone_fees" id="driver_ot_hours" value="<?php echo $orderinfo->zone_fees; ?>">
                              </div>
                           </div>
                           
                        </div>
                      <!-- </div> -->
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Total Amount</label>
                            <div class="col-sm-8">
                              <input type="text" required="required" class="form-control number-format" name="total_amount" id="total_amount" value="<?php echo $orderinfo->amount; ?>">
                              <span>Actual Total: OT * Rental Fees = <?= number_format(($orderinfo->ot_hours*$orderinfo->driver_ot_fees)+$orderinfo->amount) ?></span>
                            </div>
                         </div>
                      </div>
                       <?php foreach ($agents as $k => $agent_val): ?>
                        <?php if ($orderinfo->user_id == $agent_val->id): ?>
                          <?php $agent = true;?>
                          <div class="col-md-12">
                                 <div class="form-group">
                                    <label for="inputEmail4" class="col-sm-4 control-label">Agent Fees</label>
                                    <div class="col-sm-8">
                                      <input type="text" required="required" class="form-control number-format" name="agent_fees" id="agent_fees" value="<?= $orderinfo->agent_fees ?>">
                                      <span><?=$agent_val->commission_rate?>% (Agent Commission)</span>
                                    </div>
                                 </div>
                              </div>
                            <!-- <li><a href="<?php echo site_url(); ?>admin/order/agent_fees/<?php echo $orderinfo->order_id; ?>">Agent Fees</a></li> -->
                          <?php endif?>
                        <?php endforeach?>
                        <?php if ($orderinfo->type_of_rent == 38 || $orderinfo->type_of_rent == 37): ?>
                                <div class="col-md-12">
                                   <div class="form-group">
                                      <label for="inputEmail4" class="col-sm-4 control-label">Driver Salary</label>
                                      <div class="col-sm-8">
                                        <input type="text" required="required" class="form-control number-format" name="driver_salary" id="driver_salary" value="<?php echo $orderinfo->driver_salary; ?>">
                                      </div>
                                   </div>
                                </div>
                              <?php endif ?>
                      <?php foreach ($users as $k => $user): ?>
                          <?php if ($orderinfo->supplier_id == $user->id): ?>
                            <?php if ($user->user_lname != 'Sonic Star'): ?>
                              <?php $vendor = true;?>
                              <div class="col-md-12">
                                 <div class="form-group">
                                    <label for="inputEmail4" class="col-sm-4 control-label">Vendor Fees</label>
                                    <div class="col-sm-8">
                                      <input type="text" required="required" class="form-control number-format" name="vendor_fees" id="vendor_fees" value="<?= $orderinfo->vendor_fees ?>" <?= ($orderinfo->vendor_payment_status=="complete"? 'readonly' : '') ?>>
                                      <?php //(($orderinfo->vendor_fees==0)? $orderinfo->amount - ($orderinfo->amount * $user->commission_rate / 100) : $orderinfo->vendor_fees) ?>
                                      <span><?=$user->commission_rate?>% to Sonic Star and <?=100 - $user->commission_rate?>% to <?=$user->user_fname . ' ' . $user->user_lname?></span>
                                    </div>
                                 </div>
                              </div>
                              
                            <div class="col-md-12">
                             <div class="form-group">

                                <label for="inputEmail4" class="col-sm-4 control-label">Vendor Payment Status</label>
                                <?php if ($logged_user->user_role=="admin"): ?>
                                  <div class="col-sm-8">
                                    <select name="vendor_payment_status" class="form-control">
                                      <option value="pending" <?php if ($orderinfo->vendor_payment_status == "pending") {echo "selected";}?>>Pending</option>
                                      <option value="complete" <?php if ($orderinfo->vendor_payment_status == "complete") {echo "selected";}?>>Complete</option>
                                    </select>
                                  </div>
                                <?php else: ?>
                                  <label class="col-sm-8 control-label" style="text-align: left;"><?php echo $orderinfo->vendor_payment_status; ?> </label>
                                <?php endif ?>
                             </div>
                          </div>
                            <?php endif?>
                          <?php endif?>

                        <?php endforeach?>
                        <?php foreach ($agents as $k => $agent_val): ?>
                        <?php if ($orderinfo->user_id == $agent_val->id): ?>
                          <?php $agent = true;?>
                          <div class="col-md-12">
                             <div class="form-group">
                                <label for="inputEmail4" class="col-sm-4 control-label">Agent Payment Status</label>
                                <label class="col-sm-8 control-label" style="text-align: left;"><?php echo $orderinfo->agent_payment_status; ?> </label>
                             </div>
                          </div>
                            <!-- <li><a href="<?php echo site_url(); ?>admin/order/agent_fees/<?php echo $orderinfo->order_id; ?>">Agent Fees</a></li> -->
                          <?php endif?>
                        <?php endforeach?>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Customer Payment Status</label>
                            <?php if ($logged_user->user_role=="admin"): ?>
                              <div class="col-sm-8">
                                <select name="customer_payment_status" class="form-control">
                                  <option value="pending" <?php if ($orderinfo->customer_payment_status == "pending") {echo "selected";}?>>Pending</option>
                                  <option value="complete" <?php if ($orderinfo->customer_payment_status == "complete") {echo "selected";}?>>Complete</option>
                                </select>
                              </div>
                            <?php else: ?>

                              <label class="col-sm-8 control-label " style="text-align: left;"><?php echo $orderinfo->customer_payment_status; ?> </label>
                            <?php endif?>
                         </div>
                      </div>

                      
                       <!-- <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Vendor Payment Status</label>
                            <label class="col-sm-8 control-label " style="text-align: left;"><?php echo $orderinfo->vendor_payment_status; ?> </label>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Agent Payment Status</label>
                            <label class="col-sm-8 control-label " style="text-align: left;"><?php echo $orderinfo->agent_payment_status; ?> </label>
                         </div>
                      </div> -->
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Order Status</label>
                            <div class="col-sm-8">
                              <select name="status" class="form-control">
                                <option value="Pending" <?php if ($orderinfo->status == "Pending") {echo "selected";}?>>Pending</option>
                                <option value="Confirmed" <?php if ($orderinfo->status == "Confirmed") {echo "selected";}?>>Confirmed</option>
                                <option value="Cancelled" <?php if ($orderinfo->status == "Cancelled") {echo "selected";}?>>Cancelled</option>

                                <?php 

                                  if($orderinfo->supplier_confirmed == 1 && $orderinfo->customer_confirmed == 1 && $orderinfo->customer_payment_status=="complete"){

                                    if($vendor && $agent){

                                      // echo "<option>".($agent)."</option>";
                                      if($orderinfo->vendor_payment_status=="complete" && $orderinfo->agent_payment_status=="complete"){

                                        echo '<option value="Completed" '.($orderinfo->status == "Completed"? 'selected' : '' ).' >Completed</option>';
                                      }
                                    }else if ($vendor && $agent==false ){
                                      // echo "<option>hre 4</option>";
                                      if($orderinfo->vendor_payment_status=="complete"){
                                        echo '<option value="Completed" '.($orderinfo->status == "Completed"? 'selected' : '' ).' >Completed</option>';
                                      }
                                    }else if ($vendor==false && $agent ){
                                      // echo "<option>hre 6</option>";
                                      if($orderinfo->agent_payment_status=="complete"){
                                        echo '<option value="Completed" '.($orderinfo->status == "Completed"? 'selected' : '' ).' >Completed</option>';
                                      }
                                    }else{
                                      echo '<option value="Completed" '.($orderinfo->status == "Completed"? 'selected' : '' ).' >Completed</option>';
                                    }

                                  }

                                ?>
                               <!--  <?php if ($orderinfo->supplier_confirmed == 1 && $orderinfo->customer_confirmed == 1 && $orderinfo->customer_payment_status == "complete"): ?>

                                <?php if (($vendor && $orderinfo->vendor_payment_status == "complete") && ($agent && $orderinfo->agent_payment_status == "complete")){ ?>

                                  <option value="Completed" <?php if ($orderinfo->status == "Completed") {echo "selected";}?>>Completed</option>
                                <?php //endif?>
                                <?php }else if ($vendor && $orderinfo->vendor_payment_status == "complete"){ ?>

                                  <option value="Completed" <?php if ($orderinfo->status == "Completed") {echo "selected";}?>>Completed</option>
                                <?php //endif?>
                                <?php }else if ($agent && $orderinfo->agent_payment_status == "complete"){ ?>

                                  <option value="Completed" <?php if ($orderinfo->status == "Completed") {echo "selected";}?>>Completed</option>
                                <?php } ?>
                                <?php endif?> -->

                              </select>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                            <label for="inputEmail4" class="col-sm-4 control-label">Supplier Confirmed</label>
                            <div class="col-sm-8">
                              <input type="radio" class="" name="supplier_confirmed" value="1" <?php if ($orderinfo->supplier_confirmed == 1) {echo "checked";}?> > YES
                              <input type="radio" class="" name="supplier_confirmed" value="0" <?php if ($orderinfo->supplier_confirmed == 0) {echo "checked";}?> > NO
                            </div>
                         </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <!-- Comment Section -->

            <div class="box box-warning">
              <div class="box-header ">
                <h3 class="box-title">Comment</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-sm-12">
                    <?php if (!empty($order_comment)) {?>
                    <ul class="timeline">
                      <!-- Comment -->
                      <?php foreach ($order_comment as $key => $value) {?>
                      <li class="time-label">
                            <span class="bg-red">
                              <?php echo date("d M, Y", strtotime($value->created_date)); ?>
                            </span>
                      </li>
                      <li>
                        <i class="fa fa-comment bg-blue"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="fa fa-clock-o"></i> <?php echo date("h:i A", strtotime($value->created_date)); ?></span>

                          <h3 class="timeline-header">Submitted on <a><?php echo date("d M Y", strtotime($value->created_date)); ?></a> by <a><?php echo $value->user_fname . " " . $value->user_lname; ?></a></h3>

                          <div class="timeline-body">
                            <?php echo $value->comment; ?>
                          </div>
                        </div>
                      </li>
                      <?php }?>
                      <!-- Comment -->
                      <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                      </li>
                    </ul>
                    <?php } else {echo "<h4>No Comment yet!!</h4>";}?>
                  </div>
                </div><br/>
                <div class="row">
                  <div class="col-sm-12">
                    <?php echo form_error('comment', '<span class="error">', '</span>'); ?>
                    <textarea name="comment" id="comment" class="form-control" rows="4" placeholder="Enter Comment..." required="required"></textarea>
                  </div>
                </div>
              </div>

            </div>

            <!-- Comment Section -->

            <!-- /.box-body -->
            <div class="box-footer">
               <button type="submit" class="btn btn-info" name="save">Edit Order</button>
            </div>
            <!-- /.box-footer -->
            <?php echo form_close();} ?>
         </div>
         <!-- /.box -->
      </div>
      <!--/.col (left) -->
   </div>
   <!-- /.row -->
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('.datepicker').datepicker({
autoclose: true
});
    function getCarTag(car_id){
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url(CRM_VAR . '/Order/getCarTags') ?>',
          data: {car_id:car_id},
          dataType: "JSON",
          success: function(resultData) {
            if(resultData['status']){$("#car_tag").val(resultData['data']);}
          }
        });
      }

      function byRentType(rent_type) {
        if(rent_type == 36){
          $(".rent36").show();
          $("#trip_type_div").show();
          
        }else{
          $(".rent36").hide();
          $("#trip_type_div").hide();
          $("#trip_type").val("");
        }
        console.log(rent_type); 
      }
      function byTripType(trip_type){
        if(trip_type=="incity"){
          $("#daily_status_div").show();
        }else{
          $("#daily_status_div").hide();
          $("#daily_status").val("");
        }
      }

      var rent_type = $("#type_of_rent").val();
      byRentType(rent_type);
      $("#daily_status_div").hide();
      $("#type_of_rent").change(function(){
        byRentType($(this).val());
      });

      var trip_type = $("#trip_type").val();
      byTripType(trip_type);
      $("#trip_type").change(function(){
        byTripType($(this).val());
        
      });
      $("#paid_to_value").val($("#supplier_id :selected").text());
      var allCarNames = <?php echo json_encode($allCarNames); ?>;
      $("#supplier_id").change(function(){
        var supplier_id = $(this).val();
        var car_id = <?php echo $orderinfo->car_id; ?>;
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url(CRM_VAR . '/Order/getCars') ?>',
          data: {supplier_id:supplier_id},
          dataType: "JSON",
          success: function(resultData) {
            var html = '';//<option>Select Option</option>
            $.each(resultData,function(i,v){
              if(v.status != 2){
                html = html+'<option value="'+v.id+'"';
                if(car_id == v.id){
                  html = html+' selected="selected" ';
                }
                html = html+'>'+allCarNames[v.id]+' ('+v.car_number+')</option>';
                $("#paid_to").val(supplier_id);
                $("#paid_to_value").val($("#supplier_id :selected").text());
              }
            });
            $("#car_id").html(html);
            getCarTag($("#car_id").val());
            if(car_id != $("#car_id").val())
            {
              $("#car_tag").val('');
              $("#driver_name").val('');
              $("#driver_phone").val('');
            }
            else{
              $("#car_tag").val('<?php echo $orderinfo->car_tag; ?>');
              $("#driver_name").val('<?php echo $orderinfo->driver_name; ?>');
              $("#driver_phone").val('<?php echo $orderinfo->driver_phone; ?>');
            }
          }
        });
      });
      $("#car_id").change(function(){
        getCarTag($(this).val());
      });




      function getdestination(parent_id, my_id){
        $.ajax({
          type: 'POST',
          url: '<?php echo site_url(CRM_VAR . '/Order/getdestination') ?>',
          data: {parent_id:parent_id},
          dataType: "json",
          success: function(resultData) {
            if(resultData['status']){
              var html = '';//<option>Select Option</option>
              $.each(resultData['data'],function(i,v){
                html = html+'<option value="'+v.id+'" ';
                if(v.id == my_id){
                  html = html+'selected';
                }
                html = html+'>'+v.title+'</option>';
              });
              $('#drop_location').html(html);
            }
            else{
              $('#drop_location').html('');
            }
          }

        });
      }

      var pickup = $("#pickup_location").val();
      var destination = <?php echo $v = $orderinfo->drop_location == '' ? 0 : $orderinfo->drop_location; ?>;

      getdestination(pickup, destination);

      $("#pickup_location").change(function(){
        var destination = <?php echo $v = $orderinfo->drop_location == '' ? 0 : $orderinfo->drop_location; ?>;
        getdestination($(this).val(), destination);
      });

      $('#driver_name').change(function(){
        if($(this).val()!=""){
          $.post('<?=site_url(CRM_VAR . '/driverinfo/')?>'+$(this).val(),function(data){
            var obj=$.parseJSON(data);
            $('#driver_phone').val(obj.user_tel);

            // console.log();
          });
        }else{
          $('#driver_phone').val("");
        }
        // sub_acc_info($(this).val());

      });

  });
</script>

