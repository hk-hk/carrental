<?php
defined('BASEPATH') OR exit('ယခုစာမ်က္နာအား ၾကည့္ရႈ႕ခြင့္မျပဳပါ');

$lang['ORDER_MENU_MY_PROFILE'] = 'မိမိ၏ စာမ်က္ႏွာ';
$lang['ORDER_MENU_LOGOUT'] = 'အေကာင့္မွထြက္ရန္';
$lang['ORDER_CAR_NAME'] = 'ကား နာမည္';
$lang['ORDER_BOOKING_AMOUNT'] = 'ဘြတ္ကင္ ေငြပမာဏ';
$lang['ORDER_PICKUP_DATE'] = ' ၾကိဳရမည့္ ေန႔';
$lang['ORDER_DROPOFF_DATE'] = 'ပို႔ရမည့္ ေန႔';
$lang['ORDER_BOOKING_DATE'] = 'ဘြတ္ကင္လုပ္မည့္ေန႔';
$lang['ORDER_ACTION'] = 'လုပ္ေဆာင္ေနသည္';
$lang['ORDER_ORDER_ID'] = 'ေအာ္ဒါ အမွတ္စဥ္';
$lang['ORDER_ORDER_CANCEL'] = 'ေအာ္ဒါ ပယ္ဖ်က္ရန္';
$lang['ORDER_CUST_NAME'] = 'မွာယူသူ နာမည္';
$lang['ORDER_SUPP_NAME'] = 'ေပးသြင္းသူ နာမည္';
$lang['ORDER_CAR_TYPE'] = 'ကား အမ်ိဳးအစား';
$lang['ORDER_CAR_TAG'] = 'ကား နံပါတ္';
$lang['ORDER_DRIVER_NAME'] = 'ယာဥ္ေမာင္း နာမည္';
$lang['ORDER_DRIVER_PHONE'] = 'ယာဥ္ေမာင္း ဖုန္း';
$lang['ORDER_PICK_UP_TIME'] = ' ၾကိဳရမည့္အခ်ိန္္';
$lang['ORDER_DROP_OFF_TIME'] = ' ပို႔ရမည့္အခ်ိန္';
$lang['ORDER_PICKUP_PLACE'] = ' ၾကိဳရမည္ ေနရာ';
$lang['ORDER_PAYMENT_TYPE'] = ' ေငြေပးေခ်မႈပံုစံ';
$lang['ORDER_TOTAL_AMOUNT'] = 'စုစုေပါင္း ေငြပမာဏ';
$lang['ORDER_ORDER_STATUS'] = 'ေအာ္ဒါ အေျခအေန';
$lang['ORDER_BOOKING_CANCELLATION'] = 'ေအာ္ဒါ ပယ္ဖ်က္ျခင္း';
$lang['ORDER_REASON_FOR_CANCEL'] = 'ပယ္ဖ်က္ရျခင္း အေၾကာင္းအရာ';
$lang['ORDER_SUBMIT_CANCELLATION'] = ' ေအာ္ဒါဖ်က္ရန္ႏွိပ္ပါ';

$lang['ORDER_SUCCESS_THANKYOU_MSG'] = 'သင့္ေအာ္ဒါ အတြက္ ေက်းဇူးတင္ပါသည္.';
$lang['ORDER_FAILED_THANKYOU_MSG'] = ' သင့္ ေအာ္ဒါ မေအာင္ျမင္ပါ';
$lang['ORDER_SUCCESS_THANKYOU_MSG'] = 'သင့္ အီးေမးလ္သို႔ အတည္ျပဳျခင္းစာ ေရာက္ရိွလိမ့္မည္';
$lang['ORDER_FAILED_MSG'] = 'ယခုု ေအာ္ဒါတင္ျခင္းကိုု ဆက္လက္လုပ္ေဆာင္ ႏိုင္ပါသည္ ';
$lang['ORDER_REORDER_FAILED_ORDER_LINK'] = ' ဤေနရာကိုု ႏွိပ္ပါ';
$lang['ORDER_BOOKING_INFO_HEADING'] = 'ေအာ္ဒါ အခ်က္အလက္';
$lang['ORDER_MY_BOOKING'] = ' မိမိေအာ္ဒါမ်ား';
$lang['ORDER_ACCOUNT'] = ' အေကာင့္';
$lang['ORDER_HOME'] = 'မူလေနရာ';

$lang['ORDER_PAYMENT_PROCESSING'] = 'ပိုက္ဆံေပးေနစဥ္ အတြင္း  ယခုစာမ်က္နာမွ မထြက္ပါနဲ႔';

$lang['ORDER_DETAILS_GIVE_FEEDBACK'] = '‘ေအာ္ဒါ အေသးစိတ္';
$lang['ORDER_DETAILS_FEEDBACK_SECTION_HEADING'] = '၀န္ေဆာင္မႈ တံုု႕ျပန္ခ်က္';
$lang['ORDER_DETAILS_CUSTOMER_FEEDBACK'] = 'ခရီးသည္၏ တံုု႕ျပန္ခ်က္';
$lang['ORDER_DETAILS_SUPPLIER_FEEDBACK'] = 'ကားငွားသူ၏ တံုု႕ျပန္ခ်က္';
$lang['ORDER_DETAILS_RATING_TEXT'] = 'အဆင့္သတ္မွတ္ခ်က္';
?>
