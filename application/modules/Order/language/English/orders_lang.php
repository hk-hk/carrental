<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['ORDER_MENU_MY_PROFILE'] = 'My Profile';
$lang['ORDER_MENU_LOGOUT'] = 'Logout';
$lang['ORDER_CAR_NAME'] = 'Car Name';
$lang['ORDER_BOOKING_AMOUNT'] = 'Booking Amount';
$lang['ORDER_PICKUP_DATE'] = 'Pickup Date';
$lang['ORDER_DROPOFF_DATE'] = 'Dropoff Date';
$lang['ORDER_BOOKING_DATE'] = 'Booking Date';
$lang['ORDER_ACTION'] = 'Action';
$lang['ORDER_ORDER_ID'] = 'Order ID';
$lang['ORDER_ORDER_CANCEL'] = 'Order Cancel';
$lang['ORDER_CUST_NAME'] = 'Customer Name';
$lang['ORDER_SUPP_NAME'] = 'Supplier Name';
$lang['ORDER_CAR_TYPE'] = 'Car Type';
$lang['ORDER_CAR_TAG'] = 'Car Tag';
$lang['ORDER_DRIVER_NAME'] = 'Driver Name';
$lang['ORDER_DRIVER_PHONE'] = 'Driver Phone';
$lang['ORDER_PICK_UP_TIME'] = 'Pickup Time';
$lang['ORDER_DROP_OFF_TIME'] = 'Dropoff Time';
$lang['ORDER_PICKUP_PLACE'] = 'Pickup Place';
$lang['ORDER_PAYMENT_TYPE'] = 'Payment Type';
$lang['ORDER_TOTAL_AMOUNT'] = 'Total Amount';
$lang['ORDER_ORDER_STATUS'] = 'Order Status';
$lang['ORDER_BOOKING_CANCELLATION'] = 'Booking cancellation';
$lang['ORDER_REASON_FOR_CANCEL'] = 'Reason For Cancellation';
$lang['ORDER_SUBMIT_CANCELLATION'] = 'Submit Cancellation';

$lang['ORDER_SUCCESS_THANKYOU_MSG'] = 'Thank you for your order.';
$lang['ORDER_FAILED_THANKYOU_MSG'] = 'Your booking has failed.';
$lang['ORDER_SUCCESS_THANKYOU_MSG'] = 'You will receive an email confirmation shortly at your email address';
$lang['ORDER_FAILED_MSG'] = 'You can continue with the same booking by ';
$lang['ORDER_REORDER_FAILED_ORDER_LINK'] = ' Click Here';
$lang['ORDER_BOOKING_INFO_HEADING'] = 'Booking Info';
$lang['ORDER_MY_BOOKING'] = 'My Booking';
$lang['ORDER_ACCOUNT'] = 'Account';
$lang['ORDER_HOME'] = 'Home';

$lang['ORDER_PAYMENT_PROCESSING'] = 'While processing do not refresh page.';

$lang['ORDER_DETAILS_GIVE_FEEDBACK'] = 'Give Feedback';
$lang['ORDER_DETAILS_FEEDBACK_SECTION_HEADING'] = 'Customer/ Supplier Feedback';
$lang['ORDER_DETAILS_CUSTOMER_FEEDBACK'] = 'Customer Feedback';
$lang['ORDER_DETAILS_SUPPLIER_FEEDBACK'] = 'Supplier Feedback';
$lang['ORDER_DETAILS_RATING_TEXR'] = 'Rating';





