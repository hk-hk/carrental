<?php

$route[CRM_VAR.'/order/list']     = 'Report/Report/order';
// $route[CRM_VAR.'/order/list']     = 'Order/Order/listOrder'; 
$route[CRM_VAR.'/order/list/(:any)']     = 'Order/Order/listOrder';
$route[CRM_VAR.'/order/ajax_order_list'] = 'Order/Order/ajax_order_list'; 
$route[CRM_VAR.'/order/view/(:num)']     = 'Order/Order/viewOrder/$1';
$route[CRM_VAR.'/order/expenses/(:num)']     = 'Order/Order/tripExpenses/$1';
$route[CRM_VAR.'/order/expenses/setlatest/(:num)/(:num)']     = 'Order/Order/setlatest/$1/$2';
$route[CRM_VAR.'/order/expenses/(:num)/(:num)']     = 'Order/Order/tripExpenses/$1/$2';
$route[CRM_VAR.'/order/store_expenses/(:num)']     = 'Order/Order/tripExpenseStore/$1';
$route[CRM_VAR.'/order/store_expenses/(:num)/(:num)']     = 'Order/Order/tripExpenseStore/$1/$2';
$route[CRM_VAR.'/order/edit/(:num)']     = 'Order/Order/editOrder/$1'; 
$route[CRM_VAR.'/order/delete']     = 'Order/Order/delete_order'; 
$route[CRM_VAR.'/order/print/(:num)']     = 'Order/Order/print/$1'; 
$route[CRM_VAR.'/order/driver_ot/(:num)']     = 'Order/Order/driver_ot/$1';
$route[CRM_VAR.'/order/driver_ot_insert']     = 'Order/Order/driver_ot_insert';
$route[CRM_VAR.'/order/duplicate/(:num)']     = 'Order/Order/duplicate/$1';

$route[CRM_VAR.'/driverinfo/(:any)']     = 'Order/Order/driverInfo/$1'; 

$route[CRM_VAR.'/order/cancle/(:num)']     = 'Order/Order/cancleOrder/$1';
$route[CRM_VAR.'/Order/send_confirmation_mail'] = 'Order/Order/send_confirmation_mail';
$route[CRM_VAR.'/Order/getCars'] = 'Order/Order/getCars';
$route[CRM_VAR.'/Order/getCarTags'] = 'Order/Order/getCarTags';
$route[CRM_VAR.'/order/submit_feedback/(:num)']     = 'Order/Order/submit_feedback/$1';
$route[CRM_VAR.'/Order/getdestination'] = 'Order/Order/getdestination';

$route[CRM_VAR.'/reports']     = 'Order/Order/reports'; 
$route[CRM_VAR.'/reports/(:any)']     = 'Order/Order/reports';
$route[CRM_VAR.'/ajax-reports'] = 'Order/Order/ajax_reports'; 
$route[CRM_VAR.'/add-supplier-booking'] = 'Order/add_supplier_booking'; 
$route[CRM_VAR.'/get-supplier-booking'] = 'Order/get_supplier_booking'; 
$route[CRM_VAR.'/delete-supplier-booking'] = 'Order/delete_supplier_booking'; 
$route[CRM_VAR.'/booking-calendar'] = 'Order/booking_calendar'; 
$route[CRM_VAR.'/booking-calendar-ajax'] = 'Order/booking_calendar_ajax'; 
$route[CRM_VAR.'/suppliers'] = 'Order/suppliers'; 

$route[CRM_VAR.'/accountinfo/(:any)']      = 'Order/Order/accountinfo/$1';
$route[CRM_VAR.'/order/payment/(:any)']      = 'Order/Order/payment/$1';
$route[CRM_VAR.'/order/store_payment/(:any)']      = 'Order/Order/store_payment/$1';
$route[CRM_VAR.'/order/advance/(:any)']      = 'Order/Order/advance/$1';
$route[CRM_VAR.'/order/store_advance/(:any)']      = 'Order/Order/store_advance/$1';

$route[CRM_VAR.'/order/fuel/(:num)']     = 'Order/Order/fuelExpenses/$1';
$route[CRM_VAR.'/order/store_fuel/(:num)']     = 'Order/Order/store_fuelExpenses/$1';

$route[CRM_VAR.'/order/tollnother/(:num)']     = 'Order/Order/tollnotherExpenses/$1';
$route[CRM_VAR.'/order/store_tollnother/(:num)']     = 'Order/Order/store_tollnotherExpenses/$1';

$route[CRM_VAR.'/order/sms/(:num)']     = 'Order/Order/sms/$1';

$route[CRM_VAR.'/order/vendor_fees/(:num)']     = 'Order/Order/vendorFees/$1';
$route[CRM_VAR.'/order/agent_fees/(:num)']     = 'Order/Order/agentFees/$1';
$route[CRM_VAR.'/order/payment_complete/(:num)']     = 'Order/Order/paymentComplete/$1';
// $route[CRM_VAR.'/order/store_tollnother/(:num)']     = 'Order/Order/store_tollnotherExpenses/$1';


$route['book_now'] = 'Order/Products/booknow';
$route['kbz_book'] = 'Order/Products/kbz_book';
$route['test_book1'] = 'Order/Products/test_book1';
$route['test_book'] = 'Order/Products/test_book';
$route['returm_mpu'] = 'Order/Products/returm_mpu';

$route['booking-complete'] = 'Order/Products/booking_complete';
$route['booking-setup'] = 'Order/Products/booking_setup';

$route['order-complete'] = 'Order/Products/order_complete';
$route['order-setup'] = 'Order/Products/order_setup';

$route['submit-feedback/(:num)']     = 'Order/Products/submit_feedback/$1';
$route['success_mpu'] = 'Order/Products/success_mpu';
$route['booking_info'] = 'Order/Products/booking_success';
$route['booking_info/(:num)'] = 'Order/Products/booking_success/$1';
$route['paypal'] = 'Order/Products';
$route['products/success'] = 'Order/Products/success';
$route['products/cancel'] = 'Order/Products/cancel';
$route['products/ipn'] = 'Order/Products/ipn';
$route['email-temlate'] = 'Order/Products/emailTemplate';
//Manage customer orders
$route['myaccount/orders']     = 'Order/Products/listOrder'; 
$route['myaccount/order_details/(:num)']     = 'Order/Products/order_details/$1'; 

$route['cancel-order/(:num)'] = 'Order/Products/cancleOrder/$1';