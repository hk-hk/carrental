<?php
/***
 *
 * Created by: CIS
 * This is for authorized user's only.
 * This file contains all Property's related functions.
 *
 ***/
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "/third_party/TripleSMS.php";
class Order extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->data['module'] = 'Order';
        $this->load->model('Orders', 'orders');
        $this->load->model('Expense', 'expense');
        $this->load->model('DriverOT', 'driverot');
        $this->load->model('Users/User', 'user');
        $this->load->model('Accounts/Accounts', 'account'); // Load User modal
        $this->load->model('Payment', 'payment'); // Load User modal
        $this->load->model('Car_manager/Car_manager', 'car_manager');

        $this->load->model('Articles/Articles', 'articles');
        $this->load->model('Rating/Ratings', 'ratings');
        $this->load->model('Attributes/Attributes', 'attributes');
        $this->load->model('Transation/Transations', 'transations');
        $this->load->model('Car_manager/Car_oilslip', 'car_oilslip'); // Load Car Oilslip modal
        $this->data['logged_user']                  = $this->user->get_by("id", $this->session->userdata("crm_user_id"));
        //$this->orders->getOrderInformation(72);
        //$this->load->model('Objects', 'objects');
        // $this->load->model('ToolModel');
        // $this->load->library('fpdf');
    }
    public function index()
    {

    }

    /* submit_feedback */
    public function submit_feedback($order_id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $validate_arr = array(
                array(
                    'field' => 'rating',
                    'label' => 'Rating',
                    'rules' => 'trim|required|numeric',
                ),
                array(
                    'field' => 'comment',
                    'label' => 'Comment',
                    'rules' => 'trim|required',
                ),
            );
            $this->form_validation->set_rules($validate_arr);
            if ($this->form_validation->run() == false) {
                $this->set_msg_flash([
                    'msg' => 'Invalid Form Submittion!!',
                    'typ' => 3,
                ]);
                redirect(site_url() . CRM_VAR . '/order/view/' . $order_id);
            } else {
                $order_id = $this->input->post('order_id');
                $rating   = $this->input->post('rating');
                $comment  = $this->input->post("comment");
                $res      = $this->do_rating($order_id, $rating, $comment);
                if ($res) {
                    $this->set_msg_flash(['msg' => 'Feedback Submitted.', 'typ' => 1]);
                    redirect(site_url() . CRM_VAR . '/order/view/' . $order_id);
                }
            }
        }
    }
    public function do_rating($order_id, $rating, $comment)
    {
        $user_id = $this->session->userdata("crm_user_id");
        if ($this->session->userdata("cr_user_role") == 'customer' || $this->session->userdata("cr_user_role") == 'agent') {
            $user_role = 'Customer';
        } else if ($this->session->userdata("cr_user_role") == 'supplier') {
            $user_role = 'Supplier';
        } else {
            $user_role = 'Supplier';
        }
        //for car id
        $orderInfo = $this->orders->get_order($order_id);
        $car_id    = $orderInfo->car_id;

        //array for inserting rating
        $rating_array = array(
            'order_id'     => $order_id,
            'car_id'       => $car_id,
            'user_id'      => $user_id,
            'rating'       => $rating,
            'created_date' => date("Y-m-d H:i:s"),
            'comment'      => $comment,
            'rating_by'    => $user_role,
        );
        if ($this->ratings->insert($rating_array)) {
            $new_rating = $this->ratings->get_rating($car_id);
            $this->car_manager->update($car_id, array("ratings" => $new_rating->rating));
            return true;
        } else {
            return false;
        }
    }
    /* submit_feedback */
    /* list of order */
    public function reports()
    {
        // // $this->isAuthorized();
        // $this->checkPermission('commission_report');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        //$this->data['content_category'] = $this->articles->get_attributes(array(30),TRUE);
        $this->data['suppliers']    = $this->user->get_many_by("user_role", "supplier");
        $this->data['customers']    = $this->user->get_many_by("user_role", "customer");
        $this->data['agents']       = $this->user->get_many_by("user_role", "agent");
        $this->data['order_status'] = $this->uri->segment(4) ? $this->uri->segment(4) : "";
        $this->data['view']         = 'ajax_reports';
        $list                       = $this->orders->get_datatables();
        //print_r($list);die;
        //foreach ($list as $l){
        //print_r(json_decode($l->order_history)->carinfo->carname);die;
        //}
        $this->layout->admin($this->data);
    }
    /* list of order */

    /* ajax call for order table */

    public function ajax_reports()
    {
        //list of all terminals for ajax datatables
        // // $this->isAuthorized();
        // $this->checkPermission('commission_report');
        $this->is_ajax();
        //$this->articles->belongs = $this->session->userdata('crm_user_id'); //Logged in user id
        $list = $this->orders->get_datatables();

        // $is_edit_action = $this->checkPermission('order_edit', false);
        $data           = array();
        $no             = $_POST['start'];
        $user_role      = $this->session->userdata('cr_user_role');
        foreach ($list as $term) {
            $no++;
            $row           = array();
            $order_history = json_decode($term->order_history);
            $row[]         = $no;
            $row[]         = '<a href="' . site_url(CRM_VAR) . '/order/view/' . $term->order_id . '">' . $term->order_id . '<a/>';
            $row[]         = $order_history->carinfo->carname;
            $row[]         = $term->suplname;
            $row[]         = $term->user_tel;
            if ($user_role == 'admin') {
                $row[] = $term->user_lname;
                $row[] = $term->user_tel;
            } else {
                $row[] = $term->fullname;
                $row[] = $term->uphone;
            }
            $currency = strtoupper($order_history->currency);
            $row[]    = date("M d,Y", strtotime($term->pickup_date));
            $row[]    = date("M d,Y", strtotime($term->drop_date));
            $row[]    = $currency . number_format($term->booking_amount, 0, ".", ',');
            if ($term->sup_commi < 1) {
                $term->sup_commi = 10;
            }

            $sup_commi   = (($term->booking_amount * $term->sup_commi) / 100);
            $agent_commi = (($term->booking_amount * $term->agent_commi) / 100);
            if (in_array($user_role, array('supplier', 'admin'))) {
                $row[] = $currency . number_format(($sup_commi - $agent_commi), 0, ".", ',');
            }
            if (in_array($user_role, array('agent', 'admin'))) {
                $row[] = $currency . number_format((($term->booking_amount * $term->agent_commi) / 100), 0, ".", ',');
            }

            $row[]    = $currency . number_format(($term->booking_amount) - ($sup_commi), 0, ".", ',');
            $isActive = ($term->status == 2) ? 'btn-danger' : 'btn-success';

            switch ($term->status) {
                case 'Cancelled':
                    $isActive = 'btn-danger';
                    break;
                case 'Pending':
                    $isActive = 'btn-warning';
                    break;
                case 'Completed':
                    $isActive = 'btn-success';
                    break;
                default:
                    $isActive = 'btn-success';
                    break;
            }
            //'<span><a title="Change Status" onclick="return(confirm(\'Are you Sure ?\'))" href="'.site_url(CRM_VAR).'/order/change_status/'.$term->order_id.'" class="btn '.$isActive.' btn-xs change-status">'.$btntxt.'</a></span>';

            //$row[] = $term->status == 1?"Active":"Inactive";
            /*
            if($is_edit_action){
            $row[] = '<a href="'.site_url(CRM_VAR).'/order/edit/'.$term->order_id.'"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>&nbsp;<a href="'.site_url(CRM_VAR).'/order/view/'.$term->order_id.'"><button class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></button></a>';
            }else{
            $row[] = '<a href="'.site_url(CRM_VAR).'/order/view/'.$term->order_id.'"><button class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></button></a>';
            }
            $data[] = $row;
            }
             */
            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->orders->count_all(),
            "recordsFiltered" => $this->orders->count_filtered(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /* list of order */
    public function listOrder()
    {
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        //$this->data['content_category'] = $this->articles->get_attributes(array(30),TRUE);
        $this->data['suppliers'] = $this->user->get_many_by("user_role", "supplier");
        $this->data['drivers'] = $this->user->get_many_by("user_role", "driver");
        //$this->data['customers'] = $this->user->get_all();
        $this->data['customers']    = $this->user->get_many_by("user_active", "YES");
        $this->data['order_status'] = $this->uri->segment(4) ? $this->uri->segment(4) : "";
        $this->data['view']         = 'listOrder';
        $this->layout->admin($this->data);
    }
    /* list of order */

    /* ajax call for order table */

    public function ajax_order_list()
    {
        //list of all terminals for ajax datatables
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        $this->is_ajax();
        //$this->articles->belongs = $this->session->userdata('crm_user_id'); //Logged in user id
        $list = $this->orders->get_datatables();

        // $is_edit_action = $this->checkPermission('order_edit', false);
        $is_edit_action=true;
        $data           = array();
        $no             = $_POST['start'];
        foreach ($list as $term) {
            $no++;
            $row   = array();
            $row[] = '<a href="' . site_url(CRM_VAR) . '/order/view/' . $term->order_id . '">' . $term->order_id . '<a/>';
            $row[] = $term->trip_record_name;
            $car_val = $this->car_manager->get_by('id',$term->car_id);
            $row[] = $car_val->car_number."<br/>".$this->attributes->get_by('id',$car_val->model)->title;

            $row[] = $term->suplname;
            
            if($term->rent_type==36){
                if($term->airport_status=="pickup" || $term->airport_status=="dropoff"){
                    $row[] = "Airport - ".ucfirst($term->airport_status);
                }else if($term->airport_status=="highway"){
                    $row[] = $this->attributes->get_by('id',$term->rent_type)->title." - ".ucfirst($term->airport_status);
                }else{
                    $row[] = $this->attributes->get_by('id',$term->rent_type)->title." - ".ucfirst($term->daily_status);
                }
            }else{
                $row[] = $this->attributes->get_by('id',$term->rent_type)->title;
            }
        // } else {
            $row[] = $term->fullname;
            $row[] = $term->uphone;
            // }
            $order_history = json_decode($term->order_history);
            $currency      = strtoupper($order_history->currency);
            $row[]         = date("d-m-Y h:i A", strtotime($term->pickup_date));
            $row[]         = date("d-m-Y h:i A", strtotime($term->drop_date));
            $row[]         = $currency . number_format($term->booking_amount, 0, ".", ',');
            $isActive      = ($term->status == 2) ? 'btn-danger' : 'btn-success';

            switch ($term->status) {
                case 'Cancelled':
                    $isActive = 'btn-danger';
                    break;
                case 'Pending':
                    $isActive = 'btn-warning';
                    break;
                case 'Completed':
                    $isActive = 'btn-success';
                    break;
                default:
                    $isActive = 'btn-success';
                    break;
            }

            $btntxt = $term->status;
            $row[]  = $term->status; //'<span><a title="Change Status" onclick="return(confirm(\'Are you Sure ?\'))" href="'.site_url(CRM_VAR).'/order/change_status/'.$term->order_id.'" class="btn '.$isActive.' btn-xs change-status">'.$btntxt.'</a></span>';

            //$row[] = $term->status == 1?"Active":"Inactive";
            if ($is_edit_action) {
                $row[] = '<a href="' . site_url(CRM_VAR) . '/order/view/' . $term->order_id . '"><button class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></button></a> &nbsp;&nbsp;<button class="delete_booking btn btn-danger btn-sm change-status" data_status="deleted" order_id="' . $term->order_id . '"><i class="fa fa-remove"></i></button>';
            } else {
                $row[] = '<a href="' . site_url(CRM_VAR) . '/order/view/' . $term->order_id . '"><button class="btn btn-warning btn-sm"><i class="fa fa-eye"></i></button></a>';
            }
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->orders->count_all(),
            "recordsFiltered" => $this->orders->count_filtered(),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /* ajax call for order table*/

    /* view order */
    public function viewOrder($order_id)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $this->data['orderinfo']              = $orderinfo;
        $order_history                        = json_decode($orderinfo->order_history);
        $this->data['userinfo']               = $order_history->userInfo;
        $this->data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $this->data['agents']                  = $this->user->get_many_by("user_role", "agent");
        $this->data['allCarNames']            = $this->car_manager->get_all_carname();
        $this->data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['order_ratings']          = $this->ratings->get_order_ratings($order_id);
        $this->data['is_autorize_for_rating'] = $this->ratings->check_for_rating($order_id);
        $this->data['view']                   = 'viewOrder';
        
        
        $this->layout->admin($this->data);
    }
    /* view order */

    /* view order */
    public function getCars()
    {
        // $this->isAuthorized();
        $this->is_ajax();
        $supplier_id = $this->input->post('supplier_id');
        $cars        = $this->car_manager->get_many_by("user_id", $supplier_id);
        echo json_encode($cars);
    }
    public function getCarTags()
    {
        // $this->isAuthorized();
        $this->is_ajax();
        $car_id = $this->input->post('car_id');
        $cars   = $this->car_manager->get_cartag($car_id);
        if (!empty($cars)) {echo json_encode(array("data" => $cars, "status" => 1));} else {echo json_encode(array("status" => 0));}
    }
    public function getdestination()
    {
        // $this->isAuthorized();
        $this->is_ajax();
        $parent_id    = $this->input->post('parent_id');
        $destinations = $this->orders->get_locations($parent_id);
        if (!empty($destinations)) {echo json_encode(array("data" => $destinations, "status" => 1));} else {echo json_encode(array("status" => 0));}
    }

    // function toAscii($str, $replace=array(), $delimiter='-') {
    //     if( !empty($replace) ) {
    //     $str = str_replace((array)$replace, ' ', $str);
    //     }

    //     $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    //     $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    //     $clean = strtolower(trim($clean, '-'));
    //     $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    //     return $clean;
    // }

    public function driverInfo($drivername)
    {
        echo json_encode($this->user->get_by('user_lname', str_replace('%20', ' ', $drivername)));
    }
    public function editOrder($order_id)
    {
        // $this->checkPermission('order_edit');
        $this->data['allCarNames'] = $this->car_manager->get_all_carname();
        $this->data['drivers']     = $this->user->get_many_by('user_role', 'driver');
        $orderinfo                 = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $order_history                  = json_decode($orderinfo->order_history);
        $this->data['orderinfo']        = $orderinfo;
        $this->data['userinfo']         = $order_history->userInfo;
        $this->data['users']            = $this->user->get_many_by("user_role", "supplier");
        $this->data['agents']                  = $this->user->get_many_by("user_role", "agent");
        $this->data['cars']             = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['order_comment']    = $this->orders->get_order_comments($order_id);
        $this->data['pickup_locations'] = $this->orders->get_locations(70);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $validation_array = array(
                array(
                    'field' => 'pickup_date',
                    'label' => 'Pick Up Date',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'pickup_time',
                    'label' => 'Pick Up Time',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'pickup_location',
                    'label' => 'Pick Up Location',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'comment',
                    'label' => 'Update Comment',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'customer_name',
                    'label' => 'Customer Name',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'customer_phone_number',
                    'label' => 'Customer Phone Number',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'supplier_id',
                    'label' => 'Supplier',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'car_id',
                    'label' => 'Car Id',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'type_of_rent',
                    'label' => 'Type of rent',
                    'rules' => 'required',
                ),
                /*array(
                'field' => 'drop_date',
                'label' => 'Drop Date',
                'rules' => 'required'
                ),
                array(
                'field' => 'drop_time',
                'label' => 'Drop time',
                'rules' => 'required'
                ),
                array(
                'field' => 'drop_location',
                'label' => 'Drop Location',
                'rules' => 'required'
                ),*/
                array(
                    'field' => 'booking_date',
                    'label' => 'Booking Date',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'payment_method',
                    'label' => 'Payment Method',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'total_amount',
                    'label' => 'Total Amount',
                    'rules' => 'required',
                ),
            );
            $this->form_validation->set_rules($validation_array);
            if ($this->form_validation->run() == false) {
                /*$this->set_msg_flash(['msg'=>'Invalid Form Submittion','typ'=>2]);
                redirect(CRM_VAR.'/order/edit/'.$order_id);*/
                $this->set_msg_flash([
                    'msg' => 'Invalid Form Submittion!!',
                    'typ' => 3,
                ]);
            } else {
                $res = $this->update_order($order_id);
                if ($this->session->userdata('isOrderConfirmed') != $this->checkCustSuppConfirmed($order_id)) {
                    $emailParams = array('order_id' => $order_id, 'user_type' => 'admin', 'params' => array('emailType' => 'confirmed'));
                    $mailData    = $this->orders->modifyEmailData($emailParams);
                    $result      = $this->sendMail($mailData);
                }
                if ($res) {
                    // Upade article's information
                    $this->set_msg_flash(['msg' => 'Order Updated!!', 'typ' => 1]);
                    // redirect(CRM_VAR . '/order/list');
                    redirect(CRM_VAR . '/order/view/' . $order_id);
                } else {redirect(CRM_VAR . '/order/edit/' . $order_id);}
            }
        }
        $this->session->set_userdata('isOrderConfirmed', $this->checkCustSuppConfirmed($order_id));
        $this->data['view'] = 'editOrder';
        $this->layout->admin($this->data);
    }
    /* view order */

    private function update_order($order_id)
    {
        $orderinfo          = $this->orders->get_order($order_id);
        $supplier_id        = $this->input->post('supplier_id');
        $car_id             = $this->input->post('car_id');
        $car_tag            = $this->input->post('car_tag');
        $agent_id           = $this->input->post('agent_id');
        $driver_name        = $this->input->post('driver_name');
        $driver_phone       = $this->input->post('driver_phone');
        $type_of_rent       = $this->input->post('type_of_rent');
        $paid_to            = $this->input->post('paid_to');
        $customer_confirmed = $this->input->post('customer_confirmed');
        $pickup_date        = $this->input->post('pickup_date');
        $pickup_time        = $this->input->post('pickup_time');
        $drop_date          = $this->input->post('drop_date');
        $drop_time          = $this->input->post('drop_time');
        $pickup_location    = $this->input->post('pickup_location');
        $drop_location      = $this->input->post('drop_location');
        $booking_date       = $this->input->post('booking_date');
        $payment_method     = $this->input->post('payment_method');
        $total_amount       = $this->input->post('total_amount');
        $vendor_fees        = $this->input->post('vendor_fees');
        $status             = $this->input->post('status');

        $no_of_zone         = $this->input->post('no_of_zone');
        $zone_fees          = $this->input->post('zone_fees');
        
        $supplier_confirmed = $this->input->post('supplier_confirmed');
        $driver_ot_hours    = $this->input->post('driver_ot_hours');
        $agent_fees         = $this->input->post('agent_fees');
        $daily_status       = $this->input->post('daily_status');
        $driver_salary      = $this->input->post('driver_salary');
        $customer_name         = $this->input->post('customer_name');
        $customer_phone_number = $this->input->post('customer_phone_number');
        $customer_email        = $this->input->post('customer_email');
        $trip_record_name   = $this->input->post('trip_record_name');
        
        $vendor_payment_status      = $this->input->post('vendor_payment_status');
        $customer_payment_status    = $this->input->post('customer_payment_status');

        $trip_type = $this->input->post('trip_type');
        $driver_from = $this->input->post('driver_from');
        $driver_ot_fees     = $this->input->post('driver_ot_fees');

        $comment = $this->input->post('comment');
        if ($status == 'Completed' && $payment_method == 'COD') {
            $payment_status = 'Completed';
        }
        // if($type_of_rent=="pickup" || $type_of_rent=="dropoff"){
        //     $airport_status=$type_of_rent;
        //     $type_of_rent=36;
        // }

        $order_data = array(
            'car_id'          => $car_id,
            'pickup_date'     => date("Y-m-d H:i:s", strtotime($pickup_date . " " . $pickup_time)),
            'pickup_location' => $pickup_location,
            'booking_amount'  => $total_amount,
            'status'          => $status,
            'booking_date'    => $booking_date,
            'vendor_fees'     => $vendor_fees,
            'ot_hours'        => $driver_ot_hours,
            'rent_type'       => $type_of_rent,
            'agent_fees'      => $agent_fees,
            'daily_status'    => $daily_status,
            'driver_salary'   => $driver_salary,
            'airport_status'    => $trip_type,
            'trip_record_name'  => $trip_record_name,
            'driver_from'       => $driver_from,
            'driver_ot_fees'    => $driver_ot_fees,
            'no_of_zone'        => $no_of_zone,
            'zone_fees'         => $zone_fees,
            'vendor_payment_status'     => $vendor_payment_status,
            'customer_payment_status'   => $customer_payment_status,
        );
        // print "<pre/>";
        // print_r($order_data);
        // exit();
        // if ($type_of_rent == 36) {
            $order_data['drop_date'] = date("Y-m-d H:i:s", strtotime($drop_date . " " . $drop_time));
            $order_history           = json_decode($orderinfo->order_history)->searchData;
            if ($order_history->incity == 'highway') {
                $order_data['drop_location'] = $drop_location;
            }
            
        // }

        $order_detail_data = array(
            'supplier_id'        => $supplier_id,
            'car_tag'            => $car_tag,
            'driver_name'        => $driver_name,
            'driver_phone'       => $driver_phone,
            'agent_id'           => $agent_id,
            'type_of_rent'       => $type_of_rent,
            'paid_to'            => $paid_to,
            'customer_confirmed' => $customer_confirmed,
            'supplier_confirmed' => $supplier_confirmed,
            'name'               => $customer_name,
            'usr_email'          => $customer_email,
            'phone'              => $customer_phone_number
        );
        $payment_data = array(
            'amount'         => $total_amount,
            'payment_method' => $payment_method,
        );
        if (isset($payment_status) && $payment_status != '') {
            $payment_data['status'] = $payment_status;
        }
        $comment_data = array(
            'order_id' => $order_id,
            'user_id'  => $this->session->userdata('crm_user_id'),
            'comment'  => $comment,
        );
        if ($this->orders->update($order_id, $order_data)) {
            $this->db->insert("cr_order_comment", $comment_data);
            $a = $this->db->update("cr_order_detail", $order_detail_data, array('order_id' => $order_id));
            $b = $this->db->update("cr_payment_info", $payment_data, array('order_id' => $order_id));
            if ($a && $b) {
                //
                return true;
            } else {return false;}
        }
    }
    /* Add or Edit Article */

    /* Cancle Order */

    public function cancleOrder($order_id)
    {
        // $this->checkPermission('order_edit');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->form_validation->set_rules("cancleReason", "Cancle Reason", "required");
            if ($this->form_validation->run() == false) {
                $this->set_msg_flash(['msg' => 'Reason for Cancellation is required !!!', 'typ' => 3]);
                redirect(CRM_VAR . '/order/view/' . $order_id);
            } else {
                if ($this->orders->update($order_id, array("status" => "Cancelled"))) {

                    $comment_data = array(
                        'order_id' => $order_id,
                        'user_id'  => $this->session->userdata('crm_user_id'),
                        'comment'  => $this->input->post('cancleReason'),
                    );
                    $this->db->insert("cr_order_comment", $comment_data);

                    $orderinfo = $this->orders->get_by("order_id", $order_id);
                    $userinfo  = $this->user->get_by("id", $orderinfo->user_id);
                    //Cancelation Emall to customer
                    $emailParams = array('order_id' => $order_id, 'user_type' => 'customer', 'params' => array('emailType' => 'cancel'));
                    $mailData    = $this->orders->modifyEmailData($emailParams);
                    $result      = $this->sendMail($mailData);
                    //Cancelation Emall to Admin
                    $emailParams = array('order_id' => $order_id, 'user_type' => 'admin', 'params' => array('emailType' => 'cancel'));
                    $mailData    = $this->orders->modifyEmailData($emailParams);
                    $result      = $this->sendMail($mailData);

                    $this->set_msg_flash(['msg' => 'Order Cancelled!!', 'typ' => 1]);
                    redirect(CRM_VAR . '/order/list');
                } else {redirect(CRM_VAR . '/order/view/' . $order_id);}
            }
        } else {
            $this->set_msg_flash(['msg' => 'Direct Access not allowed !!!', 'typ' => 3]);
            redirect(CRM_VAR . '/order/view/' . $order_id);
        }
    }

    public function send_confirmation_mail()
    {
        $to       = $this->input->post('to');
        $order_id = $this->input->post('order_id');
        if ($to == 'supplier') {
            $emailParams = array('order_id' => $order_id, 'user_type' => 'supplier', 'params' => array('emailType' => 'confirmed'));
        } else if ($to == 'customer') {
            $emailParams = array('order_id' => $order_id, 'user_type' => 'customer', 'params' => array('emailType' => 'confirmed'));
        }
        $mailData = $this->orders->modifyEmailData($emailParams);
        $result   = $this->sendMail($mailData);
        echo "Mail Sent!!!";
    }
    public function checkCustSuppConfirmed($orderid)
    {
        //$orderInfo = $this->db->get("id" , $orderid);
        $this->db->select(array('cr_order_detail.*'));
        $this->db->where(array('cr_order_detail.order_id' => $orderid, 'cr_order_detail.supplier_confirmed' => '1', 'cr_order_detail.customer_confirmed' => '1', 'cr_orders.status' => 'Confirmed'));
        $q = $this->db->from('cr_order_detail');
        $this->db->join('`cr_orders`', '`cr_orders`.`order_id` = `cr_order_detail`.`order_id`', 'inner');
        $query     = $this->db->get();
        $orderInfo = $query->row();
        if (!empty($orderInfo)) {
            return '1';
        } else {
            return '0';
        }
    }
    public function suppliers()
    {
        if (!in_array($this->session->userdata('cr_user_role'), array('admin'))) {
            redirect(site_url(CRM_VAR));
        }
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        if (isset($_POST['active_supplier']) && $_POST['active_supplier'] > 0) {
            $this->session->set_userdata('active_supplier', $_POST['active_supplier']);
            redirect(site_url(CRM_VAR . '/booking-calendar'));die('Please try again');
        }
        $suppliers                     = $this->orders->getSuppliers();
        $this->data['active_supplier'] = '';
        $this->data['suppliers']       = $suppliers;
        $this->data['view']            = 'choose_supplier';
        $this->layout->admin($this->data);
    }
    public function booking_calendar()
    {
        if (!in_array($this->session->userdata('cr_user_role'), array('supplier', 'admin'))) {
            $active_supplier = $this->session->userdata('active_supplier');
            if ($active_supplier < 1) {
                redirect(site_url(CRM_VAR));
            }
        }
        if ($this->session->userdata('cr_user_role') == 'supplier') {
            $supplier_id = $this->session->userdata('crm_user_id');
        } else {
            $supplier_id                = $this->session->userdata('active_supplier');
            $supplierInfo               = $this->user->get_by("id", $supplier_id);
            $suplier_name               = ($supplierInfo->user_company != '') ? $supplierInfo->user_company : $supplierInfo->user_lname;
            $this->data['suplier_name'] = $suplier_name;
        }

        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        $supplier_cars = $this->orders->getSupplierCars($supplier_id);
        if ($this->input->post('active_car', true)) {
            $active_car = $this->input->post('active_car', true);
            //echo $active_car;die("dgfd");
        } else {
            if (count($supplier_cars) > 0) {
                $active_car = current($supplier_cars);
            } else {
                $active_car = '';
            }

        }

        //print_r($supplier_cars);die;
        $this->data['active_car']    = $active_car;
        $this->data['supplier_cars'] = $supplier_cars;
        $this->data['view']          = 'booking_calendar';
        $this->layout->admin($this->data);
    }
    public function booking_calendar_ajax()
    {
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        $bookingCalendar = $this->orders->get_supplier_car_bookings();
        $bookings        = $this->orders->get_car_bookings();
        $calendors       = array();
        foreach ($bookingCalendar as $val) {
            $manageBtn   = '<div class="manage-btn"><button onclick="return update_booking(' . $val->id . ');" type="button" class="btn btn-info">Manage</button></div><br/>';
            $calendors[] = array(
                'id'        => $val->id,
                'name'      => $val->booking_note . $manageBtn,
                'startdate' => $val->from_date,
                'enddate'   => $val->to_date,
                'starttime' => date("H:i a", strtotime($val->from_date)),
                'endtime'   => date("H:i a", strtotime($val->to_date)),
                'color'     => '#008EFE',
                'url'       => '',
            );
        }
        if (!empty($bookings)) {
            foreach ($bookings as $val) {
                $calendors[] = array(
                    'id'        => $val->order_id,
                    'name'      => "#$val->order_id Sonic Star Booking",
                    'startdate' => $val->pickup_date,
                    'enddate'   => $val->drop_date,
                    'starttime' => date("H:i a", strtotime($val->pickup_date)),
                    'endtime'   => date("H:i a", strtotime($val->drop_date)),
                    'color'     => '#fe0000',
                    'url'       => site_url(CRM_VAR . '/order/view/' . $val->order_id),
                );
            }
        }
        //print_r($bookingCalendar);
        $id                = 9;
        $output['monthly'] = $calendors;
//        $output['monthly'] = array(
        //                    array(
        //                        'id'=> $id,
        //                        'name'=>'Car Booked out of Sonic Star<div class="manage-btn"><button onclick="return update_booking('.$id.');" type="button" class="btn btn-info">Manage</button></div>',
        //                        'startdate' => '2017-10-15',
        //                        'enddate' => '2017-10-15',
        //                        'starttime' => '8:00am',
        //                        'endtime' => '2:00pm',
        //                        'color' => '#008EFE',
        //                        'url' => '',
        //                        )
        //        );
        echo json_encode($output);
    }
    public function delete_supplier_booking()
    {
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        $bookingInfo = $this->orders->delete_supplier_booking();
        echo json_encode($bookingInfo);die;
    }
    public function delete_order()
    {
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        $bookingInfo = $this->orders->delete_order();
        echo json_encode($bookingInfo);die;
    }
    public function add_supplier_booking()
    {
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        $booking_id = $this->orders->add_supplier_booking();
        echo json_encode($booking_id);die;
    }

    public function duplicate($order_id){
        $this->orders->duplicate($order_id);
        redirect(site_url() . CRM_VAR . '/order/list');
    }
    public function get_supplier_booking()
    {
        // // $this->isAuthorized();
        // $this->checkPermission('order_list');
        $bookingInfo = $this->orders->get_supplier_booking();
        if (!empty($bookingInfo)) {
            foreach ($bookingInfo[0] as $k => $v) {
                if ($k == 'from_date') {
                    $newdata[$k]          = date("m/d/Y", strtotime($v));
                    $newdata['from_time'] = date("h:i A", strtotime($v));
                } else if ($k == 'to_date') {
                    $newdata[$k]        = date("m/d/Y", strtotime($v));
                    $newdata['to_time'] = date("h:i A", strtotime($v));
                } else {
                    $newdata[$k] = $v;
                }

            }
            echo json_encode($newdata);die;
        }
    }

    /* view trip expenses */
    public function tripExpenses($order_id, $trip_expense_id = null)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $this->data['orderinfo']              = $orderinfo;
        $order_history                        = json_decode($orderinfo->order_history);
        $this->data['userinfo']               = $order_history->userInfo;
        $this->data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $this->data['agents']                 = $this->user->get_many_by("user_role", "agent");
        $this->data['allCarNames']            = $this->car_manager->get_all_carname();
        $this->data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['order_ratings']          = $this->ratings->get_order_ratings($order_id);
        $this->data['is_autorize_for_rating'] = $this->ratings->check_for_rating($order_id);
        $this->data['view']                   = 'tripExpenses';
        $this->data['driver_ot_fees']         = $this->attributes->get_by('title', 'Driver OT Fees');
        $this->data['driver_fees']            = $this->attributes->get_by('title', 'Driver Fees');
        $this->data['driver_percentage']      = $this->attributes->get_by('title', 'Driver Percentage');
        $this->data['btn']                    = 'Save';
        $this->data['expense_list']           = $this->expense->get_by_many_with_user('order_id', $order_id);
        $this->data['onclick']                = '';
        $this->data['expense_id']             = '';
        $this->data['fuel_expenses']          = $this->car_oilslip->get_many_by("order_id", $order_id);
        $this->data['expenses']               = $this->transations->get_many_by("linked_id", $order_id);
        if (!empty($trip_expense_id)) {
            $findRow = $this->expense->get_by('id', $trip_expense_id);
            if (!empty($findRow)) {
                $this->data['expense_id'] = $trip_expense_id;
                $this->data['btn']        = 'Update Expense';
                // $this->data['onclick']              = 'onclick="return confirm(\'Do you want to save updated?\')"';
                $this->data['trip_expense_detail'] = $findRow;
            }
        } else {
            $findRow = $this->expense->get_latest('order_id', $order_id);
            if (!empty($findRow)) {
                $this->data['btn'] = 'Save';
                // $this->data['onclick']              = '';
                $this->data['trip_expense_detail'] = $findRow;
            }
        }
        $this->layout->admin($this->data);
    }
    public function tripExpenseStore($order_id, $trip_expense_id = null)
    {

        $postData['order_id']   = $this->input->post('order_id');
        $postData['created_at'] = time();
        $postData['created_by'] = $this->session->userdata("crm_user_id");

        if (!empty($this->input->post('vendor_fees'))) {
            $postData['vendor_fees']    = $this->input->post('vendor_fees');
            $postData['vendor_paydate'] = $this->input->post('vendor_paydate');
            $postData['profit']         = $this->input->post('vendor_profit');
            $postData['pay_to']         = $this->input->post('pay_to');

            // $postData['is_latest'] = ($status=="complete")? 1: 0;
            // $status
        } else {
            $postData['vendor_paydate']         = $this->input->post('paydate');
            $postData['driver_ot_hour']         = $this->input->post('ot_hour');
            $postData['driver_ot_fees']         = $this->input->post('driver_ot_fees');
            $postData['driver_fees']            = $this->input->post('driver_fees');
            $postData['calc_driver_ot_fees']    = $this->input->post('calc_ot_driver_fees');
            $postData['calc_driver_fees']       = $this->input->post('calc_driver_fees');
            $postData['calc_driver_percentage'] = $this->input->post('calc_driver_percentage');

            $postData['fuel_slip']     = $this->input->post('fuel_slip');
            $postData['fuel_cash']     = $this->input->post('fuel_cash');
            $postData['toll_expense']  = $this->input->post('toll_expense');
            $postData['other_expense'] = $this->input->post('other_expense');
            $postData['comments']      = $this->input->post('comment');

            $postData['profit']     = $this->input->post('profit');
            $postData['net_income'] = $this->input->post('net_income');

        }
        $insert_id = "";
        if (!empty($trip_expense_id)) {
            $findRow = $this->expense->get_by('id', $trip_expense_id);
            $this->expense->update($trip_expense_id, $postData);
        } else {

            $this->expense->cus_update_by('order_id', $order_id, 'pay_to', $postData['pay_to'], ['is_latest' => 0]);
            $postData['is_latest'] = 1;
            $insert_id             = $this->expense->insert($postData);
        }

        $status = $this->input->post('status_vendor');

        if (!empty($this->input->post('vendor_fees'))) {
            $account        = $this->account->get_by('account_id', 35);
            $transationData = array(
                'category_id'     => $this->attributes->get_by('title', 'Vendor Payment')->id,
                'cash_type'       => $account->account_type,
                'account_id'      => $account->account_id,
                'amount'          => $this->input->post('vendor_fees'),
                'account_balance' => $account->account_price,
                'final_balance'   => $account->account_price - $this->input->post('vendor_fees'),
                'linked_id'       => $order_id,
                'description'     => "Vendor Payment (Order ID:" . $order_id . ")",
                'payment_type'    => $this->attributes->get_by('title', 'Withdraw')->id,
                'created_at'      => time(),
                'created_by'      => $this->session->userdata("crm_user_id"),
                'payment_person'  => $this->input->post("vendor_name"),
                'transaction_date'  => strtotime($this->input->post('vendor_paydate')),
                
            );
            // print "<pre/>";
            // print_r($_POST);

            $main_acc_final_amount['account_price'] = $account->account_price - $this->input->post('vendor_fees');

            $this->transations->insert($transationData);
            $this->account->update($account->account_id, $main_acc_final_amount);

        }
        // print "<pre/>";
        // print_r($postData);
        if(!empty($this->input->post("redirect"))){
            redirect(site_url() . CRM_VAR . '/order/'.$this->input->post("redirect").'/' . $order_id);
        }else{
            redirect(site_url() . CRM_VAR . '/order/expenses/' . $order_id);
        }
    }

    public function setlatest($order_id, $trip_expense_id)
    {
        // $this->expense->cus_update_by('order_id', $order_id, ['is_latest' => 0]);

        // $this->expense->update($trip_expense_id, ['is_latest' => 1]);
        // redirect(site_url() . CRM_VAR . '/order/expenses/' . $order_id);
    }

    public function payment($order_id)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $this->data['orderinfo'] = $orderinfo;
        $this->data['accounts']  = $this->account->get_all();
        $order_history           = json_decode($orderinfo->order_history);
        $this->data['userinfo']  = $order_history->userInfo;
        $this->data['users']     = $this->user->get_many_by("user_role", "supplier");

        $this->data['allCarNames']            = $this->car_manager->get_all_carname();
        $this->data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['order_ratings']          = $this->ratings->get_order_ratings($order_id);
        $this->data['is_autorize_for_rating'] = $this->ratings->check_for_rating($order_id);
        $this->data['diff']                   = $this->attributes->get_by('title', 'diff');
        $paid_amount_list                     = $this->payment->get_by_many_with_user('cr_order_id', $order_id);
        $this->data['paid_list']              = $paid_amount_list;
        $paid_total_amount                    = 0;

        foreach ($paid_amount_list as $key => $value) {
            $paid_total_amount += $value->total_price;
        }

        $this->data['paid_total_amount'] = $paid_total_amount;
        $this->data['view']              = 'payment'; //Load all user's list page

        $this->layout->admin($this->data);
    }

    public function accountinfo($id)
    {
        echo json_encode($this->account->get_by('account_id', $id));
    }
    public function store_payment($order_id)
    {

        $paymentData['account_id']  = $this->input->post('account');
        $paymentData['trasfer']     = $this->input->post('transfer_type');
        $paymentData['cr_order_id'] = $this->input->post('order_id');
        $paymentData['pay_price']   = $this->input->post('pm_mmk');
        $paymentData['pay_dollar']  = $this->input->post('pm_usd');
        $paymentData['one_dollar']  = $this->input->post('exchange_rate');
        $paymentData['date']        = strtotime($this->input->post('payment_date'));
        $paymentData['total_price'] = ($this->input->post('pm_usd') * $this->input->post('exchange_rate')) + $this->input->post('pm_mmk');

        $paymentData['created_at'] = time();
        $paymentData['created_by'] = $this->session->userdata("crm_user_id");

        $account['account_price']  = 0;
        $account['account_dollar'] = 0;

        $acc = $this->account->get_by('account_id', $this->input->post('account'));
        // $cashAccount=$this->account->get_by('account_id',31);

        $payment['category_id'] = $this->attributes->get_by('title', 'Payment')->id;
        if ($acc->account_type == 1) {
            $payment['cash_type']   = 1;
            $payment['account_balance'] = $acc->account_price;
        } else {
            $payment['cash_type']   = 2;
            $payment['account_balance'] = $acc->account_dollar;
        }
        // $payment['account_balance']=$acc->account_price;
        $payment['linked_id']        = $this->input->post('order_id');
        $payment['description']      = "Payment From Customer (Order ID: " . $order_id . ")";
        $payment['payment_type']     = $this->attributes->get_by('title', 'Deposit')->id;
        $payment['created_at']       = time();
        $payment['created_by']       = $this->session->userdata("crm_user_id");
        $payment['account_id']       = $acc->account_id;
        $payment['payment_person']   = $this->input->post('customer_name');
        $payment['transaction_date'] = strtotime($this->input->post('payment_date'));
        // print "<pre/>";
        // print_r($paymentData);
        // exit();
        if ($acc->account_type == 1) {
            $account['account_price']          = $this->input->post('pm_mmk') + $acc->account_price;
            $paymentData['account_amount_mmk'] = $acc->account_price;
            $payment['amount']                 = $this->input->post('pm_mmk');
        } else if ($acc->account_type == 2) {
            $account['account_dollar']         = $this->input->post('pm_usd') + $acc->account_dollar;
            $paymentData['account_amount_usd'] = $acc->account_dollar;
            $payment['amount']                 = $this->input->post('pm_usd');
        } else {
            $account['account_price']          = $this->input->post('pm_mmk') + $acc->account_price;
            $account['account_dollar']         = $this->input->post('pm_usd') + $acc->account_dollar;
            $paymentData['account_amount_mmk'] = $acc->account_price;
            $paymentData['account_amount_usd'] = $acc->account_dollar;
        }
        if (!empty($this->input->post('complete_order'))) {
            $orderdata['customer_payment_status'] = "complete";
            $this->orders->update($order_id, $orderdata);
            $paymentData['diff'] = $this->input->post('diff');
        }else{
            $payment['final_balance'] = $payment['amount'] + $payment['account_balance'];
            // print "<pre/>";
            // print_r($paymentData);
            // print_r($payment);

            $this->transations->insert($payment);
            $this->payment->insert($paymentData);
            $this->account->update($acc->account_id, $account);
            
        }

        redirect(site_url() . CRM_VAR . '/order/payment/' . $order_id);

        // $this->layout->admin($this->data);
    }

    public function advance($order_id)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $this->data['orderinfo']              = $orderinfo;
        $this->data['accounts']               = $this->account->get_all();
        $order_history                        = json_decode($orderinfo->order_history);
        $this->data['userinfo']               = $order_history->userInfo;
        $this->data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $this->data['allCarNames']            = $this->car_manager->get_all_carname();
        $this->data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['order_ratings']          = $this->ratings->get_order_ratings($order_id);
        $this->data['is_autorize_for_rating'] = $this->ratings->check_for_rating($order_id);
        $this->data['withdraw']               = $this->attributes->get_by('title', 'Withdraw');
        $this->data['deposit']                = $this->attributes->get_by('title', 'Deposit');
        $account_detail                       = $this->account->get_by('account_id', 31);
        $this->data['payment_type_withdraw']  = $this->attributes->get_by('title', 'Withdraw')->id;
        $this->data['payment_type_deposit']   = $this->attributes->get_by('title', 'Deposit')->id;
        $this->data['transactions']           = $this->transations->get_by_category($order_id, '4420');
        $this->data['account_detail']         = $account_detail;
        $paid_amount_list                     = $this->payment->get_by_many_with_user('cr_order_id', $order_id);
        $this->data['paid_list']              = $paid_amount_list;
        $paid_total_amount                    = 0;

        foreach ($paid_amount_list as $key => $value) {
            $paid_total_amount += $value->total_price;
        }

        $this->data['paid_total_amount'] = $paid_total_amount;
        $this->data['view']              = 'advance'; //Load all user's list page
        // print "<pre/>";
        // print_r($this->data['transactions']);
        $this->layout->admin($this->data);
    }
    public function store_advance($order_id)
    {

        $cashAccount = $this->account->get_by('account_id',35);

        $advance['category_id']      = 4420;
        $advance['cash_type']        = 1;
        $advance['amount']           = $this->input->post('amount');
        $advance['account_balance']  = $cashAccount->account_price;
        $advance['linked_id']        = $this->input->post('order_id');
        $advance['description']      = $this->input->post('description');
        $advance['payment_type']     = $this->input->post('payment_type');
        $advance['created_at']       = time();
        $advance['created_by']       = $this->session->userdata("crm_user_id");
        $advance['account_id']       = $cashAccount->account_id;
        $advance['payment_person']   = $this->input->post('driver');
        $advance['transaction_date'] = strtotime($this->input->post('transation_date'));

        $cashupdate = array();

        if ($advance['payment_type'] == 4416) {
            $cashupdate['account_price'] = $cashAccount->account_price - $this->input->post('amount');
            // $cashupdate['account_price'] = $cashAccount->account_price;
        } else if ($advance['payment_type'] == 4417) {
            $cashupdate['account_price'] = $cashAccount->account_price + $this->input->post('amount');
        }
        $advance['final_balance'] = $cashupdate['account_price'];

        // redirect(site_url() . CRM_VAR . '/order/payment/' . $order_id);
        // print "<pre/>";
        // print_r($advance);
        // print_r($cashAccount);
        // print_r($cashupdate);
        $this->account->update($cashAccount->account_id, $cashupdate);
        $this->transations->insert($advance);

        redirect(site_url() . CRM_VAR . '/order/advance/' . $order_id);
    }
    public function fuelExpenses($order_id)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Order', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }

        $this->data['fuel_recs'] = $this->car_oilslip->get_many_by("order_id", $order_id);
        $this->data['orderinfo'] = $orderinfo;
        // $this->data['accounts']               = $this->account->get_all();
        // $order_history                        = json_decode($orderinfo->order_history);
        // $this->data['userinfo']               = $order_history->userInfo;
        // $this->data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $this->data['allCarNames'] = $this->car_manager->get_all_carname();
        // $this->data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['gas_tations']           = $this->attributes->get_many_by_order('parent_id', 4430);
        $account_detail                      = $this->account->get_by('account_id', 31);
        $this->data['payment_type_withdraw'] = $this->attributes->get_by('title', 'Withdraw')->id;
        $this->data['payment_type_deposit']  = $this->attributes->get_by('title', 'Deposit')->id;
        $this->data['transactions']          = $this->transations->get_many_by('linked_id', $order_id);
        $this->data['account_detail']        = $account_detail;
        $paid_amount_list                    = $this->payment->get_by_many_with_user('cr_order_id', $order_id);
        $this->data['paid_list']             = $paid_amount_list;
        $paid_total_amount                   = 0;

        foreach ($paid_amount_list as $key => $value) {
            $paid_total_amount += $value->total_price;
        }

        $this->data['paid_total_amount'] = $paid_total_amount;
        $this->data['view']              = 'fuel_expense'; //Load all user's list page

        $this->layout->admin($this->data);
    }

    public function store_fuelExpenses($order_id)
    {
        $fuelexpense_data['order_id']      = $this->input->post('order_id');
        $fuelexpense_data['operator_name'] = $this->input->post('driver');
        $fuelexpense_data['car_id']        = $this->input->post('car_id');
        $fuelexpense_data['shop_name']     = $this->input->post('shop_name');
        $fuelexpense_data['purchased_by']  = $this->input->post('purchase_by');
        $fuelexpense_data['slip_no']       = $this->input->post('slip_no');
        $fuelexpense_data['liter']         = $this->input->post('liter');
        $fuelexpense_data['amount']        = $this->input->post('amount');
        $fuelexpense_data['date']          = strtotime($this->input->post('date'));
        $fuelexpense_data['comments']      = $this->input->post('comment');
        $fuelexpense_data['status']        = "complete";
        $fuelexpense_data['type_of_use']   = "order";
        $fuelexpense_data['description']   = "Order: " . $this->input->post('order_id') . ", Gas Station: " . $this->input->post('shop_name') . ", Driver: " . $this->input->post('driver');

        if ($fuelexpense_data['purchased_by'] != "cash") {
            $fuelexpense_data['status'] = "pending";
        }
        $os_insert_id = $this->car_oilslip->insert($fuelexpense_data);
        if ($fuelexpense_data['status'] == "complete") {

            $cashAccount = $this->account->get_by('account_id', 35);

            $fuelexpense_tran['category_id']      = $this->attributes->get_by('title', 'Fuel Expense')->id;
            $fuelexpense_tran['cash_type']        = 1;
            $fuelexpense_tran['amount']           = $this->input->post('amount');
            $fuelexpense_tran['account_balance']  = $cashAccount->account_price;
            $fuelexpense_tran['linked_id']        = $os_insert_id;
            $fuelexpense_tran['created_at']       = time();
            $fuelexpense_tran['created_by']       = $this->session->userdata("crm_user_id");
            $fuelexpense_tran['account_id']       = $cashAccount->account_id;
            $fuelexpense_tran['payment_person']   = $this->input->post('driver');
            $fuelexpense_tran['transaction_date'] = strtotime($this->input->post('date'));
            $fuelexpense_tran['payment_type']     = 4416;
            $fuelexpense_tran['description']      = "Order: " . $this->input->post('order_id') . ", Gas Station: " . $this->input->post('shop_name') . ", Driver: " . $this->input->post('driver');

            $cashupdate['account_price']       = $cashAccount->account_price - $this->input->post('amount');
            $fuelexpense_tran['final_balance'] = $cashupdate['account_price'];
            $this->transations->insert($fuelexpense_tran);
            $this->account->update($cashAccount->account_id, $cashupdate);
        }

        redirect(site_url() . CRM_VAR . '/order/fuel/' . $this->input->post('order_id'));
    }

    public function tollnotherExpenses($order_id)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Order', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }

        $this->data['expenses']  = $this->transations->get_many_by("linked_id", $order_id);
        $this->data['orderinfo'] = $orderinfo;
        // $this->data['accounts']               = $this->account->get_all();
        // $order_history                        = json_decode($orderinfo->order_history);
        // $this->data['userinfo']               = $order_history->userInfo;
        // $this->data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $this->data['allCarNames'] = $this->car_manager->get_all_carname();

        // $this->data['gas_tations'] = $this->attributes->get_many_by_order('parent_id', 4430);
        // $account_detail                       = $this->account->get_by('account_id', 31);
        $this->data['toll_val']  = $this->attributes->get_by('id', 4451)->title;
        $this->data['other_val'] = $this->attributes->get_by('id', 4452)->title;
        // $this->data['transactions']           = $this->transations->get_many_by('linked_id', $order_id);
        // $this->data['account_detail'] = $account_detail;
        $paid_amount_list        = $this->payment->get_by_many_with_user('cr_order_id', $order_id);
        $this->data['paid_list'] = $paid_amount_list;
        $paid_total_amount       = 0;

        foreach ($paid_amount_list as $key => $value) {
            $paid_total_amount += $value->total_price;
        }

        $this->data['paid_total_amount'] = $paid_total_amount;
        $this->data['view']              = 'tollnother_expense'; //Load all user's list page

        $this->layout->admin($this->data);
    }

    public function store_tollnotherExpenses($order_id)
    {
        $cashAccount = $this->account->get_by('account_id', 35);

        $tollnother_tran['category_id']      = $this->input->post('category');
        $tollnother_tran['linked_id']        = $this->input->post('order_id');
        $tollnother_tran['payment_person']   = $this->input->post('driver');
        $tollnother_tran['payment_type']     = 4416;
        $tollnother_tran['amount']           = $this->input->post('amount');
        $tollnother_tran['transaction_date'] = strtotime($this->input->post('date'));
        $tollnother_tran['cash_type']        = 1;
        $tollnother_tran['account_id']       = $cashAccount->account_id;
        $tollnother_tran['account_balance']  = $cashAccount->account_price;
        $tollnother_tran['created_at']       = time();
        $tollnother_tran['created_by']       = $this->session->userdata("crm_user_id");
        $tollnother_tran['description']      = $this->input->post('comment');

        $cashupdate['account_price']      = $cashAccount->account_price - $this->input->post('amount');
        $tollnother_tran['final_balance'] = $cashupdate['account_price'];
        $this->transations->insert($tollnother_tran);
        $this->account->update($cashAccount->account_id, $cashupdate);
        redirect(site_url() . CRM_VAR . '/order/tollnother/' . $this->input->post('order_id'));
    }

    public function vendorFees($order_id)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $this->data['orderinfo']              = $orderinfo;
        $order_history                        = json_decode($orderinfo->order_history);
        $this->data['userinfo']               = $order_history->userInfo;
        $this->data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $this->data['agents']                 = $this->user->get_many_by("user_role", "agent");
        $this->data['allCarNames']            = $this->car_manager->get_all_carname();
        $this->data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['order_ratings']          = $this->ratings->get_order_ratings($order_id);
        $this->data['is_autorize_for_rating'] = $this->ratings->check_for_rating($order_id);
        $this->data['view']                   = 'vendor_fees';
        $this->data['driver_ot_fees']         = $this->attributes->get_by('title', 'Driver OT Fees');
        $this->data['driver_fees']            = $this->attributes->get_by('title', 'Driver Fees');
        $this->data['driver_percentage']      = $this->attributes->get_by('title', 'Driver Percentage');
        $this->data['btn']                    = 'Save';
        $this->data['expense_list']           = $this->expense->get_by_many_with_user('order_id', $order_id);
        $this->data['onclick']                = '';
        $this->data['expense_id']             = '';
        $this->data['fuel_expenses']          = $this->car_oilslip->get_many_by("order_id", $order_id);
        $this->data['expenses']               = $this->transations->get_many_by("linked_id", $order_id);
        if (!empty($trip_expense_id)) {
            $findRow = $this->expense->get_by('id', $trip_expense_id);
            if (!empty($findRow)) {
                $this->data['expense_id'] = $trip_expense_id;
                $this->data['btn']        = 'Update Expense';
                // $this->data['onclick']              = 'onclick="return confirm(\'Do you want to save updated?\')"';
                $this->data['trip_expense_detail'] = $findRow;
            }
        } else {
            // $findRow = $this->expense->get_latest('order_id', $order_id);
            $findRow = $this->expense->get_latest_by('order_id', $order_id, 'is_latest', 1, "vendor");

            if (!empty($findRow)) {
                $this->data['btn'] = 'Save';
                // $this->data['onclick']              = '';
                $this->data['trip_expense_detail'] = $findRow;
            }
        }
        $this->layout->admin($this->data);
    }
    public function paymentComplete($order_id)
    {
        if ($this->input->post('pay_to') == "vendor") {
            $orderdata['vendor_payment_status'] = "complete";
            $this->orders->update($order_id, $orderdata);
            redirect(site_url() . CRM_VAR . '/order/vendor_fees/' . $order_id);
        }

        if ($this->input->post('pay_to') == "agent") {
            $orderdata['agent_payment_status'] = "complete";

            $this->orders->update($order_id, $orderdata);
            redirect(site_url() . CRM_VAR . '/order/agent_fees/' . $order_id);
        }
        // $findRow = $this->expense->get_latest('order_id', $order_id);
        // $this->expense->cus_update_by('id', $this->input->post('expense_id'),'pay_to', $this->input->post('pay_to'), ['status' => 'complete']);
        // print "<pre/>";
        // print_r($findRow);
        // $postData['order_id']   = $this->input->post('order_id');
        // $postData['created_at'] = time();
        // $postData['created_by'] = $this->session->userdata("crm_user_id");

        // if (!empty($this->input->post('vendor_fees'))) {
        //     $postData['vendor_fees']    = $this->input->post('vendor_fees');
        //     $postData['vendor_paydate'] = $this->input->post('vendor_paydate');
        //     $postData['profit']         = $this->input->post('vendor_profit');
        //     $postData['pay_to']         = !empty($this->input->post('pay_to')) ? $this->input->post('pay_to') : '';

        // }
        // $insert_id = "";
        // // $this->expense->cus_update_by('order_id',$order_id,['is_latest'=>0]);
        // $postData['is_latest'] = 1;
        // $insert_id             = $this->expense->insert($postData);

        // $status = $this->input->post('status_vendor');

        // if (!empty($this->input->post('vendor_fees'))) {
        //     $account        = $this->account->get_by('account_id', 31);
        //     $transationData = array(
        //         'category_id'     => $this->attributes->get_by('title', 'Vendor Payment')->id,
        //         'cash_type'       => $account->account_type,
        //         'account_id'      => $account->account_id,
        //         'amount'          => $this->input->post('vendor_fees'),
        //         'account_balance' => $account->account_price,
        //         'final_balance'   => $account->account_price - $this->input->post('vendor_fees'),
        //         'linked_id'       => $order_id,
        //         'description'     => "Vendor Payment (Order ID:" . $order_id . ")",
        //         'payment_type'    => $this->attributes->get_by('title', 'Withdraw')->id,
        //         'created_at'      => time(),
        //         'created_by'      => $this->session->userdata("crm_user_id"),
        //         'payment_person'  => $this->input->post("vendor_name"),
        //     );
        //     // print "<pre/>";
        //     // print_r($_POST);

        //     $main_acc_final_amount['account_price'] = $account->account_price - $this->input->post('vendor_fees');

        //     $this->transations->insert($transationData);
        //     $this->account->update($account->account_id, $main_acc_final_amount);

        // }
        // // print "<pre/>";
        // // print_r($postData);
    }
    public function agentFees($order_id)
    {
        // $this->checkPermission('order_view');
        // $this->data['is_edit_action'] = $this->checkPermission('order_edit', false);
        $this->data['is_edit_action']=true;
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $this->data['orderinfo']              = $orderinfo;
        $order_history                        = json_decode($orderinfo->order_history);
        $this->data['userinfo']               = $order_history->userInfo;
        $this->data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $this->data['agents']                 = $this->user->get_many_by("user_role", "agent");
        $this->data['allCarNames']            = $this->car_manager->get_all_carname();
        $this->data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $this->data['order_ratings']          = $this->ratings->get_order_ratings($order_id);
        $this->data['is_autorize_for_rating'] = $this->ratings->check_for_rating($order_id);
        $this->data['view']                   = 'agent_fees';
        $this->data['driver_ot_fees']         = $this->attributes->get_by('title', 'Driver OT Fees');
        $this->data['driver_fees']            = $this->attributes->get_by('title', 'Driver Fees');
        $this->data['driver_percentage']      = $this->attributes->get_by('title', 'Driver Percentage');
        $this->data['btn']                    = 'Save';
        $this->data['expense_list']           = $this->expense->get_by_many_with_user('order_id', $order_id);
        $this->data['onclick']                = '';
        $this->data['expense_id']             = '';
        $this->data['fuel_expenses']          = $this->car_oilslip->get_many_by("order_id", $order_id);
        $this->data['expenses']               = $this->transations->get_many_by("linked_id", $order_id);
        if (!empty($trip_expense_id)) {
            $findRow = $this->expense->get_by('id', $trip_expense_id);
            if (!empty($findRow)) {
                $this->data['expense_id'] = $trip_expense_id;
                $this->data['btn']        = 'Update Expense';
                // $this->data['onclick']              = 'onclick="return confirm(\'Do you want to save updated?\')"';
                $this->data['trip_expense_detail'] = $findRow;
            }
        } else {
            $findRow = $this->expense->get_latest('order_id', $order_id);
            if (!empty($findRow)) {
                $this->data['btn'] = 'Save';
                // $this->data['onclick']              = '';
                $this->data['trip_expense_detail'] = $findRow;
            }
        }
        $this->layout->admin($this->data);
    }

    public function print($order_id){
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $data['orderinfo']              = $orderinfo;
        $order_history                        = json_decode($orderinfo->order_history);
        $data['userinfo']               = $order_history->userInfo;
        $data['users']                  = $this->user->get_many_by("user_role", "supplier");
        $data['agents']                  = $this->user->get_many_by("user_role", "agent");
        $data['allCarNames']            = $this->car_manager->get_all_carname();
        $data['cars']                   = $this->car_manager->get_many_by("user_id", $orderinfo->supplier_id);
        $ot_hours                    = $this->driverot->get_many_by('order_id',$order_id);


        usort($ot_hours, function($a, $b) {
            return $a->ot_date <=> $b->ot_date;
        });
        $dup=array();
        foreach ($ot_hours as $key => $value) {
            if(empty($dup[$value->ot_date])){
                $dup[$value->ot_date]=$value->ot_hours;
            }else{
                $dup[$value->ot_date]+=$value->ot_hours;
            }
        }
        // print "<pre/>";
        // print_r();
        // exit();
        $data['auth_user'] = $this->data['logged_user'];
        $data['ot_hours_data'] = $dup;
        $this->load->view('invoice',$data);
    }
    public function driver_ot($order_id){
        // echo "hello world";
        $orderinfo                    = $this->orders->get_order($order_id);
        if (empty($orderinfo)) {
            $this->set_msg_flash(['msg' => 'This is not a valid Article', 'typ' => 2]);
            redirect(CRM_VAR . '/order/list');
        }
        $ot_hours                    = $this->driverot->get_many_by('order_id',$order_id);
        // print_r($ot_hours);
        // exit();
        $this->data['orderinfo']              = $orderinfo;

        usort($ot_hours, function($a, $b) {
            return $a->ot_date <=> $b->ot_date;
        });
        // print "<pre/>";
        // print_r($ot_hours);
        // exit();
        $this->data['ot_hours_data'] = $ot_hours;
        $this->data['view']='driver_ot';
        
        $this->layout->admin($this->data);
    }

    public function driver_ot_insert(){
        $store['ot_hours'] = $this->input->post('ot_hours');
        $store['ot_date'] = strtotime($this->input->post('date'));
        $store['order_id'] = $this->input->post('order_id');
        $store['created_at'] = time();

        // print "<pre/>";
        // print_r($store);
        // $driverot->inser
        $this->db->insert("cr_driver_ot", $store);

        $ot_hours           = $this->driverot->get_many_by('order_id',$store['order_id']);
        // print_r($ot_hours);
        $driver_ot_hours    = 0;
        foreach ($ot_hours as $key => $value) {
            $driver_ot_hours+=$value->ot_hours;
        }
        
        // echo $driver_ot_hours;
        // exit();


        $order_data = array(
            'ot_hours'        => $driver_ot_hours
        );

        $this->orders->update($store['order_id'], $order_data);
        // print_r($_POST);
        // exit();
        redirect(site_url() . CRM_VAR . '/order/driver_ot/' . $store['order_id']);
    }

    public function sms($order_id){
        // print "<Pre/>";
        // print_r($_POST);
        // echo $this->input->post('phone_no');
        // // echo "<br/>";
        // echo $this->input->post('sms_message');
        $this->sendSMS($this->input->post('phone_no'),$this->input->post('sms_message'));
        redirect(site_url() . CRM_VAR . '/order/view/' . $order_id);
    }
    public function sendSMS($phone_no,$message){
        $sms=new TripleSMS;
        $phone=[
            $phone_no
        ];
        $sms->Send(array(
            'sender' => 'SonicStar',
            'phone' => $phone,
            'message' => $message,
        ));
    }
}
