<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends MY_Controller
{
    function  __construct() {
            parent::__construct();
            $this->load->library('Paypal_lib');
            $this->data['module'] = 'Order'; // Load Users module
            $this->load->model('Orders', 'orders');
            $this->load->model('Car_manager/Car_manager', 'car_manager');
            $this->load->model('Rating/Ratings', 'ratings');
            $this->load->model('Users/User', 'user');
            $this->load->model('Attributes/Attributes', 'attributes');
            $this->load->library('paypment_kbz');
            $this->lang->load($this->data['module'].'/orders', $this->getActiveLanguage());
            
    }
    public function submit_feedback($order_id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $validate_arr = array(
                array(
                    'field' => 'rating',
                    'label' => 'Rating',
                    'rules' => 'trim|required|numeric'
                ),
                array(
                    'field' => 'comment',
                    'label' => 'Comment',
                    'rules' => 'trim|required'
                )
            );
            $this->form_validation->set_rules($validate_arr);
            if ($this->form_validation->run() == FALSE)
            {
              $this->set_msg_flash([
                  'msg'=>'Invalid Form Submittion!!',
                  'typ'=>3
              ]);
              redirect(site_url('myaccount/order_details/'.$order_id));
            }
            else{
              $order_id = $this->input->post('order_id');
              $rating = $this->input->post('rating');
              $comment = $this->input->post("comment");
              $res = $this->do_rating($order_id, $rating, $comment);
              if($res){
                $this->set_msg_flash(['msg'=>'Feedback Submitted.','typ'=>1]);
                redirect(site_url('myaccount/order_details/'.$order_id));
              }
            }
        }
    }
    private function do_rating($order_id, $rating, $comment){
        $user_id = $this->session->userdata("crm_user_id");
        if($this->session->userdata("cr_user_role") == 'customer' || $this->session->userdata("cr_user_role") == 'agent'){
          $user_role = 'Customer';
        }
        else if ($this->session->userdata("cr_user_role") == 'supplier') {
          $user_role = 'Supplier';
        }
        else{
           $user_role = 'Supplier';
        }
        //for car id
        $orderInfo = $this->orders->get_order($order_id);
        $car_id = $orderInfo->car_id;

        //array for inserting rating
        $rating_array = array(
          'order_id' => $order_id, 
          'car_id' => $car_id, 
          'user_id' => $user_id, 
          'rating' => $rating, 
          'created_date' => date("Y-m-d H:i:s"), 
          'comment' => $comment, 
          'rating_by' => $user_role, 
        );
        if($this->ratings->insert($rating_array)){
          $new_rating = $this->ratings->get_rating($car_id);
          $this->car_manager->update($car_id,array("ratings" => $new_rating->rating));
          return true;
        }
        else{
          return false;
        }
    }
   
    public function cancleOrder($order_id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){ 
          $this->form_validation->set_rules("cancleReason", "Cancle Reason", "required");
          if($this->form_validation->run() == FALSE){
            $this->set_msg_flash(['msg'=>'Reason for Cancellation is required !!!','typ'=>3]);
            redirect(site_url('/myaccount/order_details/'.$order_id));
          }
          else{
            $orderinfo = $this->orders->get_by("order_id", $order_id);
            if($orderinfo->user_id==$this->session->userdata('crm_user_id')){
                if($this->orders->update($order_id,array("status" => "Cancelled"))) {

                  $comment_data = array(
                    'order_id' => $order_id, 
                    'user_id' => $this->session->userdata('crm_user_id'), 
                    'comment' => $this->input->post('cancleReason')
                  );
                  $this->db->insert("cr_order_comment", $comment_data);


                //Cancelation Emall to customer  
                $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'cancel'));
                $mailData = $this->orders->modifyEmailData($emailParams);
                $result = $this->sendMail($mailData);
                //Cancelation Emall to Admin  
                $emailParams = array('order_id'=>$order_id,'user_type'=>'admin','params'=>array('emailType'=>'cancel'));
                $mailData = $this->orders->modifyEmailData($emailParams);
                $result = $this->sendMail($mailData);

                  $this->set_msg_flash(['msg'=>'Order Cancelled!!','typ'=>1]);
                  redirect(site_url('/myaccount/order_details/'.$order_id)); 
                }else { redirect(site_url('/myaccount/orders')); }
            }else{
                $this->set_msg_flash(['msg'=>'Direct Access not allowed !!!','typ'=>3]);
                redirect(site_url('/myaccount/orders/'));
            }
          }
        }
        else{
          $this->set_msg_flash(['msg'=>'Direct Access not allowed !!!','typ'=>3]);
          redirect(site_url('/myaccount/orders/'));
        }
    }

  
    public function listOrder(){
        $this->checkPermission('cust_orders');
        $cr_user_role = $this->session->userdata('cr_user_role');
        if($cr_user_role!='customer'){
            redirect(site_url());
        }
        $list = $this->orders->get_datatables();
        $this->data['page_title'] = getLang('ORDER_MY_BOOKING');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=> getLang('ORDER_HOME')),
                                        array('url'=>site_url('profile'),'title'=> getLang('ORDER_ACCOUNT')),
                                        array('url'=>'','title'=>getLang('ORDER_MY_BOOKING')),
                                    );
        $this->data['view'] = 'front/order_listing';         
        $this->data['orders'] = $list;         
        $this->layout->front($this->data);
    }
    public function order_details($order_id){
        $this->checkPermission('cust_orders');
        $cr_user_role = $this->session->userdata('cr_user_role');
        if($cr_user_role!='customer'){
            redirect(site_url());
        }
        $order_details = $this->orders->getOrderInformation($order_id);
        $this->data['page_title'] = getLang('ORDER_BOOKING_INFO_HEADING');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=> getLang('ORDER_HOME')),
                                        array('url'=>site_url('myaccount/orders'),'title'=> getLang('ORDER_MY_BOOKING')),
                                        array('url'=>'','title'=> getLang('ORDER_BOOKING_INFO_HEADING')),
                                    );
        $is_feedback_doneByCust = $this->ratings->get_many_by(array('order_id'=>$order_id,'rating_by'=>'Customer'));
        $this->data['is_feedback_doneByCust'] = (!empty($is_feedback_doneByCust))?$is_feedback_doneByCust[0]:array();
       
        $is_feedback_doneBySupp = $this->ratings->get_many_by(array('order_id'=>$order_id,'rating_by'=>'Supplier'));
        $this->data['is_feedback_doneBySupp'] = (!empty($is_feedback_doneBySupp))?$is_feedback_doneBySupp[0]:array();
        $this->data['view'] = 'front/order_details';         
        $this->data['order_details'] = $order_details;         
        $this->layout->front($this->data);
    }

    public function emailTemplate(){


        //Manage Email Notifications
        $emailParams = array('order_id'=>1022,'user_type'=>'admin','params'=>array('emailType'=>'success'));
        $mailData = $this->orders->modifyEmailData($emailParams);
        $mailData['to'] = 'testttttt.s@mailinator.com';
       //print_r($mailData);die;
        $result = $this->sendMail($mailData);
        if($result){
            print_r($mailData['message']);die("Sent");
        }
        else{
            print_r($mailData['message']);die("Failed");
        }

        //Manage Email Notifications
    }
    public function returm_mpu(){
        print_r($_REQUEST);die("returm_mpu");
    }
    public function success_mpu(){
         print_r($_REQUEST);die("success_mpu");
    }
    public function str_rand($length = 8, $seeds = 'alphanum') {
        // Possible seeds
        $seedings['alpha'] = 'abcdefghijklmnopqrstuvwqyz';
        $seedings['numeric'] = '0123456789';
        $seedings['alphanum'] = 'abcdefghijklmnopqrstuvwqyz0123456789';
        $seedings['hexidec'] = '0123456789abcdef';

        // Choose seed
        if (isset($seedings[$seeds])) {
            $seeds = $seedings[$seeds];
        }

        // Seed generator
        list($usec, $sec) = explode(' ', microtime());
        $seed = (float) $sec + ((float) $usec * 100000);
        mt_srand($seed);

        // Generate
        $str = '';
        $seeds_count = strlen($seeds);

        for ($i = 0; $length > $i; $i++) {
            $str .= $seeds{mt_rand(0, $seeds_count - 1)};
        }

        return strtoupper($str);
    }
    public function test_book1(){
            $invoice = "000000000000".$this->str_rand(8);
            $processedData = array(
                                'merchantID'=>'205104001002799',
                                'invoiceNo'=>$invoice,
                                'productDesc'=>"Mazda-M3",
                                'amount'=> '000000000150',
                                'currencyCode'=>'104',
                            );
            $this->data['datainfo'] = $processedData; 
            $this->data['payment_url'] = 'http://122.248.120.252:60145/UAT/Payment/Payment/pay'; 
            $this->data['view'] = 'front/api_payment_process'; 
            $this->data['data'] = array(1, 3,5,6,7,8,9); 
            
            $this->layout->front($this->data);
    }
    public function kbz_book(){
            $invoice = $this->str_rand(8); //Get randon alpha-numeric 8 characters. 
            $data = array('invoice_no'=>$invoice,'amount'=>'000000000150','currencyCode'=>'840','merchantID'=>'47220041','car_name'=>'Mazda-M3','userDefined1'=>'userDefined1','userDefined2'=>'userDefined2','userDefined3'=>'userDefined3');
            $hashValue = $this->paypment_kbz->getHash($data);
            $processedFieldsData = array(
                                'invoiceNo'=>$invoice,
                                'amount'=> '000000000150',
                                'currencyCode'=>'840',
                                'merchantID'=>'47220041',
                                'productDesc'=>"Mazda-M3",
                                'userDefined1'=>'userDefined1',
                                'userDefined2'=>'userDefined2',
                                'userDefined3'=>'userDefined3',
                                'hashValue' =>$hashValue
                            );
            //print_r($processedData);die;
            $this->data['datainfo'] = $processedFieldsData; 
            $this->data['payment_url'] = 'https://demo3.2c2p.com/KBZPGW/payment/payment/payment'; 
            
            $this->data['view'] = 'front/api_payment_process'; 
            $this->data['data'] = array(1, 3,5,6,7,8,9); 
            
            $this->layout->front($this->data);
    }
    public function _2c2p(){

    }
    

    public function test_book(){
        
        $invoice = "000000000000".$this->str_rand(8);
        $hassFields = array(
                            'invoiceNo'=>$invoice,
                            'amount'=> '000000000150',
                            'currencyCode'=> '104',
                            'merchantID'=>'205104001002799',
                            'productDesc'=>"Mazda-M3",
                            'userDefined1'=>'userDefined1',
                            'userDefined2'=>'userDefined2',
                            'userDefined3'=>'userDefined3',
                    );
        $hasValuepatter = '';
        foreach ($hassFields as $v){
            $hasValuepatter .=$v;
        }   
        $signData = hash_hmac('sha1',$hasValuepatter,'2GWWDOLHTF4CX1KNK4ENPTZOKLQV2FIG', false);
        $signData= strtoupper($signData);
        $hashValue = urlencode($signData);

        $processedData = array(
                            'merchantID'=>'205104001002799',
                            'invoiceNo'=>$invoice,
                            'productDesc'=>"Mazda-M3",
                            'amount'=> '000000000150',
                            'currencyCode'=>'104',
                            'userDefined1'=>'userDefined1',
                            'userDefined2'=>'userDefined2',
                            'userDefined3'=>'userDefined3',
                            'hashValue' =>$hashValue
                        );
        //print_r($processedData);die;
        $this->data['datainfo'] = $processedData; 
        $this->data['payment_url'] = 'http://122.248.120.252:60145/UAT/Payment/Payment/pay'; 
        $this->data['view'] = 'front/api_payment_process'; 
        $this->data['data'] = array(1, 3,5,6,7,8,9); 

        $this->layout->front($this->data);
    }

    public function sendNotificationEmailToGuestUser($bookingUserInfo){
        $to = $bookingUserInfo['user_email'];
        $subject = "Successfully Registered as Guest"; //subject for mail
                $body = file_get_contents("./templates/success_register_guest.html");  //mail template path
        $body = str_replace("{NAME}", $bookingUserInfo['user_fname']." ".$bookingUserInfo['user_lname'], $body);
                $body = str_replace("{SITE}", site_url(), $body);
                $body = str_replace("{LOGINURL}", site_url('login'), $body);
                $body = str_replace("{RANDON_PASSWORD}", $bookingUserInfo['user_password'], $body);
                $mailData = array(
            "to" => $to,
            "subject" => $subject,
            "message" => $body
        );

        //mail function call
        return $result = $this->sendMail($mailData);
    }
    public function booknow(){
            //echo $this->orders->getOrderInfoByInvoice('000000001037WBPK5EQG');
            $user_id = $this->session->userdata('crm_user_id');
            $order_history = $this->session->userdata('booking_info');
            $order_history['currency'] = $this->session->userdata('active_currency');
            //print_r($order_history['carinfo']);die;
            extract($_REQUEST);
            $carinfo = $order_history['carinfo'];
            $is_admin_sp = (in_array($this->session->userdata("cr_user_role"),array('admin','service_provider'))?true:false);
            if($is_admin_sp){
                $admin_amount = (isset($admin_amount) && $admin_amount>0)?$admin_amount:FALSE;
            }
            else{
                $admin_amount = FALSE;
            }
            $carname = $carinfo['carname'];
            $bookingUserInfo = array('user_fname'=>$user_fname,'user_lname'=>$user_lname,'user_email'=>$user_email,'user_tel'=>$phone);

            //Manage Guest User. If Customer not login then registered as Guest, If email already registered in system then get userId from db by email.

            if($user_id=='' || $user_id<0){
                $user_info = $this->user->get_by(array('user_email'=>$user_email));

                if(empty($user_info) || count($user_info)<0){
                    $randaomPassword = $this->str_rand(8);
                    $bookingUserInfo['user_active'] = 'YES';
                    $bookingUserInfo['user_password'] = $randaomPassword;
                    $bookingUserInfo['user_has_password'] = password_hash($randaomPassword,PASSWORD_DEFAULT);
                    $bookingUserInfo['user_role'] = 'customer'; 
                    $user_id = $this->user->insert($bookingUserInfo);
                    $this->sendNotificationEmailToGuestUser($bookingUserInfo);
                    $this->session->set_userdata('crm_user_id',$user_id);
                }else{
                    $user_id = $user_info->id;
                    $this->session->set_userdata('crm_user_id',$user_id);
                }
            }
            //END manage guest user. 
            $bookingUserInfo['phone'] = $phone;
            $order_history['userInfo'] = $bookingUserInfo; //Set user Information for booking


            if(isset($order_history['searchData']['pickup_date'])){
                $pickup_date = $order_history['searchData']['pickup_date'];
            }if(isset($order_history['searchData']['pickup_time'])){
                $pickup_time = " ".$order_history['searchData']['pickup_time'];
            }
            $pickup_date = date("Y-m-d H:i:s",strtotime($pickup_date.$pickup_time));
            //Tax Information
            $taxindex = (count($order_history['trackPrices'])-2);
            $tax_amount = isset($order_history['trackPrices'][$taxindex]['amount'])?$order_history['trackPrices'][$taxindex]['amount']:'0.00';
            if($admin_amount){
                $order_history['bookingPrice'] = $admin_amount;
                $trackLastIndex = count($order_history['trackPrices'])-1;
                if(isset($order_history['trackPrices'][$trackLastIndex]['amount'])){
                    $order_history['update_by_admin'] = $order_history['trackPrices'][$trackLastIndex]['amount'];
                    $order_history['trackPrices'][$trackLastIndex]['amount'] = getNumFormat($admin_amount);
                }
            }
            
            $TotalBookingAmount = str_replace(',', '', $order_history['bookingPrice']);

             //echo "<pre>";print_r($order_history);die($TotalBookingAmount);
            $user_id=!empty($this->input->post('order_for_user_id'))? $this->input->post('order_for_user_id'): $user_id ;
             // echo $user_id;
            // print "<pre/>";
            $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($order_history['trackPrices']));
            $_dailystatus = iterator_to_array($it,false)[0];
            $dailystatus ="";

            $trip_record_name="";

            if (strpos($_dailystatus, 'Full Day') !== false) {
                $dailystatus="full-day";
                $trip_record_name = $this->attributes->get_by('id',$order_history['car_city'])->title." (Full-day - ".$order_history['searchData']['incity'].")";
            }else if (strpos($_dailystatus, 'Half-day') !== false) {
                $dailystatus="half-day";
                $trip_record_name = $this->attributes->get_by('id',$order_history['car_city'])->title." (Half-day - ".$order_history['searchData']['incity'].")";
            }

            if (isset($order_history['searchData']['airport']) && ($order_history['searchData']['airport']=="pickup" || $order_history['searchData']['airport']=="dropoff")) {
                $trip_record_name = $this->attributes->get_by('id',$order_history['car_city'])->title." (Airport ".$order_history['searchData']['airport'].")";
                
            }

            if($order_history['rent_type']==37){
                $trip_record_name=$this->attributes->get_by('id',$order_history['car_city'])->title." (Weekly)";
            }
            if($order_history['rent_type']==38){
                $trip_record_name=$this->attributes->get_by('id',$order_history['car_city'])->title." (Monthly)";
            }
            if($order_history['searchData']['incity']=="highway"){
                $places=array_unique($order_history['datewiseDestinations']['titles']);
                // array_unique($places);
                // print_r($places);
                $trip_record_name= $this->attributes->get_by('id',$order_history['car_city'])->title.' - '.implode(' - ', $places);
            }
           
            // echo $order_history['carinfo']['car_id'];
            // print_r($list[0]);
            

            $orderData = array(
                                'user_id'=>$user_id,
                                'car_id'=>$order_history['carinfo']['car_id'],
                                'rent_type'=>$order_history['rent_type'],
                                'pickup_date'=>$pickup_date,
                                'pickup_location'=>$order_history['car_city'],
                                'booking_amount'=>$TotalBookingAmount,
                                'tax_amount' => $tax_amount,
                                'booking_date' => date("Y-m-d H:i:s"),
                                'order_history'=>json_encode($order_history),
                                'order_by'=>$this->session->userdata("crm_user_id"),
                                'driver_ot_fees'=>$this->attributes->get_by('id',4410)->short_desc,
                                'driver_fees'=>$this->attributes->get_by('id',4411)->short_desc,
                                'driver_percentage'=>$this->attributes->get_by('id',4412)->short_desc,
                                'daily_status' => $dailystatus,
                                'trip_record_name' => $trip_record_name,
                            );

            $check_user=$this->user->get_by('id',$user_id);
            if($check_user->user_role=="agent"){
                $orderData['agent_fees'] = $TotalBookingAmount * $check_user->commission_rate / 100;
            // echo $TotalBookingAmount * $check_user->commission_rate / 100;
            }
            if(isset($order_history['searchData']['airport'])){
                if($order_history['searchData']['airport']=="pickup" || $order_history['searchData']['airport']=="dropoff"){
                    $orderData['airport_status'] = $order_history['searchData']['airport'];
                }else if($order_history['searchData']['incity']=="in_city"){
                    $orderData['airport_status'] = 'incity';
                }else if($order_history['searchData']['incity']=="highway"){
                    $orderData['airport_status'] = 'highway';
                }
            }
            // print "<pre/>";
            // print_r($order_history);
            // print_r($this->car_manager->get_by('id',$order_history['carinfo']['car_id']));

            $car=$this->car_manager->get_by('id',$order_history['carinfo']['car_id']);
            $is_vendor=$this->user->get_by('id',$car->user_id);
            if($is_vendor->user_lname!="Sonic Star"){
                $orderData['vendor_fees']= $TotalBookingAmount - ($TotalBookingAmount * $is_vendor->commission_rate / 100);
            }

            // if($cars->){

            // }
#

           // print "<pre/>";
            // print_r($order_history['trackPrices'][0]);
            // print_r($order_history['searchData']);

            // if(soni)
            $orderData = $this->manageDropOffInfo($orderData,$order_history);
           // print_r($orderData);die;
            $order_id = $this->session->userdata('curr_order_id');
            if($order_id=='' || $order_id<0){
                $order_id = $this->orders->insert($orderData);
                $this->session->set_userdata('curr_order_id',$order_id);
            }else{ 
                $this->orders->update($order_id,$orderData);
            }
            $supplier_id = $order_history['carinfo']['supplier_id'];
            //print_r($bookingUserInfo);die;
            $fullname = $bookingUserInfo['user_fname'].' '.$bookingUserInfo['user_lname'];
            $phone = $bookingUserInfo['phone'];
            $orderInfo = array('order_id'=>$order_id,'supplier_id'=>$supplier_id,'type_of_rent'=>$order_history['rent_type'],'paid_to'=>$supplier_id,'name'=>$fullname,'phone'=>$phone,
                                'usr_email'=>$bookingUserInfo['user_email']);
            //Check order id is generate or not in cr_order_detail
            $whereClause = array('order_id'=>$order_id);
            $this->manageOrderInfo($whereClause,$orderInfo,'cr_order_detail');
            //$this->db->insert('cr_order_detail', $orderInfo);
            $paymentInfo = array('order_id'=>$order_id,'amount'=>$TotalBookingAmount);
            
            switch ($payment) {
                    case 'cash':
                        $paymentInfo['payment_method'] = 'COD';
                        $invoice = str_pad($order_id.$this->str_rand(8),20,"0",STR_PAD_LEFT);
                        $paymentInfo['invoice_no'] = $invoice;
                        //Check order id is generate or not in cr_payment_info
                        $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
                        $this->session->set_userdata('search_data_'.$order_history['rent_type'],array());
                        $this->session->set_userdata('order_id',$order_id);
                        $this->session->set_userdata('curr_order_id','');
                        //send to customer
                        $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'success'));
                        $mailData = $this->orders->modifyEmailData($emailParams);
                        $result = $this->sendMail($mailData);
                        //Send to to admin
                        $emailParams = array('order_id'=>$order_id,'user_type'=>'admin','params'=>array('emailType'=>'success'));
                        $mailData = $this->orders->modifyEmailData($emailParams);
                        $result = $this->sendMail($mailData);
                        redirect(site_url('booking_info'));
                        break;;
                    case 'paypal':
                        $paymentInfo['payment_method'] = 'Paypal';
                        $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info'); 
                        $this->by_paypal($user_id,$order_id,$TotalBookingAmount); //user_id, order_id, total_amount
                        break;;
                    case 'bank_transfer':
                        $this->session->set_userdata('order_id',$order_id);
                        $data = array('amount'=>$TotalBookingAmount,'invoice_no'=>$order_id,'car_name'=>$carname);
                        if($this->active_currency=='mmk'){
                            $paymentInfo['payment_method'] = 'MPU';
                        }else{
                            $paymentInfo['payment_method'] = 'KBZ';
                        }
                        //Check order id is generate or not in cr_payment_info
                        $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
                        if($this->active_currency=='mmk'){
                           $this->processWithMPU($data);
                        }else{
                            $this->processWith2C2PMethod($data);
                        }
                        break;
                    default :
                        redirect(site_url());
                        break;
            }
    }
    private function processWithKBZMethod($data){
            $KBZMerchantID = isset($this->data['attributesByid'][231]['short_desc'])?$this->data['attributesByid'][231]['short_desc']:'';
            $KBZPaymentMode = isset($this->data['attributesByid'][232]['short_desc'])?$this->data['attributesByid'][232]['short_desc']:'0';
            $invoice = "00000000".$data['invoice_no'].$this->str_rand(8); //Get randon alpha-numeric 8 characters. 
            $amount = str_pad(($data['amount']*100),12,"0",STR_PAD_LEFT);
            $data = array('invoice_no'=>$invoice,'amount'=>$amount,'currencyCode'=>'840','merchantID'=>'47220041','car_name'=>$data['car_name'],
                            'userDefined1'=>'userDefined1','userDefined2'=>'userDefined2','userDefined3'=>'userDefined3');
            $hashValue = $this->paypment_kbz->getHash($data);
            $processedFieldsData = array(
                                'invoiceNo'=>$invoice,
                                'amount'=> $amount,
                                'currencyCode'=>'840',
                                'merchantID'=>$KBZMerchantID,
                                'productDesc'=>$data['car_name'],
                                'userDefined1'=>'userDefined1',
                                'userDefined2'=>'userDefined2',
                                'userDefined3'=>'userDefined3',
                                'hashValue' =>$hashValue
                            );
            //print_r($processedData);die;
            $this->data['datainfo'] = $processedFieldsData; 
            if($KBZPaymentMode==1){
                $this->data['payment_url'] = 'https://demo3.2c2p.com/KBZPGW/payment/payment/payment'; 
            }else{
                $this->data['payment_url'] = 'https://demo3.2c2p.com/KBZPGW/payment/payment/payment'; 
            }
            
            $this->data['view'] = 'front/api_payment_process'; 
            $paymentInfo = array('invoice_no'=>$invoice);
            $whereClause = array('order_id'=>$data['invoice_no']);
            $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
            $this->layout->front($this->data);
    }
    private function processWithMPU($data){
        $MPUMerchantID = isset($this->data['attributesByid'][229]['short_desc'])?$this->data['attributesByid'][229]['short_desc']:'';
        $MPUPaymentMode = isset($this->data['attributesByid'][230]['short_desc'])?$this->data['attributesByid'][230]['short_desc']:'0';
        $bookingAmount = ($data['amount']*100);
        $bookingAmount = str_pad($bookingAmount,12,"0",STR_PAD_LEFT);
        //$invoice = "00000000".$data['invoice_no'].$this->str_rand(8);
        $invoice = str_pad($data['invoice_no'].$this->str_rand(8),20,"0",STR_PAD_LEFT);
        $hassFields = array(
                            'invoiceNo'=>$invoice,
                            'amount'=> $bookingAmount,
                            'currencyCode'=> '104',
                            'merchantID'=>'205104001002799',
                            'productDesc'=>$data['car_name'],
                            'userDefined1'=>'userDefined1',
                            'userDefined2'=>'userDefined2',
                            'userDefined3'=>'userDefined3',
                    );
        $hasValuepatter = '';
        foreach ($hassFields as $v){
            $hasValuepatter .=$v;
        }   
        $signData = hash_hmac('sha1',$hasValuepatter,'2GWWDOLHTF4CX1KNK4ENPTZOKLQV2FIG', false);
        $signData= strtoupper($signData);
        $hashValue = urlencode($signData);

        $processedData = array(
                            'merchantID'=>'205104001002799',
                            'invoiceNo'=>$invoice,
                            'productDesc'=>$data['car_name'],
                            'amount'=> $bookingAmount,
                            'currencyCode'=>'104',
                            'userDefined1'=>'userDefined1',
                            'userDefined2'=>'userDefined2',
                            'userDefined3'=>'userDefined3',
                            'hashValue' =>$hashValue
                        );
        //print_r($processedData);die($data['amount']);
        $this->data['datainfo'] = $processedData; 
        if($MPUPaymentMode==1){
            $this->data['payment_url'] = 'http://122.248.120.252:60145/UAT/Payment/Payment/pay';
        }else{
            $this->data['payment_url'] = 'http://122.248.120.252:60145/UAT/Payment/Payment/pay';
        }
        $this->data['view'] = 'front/api_payment_process'; 
        $paymentInfo = array('invoice_no'=>$invoice);
        $whereClause = array('order_id'=>$data['invoice_no']);
        $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
        $this->layout->front($this->data);
    }
    private function processWith2C2PMethod($data){
            $sscurrency=(string)strip_tags($this->session->userdata('active_currency'));
            $KBZMerchantID="";
            // $currency_code="";
            // echo $sscurrency;
            // exit();
            if($sscurrency=="mmk"){
                // echo ""
                $KBZMerchantID = '104104000000229';
                $currency_code=104;
            }
            if($sscurrency=="usd"){
                $KBZMerchantID = '104104000000223';
                $currency_code=840;
            }
            $KBZPaymentMode = isset($this->data['attributesByid'][232]['short_desc'])?$this->data['attributesByid'][232]['short_desc']:'0';
            $invoice =time(); //Get randon alpha-numeric 8 characters. 
            $amount = str_pad(($data['amount']*100),12,"0",STR_PAD_LEFT);
            $data = array(
                'version'=>'7.2',
                'merchant_id'=>$KBZMerchantID,
                'payment_description'=>$data['car_name'],
                'order_id'=>$invoice,
                'currency'=>$currency_code,
                'amount'=>$amount,
                'result_url_1'=>base_url()
            );
            $hashValue = $this->paypment_kbz->getHash2c2p($data,$sscurrency);
            $processedFieldsData = array(
                                'version'=>'7.2',
                                'merchant_id'=>$KBZMerchantID,
                                'payment_description'=>$data['payment_description'],
                                'order_id'=>$invoice,
                                'currency'=>$currency_code,
                                'amount'=> $amount,
                                'result_url_1' => base_url(),
                                // 'user_defined_1'=>'userDefined1',
                                // 'user_defined_2'=>'userDefined2',
                                // 'user_defined_3'=>'userDefined3',
                                'hash_value' =>$hashValue
                            );
            //print_r($processedData);die;
            $this->data['datainfo'] = $processedFieldsData; 
            if($KBZPaymentMode==1){
                $this->data['payment_url'] = 'https://t.2c2p.com/RedirectV3/payment'; 
            }else{
                $this->data['payment_url'] = 'https://t.2c2p.com/RedirectV3/payment'; 
            }
            
            $this->data['view'] = 'front/api_payment_process'; 
            $paymentInfo = array('invoice_no'=>$invoice);
            $whereClause = array('order_id'=>$data['order_id']);
            $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
            // print "<pre/>";
            // echo $sscurrency;
            // print_r($processedFieldsData);
            $this->layout->front($this->data);
    }
    //Booking setup for MPU API with MMK currency
    public function booking_setup(){ 
        $invoiceNo = isset($_REQUEST['invoiceNo'])?$_REQUEST['invoiceNo']:'';
        $order_id = $this->orders->getOrderInfoByInvoice($invoiceNo);
        if($order_id && $invoiceNo!=''){
            $response_data = $_REQUEST;
            $whereClause = array('invoice_no'=> $_REQUEST['invoiceNo']);
            $paymentInfo = array();
            if($_REQUEST['respCode']=='00'){
                $paymentInfo['status'] = 'Completed';// array('status'=> 'Completed','response_data'=>$response_data);
                //send to customer
                $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'success'));
                $mailData = $this->orders->modifyEmailData($emailParams);
                $result = $this->sendMail($mailData);
                //Send to to admin
                $emailParams = array('order_id'=>$order_id,'user_type'=>'admin','params'=>array('emailType'=>'success'));
                $mailData = $this->orders->modifyEmailData($emailParams);
                $result = $this->sendMail($mailData);
            }
            $paymentInfo['response_data'] = $response_data;
            $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
        }
    }
    //Booking response handle for MPU API with MMK currency
    public function booking_complete(){
        if($_REQUEST['respCode']=='00'){
            $paymentInfo = array('status'=> 'Completed');
            $whereClause = array('invoice_no'=> $_REQUEST['invoiceNo']);
            $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
            $order_id = $this->session->userdata('order_id');
            //send to customer
            $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'success'));
            $mailData = $this->orders->modifyEmailData($emailParams);
            $result = $this->sendMail($mailData);
            //Send to to admin
            $emailParams = array('order_id'=>$order_id,'user_type'=>'admin','params'=>array('emailType'=>'success'));
            $mailData = $this->orders->modifyEmailData($emailParams);
            $result = $this->sendMail($mailData);
            $rent_type = $user_id = $this->session->userdata('rent_type');
            $this->session->set_userdata('curr_order_id','');
            $this->session->set_userdata('search_data_'.$rent_type,array());
        }else{
            
        }
        redirect(site_url('booking_info'));
        
    }
    public function order_setup(){ 
        $invoiceNo = isset($_REQUEST['invoiceNo'])?$_REQUEST['invoiceNo']:'';
        $order_id = $this->orders->getOrderInfoByInvoice($invoiceNo);
        if($order_id && $invoiceNo!=''){
            $response_data = $_REQUEST;
            $whereClause = array('invoice_no'=> $_REQUEST['invoiceNo']);
            $paymentInfo = array();
            if($_REQUEST['respCode']=='00'){
                $paymentInfo['status'] = 'Completed';// array('status'=> 'Completed','response_data'=>$response_data);
                //send to customer
                $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'success'));
                $mailData = $this->orders->modifyEmailData($emailParams);
                $result = $this->sendMail($mailData);
                //Send to to admin
                $emailParams = array('order_id'=>$order_id,'user_type'=>'admin','params'=>array('emailType'=>'success'));
                $mailData = $this->orders->modifyEmailData($emailParams);
                $result = $this->sendMail($mailData);
            }
            $paymentInfo['response_data'] = $response_data;
            $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
        }
    }
    public function order_complete(){
        if($_REQUEST['respCode']=='00'){
            $paymentInfo = array('status'=> 'Completed');
            $whereClause = array('invoice_no'=> $_REQUEST['invoiceNo']);
            $this->manageOrderInfo($whereClause,$paymentInfo,'cr_payment_info');
            $order_id = $this->session->userdata('order_id');
            //send to customer
            $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'success'));
            $mailData = $this->orders->modifyEmailData($emailParams);
            $result = $this->sendMail($mailData);
            //Send to to admin
            $emailParams = array('order_id'=>$order_id,'user_type'=>'admin','params'=>array('emailType'=>'success'));
            $mailData = $this->orders->modifyEmailData($emailParams);
            $result = $this->sendMail($mailData);
            $rent_type = $this->session->userdata('rent_type');
            $this->session->set_userdata('curr_order_id','');
            $this->session->set_userdata('search_data_'.$rent_type,array());
        }else{
            
        }
        redirect(site_url('booking_info'));
        
    }
    
    public function booking_success($order_id=''){
        if($order_id==''){
            $order_id = $this->session->userdata('order_id');
            $curr_order_id = $this->session->userdata('curr_order_id');
        }
        $user_id = $this->session->userdata('crm_user_id');
        $orderInfo = $this->orders->get_by(array('order_id'=>$order_id,'user_id'=>$user_id));
        //print_r($orderInfo);die("dgfdf".$user_id);
        if(empty($orderInfo)){
            redirect(site_url());
        }
        $this->data['page_title'] = getLang('ORDER_BOOKING_INFO_HEADING');
        $this->data['breadcrumbs'] = array(
                                        array('url'=>site_url(),'title'=>'Home'),
                                        array('url'=>site_url('myaccount/orders'),'title'=> getLang('ORDER_MY_BOOKING')),
                                        array('url'=>'','title'=>getLang('ORDER_BOOKING_INFO_HEADING')),
                                    );
        if(isset($curr_order_id) && $curr_order_id>0){
            $this->data['order_status'] = 'failed';
            $this->data['car_url'] = $this->session->userdata('car_url');;
        }else{
            $this->data['order_status'] = 'success';
        }
        $this->data['order_message'] = ''; 
        $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'print_details'));
        $mailData = $this->orders->modifyEmailData($emailParams);
        //print_r($mailData['message']);die($order_id);
        $this->data['order_id'] = $order_id;
        $this->data['message'] = $mailData['message'];
        $this->data['order_history'] = json_decode($orderInfo->order_history);
        $this->data['view'] = 'front/order_success'; 
        $this->layout->front($this->data);
    }
    function index(){
            $this->by_paypal(8,1,500); //user_id, order_id, total_amount
    }

    function success(){
        //get the transaction data
        $paypalInfo = $this->input->post();
        //send to customer
        $emailParams = array('order_id'=>$order_id,'user_type'=>'customer','params'=>array('emailType'=>'success'));
        $mailData = $this->orders->modifyEmailData($emailParams);
        $result = $this->sendMail($mailData);
        //Send to to admin
        $emailParams = array('order_id'=>$order_id,'user_type'=>'admin','params'=>array('emailType'=>'success'));
        $mailData = $this->orders->modifyEmailData($emailParams);
        $result = $this->sendMail($mailData);
        $payment_data = array(
                            'txn_id' => $paypalInfo['txn_id'], 
                            'response_data' => serialize($paypalInfo), 
                            'status' => 'Completed'
                          );
        //print_r($this->session->all_userdata());die;
        $rent_type = $user_id = $this->session->userdata('rent_type');
        if($this->db->update("cr_payment_info", $payment_data, array('order_id' => $paypalInfo['item_number']))){
                $this->session->set_userdata('curr_order_id','');
                $this->session->set_userdata('search_data_'.$rent_type,array());
                $this->session->set_userdata('order_id',$paypalInfo['item_number']);
                redirect(site_url('booking_info'));
        }else{
            //Invalid access.
            redirect(site_url());
        }
     }

    function cancel(){
        $payment_data = array(
          'status' => 'Cancel'
        );
        $paypalInfo = $this->input->post();
        $car_url = $this->session->userdata('car_url');
        if($car_url!='')
            $car_url = "car/".$car_url;
        else
            $car_url = '';
        print_r($paypalInfo);die;
        if(!empty($paypalInfo)){
            if($this->db->update("cr_payment_info", $payment_data, array('order_id' => $paypalInfo['item_number']))){

                $this->set_msg_flash(['msg'=>'Your order has cancelled.Now you can continue again','typ'=>'warning']);
            }
        }redirect(site_url($car_url));
     }

    function ipn(){
		//paypal return transaction details array
		$paypalInfo	= $this->input->post();
		echo "<pre>";print_r($paypalInfo);die;
		$data['user_id'] = $paypalInfo['custom'];
		$data['product_id']	= $paypalInfo["item_number"];
		$data['txn_id']	= $paypalInfo["txn_id"];
		$data['payment_gross'] = $paypalInfo["payment_gross"];
		$data['currency_code'] = $paypalInfo["mc_currency"];
		$data['payer_email'] = $paypalInfo["payer_email"];
		$data['payment_status']	= $paypalInfo["payment_status"];

		$paypalURL = $this->paypal_lib->paypal_url;		
		$result	= $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
		
		//check whether the payment is verified
		if(preg_match("/VERIFIED/i",$result)){
		    //insert the transaction data into the database
			//$this->product->insertTransaction($data);
		}
    }
    private function manageDropOffInfo($orderData,$order_history){
		switch ($order_history['rent_type']) {
			case 36:
                                if($order_history['searchData']['incity']=='highway'){
                                    $orderData['drop_location'] = (isset($order_history['searchData']['destination'])?$order_history['searchData']['destination']:'');
                                }
                                $dropoff_date = $order_history['searchData']['dropoff_date'];
                                $dropoff_time = $order_history['searchData']['dropoff_time'];
				$dropoff_date = date("Y-m-d H:i:s",strtotime($dropoff_date." ".$dropoff_time));
				$orderData['drop_date'] = $dropoff_date;
				break;

			case 37:
				$no_of_weeks = $order_history['no_of_weeks'];
                                $pickup_date = $order_history['searchData']['pickup_date'];
                                $pickup_time = $order_history['searchData']['pickup_time'];
				$dropoff_date = date("Y-m-d H:i:s",strtotime("+$no_of_weeks weeks", strtotime($pickup_date." ".$pickup_time)));
				$orderData['drop_date'] = $dropoff_date;
				break;
			
			case 38:
				$no_of_months = $order_history['no_of_months'];
                                $pickup_date = $order_history['searchData']['pickup_date'];
                                $pickup_time = $order_history['searchData']['pickup_time'];
				$dropoff_date = date("Y-m-d H:i:s",strtotime("+$no_of_months months", strtotime($pickup_date." ".$pickup_time)));
				$orderData['drop_date'] = $dropoff_date;
				break;
			
			default:
				$this->redirect(site_url());
		}
		return $orderData;
	}
    private function by_paypal($user_id, $order_id, $price){
            $order = $this->orders->get_order($order_id);//echo "<pre>";print_r($order);die;
            $item_name = $this->car_manager->get_carname($order->car_id);
            $item_name = str_replace(" ", "-", $item_name);
            //Set variables for paypal form
            $returnURL = base_url().'products/success'; //payment success url
            $cancelURL = base_url().'products/cancel'; //payment cancel url
            $notifyURL = base_url().'products/ipn'; //ipn url
            //get particular product data

            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            //$this->paypal_lib->add_field('item_name', "item1");
            $this->paypal_lib->add_field('custom', $user_id);
            $this->paypal_lib->add_field('item_number',  $order_id); 
            $this->paypal_lib->add_field('amount',  $price);		
            //$this->paypal_lib->image($logo);

            $this->paypal_lib->paypal_auto_form();
    }
    private function manageOrderInfo($where, $Info,$table){
        if(is_array($where) && is_array($Info) && $table!=''){
            $this->db->where($where);
            $chkOrder = $this->db->get($table);
            $chkOrder1 = $chkOrder->result();
            if(empty($chkOrder1)){
                $this->db->insert($table, $Info);
            }else{
                $this->db->where($where);
                $this->db->set($Info);
                $this->db->update($table);
            }
        }
        return TRUE;
    }

    
    }

/*
 * Response *
Array
(
    [payer_email] => niraj.c@cisinlabs.com
    [payer_id] => RZFZQ5DKXAC2J
    [payer_status] => VERIFIED
    [first_name] => Niraj
    [last_name] => C
    [address_name] => Niraj C
    [address_street] => 1 Main St
    [address_city] => San Jose
    [address_state] => CA
    [address_country_code] => US
    [address_zip] => 95131
    [residence_country] => US
    [txn_id] => 1TG1406770523404P
    [mc_currency] => USD
    [mc_fee] => 14.80
    [mc_gross] => 500.00
    [protection_eligibility] => INELIGIBLE
    [payment_fee] => 14.80
    [payment_gross] => 500.00
    [payment_status] => Pending
    [pending_reason] => paymentreview
    [payment_type] => instant
    [item_number] => 1   //order_id
    [quantity] => 1
    [txn_type] => web_accept
    [payment_date] => 2017-08-10T13:50:25Z
    [business] => rohit@mailinator.com
    [receiver_id] => CXFWFUDS5ARVY
    [notify_version] => UNVERSIONED
    [custom] => 8  //user_id
    [verify_sign] => AFcWxV21C7fd0v3bYYYRCpSSRl31AXZ1GdnUccwxmuTWRhj9S1f8dMkh
)
	
*/