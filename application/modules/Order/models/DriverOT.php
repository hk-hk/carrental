<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DriverOT extends MY_Model
{
	public $_table      = 'cr_driver_ot'; // database table
    public $primary_key = 'id'; // primary keyfield database for datatable orderable
    // public $order = array('cr_trip_expense.id' => 'desc'); // default order
    public function __construct()
    {

        parent::__construct();

    }

    public function get_by_order($order_id) // Get total row count of search query
    {
        $query = "SELECT * from cr_driver_ot where order_id=" . $order_id;
        $res = $this->db->query($query);
        return $res->row_object();
    }
}