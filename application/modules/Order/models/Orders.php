<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders extends MY_Model
{

    public $_table        = 'cr_orders'; // database table
    public $primary_key   = 'order_id'; // primary key
    public $before_create = array('timestamps');
    public $column_order  = array('cr_orders.order_id', null, null, 'pickup_date', 'booking_amount', 'status', null); //set column field database for datatable orderable
    public $column_search = array('status', 'user_id', 'supplier_id'); //set column field database for datatable searchable
    public $order         = array('cr_orders.order_id' => 'desc'); // default order

    public $config = array( // Set validation for add/edit form.

    );

    protected function timestamps($terminal)
    {
        $terminal['modify_date'] = date('Y-m-d H:i:s');
        return $terminal;
    }

    public function add_supplier_chk($condition)
    {
        extract($condition);
        $booking_id = $this->input->post('booking_id', true);
        $this->db->from('cr_supplier_booking');
        if ($booking_id > 0) {
            $this->db->where('id!=', $booking_id);
        }
        $this->db->where(array('car_id' => $car_id));
        //$this->db->group_start();
        $this->db->group_start();
        $this->db->where("'$from_date' BETWEEN from_date AND to_date", null, false);
        $this->db->or_where("'$to_date' BETWEEN from_date AND to_date", null, false);

        $this->db->group_end();

        /*$this->db->or_group_start();
        $this->db->where("to_date >=", $from_date);
        $this->db->where("to_date <=", $to_date);
        $this->db->group_end();*/
        //$this->db->group_end();
        $bookingInfo = $this->db->get()->result();
        //echo $this->db->last_query();die;
        if (!empty($bookingInfo)) {
            return $bookingInfo;
        } else {
            $this->db->from('cr_orders');
            $this->db->where('status!=', 'Cancelled');
            $this->db->where(array('car_id' => $car_id));
            //$this->db->group_start();
            $this->db->group_start();
            $this->db->where("'$from_date' BETWEEN pickup_date AND drop_date", null, false);
            $this->db->or_where("'$to_date' BETWEEN pickup_date AND drop_date", null, false);
            $this->db->group_end();
            /*$this->db->where("pickup_date >=", $from_date);
            $this->db->where("pickup_date <=", $to_date);
            $this->db->group_end();

            $this->db->or_group_start();
            $this->db->where("drop_date >=", $from_date);
            $this->db->where("drop_date <=", $to_date);
            $this->db->group_end();*/
            //$this->db->group_end();

            $bookingInfo = $this->db->get()->result();
            //echo $this->db->last_query();die;
            return $bookingInfo;
        }

    }
    public function delete_supplier_booking()
    {
        $booking_id = $this->input->post('booking_id', true);
        $this->db->where('id', $booking_id);
        $del = $this->db->delete('cr_supplier_booking');
        if ($del) {
            $data['status'] = true;
            $data['msg']    = 'Booking Successfully Deleted';
        } else {
            $data['status'] = false;
            $data['msg']    = 'Somethin wrong in booking Deletion';
        }
        return $data;

    }
    public function delete_order()
    {
        $order_id = $this->input->post('order_id', true);
        if ($order_id > 0) {
            $this->db->where('order_id', $order_id);
            $del1 = $this->db->delete('cr_orders');

            $this->db->where('order_id', $order_id);
            $del = $this->db->delete('cr_order_detail');

            $this->db->where('order_id', $order_id);
            $del = $this->db->delete('cr_order_comment');

            $this->db->where('order_id', $order_id);
            $del = $this->db->delete('cr_car_rating');

            $this->db->where('order_id', $order_id);
            $del = $this->db->delete('cr_payment_info');
            if ($del1) {
                $data['status'] = true;
                $data['msg']    = 'Order Successfully Deleted';
            } else {
                $data['status'] = false;
                $data['msg']    = 'Somethin wrong in Order Deletion';
            }
        } else {
            $data['status'] = false;
            $data['msg']    = 'Somethin wrong in Order Deletion';
        }
        return $data;
    }
    public function add_supplier_booking()
    {
        $user_id    = $this->session->userdata('crm_user_id');
        $booking_id = $this->input->post('booking_id', true);
        $car_id     = $this->input->post('car_id', true);
        $from_date  = $this->input->post('from_date', true);
        $from_time  = $this->input->post('from_time', true);
        $from_date  = date("Y-m-d H:i:s", strtotime($from_date . " " . $from_time));
        $to_date    = $this->input->post('to_date', true);
        $to_time    = $this->input->post('to_time', true);
        //echo $to_date."###".$to_time."$$";
        $to_date   = date("Y-m-d H:i:s", strtotime($to_date . " " . $to_time));
        $condition = array('car_id' => $car_id, 'from_date' => $from_date, 'to_date' => $to_date, 'booking_id' => $booking_id);
        $chk       = $this->add_supplier_chk($condition);
        $id        = '';
        //print_r($chk);
        if (empty($chk)) {
            $booking_note = $this->input->post('booking_note', true);
            $data         = array(
                'car_id'       => $car_id,
                'supplier_id'  => $user_id,
                'from_date'    => $from_date,
                'to_date'      => $to_date,
                'booking_note' => $booking_note,
            );
            if ($booking_id > 0) {
                $id = $this->db->update('cr_supplier_booking', $data, array('id' => $booking_id));
                //echo $this->db->last_query();
                $data['status'] = true;
                $data['msg']    = 'Booking Successfully Updated...';
            } else {
                $id             = $this->db->insert('cr_supplier_booking', $data);
                $data['status'] = true;
                $data['msg']    = 'Booking Successfully added...';
            }
        } else {
            $data['status'] = false;
            $data['msg']    = 'This period already booked for this car';
        }

        if ($id > 0) {
            return $data;
        } else {
            return $data;
        }

    }


    public function get_supplier_booking()
    {
        $booking_id = $this->input->post('booking_id', true);
        $this->db->from('cr_supplier_booking');
        $this->db->where(array('id' => $booking_id));
        $bookingInfo = $this->db->get()->result();
        return $bookingInfo;
    }
    public function get_supplier_car_bookings()
    {
        $car_id = $this->input->post('car_id', true);
        $this->db->from('cr_supplier_booking');
        $this->db->where(array('car_id' => $car_id));
        $bookingInfo = $this->db->get()->result();
        return $bookingInfo;
    }
    public function get_car_bookings()
    {
        $car_id = $this->input->post('car_id', true);
        $this->db->from('cr_orders');
        $this->db->where('status!=', 'Cancelled');
        $this->db->where(array('car_id' => $car_id));
        $bookingInfo = $this->db->get()->result();
        return $bookingInfo;
    }
    public function getSuppliers()
    {
        $this->db->from('cr_users');
        $this->db->where(array('user_role' => 'supplier', 'user_active' => 'YES'));
        $suppliers = $this->db->get()->result();
        return $suppliers;
    }
    public function getSupplierCars($supplier_id)
    {

        $this->db->select('mc.id, ma.title as maker,ma1.title as model, mc.car_number as car_number');
        $this->db->from('cr_manage_cars as mc');
        $this->db->join('cr_manage_attributes as ma', 'mc.maker = ma.id', 'inner');
        $this->db->join('cr_manage_attributes as ma1', 'mc.model = ma1.id', 'inner');
        $this->db->where(array('mc.user_id' => $supplier_id));
        $carinfo = $this->db->get()->result();
        //echo $this->db->last_query();
        $all_names = array();
        foreach ($carinfo as $key => $value) {
            $all_names[$value->id] = $value->car_number . "-" . $value->maker . ' ' . $value->model;
        }
        return $all_names;
    }
    private function _get_datatables_query() // dynamic query for search terminal
    {
        $column_order  = array('cr_orders.order_id', 'cr_orders.trip_record_name', 'cr_orders.car_id', 'odetal.supplier_id','cr_orders.rent_type','cr_users.user_fname','cr_users.user_tel','cr_orders.pickup_date','cr_orders.drop_date' ,'booking_amount', 'status', null);

        if ($this->input->post('order_id')) {
            $this->db->where('cr_orders.order_id', $this->input->post('order_id'));
        }
        if ($this->input->post('user_id')) {
            $this->db->where('cr_orders.user_id', $this->input->post('user_id'));
        }
        if ($this->input->post('search_amount')) {
            $this->db->where('cr_orders.booking_amount', $this->input->post('search_amount'));
        }
        if ($this->input->post('driver')) {
            $this->db->where('odetal.driver_name', $this->input->post('driver'));
        }
        if ($this->input->post('supplier_id')) {
            $this->db->where('odetal.supplier_id', $this->input->post('supplier_id'));
        }
        if ($this->input->post('status')) {
            $this->db->where('cr_orders.status', $this->input->post('status'));
        }
        //Filter based on dates
        if ($this->input->post('rent_type')) {
            $this->db->where("cr_orders.rent_type", $this->input->post('rent_type'));
        }
        if ($this->input->post('trip_type')) {
            $this->db->where("cr_orders.airport_status", $this->input->post('trip_type'));
        }
        if ($this->input->post('daily_status')) {
            $this->db->where("cr_orders.daily_status", $this->input->post('daily_status'));
        }

        //Filter based on dates
        if ($this->input->post('booking_date_from')) {
            $this->db->where("DATE_FORMAT(cr_orders.booking_date,'%Y-%m-%d') >=", date("Y-m-d", strtotime($this->input->post('booking_date_from'))));
        }
        if ($this->input->post('booking_date_to')) {
            $this->db->where("DATE_FORMAT(cr_orders.booking_date,'%Y-%m-%d') <=", date("Y-m-d", strtotime($this->input->post('booking_date_to'))));
        }
        //Pickup Dates
        if ($this->input->post('pickup_date_from')) {
            $this->db->where("DATE_FORMAT(cr_orders.pickup_date,'%Y-%m-%d') >=", date("Y-m-d", strtotime($this->input->post('pickup_date_from'))));
        }
        if ($this->input->post('pickup_date_to')) {
            $this->db->where("DATE_FORMAT(cr_orders.pickup_date,'%Y-%m-%d') <=", date("Y-m-d", strtotime($this->input->post('pickup_date_to'))));
        }
        //DropOff Dates
        if ($this->input->post('dropoff_date_from')) {
            $this->db->where("DATE_FORMAT(cr_orders.drop_date,'%Y-%m-%d') >=", date("Y-m-d", strtotime($this->input->post('dropoff_date_from'))));
        }
        if ($this->input->post('dropoff_date_to')) {
            $this->db->where("DATE_FORMAT(cr_orders.drop_date,'%Y-%m-%d') <=", date("Y-m-d", strtotime($this->input->post('dropoff_date_to'))));
        }

        // if ($this->input->post('trip_expense_type')) {
        //     if($this->input->post('trip_expense_type')==1){
        //         $this->db->where('cr_trip_expense.order_id IS NOT NULL');
        //         $this->db->where('cr_trip_expense.is_latest',1);
        //     }else{
        //         $this->db->where('cr_trip_expense.order_id IS NULL');
        //     }
        //     $this->db->join('cr_trip_expense', 'cr_trip_expense.order_id = cr_orders.order_id', 'left');
        // }
        if ($this->session->userdata('cr_user_role') == 'supplier') {
            $where = '(odetal.supplier_id = ' . $this->session->userdata('crm_user_id') . ' OR cr_orders.car_id in (SELECT cr_manage_cars.id from cr_manage_cars WHERE cr_manage_cars.user_id = ' . $this->session->userdata('crm_user_id') . '))';
            $this->db->where($where);
        }

        if ($this->session->userdata('cr_user_role') == 'agent' || $this->session->userdata('cr_user_role') == 'customer') {
            $where = '(cr_orders.user_id = ' . $this->session->userdata('crm_user_id') . ')';
            $this->db->where($where);
        }

        //$this->db->where_not_in('status', array(2));
        $this->db->join('cr_users', 'cr_orders.user_id = cr_users.id', 'left');
        $this->db->join('cr_order_detail as odetal', 'cr_orders.order_id = odetal.order_id', 'left');
        $this->db->join('cr_users as supli', 'odetal.supplier_id = supli.id', 'left');
        // $this->db->join('cr_manage_cars as cr_manage', 'cr_orders.car_id = cr_manage.id', 'left');
        $this->db->select('cr_orders.*,odetal.name as fullname,odetal.phone as uphone, supli.user_fname as supfname,supli.commission_rate as sup_commi, supli.user_lname as suplname, supli.user_tel as suptel, cr_users.user_fname, cr_users.user_lname, cr_users.user_tel, IF(cr_users.user_role="agent" ,cr_users.commission_rate, 0) as agent_commi, odetal.supplier_id');
        $this->db->from($this->_table);

        $i = 0;

        // foreach ($this->column_search as $item) // loop column
        // {
        //     if (isset($_POST['search']['value'])) // if datatable send POST for search
        //     {

        //         if ($i === 0) // first loop
        //         {
        //             $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
        //             $this->db->like($item, $_POST['search']['value']);
        //         } else {
        //             $this->db->or_like($item, $_POST['search']['value']);
        //         }

        //         if (count($this->column_search) - 1 == $i) //last loop
        //         {
        //             $this->db->group_end();
        //         }
        //         //close bracket
        //     }
        //     $i++;
        // }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if (isset($_POST['length']) && $_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        // echo $this->db->last_query();die("dgfdfgs");
        return $query->result();
    }

    public function count_filtered() // Get total row count of search query

    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_order($order_id) // Get total row count of search query

    {
        $query = "SELECT a.*, b.*, pl.title as pl_title, dl.title as dl_title, a.status as status, d.status as payment_status, c.user_fname, c.user_lname, c.user_tel, c.user_role, d.payment_method, d.amount, us.user_fname, us.user_lname, us.user_role  FROM cr_orders as a
        LEFT JOIN cr_order_detail as b on a.order_id = b.order_id
        LEFT JOIN cr_users as c on a.user_id = c.id
        LEFT JOIN cr_payment_info as d on a.order_id = d.order_id
        LEFT JOIN cr_manage_attributes as pl on a.pickup_location = pl.id
        LEFT JOIN cr_manage_attributes as dl on a.drop_location = dl.id
        LEFT JOIN cr_users as us on a.order_by = us.id
        WHERE a.order_id = " . $order_id;
        $res = $this->db->query($query);
        return $res->row_object();
    }

    public function get_order_comments($order_id) // Get total row count of search query

    {
        $query = "SELECT *, b.user_fname, b.user_lname from cr_order_comment as a
        LEFT JOIN cr_users as b on a.user_id = b.id
        WHERE a.order_id = " . $order_id . "
        ORDER BY a.created_date  ASC";
        $res = $this->db->query($query);
        return $res->result_object();
    }
    public function get_city_name($id) // Get total row count of search query

    {
        $query = "SELECT * from cr_manage_attributes where id = " . $id;
        $res   = $this->db->query($query);
        $city  = $res->row_object();
        return $city->title;
    }

    public function getOrderInfoByInvoice($invoiceId)
    {
        $query = "SELECT order_id from cr_payment_info where invoice_no = '" . $invoiceId . "'";

        $res = $this->db->query($query);
        $row = $res->row();
        if (is_object($row)) {
            return $row->order_id;
        } else {
            return false;
        }
    }
    public function get_locations($id)
    {
        $query = "SELECT * from cr_manage_attributes where status = 1";
        if ($id) {
            $query = $query . " and parent_id = " . $id;
        }
        $res = $this->db->query($query);
        return $res->result_object();
    }
    public function getOrderInformation($orderid)
    {
        $this->db->select('ord.*,odetal.name as usr_fullname,odetal.phone as usr_phone,odetal.usr_email as usr_email, CONCAT(supli.user_fname," ", supli.user_lname) as supl_name, supli.user_tel as suptel,supli.user_email as supli_email,supli.user_company as company, user.user_fname, user.user_lname, user.user_tel, odetal.supplier_id,maker.title as car_maker, model.title as car_model, CONCAT(maker.title," - ",model.title) as carname,odetal.car_tag as car_number, odetal.driver_name, odetal.driver_phone, city.title as pickup_city,payinfo.payment_method as paymethod,payinfo.status as pay_status');
        $this->db->from('cr_orders as ord');
        $this->db->join('cr_manage_cars as car', 'car.id = ord.car_id', 'left');
        $this->db->join('cr_manage_attributes as maker', 'maker.id = car.maker', 'left');
        $this->db->join('cr_manage_attributes as model', 'model.id = car.model', 'left');
        $this->db->join('cr_manage_attributes as city', 'city.id = ord.pickup_location', 'left');
        $this->db->join('cr_payment_info as payinfo', 'ord.order_id = payinfo.order_id', 'left');
        $this->db->join('cr_order_detail as odetal', 'ord.order_id = odetal.order_id', 'left');
        $this->db->join('cr_users as supli', 'odetal.supplier_id = supli.id', 'left');
        $this->db->join('cr_users as user', 'ord.user_id = user.id', 'left');
        $this->db->where("ord.order_id", $orderid);
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $data = $query->row();
        //print_r($data);die;
        //$this->order_summarise_info($data);
    }
    private function order_summarise_info($data)
    {
        echo "<pre>"; //print_r($data);
        $order_history = json_decode($data->order_history);
        print_r($order_history);
        die;
    }
    private function manageEmailConBasedUType($orderInfo, $emailParams)
    {
        extract($emailParams);
        //print_r($orderInfo->order_id);die;
        $emailType      = $params['emailType'];
        $activeLanguage = $this->session->userdata('active_language');
        switch ($user_type) {
            case 'customer':
                $to = $orderInfo->usr_email;
                if ($emailType == 'reminder') {
                    $bodyContent = file_get_contents("./templates/booking_confirm_to_customer.html"); //mail template path
                    $heading     = "<p>Your Booking: #" . $orderInfo->order_id . " at SonicStarRental will start in 48 hours!</p>";
                    $heading .= '<p>Your upcoming trip information is as follow:</p>';
                    $bodyContent = str_replace("{HEADING_FIRST}", $heading, $bodyContent);
                    $subject     = 'Reminder: Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental will start in 48 hours!';
                } else if ($emailType == 'confirmed') {
                    $bodyContent = file_get_contents("./templates/booking_confirm_to_customer.html"); //mail template path
                    $heading     = '<p>Your booking is confirmed, and please enjoy the ride with SonicStarRental. Your upcoming trip information is as follow:</p>';
                    $bodyContent = str_replace("{HEADING_FIRST}", $heading, $bodyContent);
                    $subject     = 'Your Booking at SonicStarRental is Confirmed! Order No: #' . $orderInfo->order_id;
                } else if ($emailType == 'cancel') {
                    $bodyContent = file_get_contents("./templates/booking_cancel_to_customer.html"); //mail template path
                    $heading     = '<p>Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental is Cancelled successfully!</p>';
                    $bodyContent = str_replace("{HEADING_FIRST}", $heading, $bodyContent);
                    $subject     = 'Reminder: Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental is Cancelled successfully!';
                } else if ($emailType == 'print') {
                    $bodyContent = file_get_contents("./templates/print_info.html"); //mail template path
                    $heading     = '<p>Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental is Cancelled successfully!</p>';
                    $bodyContent = str_replace("{HEADING_FIRST}", $heading, $bodyContent);
                    $subject     = 'Reminder: Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental is Cancelled successfully!';
                } else if ($emailType == 'print_details') {
                    //This is used for Summary page
                    if ($activeLanguage == 'mk') {
                        $bodyContent = file_get_contents("./templates/print_info_mk.html");
                    }
                    //mail template path
                    else {
                        $bodyContent = file_get_contents("./templates/print_info.html");
                    }
                    //mail template path
                    $heading     = '<p>Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental is Cancelled successfully!</p>';
                    $bodyContent = str_replace("{HEADING_FIRST}", $heading, $bodyContent);
                    $subject     = 'Reminder: Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental is Cancelled successfully!';
                } else {
                    $bodyContent = file_get_contents("./templates/booking_confirm_to_customer.html"); //mail template path
                    $heading     = '<p>Thank you so much for choosing SonicStarRental. Please see your booking information as follow:</p>';
                    $bodyContent = str_replace("{HEADING_FIRST}", $heading, $bodyContent);
                    $subject     = 'You have Successfully Booked at SonicStarRental';
                }
                break;
            case 'supplier':
                if ($emailType == 'confirmed') {
                    $bodyContent = file_get_contents("./templates/booking_confirm_to_supplier.html"); //mail template path
                    $subject     = 'Your Booking: #' . $orderInfo->order_id . ' confirmed SonicStarRental';
                } else if ($emailType == 'reminder') {
                    $bodyContent = file_get_contents("./templates/booking_confirm_to_supplier.html"); //mail template path
                    $subject     = 'Reminder: Your Booking: #' . $orderInfo->order_id . ' will start in 48 hours!';
                } else if ($emailType == 'cancel') {
                    $bodyContent = file_get_contents("./templates/booking_cancel_to_supplier.html"); //mail template path
                    $subject     = 'Your Booking: #' . $orderInfo->order_id . ' at SonicStarRental.com is Cancelled successfully!';
                }
                $to = $orderInfo->supli_email;
                break;
            case 'admin':
                if ($emailType == 'cancel') {
                    $bodyContent = file_get_contents("./templates/booking_cancel_to_admin.html"); //mail template path
                    $subject     = 'Booking: # Order No #' . $orderInfo->order_id . ' is Cancelled! ';
                } else if ($emailType == 'confirmed') {
                    $bodyContent = file_get_contents("./templates/booking_confirmed_to_admin.html"); //mail template path
                    $subject     = 'Booking: # Order No #' . $orderInfo->order_id . ' is Confirmed! ';

                } else if ($emailType == 'success') {
                    $bodyContent = file_get_contents("./templates/booking_confirm_to_admin.html"); //mail template path
                    $subject     = 'New Booking by ' . $orderInfo->usr_fullname . ' – ' . date("M d,Y");
                }
                $to = "rental@sonicstartravel.com"; //Admin Email address
                break;
        }
        //$bodyContent = auto_link($bodyContent,'both');
        $data['to']      = $to;
        $data['body']    = $bodyContent;
        $data['subject'] = $subject;
        return $data;
    }
    private function OrderSummaryHtml($order_history)
    {
        $pickup_dateORG       = $order_history['searchData']['pickup_date'];
        $hoursperDay          = $order_history['hoursperDay'];
        $datewiseDestinations = isset($order_history['datewiseDestinations']) ? $order_history['datewiseDestinations'] : array();
        //print_r($datewiseDestinations);die;
        $trackPrices = $order_history['trackPrices'];
        $nextDay     = 0;
        $summaryHtml = '<tr><td colspan="3"  class="summary-section"><table class="summary-charges">';
        $summaryHtml .= '<thead><tr><th colspan="3" style="background-color: silver; padding: 5px;">Price Summary</th></tr></thead>';
        foreach ($trackPrices as $k => $item) {
            if (!isset($item['title'])) {
                $ii          = 0;
                $pickup_date = ($nextDay != 0) ? (strtotime($pickup_dateORG) + $nextDay) : strtotime($pickup_dateORG);
                foreach ($item as $it) {
                    if ($ii == 0) {
                        $hoursText = '';
                        if (count($hoursperDay) > 0) {
                            $dateby    = date("Y-m-d", $pickup_date);
                            $hoursText = (isset($hoursperDay[$dateby]) ? "(" . $hoursperDay[$dateby] . ' Hours)' : '');
                        }
                        $summaryHtml .= '<tr><th colspan="3">' . date("D,F d,Y", $pickup_date) . " " . $hoursText . '</th></tr>';
                        $ii = 1;
                    }
                    $summaryHtml .= '<tr><td>' . $it['title'] . '</td><td>:</td><td><span>' . ($it['amount']) . '</td></tr>';
                    ?>

                <?php }$nextDay = ($nextDay + 86400);?>
            <?php } else {
                $summaryHtml .= '<tr><td style="font-weight: 600">' . $item['title'] . '</td><td>:</td><td><span style="font-weight:600">' . ($item['amount']) . '</td></tr>';
            }
        }

        if (is_array($datewiseDestinations) && count($datewiseDestinations) > 1) {
            //print_r($datewiseDestinations);die;
            $titles = $datewiseDestinations['titles'];
            unset($datewiseDestinations['titles']);
            unset($datewiseDestinations['total_distance']);
            $summaryHtml .= '<tr><th colspan="3" style="background-color: silver; padding: 5px;">Trip Details</th></tr>';
            foreach ($datewiseDestinations as $date => $val) {
                $summaryHtml .= '<tr class="date-heading"><th>' . date("D,F d,Y", strtotime($date)) . '</th></tr>';
                $summaryHtml .= '<tr><td><strong>From:</strong> </td><td><strong>To: </strong></td><td><strong>Distance:</strong></td></tr>';
                $summaryHtml .= '<tr><td>' . (isset($titles[$val['from']]) ? $titles[$val['from']] : 'none') . '</td>';
                $summaryHtml .= '<td>' . (isset($titles[$val['to']]) ? $titles[$val['to']] : 'none') . '</td>';
                $summaryHtml .= '<td>' . $val['distance'] . '</td></tr>';
            }
            //$summaryHtml .='</table>';
        }$summaryHtml .= '</table></td></tr>';
        return $summaryHtml;
    }

    private function manageOrderSummaryContent($orderinfo)
    {
        $order_history = json_decode($orderinfo->order_history, true);

        $pricingSummary = '';
        if (isset($order_history['trackPrices']) && count($order_history['trackPrices']) > 1) {
            $pricingSummary = $orderSummary = $this->OrderSummaryHtml($order_history);
        }
        $summaryHtml['pricingSummary'] = $pricingSummary;
        return $summaryHtml;
    }

    public function modifyEmailData($emailParams)
    {
        ob_start();
        extract($emailParams);
        $orderInfo = $this->orders->getOrderInformation($order_id);
        $mapKeys   = array(
            'customer' => array('ORDER_NUMBER' => 'order_id', 'ORDER_STATUS'       => 'status', 'CAR_TYPE'          => 'carname', 'PICK_DATE'             => 'pickup_date', 'DROP_OFF_DATE' => 'drop_date',
                'BOOKING_DATE'                     => 'booking_date', 'PICK_CITY'      => 'pickup_city', 'TOTAL_AMOUNT' => 'booking_amount', 'CUSTOMER_EMAIL' => 'usr_email', 'CUSTOMER_PHONE'  => 'usr_phone',
                'CUSTOMER_NAME'                    => 'usr_fullname', 'PAYMENT_STATUS' => 'pay_status'),
            'supplier' => array('ORDER_NUMBER' => 'order_id', 'ORDER_STATUS'  => 'status', 'CAR_TYPE'          => 'carname', 'PICK_DATE'       => 'pickup_date', 'DROP_OFF_DATE' => 'drop_date',
                'BOOKING_DATE'                     => 'booking_date', 'PICK_CITY' => 'pickup_city', 'TOTAL_AMOUNT' => 'booking_amount', 'CAR_NAME' => 'carname', 'CAR_NUMBER'        => 'car_number', 'SUPPLIER_NAME' => 'supl_name'),
            'admin'    => array('ORDER_NUMBER' => 'order_id', 'ORDER_STATUS'      => 'status', 'CAR_TYPE'          => 'carname', 'PICK_DATE'       => 'pickup_date', 'DROP_OFF_DATE' => 'drop_date',
                'BOOKING_DATE'                     => 'booking_date', 'PICK_CITY'     => 'pickup_city', 'TOTAL_AMOUNT' => 'booking_amount', 'CAR_NAME' => 'carname', 'CAR_NUMBER'        => 'car_number',
                'CUSTOMER_NAME'                    => 'usr_fullname', 'SUPPLIER_NAME' => 'supl_name', 'SUPPLIER_TEL'   => 'suptel', 'SUPPLIER_EMAIL'   => 'supli_email', 'DRIVER_NAME'   => 'driver_name', 'DRIVER_PHONE' => 'driver_phone'),
        );
        $emailData = $this->manageEmailConBasedUType($orderInfo, $emailParams);
        $emailbody = $emailData['body'];
        $subject   = $emailData['subject'];

        //print_r($orderInfo);die;
        if (isset($mapKeys[$user_type])) {
            foreach ($mapKeys[$user_type] as $k => $v) {
                if (in_array($v, array('pickup_date', 'drop_date', 'booking_date'))) {
                    $orderInfo->$v = date("M d,Y g:i A", strtotime($orderInfo->$v));
                }
                $emailbody = str_replace("{" . $k . "}", $orderInfo->$v, $emailbody);
            }
        }
        $order_history = json_decode($orderInfo->order_history);
        $currency      = (isset($order_history->currency) ? $order_history->currency : 'MMK');
        $emailbody     = str_replace("{CURRENCY}", $currency, $emailbody);
        $emailbody     = str_replace("{TODAY_DATE}", date("M d,Y"), $emailbody);
        $emailbody     = str_replace("{SITE}", site_url(), $emailbody);
        $emailbody     = str_replace("{SITE_LOGO}", $this->encodedImages('logo'), $emailbody);
        $emailbody     = str_replace("{DIVIDER}", $this->encodedImages('divider'), $emailbody);
        $orderSummary  = $this->manageOrderSummaryContent($orderInfo);
        //print_r($orderSummary);die;
        $pricingSummary = $orderSummary['pricingSummary']; //class="currency-box"
        $emailbody      = str_replace("{PRICING_SUMMARY}", $pricingSummary, $emailbody);
        $emailbody      = str_replace("{SITE_EMAIL_ADDRESS}", 'rental@sonicstartravel.com', $emailbody);
        $emailbody      = str_replace("{SITE_PHONE_NO}", '09 426 999 100', $emailbody);
        $emailbody      = str_replace('class="currency-box"', 'style="font-size:10px;"', $emailbody);
        $to_email       = $emailData['to'];
        $mailData       = array(
            "to"      => $to_email,
            "subject" => $subject,
            "message" => $emailbody,
        );
        return $mailData;
    }

    public function duplicate($order_id){
        $query = "insert into cr_orders( `user_id`, `car_id`, `rent_type`, `pickup_date`, `drop_date`, `pickup_location`, `drop_location`, `booking_amount`, `tax_amount`, `status`, `booking_date`, `modify_date`, `order_history`, `order_by`, `vendor_payment_status`, `agent_payment_status`, `customer_payment_status`, `vendor_fees`, `ot_hours`, `driver_ot_fees`, `driver_fees`, `driver_percentage`, `driver_salary`, `agent_fees`, `daily_status`, `airport_status`, `trip_record_name`, `driver_from` ) SELECT `user_id`, `car_id`, `rent_type`, `pickup_date`, `drop_date`, `pickup_location`, `drop_location`, `booking_amount`, `tax_amount`, `status`, `booking_date`, `modify_date`, `order_history`, `order_by`, `vendor_payment_status`, `agent_payment_status`, `customer_payment_status`, `vendor_fees`, `ot_hours`, `driver_ot_fees`, `driver_fees`, `driver_percentage`, `driver_salary`, `agent_fees`, `daily_status`, `airport_status`, `trip_record_name`, `driver_from` FROM `cr_orders` where `order_id`=" . $order_id;
        $res = $this->db->query($query);
        // print_r($res);
        // echo
        $query2= "INSERT INTO `cr_order_detail`(`order_id`, `supplier_id`, `car_tag`, `driver_name`, `driver_phone`, `agent_id`, `type_of_rent`, `paid_to`, `customer_confirmed`, `supplier_confirmed`, `name`, `phone`, `usr_email`)  SELECT ".$this->db->insert_id().", `supplier_id`, `car_tag`, `driver_name`, `driver_phone`, `agent_id`, `type_of_rent`, `paid_to`, `customer_confirmed`, `supplier_confirmed`, `name`, `phone`, `usr_email` FROM `cr_order_detail` WHERE `order_id`=".$order_id;
        $res2 = $this->db->query($query2);
        return $res;
    }

    private function encodedImages($type = 'logo')
    {
        switch ($type) {
            case 'logo':
                return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWEAAABDCAYAAAE664TIAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAKPFJREFUeNpi/P//P8MgAS5AvIcYhYyDyNEwAHLQWSB+DsTyQKwLcieqCqCj/0NAJJQdAsTpQGyJJMeAhQ3jI4vdQeL/R5NHN2cemlnmQOwGxPfQxGHqPYB4GziQgcQBNAVuUEWeWByGzwPYxLqgtCgQ38Sh1gmHuYFArA5l/wXiFUD8FYifwRS6ADE/VEM0DoemoBluhMXR6A7vQhLzB+IKKD8UiI8iybHhMAsrBhHn0SxTAmI5HI4gFNK5OEIanx4Q5sGSnHDiwZYRYY5hxKeIiQwDlwIxD5S9GUpvAeIpaOpPAvEcKPszlPaCFm0gs1qB2ArN/IuEHDxYizyCACCABoujnYB4HzEKmQZJwMEcuwOI/wDxHSBeBcQ3kZIkvFJBzqn2QOwKxLFQvgieouwOllyODYDEV+KpiGDYAYh5ccjD+MfRix5rIN4DxPpIDsameQIRZeh3ND2f0CwPhdL7ceh3h9KbkesJdB+gh4oIjvIYX9n5H4uD/XHYAQLdpFYkIByOJMgMxNxYksR/HPQ2Ag5G9pwMjur/CZJHng+1SmQeECdTWkog+yobiB2gYq5AfAZJThNKX8diRiYQL0LiqyCxc6HmMeJz7JCsPAACaEjWdgMMFIC4AojTgVgdiG+NBjJ5QBbarlMC4h9AvBuI3wOxOBArA7EIEKtBs94bID4HxKZArAHVr4WSlaF9gg6kEm85lG6Flo4CQBwDxNpAbAitmEvR6kNCnSlC1Y8JAf3TsNSlnTj0EOMWFhKqrP/QOhfE/gClW4B4IbR9gKzOB4tbbuFzHAg8A+IsKPsXNDA2APEfLI2O/9CqC18gg4AukZ67j9SlxtZg6SIyUmH8dThabVKktBFwYFBnWQeII4C4BIidkeVZsPStYLUPC7SdygfEL6FZZQU0u5hhK3rQ9GMDMUB8iZiuERAoYnETvlr5ALT7BuvOoesJQlIL8tMnqDyh8tIGiI8SUPMRiq9gdx32AR0YZoKKqUJTIAgwIsmL4Wl54ktZs9DsvIEn6+PK/rOg7LdQjM8N6GbcI2K04T8pIxPEtJCpgf8TGE6hxNxYaITjUwfqs2iRYK41kQEcR6kfqNmVZsTRsAY1jGcBMTeanCASH1Rzf8Fhbg0QL4Zm8f94svdOIL6KxBeG6kN3jz8QO6OZsw/JbBmoX2B4EaUBAxCAXasJqSqIwtff9AlSuhE30iIoIVuFLiQRjBaC2I9ZuDC3RtsgIsSVCikIJi6M1i0KgqAwiHL1SLNyI9im3GUIIkiE9aYz8N04Hubee9578zZyD3y8mbnDvJlz7535zvluSuEO2xhh3OeA5alPD5nGuddwwD1NHVyc9bOt4yvkpWXCAOEDoQP1HGEqapDK1I9Os8mTITj2MzL3tYj+LJ1sImSg5b1jEV+AVOwgp2wXYqiJQUr2gUPKCoSUJts7ItjFCQcduqOgbHEWJwyt5EG9GhHFnid0IpLTMIpQqVvCWurDtLOcVJX4s3vQD4cg370n3FKGrO2ORV9BvSJBMZP1SoXC1oP2nGjPEl6j3JInnRxgZRvdniG0ERbhG973kxjb8nwj9+AD/PYSThGeE34ih2mTHV2EbcI5QdG0VOQZ+v9NoHnWLrPyH8XYbxi9irLvjrYpCHarjmsZVq5m24KlbxNov0rYxzhl7CDcDreIJnh+g3n/IaGBMCxer8cg6geKRE+7Ug9OIv1JEaNssxHepniCf0DMMdgyQmXeR3BUDanBKlx9UhOJWtCyeJ0+InQ2QvnnE7wt2ot18P/XTOFga/chBl0S17N4fZ9EZPv2IMEFpUDUxG+y8nVCDQtHrfVGLHSTTdyHgwNk9JYUDn7JYG3EsQc3O5xvlAdowQ7ejXnNf+M3m8epHefgBtS7WVttwiG36zhQ+HW7JbyIecq5g8P2L47+g6VwcoAvhEKTC5kmjMMxc4QJ5fcyBvQv6SaEVi+utyoya5o5nCR8I6wp5x01x9OFOjjNReiU4LJCByn3PJm3nhd5gzCqoIEmD6rI6VlJnes7F7GHz474grsRSkon3CVsoW5TmY8YD19nY9rvB+axyGOMexrRj9srwizKO+jbR5gBZ13DtVzCjcoU61yubvhKugcxIueC6DeJel3MOL/YV1QVysiPl8+K+kVWDs+T42yvnfRN00q1Bxsvd/8I2D8B2DnX0DqKKI5vjKVpa622N7UPH8W2+aDiW9SCYH0gFcQHUSwaRamKiEglFRXqBysKkVofaD8Iglp8UrSNFlErahFakRqMKLbWVGlCG0WJSZNoo+sc9jd4Mt3dO7v33uQG7oHDvfuYmd3ZszNn/ud/tjbJ1cRH1hBdCggIC1y7hyG4H15JqBBaGX6HONYDr6RmxDUZd7kFKFz8kzfKUN8TGPnjwOxdNSOuSSVkKgD13wTOdDys3uhfMWXE+S6gkr91WhDF7OUFEI5Ag4qnHUJHGMm7eEm2Gv3M6GAeI76dwJJU/BpaTM7Dy5+lUPj9oPc7U8oJki2EBSFIfOzZqc1q6fi72n89bb/lUcdyUP2ZdNoWo+9kfLgnGl0VRMzpTqNPMm1aWaGiGD4yKfg/bUfLY0Y3M32nyVLuR4tc164SjViiL8dR93QMUKIm3xOw1HJMEKUWCeduZRCF4D9V7oS4HntjVorTMPSVKqg5wmjfHUS84mm8CGdiU8165LZGHKrwysNBlC8lhIx5RtcabXUafoHOXcFSVGQGF96kOu9Oo6fiMz3o1FEgNGWliYiuD/5xgdOJacljS2PwEjHaH3hIlzNC+OAQ+4zO5/866l0GdqLLvukYsTz4mxi9vjP6dcwLfUMKNnIZo1OSbDd6Pn7qBqOzedCN5cJXkJO439uCiBz6E0ZrZQf7LYk9ZIC7htE8C8ZlIZde4BO5rw9xP/qw1Raj2wKFsfvy6bbERNcl02WQ7Y6Y4y/HwAmWftxHyM8H63axfZ9gd+jEGEqFYPLi8UOUbcvQznLPNm185IOUuloqFCULHV5mp2pzP78HsJvV0KybCf+2Edl7j4RrGzFcYnSr0d6E9h5SbXQcCTAo1m/5irOdEVJLL2/VMqaHg7yN9fhPFvbpYV+/WhTIyCu57nOdOo8ORtO7Q1yFWWVCrYW2fVWZfMRO/DypWzI+7q2AH/oHv28bfR0NPSE1d4Fzsfr/qnNMXLhL1fZ6XKSDat9kZtoepvSj8GcbmVl3c02NuB0y464OIl5UAzNMHWiF1cnYxhBIxzDuxp9oH/dxLfZyLm6FzFRP49YMKDD7MJB4txOgus453qqOtUIFOqT4RCEkhEJM2Frexp9jRuIwhtMUOjT3PCOxlZkVGH3cqOmBMo3EFkj/LWHWCouMxHFyUcrovLZSHIaxVN9oq94nn5HYbHS+0efZt0qF1i19X7KW9mCU2zG8MMODeUAdP7kEI/6ywp14tmrrxhKM+J7QX3zdiTCFvpBH/mWav7XajLg9ow9oqQihYn7K9g6V59fC/0JK/kzB09/rU+f9ktMnzuLH1ifsPyLl2zF30cZXOY14nud1XpFwXppP3OFR9yJyWPPK8Hgb8aC6mH6j7zOiWrk/ofAZMNo2qXO71f9n4fNcklC+kNHAwhwLO9dArAzAshvISKLS8gWjkm/ZNCPO0g8bOXfE04hFm0pYlAo397mcM8SYqIsTzwES2wcE5SvHgtUuBlrZ6OC41SYXshCVBey3CSwVnzqmBOWnO2WRKSyQsorNPc4jAmslJRiOC5ej2iJ2LTGr6JvBCKtZ7iNgsS44PIUiiwiWLJS3TSUYWTEpgP9/k6NsN2VdGZ19NdZSZStNkSuNrofL3aamqbnOucfzu9DBuOvUhxZdvy8gkyuIyVKboTLUrC4u8u2PtCl0agwysEh9ljCtvukxrsE5aoFrPzIZ0F9uHSeo7bNwJ+z2khg6eJp+kuA6fD4R0InxMuK0fXtZRO5yFmI7FRwnvM5nUviZcnxNQnsfOV8fDYssAH8tcj/tDvwWet5/3GK6Cx1g3wZesmK5AQtBJ672NOKXEoz20WqF2CZa5vgCgitNiogyAhh/hwPSp3E2JiUECoap2yfh+x9A/hdVMGKbU18dwaNTsrp5QUR9bFfbC9C7Vf0NMWVf4djp9MOP9M+7Cf05rPgu8iGQp4LRH9iw+ki1GsVEYrHVEcGZUyOX1UTLfwKwdy3AWk1ReN8eSoo8+inRkLquHsiUx5CMiqRwZRrSoKm8GhUjlGaKJMZIwpTQU6SQRyV5xB2PMdVMD7rc8gq9C6GH6tfX/21n/fues8/jv3/33JmzZs789+5/733Of846a6+91rfWSqCYCbnRUdxkY9V6IY+bzISBE8o7AWY74cDRhvgG4C2QuX0jMR4w72m0XjUeQKv9QxPrWhU+6jFh4IRiQQAKIb0tILPwH7RneweVAVFtSxg4oUNBYEIgyBA6NJNSNRe6WWUcRONVxsHUkHuchIETyjshCADBD7cZm2qoF79RfdDwyo1shwcU3kQ4VlLUrQH17cKxgHsC2nmMcqCnCQMnlDMh1AqYYIT9yARqjVR26JUkMPXR7HM250hxnlpk5Opk9N1kbphE/6DejGiVxSqg9zBh4ISCUmeVKcvVikzXQDnlHNapDBAeDFmDui9A6scKxq1N5sZ3+3j8TYm7gfowGL8rGVwT8DoAuj/nx8AIcES8HIzviPUazYn9CGYWlIK5n6aXFtRt7vIZB3Q+HAuvBbyBuCGXqYxNeLZo13Fw0KX8gElwViAG7BzunL+MqNNhCS3mQ0BM3kvGg24Ycr6lqnxy/js5z4MBxl+nHKAS7g/MXu/nyLB1yAv1ubQfSQsDeAJRF1uN/meRuRFrh1wUk6hC/EomBaCqRJUPkm3CsdLh8gUPvAyN+QJ05XcIvB3yf0+65lZFgM8hLeZ0YhhuEGnSuqUzBSc/d0ltpozEp2Egeqd79P+abUMtY+8JABP8kkB+2zV0tIwf5oMlsNEQC/yya0gYqkktIrprC4jlaMs0TSgt0o6pPFP8W8ZTvidqHwCv0T/CObcacY+4pyOYlqkv+ywgLqNYlzjZpJyI13HUQSDVbLFh2l2KzxvZ9hAl49tSwivvuK99ynHt7lf+yci8dJ19Yg43+ldlu4ghpRdy3HncDYMQCYwEZtMt17CIn3Brj6Q+N4i7dIx9hH1M/RASRKcSmKLKu7S/Nv4fJ/5+VwWHL47kuc4UGyS9y98QUgLjfq8x2jAnIozf4v/XUq1QXNlwT+dwRbiTc3QW982PELG9jPPUpx48gvPohHHL+fte1xLYJgUnerztF4gxWvLuYFtjIqKO86l2stOQFIt83s5Cj2tdIcKhbJLpVsvcNRhC9YCljwZ8z3P57vB0+XLEXvnXVQSJWi9idPUmtm/KIxCrHVOIT2XbtWyfJvr14v2bRJ7qw0AJc76UyH/vVl15ivF7d4aNFFgmJph94NgmxvZjwmf9/QcivB5tCz0YeJi4hicqkIEfE/0rolrXUs7VM8LYMJEpJ4i+s/i5NSID981zJEURQ5oksk9H7KwTauIKMu4dRMhdzZD88eSjxeQPTVpwjjLON5yIQNcqC9cEuODevICL+f+V1HdXClzuYL455wu9sZ1R5EAycNu0U042TR26Ihg4HSJvQ5DjyRyYIQwDaxoeIhTJq88gn/HAMN/FEKZvRGb2S0JI4BEe3zUlb1xFnfgSrtwpFwz2MJ53F+fES3w89yTd095ljQ8yXanKZF/RFoV+HvoJSo1O5M4yRf2oEU0o39GcggI1A2g6gV5dSn0FO+K7VQbq9wrn20nTis7Ms4g6EOhUVT5ZXCHnU4ZOuIK73wcOHGPYBivDEpe+uZLUw5tSFw5CMnOR7XqqK6fwhe6n9yhlyoGTel2XOfduOg1W8t5ramXYWb+hBWqBMb4TddHttALV4X4Ceu0NtD5NVZk6Oimazeryudbm79HFP3TutL3cl+zh8TsPWE52iHOtoCVjMufFb/iA16RMK0SpoXN196jLgLelxEj5nxaFK9IEfO8w3hgdnFjiIoFl8OYqi8QII4GfydPS2cS4T8srWAJvZJ+SkAGhmt4goH6txZIk9fEZMQCnV+NK0ICBC20psXtSd37MtjeR//QwfvQvRmck5TiL3xXyZOtYkWC4GHc7k6NMYchOJ3ETf/BhYJkaK50DA7+e53Dwn4x71aWCGNgvfcBTIc1osz1ql1RqhHG+IjPmUNyvFj5vuWSuohgHXcTlE6gipNBsJvr9SWPzJzRGSxPRZwGW21oBTGd+9IPLXBVJTegm1TTfsrwHpfsMc9VV4niE7QN95uhOs56mHoZXS9EsJe9vmqpfH+UkO6w65MHZZ7jkZ2hAxVzrzpCUNbkrnExFvAUlojbqb2ZuCdBoLl9+EtiUKnsjSODjD6GU2R3gXMeFUAP8aGhAyV1iOefYiAlO1nNsUVUI8FwqPFTyRs0VHiKpByNX2XXczeoiStNc7MZBGVjmZVuWdqpOhbVCzDlEgam5MPDl4vu309mlx/TxYQAVw2zXu/ptHhalvencaH4cGNjLfap14lIjU0+RwbhbXG6ipg7cUDxuhIgHYWDzwb8fkoFvFP07BrwhzSzfvZhHBtbUO+CL0jLE+TXdYZl3YI6MfEVl6sBA+9zjol1owMwE0bacpjaAL8bS3LWGpg7obS2p5wI5dBPhcc/ThPN9BA0HprnrhUknDM0gkES7gBdb+vbitU70MXG5pahtk6MWJ/ch0336ahfzyhDza534WZ95dRRya+WeZd5G8+hWrxQdWNIX9Kb4gXmeZmbKyzzeyO00tdWzSK4gElieLx3BlWxWIZX53da5tK+0XMPHBtBkHj81vRxRAq8WOSDCqCuHRTCzrY8g5YrT2UmuvejbylIhmlvSbdoGn8h+ZfTINSDT9hQbm6YBbmr7gBe7xONBbRabRJsbeo/PAxgZ4Bre9Bh7t8+4lMe1V4+w2XzVZUwYE92EHBimOG6J/0xA+7n0pHwU0oR1Mb0zBfTmlMQ8JKaQ6LXV9CyGpYbEyv6s/GuB2Chq0r5cVZb9uSzaHu0fK6cc6SGjJCIjoTDU3BI0UCkZK+OUYgr4iQtcJF3cs/FgEzdK4DiiUktiq7vk+Xp7Rxx3oYV561TWzY8TA7dy8dQBGN4izn4g7u7hdbxa5ZbAA0VzjqCHL5/L4rQIYx62qIUFlaAGxZKB8dCGiIeHz0uFK7SZsZRp6mzMc5pwd5vu6a6G3q4s86T46aXXwX2MRB3n00w3wFhGzfl0haaOFkYA2mq0x/emZG7KFaqxIQSGG6tW5xyeycl8Dm5xeR1UDIqWxy1L5TnKwWCcwWVVEwoK1uMGbKFg8lMNiVVGOyaCRh9iWzv22cFPlJ/qT6npNQ9s2YB59uR53RjKSyoBJniKMR/UDARvXq6yixZqQoQuYI2AQD7q8nL/Ycy3hss6bPONhB25SLyo6H9SBImO6GD4B35y+e5eMu4ncWCYGjFjYJ0yFQ94iyEl6/JB/MOlVkutMS7z9OUnHBlID7qPDPQp25GqFJG2wLLO5Ty3qOwKlDVplYlCwOAWq+w6wdX5gnpRPf4urDpwLAxlOxwRcFzMchkzm4xfxPuGCO+vlFPyt0Blx9f5USFfIlOwbeH+pCxm/BLbPMFbPNpfM/RkMPT9lnn+tbysNQXD4nOycoo4BtlVH87zS8TbeH5CWr6hynsfD7PMt5+qkV5duglV5R1K0gKX31FLOWaxmiq7qCGub2CA+z2YfUsFT2C16KWcoodlcWSUqpboWsMMNSFi9y8yfC6JmB/mA1yrytc+9qJd1BF1oui0kHxD+f92Xl9YQpSzju5GtEtrzrdTOREbboSo5M+FyoDfsp4v8iCX/s+y72gKhv7KcSkjF9rMuDNEVbIDQ7r8qSrL555QLKkqSeAfhe6bUEIH6T8B2rsS4KqKLNpZSAIJCQFBFFCEsAq4gQsIRTGo1MjouOJCFeKGC1gqjiKOgzoqMIA6Ko7iCIogLriMgyI6royOiiMKrgwwIWBkoiyBBAMYMjmV0/UvnX5/ef/9nx/Tt6rr/de/X79+/brvu33v6XudJc6RI0exElTCg1SdEQ+bsg/jxxDrJGzg/oSKh3LXVY4JO3LkKPnL1SOpi4C9DTbBfKFbyFEhJR803FqJpj0v6P/0byjeMlTIi1wmUzqPuSqkEMxifdCtQLMMT2X/4NExYUeOHP3iSQMZNqdIe2BXxn79i1QdAELvE7uZjNoxYUeOHP0iCQAjWMQG89iGUu0+MmgAbgDqgfqihOfwnF6pwlvsNGWzTh2ApYASNs5bqJBvMzA3WPN+4n2Q4DMN/oHHU0qHt/vnKVlDcoe/M+BUtiWrsxwTduTIkR+SKgK/BFgw0KdFTECl9iZzPCDCtWDasNLrqFg7mLedEm81GfBeMuN9zNPqDSBAy8SzFFC1sZP3xzlAcT84JuzIkaNUpRZknB0omWL534mMLofMEVLuGjLAQjK6ShXat6KZJNIeqgmqyFQrKcUGtQEI7WpJabiAxzbiNxDXAGICJNpT2Xc3IrjWV0wwPgLNvELFsakq1ZgwXurhfAHYUgi4dzkfeJcb844cpYwUrGKUhJtxbvck42tBFUAa1QgFPGYxL58MO1uFUPDfUXo+WKgbFFUY2siXzt/bKCVvYZ25vEdrMuJ8Mt5ofNp6uZT3os9UHdAZe7ywU6AiViYMxfUlKjz+EqL8YorrK3y+SMRnupvLEHwxsT3hXeqMPuJSAbolILw716aRZMqKTBm7Ql/xcd8DVX3DwUifdXlRH1XfqRn2TL0XZmnVWpxPV+G3n0Siy5mOieEa9Dk81QBdvzLAvsDkwy6CiSr8vjZIQU9xTH1m/Pc1J2+iCRsUJ0coAziW3H6zidJfvFQSQz0lnCsPc7I3BKWRkTYXjLIZJeACMr4DyOg6kHn24JzOpbQLSRfOyLE96H0ev47ADDE+rk3wswF+N1fVdwReSQExnwz/XfKtpfxApPPZtpJXYaMu9MwbKfGvJ9/auP9T1fmeGhCHy82FMfi4uo7XPFub8ujG9Umjvt/XpmOED64F4r8fGS2nFaNLlkURG87mgO8nj2cZGpAvrx6WusM5N1xllJ0U4/0GsG+CpnFx9MHwOO673qhrbU1yaHqEZ+rkcd1/AxgzH8bZ9mFJ9leXzZgneZyPbehH8XA6A0X8mo9rUzFdL2+kf8U7PaLKIkzJLPpqrBEh3EyXyTfyvyWMZZPIZ5zk4dyyA/2knyD8q2t6QoQUPMKIt/MS+dsnOgKtdlw51nKjmXSqaGtYFv0ol7DsnCgeRgcU6s0HqOD5HjKPLUzvGIF8FotglKVM31uch0/h+R1xMuGgJlSymPDJYZ6jhP3RL0Id+axnDmP+SDrP5/Mf6YPBXSre7b/j6PtdUURr8BtWujJMfy8LmAk/Fybs3ziPMTwlhWP1dCVDnSl4Rw3jYeL/Qxg3SsYr/TWZuBxDLY0x8w0DqK2jN+RptWkMBZOuDPrgt80djP69WzB+HcQi3XJdH+EsFvOqv1HPbobs7IznVRYJqpuPyGqRIggPrk0HWTzNnlhTF5f8bbreVpSs3xYPMZkdX0YJCaknH1TTWpafb3ENHYkJ744hgFWqMOF2Hm3Gx6tvQJNmZAzxPM30jtGuM330X7bPeKkmcwoqjutSo969NaFAy0EwfJMJvxbFNbMtY+D4RhRE7Woj8LAONfUS3cLPCNP/NQyGdwclThWBDxUxvnBRDO27yVgVXkwmrwXFaFf++P2Qx5zdY9PRQd9ydQz6kw0e+RepOnc40AEBh3eUAU2BTvhZkQevePBedwF1hwXUq7xJ3fG9KuQZ72vqdqFz+QP1LtAXjaEiHDrr01XIe0g4yhJGA6mLPJt1zlR1DsNShWCUMOP0Qk+FLaTrArzPkgDrmsj3Ei19m2KGKDilG2Ho8KHvhG+yD0U+Av4B4vRAktoFr5NXGXn9jDbFq/dVYj5WKX+efLzoBB7hpmocfwMS9ltRBlhjeNlczv8KaXTTxjbwjfOpm86hbrqa+tkd1NWWUXdfzbmexZTDOnKE4a5bGJvJhaLNZ7BeuIO9Xe3v9kvaZnQ/lnrUuwidiyiSMnLNVUxwiPe4z87N5rWPs577yCB+pmX0MhrHzhQwFTDcm/lC+tI4lKZCrqGmsBPSRTt/5uTYSSNBfyq99eDRcJhI1J8DN439IZ0KIlD29WTS36QAQ1ip6vth661CUZtSga41jHsD+Q4xWZ5uZEgAOI4cbeT1EBMT/y0Q/91Pw8zCJLRtlCXv7SiuO5lzCe7c2kYw1r5H4/tnlo+/H+pBA+gZPEffhXPMvlwlJ6yJFsaqDNRHNhl/OYWxZRTyYKBt51GX/DjP59iv4LPCqJcn5nAOmPBjlFS/VCF3tIpS7DxePE2FwvpGQ8/SMj+YzHE2meXfKeUM4UAFDeUX7ltKEEViAm+nlHsXG53H+tYJ6XkpLawn8bqRZEpf8r7jY4CgaGTCdNarpZ90St8VRD5saCCGMNHy4kelGAPWEJ2D+Q4K5VefSRGFMDXFGTAs2bcYeV0oCWtayI+3DAW4gGMqHh+KXtjYnpQabQgBrCK9nI0Op+RsUhV5wKOqziV1tJRORE8rFXLo2pbvu5DMJov51WRce/mRONOoSyMs8gSTyqMQlSt+Nzck3kwV8hmRQSbajL8zjZTBlC6O6Zb8DCEty3pqxCpoa4Q59wU/cK+JvPu8uU99o9sHEayw90ShryujFfMI49qjDV3WRzw/NoLRQ+t/iqhs1xbUM3j9NaLcJuaVCF1xJL1huMjOpZa2PNRAOuFFRjlERW6f4nq/XGHE8KKn2Nfx3itInXD3GIPv3mwp3ylJ6Ih3IszJyZZr7m9EuuNfdEq3YDUH8suewaWYSdepkAfo8z14ewW/ittFHjCNn/K6q7gcgdj+Opc9ufwK2qRUtOc2fuVH8Ks1iXrGUi4BS1heS9jtxW+/VE6JzozGciXbdVeSJbO9FmkkM8WlyUrq+NIowfzJUuZ8jhX06Wkp0OaWFr001GGvhrlmKiVKSSUqsfHKHma/DqWkGc6OYNIEzrtbeH2BctQwFAPHHu8Rn/hVS9nfidjMa4WFcKi4bivziilZZooA5R35X38RxFsxGPdsWlAltSAUpYYYvUP4e0yckrCZJnhIIkOSJAlfaKl3fCOVAACBnOjRnxuJP20ISTjbCPoeL1VwNRCrJLzYUuZYI8a4plXErYar/3ofbcdKa3Nt+pzSNhAKzxPBNJdxuu8ldGsKx+LpRENhFdDMSbrRBbj3CyuR9KClXDlhXt1EOeQdxkF5IF+kZrCZPC4XOFeNWTaXiN2JfwV281RRfylxe9Ueqoh4mbBOH3jAw3IFTjBRELVPLXX3buQDcYjlmaY1EBMuTsAmkC0JgKj9x3KfsVHcZ15NwxI2cNwqsLROHeFDeIax62Ijb4SlXAGNAbdxyVRNdcN6GsuyiJyAamI1l9p30ZiHiGj7aGS7gQY3DUGbyaXiKlUXeEpDqZ7gsgsqiLUqFKg2ETSQS+s1Iq8d1TCwnhYbqpggCVC0r4w8GMDOTZHFVYaKPeQQLPDXGHmnNkDbYXw91OjXNJ9JbudvTYNOkAF9u3E+SJqrIrsRGCvaeAjVi8uT2McDVB20dLUKOe55g2iDJknp7ITvIkBVTBpknG/0KFeoQrFlM2m9XUX0gdbhItJ1XzK16WS4f+ZkWEJr6kBObuRrvPEjvPZbDqjneJ+/CQhRImk379PPAv/ZSatxougIfmgkPcPnHxtA/ejXN6nDj5WhVvO60hj1jOYkXJPkuTBe7e+fYi/72S8dq/bHjoIRPxhwm+dbxnp/joPWUVy/kVb7IcYHBAiQi4hkSYZf3eEUzmoSLDylrE74c2O58HptOpu7TKTYDB3dFZblxT5jK6FXWi32U+ul+SUee7P1VsOTLNtQa7idsAuRAYOYtzyOJetxASwrrouwDEvEtuUOwn+G19JvMvuoldiKns5z+OgYzR1L/7Ncv8RnX8w36lnBXXP5FvTJNYa/AE1HJVEdMcZy/xMDGBNtLfX+JcAdc7b5FZTvj3C6/BM5rl4Ps+M0Hrq1qemE/xhHZ73i88YrxTbPo2lwOI8TcjqNEtjO/Fhtup17xEfQUAeG8nQcTCJRTFinOUlkwtL/Q9C6vh8JM/TTB2PjuO8LSYaoDbO04YKAfSaYdGcCmLDi3DFpQQPo96HPfyPO8de+qTBh05UldoVdyuVBD4KWzeULNnY8oPZ35+eXcgg8vzJKFcIqqiEeiwDJCUeFXGo3F/C3Uaw7KMKuQGyZPoDL80j3eLE29VIhz/8z1P67GGMluEQ8i9AjBF3sqEI+YE1IYhnB5XDL95aqc1IdJPXlswOS2N0CqdtAldMMi647nne7j+MX42WWR/lehEjuEPpsqMTmBdwH3QmnzGC78qkXfVSUWURVmx4DeB+X++zvp1iH9q+LfhkWQP/6oYVUQ8YKxSxU0e12bfTkIms4cuQokbTEh5EVxrtPmkoHZbox4siRowQQpH0YaLvGcE2xaoIoiXQ3Vqw0UoXgMwPC9F0py/zGdZkvAhxxhehrmRDOqqG8193n0abG+J6Hse2PJOl+ejdpeQwMGCqgNNVEYWqOCdvpJ/Flho7U9PyFbdI6ppUs7yg6aseJCnzwPcqOs4VeG+4HsxugfdqLVgvRnqGqTs//ZJLbAnhmlziur+JxawLbCIc8/+Q7fSjKa15SIWjcpKY8GRwTDk9LOUj6cIDdyQl6DvMvs/TjUSxrYkJfZr6OkwdV0IuGpGXzugWfGjBUwAPd+6Lsyx4TFv9hw8iXoiy8rwHPvFPkPWy5/iajPfAtYAs9Dh8Km0W/IGEzQjRYczBVBGDcwD5c5FFuC//bbZGydIKhs6Pl2rm8DgaxClE+2hhu2lDSWiytrxZjwkZ4ltVG+yZYyj3B/+YYZU2/wMcxH+98HX+vN/rRXEXcmKR5gXvfwtUK7gtj/aAI12zmONQfNRhqlzsWo5TbNmhPv7LAewA12sk9+DpP46ZP9gg7tIjnOo5eD1HmMIuXLZvPgFzm/9VSttjIG8j82w1fCKbXrHTmvSjygBs2Y39VMXqEFwzPT1yzIl47L8br8mrqh2r6Tmx7l2kq88/x+f41bPNNYq1BO8KUH8wy51re0S4PDLWM8DCKeUM9oH5dPLy8tRHnbVh2tMeYmBrHfEAYnlkx+tR4rib5ce8aZXKGuegJ0kjLKMsC9oSt3K9R8m1LKVV65oKD7LMoPa2gVKyU3UM/6GcP6ctG0ul7cx53iTwtse4Tedsp+cEXMOBB2J6+zSij6SAe3/LRj2sphV9BiT2Sk3cd2ryCEjQkQ+xsQ6QVQMtsOxO19P6Cz3edwyMkWQ3rulxIsOOM8sfzuJvvO4ft7kWViu2dNRd5uz3eZwdxNCGha1QoAAHGzgeW9+yX+vMZL41SdfMqpeEXwoxfR04dERNlCp1gOMoyyktaxkkMhgdXjv8y/sdybLGqc4APHwA6rEobLqPTwkxaTXnGeYbBROQ7zrEwAvl8rfihAQM+kh+J9nw2GCebWRi7X4JaYTDVDdrp9wKqe2ZTzaCX2LKPsaQFDrYzdfV6m/Ephloix+gPv0xYzg8w3yLBjNuL/2ZQFwo95xS+y8Op895k1NPc0javcfQFjzAUjiaTGy4+CtM4tibwfiBzK71tTEg6RbwHnVYYDBi2kWeokulq6O7R5tOoZnEM2Ac5nLD3JOxI6fDHMOUgGSPWHXxvmMY5hGq6m0aH6WHqGESd78e8ZzdO3mrBMDtT6pMRHQ5lmU2Grq4Ty20TjKQzz7eJiXkoJ80PRnv6UdJdxvPhlI5lP7QjAy8OqL+b8b74EFRS6ivzKNuLjGCJaN9qSslKfMjy2T4/A7yQq4ISVd+HMwiGMvjFWOnxHxIMYZ96SOm4Fhuf9oiP4UEcR1WWa/pQGkbInHLjA4uIMqXsg16s+8MIY0K3Ax+S77nKyuaxQrTLkWPCjZYgGd3ANMt1hyNHjrzo/4QsJ9JJvvJSAAAAAElFTkSuQmCC";
                break;
            case 'divider':
                return "data:image/gif;base64,R0lGODlhPQIcAMQAAOHh4fPz8/f39+vr6/r6+vn5+fz8/Ojo6Ofn5/Dw8Onp6eTk5P39/fLy8uPj4/j4+O/v7/7+/uzs7O7u7vb29ubm5vv7++Li4vX19eXl5fT09PHx8e3t7erq6uDg4P///yH5BAAAAAAALAAAAAA9AhwAQAX/4CeOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSCwaj0icAIIAeDwXxERAMxCS2Kx2y+16v+CwTeHhuCwQx9OzSBhUkQIFIBHb7/i8fs/v+/+AgYKDhIWGh4iJiouMjY4lAgNqaxUHCVcpDwNOHnQFKgkeEI+kpaanqIoEAgEBD2+psbKztLW2t7i5uru8vb6/wME7DxkdsCUEExdrChQuGE8AwtPU1dYoDw7OJwISnB4VDQwpGGSUEg0CBQ0VExHX8PHyugtPDTIMAQhrHggQrgXkDKjwYJ7BgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihzpSJ+EABgCrCSQtMbBBAswDCwbRbKmTWoRQg1YEUHDvicLxLE44MHBzaNIb1k4IIFCQAEbFCwDOgHTCQYb6gFNALNEBAQHklLkR7as2bNo06pdy7at27dw48qdS5cuCgsAdsIJQDTaACooDARQ8O3JAask6ipezLix48eQIz8Wa9DJgnEvIn1ToAGzCQIZaFIeTfoRgQEJbBhoYI7sgQClY8ueTbu27du4c+vezbu3798iQgAAOw==";
                break;
        }
    }

}
