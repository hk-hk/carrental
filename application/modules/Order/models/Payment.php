<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Model {

	public $_table = 'payment'; // database table
	public $primary_key   = 'pay_id'; 
 	public function __construct() {

        parent::__construct();

    }
    public function get_by_many_with_user($key,$val){
        $this->db->select("payment.*");
        $this->db->select("cr_users.user_fname");
        $this->db->select("cr_users.user_lname");
        $this->db->select("cr_users.user_role");
        $this->db->select("ums_account.account_name");
        $this->db->from($this->_table);
        $this->db->where($key,$val);
        $this->db->join('cr_users`', '`cr_users`.`id` = `payment`.`created_by`','inner');
        $this->db->join('ums_account`', '`ums_account`.`account_id` = `payment`.`account_id`','inner');
        $query = $this->db->get();
        return $query->result();
    }
}
