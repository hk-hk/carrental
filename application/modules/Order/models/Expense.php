<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Expense extends MY_Model
{
    public $_table      = 'cr_trip_expense'; // database table
    public $primary_key = 'id'; // primary key
    // public $before_create = array('timestamps');
    // public $column_order  = array('cr_orders.order_id', null, null, 'pickup_date', 'booking_amount', 'status', null); //set column field database for datatable orderable
    // public $column_search = array('status', 'user_id', 'supplier_id'); //set column field database for datatable searchable
    public $order = array('cr_trip_expense.id' => 'desc'); // default order
    // protected function timestamps($terminal)
    //    {
    //        $terminal['modify_date'] = date('Y-m-d H:i:s');
    //        $terminal['modify_date'] = date('Y-m-d H:i:s');
    //        return $terminal;
    //    }
    public function __construct()
    {

        parent::__construct();

    }

    public function _get_datatables_query() // dynamic search query of. user.

    {
        $this->db->select("cr_maintenance.*");
        $this->db->select("cr_maintenance.id as mid");
        $this->db->select("cr_users.user_fname");
        $this->db->select("cr_users.user_lname");
        // $this->db->select("cr_manage_attributes.*");
        $this->db->select("cr_manage_cars.*");
        $this->db->select("cr_manage_attributes.title as maker");
        $this->db->from('cr_maintenance');
        // $this->db->select("IF(cr_users.user_company!='',cr_users.user_company,cr_users.user_fname) as supplier,cr_users.profile_image as profile_image");
        // $this->db->join('`cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`','inner');
        $this->db->join('cr_manage_cars`', '`cr_manage_cars`.`id` = `cr_maintenance`.`car_id`', 'inner');
        $this->db->join('cr_users`', '`cr_users`.`id` = `cr_manage_cars`.`user_id`', 'inner');
        $this->db->join('cr_manage_attributes`', '`cr_manage_attributes`.`id` = `cr_manage_cars`.`model`', 'inner');
        //$this->db->join('`cr_orders`', '`cr_orders`.`car_id` = `cr_manage_cars`.`id`','left');
        // $this->db->where('cr_users.user_active', 'YES');
        $this->db->where('cr_users.user_role', 'supplier');
        if ($this->input->post('user_id')) {
            $this->db->where('cr_manage_cars.user_id', $this->input->post('user_id'));
        }
        if ($this->input->post('maker')) {
            $this->db->where('cr_manage_attributes.id', $this->input->post('maker'));
        }
        if ($this->input->post('maker')) {
            $this->db->where('cr_manage_attributes.id', $this->input->post('maker'));
        }
        if ($this->input->post('driver')) {
            $this->db->where('', $this->input->post('driver'));
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if (isset($_POST['length']) && $_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        // echo $this->db->last_query();die("dgfdfgs");
        return $query->result();
    }

    public function cus_update_by($key, $primary_value, $key2, $primary_value2, $data, $skip_validation = false)
    {
        $valid = true;
        $data  = $this->trigger('before_update', $data);
        if ($skip_validation === false) {
            $data = $this->validate($data);
        }
        if ($data !== false) {
            $result = $this->_database
                ->where($key, $primary_value)
                ->where($key2, $primary_value2)
                ->set($data)
                ->update($this->_table);
            $this->trigger('after_update', array($data, $result));
            return $result;
        } else {
            return false;
        }
    }

    public function get_latest($key, $val)
    {
        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->where($key, $val);
        $this->db->where('is_latest', 1);
        $query = $this->db->get();
        return !empty($query->result()[0]) ? $query->result()[0] : [];
    }

    public function get_latest_by($key, $val,$key2, $val2,$type)
    {
        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->where($key, $val);
        $this->db->where($key2, $val2);
        $this->db->where('pay_to', $type);
        $this->db->where('is_latest', 1);
        $query = $this->db->get();
        return !empty($query->result()[0]) ? $query->result()[0] : [];
    }
    public function get_by_many_with_user($key, $val)
    {
        $this->db->select("cr_trip_expense.*");
        $this->db->select("cr_users.user_fname");
        $this->db->select("cr_users.user_lname");
        $this->db->select("cr_users.user_role");
        $this->db->from($this->_table);
        $this->db->where($key, $val);
        $this->db->join('cr_users`', '`cr_users`.`id` = `cr_trip_expense`.`created_by`', 'inner');
        $query = $this->db->get();
        return $query->result();
    }

}
