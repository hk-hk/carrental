<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paypment_kbz {

// everything else is sorted at the end
    static $char2order;
   
    static function compare($a, $b) {
        if ($a == $b) {
            return 0;
        }
// lazy init mapping
        $LETTERS = range(chr(32), chr(127));
        $strorder = join('', $LETTERS);
        if (empty(self::$char2order)) {
            $order = 1;
            $len = mb_strlen($strorder);
            for ($order = 0; $order < $len; ++$order) {
                self::$char2order[mb_substr($strorder, $order, 1)] = $order;
            }
        }
        $len_a = mb_strlen($a);
        $len_b = mb_strlen($b);
        $max = min($len_a, $len_b);
        for ($i = 0; $i < $max; ++$i) {
            $char_a = mb_substr($a, $i, 1);
            $char_b = mb_substr($b, $i, 1);
            if ($char_a == $char_b)
                continue;
            $order_a = (isset(self::$char2order[$char_a])) ?
                    self::$char2order[$char_a] : 9999;
            $order_b = (isset(self::$char2order[$char_b])) ?
                    self::$char2order[$char_b] : 9999;
            return ($order_a < $order_b) ? -1 : 1;
        }
        return ($len_a < $len_b) ? -1 : 1;
    }

    public function getHash($param_data){
        // uasort($param_data, 'Paypment_kbz::compare');
        $signdata = join('', $param_data);
        $hash = hash_hmac('sha1', $signdata, 'F46273C39A387D86E081BAD280EF98B22B541A6F2F1F7E827F6342D82DBDA003', false);
        // 857CEB32DA106AFEE1CF2E0169135FC858CB7A3C22DE0585B287E25E1883E61E
        // SZHLNVQXB1KYYQIY2MATHGLXWY3WJU4Q
        // $hash = strtoupper($hash);
        // $hash = urlencode($hash);
        // return urlencode($hash);
        return $hash;
    }
    public function getHash2c2p($param_data,$currency){
        // uasort($param_data, 'Paypment_kbz::compare');
        $signdata = join('', $param_data);
        $key="";
        if($currency=="mmk"){
            $key='F46273C39A387D86E081BAD280EF98B22B541A6F2F1F7E827F6342D82DBDA003';
        }
        if($currency=="usd"){
            $key='C712617C09C1DB2B58BEC2CD43A2669C79BF43044A759A46895F7680EDDE90F7';
        }
        $hash = hash_hmac('sha1', $signdata, $key , false);
        // 857CEB32DA106AFEE1CF2E0169135FC858CB7A3C22DE0585B287E25E1883E61E
        // SZHLNVQXB1KYYQIY2MATHGLXWY3WJU4Q
        // $hash = strtoupper($hash);
        // $hash = urlencode($hash);
        // return urlencode($hash);
        return $hash;
    }

}
