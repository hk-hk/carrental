<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ToolModel extends MY_Model {
		public function __construct()
		{
			parent::__construct();
		}

		public function create_options($target , $where=array(),$order=array()) {

			$this->db->select('*');
			$this->db->from($target);

			//var_dump(array_filter($where));
			if (count($where) > 0) {
				foreach($where as $key=>$val)
				$this->db->where($key,$val);
			}
			if (count($order) > 0) {
				foreach($order as $key=>$val)
				$this->db->order_by($key,$val);
			}
			$query = $this->db->get();
			//echo $this->db->last_query();
			return $query->result();
		}
		public function run_query($value)
		{
			return $this->db->query($value)->result_array();
		}


}