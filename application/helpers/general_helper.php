<?php

function get_exchangeRate(){
    $CI = get_instance();
    $CI->db->from('cr_manage_attributes');
    $CI->db->where('id', 185);
    $CI->db->where('status', '1');
    $query = $CI->db->get();    
    $row = $query->result();
    if(!empty($row) && isset($row[0]->short_desc) && $row[0]->short_desc>0){
        return $row[0]->short_desc;
    }else{
        return FALSE;
    }
    
}
function getLang($langKey = FALSE){
    $CI = get_instance();
    return $CI->lang->line($langKey);
}
function get_attributes($parentids='', $byParent=false) 
{
	$CI = get_instance();
	$CI->db->from('cr_manage_attributes');
	if(is_array($parentids) && count($parentids)>0){
		$CI->db->where_in('parent_id', implode(',',$parentids));
	}else if(is_int($parentids)){    
		$CI->db->where('parent_id', $parentids);
	}else{
		$CI->db->where('status', 1);
	}
	$query = $CI->db->get();    
	//echo $CI->db->last_query();die; 
	$attributes = $query->result();
	foreach ($attributes as $attr){
		if($byParent){
			$attributesList[$attr->parent_id][$attr->id]['id'] = $attr->id; 
			$attributesList[$attr->parent_id][$attr->id]['title'] = $attr->title; 
			$attributesList[$attr->parent_id][$attr->id]['short_desc'] = $attr->short_desc; 
		}else{
			$attributesList[$attr->id]['title'] = $attr->title; 
			$attributesList[$attr->id]['short_desc'] = $attr->short_desc; 
		}
	}
	return $attributesList;
}
function getNumFormat($number,$only_number=FALSE){
    $CI = get_instance();
    
    $active_currency = $CI->session->userdata('active_currency');
    if($active_currency=='mmk'){
            $CI->currency = '<span class="currency-box">MMK</span>';
    }else if($active_currency=='usd'){
        $CI->currency = '<span class="currency-box">USD</span>';
        $exchangRate = get_exchangeRate();
        if($exchangRate){
            $number = ($number/$exchangRate);
            if($only_number){
                return number_format($number, 0, ".", ',');
            }
        }
    }else{
        $CI->currency = '<span class="currency-box">MMK</span>';
    }
    if($only_number){
        return number_format($number, 0, "", '');
    }else{
        return $CI->currency.number_format($number, 0, ".", ',');
    }
}
