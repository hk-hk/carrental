<?php
function str_rand($length = 8, $seeds = 'alphanum') {
    // Possible seeds
    $seedings['alpha'] = 'abcdefghijklmnopqrstuvwqyz';
    $seedings['numeric'] = '0123456789';
    $seedings['alphanum'] = 'abcdefghijklmnopqrstuvwqyz0123456789';
    $seedings['hexidec'] = '0123456789abcdef';
    // Choose seed
    if (isset($seedings[$seeds])) {
        $seeds = $seedings[$seeds];
    }
    // Seed generator
    list($usec, $sec) = explode(' ', microtime());
    $seed = (float) $sec + ((float) $usec * 100000);
    mt_srand($seed);
    // Generate
    $str = '';
    $seeds_count = strlen($seeds);
    for ($i = 0; $length > $i; $i++) {
        $str .= $seeds{mt_rand(0, $seeds_count - 1)};
    }
    return strtoupper($str);
}


$invoice = "000000000000".str_rand(8);

$hashFields = array(
                    'amount'=> '000000000150',
                    'invoiceNo'=>$invoice,
                    'merchantID'=>'205104001002799',
                    'productDesc'=>"Mazda-M3",
                    'userDefined1'=>'userDefined1',
                    'userDefined2'=>'userDefined2',
                    'userDefined3'=>'userDefined3',
                    'currencyCode'=>'104'
            );
$hasValuepatter = '';
foreach ($hashFields as $v){
    $hasValuepatter .=$v;
}

$hasValuepatter = ord($hasValuepatter);
//Generate HashValue
$signData = hash_hmac('sha1',$hasValuepatter,'2GWWDOLHTF4CX1KNK4ENPTZOKLQV2FIG', false);
$signData= strtoupper($signData);
$hashValue = urlencode($signData);
//Process fields key and value for payment.
$processedData = array(
                    'amount'=> '000000000150',
                    'invoiceNo'=>$invoice,
                    'merchantID'=>'205104001002799',
                    'productDesc'=>"Mazda-M3",
                    'userDefined1'=>'userDefined1',
                    'userDefined2'=>'userDefined2',
                    'userDefined3'=>'userDefined3',
                    'currencyCode'=>'104',
                    'hashValue' =>$hashValue
                );
$datainfo = $processedData; 
//Payment URL
$payment_url = 'http://122.248.120.252:60145/UAT/Payment/Payment/pay'; 
?>
<div class="payment-form">
    <Form method="post" action="<?php echo $payment_url; ?>">
        <?php foreach ($datainfo as $k=>$v){?>
            <?php echo $k;?>-: <input type="text" id="<?php echo $k;?>" name="<?php echo $k;?>" value="<?php echo $v;?>" size="50"/><br/>
        <?php } ?>
    <input type="submit" value="Submit"/>
    </Form>
</div>
<style>
    .payment-form{ padding: 2% 25%; }
</style>