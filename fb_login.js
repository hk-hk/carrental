window.fbAsyncInit = function() {
    FB.init({
      appId      : '1496479980456801',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.12'
    });
      
    FB.AppEvents.logPageView();   
   
  FB.getLoginStatus(function(response) {
      FB.api('/me?fields=id,name,first_name,last_name,email,gender,locale,picture', function(response) 
      {
        if (typeof response.id !== 'undefined') {
          //console.log(response);
        }else{

        }
      });
      //console.log(response.authResponse.accessToken);
  }); 
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


 
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    FB.api('/me?fields=id,name,first_name,last_name,email,gender,locale,picture', function(response) 
      {
        if (typeof response.id !== 'undefined') {
          jQuery.ajax({
            type: 'POST',
            url: SITE_URL+'fb-login',
            dataType: 'json',
            data: response,
            success: function(resultData) {
              if(resultData.return_type==1){
                $("#ftlwf_token").val(resultData.token)
                $('#update_user_type').modal('show');
              }else if(resultData.return_type==0){
                  window.location.href = resultData.redirect_url;
              }else{
                alert("Please contact to support!! May be your account has been suspended by admin.")
              }
            }
          });
          
        }else{

        }
      });
    //console.log(response.authResponse.accessToken);
  });
}
jQuery(document).ready(function(){
  jQuery(".update_type").click(function(){
    document.getElementById("update_type").submit();
  });
});