function run_waitMe(el, num, effect){
    text = 'Please wait...';
    fontSize = '';
    switch (num) {
      case 1:
      maxSize = '';
      textPos = 'vertical';
      break;
      case 2:
      text = '';
      maxSize = 30;
      textPos = 'vertical';
      break;
      case 3:
      maxSize = 30;
      textPos = 'horizontal';
      fontSize = '18px';
      break;
    }
    console.log(effect)
    el.waitMe({
      effect: effect,
      text: text,
      bg: 'rgba(255,255,255,0.7)',
      color: '#000',
      maxSize: maxSize,
      source: 'img.svg',
      textPos: textPos,
      fontSize: fontSize,
      onClose: function() {}
    });
}

$(function() {

  $(document).ajaxStart(function(){ 
  //$('#ajaxProgress').show();
  run_waitMe($("body"), 2, 'facebook'); 
  console.log("Ajax start");
  });
  $(document).ajaxStop(function(){ 
  //$('#ajaxProgress').hide(); 
  $('body').waitMe('hide');
  //console.log("Ajax stope");
  });
});

// $.number( $('input[type=number]').val(), 2 );
function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

// var number=addCommas($(':input[name="amount"]').val());

// $(':input[name="amount"]').val(number);

// $(':input[name="amount"]').keyup(function(){
//   $(':input[name="amount"]').val(addCommas($(this).val()));
//   // $(':input[name="amount"]').val(addCommas($(this).val()));  
// });

$(function(){
        // Set up the number formatting.
  $('.number-format').number( true, 0 );
        
        
  $('form').submit(function() {
      if ($(this).valid()) {
          // Remove commas from any amount inputs
          $('.number-format').number(true, 0, '.', '');
      }else{
        return false;
      }
  });
});

function create_new(url){
  // hash = '#tab_2';

  // $("a[href="+url+"]");
  // hash = window.location.hash;
  // $("a[href=#{hash}]").click()
  window.location.href=url;
}