function tran_load(id){
      $("#files_dd").html("");
      $.post(SITE_URL+"/tran_load/"+id,function(data){
        obj=$.parseJSON(data);
        $("#transaction_data_dd").html(obj.transaction_data_dd);
        $("#category_dd").html(obj.category_dd);
        $("#process_by_dd").html(obj.process_by_dd);
        // $("#paymentto_dd").html(obj.paymentto_dd);
        $("#paymentto_dd").html(obj.paymentto_dd);
        $("#amount_dd").html(obj.amount_dd);
        $("#payment_type_dd").html(obj.payment_type_dd);
        $("#description_dd").html(obj.description_dd);
        
        if(obj.files.length>0){
          // console.log(obj.files);
          $.each($.parseJSON(obj.files),function(k,v){
            $("#files_dd").append(light_img(v));

          });
        }
      
      });

    }
    function light_img(val){
      var url=BASE_URL+"/uploads/file_attachments/";
      var html ='<div class="col-xs-6 col-md-4 pull-right">'
                +'<a href="'+url+val+'" target="_blank" class="thumbnail">'
                  +'<img src="'+url+'thumb/350X250/'+val+'" height="100px" width="100px">'
                +'</a>'
              +'</div>';
      return html;
    }