//Datepicker 
$(document).ready(function(){
   	$("#total_by_admin").on("keyup blur", function(){
 		$("#admin_amount").val($(this).val());  
   	});
    if($("#currency_switcher").length>0){
        jQuery("#currency_switcher").on('click, change', function(){
            window.location = SITE_URL+"changecurrency/"+$(this).val();
        });
    }
    
// Carausal for Newest Cars
if($(".newset-cars .owl-carousel").length>0){
    $(".newset-cars .owl-carousel").owlCarousel({
      margin:15,
      loop:false,
      dots: false,
      nav: true,
      items: 4,
      autoplay: true,
      autoplayTimeout: 4000,
      responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    }); 
}
// Carausal for Welcome Text
if($(".welcome-image .owl-carousel").length>0){
    $(".welcome-image .owl-carousel").owlCarousel({
      margin:0,
      loop:false,
      dots: false,
      nav: true,
      items: 1,
      autoplay: true,
      autoplayTimeout: 4000
    }); 
}
if($(".select2").length>0){
  $(".select2").select2();
}
// Carausal for Testimonial
if($(".client-section .owl-carousel").length>0){
    $(".client-section .owl-carousel").owlCarousel({
      margin:15,
      loop:false,
      dots: true,
      nav: false,
      items: 2,
      autoplay: true,
      autoplayTimeout: 4000,
      responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:2
            }
        }
    }); 
}

// Carausal for Place Desicription Image
if($(".places-image-slider .owl-carousel").length>0){
    $(".places-image-slider .owl-carousel").owlCarousel({
      margin:0,
      loop:false,
      dots: true,
      nav: true,
      items: 1,
      autoplay: true,
      autoplayTimeout: 4000,
      responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
}


    // Cauntry Code
    if($('#select_country').length){
        $('#select_country').attr('data-selected-country','CN');
        $('#select_country').flagStrap();
    }
});

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

// About Us Page Counter
jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});

// Load More Images
$(function () {
    $(".places-box").slice(0, 9).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".places-box:hidden").slice(0, 3).slideDown();
        if ($(".places-box:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
    });
});

// Destinated location click on Highway
$(document).ready(function(){});



$(".rent_type").click(function(){
    
    $("#selected_rent_type").val($(this).attr("id"));
});
function checkValid(){
    
   var selected_rent_type = $("#selected_rent_type").val();
   var returntype = true;
   //alert($("#car_city_daily").val());
   //$("#car_city_weekly").append("<span>Hello</span>");
   if(selected_rent_type==36){
       if($("#car_city_daily").val() ==''){ 
           $("#car_city_daily_lbl").addClass("error");
           //$("#daily_pickup_date_lbl").addClass("error");
           returntype = false;
       }
           
   }else if(selected_rent_type==37){
       if($("#car_city_weekly").val()==''){
           $("#car_city_weekly_lbl").addClass("error");
           //$("#weekly_pickup_date_lbl").addClass("error");
           returntype = false;
       }
   }else if(selected_rent_type==38){
       if($("#car_city_monthly").val()==''){
           $("#car_city_monthly_lbl").addClass("error");
          // $("#monthly_pickup_date_lbl").addClass("error");
           returntype = false;
       }
   }
   if(!returntype){
       alert("Please select required fields");
   }
   return returntype;
}

function daydiff(first, second) {
    return Math.round((second-first)/(1000*60*60*24));
}
function getActiveCountry(){
     jQuery.getJSON("https://freegeoip.net/json/", function (data) {
        var country = data.country_name;
        var country_code = data.country_code;
        var ip = data.ip;
        //alert(country+"-"+country_code);
        jQuery.ajax({
             type: 'POST',
             url: SITE_URL+'set-default-country',
             data: {country_name:country,country_code:country_code},
             success: function(resultData) { 
                //alert(resultData);
                 location.reload();
             }
         });
    });
}

//Modify Search pop-up with set each day hours
$(function(){});
