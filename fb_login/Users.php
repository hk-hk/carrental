<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->data['module'] = 'Users'; // Load Users module
		$this->load->model('User', 'user'); // Load User modal
                //$this->user->check_permission(); // check permission for accessing function of this file
        //echo $this->session->userdata("profile_image1");die("sdf");
		if($this->session->userdata("crm_user_logged")){
			redirect(site_url().'profile');
		}
                $this->lang->load($this->data['module'].'/users', $this->getActiveLanguage());
                //$this->load->library('facebook');

	}
	public function facebook_login(){
           $userInfo['id'] = $this->input->post('id' ,true);
           $userInfo['oauth_provider'] = 'facebook';//$this->input->post('oauth_provider' ,true);
           $userInfo['email'] = $this->input->post('email' ,true);
           $userInfo['name'] = $this->input->post('name' ,true);
           $userInfo['picture'] = $this->input->post('picture' ,true);
           $picture = $this->input->post('picture' ,true);
           $fbuinfo = $this->user->checkSocialUser($userInfo['id'],$userInfo['oauth_provider']);
           // echo $user_id;die;
            $return_data['return_type'] = 0;
            if(!is_object($fbuinfo)){
                $userStatus = $this->user->manageSocialUser($userInfo);
                $user_id = $userStatus['user_id'];
                if($userStatus['register_type'] == 'byfb'){
                    $this->load->helper('security');
                    $return_data['token'] = do_hash($userInfo['id']); // SHA1
                    $return_data['return_type'] = 1;
                    $this->session->set_userdata('ftlwf_token', $return_data['token']);
                }else{
                    $return_data['return_type'] = 0;
                }
                $this->session->set_userdata('profile_image1',$picture['data']['url']);
            }else{
                $this->session->set_userdata('profile_image1', $fbuinfo->profile_pic);
                $user_id = $fbuinfo->user_id;
                //print_r($fbuinfo);die;
            }
            $user_obj = $this->user->get_by(array('id'=>$user_id,'user_active'=>'YES'));

           //print_r($user_obj);die("sdf");
            if ($user_obj && $user_obj->user_active !='deleted') {
                $crm_user_logged = ($return_data['return_type']==1)?FALSE:TRUE;
                $set_sessions = [
                                    'crm_user_id'=>$user_obj->id,
                                    'crm_user_email'=>$user_obj->user_email,
                                    'crm_user_fname'=>$user_obj->user_fname,
                                    'crm_user_lname'=>$user_obj->user_lname,
                                    'cr_user_role' => ($user_obj->user_role!='')?$user_obj->user_role:'customer',
                                    'crm_user_logged'=>$crm_user_logged,
                                    'user_created_at' =>$user_obj->created_at,
                                    'profile_image' =>$user_obj->profile_image
                                ];
                $this->set_sessions($set_sessions);
                $this->set_msg_flash([
                        'msg'=>sprintf($this->welcome_message,$user_obj->user_fname." ".$user_obj->user_lname),
                        'typ'=>1
                ]);
                if($user_obj->profile_image!=''){
                    $this->session->set_userdata('profile_image1', site_url("uploads/user/".$user_obj->profile_image));
                }
                if($return_data['return_type']==0){  
                    if($user_obj->user_role!='customer'){
                        $return_data['redirect_url'] = site_url(CRM_VAR.'/dashboard');
                    }else{
                        $return_data['redirect_url'] = site_url('myaccount/orders');
                    }
                }  
            }else{
                $return_data['return_type'] = 3;
            }
            echo json_encode($return_data);die;
	}
}