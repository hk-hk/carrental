<html>
    <head>
        <title>Facebook Login</title>
        <script src="https://www.sonicstartravel.com/public/front/js/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script src="fb_login.js" type="text/javascript"></script>
    </head>
    <body>
    	<div id="fb-login">
        	<fb:login-button size="xlarge" scope="public_profile,email" onlogin="checkLoginState();"> Login with Facebook </fb:login-button>
    	</div>
    	<div id="fb-profile" style="display: none;"></div>
    </body>
</html>